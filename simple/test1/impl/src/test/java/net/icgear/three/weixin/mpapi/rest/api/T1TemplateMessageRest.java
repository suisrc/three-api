package net.icgear.three.weixin.mpapi.rest.api;

import java.sql.Timestamp;

import org.junit.Test;

import com.suisrc.jaxrsapi.client.ClientUtils;

import net.icgear.product.ztest1.weixin.template.OPENTM415627956;
import net.icgear.three.weixin.mpapi.rest.dto.template.TemplateMessageBody;
import net.icgear.three.weixin.mpapi.rest.dto.template.TemplateMessageResult;

public class T1TemplateMessageRest {

    @Test
    public void test() {

        // 模版消息
        OPENTM415627956 message = new OPENTM415627956();
        message.setTitle("您好，您已成功加入本次内测。");
        message.setMember("申请参加内测的用户1");
        message.setTime(new Timestamp(System.currentTimeMillis()));
        message.setContent("我的主页-签到功能（点击详情查看更新日志）");
        message.setRemark("感谢您申请加入此次内测，有任何反馈建议，可随时通过APP内通道反馈给我们，谢谢。");
        
        TemplateMessageResult res = ClientUtils.getRestfulImpl("http://127.0.0.1:8771", TemplateMessageRest.class, rest -> {
            TemplateMessageBody body = new TemplateMessageBody();
            body.setTouser("sldfdsa");
            body.setTemplateId("sdfadfa");
            TemplateMessageResult res1 = rest.sendTemplateMessage("123", body);
            return res1;
        });
        System.out.println(res);
    }

}
