package net.icgear.product.ztest1.weixin.handler;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import net.icgear.three.weixin.qyapi.handler.DefaultQyPermanentHandler;

@ApplicationScoped
@Named("qytest-permanent")
public class QyPermanentHandler extends DefaultQyPermanentHandler {

}
