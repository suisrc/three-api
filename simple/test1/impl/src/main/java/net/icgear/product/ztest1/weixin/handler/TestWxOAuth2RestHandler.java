package net.icgear.product.ztest1.weixin.handler;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

import com.suisrc.core.Global;
import com.suisrc.core.fasterxml.FF;
import com.suisrc.core.fasterxml.FasterFactory.Type;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.oauth2.WxOAuth2RestHandler;
import net.icgear.three.weixin.open.handler.DefaultWxOAuth2RestHandler;
import net.icgear.three.weixin.qyapi.rest.dto.user.UserInfoResult;

@Named("weixin-oauth2-test")
@Dependent
public class TestWxOAuth2RestHandler extends DefaultWxOAuth2RestHandler implements WxOAuth2RestHandler {
    
    @Override
    public String getOAuth2Secret() {
        return "123456";
    }
    
    @Override
    public boolean doUserTicket2Redirect(Object owner, String openid, String scope, String key, String userTicket, boolean isFirst) {
        System.out.println(openid);
        UserInfoResult info = (UserInfoResult) Global.getThreadCache().get(WxConsts.OAUTH2_USER_INFO);
        if (info != null) {
            String tr = FF.getDefault().bean2String(info, Type.JSON);
            System.out.println(tr);
        }
        return super.doUserTicket2Redirect(owner, openid, scope, key, userTicket, isFirst);
    }
}
