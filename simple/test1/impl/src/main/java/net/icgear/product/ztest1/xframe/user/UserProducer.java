package net.icgear.product.ztest1.xframe.user;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Produces;

import net.icgear.im.core.IM;
import net.icgear.im.core.user.LoginAccount;

/**
 * <p> 如果当前系统没有登录方面控制内容，需要对该方法的内容进行控制和实现
 * 
 * @author Y13
 *
 */
@Alternative
@ApplicationScoped
public class UserProducer {
  private static final String JAX_LOGIN_KEY = "JAX_LOGIN_KEY_836847172";

  
  /**
   * <p> 获取当前登录者信息
   * 
   * @return
   */
  @Produces
  @RequestScoped
  public LoginAccount getCurrentLoginAccount() {
    LoginAccount account = IM.getCacheSafe(IM.getSessionCache(), JAX_LOGIN_KEY, LoginAccount.class);
    return account != null ? account : new LoginAccountSimple();
  }
  
}
