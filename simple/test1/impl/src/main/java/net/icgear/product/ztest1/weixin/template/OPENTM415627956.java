package net.icgear.product.ztest1.weixin.template;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import net.icgear.three.weixin.core.annotation.WxTemplateMessage;
import net.icgear.three.weixin.core.annotation.WxTemplateProperty;

/**
 * 内测参加成功通知
 * @author Y13
 *
 */
@WxTemplateMessage("OPENTM415627956")
public class OPENTM415627956 {
    
    @WxTemplateProperty("first")
    private String title;

    private String remark;
    
    @WxTemplateProperty("keyword1")
    private String member;
    
    private Timestamp time;
    
    @WxTemplateProperty("keyword3")
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
    
    @WxTemplateProperty("keyword2")
    public String getTime2() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
        return sdf.format(time);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
}
