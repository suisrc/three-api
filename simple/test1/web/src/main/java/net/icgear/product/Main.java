package net.icgear.product;

/**
 * 程序入口
 * 
 * 该对象需要在pom.xml文件中进行配置
 * 
 * @author Y13
 *
 */
public class Main {

  /**
   * 入口Main
   * 
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
    Config.start(args);
  }

}
