package net.icgear.three.weixin.db.config.entity;

import javax.persistence.Entity;

import net.icgear.three.weixin.core.db.WxConfigDB;

/**
 * 配置信息
 * 
 * @author Y13
 *
 */
@Entity
public class WxConfigEntity implements WxConfigDB {

  @Override
  public String getWxNodeId() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getAppUniqueKey() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getWxAppId() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getWxSuiteId() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Integer getAgentId() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getWxAppSecret() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getWxToken() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getWxEncodingKey() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean isWxEncrypt() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public String[] getIndustryIds() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean isIndustryEdit() {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public String getEncryptType() {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public String getWebDomain() {
    return null;
  }

  @Override
  public String getWebIndex() {
    return null;
  }

  @Override
  public String getWebIndex(String key) {
    return null;
  }
}
