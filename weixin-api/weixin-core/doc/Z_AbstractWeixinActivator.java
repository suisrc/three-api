package net.icgear.three.weixin.core;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;

import com.suisrc.core.Global;
import com.suisrc.jaxrsapi.core.AbstractTokenActivator;
import com.suisrc.jaxrsapi.core.ApiActivator;
import com.suisrc.jaxrsapi.core.token.Token;
import com.suisrc.jaxrsapi.core.token.TokenReference;

import net.icgear.three.weixin.core.bean.WxAccessToken;
import net.icgear.three.weixin.core.filter.WxClientResponseFilter;
import net.icgear.three.weixin.core.handler.WxTokenHandler;
import net.icgear.three.weixin.core.handler.WxTopologyHandler;

/**
 * 程序入口配置抽象
 * 
 * 20180810, 留作备份使用
 * 
 * 由于新系统激活器迁移出多微信配置激活器分支，所以这里需要对激活器进行特殊处理
 * 
 * @author Y13
 */
@Deprecated
public abstract class Z_AbstractWeixinActivator extends AbstractTokenActivator implements ApiActivator, WxConfig {
    
    /**
     * 应用名称
     */
    private String appName;
    
    /**
     * 应用ID
     */
    private String appId;
    
    /**
     * 企业应用ID
     */
    private String appSuiteId;
    
    /**
     * 应用秘钥
     */
    private String appSecret;
    
    /**
     * 应用回调加密令牌
     */
    private String appToken;
    
    /**
     * 微信回调加密秘钥
     */
    private String appEncodingKey;
    
    /**
     * 微信回调内容是否加密
     */
    private boolean appEncrypt;
    
    /**
     * 集群控制
     */
    private WxTopologyHandler topologyHandler;
    
    //--------------------------------------------需要实现的接口
    
    /**
     * 获取令牌生产者
     * 
     * 如果该方法无法满足要求，请重写getTokenByRemote方法
     */
    protected abstract WxTokenHandler getWxTokenHandler(String tokenKey);
    
    //-------------------------------------------可以从写的接口
    
    /**
     * 获取应用名称的关键字
     */
    protected String getAppNameKey() {
        return CoreWxConsts.WEIXIN_PRE + getAppUniqueKey() + ".endpoint.name";
    }
    
    /**
     * 获取应用ID的关键字
     */
    protected String getAppIdKey() {
        return CoreWxConsts.WEIXIN_PRE + getAppUniqueKey() + ".endpoint.id";
    }
    
    /**
     * 获取应用ID的关键字
     */
    protected String getAppSuiteIdKey() {
        return CoreWxConsts.WEIXIN_PRE + getAppUniqueKey() + ".endpoint.suite-id";
    }
    
    /**
     * 获取应用秘钥的关键字
     */
    protected String getAppSecretKey() {
        return CoreWxConsts.WEIXIN_PRE + getAppUniqueKey() + ".endpoint.secret";
    }
    
    /**
     * 获取回调加密令牌关键字
     */
    protected String getAppTokenKey() {
        return CoreWxConsts.WEIXIN_PRE + getAppUniqueKey() + ".endpoint.token";
    }
    
    /**
     * 获取回调加密秘钥关键字
     */
    protected String getAppEncodingKey() {
        return CoreWxConsts.WEIXIN_PRE + getAppUniqueKey() + ".endpoint.encoding-key";
    }
    
    /**
     * 获取回调加密秘钥关键字
     */
    protected String getAppEncryptKey() {
        return CoreWxConsts.WEIXIN_PRE + getAppUniqueKey() + ".endpoint.encrypt";
    }

    /**
     * 获取基本访问地址关键字
     */
    @Override
    protected String getBaseUrlKey() {
        return CoreWxConsts.WEIXIN_PRE + getAppUniqueKey() + ".endpoint.base-url";
    }
    
    //--------------------------------------------重写内容
    
    /**
     * 构造方法  
     */
    @Override
    @PostConstruct
    public void doPostConstruct() {
        // 应用的配置信息
        appName = System.getProperty(getAppNameKey());
        appId = System.getProperty(getAppIdKey());
        appSuiteId = System.getProperty(getAppSuiteIdKey());
        appSecret = System.getProperty(getAppSecretKey());
        appToken = System.getProperty(getAppTokenKey());
        appEncodingKey = System.getProperty(getAppEncodingKey());
        // 回调内容是否加密，默认使用加密
        appEncrypt = Boolean.valueOf(System.getProperty(getAppEncryptKey(), "true"));
        // 继续实现构造
        super.doPostConstruct();
        
        try {
            // 注入集群内容
            topologyHandler = createTopologyHandler();
        } catch (Exception e) {
            Global.getLogger().info(e.getMessage());
        }
    }
    
    /**
     * 获取当前执行的token关键字
     */
    @Override
    protected String getTokenKeyByCache() {
        return "";
    }

    /**
     * 
     */
    @Override
    protected Token getTokenByRemote(String tokenKey) {
        WxAccessToken result = getWxTokenHandler(tokenKey).getToken();
        if (result.getAccessToken() != null) {
            Token token = new Token();
            token.setAccessToken(result.getAccessToken());
            token.setExpiresIn(result.getExpiresIn());
            return token;
        } else {
            String info = String.format("获取认证发生异常: [%s]%s", result.getErrcode(), result.getErrmsg());
            throw new RuntimeException(info);
        }
    }

    /**
     * 
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(String key, Class<T> type) {
        if (key != null && type == String.class) {
            switch (key) {
                case CoreWxConsts.ACCESS_TOKEN:
                    return (T) getWxAccessToken();
                case CoreWxConsts.APP_ID:
                    return (T) getWxAppId();
                case CoreWxConsts.APP_SUITE_ID:
                    return (T) getWxSuiteId();
                case CoreWxConsts.APP_SECRET:
                    return (T) getWxAppSecret();
                case CoreWxConsts.APP_TOKEN:
                    return (T) getWxToken();
                case CoreWxConsts.APP_ENCODING_KEY:
                    return (T) getWxEncodingKey();
                case CoreWxConsts.APP_OPEN_ID:
                    String id = getWxSuiteId();
                    return (T) (id != null ? id : getWxAppId());
            }
        }
        return super.getAdapter(key, type);
    }
    
    @Override
    protected void registerTargetClient(Client client) {
        super.registerTargetClient(client);
        client.register(new WxClientResponseFilter());
    }
    //--------------------------------------------ApiActivator

    /**
     * 应用名称
     */
    @Override
    public String getAppName() {
        return appName;
    }

    //--------------------------------------------WxConfig
    
    /**
     * 获取服务器节点信息
     */
    @Override
    public String getWxNodeId() {
        return getAppName();
    }

    /**
     * 获取应用ID
     */
    @Override
    public String getWxAppId() {
        return appId;
    }
    
    /**
     * 企业应用ID
     */
    @Override
    public String getWxSuiteId() {
        return appSuiteId;
    }

    /**
     * 获取应用秘钥
     */
    @Override
    public String getWxAppSecret() {
        return appSecret;
    }

    /**
     * 获取微信加密令牌
     */
    @Override
    public String getWxToken() {
        return appToken;
    }

    /**
     * 获取微信加密秘钥
     */
    @Override
    public String getWxEncodingKey() {
        return appEncodingKey;
    }
    
    /**
     * 微信回调内容是否加密
     */
    @Override
    public boolean isWxEncrypt() {
        return appEncrypt;
    }
    
    /**
     * 加密方式
     */
    @Override
    public String getEncryptType() {
        return CoreWxConsts.ENCRYPT_TYPE_AES;
    }

    /**
     * 获取微信访问令牌
     */
    @Override
    public String getWxAccessToken() {
        return getToken();
    }

    /**
     * 清空微信访问令牌
     */
    @Override
    public void clearWxAccessToken() {
        clearToken(false);
    }
    //---------------------------------------------------------------------集群同步
    
    /**
     * 获取集群同步控制器
     * @return
     */
    protected WxTopologyHandler createTopologyHandler() {
        return null;
    }
    
    
    /**
     * 更新拓扑中的token
     * @param key
     * @param accessToken
     * @return
     */
    protected TokenReference putTokenByTopology(String tokenKey, TokenReference token) {
        return topologyHandler == null ? token : topologyHandler.putTokenByTopology(tokenKey, token);
    }

    /**
     * 获取拓扑中的token
     * @param key
     * @return
     */
    protected TokenReference getTokenByTopology(String tokenKey) {
        return topologyHandler == null ? null : topologyHandler.getTokenByTopology(tokenKey);
    }

    /**
     * 获取拓扑中的token
     * @param key
     * @return
     */
    protected List<TokenReference> getTokensByTopology() {
        return topologyHandler == null ? null : topologyHandler.getTokensByTopology();
    }
    
    /**
     * 检测拓扑网络中是否有人正在修改token
     * @return
     */
    protected boolean hasUpdateTokenByTopology(String tokenKey) {
        return topologyHandler == null ? false : topologyHandler.hasUpdateTokenByTopology(tokenKey);
    }
    
    /**
     * 标记拓扑网络，当前环境正在修改token
     * 
     * 当执行set操作后，应该立即解锁拓扑网络中的标记
     * @return ture 标记成功， false 标记失败，当前有其他环境正在修改token
     */
    protected boolean lockUpdateTokenByTopology(String tokenKey) {
        return topologyHandler == null ? true : topologyHandler.lockUpdateTokenByTopology(tokenKey);
    }
   
    /**
     * 解锁拓扑网络中对token更新的锁定
     * @return
     */
    protected void unlockUpdateTokenByTopology(String tokenKey) {
        if (topologyHandler != null) {
            topologyHandler.unlockUpdateTokenByTopology(tokenKey);
        }
    }

    /**
     * 远程服务代理KEY
     * @return
     */
    protected String getProxyHostKey() {
        return CoreWxConsts.WEIXIN_PRE + getAppUniqueKey() + ".proxy.host";
    }

    /**
     * 远程服务器代理KEY
     * @return
     */
    protected String getProxyPortKey() {
        return CoreWxConsts.WEIXIN_PRE + getAppUniqueKey() + ".proxy.port";
    }
    /**
     * 获取代理地址
     * @return
     */
    public String createProxyHost() {
        return System.getProperty(getProxyHostKey());
    }

    /**
     * 获取代理端口
     * @return
     */
    public int createProxyPort() {
        return Integer.getInteger(getProxyPortKey(), -1);
    }
}
