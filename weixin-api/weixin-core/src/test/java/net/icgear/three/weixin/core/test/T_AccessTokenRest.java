package net.icgear.three.weixin.core.test;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxAccessToken;
import net.icgear.three.weixin.core.handler.WxTokenHandler;

/**
 * 
 * 例子
 * 
 * @author Y13
 *
 */
@RemoteApi
public interface T_AccessTokenRest extends WxTokenHandler {

    /**
     * https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
     * 
     * @param grantType 是必须, 获取access_token填写client_credential
     * @param appid 是必须, 第三方用户唯一凭证
     * @param secret 是必须, 第三方用户唯一凭证密钥，即appsecret
     * @return
     */
    @GET
    @Path("cgi-bin/token")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxAccessToken getToken(@QueryParam("grant_type") String grantType, 
            @QueryParam("appid")@Value(WxConsts.APP_ID) String appid, 
            @QueryParam("secret")@Value(WxConsts.APP_SECRET) String secret);
    
    /**
     * 获取访问的令牌信息
     */
    default WxAccessToken getToken() {
        return getToken("client_credential", (String) null, (String) null);
    }
    
}
