package net.icgear.three.weixin.core.reply;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.media.MediaId;
import net.icgear.three.weixin.core.type.msg.BaseMessage;

/**
 * 图片
 * <Image>
 * <MediaId><![CDATA[media_id]]></MediaId>
 * </Image>
 */
@JacksonXmlRootElement(localName = "xml")
public class ReplyImageMessage extends BaseMessage {

    /**
     * 通过素材管理中的接口上传多媒体文件，得到的id。 必须
     */
    @JacksonXmlProperty(localName = "Image")
    @JsonProperty("Image")
    private MediaId image;

    public ReplyImageMessage() {
        setMsgType("image");
        setSystemToCreateTime();
    }

    /**
     * 获取通过素材管理中的接口上传多媒体文件，得到的id。 必须
     * @return the image
     */
    public MediaId getImage() {
        return image;
    }

    /**
     * 设定通过素材管理中的接口上传多媒体文件，得到的id。 必须
     * @param image the image to set
     */
    public void setImage(MediaId image) {
        this.image = image;
    }

}
