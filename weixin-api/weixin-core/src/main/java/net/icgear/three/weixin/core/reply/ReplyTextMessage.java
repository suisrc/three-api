package net.icgear.three.weixin.core.reply;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.type.msg.BaseMessage;

/**
 * 文本
 * <Content><![CDATA[你好]]></Content>
 */
@JacksonXmlRootElement(localName = "xml")
public class ReplyTextMessage extends BaseMessage {

    /**
     * 回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示） 必须
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "Content")
    @JsonProperty("Content")
    private String content;

    public ReplyTextMessage() {
        setMsgType("text");
        setSystemToCreateTime();
    }

    /**
     * 获取回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示） 必须
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * 设定回复的消息内容（换行：在content中能够换行，微信客户端就支持换行显示） 必须
     * @param content the content to set
     */
    public void setContent(String content) {
        this.content = content;
    }

}
