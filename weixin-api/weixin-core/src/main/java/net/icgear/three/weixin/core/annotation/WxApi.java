package net.icgear.three.weixin.core.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 接口描述
 * 
 * @author Y13
 */
@Target(TYPE)
@Retention(RUNTIME)
@Inherited
public @interface WxApi {

  /**
   * 归属类型 默认没有归属类型， 当没有归属类型及输入所有
   */
  String[] types() default {};
}
