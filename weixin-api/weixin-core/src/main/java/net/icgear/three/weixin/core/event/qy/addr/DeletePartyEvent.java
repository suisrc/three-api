package net.icgear.three.weixin.core.event.qy.addr;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.type.msg.BaseChangeContactEvent;

/**
 * 删除部门事件
 * 
 * @author Y13
 *
 */
@WxChangeType("delete_party")
@JacksonXmlRootElement(localName="xml")
public class DeletePartyEvent extends BaseChangeContactEvent {

    /**
     * 部门Id
     */
    @JacksonXmlProperty(localName = "Id")
    @JsonProperty("Id")
    private String id;

    /**
     * 获取部门Id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * 设定部门Id
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

}
