package net.icgear.three.weixin.core.listener;

import java.util.HashMap;
import java.util.Map;

import com.suisrc.core.Global;
import com.suisrc.core.reference.RefVal;

import net.icgear.three.weixin.core.rest.AbstractWxBindingOne;
import net.icgear.three.weixin.core.type.msg.BaseMessage;

/**
 * 
 * <p> 用锁防止微信服务器多次同时访问
 * 
 * @author Y13
 *
 */
public abstract class AbstractLockListener<T extends BaseMessage> {

  /**
   * 锁索引
   */
  private Map<String, RefVal<Object>> locks = new HashMap<>(4);
  
  /**
   * 处理结果
   * @param result
   * @param bean
   * @return
   */
  public boolean accept(RefVal<Object> result, T bean) {
    return false;
  }

  /**
   * 处理结果
   */
  public boolean accept(RefVal<Object> result, T bean, Object owner) {
    // 锁关键字
    String key = bean.getFromUserName() + "->" + bean.getToUserName();
    // 锁，防止多次访问
    RefVal<Object> lock = locks.get(key);
    if (lock == null) {
      // 将锁放入缓存
      locks.put(key, lock = new RefVal<>());
    }
    synchronized (lock) {
      try {
        if (lock.get() != null) {
          return processLastResult(key, result, lock.get(), bean, owner);
        }
        boolean res = processResult(key, result, bean, owner);
        if (!res && result.get() != null) {
          // 处理过程中已经有结果
          lock.set(result.get());
        }
        return res;
      } catch (Exception e) {
        return processException(key, e, result, bean, owner);
      } finally {
        // 必须强制移除锁
        locks.remove(key);
      }
    }
  }

  /**
   * 处理结果
   * @param key
   * @param result
   * @param bean
   * @param owner
   * @return
   */
  protected boolean processResult(String key, RefVal<Object> result, T bean, Object owner) {
    return processResult(result, bean, (AbstractWxBindingOne) owner);
  }
  
  /**
   * 处理结果
   * @param result
   * @param bean
   * @param owner
   * @return
   */
  protected abstract boolean processResult(RefVal<Object> result, T bean, AbstractWxBindingOne owner);

  /**
   * 处理异常
   * @param key
   * @param e
   * @param result
   * @param bean
   * @param owner
   * @return
   */
  protected abstract boolean processException(String key, Exception e, RefVal<Object> result, T bean, Object owner);

  /**
   * 处理上次的结果
   * @param key 
   * @param result
   * @param res
   * @param bean
   * @param owner
   * @return
   */
  protected boolean processLastResult(String key, RefVal<Object> result, Object res, T bean, Object owner) {
    Global.getLogger().info("接收到重复请求，直接返回上次处理结果：" + bean.getTargetRawData());
    result.set(res);
    return false;
  }

}
