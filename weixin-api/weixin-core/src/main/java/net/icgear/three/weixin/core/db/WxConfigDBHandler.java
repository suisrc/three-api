package net.icgear.three.weixin.core.db;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Named;

import com.suisrc.core.Global;
import com.suisrc.core.utils.CdiUtils;

import net.icgear.three.weixin.core.AbstractWeixinActivator;
import net.icgear.three.weixin.core.WxConfig;
import net.icgear.three.weixin.core.handler.CallbackActivatorHandler;
import net.icgear.three.weixin.core.handler.WxConfigHandler;

/**
 * 
 * 微信配置移动到数据库中，以最求更多的配置场景
 * 
 * 数据库配置参考
 * 
 * 当前内容并没有实现数据库的Entry和Service，参考实现参看weixin-xdb-config
 * 
 * @author Y13
 *
 */
@Named("weixin-config-dbRef")
@Dependent
public class WxConfigDBHandler implements WxConfigHandler, CallbackActivatorHandler {

  /**
   * 配置缓存
   */
  private Map<String, WxConfigDBProxy> configs = new ConcurrentHashMap<>();

  /**
   * 唯一性表示KEY
   */
  private String uniqueKey;

  /**
   * 
   */
  private WxConfigDBService service;


  @Override
  public void setActivator(AbstractWeixinActivator activator) {
    this.uniqueKey = activator.getAppUniqueKey();
    if (service instanceof CallbackActivatorHandler) {
      ((CallbackActivatorHandler) service).setActivator(activator);
    }
  }

  /**
   * 
   */
  @PostConstruct
  protected void doPostConstruct() {
    service = CdiUtils.selectFirst(WxConfigDBService.class);
    if (service == null) {
      // 无法完成配置
      throw new RuntimeException("not found WxConfigDBService Implement: " + WxConfigDBHandler.class.getName());
    }
  }

  /**
   * 获取微信配置信息
   * 
   * @param appKey
   * @return
   */
  public WxConfig findByAppKey(String appKey) {
    WxConfigDBProxy proxy = configs.get(appKey);

    if (proxy == null || proxy.isExpired()) {
      // 暂时没有代理的情况
      WxConfigDB target = service.findByUniqueAndAppKey(getAppUniqueKey(), appKey);
      if (target == null || target.getWxAppId() == null) {
        if (proxy != null) {
          configs.remove(appKey);
        }
        return null;
      }
      if (proxy == null) {
        proxy = new WxConfigDBProxy();
        configs.put(appKey, proxy);
      }
      proxy.setTarget(target);

      // 初始化清理器，用户回收资源
      if (target.isRecycleRubbish()) {
        recycleRubbish(appKey, proxy);
      }
    }
    // 重置最后一次访问时间
    proxy.resetLastUseTime();
    return proxy;
  }

  /**
   * 回收长期不使用的资源
   * 
   * @param key
   * @param proxy
   */
  protected void recycleRubbish(String key, WxConfigDBProxy proxy) {
    if (proxy.getFuture() != null && !proxy.getFuture().isDone()) {
      // 回收器正确运行或者等待运行
      return;
    }
    ScheduledFuture<?> f = Global.getScExecutor().schedule(() -> {
      WxConfigDBProxy pr = configs.get(key);
      if (pr != proxy) {
        // 内容已经被更换，不处理
      } else if (pr.isExpired()) {
        // 移除过期的内容
        configs.remove(key);
      } else {
        // 内容没有过期，重置请求器
        recycleRubbish(key, pr);
      }
    }, proxy.getTarget().getRefreshTime(), TimeUnit.SECONDS);
    proxy.setFuture(f);
  }

  public String getAppUniqueKey() {
    return uniqueKey;
  }

  /**
   * 清除缓存
   */
  @Override
  public void clearByAppKey(String appKey) {
    configs.remove(appKey);
    Global.getLogger().info("已经清除系统【" + getAppUniqueKey() + "】->[" + appKey + "]的配置信息");
  }

}
