package net.icgear.three.weixin.core.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * WxAccessToken 执行过程中访问凭证暂存 {"access_token":"ACCESS_TOKEN","expires_in":7200}
 * 
 * @author Y13
 *
 */
public class WxAccessToken extends WxErrCode {
  private static final long serialVersionUID = 8709719312922168909L;

  /**
   * 获取到的凭证
   */
  @JsonProperty("access_token")
  private String accessToken;

  /**
   * 凭证有效时间，单位：秒
   */
  @JsonProperty("expires_in")
  private long expiresIn = -1;

  /**
   * 对象构建时间
   * 
   * 时间不是很准确，但是几秒的误差是可以接受的
   * 
   * 单位：秒
   */
  @JsonIgnore
  private long createTime;

  public WxAccessToken() {
    // 初始化对象构建时间
    createTime = System.currentTimeMillis() / 1000;
  }

  public String getAccessToken() {
    return this.accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }

  public long getExpiresIn() {
    return this.expiresIn;
  }

  public void setExpiresIn(long expiresIn) {
    this.expiresIn = expiresIn;
  }

  /**
   * 给出对象创建时间，理论上该值不可以修改
   * 
   * @return
   */
  public long getCreateTime() {
    return createTime;
  }
}
