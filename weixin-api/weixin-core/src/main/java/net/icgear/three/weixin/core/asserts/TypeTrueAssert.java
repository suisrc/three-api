package net.icgear.three.weixin.core.asserts;

/**
 * 正断言
 * 
 * @author Y13
 *
 */
public class TypeTrueAssert implements TypeAssert {

  public boolean apply(String source, String target) {
    return true;
  }

}
