package net.icgear.three.weixin.wildfly.exception;

import javax.annotation.Priority;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.suisrc.core.exception.UUIDException;
import com.suisrc.core.message.ECM;

import net.icgear.three.weixin.core.WxConsts;

/**
 * 发生未知异常
 * 
 * @author Y13
 *
 */
@Provider
@Priority(9999)
public class OtherExceptionMapper implements ExceptionMapper<Exception> {

  /**
   * 输出错误
   */
  @Override
  public Response toResponse(Exception exception) {
    UUIDException ue = new UUIDException(exception);
    ue.printStackTrace();
    return Response.status(500).entity(ECM.fail(500101, ue.getMessage()))
        .type(MediaType.APPLICATION_JSON + WxConsts.MediaType_UTF_8).build();
  }

}
