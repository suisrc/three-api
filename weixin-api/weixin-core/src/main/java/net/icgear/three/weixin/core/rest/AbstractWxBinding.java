package net.icgear.three.weixin.core.rest;

import java.util.Collection;
import java.util.HashSet;
import java.util.function.Function;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.suisrc.core.exception.NoSupportException;
import com.suisrc.core.fasterxml.FasterFactory.Type;
import com.suisrc.core.reference.RefVal;
import com.suisrc.core.utils.CoreUtils;
import com.suisrc.three.core.AbstractThreeBinding;
import com.suisrc.three.core.MessageController;
import com.suisrc.three.core.exception.UnHandleException;
import com.suisrc.three.core.listener.ListenerManager;
import com.suisrc.three.core.listener.inf.Listener;
import com.suisrc.three.core.msg.IMessage;
import com.suisrc.three.core.msg.MsgNode;

import net.icgear.three.weixin.core.WxConfig;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.EncryptMessage;
import net.icgear.three.weixin.core.bean.WxEncryptSignature;
import net.icgear.three.weixin.core.crypto.WxCrypto;
import net.icgear.three.weixin.core.oauth2.WxAuthParams;
import net.icgear.three.weixin.core.oauth2.WxAuthResult;
import net.icgear.three.weixin.core.type.WxTypeParser;
import net.icgear.three.weixin.core.type.msg.DefaultMsgTypeParser;
import net.icgear.three.weixin.core.utils.WxUtils;

/**
 * 跟微信服务器捆绑
 * 
 * @author Y13
 *
 */
public abstract class AbstractWxBinding extends AbstractThreeBinding {

  protected static final boolean DEBUG = Boolean.getBoolean("net.icgear.three.weixin.core.AbstractWxBinding.debug");

  /**
   * 消息解析
   */
  private WxTypeParser msgParser;

  /**
   * 监听控制器
   */
  private MessageController msgController;

  /**
   * 微信服务器可信白名单
   * 
   * 默认null表示不使用白名单
   */
  private HashSet<String> whitelist = null;

  /**
   * 构造方法
   */
  @Override
  @PostConstruct
  protected void doPostConstruct() {
    Collection<Class<? extends IMessage>> msgTypes = getMsgTypes();
    if (msgTypes != null) {
      setMsgParser(new DefaultMsgTypeParser(msgTypes, getBindingCategory()));
    }
    ListenerManager controller = createMessageController();
    if (controller != null) {
      setMessageController(controller);
      String[] packages = getSystemProperty(getListenerPackagesKey());
      if (packages != null && packages.length > 0) {
        controller.scanPackages(packages);
      }
      String[] classes = getSystemProperty(getListenerClassesKey());
      if (classes != null && classes.length > 0) {
        controller.scanClasses(classes);
      }
      if (packages == null && classes == null) {
        Collection<Class<Listener<?>>> listenterClasses = getAlternativeListenerClasses();
        if (listenterClasses != null) {
          listenterClasses.forEach(controller::addListener);
        }
      }
      Collection<Class<Listener<?>>> listenterClasses = getAppendListenerClasses();
      if (listenterClasses != null) {
        listenterClasses.forEach(controller::addListener);
      }
    }
  }

  // --------------------------------------------------接口

  /**
   * 获取需要监听消息的类型
   * 
   * 如果返回值不为null, 会使用DefaultWxMsgTypeParser作为默认的消息解析器
   * 
   * 摸过希望使用系统默认配置，请务必实现该接口
   * 
   * @return
   */
  protected abstract Collection<Class<? extends IMessage>> getMsgTypes();

  /**
   * 获取消息监听器的名称
   * 
   * @return
   */
  protected abstract String getMessageControllerKey();


  /**
   * 获取解密和加密使用的令牌
   * 
   * @param sign
   * @param encrypt
   * @return
   */
  protected abstract WxConfig getWxConfig(WxEncryptSignature sign, EncryptMessage encrypt);

  /**
   * 获取应用唯一区分关键字
   */
  protected abstract String getAppUniqueKey();

  // -------------------------------------------------------
  /**
   * 创建默认的监听控制器
   */
  protected ListenerManager createMessageController() {
    return new ListenerManager(this, getMessageControllerKey());
  }

  /**
   * 获取绑定的类型
   * 
   * @return
   */
  protected String getBindingCategory() {
    return "";
  }

  /**
   * 获取监听的包的路径
   */
  protected String getListenerPackagesKey() {
    return WxConsts.WEIXIN_PRE + getAppUniqueKey() + ".binding.packages";
  }

  /**
   * 获取监听的类的路径
   */
  protected String getListenerClassesKey() {
    return WxConsts.WEIXIN_PRE + getAppUniqueKey() + ".binding.classes";
  }

  /**
   * 获取监听控制器
   */
  @Override
  protected MessageController getMessageController() {
    return msgController;
  }

  /**
   * 设定监听控制器
   * 
   * @param controller
   */
  protected void setMessageController(MessageController controller) {
    this.msgController = controller;
  }

  /**
   * 设定消息解析器
   * 
   * @param parser
   */
  protected void setMsgParser(WxTypeParser parser) {
    this.msgParser = parser;
  }

  /**
   * 解析消息类型
   */
  protected Class<? extends IMessage> getMsgClass(MsgNode node) {
    return msgParser.parser(node);
  }

  /**
   * 搜索监听类
   * 
   * 备选列表，当配置文件中没有指定时候加载
   * 
   * @return
   */
  protected Collection<Class<Listener<?>>> getAlternativeListenerClasses() {
    return null;
  }

  /**
   * 搜索监听类
   * 
   * 必然加载的监听类，只要使用默认加载监听器，该接口返回的监听都会被加载
   * 
   * @return
   */
  protected Collection<Class<Listener<?>>> getAppendListenerClasses() {
    return null;
  }

  /**
   * 通过系统环境变量指定监听的对象
   * 
   * @param key
   */
  protected String[] getSystemProperty(String key) {
    return CoreUtils.getSystemProperty2Array(key + ".*");
  }

  /**
   * 打印异常
   * 
   * @param e
   */
  protected void printThrowable(Throwable e) {
    if (DEBUG) {
      e.printStackTrace();
    } else {
      logger.warning("监听器发生异常：" + e.getMessage());
    }
  }

  /**
   * 断言访问者信息
   */
  protected void assertVisitorInfo() {
    WxUtils.setAppKey();
    if (whitelist == null) {
      return; // 白名单为空，不进行处理
    }
    String visitorIp = getRemoteHost();
    if (whitelist.contains(visitorIp)) {
      return; // 客户ip在白名单中，不进行处理
    }
    // 访问的客户端地址不在白名单中
    String msg =
        String.format("Illegal access, from:%s, Invalid IP[%s],Not in the weixin server IP whitelist", visitorIp, visitorIp);
    throw new RuntimeException(msg);
  }


  /**
   * 获取业务返回处理的内容，主要是如果把返回值处理为Response对象
   * 
   * 该接口如有必要可以重写，对返回值有要求的情况
   * 
   * @return
   */
  protected Function<String, Response> getResponseAdapter() {
    return null;
  }

  /**
   * 获取内容的最终处理结果（默认处理加密内容），可以重写该内容，处理其他加密信息
   * 
   * @return
   */
  protected Function<String, String> getResultAdapter(WxCrypto crypto, boolean isJson) {
    return crypto == null ? null : result -> getEncryptResultAdapter(crypto, isJson, result);
  }

  /**
   * 处理加密内容
   * 
   * @return
   */
  protected String getEncryptResultAdapter(WxCrypto crypto, boolean isJson, String content) {
    // 消息内容需要加密返回
    String encryText = crypto.encrypt(content);
    // 构建返回对象
    EncryptMessage encryptMsg = new EncryptMessage();
    encryptMsg.setEncrypt(encryText);
    encryptMsg.setNonce(WxCrypto.genRandomStr());
    encryptMsg.setTimeStamp(System.currentTimeMillis());
    // 生成签名
    String signature = WxCrypto.genSHA1(crypto.getToken(), encryptMsg.getTimeStamp(), encryptMsg.getNonce(), encryText);
    encryptMsg.setMsgSignature(signature);
    // 生成字符串
    return bean2Str(encryptMsg, isJson);
  }

  /**
   * 微信回调URL绑定
   * 
   */
  public String doGet(WxEncryptSignature sign) {
    assertVisitorInfo();
    WxConfig config = getWxConfig(sign, null);
    if (!sign.isValid() || config == null || config.getWxToken() == null) {
      return "Illegal request";
    }
    // 进行验证
    String result = getDoGetResult(sign, config);
    return result;
  }

  /**
   * 微信回调URL绑定
   * 
   */
  public String doGet2(WxEncryptSignature sign) {
    assertVisitorInfo();
    WxConfig config = getWxConfig(sign, null);
    if (!sign.isSuperValid() || config == null || config.getWxToken() == null) {
      return "Illegal request";
    }
    // 进行验证
    String result = getDoGetResult2(sign, config);
    return result;
  }

  /**
   * 获取执行的结果
   * 
   * @param sign
   * @param config
   * @return
   */
  protected String getDoGetResult(WxEncryptSignature sign, WxConfig config) {
    String result;
    String signature = WxCrypto.genSHA1(config.getWxToken(), sign.getTimestamp(), sign.getNonce(), sign.getEchostr());
    if (signature.equals(sign.getMsgSignature())) {
      WxCrypto crypto = getNewWxCrypto(config, sign, null);
      result = crypto.decrypt(sign.getEchostr());
      // 输出绑定操作的logger
      String id = config.getWxSuiteId() != null ? config.getWxSuiteId() : config.getWxAppId();
      logger.info(String.format("完成账户[%s]的回调接口绑定操作", id));
    } else {
      result = "";
    }
    return result;
  }

  /**
   * 微信回调URL绑定
   * 
   */
  protected String getDoGetResult2(WxEncryptSignature sign, WxConfig config) {
    String result;
    String signature = WxCrypto.genSHA1(config.getWxToken(), sign.getTimestamp(), sign.getNonce());
    if (signature.equals(sign.getSignature())) {
      result = sign.getEchostr();
      // 输出绑定操作的logger
      String id = config.getWxSuiteId() != null ? config.getWxSuiteId() : config.getWxAppId();
      logger.info(String.format("完成账户[%s]的回调接口绑定操作", id));
    } else {
      result = "";
    }
    return result;
  }

  /**
   * 微信回调请求绑定
   * 
   */
  public Response doPost(WxEncryptSignature sign, String data) {
    try {
      assertVisitorInfo();
      if (data == null || data.isEmpty()) {
        // 没有消息内容，直接快速结束处理（可能是接口别其他网站异常调用）
        return Response.ok().entity("There is no valid request data").type(MediaType.TEXT_PLAIN).build();
      }
      // 确定数据传输格式
      boolean isJson = data.startsWith("<xml>") ? false : true;
      // --------------------------------消息签名验证------------------------------------//
      // 处理消息内容
      WxCrypto wxCrypt = null;
      String content;
      if (sign.getMsgSignature() != null) {
        // 使用AES解密
        RefVal<WxCrypto> cryptValue = new RefVal<>();
        Object obj = decryptMessage(cryptValue, sign, data, isJson);
        if (obj instanceof Response) {
          // 处理发生某种中断行为，直接返回处理结果
          return (Response) obj;
        } else {
          // 获取解密的内容
          content = obj.toString();
        }
        // 用于数据加密使用，如果业务不需要加密，可以直接返回null
        wxCrypt = cryptValue.get();
      } else {
        // raw 明文传输
        content = data;
      }
      // 如果解密器存在，可以通过解密器，获取消息的发生者信息，
      // 注意，这里不使用配置的appIdOrCropId,是因为该信息在任何位置都可以轻易获取。
      // 但是，如果该接口对多个应用号提供服务，那么就需要判断解密和加密使用使用的appId才是我们需要的内容
      String appidOrCorpid = wxCrypt != null ? wxCrypt.getAppidOrCorpid() : null;
      // 业务处理
      return doInternalWork(content, isJson, appidOrCorpid, getResultAdapter(wxCrypt, isJson), getResponseAdapter());
    } catch (UnHandleException e) {
      // 服务警告，无法处理内容
      e.printStackTrace();
      return Response.ok().entity(e.getMessage()).type(MediaType.TEXT_PLAIN).build();
    } catch (Throwable e) {
      printThrowable(e);
      // 虽然失败了，但是还是需要通知服务器已经接受到请求
      return Response.ok().entity(WxConsts.SUCCESS).type(MediaType.TEXT_PLAIN).build();
    }
  }

  /**
   * 使用AES解密消息内容
   * 
   * @return 如果结果是Response类型，直接作为请求的结果返回给微信服务器， 否则使用toString方法转换为字符串作为请求的post内容 注意，不可返回null,
   *         如果希望中断处理，请抛出异常
   */
  protected Object decryptMessage(RefVal<WxCrypto> crypt, WxEncryptSignature sign, String data, boolean isJson) {
    // 解析网络数据
    EncryptMessage encryptMsg = getFasterFactory().string2Bean(data, EncryptMessage.class, isJson ? Type.JSON : Type.XML);
    // 微信配置信息
    WxConfig config = getWxConfig(sign, encryptMsg);
    // 服务器验证
    if (config.isWxEncrypt() && (config.getWxToken() == null || !sign.isValid())) {
      // 没有签名信息,或者签名不可用，直接放回请求不可用
      return Response.ok().entity("Illegal request").type(MediaType.TEXT_PLAIN).build();
    }
    // 获取加密和解密工具
    WxCrypto wxCrypt = getNewWxCrypto(config, sign, encryptMsg);
    crypt.set(wxCrypt);
    // 验证数据签名
    String signature = WxCrypto.genSHA1(wxCrypt.getToken(), sign.getTimestamp(), sign.getNonce(), encryptMsg.getEncrypt());
    if (!signature.equals(sign.getMsgSignature())) {
      // 消息的签名有问题，无法进行处理
      return Response.ok().entity("Data signature exception").type(MediaType.TEXT_PLAIN).build();
    }
    // 对消息记性解密
    String content = decryptMessage(wxCrypt, encryptMsg.getEncrypt());
    // 返回解密的结果
    return content;
  }

  /**
   * 使用解密器对内容记性解密
   * 
   * @param wxCrypt
   * @param encrypt
   * @return
   */
  protected String decryptMessage(WxCrypto wxCrypt, String encrypt) {
    return wxCrypt.decrypt(encrypt);
  }

  /**
   * 使用解密器对内容记性解密
   * 
   * 算法备份，对解密工具的应用ID进行更改
   */
  protected String decryptMessage2(WxCrypto wxCrypt, String encrypt) {
    // 执行后把新应用ID给加密器
    return wxCrypt.decrypt(encrypt, wxCrypt::setAppidOrCorpid);
  }

  /**
   * 获取一个新的加密解密工具对象
   * 
   * @param encryptMsg
   * @param sign
   * @return
   */
  protected WxCrypto getNewWxCrypto(WxConfig config, WxEncryptSignature sign, EncryptMessage encryptMsg) {
    return new WxCrypto(config.getWxToken(), config.getWxEncodingKey(), config.getWxAppId());
  }

  /**
   * 获取访问者的IP
   * 
   * @return
   */
  protected String getRemoteHost() {
    return null;
  }


  /**
   * 获取白名单操作密钥
   * 
   * @return
   */
  protected String getWhitelistSecret() {
    return null;
  }

  /**
   * 获取微信服务器白名单列表
   * 
   * @return
   */
  protected String[] getWxServerIpList() {
    return null;
  }

  /**
   * 刷新/启用微信服务器IP白名单
   */
  public String updateWhiteList(String secret) {
    if (getWhitelistSecret() == null) {
      return "The access key was not configured, no operation";
    } else if (!getWhitelistSecret().equals(secret)) {
      return "Key authentication failed, please replace the operation key";
    }
    // -------------------------------------------
    try {
      String[] ipList = getWxServerIpList();
      if (ipList == null || ipList.length == 0) {
        return "Weixin service interface's whitelist failed to start：whitelist is empty.";
      }
      whitelist = new HashSet<>();
      StringBuilder sbir = new StringBuilder("Weixin service interface's whitelist has been enabled, IP list:\n");
      for (String ip : ipList) {
        int offset = ip.indexOf('/');
        String ipf = offset > 0 ? ip.substring(0, offset) : ip;
        sbir.append(ipf).append('\n');
        whitelist.add(ipf);
      }
      return sbir.toString();
    } catch (Exception e) {
      e.printStackTrace();
      return "Weixin service interface's whitelist failed to start：" + e.getMessage();
    }
  }

  /**
   * 删除/禁用微信服务器IP白名单
   */
  public String deleteWhiteList(String secret) {
    if (getWhitelistSecret() == null) {
      return "The access key was not configured, no operation";
    } else if (!getWhitelistSecret().equals(secret)) {
      return "Key authentication failed, please replace the operation key";
    }
    // -------------------------------------------
    if (whitelist == null) {
      return "Weixin service interface's whitelist does not start";
    }
    HashSet<String> list = whitelist;
    whitelist = null;
    StringBuilder sbir = new StringBuilder("Weixin service interface's whitelist has been disabled, IP list:\n");
    list.forEach(ip -> sbir.append(ip).append('\n'));
    return sbir.toString();
  }

  /**
   * 清除
   * 
   * @return
   */
  public String clear(String secret, boolean remove) {
    return "No Support: Clear Rest Api.";
  }

  /**
   * 基本认证
   * 
   * @param key
   * @return
   */
  public WxAuthResult doOAuth2(WxAuthParams params) {
    throw new NoSupportException("No Support: OAuth2 Rest Api.");
  }

  /**
   * 身份认证
   * 
   * @param key
   * @return
   */
  public String getOAuth2State(String secret, String token) {
    throw new NoSupportException("No Support: OAuth2 State Rest Api.");
  }
}
