package net.icgear.three.weixin.core.event.qy.addr;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.type.msg.BaseChangeContactEvent;

/**
 * 更新部门事件
 * 
 * @author Y13
 *
 */
@WxChangeType("update_party")
@JacksonXmlRootElement(localName="xml")
public class UpdatePartyEvent extends BaseChangeContactEvent {

    /**
     * 部门Id
     */
    @JacksonXmlProperty(localName = "Id")
    @JsonProperty("Id")
    private String id;

    /**
     * 部门名称
     */
    @JacksonXmlProperty(localName = "Name")
    @JsonProperty("Name")
    private String name;

    /**
     * 父部门id
     */
    @JacksonXmlProperty(localName = "ParentId")
    @JsonProperty("ParentId")
    private String parentId;

    /**
     * 获取部门Id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * 设定部门Id
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取部门名称
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * 设定部门名称
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取父部门id
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * 设定父部门id
     * @param parentId the parentId to set
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

}
