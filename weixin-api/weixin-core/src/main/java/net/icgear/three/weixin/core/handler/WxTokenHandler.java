package net.icgear.three.weixin.core.handler;

import net.icgear.three.weixin.core.bean.WxAccessToken;

/**
 * 获取获取访问令牌接口
 * 
 * @author Y13
 *
 */
public interface WxTokenHandler {

  /**
   * 获取访问令牌
   * 
   * @return
   */
  WxAccessToken getToken();
}
