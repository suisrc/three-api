package net.icgear.three.weixin.core.event;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxEvent;
import net.icgear.three.weixin.core.type.msg.WxEventMessage;

/**
 * 订阅事件
 * <Event><![CDATA[subscribe]]></Event>
 * @author Y13
 *
 */
@WxEvent("subscribe")
@JacksonXmlRootElement(localName="xml")
public class SubscribeEvent extends WxEventMessage {
}
