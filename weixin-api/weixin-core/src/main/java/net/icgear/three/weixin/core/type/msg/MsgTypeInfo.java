package net.icgear.three.weixin.core.type.msg;

import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.three.core.msg.IMessage;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.annotation.WxEvent;
import net.icgear.three.weixin.core.annotation.WxMsgType;
import net.icgear.three.weixin.core.asserts.TypeAssert;
import net.icgear.three.weixin.core.type.AbstractTypeInfo;

/**
 * 消息类型描述
 * 
 * @author Y13
 *
 */
public class MsgTypeInfo extends AbstractTypeInfo<Class<? extends IMessage>> {

    /**
     * 构建消息类型
     * @param clazz
     * @return
     */
    public static MsgTypeInfo create(Class<? extends IMessage> clazz) {
        try {
            return new MsgTypeInfo(clazz);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }
    
    /**
     * 优先级
     */
    private String priority;
    
    /**
     * 类别
     */
    private String category;
    
    /**
     * 构造方法
     */
    public MsgTypeInfo(Class<? extends IMessage> target) {
        super(target);
    }

    /**
     * 初始化
     * 
     * 其中MsgType注解是必须有的内容
     * 
     */
    @Override
    protected void initialize() {
        // 消息注解
        WxMsgType msgAnno = this.target.getAnnotation(WxMsgType.class);
        if (msgAnno == null) {
            throw new RuntimeException("无法初始化消息类型，没有@WxMsgType注解标记：" + this.target);
        }
        // 对比的内容
        String value = msgAnno.value();
        // 对比的方式
        TypeAssert hander = ReflectionUtils.newInstance(msgAnno.handler());
        // 对比顺序
        this.priority = msgAnno.priority();
        this.asserts.add(new RefAssert(value, hander));
        this.category = msgAnno.category();
        
        // 事件注解
        WxEvent eventAnno = this.target.getAnnotation(WxEvent.class);
        if (eventAnno == null) {
            return;
        }
        value = eventAnno.value();
        hander = ReflectionUtils.newInstance(eventAnno.handler());
        this.priority = eventAnno.priority();
        this.asserts.add(new RefAssert(value, hander));
        if (eventAnno.category().isEmpty()) {
            this.category = eventAnno.category();
        }
        
        // 变更事件注解
        WxChangeType changeAnno = this.target.getAnnotation(WxChangeType.class);
        if (changeAnno == null) {
            return;
        }
        value = changeAnno.value();
        hander = ReflectionUtils.newInstance(changeAnno.handler());
        this.priority = changeAnno.priority();
        this.asserts.add(new RefAssert(value, hander));
        if (changeAnno.category().isEmpty()) {
            this.category = changeAnno.category();
        }
    }
    
    /**
     * 优先级
     */
    @Override
    public String getPriority() {
        return priority;
    }

    /**
     * 获取类别
     * @return
     */
    public String getCategory() {
        return category;
    }
}
