package net.icgear.three.weixin.core;

import java.util.concurrent.ConcurrentHashMap;

import com.suisrc.core.Global;
import com.suisrc.jaxrsapi.core.AbstractTokenActivator.TokenAtom;

/**
 * 微信配置接口
 * 
 * @author Y13
 *
 */
public abstract class WxConfigCache implements WxConfig {

  /**
   * 缓存
   */
  private ConcurrentHashMap<String, TokenAtom> tokenCache;

  /**
   * 构造方法
   */
  public WxConfigCache() {
    this.tokenCache = new ConcurrentHashMap<>(2);
  }

  /**
   * 获取令牌元
   */
  @Override
  public TokenAtom findTokenAtom(String tokenKey) {
    return tokenCache.get(tokenKey);
  }

  /**
   * 保存令牌元
   */
  @Override
  public TokenAtom saveTokenAtom(String tokenKey, TokenAtom tokenAtom) {
    tokenCache.put(tokenKey, tokenAtom);
    return tokenAtom;
  }

  /**
   * 
   */
  @Override
  public void clearConfig(boolean remove) {
    // 清除缓存内容
    tokenCache.clear();
    Global.getLogger().info("已经清除系统【" + getAppUniqueKey() + "】的令牌信息");
  }
}
