package net.icgear.three.weixin.core.handler;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

import net.icgear.three.weixin.core.AbstractWeixinActivator;

/**
 * 微信配置获取助手
 * 
 * 只有在使用多配置信息截获器时候，该内容才有效
 * 
 * @author Y13
 *
 */
@Named("weixin-config-default")
@Dependent
public class WxConfigSystemHandlerDef extends WxConfigSystemHandler implements CallbackActivatorHandler {

  private String uniqueKey;

  @Override
  public void setActivator(AbstractWeixinActivator activator) {
    this.uniqueKey = activator.getAppUniqueKey();
  }

  @Override
  protected String getAppUniqueKey() {
    return uniqueKey;
  }

}
