package net.icgear.three.weixin.core.event.qy.addr;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.type.msg.BaseUserChangeContactEvent;

/**
 * 更新成员事件
 * 
 * @author Y13
 *
 */
@WxChangeType("update_user")
@JacksonXmlRootElement(localName="xml")
public class UpdateUserEvent extends BaseUserChangeContactEvent {

    /**
     * 新的UserID，变更时推送（userid由系统生成时可更改一次）
     */
    @JacksonXmlProperty(localName = "NewUserID")
    @JsonProperty("NewUserID")
    private String newUserID;

    /**
     * 获取新的UserID，变更时推送（userid由系统生成时可更改一次）
     * @return the newUserID
     */
    public String getNewUserID() {
        return newUserID;
    }

    /**
     * 设定新的UserID，变更时推送（userid由系统生成时可更改一次）
     * @param newUserID the newUserID to set
     */
    public void setNewUserID(String newUserID) {
        this.newUserID = newUserID;
    }
}
