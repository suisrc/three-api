package net.icgear.three.weixin.core.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxMsgType;
import net.icgear.three.weixin.core.type.msg.WxMessage;

/**
 * 小视频消息
 * <MsgType><![CDATA[shortvideo]]></MsgType>
 * 
 * <MediaId><![CDATA[media_id]]></MediaId>
 * <ThumbMediaId><![CDATA[thumb_media_id]]></ThumbMediaId>
 * @author Y13
 *
 */
@WxMsgType("shortvideo")
@JacksonXmlRootElement(localName="xml")
public class ShortvideoMessage extends WxMessage {

    /**
     * 视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "MediaId")
    @JsonProperty("MediaId")
    private String mediaId;

    /**
     * 视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "ThumbMediaId")
    @JsonProperty("ThumbMediaId")
    private String thumbMediaId;

    /**
     * 获取视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
     * @return the mediaId
     */
    public String getMediaId() {
        return mediaId;
    }

    /**
     * 设定视频消息媒体id，可以调用多媒体文件下载接口拉取数据。
     * @param mediaId the mediaId to set
     */
    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    /**
     * 获取视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
     * @return the thumbMediaId
     */
    public String getThumbMediaId() {
        return thumbMediaId;
    }

    /**
     * 设定视频消息缩略图的媒体id，可以调用多媒体文件下载接口拉取数据。
     * @param thumbMediaId the thumbMediaId to set
     */
    public void setThumbMediaId(String thumbMediaId) {
        this.thumbMediaId = thumbMediaId;
    }

}
