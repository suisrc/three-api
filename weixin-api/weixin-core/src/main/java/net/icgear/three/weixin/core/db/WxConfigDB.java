package net.icgear.three.weixin.core.db;

import com.suisrc.jaxrsapi.core.AbstractTokenActivator.TokenAtom;

import net.icgear.three.weixin.core.WxConfig;

/**
 * 微信配置接口
 * 
 * @author Y13
 *
 */
public interface WxConfigDB extends WxConfig {

  /**
   * 配置刷新时间 请不要将刷新时间配置小于0，会导致系统异常
   * 
   * 只有isRecycleRubbish为true时候才有效
   * 
   * @return
   */
  default int getRefreshTime() {
    return 7200; // 2个小时
  }

  /**
   * 是否开启垃圾回收控制器
   * 
   * 清除长期不使用的内容
   * 
   * 默认清除管理器关闭
   */
  default boolean isRecycleRubbish() {
    return false;
  }

  // --------------------------------------以下方法不参与计算
  /**
   * 获取访问令牌
   */
  @Deprecated
  default String getWxAccessToken() {
    return null;
  }

  /**
   * 清除微信访问令牌
   */
  @Deprecated
  default void clearWxAccessToken() {
    // do nothing
  }

  /**
   * 推送配置信息
   * 
   * @param key
   * @param value
   */
  @Deprecated
  default <T> void setAdapter(String named, Class<T> type, T value) {
    // do nothing
  }

  /**
   * 获取配置属性
   */
  @Deprecated
  default <T> T getAdapter(String named, Class<T> type) {
    return null;
  }

  /**
   * 查询令牌元
   * 
   * @param tokenKey
   * @return
   */
  default TokenAtom findTokenAtom(String tokenKey) {
    // do nothing
    return null;
  }

  /**
   * 保存令牌元
   * 
   * 通常情况使用ConcurrentHashMap作为缓存即可
   * 
   * @param tokenKey
   * @param tokenAtom
   * @return
   */
  default TokenAtom saveTokenAtom(String tokenKey, TokenAtom tokenAtom) {
    // do nothing
    return null;
  }
}
