package net.icgear.three.weixin.core.suite;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxInfoType;
import net.icgear.three.weixin.core.type.suite.BaseInfoSuite;

/**
 * 变更授权通知
 * 
 * 当授权方（即授权企业）在企业微信管理端的授权管理中，修改了对应用的授权后，企业微信服务器推送变更授权通知。
 * 服务商接收到变更通知之后，需自行调用获取企业授权信息进行授权内容变更比对。
 * 
 * <InfoType><![CDATA[change_auth]]></InfoType>
 * 
 *  <AuthCorpId><![CDATA[wxf8b4f85f3a794e77]]></AuthCorpId>
 * @author Y13
 *
 */
@WxInfoType("change_auth")
@JacksonXmlRootElement(localName="xml")
public class ChangeAuthSuite extends BaseInfoSuite {

    /**
     * 授权方的corpid
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "AuthCorpId")
    @JsonProperty("AuthCorpId")
    private String authCorpId;

    /**
     * 获取授权方的corpid
     * @return the authCorpId
     */
    public String getAuthCorpId() {
        return authCorpId;
    }

    /**
     * 设定授权方的corpid
     * @param authCorpId the authCorpId to set
     */
    public void setAuthCorpId(String authCorpId) {
        this.authCorpId = authCorpId;
    }

}
