package net.icgear.three.weixin.core.filter;

import java.util.Arrays;
import java.util.List;

import com.suisrc.jaxrsapi.core.ApiActivator;
import com.suisrc.jaxrsapi.core.retry.AutoClearAccessToken;

import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 对微信中的Access Token 有效性记性验证 系统很容易形成恶性竞争，所以请谨慎使用。
 * 
 * @author Y13
 *
 */
public class WxTokenReTryFilter extends AutoClearAccessToken {

    /**
     * AccessToken 异常的code
     */
    private static final List<Long> errList = Arrays.asList(40001L, 40014L, 42001L);

    public WxTokenReTryFilter(ApiActivator activator) {
        super(activator);
    }

    @Override
    protected boolean isTokenExpired(Object result, Exception e) {
        if (result instanceof WxErrCode) {
            WxErrCode wec = (WxErrCode) result;
            if (wec.getErrcode() != null && errList.contains(wec.getErrcode())) {
                return errList.contains(wec.getErrcode()); // token 已经失效，通知系统清理
            }
        }
        return false;
    }

}
