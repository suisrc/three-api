package net.icgear.three.weixin.core.type.msg;

import java.util.Collection;

import com.suisrc.three.core.msg.IMessage;
import com.suisrc.three.core.msg.MsgNode;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.annotation.WxEvent;
import net.icgear.three.weixin.core.annotation.WxMsgType;
import net.icgear.three.weixin.core.type.WxTypeParser;

/**
 * 默认的微信消息解析器
 * 
 * @author Y13
 *
 */
public class DefaultMsgTypeParser implements WxTypeParser {

    /**
     * 消息类型索引
     */
    private MsgTypeIndexs indexs;
    
    public DefaultMsgTypeParser(Collection<Class<? extends IMessage>> classes, String category) {
        indexs = new MsgTypeIndexs(classes, category);
    }
    
    /**
     * 执行微信内容解析内容
     */
    @Override
    public Class<? extends IMessage> parser(MsgNode node) {
        String[] keys = {
                node.getV(WxMsgType.XML_ATTR),
                node.getV(WxEvent.XML_ATTR),
                node.getV(WxChangeType.XML_ATTR)
        };
        return indexs.searchFirstV(keys);
    }

}
