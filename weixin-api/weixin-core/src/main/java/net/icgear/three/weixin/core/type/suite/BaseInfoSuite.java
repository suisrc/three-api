package net.icgear.three.weixin.core.type.suite;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.suisrc.three.core.msg.IMessage;
import com.suisrc.three.core.msg.UnknowMessage;

/**
 * <SuiteId><![CDATA[ww4asffe99e54c0f4c]]></SuiteId>
 * <InfoType> <![CDATA[suite_ticket]]></InfoType>
 * <TimeStamp>1403610513</TimeStamp>
 * @author Y13
 *
 */
public abstract class BaseInfoSuite extends UnknowMessage implements IMessage {

    /**
     * 第三方应用的SuiteId
     */
    @JacksonXmlProperty(localName = "SuiteId")
    @JsonProperty("SuiteId")
    private String suiteId;

    /**
     * 时间戳
     */
    @JacksonXmlProperty(localName = "TimeStamp")
    @JsonProperty("TimeStamp")
    private Long timeStamp;
    
    /**
     * 信息类型
     */
    @JacksonXmlProperty(localName = "InfoType")
    @JsonProperty("InfoType")
    private String infoType;

    /**
     * 获取第三方应用的SuiteId
     * @return the suiteId
     */
    public String getSuiteId() {
        return suiteId;
    }

    /**
     * 设定第三方应用的SuiteId
     * @param suiteId the suiteId to set
     */
    public void setSuiteId(String suiteId) {
        this.suiteId = suiteId;
    }

    /**
     * 获取时间戳
     * @return the timeStamp
     */
    public Long getTimeStamp() {
        return timeStamp;
    }

    /**
     * 设定时间戳
     * @param timeStamp the timeStamp to set
     */
    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * 获取信息类型
     * @return the infoType
     */
    public String getInfoType() {
        return infoType;
    }

    /**
     * 设定信息类型
     * @param infoType the infoType to set
     */
    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }
    
}
