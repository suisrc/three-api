package net.icgear.three.weixin.core.asserts;

/**
 * 断言接口
 * 
 * @author Y13
 *
 */
@FunctionalInterface
public interface TypeAssert {

  /**
   * 执行断言
   * 
   * @param source 对比模版信息
   * @param value 对比内容
   * @return
   */
  boolean apply(String source, String target);

}
