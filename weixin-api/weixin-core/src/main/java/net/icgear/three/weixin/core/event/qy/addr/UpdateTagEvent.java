package net.icgear.three.weixin.core.event.qy.addr;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.type.msg.BaseChangeContactEvent;

/**
 * 标签成员变更事件
 * 
 * 回调设置在授权应用可见范围内的标签的变更事件。由第三方应用调用接口触发的标签变更事件不回调给该应用本身。
 * 
 * @author Y13
 *
 */
@WxChangeType("update_tag")
@JacksonXmlRootElement(localName="xml")
public class UpdateTagEvent extends BaseChangeContactEvent {

    /**
     * 标签Id
     */
    @JacksonXmlProperty(localName = "TagId")
    @JsonProperty("TagId")
    private String tagId;

    /**
     * 标签中新增的成员userid列表，用逗号分隔
     */
    @JacksonXmlProperty(localName = "AddUserItems")
    @JsonProperty("AddUserItems")
    private String addUserItems;

    /**
     * 标签中删除的成员userid列表，用逗号分隔
     */
    @JacksonXmlProperty(localName = "DelUserItems")
    @JsonProperty("DelUserItems")
    private String delUserItems;

    /**
     * 标签中新增的部门id列表，用逗号分隔
     */
    @JacksonXmlProperty(localName = "AddPartyItems")
    @JsonProperty("AddPartyItems")
    private String addPartyItems;

    /**
     * 标签中删除的部门id列表，用逗号分隔
     */
    @JacksonXmlProperty(localName = "DelPartyItems")
    @JsonProperty("DelPartyItems")
    private String delPartyItems;

    /**
     * 获取标签Id
     * @return the tagId
     */
    public String getTagId() {
        return tagId;
    }

    /**
     * 设定标签Id
     * @param tagId the tagId to set
     */
    public void setTagId(String tagId) {
        this.tagId = tagId;
    }

    /**
     * 获取标签中新增的成员userid列表，用逗号分隔
     * @return the addUserItems
     */
    public String getAddUserItems() {
        return addUserItems;
    }

    /**
     * 设定标签中新增的成员userid列表，用逗号分隔
     * @param addUserItems the addUserItems to set
     */
    public void setAddUserItems(String addUserItems) {
        this.addUserItems = addUserItems;
    }

    /**
     * 获取标签中删除的成员userid列表，用逗号分隔
     * @return the delUserItems
     */
    public String getDelUserItems() {
        return delUserItems;
    }

    /**
     * 设定标签中删除的成员userid列表，用逗号分隔
     * @param delUserItems the delUserItems to set
     */
    public void setDelUserItems(String delUserItems) {
        this.delUserItems = delUserItems;
    }

    /**
     * 获取标签中新增的部门id列表，用逗号分隔
     * @return the addPartyItems
     */
    public String getAddPartyItems() {
        return addPartyItems;
    }

    /**
     * 设定标签中新增的部门id列表，用逗号分隔
     * @param addPartyItems the addPartyItems to set
     */
    public void setAddPartyItems(String addPartyItems) {
        this.addPartyItems = addPartyItems;
    }

    /**
     * 获取标签中删除的部门id列表，用逗号分隔
     * @return the delPartyItems
     */
    public String getDelPartyItems() {
        return delPartyItems;
    }

    /**
     * 设定标签中删除的部门id列表，用逗号分隔
     * @param delPartyItems the delPartyItems to set
     */
    public void setDelPartyItems(String delPartyItems) {
        this.delPartyItems = delPartyItems;
    }

}
