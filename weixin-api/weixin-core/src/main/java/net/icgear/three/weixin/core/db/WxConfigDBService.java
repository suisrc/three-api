package net.icgear.three.weixin.core.db;

/**
 * 微信配置服务接口
 * 
 * @author Y13
 *
 */
public interface WxConfigDBService {

  /**
   * 获取微信的配置信息
   * 
   * @param uniqueKey
   * @param appKey
   * @return
   */
  WxConfigDB findByUniqueAndAppKey(String uniqueKey, String appKey);

}
