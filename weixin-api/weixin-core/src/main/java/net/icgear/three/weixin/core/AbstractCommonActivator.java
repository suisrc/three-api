package net.icgear.three.weixin.core;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;

import com.suisrc.core.Global;
import com.suisrc.jaxrsapi.core.AbstractTokenActivator;
import com.suisrc.jaxrsapi.core.token.Token;
import com.suisrc.jaxrsapi.core.token.TokenReference;

import net.icgear.three.weixin.core.bean.WxAccessToken;
import net.icgear.three.weixin.core.filter.WxClientResponseFilter;
import net.icgear.three.weixin.core.handler.WxTokenHandler;
import net.icgear.three.weixin.core.handler.WxTopologyHandler;
import net.icgear.three.weixin.core.utils.WxUtils;

/**
 * 
 * 通用微信激活器层内容
 * 
 * @author Y13
 */
public abstract class AbstractCommonActivator extends AbstractTokenActivator {

  /**
   * 应用名称
   */
  private String appName;

  /**
   * 集群控制
   */
  private WxTopologyHandler topologyHandler;

  // --------------------------------------------需要实现的接口

  /**
   * 获取令牌生产者
   * 
   * 如果该方法无法满足要求，请重写getWxTokenHandler方法
   */
  protected abstract WxTokenHandler getWxTokenHandler(String tokenKey);

  /**
   * 唯一key的内容
   */
  public abstract String getAppUniqueKey();

  // -------------------------------------------可以从写的接口

  /**
   * 获取应用名称的关键字
   */
  protected String getAppNameKey() {
    return WxConsts.WEIXIN_PRE + getAppUniqueKey() + ".endpoint.name";
  }

  /**
   * 获取基本访问地址关键字
   */
  @Override
  protected String getBaseUrlKey() {
    return WxConsts.WEIXIN_PRE + getAppUniqueKey() + ".endpoint.base-url";
  }

  // --------------------------------------------重写内容

  /**
   * 构造方法
   */
  @Override
  @PostConstruct
  public void doPostConstruct() {
    // 应用的配置信息
    appName = System.getProperty(getAppNameKey());
    // 继续实现构造
    super.doPostConstruct();

    try {
      // 注入集群内容
      topologyHandler = createTopologyHandler();
    } catch (Exception e) {
      Global.getLogger().info(e.getMessage());
    }
  }

  /**
   * 获取当前执行的token关键字
   */
  @Override
  protected String getTokenKeyByCache() {
    return "";
  }

  /**
   * 请求带有的应用关键字
   * 
   * @return
   */
  public String getRequestAppKey() {
    return WxUtils.getAppKey();
  }

  /**
   * 
   */
  @Override
  protected Token getTokenByRemote(String tokenKey) {
    WxAccessToken result = getWxTokenHandler(tokenKey).getToken();
    if (result.getAccessToken() != null) {
      Token token = new Token();
      token.setAccessToken(result.getAccessToken());
      token.setExpiresIn(result.getExpiresIn());
      return token;
    } else {
      String info = String.format("获取认证发生异常: [%s]%s", result.getErrcode(), result.getErrmsg());
      throw new RuntimeException(info);
    }
  }

  @Override
  protected void registerTargetClient(String key, Client client) {
    super.registerTargetClient(key, client);
    client.register(new WxClientResponseFilter());
  }
  // --------------------------------------------ApiActivator

  /**
   * 应用名称
   */
  @Override
  public String getAppName() {
    return appName;
  }

  // ---------------------------------------------------------------------集群同步

  /**
   * 获取集群同步控制器
   * 
   * @return
   */
  protected WxTopologyHandler createTopologyHandler() {
    return null;
  }


  /**
   * 更新拓扑中的token
   * 
   * @param key
   * @param accessToken
   * @return
   */
  protected TokenReference putTokenByTopology(String tokenKey, TokenReference token) {
    return topologyHandler == null ? token : topologyHandler.putTokenByTopology(tokenKey, token);
  }

  /**
   * 获取拓扑中的token
   * 
   * @param key
   * @return
   */
  protected TokenReference getTokenByTopology(String tokenKey) {
    return topologyHandler == null ? null : topologyHandler.getTokenByTopology(tokenKey);
  }

  /**
   * 获取拓扑中的token
   * 
   * @param key
   * @return
   */
  protected List<TokenReference> getTokensByTopology() {
    return topologyHandler == null ? null : topologyHandler.getTokensByTopology();
  }

  /**
   * 检测拓扑网络中是否有人正在修改token
   * 
   * @return
   */
  protected boolean hasUpdateTokenByTopology(String tokenKey) {
    return topologyHandler == null ? false : topologyHandler.hasUpdateTokenByTopology(tokenKey);
  }

  /**
   * 标记拓扑网络，当前环境正在修改token
   * 
   * 当执行set操作后，应该立即解锁拓扑网络中的标记
   * 
   * @return ture 标记成功， false 标记失败，当前有其他环境正在修改token
   */
  protected boolean lockUpdateTokenByTopology(String tokenKey) {
    return topologyHandler == null ? true : topologyHandler.lockUpdateTokenByTopology(tokenKey);
  }

  /**
   * 解锁拓扑网络中对token更新的锁定
   * 
   * @return
   */
  protected void unlockUpdateTokenByTopology(String tokenKey) {
    if (topologyHandler != null) {
      topologyHandler.unlockUpdateTokenByTopology(tokenKey);
    }
  }

  /**
   * 远程服务代理KEY
   * 
   * @return
   */
  protected String getProxyHostKey() {
    return WxConsts.WEIXIN_PRE + getAppUniqueKey() + ".proxy.host";
  }

  /**
   * 远程服务器代理KEY
   * 
   * @return
   */
  protected String getProxyPortKey() {
    return WxConsts.WEIXIN_PRE + getAppUniqueKey() + ".proxy.port";
  }

  /**
   * 获取代理地址
   * 
   * @return
   */
  public String createProxyHost() {
    return System.getProperty(getProxyHostKey());
  }

  /**
   * 获取代理端口
   * 
   * @return
   */
  public int createProxyPort() {
    return Integer.getInteger(getProxyPortKey(), -1);
  }
}
