package net.icgear.three.weixin.core.handler;

import java.io.File;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

import com.suisrc.core.utils.FileUtils;

import net.icgear.three.weixin.core.AbstractWeixinActivator;
import net.icgear.three.weixin.core.WxConfig;

/**
 * 微信配置获取助手
 * 
 * 只有在使用多配置信息截获器时候，该内容才有效
 * 
 * @author Y13
 *
 */
@Named("weixin-template-default")
@Dependent
public class WxTemplateSystemHandlerDef implements WxTemplateHandler, CallbackActivatorHandler {

  private WxConfig config;

  @Override
  public void setActivator(AbstractWeixinActivator activator) {
    this.config = activator;
  }

  /**
   * 获取数据文件夹
   * 
   * @return
   */
  private String getFolderName() {
    return "data/" + config.getAppUniqueKey() + "/template/" + config.getWxAppId() + "/";
  }

  /**
   * 
   */
  @Override
  public String findByShortId(String shortId) {
    String filename = getFolderName() + shortId;
    File file = new File(filename);
    if (!file.exists()) {
      return null;
    }
    return FileUtils.readContent(filename);
  }

  /**
   * 
   */
  @Override
  public void save(String shortId, String templateId) {
    String foldername = getFolderName();
    File folder = new File(foldername);
    if (!folder.exists()) {
      folder.mkdirs();
    }
    String filename = getFolderName() + shortId;
    FileUtils.writeConent(filename, templateId);
  }


}
