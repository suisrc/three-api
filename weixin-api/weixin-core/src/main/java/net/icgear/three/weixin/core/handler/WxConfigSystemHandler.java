package net.icgear.three.weixin.core.handler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.suisrc.core.Global;

import net.icgear.three.weixin.core.WxConfig;
import net.icgear.three.weixin.core.WxConfigSystem;

/**
 * 微信配置获取助手
 * 
 * 只有在使用多配置信息截获器时候，该内容才有效
 * 
 * @author Y13
 *
 */
public abstract class WxConfigSystemHandler implements WxConfigHandler {

  /**
   * 配置缓存
   */
  private Map<String, WxConfig> configs = new ConcurrentHashMap<>();

  /**
   * 获取微信配置信息
   * 
   * @param appKey
   * @return
   */
  public WxConfig findByAppKey(String appKey) {
    WxConfig config = configs.get(appKey);
    if (config == null) {
      config = new WxConfigSystem(getAppUniqueKey() + ".applications." + appKey);
      if (config.getWxAppId() == null) {
        // 如果appid没有，那么该配置是完全无效的
        return null;
      }
      configs.put(appKey, config);
    }
    return config;
  }

  /**
   * 获取应用关键字
   * 
   * @return
   */
  protected String getNodeKey() {
    return getAppUniqueKey();
  }

  /**
   * 获取应用关键字
   * 
   * @return
   */
  protected abstract String getAppUniqueKey();

  /**
   * 清除缓存
   */
  @Override
  public void clearByAppKey(String appKey) {
    configs.remove(appKey);
    Global.getLogger().info("已经清除系统【" + getAppUniqueKey() + "】->[" + appKey + "]的配置信息");
  }

}
