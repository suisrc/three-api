package net.icgear.three.weixin.core.type.suite;

import java.util.Collection;

import com.suisrc.three.core.msg.IMessage;
import com.suisrc.three.core.msg.MsgNode;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.annotation.WxInfoType;
import net.icgear.three.weixin.core.type.WxTypeParser;

/**
 * 默认的微信消息解析器
 * 
 * @author Y13
 *
 */
public class DefaultSuiteTypeParser implements WxTypeParser {

    /**
     * 消息类型索引
     */
    private SuiteTypeIndexs indexs;
    
    public DefaultSuiteTypeParser(Collection<Class<? extends IMessage>> classes, String category) {
        indexs = new SuiteTypeIndexs(classes, category);
    }
    
    /**
     * 执行微信内容解析内容
     */
    @Override
    public Class<? extends IMessage> parser(MsgNode node) {
        String[] keys = {
                node.getV(WxInfoType.XML_ATTR),
                node.getV(WxChangeType.XML_ATTR)
        };
        return indexs.searchFirstV(keys);
    }

}
