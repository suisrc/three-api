package net.icgear.three.weixin.core.asserts;

/**
 * 起始断言
 * 
 * @author Y13
 *
 */
public class TypeEndsWithAssert implements TypeAssert {

  public boolean apply(String source, String target) {
    return source != null && source.endsWith(target);
  }

}
