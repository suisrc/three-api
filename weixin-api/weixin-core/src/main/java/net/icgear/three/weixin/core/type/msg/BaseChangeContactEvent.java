package net.icgear.three.weixin.core.type.msg;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import net.icgear.three.weixin.core.annotation.WxEvent;

/**
 * 变更基础类
 * 
 * @author Y13
 *
 */
@WxEvent("change_contact")
public class BaseChangeContactEvent extends WxEventMessage {

    /**
     * 修改类型
     */
    @JacksonXmlProperty(localName = "ChangeType")
    @JsonProperty("ChangeType")
    private String changeType;

    /**
     * 获取修改类型
     * @return the changeType
     */
    public String getChangeType() {
        return changeType;
    }

    /**
     * 设定修改类型
     * @param changeType the changeType to set
     */
    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

}
