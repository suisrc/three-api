package net.icgear.three.weixin.core.event.qy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.annotation.WxEvent;
import net.icgear.three.weixin.core.type.msg.WxEventMessage;

/**
 * 异步任务完成事件推送
 * 
 * 本事件是成员在使用异步任务接口时，用于接收任务执行完毕的结果通知。
 * 
 * @author Y13
 *
 */
@WxEvent(value="batch_job_result", category=WxConsts.QY)
@JacksonXmlRootElement(localName = "xml")
public class BatchJobResultEvent extends WxEventMessage {
    
    /**
     * 异步任务
     */
    public static class BatchJob {

        /**
         * 异步任务id，最大长度为64字符 
         */
        @JacksonXmlProperty(localName = "JobId")
        @JsonProperty("JobId")
        private String jobId;

        /**
         * 操作类型，字符串，目前分别有：sync_user(增量更新成员)、 replace_user(全量覆盖成员）、invite_user(邀请成员关注）、replace_party(全量覆盖部门)
         */
        @JacksonXmlProperty(localName = "JobType")
        @JsonProperty("JobType")
        private String jobType;

        /**
         * 返回码
         */
        @JacksonXmlProperty(localName = "ErrCode")
        @JsonProperty("ErrCode")
        private Integer errCode;

        /**
         * 对返回码的文本描述内容
         */
        @JacksonXmlProperty(localName = "ErrMsg")
        @JsonProperty("ErrMsg")
        private String errMsg;

        /**
         * 获取异步任务id，最大长度为64字符
         * @return the jobId
         */
        public String getJobId() {
            return jobId;
        }

        /**
         * 设定异步任务id，最大长度为64字符
         * @param jobId the jobId to set
         */
        public void setJobId(String jobId) {
            this.jobId = jobId;
        }

        /**
         * 获取操作类型，字符串，目前分别有：sync_user(增量更新成员)、 replace_user(全量覆盖成员）、invite_user(邀请成员关注）、replace_party(全量覆盖部门)
         * @return the jobType
         */
        public String getJobType() {
            return jobType;
        }

        /**
         * 设定操作类型，字符串，目前分别有：sync_user(增量更新成员)、 replace_user(全量覆盖成员）、invite_user(邀请成员关注）、replace_party(全量覆盖部门)
         * @param jobType the jobType to set
         */
        public void setJobType(String jobType) {
            this.jobType = jobType;
        }

        /**
         * 获取返回码
         * @return the errCode
         */
        public Integer getErrCode() {
            return errCode;
        }

        /**
         * 设定返回码
         * @param errCode the errCode to set
         */
        public void setErrCode(Integer errCode) {
            this.errCode = errCode;
        }

        /**
         * 获取对返回码的文本描述内容
         * @return the errMsg
         */
        public String getErrMsg() {
            return errMsg;
        }

        /**
         * 设定对返回码的文本描述内容
         * @param errMsg the errMsg to set
         */
        public void setErrMsg(String errMsg) {
            this.errMsg = errMsg;
        }
        
    }

    /**
     * 任务详情
     */
    @JacksonXmlProperty(localName = "BatchJob")
    @JsonProperty("BatchJob")
    private BatchJob batchJob;

    /**
     * 获取任务详情
     * @return the batchJob
     */
    public BatchJob getBatchJob() {
        return batchJob;
    }

    /**
     * 设定任务详情
     * @param batchJob the batchJob to set
     */
    public void setBatchJob(BatchJob batchJob) {
        this.batchJob = batchJob;
    }
    
}
