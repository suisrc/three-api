package net.icgear.three.weixin.core.event;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxEvent;
import net.icgear.three.weixin.core.type.msg.WxEventMessage;

/**
 * 取消订阅事件
 * <Event><![CDATA[unsubscribe]]></Event>
 * @author Y13
 *
 */
@WxEvent("unsubscribe")
@JacksonXmlRootElement(localName="xml")
public class UnsubscribeEvent extends WxEventMessage {
}
