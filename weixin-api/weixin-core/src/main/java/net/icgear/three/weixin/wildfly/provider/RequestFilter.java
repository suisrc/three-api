package net.icgear.three.weixin.wildfly.provider;

import java.io.IOException;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.ext.Provider;

import com.suisrc.core.Global;
import com.suisrc.core.ScConsts;

import net.icgear.three.weixin.core.WxConsts;


/**
 * 该对象拦截所有请求 对登录凭证进行获取，放入登录缓存中。
 * 
 * 语言选择使用“lang_choice”进行标记
 * 
 * @author Y13
 *
 */
@Provider
@Priority(1)
@ApplicationScoped
public class RequestFilter implements ContainerRequestFilter {

  @Override
  public void filter(ContainerRequestContext requestContext) throws IOException {
    // 把请求的内容放入线程缓存中，以备框架使用
    Global.putCacheSafe(Global.getThreadCache(), WxConsts.CONTAINER_REQUEST_CONTEXT, requestContext);
    try { // 防止发生异常
      Cookie cookie = requestContext.getCookies().get("lang_choice");
      if (cookie != null) {
        Global.getRequestCache().put(ScConsts.REQUEST_LANG, cookie.getValue());
      }
    } catch (Exception e) {
    }
  }


}
