package net.icgear.three.weixin.core.oauth2;

import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 用户系统重定向跳转备用方式返回授权信息
 * 
 * @author Y13
 *
 */
@JsonInclude(Include.NON_NULL)
public class WxAuthResult {

  /**
   * 用于openid
   */
  private String openid;

  /**
   * 扩展属性
   */
  private Map<String, Object> props;

  public String getOpenid() {
    return openid;
  }

  public void setOpenid(String openid) {
    this.openid = openid;
  }

  public Map<String, Object> getProps() {
    return props;
  }

  public void setProps(Map<String, Object> props) {
    this.props = props;
  }

  public Object putProps(String key, Object value) {
    if (props == null) {
      props = new LinkedHashMap<>();
    }
    return props.put(key, value);
  }

}
