package net.icgear.three.weixin.core.oauth2;

import net.icgear.three.weixin.core.WxConfig;

/**
 * <p> binding中对回调授权时候使用的和openid组件通信的句柄
 * 
 * @author Y13
 *
 */
public interface WxOAuth2RestHandler {

  /**
   * <p> 获取登录凭据
   * 
   * @param dto
   */
  String getOpenIdByWxAuth2();


  /**
   * <p> 调用用户凭据，可以同步用户信息
   * 
   * <p> 返回true表示支持重定向跳转，否则禁止跳转
   * 
   * @param owner
   * @param openid
   * @param scope
   * @param key
   * @param userTicket
   * @param newCookie
   */
  boolean doUserTicket2Redirect(Object owner, String openid, String scope, String key, String userTicket, boolean isFirst);

  /**
   * <p> 初始化登录凭证
   * 
   * @param dto
   */
  default WxAuthResult getWxAuthResult(String openid) {
    WxAuthResult res = new WxAuthResult();
    res.setOpenid(openid);
    return res;
  }

  /**
   * <p> 获取OAuth2临时访问密匙
   * 
   * @param secret
   * @param token
   * @return
   */
  String getOAuth2State(String secret, String token);

  /**
   * <p> 自定义重定向
   * 
   * @param config
   * @param openid
   * @param key
   * @return
   */
  default String getRedirectUri(WxConfig config, String openid, String key) {
    return null;
  }
}
