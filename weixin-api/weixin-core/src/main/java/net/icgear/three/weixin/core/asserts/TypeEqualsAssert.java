package net.icgear.three.weixin.core.asserts;

/**
 * 等式断言
 * 
 * @author Y13
 *
 */
public class TypeEqualsAssert implements TypeAssert {

  public boolean apply(String source, String target) {
    return source != null && source.equals(target);
  }

}
