package net.icgear.three.weixin.core.type;

import java.util.ArrayList;

import net.icgear.three.weixin.core.asserts.TypeAssert;
import net.icgear.three.weixin.core.asserts.TypeEqualsAssert;

/**
 * 事件中类型的描述信息
 * 
 * @author Y13
 *
 */
public abstract class AbstractTypeInfo<V> {

  /**
   * 绑定的类型
   */
  protected V target;

  /**
   * 断言匹配器
   */
  protected ArrayList<RefAssert> asserts;

  /**
   * 构造方法
   */
  protected AbstractTypeInfo(V target) {
    this.target = target;
    this.asserts = new ArrayList<>(4);
    initialize();
  }

  /**
   * 初始化
   */
  protected abstract void initialize();


  /**
   * 优先级
   */
  public abstract String getPriority();

  /**
   * 绑定的消息类型
   * 
   * @return
   */
  public V getTarget() {
    return target;
  }

  /**
   * 获取断言器
   * 
   * @return
   */
  public ArrayList<RefAssert> getAsserts() {
    return asserts;
  }

  /**
   * 内容匹配
   * 
   * @param content
   * @return
   */
  public boolean matches(String... targets) {
    if (targets == null || targets.length < getAsserts().size()) {
      // 快速匹配失败
      return false;
    }
    for (int index = 0; index < getAsserts().size(); index++) {
      RefAssert ra = getAsserts().get(index);
      String target = targets[index];
      if (target == null || !ra.getHandler().apply(ra.getValue(), target)) {
        // 断言验证失败，直接快速返回
        return false;
      }
    }
    // 断言验证成功
    return true;
  }

  /**
   * 获取归档关键字
   * 
   * @return
   */
  public String getArchiveKey() {
    StringBuilder sbir = new StringBuilder();
    for (RefAssert ra : getAsserts()) {
      if (ra.getHandler().getClass() != TypeEqualsAssert.class) {
        // 快速失败，无法进行匹配
        break;
      }
      if (sbir.length() > 0) {
        sbir.append('|');
      }
      sbir.append(ra.getValue());
    }
    return sbir.toString();
  }

  /**
   * 获取关键字列表
   * 
   * @param sort
   * 
   * @param targets
   * @return
   */
  public static String[] getIdentity(String... targets) {
    if (targets == null || targets.length == 0) {
      return new String[] {""};
    }

    ArrayList<String> ids = new ArrayList<>();
    ids.add(""); // 共通内容
    StringBuilder sbir = new StringBuilder();
    for (String target : targets) {
      if (target == null) {
        // 快速失败
        break;
      }
      if (sbir.length() > 0) {
        sbir.append('|');
      }
      sbir.append(target);
      ids.add(sbir.toString());
    }
    if (ids.isEmpty()) {
      return new String[0];
    }
    if (ids.size() == 1) {
      return new String[] {ids.get(0)};
    }
    String[] res = new String[ids.size()];
    int len = ids.size() - 1;
    for (int i = 0; i <= len; i++) {
      res[i] = ids.get(len - i);
    }
    return res;
  }

  /**
   * 断言器数据
   */
  public static class RefAssert {
    private final String value;
    private final TypeAssert handler;

    public RefAssert(String val, TypeAssert hdl) {
      value = val;
      handler = hdl;
    }

    public String getValue() {
      return value;
    }

    public TypeAssert getHandler() {
      return handler;
    }
  }
}
