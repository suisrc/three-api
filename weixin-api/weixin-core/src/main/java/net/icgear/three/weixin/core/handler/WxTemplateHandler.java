package net.icgear.three.weixin.core.handler;

/**
 * 消息模版处理句柄模版
 * 
 * @author Y13
 *
 */
public interface WxTemplateHandler {

  /**
   * 获取模版ID
   */
  String findByShortId(String shortId);

  /**
   * 保存模版对应关系
   */
  void save(String shortId, String templateId);

}
