package net.icgear.three.weixin.core.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.asserts.TypeAssert;
import net.icgear.three.weixin.core.asserts.TypeEqualsAssert;

/**
 * 
 * @author Y13
 */
@Target(TYPE)
@Retention(RUNTIME)
@Inherited
public @interface WxInfoType {

  final String XML_ATTR = "InfoType";

  /**
   * 匹配内容
   * 
   * @return
   */
  String value();

  /**
   * 类别，如果类别不为空，会对需要加入的内容该的类别记性识别，一直才可以被加入监听
   * 
   * 如果为空，则忽略
   */
  String category() default WxConsts.EMPTY;

  /**
   * 匹配的顺序，在多匹配类型中，只有最后一个匹配配种的顺序是有效的
   */
  String priority() default WxConsts.DEFAULT_PRIORITY;;

  /**
   * 适配对比方式，默认采用等式断言
   */
  Class<? extends TypeAssert> handler() default TypeEqualsAssert.class;

}
