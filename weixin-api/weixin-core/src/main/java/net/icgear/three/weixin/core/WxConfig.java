package net.icgear.three.weixin.core;

import com.suisrc.jaxrsapi.core.AbstractTokenActivator.TokenAtom;

/**
 * 微信配置接口
 * 
 * @author Y13
 *
 */
public interface WxConfig {

  /**
   * 服务节点信息
   */
  String getWxNodeId();

  /**
   * 唯一key的内容
   */
  String getAppUniqueKey();

  /**
   * 微信应用ID
   */
  String getWxAppId();

  /**
   * 第三方应用ID
   * 
   * 只有企业号有效，否则返回null
   */
  String getWxSuiteId();

  /**
   * 企业自建应用ID
   * 
   * 只有企业自建ID有效
   */
  Integer getAgentId();

  /**
   * 微信应用秘钥
   */
  String getWxAppSecret();

  /**
   * 微信回调加密令牌
   */
  String getWxToken();

  /**
   * 微信回调加密秘钥
   */
  String getWxEncodingKey();

  /**
   * 回调消息是否需要加密
   * 
   * @return
   */
  boolean isWxEncrypt();

  /**
   * 归属行业编码
   */
  String[] getIndustryIds();

  /**
   * 是否可以重置行业编码
   * 
   * 由于行业编码的特殊性，正常情况下是不允许重置行业编码 但是在系统自动部署时候，行业编码需要系统进行修正
   * 
   * 使用该方法时候，最好调用后，直接重置为false,防止再次调用
   */
  boolean isIndustryEdit();

  /**
   * 加密方式
   * 
   * @return
   */
  String getEncryptType();

  /**
   * 获取当前服务器域名
   */
  default String getWebDomain() {
    return null;
  }

  /**
   * 获取激活器绑定的微信首页地址
   * 
   * 如果地址没有http/https，系统会使用当前请求的域增加该值返回的内容
   * 
   * 如果该值返回null, 则系统也会返回null
   */
  default String getWebIndex() {
    return null;
  }

  /**
   * 如果地址没有http/https，系统会使用当前请求的域增加该值返回的内容
   * 
   * 如果该值返回null, 则系统也会返回null
   * 
   * @param key
   * @return
   */
  default String getWebIndex(String key) {
    return null;
  }

  // -------------------------------------------------Access Token
  /**
   * 获取访问令牌
   */
  String getWxAccessToken();

  /**
   * 清除微信访问令牌
   */
  void clearWxAccessToken();

  /**
   * 推送配置信息
   * 
   * @param key
   * @param value
   */
  <T> void setAdapter(String named, Class<T> type, T value);

  /**
   * 获取配置属性
   */
  <T> T getAdapter(String named, Class<T> type);

  // -------------------------------------------------Token Atom

  /**
   * 查询令牌元
   * 
   * @param tokenKey
   * @return
   */
  TokenAtom findTokenAtom(String tokenKey);

  /**
   * 保存令牌元
   * 
   * 通常情况使用ConcurrentHashMap作为缓存即可
   * 
   * @param tokenKey
   * @param tokenAtom
   * @return
   */
  TokenAtom saveTokenAtom(String tokenKey, TokenAtom tokenAtom);

  /**
   * 清除，一般指清除系统缓存中的配置信息
   */
  default void clearConfig(boolean remove) {};

  // -------------------------------------------------other

  /**
   * 验证请求应用唯一标识
   */
  default boolean checkRequestAppKey() {
    return true;
  }
}
