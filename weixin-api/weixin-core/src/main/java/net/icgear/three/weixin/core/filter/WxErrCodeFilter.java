package net.icgear.three.weixin.core.filter;

import com.suisrc.jaxrsapi.core.runtime.RetryPredicate;

import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.core.exception.WxErrCodeException;

/**
 * 
 * 将访问放生异常的情况使用抛出异常的方式进行加载
 * 
 * 该方法不会重复访问多次
 * 
 * @author Y13
 *
 */
public class WxErrCodeFilter implements RetryPredicate<Object> {

    /**
     * 断言
     */
    @Override
    public boolean test(int count, int time, Object result, Exception e) {
        if (result instanceof WxErrCode) {
            WxErrCode wec = (WxErrCode) result;
            if (wec.getErrcode() != null && wec.getErrcode() != 0) {
                // 非空，非零就是异常
                throw WxErrCodeException.err(wec);
            }
        }
        return false;
    }

}
