package net.icgear.three.weixin.core.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.ApiOperation;
import net.icgear.three.weixin.core.WxConsts;

public interface WxWhitelistRest {

  // --------------------------------------------------------
  // 企业端口白名单
  // --------------------------------------------------------

  @ApiOperation(nickname = "wt11010", value = "刷新/启用微信服务器IP白名单")
  @GET
  @Path("wt/u")
  @Produces(MediaType.TEXT_PLAIN + WxConsts.MediaType_UTF_8)
  String updateWhiteList(@QueryParam("secret") String secret);

  @ApiOperation(nickname = "wt11011", value = "删除/禁用微信服务器IP白名单")
  @GET
  @Path("wt/d")
  @Produces(MediaType.TEXT_PLAIN + WxConsts.MediaType_UTF_8)
  String deleteWhiteList(@QueryParam("secret") String secret);
}
