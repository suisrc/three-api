package net.icgear.three.weixin.core.event.qy;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.annotation.WxEvent;
import net.icgear.three.weixin.core.type.msg.WxEventMessage;

/**
 * 本事件在成员进入企业微信的应用时触发
 * 
 * <Event><![CDATA[enter_agent]]></Event>
 * 事件KEY值，此事件该值为空
 * <EventKey><![CDATA[]]></EventKey>
 * 
 * @author Y13
 *
 */
@WxEvent(value="enter_agent", category=WxConsts.QY)
@JacksonXmlRootElement(localName = "xml")
public class EnterAgentEvent extends WxEventMessage {

    /**
     * 事件KEY值，此事件该值为空
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "EventKey")
    @JsonProperty("EventKey")
    private String eventKey;

    /**
     * 事件KEY值，此事件该值为空
     * @return the eventKey
     */
    public String getEventKey() {
        return eventKey;
    }

    /**
     * 事件KEY值，此事件该值为空
     * @param eventKey the eventKey to set
     */
    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }
}
