package net.icgear.three.weixin.core.handler;

import net.icgear.three.weixin.core.AbstractWeixinActivator;

/**
 * 消息模版处理句柄模版
 * 
 * 回调接口
 * 
 * @author Y13
 *
 */
public interface CallbackActivatorHandler {

  /**
   * 
   * @param activator
   */
  void setActivator(AbstractWeixinActivator activator);

}
