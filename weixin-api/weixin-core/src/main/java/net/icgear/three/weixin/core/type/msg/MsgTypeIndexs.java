package net.icgear.three.weixin.core.type.msg;

import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Set;

import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.three.core.msg.IMessage;

import net.icgear.three.weixin.core.annotation.WxMsgType;
import net.icgear.three.weixin.core.type.AbstractTypeIndexs;

/**
 * 类型构建的索引
 * 
 * @author Y13
 *
 */
public class MsgTypeIndexs extends AbstractTypeIndexs<MsgTypeInfo, Class<? extends IMessage>> {

    /**
     * 构造方法
     * @param creater
     * @param values
     */
    public MsgTypeIndexs(Collection<Class<? extends IMessage>> classes, String category) {
        super(clazz -> newTypeInfo(clazz, category), classes);
    }

    /**
     * 获取所有文件中的监听类型
     * @param packages
     * @return
     */
    public static <T> Set<Class<? extends T>> getTypeInfoClasses(Class<T> clazz, String[] packages) {
        if (packages.length == 0) {
            throw new RuntimeException("无法构建消息类型，构建的包为空:" + MsgTypeIndexs.class.getName());
        }
        // 查询指定包内所有继承接口
        Set<Class<? extends T>> classes = ReflectionUtils.getSubclasses(clazz, packages);
        return classes;
    }

    /**
     * 根据类型创建消息描述对象
     * @param clazz
     * @return
     */
    private static MsgTypeInfo newTypeInfo(Class<? extends IMessage> clazz, String category) {
        if (Modifier.isAbstract(clazz.getModifiers())) {
            return null; // 抽象类跳过
        }
        if (!clazz.isAnnotationPresent(WxMsgType.class)) {
            // 不存在解析类型或者失效，跳过
            return null; 
        }
        MsgTypeInfo info = MsgTypeInfo.create(clazz);
        if (!info.getCategory().isEmpty() && !info.getCategory().equals(category)) {
            // 类型匹配失败
            return null;
        }
        return info;
    }
}
