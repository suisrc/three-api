package net.icgear.three.weixin.core.asserts;

/**
 * 正在表达式断言
 * 
 * @author Y13
 *
 */
public class TypeRegexAssert implements TypeAssert {

  public boolean apply(String source, String target) {
    return source != null && source.matches(target);
  }

}
