package net.icgear.three.weixin.core.rest;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.ApiOperation;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxEncryptSignature;
import net.icgear.three.weixin.core.oauth2.WxAuthParams;
import net.icgear.three.weixin.core.oauth2.WxAuthResult;

public interface WxBindingRest extends WxWhitelistRest {

  @ApiOperation(nickname = "wx11010", value = "后台微信请求服务器运行状态")
  @GET
  @Path("info")
  @Produces(MediaType.TEXT_PLAIN + WxConsts.MediaType_UTF_8)
  String getServerInfo();

  @ApiOperation(nickname = "wx11011", value = "清除微信服务器的token等信息")
  @GET
  @Path("clear")
  @Produces(MediaType.TEXT_PLAIN + WxConsts.MediaType_UTF_8)
  String clear(@QueryParam("secret") String secret, @QueryParam("remove") boolean remove);

  // --------------------------------------------------------
  // 回调接口
  // --------------------------------------------------------

  @ApiOperation(nickname = "wx11020", value = "微信回调URL绑定")
  @GET
  @Path(WxConsts.REST_PATH_WX)
  @Produces(MediaType.TEXT_PLAIN + WxConsts.MediaType_UTF_8)
  String doGet(@BeanParam WxEncryptSignature sign);

  @ApiOperation(nickname = "wx11021", value = "微信回调请求绑定")
  @POST
  @Path(WxConsts.REST_PATH_WX)
  @Produces(MediaType.APPLICATION_XML + WxConsts.MediaType_UTF_8)
  Response doPost(@BeanParam WxEncryptSignature sign, String data);

  // --------------------------------------------------------
  // 重定向授权
  // --------------------------------------------------------

  @ApiOperation(nickname = "au11030", value = "微信授权重定向, key='none'表示不进行重定向跳转")
  @GET
  @Path(WxConsts.REST_PATH_AUTH)
  @Produces(MediaType.APPLICATION_JSON + WxConsts.MediaType_UTF_8)
  WxAuthResult doOAuth2(@BeanParam WxAuthParams params);

  @ApiOperation(nickname = "wx11031", value = "微信授权重定向, 颁发临时密匙(如果token不为空，颁发的内容为永久密匙)")
  @GET
  @Path(WxConsts.REST_PATH_AUTH + "/state")
  @Produces(MediaType.APPLICATION_JSON + WxConsts.MediaType_UTF_8)
  String getOAuth2State(@QueryParam("secret") String secret, @QueryParam("token") String token);

}
