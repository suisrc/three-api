package net.icgear.three.weixin.wildfly.exception;

import java.util.logging.Logger;

import javax.annotation.Priority;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.suisrc.core.exception.ResponseException;

/**
 * 发生代理执行异常
 * 
 * @author Y13
 *
 */
@Provider
@Priority(9990)
public class ResponseExceptionMapper implements ExceptionMapper<ResponseException> {
  private static final Logger logger = Logger.getLogger(ResponseExceptionMapper.class.getPackage().getName());

  @Override
  public Response toResponse(ResponseException e) {
    logger.info(e.getMessage());
    return e.getResponse();
  }

}
