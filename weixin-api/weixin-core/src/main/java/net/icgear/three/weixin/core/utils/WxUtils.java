package net.icgear.three.weixin.core.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URI;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.function.BiFunction;

import javax.ws.rs.container.ContainerRequestContext;

import com.suisrc.core.Global;
import com.suisrc.core.utils.Throwables;
import com.suisrc.jaxrsapi.core.ServiceClient;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.annotation.WxTemplateMessage;
import net.icgear.three.weixin.core.annotation.WxTemplateProperty;
import net.icgear.three.weixin.core.bean.TemplateDataInfo;

public class WxUtils {

  /**
   * <p> 获取请求句柄内容
   * 
   * @return
   */
  public static ContainerRequestContext getContainerRequestContext() {
    return Global.getCacheSafe(Global.getThreadCache(), WxConsts.CONTAINER_REQUEST_CONTEXT, ContainerRequestContext.class);
  }

  /**
   * <p> 设定请求的appKey
   * 
   * @param request
   */
  public static void setAppKey() {
    ContainerRequestContext request = getContainerRequestContext();
    setAppKey(request);
  }


  /**
   * <p> 设定请求的appKey
   * 
   * @param request
   */
  public static void setAppKey(ContainerRequestContext request) {
    if (request == null) {
      return;
    }
    String appKey = request.getUriInfo().getQueryParameters().getFirst(WxConsts.REST_APP_KEY);
    if (appKey != null && !appKey.isEmpty()) {
      setAppKey(appKey);
    }
  }

  /**
   * <p> 设定请求的appKey
   * 
   * @param sign
   */
  public static void setAppKey(String appKey) {
    if (appKey != null && !appKey.isEmpty()) {
      Global.putCacheSafe(Global.getThreadCache(), WxConsts.REQUEST_APP_KEY, appKey);
    } else {
      Global.removeCacheSafe(Global.getThreadCache(), WxConsts.REQUEST_APP_KEY);
    }
  }

  /**
   * <p> 设定请求的appKey
   */
  public static void setTempAppKey(String appKey, Runnable runnable) {
    Global.execByTempValue(WxConsts.REQUEST_APP_KEY, appKey, runnable);
  }

  /**
   * <p> 设定请求的appKey
   */
  public static <T> T setTempAppKey(String appKey, Callable<T> callable) {
    return Global.execByTempValue(WxConsts.REQUEST_APP_KEY, appKey, callable);
  }

  /**
   * <p> 获取请求的appKey
   */
  public static String getAppKey() {
    return Global.getCacheSafe(Global.getThreadCache(), WxConsts.REQUEST_APP_KEY, String.class);
  }

  /**
   * <p> 获取请求的openid
   */
  public static String getWxOpenId() {
    return Global.getCacheSafe(Global.getThreadCache(), WxConsts.COOKIE_OPEN_ID, String.class);
  }

  /**
   * <p> 获取URI的域
   * 
   * @param uri
   * @return
   */
  public static String getHostPortFromURI(URI uri) {
    if (uri == null) {
      return null;
    }
    StringBuffer sb = new StringBuffer();
    if (uri.getScheme() != null) {
      sb.append(uri.getScheme());
      sb.append(':');
    }
    if (uri.isOpaque()) {
      sb.append(uri.getRawSchemeSpecificPart());
    } else {
      if (uri.getHost() != null) {
        sb.append("//");
        if (uri.getRawUserInfo() != null) {
          sb.append(uri.getRawUserInfo());
          sb.append('@');
        }
        boolean needBrackets =
            ((uri.getHost().indexOf(':') >= 0) && !uri.getHost().startsWith("[") && !uri.getHost().endsWith("]"));
        if (needBrackets)
          sb.append('[');
        sb.append(uri.getHost());
        if (needBrackets)
          sb.append(']');
        if (uri.getPort() != -1) {
          sb.append(':');
          sb.append(uri.getPort());
        }
      } else if (uri.getRawAuthority() != null) {
        sb.append("//");
        sb.append(uri.getRawAuthority());
      }
    }
    return sb.toString();
  }

  /**
   * <p> 获取URI的参数
   * 
   * @param uri
   * @return
   */
  public static String getPathQueryFromURI(URI uri) {
    if (uri == null) {
      return null;
    }
    StringBuffer sb = new StringBuffer();
    if (uri.getRawPath() != null)
      sb.append(uri.getRawPath());
    if (uri.getRawQuery() != null) {
      sb.append('?');
      sb.append(uri.getRawQuery());
    }
    if (uri.getRawFragment() != null) {
      sb.append('#');
      sb.append(uri.getRawFragment());
    }
    return sb.toString();
  }

  /**
   * <p> 获取模版消息中的数据
   */
  public static Map<String, TemplateDataInfo> getTemplateMessageDatas(Object data) {
    if (data == null) {
      return Collections.emptyMap();
    }
    // Object[3]-> 0:order, 1:color, 2:method
    Map<String, Object[]> tmp3 = new HashMap<>();
    Map<String, Method> other = new HashMap<>();

    Class<?> clazz = data.getClass();
    // 执行方法
    for (Method method : clazz.getMethods()) {
      // 查询对应的属性
      String field = method.getName();
      if (field.startsWith("get")) {
        field = field.substring(3, 4).toLowerCase() + field.substring(4);
      } else if (field.startsWith("is")) {
        field = field.substring(2, 3).toLowerCase() + field.substring(3);
      }
      WxTemplateProperty wtp = method.getAnnotation(WxTemplateProperty.class);
      if (wtp == null) {
        other.put(field, method);
        continue;
      }
      String property = wtp.value();
      if (property.isEmpty()) {
        property = field;
      }
      String color = wtp.color().isEmpty() ? null : wtp.color();
      int order = wtp.order();
      tmp3.put(property, new Object[] {order, color, method});
    }
    // 执行私有属性构建
    while (clazz != null && clazz != Object.class) {
      for (Field field : clazz.getDeclaredFields()) {
        WxTemplateProperty wtp = field.getAnnotation(WxTemplateProperty.class);
        if (wtp == null) {
          continue;
        }
        Method method = other.get(field.getName());
        if (method == null) {
          // 没有get方法的属性放弃
          continue;
        }
        String property = wtp.value();
        if (property.isEmpty()) {
          property = field.getName();
        }
        String color = wtp.color().isEmpty() ? null : wtp.color();
        int order = wtp.order();
        tmp3.put(property, new Object[] {order, color, method});
      }
      // 执行上级检索
      clazz = clazz.getSuperclass();
    }
    Map<String, TemplateDataInfo> res = new LinkedHashMap<>();
    tmp3.entrySet().stream().sorted((l, r) -> (int) l.getValue()[0] - (int) r.getValue()[0]).forEach(e -> {
      try {
        String property = e.getKey();
        String color = (String) e.getValue()[1];
        Method method = (Method) e.getValue()[2];
        Object value = method.invoke(data);
        TemplateDataInfo tdi = new TemplateDataInfo();
        tdi.setColor(color);
        tdi.setValue(value == null ? "" : value.toString());
        res.put(property, tdi);
      } catch (Exception e1) {
        throw Throwables.getRuntimeException(e1);
      }
    });
    return res;
  }

  /**
   * <p> 发送模版消息
   */
  public static <R> R sendTemplateMessage(Object target, Object data,
      BiFunction<String, Map<String, TemplateDataInfo>, R> func) {
    WxTemplateMessage wtm = data.getClass().getAnnotation(WxTemplateMessage.class);
    if (wtm == null) {
      // 无法获取模版类型， 完全失败错误
      throw new RuntimeException("The class has no 'WxTemplateMessage' annotation: " + data.getClass());
    }
    String templateId = wtm.id();
    if (templateId.isEmpty()) {
      if (!(target instanceof ServiceClient)) {
        // 当前client不可用，完全失败错误
        throw new RuntimeException("this class is not implements 'ServiceClient' interface: " + target.getClass());
      }
      if (wtm.value().isEmpty()) {
        // 模版短名为空，完全失败错误
        throw new RuntimeException("'WxTemplateMessage' value is empty: " + target.getClass());
      }
      String templateIdShort = WxConsts.FIND_TEMPLATE_ID_BY2 + wtm.value();
      templateId = ((ServiceClient) target).getActivator().getAdapter(templateIdShort, String.class);
      if (templateId == null || templateId.isEmpty()) {
        // 可失败性错误，返回空
        return null;
      }
    }
    // 模版数据
    Map<String, TemplateDataInfo> map = WxUtils.getTemplateMessageDatas(data);
    R res = func.apply(templateId, map);
    return res;
  }

}
