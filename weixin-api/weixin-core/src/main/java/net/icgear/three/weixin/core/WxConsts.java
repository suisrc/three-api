package net.icgear.three.weixin.core;

/**
 * 常数
 * 
 * @author Y13
 *
 */
public interface WxConsts {

  /**
   * 测试标识
   */
  final boolean DEBUG = Boolean.getBoolean("debug"); // $NON-NLS-N$

  /**
   * 空
   */
  final String EMPTY = ""; // $NON-NLS-N$

  // --------------------------------------------------企业OR公众

  /**
   * 企业
   * 
   * 主要用于绑定企业回调接口
   */
  final String QY = "QY"; // $NON-NLS-N$

  /**
   * 企业主体
   * 
   * 用于区分企业主体和企业应用
   */
  final String QM = "QM"; // $NON-NLS-N$

  /**
   * 企业应用
   */
  final String QT = "QT"; // $NON-NLS-N$

  /**
   * 公众
   */
  final String MP = "MP"; // $NON-NLS-N$

  /**
   * 小程序
   * 
   * mini program page 由于缩写变为MP和公众号相同，说以这里变更一下 small program page -> SP
   * 
   * 在绑定回调接口，小程序使用MP的内容 所以这里仅仅用于区分接口，没有太多意义
   */
  final String SP = "SP"; // $NON-NLS-N$

  /**
   * 支付平台
   */
  final String PAY = "PAY"; // $NON-NLS-N$

  // --------------------------------------------------微信配置信息
  /**
   * 公众号ID或者企业ID
   */
  final String APP_ID = "APP_ID"; // $NON-NLS-N$

  /**
   * 应用秘钥
   */
  final String APP_SECRET = "APP_SECRET"; // $NON-NLS-N$

  /**
   * 应用ID
   */
  final String APP_SUITE_ID = "APP_SUITE_ID"; // $NON-NLS-N$

  /**
   * 用于OAuth2认证的ID
   */
  final String APP_OPEN_ID = "APP_OPEN_ID"; // $NON-NLS-N$

  /**
   * 微信访问令牌
   */
  final String ACCESS_TOKEN = "ACCESS_TOKEN"; // $NON-NLS-N$

  /**
   * 微信回调消息加密令牌
   */
  final String APP_TOKEN = "APP_TOKEN"; // $NON-NLS-N$

  /**
   * 微信回调消息加解密密钥
   */
  final String APP_ENCODING_KEY = "ENCODING_KEY"; // $NON-NLS-N$

  /**
   * 微信回调消息加密方式
   */
  final String ENCRYPT_TYPE_AES = "aes"; // $NON-NLS-N$
  // --------------------------------------------------微信配置信息

  /**
   * 默认顺序,事件的响应顺序
   */
  final String DEFAULT_PRIORITY = "N"; // $NON-NLS-N$

  /**
   * 请求成功
   */
  final String SUCCESS = "success"; // $NON-NLS-N$

  /**
   * 请求信息
   */
  final String CONTAINER_REQUEST_CONTEXT = "CONTAINER_REQUEST_CONTEXT"; // $NON-NLS-N$

  /**
   * 请求授权的openid， 在企业账户上，应该是userid
   */
  final String COOKIE_OPEN_ID = "wxopenid"; // $NON-NLS-N$

  /**
   * cookie配置前缀
   */
  final String COOKIE_OAUTH2_PRE = "com.qq.servers.oauth2."; // $NON-NLS-N$

  /**
   * 用户系统中线程上的openid调取，系统使用的内容，请不要单独使用
   */
  final String COOKIE_OPEN_ID2 = "$$wxopenid"; // $NON-NLS-N$

  /**
   * OAuth2授权时候使用的code
   */
  final String OAUTH2_CODE = "OAUTH2_CODE"; // $NON-NLS-N$

  /**
   * OAuth2授权时候使用的code
   */
  final String T_OAUTH2_CODE = "T:" + OAUTH2_CODE; // $NON-NLS-N$

  /**
   * 微信前缀
   */
  final String WEIXIN_PRE = "com.qq.weixin."; // $NON-NLS-N$

  /**
   * 微信默认
   */
  final String WEIXIN_DEF = "com.qq.default-weixin"; // $NON-NLS-N$


  /**
   * 请求的应用关键字
   */
  final String REQUEST_APP_KEY = "REQUEST_APP_KEY"; // $NON-NLS-N$

  /**
   * 重定向时候使用的当前服务器的url
   * 
   * 在处理过程中发现，通过nodejs进行重定向时候，服务器无法正确识别url地址
   */
  final String LOCAL_DOMAIN = "LOCAL_DOMAIN"; // $NON-NLS-N$

  /**
   * 当获取Oauth2授权时候，会把用户票据放入线程缓存中
   */
  final String OAUTH2_USER_TICKET = "OAUTH2_USER_TICKET"; // $NON-NLS-N$

  /**
   * 当获取Oauth2授权时候，用户的信息
   * 
   * 注意类型不确定，请自行确定返回类型
   */
  final String OAUTH2_USER_INFO = "OAUTH2_USER_INFO"; // $NON-NLS-N$

  /**
   * 查找模版ID，通过模版短名称
   * 
   * 该标记具有强制插入模版功能
   */
  final String FIND_TEMPLATE_ID_BY2 = "FIND_TEMPLATE_ID_BY2"; // $NON-NLS-N$

  /**
   * 安装应用后的agentid
   */
  final String APP_AGENT_ID = "APP_AGENT_ID"; // $NON-NLS-N$

  /**
   * MediaType内容增加utf-8
   */
  final String MediaType_UTF_8 = ";charset=utf-8"; // $NON-NLS-N$

  /**
   * 
   */
  final String WEIXIN_AUTHC_TYPE = "application/x-authc-code+json"; // $NON-NLS-N$

  /**
   * bind配置路径
   */
  final String REST_PATH_BIND = "bind"; // $NON-NLS-N$

  /**
   * suite配置路径
   */
  final String REST_PATH_SUITE = "suite"; // $NON-NLS-N$

  /**
   * 微信回调配置路径
   */
  final String REST_PATH_WX = "wx"; // $NON-NLS-N$

  /**
   * 使用app key的资源地址
   */
  final String REST_APP_KEY = "k"; // $NON-NLS-N$

  /**
   * 微信授权重定向配置路径
   */
  final String REST_PATH_AUTH = "au"; // $NON-NLS-N$
}
