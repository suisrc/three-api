package net.icgear.three.weixin.core.rest;

import java.net.URI;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.suisrc.core.Global;
import com.suisrc.core.exception.ResponseException;
import com.suisrc.core.message.ECM;
import com.suisrc.core.utils.CdiUtils;

import net.icgear.three.weixin.core.WxConfig;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.EncryptMessage;
import net.icgear.three.weixin.core.bean.WxEncryptSignature;
import net.icgear.three.weixin.core.oauth2.WxAuthParams;
import net.icgear.three.weixin.core.oauth2.WxAuthResult;
import net.icgear.three.weixin.core.oauth2.WxOAuth2RestHandler;
import net.icgear.three.weixin.core.utils.WxUtils;

/**
 * <p> 跟微信服务器捆绑
 * 
 * <p> 单个配置信息
 * 
 * @author Y13
 *
 */
public abstract class AbstractWxBindingOne extends AbstractWxBinding {

  /**
   * <p> 微信服务器的配置信息
   */
  private WxConfig config;

  /**
   * <p> 更OAuth2内容相关
   */
  private WxOAuth2RestHandler oauth2;



  /**
   * <p> 构造方法
   */
  @Override
  @PostConstruct
  protected void doPostConstruct() {
    WxConfig config = createWxConfig();
    if (config != null) {
      setConfig(config);
    }
    WxOAuth2RestHandler oauth2 = createOAuth2Handler();
    if (oauth2 != null) {
      setOauth2Handler(oauth2);
    }
    super.doPostConstruct();
  }

  // --------------------------------------------------接口

  /**
   * <p> 构建微信配置信息
   */
  protected abstract WxConfig createWxConfig();

  /**
   * 
   * @return
   */
  @Named("weixin-oauth2-openapi")
  protected WxOAuth2RestHandler createOAuth2Handler() {
    try {
      return CdiUtils.selectWithQualifier(WxOAuth2RestHandler.class);
    } catch (Exception e) {
      logger.warning("注入OAuth2句柄失败：" + e.getMessage());
      return null; // 这里可以忽略oauth2句柄的内容
    }
  }

  // -------------------------------------------------------

  /**
   * <p> 获取应用唯一关键字
   */
  @Override
  protected String getAppUniqueKey() {
    return config.getAppUniqueKey();
  }

  /**
   * <p> 设定微信信息
   * 
   * <p> 可以通过@Inject注解注入微信的控制激活器
   * 
   * @param config
   */
  protected void setConfig(WxConfig config) {
    this.config = config;
  }

  /**
   * <p> 运行重新指定访问授权的操作句柄
   * 
   * @param oauht2
   */
  public void setOauth2Handler(WxOAuth2RestHandler oauth2) {
    this.oauth2 = oauth2;
  }

  /**
   * <p> 获取微信绑定信息
   * 
   * @return
   */
  public WxConfig getConfig() {
    return config;
  }

  /**
   * <p> 后台微信请求服务器运行状态
   */
  @Override
  public String getServerInfo() {
    return "Server [" + config.getWxNodeId() + "] is Running! time:" + LocalDateTime.now();
  }

  /**
   * <p> 获取配置信息
   */
  @Override
  protected WxConfig getWxConfig(WxEncryptSignature sign, EncryptMessage encrypt) {
    return getConfig();
  }

  /**
   * <p> 校验地址是否可用
   */
  @Override
  protected void assertVisitorInfo() {
    super.assertVisitorInfo();
    if (!getConfig().checkRequestAppKey()) {
      // 请求应用唯一标识异常
      String msg = "Illegal request: AppKey error.";
      throw new ResponseException(msg, re -> {
        return Response.ok(msg).type(MediaType.TEXT_PLAIN + WxConsts.MediaType_UTF_8).build();
      });
    }
  }

  /**
   * <p> 清除
   * 
   * <p> 接口没有进行权限控制, 临时使用的密钥为引用ID
   * 
   * <p> 执行清除内容的密钥就是用户设定的应用token内容
   * 
   * @return
   */
  @Override
  public String clear(String secret, boolean remove) {
    if (secret == null) {
      return "认证失败：密钥为空";
    }
    WxUtils.setAppKey();
    WxConfig config = getConfig();
    if (config == null || !secret.equals(config.getWxToken())) {
      return "认证失败：密钥失败";
    }
    config.clearConfig(remove);
    return "操作成功：异常缓存【" + config.getWxAppId() + "】";
  }

  /**
   * <p> 重定向授权认证
   * 
   * <p> OAuth2Get注解带有AppKey验证
   * 
   * @param key
   * @return
   */
  protected WxAuthResult doDefaultOAuth2(WxAuthParams params) {
    if (oauth2 == null) {
      // 无法完成授权
      throw new RuntimeException("没有可用的OAuth2句柄，无法完成授权操作");
    }
    // 获取用户信息凭证
    String openid = oauth2.getOpenIdByWxAuth2();
    if (openid == null) {
      // 当前用户没有登录
      throw ECM.exception("SECURES-LOGIN-ON-401", "登录凭据中无有效的openid信息");
    }
    boolean redirect = true;
    if (params.isSync()) {
      // 成员票据，最大为512字节。
      // scope为snsapi_userinfo或snsapi_privateinfo，且用户在应用可见范围之内时返回此参数。
      // 后续利用该参数可以获取用户信息或敏感信息。
      // 企业自建应用可以根据userid获取成员详情，无需使用snsapi_userinfo和snsapi_privateinfo
      // 访问时候产生的用户访问令牌
      String userTicket = Global.getCacheSafe(Global.getThreadCache(), WxConsts.OAUTH2_USER_TICKET, String.class);
      // 是否为新cookie
      boolean newCookie = Global.getCacheSafe(Global.getThreadCache(), WxConsts.COOKIE_OPEN_ID2) != null;
      redirect = oauth2.doUserTicket2Redirect(this, openid, params.getScope(), params.getKey(), userTicket, newCookie);
    }
    if (!"none".equals(params.getKey()) && redirect) {
      // 业务处理
      String url = oauth2.getRedirectUri(config, openid, params.getKey());
      if (url != null) {
        String url0 = url;
        throw new ResponseException("自定义重定向到业务页面：" + url, re -> Response.seeOther(URI.create(url0)).build());
      }
      // 系统默认重定向规则
      url = config.getWebIndex(params.getKey());
      if (url != null && !url.isEmpty()) {
        // 请求重定向
        if (url.endsWith("???")) {
          // 三个问号结尾的url需要保留请求参数
          url = url.substring(0, url.length() - 3); // 去除最后三个标记符号
          ContainerRequestContext crc = WxUtils.getContainerRequestContext();
          Map<String, List<String>> queryParams = new HashMap<>(crc.getUriInfo().getQueryParameters());
          // 系统处理的参数，需要屏蔽
          String[] excludes = {"code", "state", "type", "k", "sync", "key", "scope"};
          for (String ekey : excludes) {
            queryParams.remove(ekey);
          }
          if (!queryParams.isEmpty()) {
            url += url.indexOf('?') < 0 ? "?" : "&";
            StringBuilder sb = new StringBuilder();
            queryParams.forEach((k, vs) -> vs.forEach(v -> sb.append(k).append('=').append(v).append('&')));
            sb.setLength(sb.length() - 1);
            url += sb.toString();
          }
        }
        String url0 = url;
        throw new ResponseException("重定向到业务页面：" + url, re -> Response.seeOther(URI.create(url0)).build());
      }
    }
    // 执行跳转登录
    return oauth2.getWxAuthResult(openid);
  }

  /**
   * <p> 获取密匙
   */
  @Override
  public String getOAuth2State(String secret, String token) {
    if (oauth2 == null) {
      // 无法完成授权
      throw new RuntimeException("没有可用的OAuth2句柄，无法完成授权操作");
    }
    return oauth2.getOAuth2State(secret, token);
  }

}
