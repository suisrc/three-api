package net.icgear.three.weixin.core.suite;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxInfoType;
import net.icgear.three.weixin.core.type.suite.BaseInfoSuite;

/**
 * 推送suite_ticket
 * 企业微信服务器会定时（每十分钟）推送ticket。ticket会实时变更，并用于后续接口的调用。
 * 
 * <InfoType> <![CDATA[suite_ticket]]></InfoType>
 * 
 * <SuiteTicket><![CDATA[asdfasfdasdfasdf]]></SuiteTicket>
 * @author Y13
 *
 */
@WxInfoType("suite_ticket")
@JacksonXmlRootElement(localName="xml")
public class TicketSuite extends BaseInfoSuite {

    /**
     * Ticket内容，最长为512字节
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "SuiteTicket")
    @JsonProperty("SuiteTicket")
    private String suiteTicket;

    /**
     * 获取文本消息内容
     * @return the suiteTicket
     */
    public String getSuiteTicket() {
        return suiteTicket;
    }

    /**
     * 设定文本消息内容
     * @param suiteTicket the suiteTicket to set
     */
    public void setSuiteTicket(String suiteTicket) {
        this.suiteTicket = suiteTicket;
    }

}
