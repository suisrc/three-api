package net.icgear.three.weixin.wildfly.provider;

import java.io.IOException;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.ext.Provider;

import com.suisrc.core.Global;

import net.icgear.three.weixin.core.WxConsts;

/**
 * 请求返回数据拦截器
 * 
 * @author Y13
 */
@Provider
@Priority(9999)
@ApplicationScoped
public class ResponseFilter implements ContainerResponseFilter {

  /**
   * cookie的声明周期，单位是秒，默认为1天
   * 
   * 当前没有使用@ConfigProperty注解，有点不符合规则，请注意
   */
  private int cookieMaxAge = Integer.getInteger(WxConsts.COOKIE_OAUTH2_PRE + "cookie.max-age", 24 * 60 * 60);

  /**
   * cookie的声明域名，默认为当前访问的地址
   */
  private String cookieDomain = System.getProperty(WxConsts.COOKIE_OAUTH2_PRE + "cookie.domain");

  @Override
  public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
    String openid = Global.getCacheSafe(Global.getThreadCache(), WxConsts.COOKIE_OPEN_ID2, String.class);
    if (openid != null) { // 保存cookies
      Cookie oldCookie = requestContext.getCookies().get(WxConsts.COOKIE_OPEN_ID);
      if (oldCookie == null || !openid.equals(oldCookie.getValue())) {
        String path = "/";
        String domain = cookieDomain != null ? cookieDomain : requestContext.getUriInfo().getBaseUri().getHost();
        int version = 1;
        NewCookie cookie = new NewCookie(WxConsts.COOKIE_OPEN_ID, openid, path, domain, version, null, cookieMaxAge, false);
        responseContext.getHeaders().addFirst("Set-Cookie", cookie);
        // responseContext.getHeaders().addFirst("Set-Cookie2", cookie); // ???
      }
    }
    // 清除线程上的所有内容
    Global.clearCacheSafe(Global.getThreadCache());
  }

}
