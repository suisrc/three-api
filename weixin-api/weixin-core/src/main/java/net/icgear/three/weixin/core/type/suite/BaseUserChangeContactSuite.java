package net.icgear.three.weixin.core.type.suite;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * 变更成员信息基础类型
 * 
 * 该内容比较复杂，固单独共通
 * 
 * @author Y13
 *
 */
public class BaseUserChangeContactSuite extends BaseChangeContactSuite {
    
    @JsonRootName("Item")
    public static class ExtAttr {
        
        @JacksonXmlProperty(localName = "Name")
        @JsonProperty("Name")
        private String name;

        @JacksonXmlProperty(localName = "Value")
        @JsonProperty("Value")
        private String value;
        
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getValue() {
            return value;
        }
        public void setValue(String value) {
            this.value = value;
        }
        
    }
    
    /**
     * 变更信息的成员UserID
     */
    @JacksonXmlProperty(localName = "UserID")
    @JsonProperty("UserID")
    private String userID;
    
    /**
     * 成员名称，变更时推送
     */
    @JacksonXmlProperty(localName = "Name")
    @JsonProperty("Name")
    private String name;
    
    /**
     * 更新后成员所在部门列表
     */
    @JacksonXmlProperty(localName = "Department")
    @JsonProperty("Department")
    private String department;
    
    /**
     * 手机号码，变更时推送，仅通讯录应用可获取
     */
    @JacksonXmlProperty(localName = "Mobile")
    @JsonProperty("Mobile")
    private String mobile;
    
    /**
     * 职位信息。长度为0~64个字节，仅通讯录管理应用可获取
     */
    @JacksonXmlProperty(localName = "Position")
    @JsonProperty("Position")
    private String position;
    
    /**
     * 性别，变更时推送。1表示男性，2表示女性
     */
    @JacksonXmlProperty(localName = "Gender")
    @JsonProperty("Gender")
    private Integer gender;
    
    /**
     * 邮箱，变更时推送 ，仅通讯录应用可获取
     */
    @JacksonXmlProperty(localName = "Email")
    @JsonProperty("Email")
    private String email;
    
    /**
     * 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。变更时推送，仅通讯录管理应用可获取
     */
    @JacksonXmlProperty(localName = "Avatar")
    @JsonProperty("Avatar")
    private String avatar;
    
    /**
     * 激活状态：1=激活或关注， 2=禁用， 4=未激活（重新启用未激活用户或者退出企业并且取消关注时触发）
     */
    @JacksonXmlProperty(localName = "Status")
    @JsonProperty("Status")
    private Integer status;
    
    /**
     * 英文名
     */
    @JacksonXmlProperty(localName = "EnglishName")
    @JsonProperty("EnglishName")
    private String englishName;
    
    /**
     * 上级字段，标识是否为上级。0表示普通成员，1表示上级。仅通讯录管理应用可获取
     */
    @JacksonXmlProperty(localName = "IsLeader")
    @JsonProperty("IsLeader")
    private Integer isLeader;
    
    /**
     * 座机，仅通讯录应用可获取
     */
    @JacksonXmlProperty(localName = "Telephone")
    @JsonProperty("Telephone")
    private String telephone;
    
    /**
     * 扩展属性，变更时推送，仅通讯录应用可获取
     */
    @JacksonXmlElementWrapper(localName = "ExtAttr")
    @JacksonXmlProperty(localName = "Item")
    @JsonProperty("ExtAttr")
    private List<ExtAttr> extAttrs = new ArrayList<>();

    /**
     * 获取变更信息的成员UserID
     * @return the userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     * 设定变更信息的成员UserID
     * @param userID the userID to set
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

    /**
     * 获取成员名称，变更时推送
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * 设定成员名称，变更时推送
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取更新后成员所在部门列表
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * 设定更新后成员所在部门列表
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * 获取手机号码，变更时推送，仅通讯录应用可获取
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设定手机号码，变更时推送，仅通讯录应用可获取
     * @param mobile the mobile to set
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取职位信息。长度为0~64个字节，仅通讯录管理应用可获取
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * 设定职位信息。长度为0~64个字节，仅通讯录管理应用可获取
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * 获取性别，变更时推送。1表示男性，2表示女性
     * @return the gender
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * 设定性别，变更时推送。1表示男性，2表示女性
     * @param gender the gender to set
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * 获取邮箱，变更时推送 ，仅通讯录应用可获取
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设定邮箱，变更时推送 ，仅通讯录应用可获取
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取头像url。注：如果要获取小图将url最后的” 0”改成” 100”即可。变更时推送，仅通讯录管理应用可获取
     * @return the avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 设定头像url。注：如果要获取小图将url最后的” 0”改成” 100”即可。变更时推送，仅通讯录管理应用可获取
     * @param avatar the avatar to set
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 获取激活状态：1=激活或关注， 2=禁用， 4=未激活（重新启用未激活用户或者退出企业并且取消关注时触发）
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 设定激活状态：1=激活或关注， 2=禁用， 4=未激活（重新启用未激活用户或者退出企业并且取消关注时触发）
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 获取英文名
     * @return the englishName
     */
    public String getEnglishName() {
        return englishName;
    }

    /**
     * 设定英文名
     * @param englishName the englishName to set
     */
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    /**
     * 获取上级字段，标识是否为上级。0表示普通成员，1表示上级。仅通讯录管理应用可获取
     * @return the isLeader
     */
    public Integer getIsLeader() {
        return isLeader;
    }

    /**
     * 设定上级字段，标识是否为上级。0表示普通成员，1表示上级。仅通讯录管理应用可获取
     * @param isLeader the isLeader to set
     */
    public void setIsLeader(Integer isLeader) {
        this.isLeader = isLeader;
    }

    /**
     * 获取座机，仅通讯录应用可获取
     * @return the telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * 设定座机，仅通讯录应用可获取
     * @param telephone the telephone to set
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * 获取扩展属性，变更时推送，仅通讯录应用可获取
     * @return the extAttrs
     */
    public List<ExtAttr> getExtAttrs() {
        return extAttrs;
    }

    /**
     * 设定扩展属性，变更时推送，仅通讯录应用可获取
     * @param extAttrs the extAttrs to set
     */
    public void setExtAttrs(List<ExtAttr> extAttrs) {
        this.extAttrs = extAttrs;
    }

}
