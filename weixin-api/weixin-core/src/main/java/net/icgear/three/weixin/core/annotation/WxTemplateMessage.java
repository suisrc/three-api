package net.icgear.three.weixin.core.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 微信模版消息内容
 * 
 * @author Y13
 *
 */
@Target(TYPE)
@Retention(RUNTIME)
public @interface WxTemplateMessage {

  /**
   * template_id
   */
  String id() default "";

  /**
   * template_id_short
   */
  String value() default "";

}
