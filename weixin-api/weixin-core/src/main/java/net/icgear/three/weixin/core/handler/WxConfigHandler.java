package net.icgear.three.weixin.core.handler;

import net.icgear.three.weixin.core.WxConfig;

/**
 * 微信配置获取助手
 * 
 * 只有在使用多配置信息截获器时候，该内容才有效
 * 
 * @author Y13
 *
 */
public interface WxConfigHandler {

  /**
   * 获取微信配置信息
   * 
   * @param appKey
   * @return
   */
  WxConfig findByAppKey(String appKey);

  /**
   * 清除微信配置信息
   */
  default void clearByAppKey(String appKey) {};

}
