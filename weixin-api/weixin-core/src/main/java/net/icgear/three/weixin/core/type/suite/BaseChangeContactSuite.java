package net.icgear.three.weixin.core.type.suite;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import net.icgear.three.weixin.core.annotation.WxInfoType;

/**
 * 变更基础类
 * 
 * @author Y13
 *
 */
@WxInfoType("change_contact")
public class BaseChangeContactSuite extends BaseInfoSuite {

    /**
     * 授权企业的CorpID
     */
    @JacksonXmlProperty(localName = "AuthCorpId")
    @JsonProperty("AuthCorpId")
    private String authCorpId;

    /**
     * 修改类型
     */
    @JacksonXmlProperty(localName = "ChangeType")
    @JsonProperty("ChangeType")
    private String changeType;

    /**
     * 获取授权企业的CorpID
     * @return the authCorpId
     */
    public String getAuthCorpId() {
        return authCorpId;
    }

    /**
     * 设定授权企业的CorpID
     * @param authCorpId the authCorpId to set
     */
    public void setAuthCorpId(String authCorpId) {
        this.authCorpId = authCorpId;
    }

    /**
     * 获取修改类型
     * @return the changeType
     */
    public String getChangeType() {
        return changeType;
    }

    /**
     * 设定修改类型
     * @param changeType the changeType to set
     */
    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

}
