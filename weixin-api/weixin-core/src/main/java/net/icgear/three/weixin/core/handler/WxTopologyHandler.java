package net.icgear.three.weixin.core.handler;

import java.util.List;

import com.suisrc.jaxrsapi.core.token.TokenReference;

/**
 * 集群控制器
 * 
 * @author Y13
 *
 */
public interface WxTopologyHandler {

  /**
   * 更新拓扑中的token
   * 
   * @param key
   * @param accessToken
   * @return
   */
  TokenReference putTokenByTopology(String tokenKey, TokenReference token);

  /**
   * 获取拓扑中的token
   * 
   * @param key
   * @return
   */
  TokenReference getTokenByTopology(String tokenKey);

  /**
   * 获取拓扑中的token
   * 
   * @param key
   * @return
   */
  List<TokenReference> getTokensByTopology();

  /**
   * 检测拓扑网络中是否有人正在修改token
   * 
   * @return
   */
  boolean hasUpdateTokenByTopology(String tokenKey);

  /**
   * 标记拓扑网络，当前环境正在修改token
   * 
   * 当执行set操作后，应该立即解锁拓扑网络中的标记
   * 
   * @return ture 标记成功， false 标记失败，当前有其他环境正在修改token
   */
  boolean lockUpdateTokenByTopology(String tokenKey);

  /**
   * 解锁拓扑网络中对token更新的锁定
   * 
   * @return
   */
  void unlockUpdateTokenByTopology(String tokenKey);
}
