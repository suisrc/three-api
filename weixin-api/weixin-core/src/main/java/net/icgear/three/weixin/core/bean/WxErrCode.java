package net.icgear.three.weixin.core.bean;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

/**
 * 异常获取，最好所有的返回值都集成该对象，这样对于异常的返回也可以正常处理 判断异常只需要返回值，判断errcode是否为null即可
 * 
 * @author Y13
 *
 */
@JsonInclude(Include.NON_NULL)
@JacksonXmlRootElement(localName = "xml")
public class WxErrCode implements Serializable {
  private static final long serialVersionUID = 1237064203482294732L;

  /**
   * 返回码
   */
  private Long errcode = null;

  /**
   * 对返回码的文本描述内容
   */
  private String errmsg = null;


  /**
   * 未知属性列表
   */
  private Map<String, Object> others = null;

  public Long getErrcode() {
    return errcode;
  }

  public void setErrcode(Long errcode) {
    this.errcode = errcode;
  }

  public String getErrmsg() {
    return errmsg;
  }

  public void setErrmsg(String errmsg) {
    this.errmsg = errmsg;
  }


  /**
   * 设定未知属性
   * 
   * @param name
   * @param value
   */
  @JsonAnySetter
  public void other(String name, Object value) {
    others.put(name, value);
  }

  /**
   * 获取未知属性列表
   * 
   * @return
   */
  public Map<String, Object> other() {
    return others;
  }

  /**
   * 是否有异常
   */
  @JsonIgnore
  public boolean isErr() {
    return errcode != null && !errcode.equals(0L);
  }

  /**
   * 打印异常
   */
  @JsonIgnore
  public String printErr() {
    return errcode + ":" + errmsg;
  }
}
