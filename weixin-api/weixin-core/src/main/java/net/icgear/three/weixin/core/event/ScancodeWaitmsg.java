package net.icgear.three.weixin.core.event;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxEvent;

/**
 * 扫码推事件且弹出“消息接收中”提示框的事件推送
 * 
 * <Event><![CDATA[scancode_waitmsg]]></Event>
 * <EventKey><![CDATA[6]]></EventKey>
 * <ScanCodeInfo>
 *   <ScanType><![CDATA[qrcode]]></ScanType>
 *   <ScanResult><![CDATA[2]]></ScanResult>
 * </ScanCodeInfo>
 * 
 * @author Y13
 *
 */
@WxEvent(value="scancode_waitmsg")
@JacksonXmlRootElement(localName="xml")
public class ScancodeWaitmsg extends ScancodePushEvent {

}
