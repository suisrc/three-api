package net.icgear.three.weixin.core.event;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxEvent;
import net.icgear.three.weixin.core.asserts.TypeTrueAssert;
import net.icgear.three.weixin.core.type.msg.WxEventMessage;

/**
 * 未知事件类型
 * @author Y13
 *
 */
@WxEvent(value = "", priority = "x", handler = TypeTrueAssert.class)
@JacksonXmlRootElement(localName="xml")
public class UnknowEvent extends WxEventMessage {

    /**
     * 未知属性列表
     */
    private Map<String, Object> other = new HashMap<>();
    
    /**
     * 设定未知属性
     * @param name
     * @param value
     */
    @JsonAnySetter
    public void other(String name,Object value) {  
        other.put(name,value);  
    }
    
    /**
     * 获取未知属性列表
     * @return
     */
    public Map<String, Object> other() {
        return other;
    }
}
