package net.icgear.three.weixin.core.suite;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxInfoType;
import net.icgear.three.weixin.core.type.suite.BaseInfoSuite;

/**
 * 授权成功通知
 * 
 * 从企业微信应用市场发起授权时，企业微信后台会推送授权成功通知。
 * 
 * 从第三方服务商网站发起的应用授权流程，由于授权完成时会跳转第三方服务商管理后台，因此不会通过此接口向第三方服务商推送授权成功通知。
 * 
 * <InfoType><![CDATA[create_auth]]></InfoType>
 * 
 *  <AuthCode><![CDATA[AUTHCODE]]></AuthCode>
 * @author Y13
 *
 */
@WxInfoType("create_auth")
@JacksonXmlRootElement(localName="xml")
public class CreateAuthSuite extends BaseInfoSuite {

    /**
     * 授权的auth_code,最长为512字节。用于获取企业的永久授权码
     */
    @JacksonXmlCData
    @JacksonXmlProperty(localName = "AuthCode")
    @JsonProperty("AuthCode")
    private String authCode;

    /**
     * 获取授权的auth_code,最长为512字节。用于获取企业的永久授权码
     * @return the authCode
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * 设定授权的auth_code,最长为512字节。用于获取企业的永久授权码
     * @param authCode the authCode to set
     */
    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

}
