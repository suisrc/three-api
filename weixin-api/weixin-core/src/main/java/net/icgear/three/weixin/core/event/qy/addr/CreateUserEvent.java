package net.icgear.three.weixin.core.event.qy.addr;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.type.msg.BaseUserChangeContactEvent;

/**
 * 新增成员事件
 * 
 * @author Y13
 *
 */
@WxChangeType("create_user")
@JacksonXmlRootElement(localName="xml")
public class CreateUserEvent extends BaseUserChangeContactEvent {
}
