package net.icgear.three.weixin.core.suite.addr;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.type.suite.BaseChangeContactSuite;

/**
 * 删除部门事件
 * 
 * @author Y13
 *
 */
@WxChangeType("delete_party")
@JacksonXmlRootElement(localName="xml")
public class DeletePartySuite extends BaseChangeContactSuite {

    /**
     * 部门Id
     */
    @JacksonXmlProperty(localName = "Id")
    @JsonProperty("Id")
    private String id;

    /**
     * 获取部门Id
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * 设定部门Id
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

}
