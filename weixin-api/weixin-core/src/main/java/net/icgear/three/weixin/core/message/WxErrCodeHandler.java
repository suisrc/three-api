  package net.icgear.three.weixin.core.message;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;

import com.suisrc.core.message.AbstractErrCodeHandler;

/**
 * 默认异常提供器
 * 
 * 使用该接口使用，请使用继承方式，保证异常提供器都可以正常加载
 * 
 * @author Y13
 *
 */
@Priority(102)
@ApplicationScoped
public class WxErrCodeHandler extends AbstractErrCodeHandler {
}
