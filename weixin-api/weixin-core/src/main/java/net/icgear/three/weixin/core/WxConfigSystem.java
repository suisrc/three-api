package net.icgear.three.weixin.core;

import java.util.LinkedHashMap;
import java.util.Map;

import com.suisrc.core.annotation.ScConfig;
import com.suisrc.core.utils.CoreUtils;

/**
 * 微信配置接口
 * 
 * @author Y13
 *
 */
public class WxConfigSystem extends WxConfigCache implements WxConfig {

  private String uniqueKey;

  @ScConfig("name")
  private String nodeId;

  @ScConfig("desc")
  private String description;

  @ScConfig("id")
  private String appId;

  @ScConfig("suite-id")
  private String suiteId;

  @ScConfig("agent-id")
  private Integer agentId;

  @ScConfig("secret")
  private String appSecret;

  @ScConfig("token")
  private String token;

  @ScConfig("encoding-key")
  private String encodingKey;

  @ScConfig("encrypt")
  private boolean isEncrypt;

  @ScConfig("web-domain")
  private String webDomain;

  @ScConfig("web-index")
  private String webIndex;

  @ScConfig("web-indexs.*")
  private Map<String, String> webIndexs;

  @ScConfig("industry-id.*")
  private String[] industryIds;

  @ScConfig("industry-edit")
  private boolean industryEdit;

  /**
   * 构造方法
   */
  public WxConfigSystem(String uniqueKey) {
    this.uniqueKey = uniqueKey;
    doPostConstruct();
  }

  /**
   * 获取配置的前缀
   */
  protected String geConfigKey(String key) {
    return WxConsts.WEIXIN_PRE + getAppUniqueKey() + "." + key;
  }

  /**
   * 执行单配置初始化
   */
  @SuppressWarnings("rawtypes")
  protected void doPostConstruct() {
    CoreUtils.setConfigProperty(this, (config, type) -> {
      String key = geConfigKey(config.value());
      Class clazz = type;
      if (clazz == String.class) {
        if (config.defaultValue().isEmpty()) {
          return System.getProperty(key);
        } else {
          return System.getProperty(key, config.defaultValue());
        }
      } else if (clazz == boolean.class) {
        if (config.defaultValue().isEmpty()) {
          return Boolean.getBoolean(key);
        } else {
          String value = System.getProperty(key, config.defaultValue());
          return Boolean.valueOf(value);
        }
      } else if (clazz == String[].class) {
        return CoreUtils.getSystemProperty2Array(key);
      } else if (clazz == Integer.class) {
        return Integer.getInteger(key);
      } else if (clazz == Map.class) {
        return CoreUtils.getSystemProperty2Map(key, LinkedHashMap::new);
      }
      return null;
    });
  }

  /**
   * 服务节点信息
   */
  public String getWxNodeId() {
    return nodeId;
  }

  /**
   * 服务器节点描述
   * 
   * @return
   */
  public String getDescription() {
    return description;
  }

  /**
   * 唯一key的内容
   */
  public String getAppUniqueKey() {
    return uniqueKey;
  }

  /**
   * 微信应用ID
   */
  public String getWxAppId() {
    return appId;
  }

  /**
   * 第三方应用ID
   * 
   * 只有企业号有效，否则返回null
   */
  public String getWxSuiteId() {
    return suiteId;
  }

  /**
   * 企业自建应用ID
   */
  public Integer getAgentId() {
    return agentId;
  }

  /**
   * 微信应用秘钥
   */
  public String getWxAppSecret() {
    return appSecret;
  }

  /**
   * 微信回调加密令牌
   */
  public String getWxToken() {
    return token;
  }

  /**
   * 微信回调加密秘钥
   */
  public String getWxEncodingKey() {
    return encodingKey;
  }

  /**
   * 回调消息是否需要加密
   * 
   * @return
   */
  public boolean isWxEncrypt() {
    return isEncrypt;
  }

  /**
   * 加密方式
   * 
   * @return
   */
  public String getEncryptType() {
    return WxConsts.ENCRYPT_TYPE_AES;
  }

  /**
   * 当前服务器域名
   */
  @Override
  public String getWebDomain() {
    return webDomain;
  }

  /**
   * 服务器首页
   */
  public String getWebIndex() {
    return webIndex;
  }

  /**
   * 服务器跳转页面
   */
  public String getWebIndex(String key) {
    if (key == null) {
      return getWebIndex();
    }
    return webIndexs == null ? null : webIndexs.get(key);
  }

  /**
   * 所属行业编码
   * 
   * @return
   */
  public String[] getIndustryIds() {
    return industryIds;
  }

  /**
   * 修改行业编码的行为，只允许访问一次
   * 
   * 如果需要重复访问，可以清空用于配置后再次访问
   */
  @Override
  public boolean isIndustryEdit() {
    if (industryEdit) {
      // 只允许访问一次
      industryEdit = false;
      return true;
    } else {
      return false;
    }
  }

  // --------------------------------------以下方法不参与计算

  /**
   * 获取访问令牌
   */
  @Deprecated
  public String getWxAccessToken() {
    return null;
  }

  /**
   * 清除微信访问令牌
   */
  @Deprecated
  public void clearWxAccessToken() {
    // do nothing
  }

  /**
   * 推送配置信息
   * 
   * @param key
   * @param value
   */
  @Deprecated
  public <T> void setAdapter(String named, Class<T> type, T value) {
    // do nothing
  }

  /**
   * 获取配置属性
   */
  @Deprecated
  public <T> T getAdapter(String named, Class<T> type) {
    return null;
  }
}
