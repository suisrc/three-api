package net.icgear.three.weixin.core.oauth2;

import javax.ws.rs.QueryParam;

/**
 * 
 * <p> 微信服务器要授权信息的令牌
 * 
 * <p> 重定向类型type: <br>空:网页授权登录 <br>wqr: 企业扫描登录 <br>cqr: 普通扫描登录
 * 
 * <p> 其他参数 <br> k:AppId
 * 
 * @author Y13
 *
 */
public class WxAuthParams {

  /**
   * 是否执行人员身份信息同步
   */
  @QueryParam("sync")
  private boolean isSync;

  /**
   * 重定向跳转使用的key
   * 
   * 如果为"none"禁止跳转
   */
  @QueryParam("key")
  private String key;

  /**
   * 验证授权使用的key
   * 
   * snsapi_base：静默授权，可获取成员的的基础信息（UserId与DeviceId）； snsapi_userinfo：静默授权，可获取成员的详细信息，但不包含手机、邮箱；
   * snsapi_privateinfo：手动授权，可获取成员的详细信息，包含手机、邮箱；
   * 
   * 注意：企业自建应用可以根据userid获取成员详情，无需使用snsapi_userinfo和snsapi_privateinfo两种scope。
   * https://work.weixin.qq.com/api/doc#10028/scope%E7%9A%84%E7%89%B9%E6%AE%8A%E6%83%85%E5%86%B5
   * 
   * three_xxx: 自建api，用户通知系统可以使用企业api接口获取用户信息
   */
  @QueryParam("scope")
  private String scope;

  public boolean isSync() {
    return isSync;
  }

  public void setSync(boolean isSync) {
    this.isSync = isSync;
  }

  public String getKey() {
    return key;
  }

  public void setKey(String key) {
    this.key = key;
  }

  public String getScope() {
    return scope;
  }

  public void setScope(String scope) {
    this.scope = scope;
  }

}
