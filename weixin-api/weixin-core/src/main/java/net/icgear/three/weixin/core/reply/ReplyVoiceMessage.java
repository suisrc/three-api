package net.icgear.three.weixin.core.reply;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.media.MediaId;
import net.icgear.three.weixin.core.type.msg.BaseMessage;

/**
 * 语音
 * <Voice>
 * <MediaId><![CDATA[media_id]]></MediaId>
 * </Voice>
 */
@JacksonXmlRootElement(localName = "xml")
public class ReplyVoiceMessage extends BaseMessage {


    /**
     * 语音信息 通过素材管理中的接口上传多媒体文件，得到的id 必须
     */
    @JacksonXmlProperty(localName = "Voice")
    @JsonProperty("Voice")
    private MediaId voice;

    public ReplyVoiceMessage() {
        setMsgType("voice");
        setSystemToCreateTime();
    }

    /**
     * 获取语音信息 通过素材管理中的接口上传多媒体文件，得到的id 必须
     * @return the voice
     */
    public MediaId getVoice() {
        return voice;
    }

    /**
     * 设定语音信息 通过素材管理中的接口上传多媒体文件，得到的id 必须
     * @param voice the voice to set
     */
    public void setVoice(MediaId voice) {
        this.voice = voice;
    }

}
