package net.icgear.three.weixin.core.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;

/**
 * 具有排序功能的集合
 * 
 * @author Y13
 *
 * @param <E>
 */
public class SortedList<E> extends ArrayList<E> {
  private static final long serialVersionUID = 1L;

  /**
   * 排序控制器
   */
  private Comparator<? super E> sortor = null;

  public SortedList() {}

  public SortedList(Comparator<? super E> sortor) {
    this.sortor = sortor;
  }

  public SortedList<E> sortor(Comparator<? super E> sortor) {
    this.sortor = sortor;
    return this;
  }

  @Override
  public boolean add(E e) {
    if (sortor == null || isEmpty() || sortor.compare(e, get(size() - 1)) >= 0) {
      return super.add(e);
    }
    int index = 0;
    for (; index < size() && sortor.compare(e, get(index)) <= 0; index++);
    add(index, e);
    return true;
  }

  @Override
  public boolean addAll(Collection<? extends E> c) {
    c.forEach(this::add);
    return true;
  }

}
