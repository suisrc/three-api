package net.icgear.three.weixin.core.reply;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.media.MediaInfo;
import net.icgear.three.weixin.core.type.msg.BaseMessage;

/**
 * 视频
 * <Video>
 * <MediaId><![CDATA[media_id]]></MediaId>
 * <Title><![CDATA[title]]></Title>
 * <Description><![CDATA[description]]></Description>
 * </Video>
 */
@JacksonXmlRootElement(localName = "xml")
public class ReplyVideoMessage extends BaseMessage {

    /**
     * 视频信息 必须
     */
    @JacksonXmlProperty(localName = "Video")
    @JsonProperty("Video")
    private MediaInfo video;

    public ReplyVideoMessage() {
        setMsgType("video");
        setSystemToCreateTime();
    }

    /**
     * 获取视频信息 必须
     * @return the video
     */
    public MediaInfo getVideo() {
        return video;
    }

    /**
     * 设定视频信息 必须
     * @param video the video to set
     */
    public void setVideo(MediaInfo video) {
        this.video = video;
    }

}
