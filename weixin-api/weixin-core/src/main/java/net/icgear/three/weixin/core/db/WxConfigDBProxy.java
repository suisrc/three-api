package net.icgear.three.weixin.core.db;

import java.util.concurrent.ScheduledFuture;

import net.icgear.three.weixin.core.WxConfigCache;

/**
 * 配置代理
 * 
 * @author Y13
 *
 */
public class WxConfigDBProxy extends WxConfigCache implements WxConfigDB {

  /**
   * 代理的内容
   */
  private WxConfigDB target;

  /**
   * 备用事件
   */
  private ScheduledFuture<?> future;

  /**
   * 最后使用时间 默认为当前时间
   */
  private long lastUseTime = System.currentTimeMillis();

  public void setTarget(WxConfigDB target) {
    this.target = target;
  }

  public WxConfigDB getTarget() {
    return target;
  }

  /**
   * 每次调用都会更新使用的时间
   * 
   * @return
   */
  public long resetLastUseTime() {
    long time = lastUseTime;
    lastUseTime = System.currentTimeMillis();
    return time;
  }

  /**
   * 查看是否过期，代理的内容是否可用
   * 
   * @return
   */
  public boolean isExpired() {
    if (target == null) {
      return false;
    }
    // 验证是否过期
    return lastUseTime + target.getRefreshTime() * 1000 < System.currentTimeMillis();
  }

  public ScheduledFuture<?> getFuture() {
    return future;
  }

  public void setFuture(ScheduledFuture<?> future) {
    this.future = future;
  }

  @Override
  public String getWxNodeId() {
    return target.getWxNodeId();
  }

  @Override
  public String getAppUniqueKey() {
    return target.getAppUniqueKey();
  }

  @Override
  public String getWxAppId() {
    return target.getWxAppId();
  }

  @Override
  public String getWxSuiteId() {
    return target.getWxSuiteId();
  }

  @Override
  public Integer getAgentId() {
    return target.getAgentId();
  }

  @Override
  public String getWxAppSecret() {
    return target.getWxAppSecret();
  }

  @Override
  public String getWxToken() {
    return target.getWxToken();
  }

  @Override
  public String getWxEncodingKey() {
    return target.getWxEncodingKey();
  }

  @Override
  public boolean isWxEncrypt() {
    return target.isWxEncrypt();
  }

  @Override
  public String[] getIndustryIds() {
    return target.getIndustryIds();
  }

  @Override
  public boolean isIndustryEdit() {
    return target.isIndustryEdit();
  }

  @Override
  public String getEncryptType() {
    return target.getEncryptType();
  }

  public String getWebDomain() {
    return target.getWebDomain();
  }

  public String getWebIndex() {
    return target.getWebIndex();
  }

  public String getWebIndex(String key) {
    return target.getWebIndex(key);
  }

}
