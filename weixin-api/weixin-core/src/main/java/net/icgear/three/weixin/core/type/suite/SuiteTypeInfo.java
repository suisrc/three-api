package net.icgear.three.weixin.core.type.suite;

import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.three.core.msg.IMessage;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.annotation.WxInfoType;
import net.icgear.three.weixin.core.asserts.TypeAssert;
import net.icgear.three.weixin.core.type.AbstractTypeInfo;

/**
 * 消息类型描述
 * 
 * @author Y13
 *
 */
public class SuiteTypeInfo extends AbstractTypeInfo<Class<? extends IMessage>> {

    /**
     * 构建消息类型
     * @param clazz
     * @return
     */
    public static SuiteTypeInfo create(Class<? extends IMessage> clazz) {
        try {
            return new SuiteTypeInfo(clazz);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return null;
        }
    }
    
    /**
     * 优先级
     */
    private String priority;
    
    /**
     * 类别
     */
    private String category;
    
    /**
     * 构造方法
     */
    public SuiteTypeInfo(Class<? extends IMessage> target) {
        super(target);
    }

    /**
     * 初始化
     * 
     * 其中WxInfoType注解是必须有的内容
     * 
     */
    @Override
    protected void initialize() {
        // 消息注解
        WxInfoType infoAnno = this.target.getAnnotation(WxInfoType.class);
        if (infoAnno == null) {
            throw new RuntimeException("无法初始化消息类型，没有@WxInfoType注解标记：" + this.target);
        }
        // 对比的内容
        String value = infoAnno.value();
        // 对比的方式
        TypeAssert hander = ReflectionUtils.newInstance(infoAnno.handler());
        // 对比顺序
        this.priority = infoAnno.priority();
        this.asserts.add(new RefAssert(value, hander));
        this.category = infoAnno.category();

        // 变更事件注解
        WxChangeType changeAnno = this.target.getAnnotation(WxChangeType.class);
        if (changeAnno == null) {
            return;
        }
        value = changeAnno.value();
        hander = ReflectionUtils.newInstance(changeAnno.handler());
        this.priority = changeAnno.priority();
        this.asserts.add(new RefAssert(value, hander));
        if (changeAnno.category().isEmpty()) {
            this.category = changeAnno.category();
        }
    }
    
    /**
     * 优先级
     */
    @Override
    public String getPriority() {
        return priority;
    }
    
    /**
     * 
     * @return
     */
    public String getCategory() {
        return category;
    }

}
