package net.icgear.three.weixin.core.type;

import com.suisrc.three.core.msg.IMessage;
import com.suisrc.three.core.msg.MsgNode;

/**
 * 微信公众号消息类型解析器
 * 
 * @author Y13
 *
 */
public interface WxTypeParser {

  /**
   * 获取消息对应的类型
   * 
   * @param node
   * @return
   */
  Class<? extends IMessage> parser(MsgNode node);

}
