package net.icgear.three.weixin.core.type;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import net.icgear.three.weixin.core.utils.SortedList;

/**
 * 用户构建类型的索引
 * 
 * @author Y13
 *
 */
public abstract class AbstractTypeIndexs<T extends AbstractTypeInfo<V>, V> {

  /**
   * 快速检索MAP
   */
  private Map<String, List<T>> indexs;

  /**
   * 简单构造
   */
  public AbstractTypeIndexs() {
    indexs = new HashMap<>(4);
  }

  /**
   * 索引的构造方法
   * 
   * @param packages
   */
  public AbstractTypeIndexs(Function<V, T> creater, Collection<V> values) {
    indexs = new HashMap<>();
    for (V value : values) {
      T info = creater.apply(value);
      if (info != null) {
        String key = info.getArchiveKey();
        List<T> infos = indexs.get(key);
        if (infos == null) {
          SortedList<T> c = new SortedList<>((l, r) -> l.getPriority().compareTo(r.getPriority()));
          indexs.put(key, infos = c);
        }
        infos.add(info);
      }
    }
    // 初始化
    initialize();
  }

  /**
   * 初始化
   */
  protected void initialize() {}

  /**
   * 搜索第一匹配的类型
   * 
   * 第一匹配类型需要全匹配，和排序在第一位
   * 
   * @param targets
   * @return
   */
  public V searchFirstV(String... targets) {
    if (targets == null || targets.length == 0) {
      return null;
    }
    String[] keys = AbstractTypeInfo.getIdentity(targets);
    SortedList<T> selector = new SortedList<>((l, r) -> l.getPriority().compareTo(r.getPriority()));
    for (String key : keys) {
      List<T> set = indexs.get(key);
      if (set != null) {
        selector.addAll(set);
      }
    }
    // 执行优先级排序
    for (T t : selector) {
      if (t.matches(targets)) {
        return t.getTarget();
      }
    }
    return null;
  }

}
