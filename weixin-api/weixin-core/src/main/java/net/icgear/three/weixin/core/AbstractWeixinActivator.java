package net.icgear.three.weixin.core;

import java.net.URI;

import javax.annotation.PostConstruct;
import javax.ws.rs.container.ContainerRequestContext;

import net.icgear.three.weixin.core.handler.CallbackActivatorHandler;
import net.icgear.three.weixin.core.handler.WxConfigHandler;
import net.icgear.three.weixin.core.handler.WxTemplateHandler;
import net.icgear.three.weixin.core.utils.WxUtils;

/**
 * 程序入口配置抽象
 * 
 * @author Y13
 */
public abstract class AbstractWeixinActivator extends AbstractCommonActivator implements WxConfig {

  /**
   * 系统默认但配置信息
   */
  private WxConfig wxConfig = null;

  /**
   * 微信配置信息句柄
   */
  private WxConfigHandler wxConfigHandler;

  /**
   * 微信模版信息句柄
   */
  private WxTemplateHandler wxTemplateHandler;

  // --------------------------------------------重写内容

  /**
   * 生成微信配置信息内容
   * 
   * @return
   */
  protected WxConfigHandler createWxConfigHandler() {
    return null;
  }

  /**
   * 生成模版配置信息内容
   */
  // @Named("weixin-template-default")
  protected WxTemplateHandler createWxTemplateHandler() {
    // try {
    // return CdiUtils.selectWithQualifier(WxTemplateHandler.class);
    // } catch (Exception e) {
    // logger.warning("inject WxTemplateHandler by 'weixin-template-default' is error: " +
    // e.getMessage());
    // return null;
    // }
    // 默认不使用模版
    return null;
  }


  /**
   * 获取模版消息ID
   * 
   * @param shortId
   * @param ifNotGetByWx 如果没有，从微信服务器获取
   * @return
   */
  protected String getTemplateMessageId(String shortId, boolean ifNotGetByWx) {
    if (wxTemplateHandler == null) {
      return null;
    }
    String templateId = wxTemplateHandler.findByShortId(shortId);
    if (templateId != null || !ifNotGetByWx) {
      return templateId;
    }
    templateId = getTemplateIdFromWeixin(shortId);
    if (templateId != null) {
      wxTemplateHandler.save(shortId, templateId);
    }
    return templateId;
  }

  /**
   * 通过微信服务器获取模版ID
   * 
   * 如果可能，需要增加到私有库中
   * 
   * @param shortId
   * @return
   */
  protected String getTemplateIdFromWeixin(String shortId) {
    return null;
  }

  /**
   * 构造方法
   */
  @Override
  @PostConstruct
  public void doPostConstruct() {
    // 继续实现构造
    super.doPostConstruct();
    // 微信外挂配置句柄
    wxConfigHandler = createWxConfigHandler();
    if (wxConfigHandler != null && wxConfigHandler instanceof CallbackActivatorHandler) {
      ((CallbackActivatorHandler) wxConfigHandler).setActivator(this);
    }
    wxTemplateHandler = createWxTemplateHandler();
    if (wxTemplateHandler != null && wxTemplateHandler instanceof CallbackActivatorHandler) {
      ((CallbackActivatorHandler) wxTemplateHandler).setActivator(this);
    }
  }

  /**
   * 适配器
   */
  @SuppressWarnings("unchecked")
  @Override
  public <T> T getAdapter(String key, Class<T> type) {
    if (key != null && type == String.class) {
      switch (key) {
        case WxConsts.ACCESS_TOKEN:
          return (T) getWxAccessToken();
        case WxConsts.APP_ID:
          return (T) getWxAppId();
        case WxConsts.APP_SUITE_ID:
          return (T) getWxSuiteId();
        case WxConsts.APP_SECRET:
          return (T) getWxAppSecret();
        case WxConsts.APP_TOKEN:
          return (T) getWxToken();
        case WxConsts.APP_ENCODING_KEY:
          return (T) getWxEncodingKey();
        case WxConsts.APP_OPEN_ID:
          String id = getWxSuiteId();
          return (T) (id != null ? id : getWxAppId());
        case WxConsts.LOCAL_DOMAIN:
          return (T) getWebDomain();
        case WxConsts.REQUEST_APP_KEY:
          return (T) getRequestAppKey();
      }
      if (key.startsWith(WxConsts.FIND_TEMPLATE_ID_BY2)) {
        String shortId = key.substring(WxConsts.FIND_TEMPLATE_ID_BY2.length());
        if (shortId.isEmpty()) {
          // 无法查找
          return null;
        }
        return (T) getTemplateMessageId(shortId, true);
      }
    }
    if (key != null && type == Integer.class) {
      if (key.equals(WxConsts.APP_AGENT_ID)) {
        return (T) getAgentId();
      }
    }

    return super.getAdapter(key, type);
  }

  // --------------------------------------------WxConfig

  /**
   * 获取服务器节点信息
   */
  @Override
  public String getWxNodeId() {
    return getAppName();
  }

  /**
   * 加密方式
   */
  @Override
  public String getEncryptType() {
    WxConfig wc = getWxConfig();
    return wc != null && wc.getEncryptType() != null ? wc.getEncryptType() : WxConsts.ENCRYPT_TYPE_AES;
  }

  /**
   * 获取应用ID
   */
  @Override
  public String getWxAppId() {
    WxConfig wc = getWxConfig();
    return wc == null ? null : wc.getWxAppId();
  }

  /**
   * 企业第三方应用ID
   */
  @Override
  public String getWxSuiteId() {
    WxConfig wc = getWxConfig();
    return wc == null ? null : wc.getWxSuiteId();
  }

  /**
   * 企业自建应用ID
   */
  @Override
  public Integer getAgentId() {
    WxConfig wc = getWxConfig();
    return wc == null ? null : wc.getAgentId();
  }

  /**
   * 获取应用秘钥
   */
  @Override
  public String getWxAppSecret() {
    WxConfig wc = getWxConfig();
    return wc == null ? null : wc.getWxAppSecret();
  }

  /**
   * 获取微信加密令牌
   */
  @Override
  public String getWxToken() {
    WxConfig wc = getWxConfig();
    return wc == null ? null : wc.getWxToken();
  }

  /**
   * 获取微信加密秘钥
   */
  @Override
  public String getWxEncodingKey() {
    WxConfig wc = getWxConfig();
    return wc == null ? null : wc.getWxEncodingKey();
  }

  /**
   * 微信回调内容是否加密
   */
  @Override
  public boolean isWxEncrypt() {
    WxConfig wc = getWxConfig();
    return wc == null ? true : wc.isWxEncrypt();
  }

  /**
   * 归属行业编码
   */
  @Override
  public String[] getIndustryIds() {
    WxConfig wc = getWxConfig();
    return wc == null ? null : wc.getIndustryIds();
  }

  /**
   * 是否可以修改行业编码
   */
  @Override
  public boolean isIndustryEdit() {
    WxConfig wc = getWxConfig();
    return wc == null ? null : wc.isIndustryEdit();
  }

  /**
   * 当前服务的域名
   */
  @Override
  public String getWebDomain() {
    WxConfig wc = getWxConfig();
    return wc == null ? null : wc.getWebDomain();
  }

  /**
   * 获取激活器绑定的微信首页地址
   */
  public String getWebIndex() {
    WxConfig wc = getWxConfig();
    String indexUrl;
    if (wc == null || (indexUrl = wc.getWebIndex()) == null) {
      // 不进行处理
      return null;
    }
    return getIndexUrlByDomain(wc, indexUrl);
  }

  /**
   * 获取激活器绑定的微信地址
   */
  @Override
  public String getWebIndex(String key) {
    WxConfig wc = getWxConfig();
    String indexUrl;
    if (wc == null || (indexUrl = wc.getWebIndex(key)) == null) {
      // 不进行处理
      return null;
    }
    return getIndexUrlByDomain(wc, indexUrl);
  }

  /**
   * 返回地址，同时增加域信息
   * 
   * @param wc
   * @param indexUrl
   * @return
   */
  private String getIndexUrlByDomain(WxConfig wc, String indexUrl) {
    if (!indexUrl.isEmpty()) {
      String urlLower = indexUrl.toLowerCase();
      if (urlLower.startsWith("http://") || urlLower.startsWith("https://")) {
        // 系统指定的全路径
        return indexUrl;
      }
    }
    String indexUrl0 = wc.getWebDomain();
    if (indexUrl0 == null) {
      // 从访问的url中获取
      ContainerRequestContext crc = WxUtils.getContainerRequestContext();
      if (crc == null) {
        throw new RuntimeException("The thread's cache has no container request context!");
      }
      URI uri = crc.getUriInfo().getAbsolutePath();
      indexUrl0 = WxUtils.getHostPortFromURI(uri);
    }
    return indexUrl0 + indexUrl;
  }

  /**
   * 获取微信访问令牌
   */
  @Override
  public String getWxAccessToken() {
    return getToken();
  }

  /**
   * 清空微信访问令牌
   */
  @Override
  public void clearWxAccessToken() {
    clearToken(false);
  }

  /**
   * 查询令牌元
   */
  @Override
  public TokenAtom findTokenAtom(String tokenKey) {
    WxConfig wc = getWxConfig();
    return wc == null ? null : wc.findTokenAtom(tokenKey);
  }

  /**
   * 保存令牌元
   */
  @Override
  public TokenAtom saveTokenAtom(String tokenKey, TokenAtom tokenAtom) {
    WxConfig wc = getWxConfig();
    return wc == null ? tokenAtom : wc.saveTokenAtom(tokenKey, tokenAtom);
  }

  /**
   * 令牌缓存名称
   */
  @Override
  protected String getTokenKeyByCache() {
    // return wxConfigHandler == null ? "" : getRequestAppKey();
    return "";
  }

  /**
   * 移除
   */
  @Override
  public void clearConfig(boolean remove) {
    WxConfig wc = getWxConfig();
    if (wc == null) {
      return;
    }
    wc.clearConfig(remove);
    if (remove) {
      if (wxConfigHandler == null) {
        wxConfig = null;
      } else {
        String appKey = getRequestAppKey();
        wxConfigHandler.clearByAppKey(appKey);
      }
    }
  }

  // --------------------------------------------WxConfig
  public String getRequestAppKey() {
    if (wxConfigHandler == null) {
      // 系统是单服务，没有appkey概念
      return null;
    } else {
      return WxUtils.getAppKey();
    }
  }

  /**
   * 验证请求key是否有效
   * 
   * @return
   */
  public boolean checkRequestAppKey() {
    String appKey = WxUtils.getAppKey();
    if (wxConfigHandler == null) {
      // 系统是单服务，没有appkey概念
      return appKey == null;
    } else {
      return appKey != null && wxConfigHandler.findByAppKey(appKey) != null;
    }
  }

  /**
   * 获取WxConfig内容
   * 
   * @return
   */
  protected WxConfig getWxConfig() {
    if (wxConfigHandler == null) {
      if (wxConfig == null) {
        wxConfig = new WxConfigSystem(getAppUniqueKey() + ".endpoint");
      }
      return wxConfig;
    }
    String appKey = getRequestAppKey();
    if (appKey == null) {
      return null;
    }
    WxConfig config = wxConfigHandler.findByAppKey(appKey);
    if (config == null) {
      throw new RuntimeException("没有找到可用的配置信息:" + appKey);
    }
    return config;
  }


  // --------------------------------------------TempFile

  @Override
  protected String getTempFileNameByKey(String filename, String key) {
    if (key == null || key.isEmpty()) {
      return super.getTempFileNameByKey(filename, key);
    }
    String appKey = getRequestAppKey();
    String tempname;
    if (appKey == null) {
      tempname = super.getTempFileNameByKey(filename, key);
    } else {
      tempname = super.getTempFileNameByKey(filename, appKey + "-" + key);
    }
    return tempname;
  }

}
