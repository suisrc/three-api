package net.icgear.three.weixin.core.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * 微信模版消息内容
 * 
 * @author Y13
 *
 */
@Target({FIELD, METHOD})
@Retention(RUNTIME)
public @interface WxTemplateProperty {

  /**
   * 值对应的关键字名称
   * 
   * 绑定值必须有get方法（boolean类型是is方法）
   * 
   * 默认同属性名称，如果是方法，同方法名首字母小写
   * 
   * @return
   */
  String value() default "";

  /**
   * 颜色，默认为空
   * 
   * @return
   */
  String color() default "";

  /**
   * 排序
   * 
   * 预留字段，用于格式化json数据时候，属性的顺序
   * 
   * 暂时为开发，所以没有意义
   */
  int order() default 1024;

}
