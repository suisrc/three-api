package net.icgear.three.weixin.core.asserts;

/**
 * 起始断言
 * 
 * @author Y13
 *
 */
public class TypeStartsWithAssert implements TypeAssert {

  /**
   * 执行assert
   * 
   * @param type
   * @return
   */
  public boolean apply(String source, String target) {
    return source != null && source.startsWith(target);
  }

}
