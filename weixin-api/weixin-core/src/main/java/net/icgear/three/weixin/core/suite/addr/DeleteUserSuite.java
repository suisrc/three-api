package net.icgear.three.weixin.core.suite.addr;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import net.icgear.three.weixin.core.annotation.WxChangeType;
import net.icgear.three.weixin.core.type.suite.BaseChangeContactSuite;

/**
 * 删除成员事件
 * 
 * @author Y13
 *
 */
@WxChangeType("delete_user")
@JacksonXmlRootElement(localName="xml")
public class DeleteUserSuite extends BaseChangeContactSuite {

    /**
     * 变更信息的成员UserID
     */
    @JacksonXmlProperty(localName = "UserID")
    @JsonProperty("UserID")
    private String userID;

    /**
     * 获取变更信息的成员UserID
     * @return the userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     * 设定变更信息的成员UserID
     * @param userID the userID to set
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }

}
