package net.icgear.three.weixin.spapi;

import java.util.Set;

import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.three.weixin.core.AbstractWeixinActivator;
import net.icgear.three.weixin.core.handler.WxTokenHandler;
import net.icgear.three.weixin.spapi.rest.api.SpTokenServiceRest;

/**
 * 对于本公司而已，存放了本公司信息直接访问的激活器
 * 
 * @author Y13
 */
public abstract class SpActivator extends AbstractWeixinActivator {

    @Override
    public Set<Class<?>> getClasses() {
        return ReflectionUtils.getTypesAnnotatedWith(RemoteApi.class, true, SpTokenServiceRest.class.getPackage().getName());
    }

    /**
     * 获取默认基础链接地址
     */
    @Override
    protected String getDefaultBaseUrl() {
        return "https://api.weixin.qq.com";
    }
    
    private SpTokenServiceRest tokenRest;

    /**
     * 获取服务控制器
     * @return
     */
    public SpTokenServiceRest getAccessTokenRest() {
        if (tokenRest == null) {
            tokenRest = createTokenRest();
        }
        return tokenRest;
    }
    
    /**
     * 获取访问控制器
     * @return
     */
    protected SpTokenServiceRest createTokenRest() {
        return getApiImplement(SpTokenServiceRest.class);
    }
    
    /**
     * 获取访问令牌控制器
     */
    @Override
    protected WxTokenHandler getWxTokenHandler(String tokenKey) {
        // 对于本公司而已，其中tokenKey可以忽略
        return () -> getAccessTokenRest().getToken(getWxAppId(), getWxAppSecret());
    }
}
