package net.icgear.three.weixin.spapi.retry;

import com.suisrc.jaxrsapi.core.ApiActivator;

import net.icgear.three.weixin.core.filter.WxTokenReTryFilter;

/**
 * 对微信中的Access Token 有效性记性验证 系统很容易形成恶性竞争，所以请谨慎使用。
 * 
 * @author Y13
 *
 */
public class WxAccessTokenReTryFilter extends WxTokenReTryFilter {

    public WxAccessTokenReTryFilter(ApiActivator activator) {
        super(activator);
    }
    
}
