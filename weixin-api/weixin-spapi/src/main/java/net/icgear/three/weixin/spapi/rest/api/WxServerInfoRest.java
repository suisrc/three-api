package net.icgear.three.weixin.spapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsConsts;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Retry;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.spapi.rest.dto.ServerIpResult;
import net.icgear.three.weixin.spapi.retry.WxAccessTokenReTryFilter;

/**
 * 获取微信服务器信息
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin")
public interface WxServerInfoRest {
    final String NAMED = JaxrsConsts.separator + "net.icgear.three.weixin.spapi.rest.api.WxServerInfoRest";

    /**
     * 获取微信服务器IP地址 
     * 企业微信在回调企业指定的URL时，是通过特定的IP发送出去的。如果企业需要做防火墙配置，那么可以通过这个接口获取到所有相关的IP段。
     * 
     * https://api.weixin.qq.com/cgi-bin/getcallbackip?access_token=ACCESS_TOKEN
     * 
     * @param accessToken 公众号的access_token
     * @return
     */
    @GET
    @Path("getcallbackip")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Retry(value = WxAccessTokenReTryFilter.class, master = JaxrsConsts.FIELD_ACTIVATOR) // 放生异常清空激活器上的accessToken后重试
    ServerIpResult getCallbackIp(@QueryParam("access_token") @Value(WxConsts.ACCESS_TOKEN) @NotNull("访问令牌为空") String accessToken);
    default ServerIpResult getCallbackIpDef() {
        return getCallbackIp(null);
    }
}
