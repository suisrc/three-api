package com.suisrc.weixin.wildfly.runtime;

import javassist.CtClass;

/**
 * 
 * @author Y13
 *
 */
@FunctionalInterface
public interface WxConsumer {

    /**
     * 
     */
    void accept(ClassLoader loader, Class<?> api, CtClass ctClass) throws Exception;

}
