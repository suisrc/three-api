package com.suisrc.weixin.wildfly.runtime;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.suisrc.core.Global;
import com.suisrc.core.annotation.ScConfig;
import com.suisrc.core.utils.CoreUtils;
import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.jaxrsapi.core.ApiActivatorInfo;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.annotation.WxApi;

/**
 * 微信激活器信息
 * @author Y13
 *
 */
public class WeixinActivatorInfo implements ApiActivatorInfo {
    

    /**
     * 获取企业微信信息内容
     * @param nameKey
     * @param loader
     * @throws Exception 
     */
    @SuppressWarnings({"rawtypes"})
    static WeixinActivatorInfo create(String nameKey, ClassLoader loader, Logger logger, String apiPkg, String type) {
        WeixinActivatorInfo aInfo = new WeixinActivatorInfo();
        // 设定值
        CoreUtils.setConfigProperty(aInfo, (config, clazz) -> {
            // 构建检索KEY
            String key = WxConsts.WEIXIN_PRE + nameKey + ".build." + config.value();
            
            if ("remote-apis".equals(config.name())) {
                // 该内容需要特殊处理
                try {
                    return getRemoteApi(key, loader, apiPkg, type);
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
            if ("oauth2-apis".equals(config.name())) {
                Map<String, String> properties = CoreUtils.getSystemProperty2Map(key, LinkedHashMap::new);
                return WeixinOpenApis.getList(properties);
            }
            Class cls = clazz;
            if (cls == String.class) {
                String content = System.getProperty(key);
                if (content != null) {
                    return content;
                }
                if (config.defaultValue().isEmpty()) {
                    return null;
                }
                if (config.defaultValue().equals("named")) {
                    // 特殊处理
                    return nameKey;
                }
                
                String name = nameKey.substring(0, 1).toUpperCase() + nameKey.substring(1);
                String value = config.defaultValue().replace("{name}", name);
                return value;
            }
            if (cls == boolean.class) {
                if (System.getProperty(key) == null) {
                    if (config.defaultValue().isEmpty()) {
                        return null;
                    } else {
                        return Boolean.valueOf(config.defaultValue());
                    }
                } else {
                    return Boolean.getBoolean(key);
                }
            }
            if (cls == Integer.class) {
                return Integer.getInteger(key);
            }
            return null;
        });
        return aInfo;
    }
    
    /**
     * 获取远程API接口
     * @return
     * @throws ClassNotFoundException 
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private static Set<Class<?>> getRemoteApi(String key, ClassLoader loader, String apiPkg, String type) throws ClassNotFoundException {
        Set<Class<?>> classes = new HashSet<>();
        
        if (apiPkg != null) {
            // 系统默认的内容，基本上更TokenRest对应的pkg进行绑定
            Set<Class<?>> classes1 = ReflectionUtils.getTypesAnnotatedWith(RemoteApi.class, true, apiPkg);
            if (type != null) {
                // 类型验证失败
                // c.getAnnotation(WxApi.class)
                // 该方法无效
                // 由于运行时加载WxApi和加载RestClass是两个classloader,似乎WxApi被加载了两次
                // 而这种情况在正式部署过程中却不存在
                // 所以这里执行了特殊的写法
                Predicate<Class<?>> predicate = null;
                try {
                    Class classWxApi = Thread.currentThread().getContextClassLoader().loadClass(WxApi.class.getName());
                    predicate = c -> {
                        Annotation wxApi = c.getAnnotation(classWxApi); // 该方法无法正确获取注解内容
                        if (wxApi == null) {
                            return true;
                        }
                        String[] types = (String[]) ReflectionUtils.invokeGetMethod(wxApi, "types");
                        if (types == null || types.length == 0) {
                            return true;
                        }
                        return Arrays.asList(types).stream().anyMatch(type::equals);
                    };
                } catch (Exception e) {
                    Global.getLogger().info("主类加载无法加载WxApi注解，已经切换加载方式: 通过模块加载类构建");
                    predicate = c -> {
                        WxApi wxApi = c.getAnnotation(WxApi.class);
                        if (wxApi == null) {
                            return true;
                        }
                        String[] types = wxApi.types();
                        if (types == null || types.length == 0) {
                            return true;
                        }
                        return Arrays.asList(types).stream().anyMatch(type::equals);
                    };
                }
                Set<Class<?>> classes2 = classes1.stream().filter(predicate).collect(Collectors.toSet());
                classes.addAll(classes2);
            } else {
                classes.addAll(classes1);
            }
        }
        
        // 自定义的内容
        String[] apiArray = CoreUtils.getSystemProperty2Array(key);
        if (apiArray != null && apiArray.length > 0) {
            for (String apiClass : apiArray) {
                classes.add(loader.loadClass(apiClass));
            }
        }
        return classes;
    }
    
    /**
     * 部署模式，是否为多激活器模式
     */
    private boolean mulitMode = true;
    
    /**
     * 关键字
     */
    private String nameKey;
    
    /**
     * 绑定到激活器上的接口列表
     */
    @ScConfig(value="remote-apis.*", name="remote-apis")
    private Set<Class<?>> remoteApiClasses;
    
    /**
     * 激活器的名称
     */
    @ScConfig(value="named", defaultValue="named")
    private String activatorName;
    
    /**
     * 激活器的类名
     */
    @ScConfig(value="activator-name", defaultValue="{name}Activator")
    private String activatorClassName;
    
    /**
     * 激活器抽象类
     */
    @ScConfig(value="activator-class")
    private String activatorClass = null;
    
    /**
     * 激活器所在的包名
     */
    @ScConfig(value="package-name", defaultValue="com.suisrc.weixin.wildfly.{name}")
    private String activatorPackageName;
    
    /**
     * 绑定数据接口类型
     */
    @ScConfig(value="binding-name", defaultValue="{name}BindingRest")
    private String bindingClassName;
    
    /**
     * 绑定数据接口抽象类
     */
    @ScConfig(value="binding-class")
    private String bindingClass;
    
    /**
     * 绑定指令接口类型
     */
    @ScConfig(value="receive-name", defaultValue="{name}ReceiveRest")
    private String receiveClassName;
    
    /**
     * 绑定指令接口抽象类
     */
    @ScConfig(value="receive-class")
    private String receiveClass;
    
    /**
     * 调试
     */
    @ScConfig("debug")
    private boolean debug = false;
    
    /**
     * 源代码位置
     */
    @ScConfig("test-source")
    private String sourcePath = null;
    
    /**
     * 是否暴露swagger接口
     */
    @ScConfig("swagger-api")
    private boolean swaggerApi = false;
    
    /**
     * 微信配置别名
     */
    @ScConfig("config-named")
    private String configNamed = null;
    
    /**
     * 注入的集群内容控制器
     */
    @ScConfig("topology-named")
    private String topologyNamed = null;
    
    /**
     * 注入的企业信息控制器
     */
    @ScConfig("permanent-named")
    private String permanentNamed = null;
    
    /**
     * 支付配置别名
     */
    @ScConfig("pay-config-named")
    private String payConfigNamed = null;
    
    /**
     * 消息模版配置别名
     */
    @ScConfig("template-named")
    private String templateNamed = null;
    
    /**
     * oauth2授权接口
     */
    @ScConfig("open-oauth2-api")
    private boolean isOpenOauth2Api = false;
    
    /**
     * oauth2授权回调接口
     */
    @ScConfig("open-oauth2-named")
    private String openOauth2Named = null;
    
    /**
     * 
     */
    @ScConfig("open-oauth2-state")
    private String openOAuth2State = null;
    
    /**
     * 
     */
    @ScConfig(value = "open-oauth2-apis.*", name = "oauth2-apis")
    private List<WeixinOpenApis> openApis = null;
    
    /**
     * 远程api使用的优先级
     * 可以通过CdiUtils.selectIndex进行选择
     */
    @ScConfig("remote-api-priority")
    private Integer remoteApiPriority = null;
    
    /**
     * 是否生成回调接口
     */
    @ScConfig("binding-api")
    private boolean isBindingApi = true;
    
    @Override
    public Set<Class<?>> getClasses() {
        return remoteApiClasses;
    }

    @Override
    public boolean isMulitMode() {
        return mulitMode;
    }

    @Override
    public String getActivatorName() {
        return activatorName;
    }

    @Override
    public String getActivatorClassName() {
        return activatorClassName;
    }

    @Override
    public String getActivatorPackageName() {
        return activatorPackageName;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(String key, Class<T> type) {
        if (type == String.class) {
            return (T) System.getProperty(key);
        }
        if (type == Boolean.class) {
            boolean res = Boolean.getBoolean(key);
            return (T) Boolean.valueOf(res);
        }
        return null;
    }

    @Override
    public boolean isStdInject() {
        return true;
    }
    
    @Override
    public Integer getApiPriority() {
        return remoteApiPriority;
    }
    
    public Integer getRemoteApiPriority() {
        return remoteApiPriority;
    }
    
    public void setRemoteApiPriority(Integer remoteApiPriority) {
        this.remoteApiPriority = remoteApiPriority;
    }
    
    public Set<Class<?>> getRemoteApiClasses() {
        return remoteApiClasses;
    }

    public void setRemoteApiClasses(Set<Class<?>> remoteApiClasses) {
        this.remoteApiClasses = remoteApiClasses;
    }


    public void setMulitMode(boolean mulitMode) {
        this.mulitMode = mulitMode;
    }

    public void setActivatorName(String activatorName) {
        this.activatorName = activatorName;
    }

    public void setActivatorClassName(String activatorClassName) {
        this.activatorClassName = activatorClassName;
    }

    public void setActivatorPackageName(String activatorPackageName) {
        this.activatorPackageName = activatorPackageName;
    }

    public String getBindingClassName() {
        return bindingClassName;
    }

    public void setBindingClassName(String bindingClassName) {
        this.bindingClassName = bindingClassName;
    }

    public String getReceiveClassName() {
        return receiveClassName;
    }

    public void setReceiveClassName(String receiveClassName) {
        this.receiveClassName = receiveClassName;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public void setNameKey(String nameKey) {
        this.nameKey = nameKey;
    }
    
    public String getNameKey() {
        return this.nameKey;
    }

    public void setSwaggerApi(boolean swaggerApi) {
        this.swaggerApi = swaggerApi;
    }
    
    public boolean isSwaggerApi() {
        return swaggerApi;
    }

    public String getConfigNamed() {
        return configNamed;
    }

    public void setConfigNamed(String configNamed) {
        this.configNamed = configNamed;
    }

    public String getPayConfigNamed() {
        return payConfigNamed;
    }

    public void setPayConfigNamed(String payConfigNamed) {
        this.payConfigNamed = payConfigNamed;
    }

    public String getActivatorClass() {
        return activatorClass;
    }

    public void setActivatorClass(String activatorClass) {
        this.activatorClass = activatorClass;
    }

    public String getBindingClass() {
        return bindingClass;
    }

    public void setBindingClass(String bindingClass) {
        this.bindingClass = bindingClass;
    }

    public String getReceiveClass() {
        return receiveClass;
    }

    public void setReceiveClass(String receiveClass) {
        this.receiveClass = receiveClass;
    }

    public String getTemplateNamed() {
        return templateNamed;
    }

    public void setTemplateNamed(String templateNamed) {
        this.templateNamed = templateNamed;
    }

    public boolean isOpenOauth2Api() {
        return isOpenOauth2Api;
    }

    public void setOpenOauth2Api(boolean isOpenOauth2Api) {
        this.isOpenOauth2Api = isOpenOauth2Api;
    }

    public String getOpenOauth2Named() {
        return openOauth2Named;
    }

    public void setOpenOauth2Named(String openOauth2Named) {
        this.openOauth2Named = openOauth2Named;
    }

    public String getOpenOAuth2State() {
        return openOAuth2State;
    }

    public void setOpenOAuth2State(String openOAuth2State) {
        this.openOAuth2State = openOAuth2State;
    }

    public List<WeixinOpenApis> getOpenApis() {
        return openApis;
    }

    public void setOpenApis(List<WeixinOpenApis> openApis) {
        this.openApis = openApis;
    }
    
    public String getTopologyNamed() {
        return topologyNamed;
    }

    public void setTopologyNamed(String topologyNamed) {
        this.topologyNamed = topologyNamed;
    }

    public String getPermanentNamed() {
        return permanentNamed;
    }

    public void setPermanentNamed(String permanentNamed) {
        this.permanentNamed = permanentNamed;
    }

    public boolean isBindingApi() {
        return isBindingApi;
    }

    public void setBindingApi(boolean isBindingApi) {
        this.isBindingApi = isBindingApi;
    }
    
}
