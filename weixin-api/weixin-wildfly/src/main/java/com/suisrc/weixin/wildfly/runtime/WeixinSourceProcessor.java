package com.suisrc.weixin.wildfly.runtime;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.jandex.IndexView;
import org.jboss.jdeparser.JAnnotation;
import org.jboss.jdeparser.JBlock;
import org.jboss.jdeparser.JCall;
import org.jboss.jdeparser.JDocComment;
import org.jboss.jdeparser.JExpr;
import org.jboss.jdeparser.JExprs;
import org.jboss.jdeparser.JJClass;
import org.jboss.jdeparser.JJMethod;
import org.jboss.jdeparser.JJParam;
import org.jboss.jdeparser.JMod;
import org.jboss.jdeparser.JParamDeclaration;
import org.jboss.jdeparser.JTypes;
import org.jboss.modules.Module;

import com.suisrc.core.jdejst.JdeJst;
import com.suisrc.core.security.ScCrypto;
import com.suisrc.core.utils.CdiUtils;
import com.suisrc.core.utils.StringUtils;
import com.suisrc.core.utils.Throwables;
import com.suisrc.jaxrsapi.core.factory.ClientServiceFactory;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.CtClass;
import net.icgear.three.weixin.core.WxConfig;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.handler.WxConfigHandler;
import net.icgear.three.weixin.core.handler.WxTemplateHandler;
import net.icgear.three.weixin.core.handler.WxTopologyHandler;
import net.icgear.three.weixin.core.oauth2.WxAuthParams;
import net.icgear.three.weixin.core.oauth2.WxAuthResult;
import net.icgear.three.weixin.core.oauth2.WxOAuth2RestHandler;
import net.icgear.three.weixin.core.rest.WxBindingRest;
import net.icgear.three.weixin.mpapi.MpActivator;
import net.icgear.three.weixin.mpapi.rest.MpWxBinding;
import net.icgear.three.weixin.mpapi.rest.api.MpTokenServiceRest;
import net.icgear.three.weixin.open.intercept.WxOAuth2Config;
import net.icgear.three.weixin.open.intercept.WxOAuth2Get;
import net.icgear.three.weixin.payapi.PayActivator;
import net.icgear.three.weixin.payapi.handler.PayConfigHandler;
import net.icgear.three.weixin.payapi.rest.api.MmpaymkttransfersRest;
import net.icgear.three.weixin.qyapi.QyMainActivator;
import net.icgear.three.weixin.qyapi.QyThreeActivator;
import net.icgear.three.weixin.qyapi.handler.PermanentHandler;
import net.icgear.three.weixin.qyapi.rest.QmWxBinding;
import net.icgear.three.weixin.qyapi.rest.QySuiteReceive;
import net.icgear.three.weixin.qyapi.rest.QySuiteReceiveRest;
import net.icgear.three.weixin.qyapi.rest.QyWxBinding;
import net.icgear.three.weixin.qyapi.rest.api.QyMainServiceRest;
import net.icgear.three.weixin.qyapi.rest.api.QyThreeServiceRest;
import net.icgear.three.weixin.spapi.SpActivator;
import net.icgear.three.weixin.spapi.rest.SpWxBinding;
import net.icgear.three.weixin.spapi.rest.api.SpTokenServiceRest;

/**
 * 生成运行时代码
 * 
 * @author Y13
 *
 */
public class WeixinSourceProcessor {
    
    /**
     * 日志
     */
    private static final Logger logger = Logger.getLogger(WeixinSourceProcessor.class.getName());


    /**
     * 处理和构建执行代码
     * @param archive
     * @param name
     * @param isMulitMode
     * @throws Exception 
     */
    public void process(IndexView index, WxConsumer consumer, String nameKey, boolean isMulitMode) 
            throws Exception {
        String type = System.getProperty(WxConsts.WEIXIN_PRE + nameKey + ".type");
        if (type == null) {
            // 跳过配置
            logger.warning("跳过配置，指定类型为空：" + nameKey);
        }
        switch(type) {
            case WxConsts.PAY:
                logger.info("正在进行支付平台配置：" + nameKey);
                processBuild(index, consumer, nameKey, isMulitMode, WxConsts.PAY, PayActivator.class, 
                        MmpaymkttransfersRest.class, null, null, false);
                break;
            case WxConsts.SP:
                logger.info("正在进行小程序配置：" + nameKey);
                processBuild(index, consumer, nameKey, isMulitMode, WxConsts.SP, SpActivator.class, 
                        SpTokenServiceRest.class, "微信小程序消息回调接口", SpWxBinding.class, false);
                break;
            case WxConsts.MP:
                logger.info("正在进行公众号配置：" + nameKey);
                processBuild(index, consumer, nameKey, isMulitMode, WxConsts.MP, MpActivator.class,
                        MpTokenServiceRest.class, "微信公众号消息回调接口", MpWxBinding.class, false);
                break;
            case WxConsts.QM:
                logger.info("正在进行企业号配置：" + nameKey);
                processBuild(index, consumer, nameKey, isMulitMode, WxConsts.QM, QyMainActivator.class,
                        QyMainServiceRest.class, "微信企业号消息回调接口", QmWxBinding.class, false);
                break;
            case WxConsts.QT:
                logger.info("正在进行企业应用配置：" + nameKey);
                processBuild(index, consumer, nameKey, isMulitMode, WxConsts.QT, QyThreeActivator.class,
                        QyThreeServiceRest.class, "微信企业应用消息回调接口", QyWxBinding.class, true);
                break;
            default:
                // 跳过配置
                logger.warning("跳过配置，指定类型为[" + type + "]:" + nameKey);
                break;
        }
    }

    /**
     * 企业号配置
     * @param index
     * @param archive
     * @param nameKey
     */
    private void processBuild(IndexView index, WxConsumer consumer, String nameKey, boolean isMulitMode, String type,
            Class<?> activatorClass, Class<?> tokenRestClass, String swaggerBinding, Class<?> bindingClass, boolean hasReceive) throws Exception {
        // ClassLoader loader = Thread.currentThread().getContextClassLoader();
        // ClassLoader loader = ClassLoader.getSystemClassLoader();
        ClassLoader loader = Module.getCallerModule().getClassLoader();
        
        WeixinActivatorInfo aInfo = WeixinActivatorInfo.create(nameKey, loader, logger, tokenRestClass.getPackage().getName(), type);
        aInfo.setMulitMode(isMulitMode);
        aInfo.setNameKey(nameKey);

        JdeJst jj = new JdeJst();
        if (aInfo.getSourcePath() != null) {
            // 暂时生成的代码
            jj.setShowSrc(true);
            jj.setTarget(new File(aInfo.getSourcePath()));
        }
        
        if (aInfo.getActivatorClass() != null) {
            activatorClass = loader.loadClass(aInfo.getActivatorClass());
        }
        // 构建Activator
        buildActivator(jj, aInfo, activatorClass, null);
        
        // 构建Binding
        if (aInfo.isBindingApi() && swaggerBinding != null) {
            if (aInfo.getBindingClass() != null) {
                bindingClass = loader.loadClass(aInfo.getBindingClass());
            }
            String bindPath = aInfo.getNameKey() + "/" + WxConsts.REST_PATH_BIND;
            buildBindingRest(jj, aInfo, bindingClass, WxBindingRest.class, aInfo.getBindingClassName(), swaggerBinding,
                    bindPath, aInfo.isSwaggerApi(), (jja, jjc) -> buildOpenOauth2Rest(swaggerBinding, aInfo, jja, jjc));

            // logger
            String restPath = bindPath + "/" + WxConsts.REST_PATH_WX;
            if (aInfo.getConfigNamed() != null) {
                restPath += "?" + WxConsts.REST_APP_KEY + "={AppKey}";
            }
            logger.info(swaggerBinding + "绑定地址: {RootPath}/" + restPath);
        }

        // 构建Receive
        if (aInfo.isBindingApi() && hasReceive) {
            // 企业微信特殊的回调指令接口
            Class<?> receiveClass = QySuiteReceive.class;
            if (aInfo.getReceiveClass() != null) {
                receiveClass = loader.loadClass(aInfo.getReceiveClass());
            }
            String swaggerReceive = swaggerBinding.replace("消息", "指令");
            String suitePath = aInfo.getNameKey() + "/" + WxConsts.REST_PATH_SUITE;
            buildBindingRest(jj, aInfo, receiveClass, QySuiteReceiveRest.class, aInfo.getReceiveClassName(), swaggerReceive,
                    suitePath, aInfo.isSwaggerApi(), null);

            // logger
            String restPath = suitePath + "/" + WxConsts.REST_PATH_WX;
            if (aInfo.getConfigNamed() != null) {
                restPath += "?" + WxConsts.REST_APP_KEY + "={AppKey}";
            }
            logger.info(swaggerReceive + "绑定地址: {RootPath}/" + restPath);
        }

        // 将代码写出部署环境中
        jj.getClassMap().values().forEach(JJClass::imports);
        Map<Object, CtClass> ctClasses = jj.writeSource();
        // 构建接口信息
        ClientServiceFactory.processIndex(loader, aInfo, index, ctClasses::put, "_" + nameKey, true, aInfo.getSourcePath());
        // 代码写出到部署环境中
        ctClasses.entrySet().forEach(v -> this.processCtClass2Archive(loader, consumer, (Class<?>) v.getKey(), v.getValue()));
    }

    /**
     * oauth2认证部分的内容
     * @param aInfo
     * @param jjc
     */
    private void buildOpenOauth2Rest(String pname, WeixinActivatorInfo aInfo, JJClass jja, JJClass jjc) {
        if (aInfo.isOpenOauth2Api() || aInfo.getOpenApis() != null)
        if (aInfo.getOpenOauth2Named() != null) {
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, aInfo.getOpenOauth2Named(), "createOAuth2Handler", WxOAuth2RestHandler.class);
        }
        jjc._import(Named.class);
        jjc._import(WxOAuth2Get.class);
        jjc._import(WxOAuth2Config.class);
        //jjc._import(WxAuthParams.class);
        
        // logger
        String restPath = aInfo.getNameKey() + "/" + WxConsts.REST_PATH_BIND + "/" + WxConsts.REST_PATH_AUTH + "{0}";
        if (aInfo.getConfigNamed() != null) {
            restPath += "?" + WxConsts.REST_APP_KEY + "={AppKey}";;
        }
        if (aInfo.isOpenOauth2Api()) {
            // @Named("ActivatorName")
            // @WxOAuth2Get
            // @WxOAuth2Config(refreshKey = "r", scopeKey = "scope", "state" = "xxx")
            JJMethod jjm = jjc.method(JMod.PUBLIC, WxAuthResult.class, "doOAuth2");
            JParamDeclaration jpd = jjm.getJMethodDef().param(WxAuthParams.class, "params");
            // @Named("ActivatorName")
            JAnnotation anno = jjm.annotate(Named.class);
            jjm.annotateValue(anno, "value", aInfo.getActivatorName());
            // @WxOAuth2Get
            jjm.annotate(WxOAuth2Get.class);
            // @WxOAuth2Config(refreshKey = "r", scopeKey = "scope", "state" = "xxx")
            anno = jjm.annotate(WxOAuth2Config.class);
            jjm.annotateValue(anno, "refreshKey", "r");
            jjm.annotateValue(anno, "scopeKey", "scope");
            String state = aInfo.getOpenOAuth2State() != null ? aInfo.getOpenOAuth2State() : ScCrypto.genRandomStr(13);
            jjm.annotateValue(anno, "state", state);
            // body
            JBlock jbk = jjm.getJMethodDef().body();
            JCall jcl = JExprs.call("doDefaultOAuth2");
            jcl.arg(JExprs.$v(jpd));
            jbk._return(jcl);
            // logger
            String restPath0 = StringUtils.format(restPath, "");
            logger.info(pname + "OAuth2授权: {RootPath}/" + restPath0);
        }
        if (aInfo.getOpenApis() != null) {
            if (jja != null) {
                jja._import(GET.class);
                jja._import(Produces.class);
                jja._import(BeanParam.class);
                jja._import(ApiOperation.class);
            } else {
                jjc._import(GET.class);
                jjc._import(Produces.class);
                jjc._import(BeanParam.class);
                jjc._import(ApiOperation.class);
            }
            // au11030
            int offset = 91031;
            for (WeixinOpenApis api : aInfo.getOpenApis()) {
                String apiPath = api.getPath();
                if (apiPath.charAt(0) != '/') {
                    apiPath = "/" + apiPath;
                }
                String path = WxConsts.REST_PATH_AUTH + apiPath;
                String name = api.getName();
                name = name.substring(0, 1).toUpperCase() + name.substring(1);
                // @Named("ActivatorName")
                // @WxOAuth2Get
                // @WxOAuth2Config(refreshKey = "r", scopeKey = "scope", "state" = "xxx")
                // @GET
                // @Path(WxConsts.REST_PATH_AUTH + {path})
                // @Produces(MediaType.APPLICATION_JSON + WxConsts.MediaType_UTF_8)
                // WxAuthResult doOAuth2{name}(@BeanParam WxAuthParams params);
                // 开始构建吧
                // WxAuthResult doOAuth2{name}(@BeanParam WxAuthParams params);
                JJMethod jjm = jjc.method(JMod.PUBLIC, WxAuthResult.class, "doOAuth2" + name);
                JJParam jjp = jjm.param(WxAuthParams.class, "params");
                // @Named("ActivatorName")
                JAnnotation anno = jjm.annotate(Named.class);
                jjm.annotateValue(anno, "value", aInfo.getActivatorName());
                // @WxOAuth2Get
                jjm.annotate(WxOAuth2Get.class);
                // @WxOAuth2Config(refreshKey = "r", scopeKey = "scope", "state" = "xxx")
                anno = jjm.annotate(WxOAuth2Config.class);
                jjm.annotateValue(anno, "refreshKey", "r");
                // scope
                if (api.getScope() != null && !api.getScope().isEmpty()) {
                    jjm.annotateValue(anno, "scope", api.getScope());
                } else {
                    jjm.annotateValue(anno, "scopeKey", "scope");
                }
                String state = api.getState() != null ? api.getState() : ScCrypto.genRandomStr(13);
                jjm.annotateValue(anno, "state", state);
                if (api.getTime() != null) {
                    jjm.annotateValue(anno, "timeWait", api.getTime());
                }
                if (api.getType() != null) {
                    jjm.annotateValue(anno, "type", api.getType());
                }
                
                JJMethod jjam;
                if (jja == null) {
                    // 实现即实体
                    jjam = jjm;
                    jjp.annotate(BeanParam.class);
                } else {
                    // 方法为继承
                    jjm.annotate(Override.class);
                    jjam = jja.method(0, WxAuthResult.class, "doOAuth2" + name);
                    JJParam jjap = jjam.param(WxAuthParams.class, "params");
                    jjap.annotate(BeanParam.class);
                }
                jjam.annotate(GET.class);
                anno = jjam.annotate(Path.class);
                jjam.annotateValue(anno, "value", path);
                anno = jjam.annotate(Produces.class);
                String mediaType = MediaType.APPLICATION_JSON + WxConsts.MediaType_UTF_8;
                jjam.annotateValues(anno, "value", new String[]{ mediaType });
                anno = jjam.annotate(ApiOperation.class);
                jjam.annotateValue(anno, "nickname", "au" + offset++);
                jjam.annotateValue(anno, "value", api.getDesc() == null ? api.getName() : api.getDesc());
                
                // au11030
                // body 内容
                JBlock jbk = jjm.getJMethodDef().body();
                JExpr param = JExprs.$v(jjp.getParamDef());
                // 进行默认赋值
                // sync
                if (api.getSync() != null) {
                    jbk.call(param, "setSync").arg(api.getSync() ? JExpr.TRUE : JExpr.FALSE);
                }
                // key
                if (api.getKey() != null) {
                    jbk.call(param, "setKey").arg(JExprs.str(api.getKey()));
                }
                // scope
                if (api.getScope() != null) {
                    jbk.call(param, "setScope").arg(JExprs.str(api.getScope()));
                }
                JCall jcl = JExprs.call("doDefaultOAuth2");
                jcl.arg(param);
                jbk._return(jcl);
                
                //logger
                String restPath0 = StringUtils.format(restPath, apiPath);
                logger.info(pname + "OAuth2授权[" + api.getName() +"]: {RootPath}/" + restPath0);
            }
        }

    }
    
    /**
     * 将CtClass注入系统中
     * @param api
     * @param impl
     */
    private void processCtClass2Archive(ClassLoader loader, WxConsumer consumer, Class<?> api, CtClass impl) {
        try {
            consumer.accept(loader, api, impl);
        } catch (Exception e) {
            throw Throwables.getRuntimeException(e);
        }
    }

    /**
     * (指令回调接口)
     * @param jj
     * @param aInfo
     */
    private void buildBindingRest(JdeJst jj, WeixinActivatorInfo aInfo, Class<?> superClass, Class<?> interfaceClass, String className, 
            String swaggerValue, String pathValue, boolean hasInterface, BiConsumer<JJClass, JJClass> acceptThen) {
        String pkgname = aInfo.getActivatorPackageName() + ".api";
        String clsname = className;
        JJClass jjApi = null;
        if (hasInterface) {
            jjApi = jj.createInterface(interfaceClass, clsname, pkgname);
            // 注释说明
            addDocComment(jjApi);
            // 注解
            jjApi._import(Api.class);
            JAnnotation anno = jjApi.annotate(Api.class);
            jjApi.annotateValue(anno, "value", aInfo.getActivatorName() + ":" + swaggerValue);
            jjApi._import(Path.class);
            anno = jjApi.annotate(Path.class);
            jjApi.annotateValue(anno, "value", pathValue);
            // 继承
            jjApi._import(interfaceClass);
            jjApi._extends(interfaceClass);
            // 重命名
            pkgname = aInfo.getActivatorPackageName() + ".impl";
            clsname += "Impl";
        }
        // 微信回调接口
        JJClass jjImpl = jj.createClass(superClass, clsname, pkgname);
        // 注释说明
        addDocComment(jjImpl);
        // 继承
        jjImpl._import(superClass);
        jjImpl._extends(superClass);
        
        if (jjApi == null) {
            jjImpl._import(interfaceClass);
            jjImpl._implements(interfaceClass);
            
            jjImpl._import(Path.class);
            JAnnotation anno = jjImpl.annotate(Path.class);
            jjImpl.annotateValue(anno, "value", pathValue);
        } else {
            jjImpl._implements(jjApi.getCanonicalName());
        }
        // 注解
        jjImpl._import(ApplicationScoped.class);
        jjImpl.annotate(ApplicationScoped.class);
        
        // getMessageControllerKey
        JJMethod jjm = jjImpl.method(JMod.PROTECTED, String.class, "getMessageControllerKey");
        jjm.getJMethodDef().body()._return(JExprs.str(className));
        
        // createWxConfig
        buildCallCdiInjectMethod(JMod.PROTECTED, jjImpl, aInfo.getActivatorName(), "createWxConfig", WxConfig.class);
        
        // other
        if (acceptThen != null) {
            acceptThen.accept(jjApi, jjImpl);
        }
    }
    
    /**
     * 使用CdiUtils注入调用的内容
     * @param jMod
     * @param jj
     * @param named
     * @param methodName
     * @param infClass
     */
    private void buildCallCdiInjectMethod(int jMod, JJClass jj, String named, String methodName, Class<?> infClass) {
        JJMethod jjm = jj.method(jMod, infClass, methodName);
        if (named != null) {
            jj._import(Named.class);
            JAnnotation anno = jjm.annotate(Named.class);
            jjm.annotateValue(anno, "value", named);
        }
        JBlock jbk = jjm.getJMethodDef().body();
        JCall jcl = JExprs.callStatic(CdiUtils.class, CdiUtils.MED_DEF_QUALIFIER);
        jcl.arg(JTypes.typeOf(infClass).field("class"));
        jbk._return(jcl);
    }

    /**
     * 构建激活器
     * @param jj
     * @param aInfo
     * @param activatorClass
     * @param restApi
     */
    private void buildActivator(JdeJst jj, WeixinActivatorInfo aInfo, Class<?> superClass, Consumer<JJClass> acceptThen) {
        JJClass jjc = jj.createClass(superClass, aInfo.getActivatorClassName(), aInfo.getActivatorPackageName());
        // 注释说明
        addDocComment(jjc);
        // 继承
        // jjc._import(WxConfig.class);
        // jjc._implements(WxConfig.class);
        jjc._import(superClass);
        jjc._extends(superClass);
        // 注解
        jjc._import(ApplicationScoped.class);
        jjc._import(Named.class);
        
        JAnnotation anno = jjc.annotate(ApplicationScoped.class);
        anno = jjc.annotate(Named.class);
        jjc.annotateValue(anno, "value", aInfo.getActivatorName());
        
        // isStdInject
        JJMethod jjm = jjc.method(JMod.PUBLIC, boolean.class, "isStdInject");
        jjm.getJMethodDef().body()._return(JExpr.TRUE);
        
        // isMulitMode
        jjm = jjc.method(JMod.PUBLIC, boolean.class, "isMulitMode");
        jjm.getJMethodDef().body()._return(aInfo.isMulitMode() ? JExpr.TRUE : JExpr.FALSE);
        
        // getAppUniqueKey
        jjm = jjc.method(JMod.PUBLIC, String.class, "getAppUniqueKey");
        jjm.getJMethodDef().body()._return(JExprs.str(aInfo.getNameKey()));
        
        // getTempFileName
        if (aInfo.isDebug()) {
            jjm = jjc.method(JMod.PROTECTED, String.class, "getTempFileName");
            jjm.getJMethodDef().body()._return(JExprs.str("target/token_" + aInfo.getNameKey() + ".obj"));
        }
        
        // getApiImplement
        jjm = jjc.method(JMod.PUBLIC, Object.class, "getApiImplement");
        JParamDeclaration jpd = jjm.getJMethodDef().param(Class.class, "type");
        JBlock jbk = jjm.getJMethodDef().body();
        JCall jcl = JExprs.call("getApiImplement2");
        jcl.arg(JExprs.$v(jpd));
        jbk._return(jcl);
        
        // protected WxTopologyHandler createTopologyHandler
        if (aInfo.getTopologyNamed() != null) {
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, aInfo.getTopologyNamed(), "createTopologyHandler", WxTopologyHandler.class);
        }
        // protected WxConfigHandler createWxConfigHandler
        if (aInfo.getConfigNamed() != null) {
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, aInfo.getConfigNamed(), "createWxConfigHandler", WxConfigHandler.class);
        }
        // protected PermanentHandler createPermanentHandler
        if (aInfo.getPermanentNamed() != null) {
            // 只有企业微信应用有值
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, aInfo.getPermanentNamed(), "createPermanentHandler", PermanentHandler.class);
        }
        // protected PayConfigHandler createPayConfigHandler()
        if (aInfo.getPayConfigNamed() != null) {
            // 只有企业微信应用有值
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, aInfo.getPayConfigNamed(), "createPayConfigHandler", PayConfigHandler.class);
        }
        // protected WxTemplateHandler createWxTemplateHandler()
        if (aInfo.getTemplateNamed() != null) {
            // 模版消息配置内容
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, aInfo.getTemplateNamed(), "createWxTemplateHandler", WxTemplateHandler.class);
        }
        // other
        if (acceptThen != null) {
            acceptThen.accept(jjc);
        }
    }
    
    /**
     * 注释说明
     * @param jjc
     */
    private void addDocComment(JJClass jjc) {
        JDocComment comment = jjc.getJClassDef().docComment();
        comment.text("Follow the implementation of the restful 2.0 standard remote access agent.");
        comment.htmlTag("see", true).text("https://suisrc.github.io/jaxrsapi");
        comment.htmlTag("generateBy", true).text(ClientServiceFactory.class.getCanonicalName());
        comment.htmlTag("time", true).text(LocalDateTime.now().toString());
        comment.htmlTag("author", true).text("Y13");
    }
    
}
