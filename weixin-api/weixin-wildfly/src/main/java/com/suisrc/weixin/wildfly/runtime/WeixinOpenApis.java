package com.suisrc.weixin.wildfly.runtime;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.suisrc.core.annotation.ScConfig;
import com.suisrc.core.utils.CoreUtils;

/**
 * 
 * @author Y13
 *
 */
public class WeixinOpenApis {

    /**
     * 
     */
    @SuppressWarnings("rawtypes")
    public static List<WeixinOpenApis> getList(Map<String, String> props) {
        if (props == null || props.isEmpty()) {
            return null;
        }
        
        // 获取名称列表
        Set<String> names = new LinkedHashSet<>();
        //props.keySet().forEach(k -> names.add(k.substring(0, k.indexOf('.'))));
        for (String key : props.keySet()) {
            int offset = key.indexOf('.');
            if (offset > 0) {
                names.add(key.substring(0, offset));
            }
        }
        
        // 获取apis内容
        List<WeixinOpenApis> apis = new ArrayList<>();
        for (String name : names) {
            WeixinOpenApis api = new WeixinOpenApis(name);
            CoreUtils.setConfigProperty(api, (config, clazz) -> {
                String key = name + "." + config.value();
                Class cls = clazz;
                if (cls == String.class) {
                    return props.get(key);
                }
                if (cls == Boolean.class) {
                    String value = props.get(key);
                    return value == null ? null : Boolean.valueOf(value);
                }
                if (cls == Integer.class) {
                    String value = props.get(key);
                    return value == null ? null : Integer.valueOf(value);
                }
                return null;
            });
            apis.add(api);
        }
        return apis;
    }

    /**
     * 名称
     */
    private String name;

    /**
     * 描述
     */
    @ScConfig("desc")
    private String desc;
    
    /**
     * url路径
     */
    @ScConfig("path")
    private String path;
    
    /**
     * 状态
     */
    @ScConfig("state")
    private String state;
    
    /**
     * 访问类型
     */
    @ScConfig("type")
    private String type;
    
    /**
     * 同步
     */
    @ScConfig("sync")
    private Boolean sync;
    
    /**
     * 访问识别关键字
     */
    @ScConfig("key")
    private String key;
    
    /**
     * 访问作用域
     */
    @ScConfig("scope")
    private String scope;
    
    /**
     * 回调等待的时间
     */
    @ScConfig("time")
    private Integer time;
    
    public WeixinOpenApis(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPath() {
        return path != null ? path : name;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getSync() {
        return sync;
    }

    public void setSync(Boolean sync) {
        this.sync = sync;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
    
}
