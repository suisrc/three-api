package com.suisrc.weixin.wildfly.runtime;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.inject.Inject;

import org.jboss.jandex.IndexView;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.asset.ByteArrayAsset;
import org.wildfly.swarm.jaxrs.JAXRSArchive;
import org.wildfly.swarm.spi.api.DeploymentProcessor;
import org.wildfly.swarm.spi.runtime.annotations.DeploymentScoped;

import net.icgear.three.weixin.core.WxConsts;

/**
 * 生成运行时代码
 * 
 * "swarm.deployment" CoreWxConsts.WEIXIN_PRE CoreWxConsts.WEIXIN_DEF
 * 
 * @author Y13
 *
 */
@SuppressWarnings("rawtypes")
@DeploymentScoped
public class WeixinDeploymentProcessor implements DeploymentProcessor {
    
    /**
     * 
     */
    private final Archive<?> archive;

    /**
     * 
     */
    private final IndexView index;

    /**
     * 部署的应用报名
     */
    private final List<String> wars;

    @Inject
    public WeixinDeploymentProcessor(Archive archive, IndexView index) {
        this.archive = archive;
        this.index = index;
        this.wars = Arrays.asList(getSystemPropertyName("swarm.deployment."));
    }

    @Override
    public void process() throws Exception {
        String arcName = archive.getName();
        if (wars.stream().anyMatch(arcName::equals)) {
            JAXRSArchive arc = archive.as(JAXRSArchive.class);
            WeixinSourceProcessor processor = new WeixinSourceProcessor();
            
            String qynameDef = System.getProperty(WxConsts.WEIXIN_DEF);
            String[] qynames = getSystemPropertyName(WxConsts.WEIXIN_PRE);
            
            for (String qyname : qynames) {
                processor.process(index, (cl, api, ct) -> {
                    String path = "WEB-INF/classes/" + ct.getName().replace('.', '/') + ".class";
                    arc.add(new ByteArrayAsset(ct.toBytecode()), path);
                    // arc.add(new ClassAsset(ct.toClass(cl, null)), path);
                    // arc.addClass(ct.toClass(cl, null));
                }, qyname, !qyname.equals(qynameDef));
            }
        }
    }

    /**
     * 获取配置的前缀
     * 
     */
    private String[] getSystemPropertyName(String prefix) {
        HashSet<String> result = new HashSet<>(); // 去重
        System.getProperties().keySet().stream().map(Object::toString).filter(k -> k.startsWith(prefix) && !k.equals(prefix))
                .forEach(k -> {
                    String key = k.substring(prefix.length());
                    int start = 0;
                    int end = 0;
                    if (key.charAt(0) == '[') {
                        start = 1;
                        end = key.indexOf(']');
                    } else {
                        end = key.indexOf('.');
                    }
                    if (end > start) {
                        result.add(key.substring(start, end));
                    }
                });
        return result.toArray(new String[result.size()]);
    }

}
