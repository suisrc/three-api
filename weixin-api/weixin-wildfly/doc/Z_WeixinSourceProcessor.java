package com.suisrc.weixin.wildfly.runtime;

import java.io.File;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.ws.rs.Path;

import org.jboss.jandex.IndexView;
import org.jboss.jdeparser.JAnnotation;
import org.jboss.jdeparser.JBlock;
import org.jboss.jdeparser.JCall;
import org.jboss.jdeparser.JDocComment;
import org.jboss.jdeparser.JExpr;
import org.jboss.jdeparser.JExprs;
import org.jboss.jdeparser.JJClass;
import org.jboss.jdeparser.JJMethod;
import org.jboss.jdeparser.JMod;
import org.jboss.jdeparser.JParamDeclaration;
import org.jboss.jdeparser.JTypes;
import org.jboss.shrinkwrap.api.asset.ByteArrayAsset;
import org.wildfly.swarm.spi.api.JARArchive;

import com.suisrc.core.jdejst.JdeJst;
import com.suisrc.core.utils.CdiUtils;
import com.suisrc.jaxrsapi.core.JaxrsConsts;
import com.suisrc.jaxrsapi.core.factory.ClientServiceFactory;

import io.swagger.annotations.Api;
import javassist.CtClass;
import net.icgear.three.weixin.core.CoreWxConsts;
import net.icgear.three.weixin.core.WxConfig;
import net.icgear.three.weixin.core.handler.WxConfigHandler;
import net.icgear.three.weixin.core.handler.WxTopologyHandler;
import net.icgear.three.weixin.core.rest.WxBindingRest;
import net.icgear.three.weixin.core.rest.WxKeyBindingRest;
import net.icgear.three.weixin.mpapi.MpActivator;
import net.icgear.three.weixin.mpapi.rest.MpWxBinding;
import net.icgear.three.weixin.mpapi.rest.api.MpTokenServiceRest;
import net.icgear.three.weixin.qyapi.QyThreeActivator;
import net.icgear.three.weixin.qyapi.handler.PermanentHandler;
import net.icgear.three.weixin.qyapi.rest.QmWxBinding;
import net.icgear.three.weixin.qyapi.rest.QyKeySuiteReceiveRest;
import net.icgear.three.weixin.qyapi.rest.QySuiteReceive;
import net.icgear.three.weixin.qyapi.rest.QySuiteReceiveRest;
import net.icgear.three.weixin.qyapi.rest.QyWxBinding;
import net.icgear.three.weixin.qyapi.rest.api.QyMainServiceRest;
import net.icgear.three.weixin.qyapi.rest.api.QyThreeServiceRest;
import net.icgear.three.weixin.spapi.rest.SpWxBinding;
import net.icgear.three.weixin.spapi.rest.api.SpTokenServiceRest;

/**
 * 生成运行时代码
 * 
 * @author Y13
 *
 */
public class Z_WeixinSourceProcessor {
    
    /**
     * 日志
     */
    private static final Logger logger = Logger.getLogger(Z_WeixinSourceProcessor.class.getName());


    /**
     * 处理和构建执行代码
     * @param archive
     * @param name
     * @param isMulitMode
     * @throws Exception 
     */
    public void process(IndexView index, JARArchive archive, String nameKey, boolean isMulitMode) throws Exception {
        String type = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".type");
        if (type == null) {
            // 跳过配置
            logger.warning("跳过配置，指定类型为空：" + nameKey);
        }
        switch(type) {
            case CoreWxConsts.SP:
                logger.info("正在进行小程序配置：" + nameKey);
                // processSp(index, archive, nameKey, isMulitMode, CoreWxConsts.SP);
                processBuild(index, archive, nameKey, isMulitMode, CoreWxConsts.SP, SpTokenServiceRest.class,
                        "微信小程序消息回调接口", SpWxBinding.class, false);
                break;
            case CoreWxConsts.MP:
                logger.info("正在进行公众号配置：" + nameKey);
                // processMp(index, archive, nameKey, isMulitMode, CoreWxConsts.MP);
                processBuild(index, archive, nameKey, isMulitMode, CoreWxConsts.MP, MpTokenServiceRest.class,
                        "微信公众号消息回调接口", MpWxBinding.class, false);
                break;
            case CoreWxConsts.QM:
                logger.info("正在进行企业号配置：" + nameKey);
                // processQm(index, archive, nameKey, isMulitMode, CoreWxConsts.QM);
                processBuild(index, archive, nameKey, isMulitMode, CoreWxConsts.QM, QyMainServiceRest.class,
                        "微信企业号消息回调接口", QmWxBinding.class, false);
                break;
            case CoreWxConsts.QT:
                logger.info("正在进行企业应用配置：" + nameKey);
                // processQt(index, archive, nameKey, isMulitMode, CoreWxConsts.QT);
                processBuild(index, archive, nameKey, isMulitMode, CoreWxConsts.QT, QyThreeServiceRest.class,
                        "微信企业应用消息回调接口", QyWxBinding.class, false);
                break;
            default:
                // 跳过配置
                logger.warning("跳过配置，指定类型为[" + type + "]:" + nameKey);
                break;
        }
    }

    /**
     * 企业号配置
     * @param index
     * @param archive
     * @param nameKey
     * @param isMulitMode
     */
    private void processBuild(IndexView index, JARArchive archive, String nameKey, boolean isMulitMode, String type,
            Class<?> tokenRestClass, String swaggerBinding, Class<?> bindingClass, boolean hasReceive) throws Exception {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        
        WeixinActivatorInfo aInfo = WeixinActivatorInfo.create(nameKey, loader, logger, tokenRestClass.getPackage().getName(), type);
        aInfo.setMulitMode(isMulitMode);
        aInfo.setNameKey(nameKey);

        JdeJst jj = new JdeJst();
        if (aInfo.getSourcePath() != null) {
            // 暂时生成的代码
            jj.setShowSrc(true);
            jj.setTarget(new File(aInfo.getSourcePath()));
        }
        // 构建Activator
        buildActivator(jj, aInfo, QyThreeActivator.class, jjc -> {
            
            // getApiImplement
            JJMethod jjm = jjc.method(JMod.PUBLIC, Object.class, "getApiImplement");
            JParamDeclaration jpd = jjm.getJMethodDef().param(Class.class, "type");
            JBlock jbk = jjm.getJMethodDef().body();
            JCall jcl = JExprs.call("getApiImplement2");
            jcl.arg(JExprs.$v(jpd));
            jbk._return(jcl);
            
            // protected ServiceRest createTokenRest()
            String named = null;
            if (isMulitMode) {
                named = aInfo.getActivatorName() + JaxrsConsts.separator + tokenRestClass.getName();
            }
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, named, "createTokenRest", tokenRestClass);
            
            // protected PermanentHandler createPermanentHandler
            if (aInfo.getPermanentNamed() != null) {
                buildCallCdiInjectMethod(JMod.PROTECTED, jjc, aInfo.getPermanentNamed(), "createPermanentHandler", PermanentHandler.class);
            }
        });
        // 构建Binding
        Class<?> restApi = aInfo.getConfigNamed() == null ? WxBindingRest.class : WxKeyBindingRest.class;
        buildBindingRest(jj, aInfo, bindingClass, restApi, aInfo.getBindingClassName(), swaggerBinding, 
                aInfo.getNameKey() + "/binding", aInfo.isSwaggerApi(), null);
        
        if (hasReceive) {
            // 企业微信特殊
            // 构建Receive
            restApi = aInfo.getConfigNamed() == null ? QySuiteReceiveRest.class : QyKeySuiteReceiveRest.class;
            buildBindingRest(jj, aInfo, QySuiteReceive.class, restApi, aInfo.getReceiveClassName(), "企业微信应用指令回调接口", 
                    aInfo.getNameKey() + "/suite", aInfo.isSwaggerApi(), null);
        }

        // 将代码写出部署环境中
        jj.getClassMap().values().forEach(JJClass::imports);
        Map<Object, CtClass> ctClasses = jj.writeSource();
        // 构建接口信息
        ClientServiceFactory.processIndex(aInfo, index, ctClasses::put, "_" + nameKey, true, aInfo.getSourcePath());
        // 代码写出到部署环境中
        ctClasses.entrySet().forEach(v -> this.processCtClass2Archive(loader, archive, (Class<?>) v.getKey(), v.getValue()));
    }
    
    /**
     * 将CtClass注入系统中
     * @param api
     * @param impl
     */
    private void processCtClass2Archive(ClassLoader loader, JARArchive archive, Class<?> api, CtClass impl) {
        try {
            String path = "WEB-INF/classes/" + impl.getName().replace('.', '/') + ".class";
            archive.add(new ByteArrayAsset(impl.toBytecode()), path);
//            Class<?> clazz = impl.toClass(loader, null);
//            archive.add(new ClassAsset(clazz), path);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * (指令回调接口)
     * @param jj
     * @param aInfo
     */
    private void buildBindingRest(JdeJst jj, WeixinActivatorInfo aInfo, Class<?> superClass, Class<?> interfaceClass, String className, 
            String swaggerValue, String pathValue, boolean hasInterface, Consumer<JJClass> acceptThen) {
        String pkgname = aInfo.getActivatorPackageName() + ".api";
        String clsname = className;
        JJClass jjApi = null;
        if (hasInterface) {
            jjApi = jj.createInterface(interfaceClass, clsname, pkgname);
            // 注释说明
            addDocComment(jjApi);
            // 注解
            jjApi._import(Api.class);
            JAnnotation anno = jjApi.annotate(Api.class);
            jjApi.annotateValue(anno, "value", aInfo.getActivatorName() + ":" + swaggerValue);
            jjApi._import(Path.class);
            anno = jjApi.annotate(Path.class);
            jjApi.annotateValue(anno, "value", pathValue);
            // 继承
            jjApi._import(interfaceClass);
            jjApi._extends(interfaceClass);
            // 重命名
            pkgname = aInfo.getActivatorPackageName() + ".impl";
            clsname += "Impl";
        }
        // 微信回调接口
        JJClass jjImpl = jj.createClass(superClass, clsname, pkgname);
        // 注释说明
        addDocComment(jjImpl);
        // 继承
        jjImpl._import(superClass);
        jjImpl._extends(superClass);
        
        if (jjApi == null) {
            jjImpl._import(interfaceClass);
            jjImpl._implements(interfaceClass);
            
            jjImpl._import(Path.class);
            JAnnotation anno = jjImpl.annotate(Path.class);
            jjImpl.annotateValue(anno, "value", pathValue);
        } else {
            jjImpl._implements(jjApi.getCanonicalName());
        }
        // 注解
        jjImpl._import(ApplicationScoped.class);
        jjImpl.annotate(ApplicationScoped.class);
        
        // getMessageControllerKey
        JJMethod jjm = jjImpl.method(JMod.PROTECTED, String.class, "getMessageControllerKey");
        jjm.getJMethodDef().body()._return(JExprs.str(className));
        
        // createWxConfig
        buildCallCdiInjectMethod(JMod.PROTECTED, jjImpl, aInfo.getActivatorName(), "createWxConfig", WxConfig.class);
        
        // other
        if (acceptThen != null) {
            acceptThen.accept(jjImpl);
        }
    }
    
    /**
     * 使用CdiUtils注入调用的内容
     * @param jMod
     * @param jj
     * @param named
     * @param methodName
     * @param infClass
     */
    private void buildCallCdiInjectMethod(int jMod, JJClass jj, String named, String methodName, Class<?> infClass) {
        JJMethod jjm = jj.method(jMod, infClass, methodName);
        if (named != null) {
            jj._import(Named.class);
            JAnnotation anno = jjm.annotate(Named.class);
            jjm.annotateValue(anno, "value", named);
        }
        JBlock jbk = jjm.getJMethodDef().body();
        JCall jcl = JExprs.callStatic(CdiUtils.class, CdiUtils.MED_DEF_QUALIFIER);
        jcl.arg(JTypes.typeOf(infClass).field("class"));
        jbk._return(jcl);
    }

    /**
     * 构建激活器
     * @param jj
     * @param aInfo
     * @param activatorClass
     * @param restApi
     */
    private void buildActivator(JdeJst jj, WeixinActivatorInfo aInfo, Class<?> superClass, Consumer<JJClass> acceptThen) {
        JJClass jjc = jj.createClass(superClass, aInfo.getActivatorClassName(), aInfo.getActivatorPackageName());
        // 注释说明
        addDocComment(jjc);
        // 继承
        jjc._import(WxConfig.class);
        jjc._import(superClass);
        
        jjc._implements(WxConfig.class);
        jjc._extends(superClass);
        // 注解
        jjc._import(ApplicationScoped.class);
        jjc._import(Named.class);
        
        JAnnotation anno = jjc.annotate(ApplicationScoped.class);
        anno = jjc.annotate(Named.class);
        jjc.annotateValue(anno, "value", aInfo.getActivatorName());
        
        // isStdInject
        JJMethod jjm = jjc.method(JMod.PUBLIC, boolean.class, "isStdInject");
        jjm.getJMethodDef().body()._return(JExpr.TRUE);
        
        // isMulitMode
        jjm = jjc.method(JMod.PUBLIC, boolean.class, "isMulitMode");
        jjm.getJMethodDef().body()._return(aInfo.isMulitMode() ? JExpr.TRUE : JExpr.FALSE);
        
        // getAppUniqueKey
        jjm = jjc.method(JMod.PUBLIC, String.class, "getAppUniqueKey");
        jjm.getJMethodDef().body()._return(JExprs.str(aInfo.getNameKey()));
        
        // getTempFileName
        if (aInfo.isDebug()) {
            jjm = jjc.method(JMod.PROTECTED, String.class, "getTempFileName");
            jjm.getJMethodDef().body()._return(JExprs.str("target/token_" + aInfo.getNameKey() + ".obj"));
        }
        // protected WxTopologyHandler createTopologyHandler
        if (aInfo.getTopologyNamed() != null) {
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, aInfo.getTopologyNamed(), "createTopologyHandler", WxTopologyHandler.class);
        }
        // protected WxConfigHandler createWxConfigHandler
        if (aInfo.getConfigNamed() != null) {
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, aInfo.getConfigNamed(), "createWxConfigHandler", WxConfigHandler.class);
        }
        
        // other
        if (acceptThen != null) {
            acceptThen.accept(jjc);
        }
    }
    
    /**
     * 注释说明
     * @param jjc
     */
    private void addDocComment(JJClass jjc) {
        JDocComment comment = jjc.getJClassDef().docComment();
        comment.text("Follow the implementation of the restful 2.0 standard remote access agent.");
        comment.htmlTag("see", true).text("https://suisrc.github.io/jaxrsapi");
        comment.htmlTag("generateBy", true).text(ClientServiceFactory.class.getCanonicalName());
        comment.htmlTag("time", true).text(LocalDateTime.now().toString());
        comment.htmlTag("author", true).text("Y13");
    }
    
    //---------------------------------------------------------------------TODO 20180821，对共通内容进行合并一下内容暂时保留
    
    /**
     * 小程序配置
     * @param index
     * @param archive
     * @param nameKey
     * @param isMulitMode
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void processSp(IndexView index, JARArchive archive, String nameKey, boolean isMulitMode, String type) throws Exception {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        
        WeixinActivatorInfo aInfo = WeixinActivatorInfo.create(nameKey, loader, logger, SpTokenServiceRest.class.getPackage().getName(), type);
        aInfo.setMulitMode(isMulitMode);
        aInfo.setNameKey(nameKey);

        JdeJst jj = new JdeJst();
        if (aInfo.getSourcePath() != null) {
            // 暂时生成的代码
            jj.setShowSrc(true);
            jj.setTarget(new File(aInfo.getSourcePath()));
        }
        // 构建Activator
        buildActivator(jj, aInfo, SpActivator.class, jjc -> {
            
            // getApiImplement
            JJMethod jjm = jjc.method(JMod.PUBLIC, Object.class, "getApiImplement");
            JParamDeclaration jpd = jjm.getJMethodDef().param(Class.class, "type");
            JBlock jbk = jjm.getJMethodDef().body();
            JCall jcl = JExprs.call("getApiImplement2");
            jcl.arg(JExprs.$v(jpd));
            jbk._return(jcl);
            
            // protected SpTokenServiceRest createTokenRest()
            String named = null;
            if (isMulitMode) {
                named = aInfo.getActivatorName() + JaxrsConsts.separator + SpTokenServiceRest.class.getName();
            }
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, named, "createTokenRest", SpTokenServiceRest.class);
        });
        // 构建Binding
        Class<?> restApi = aInfo.getConfigNamed() == null ? WxBindingRest.class : WxKeyBindingRest.class;
        buildBindingRest(jj, aInfo, SpWxBinding.class, restApi, aInfo.getBindingClassName(), "微信小程序消息回调接口", 
                aInfo.getNameKey() + "/binding", aInfo.isSwaggerApi(), null);

        // 将代码写出部署环境中
        jj.getClassMap().values().forEach(JJClass::imports);
        Map<Object, CtClass> ctClasses = jj.writeSource();
        // 构建接口信息
        ClientServiceFactory.processIndex(aInfo, index, ctClasses::put, "_" + nameKey, true, aInfo.getSourcePath());
        // 代码写出到部署环境中
        ctClasses.entrySet().forEach(v -> this.processCtClass2Archive(loader, archive, (Class<?>) v.getKey(), v.getValue()));
    }
    
    /**
     * 公众号配置
     * @param index
     * @param archive
     * @param nameKey
     * @param isMulitMode
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void processMp(IndexView index, JARArchive archive, String nameKey, boolean isMulitMode, String type) throws Exception {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        
        WeixinActivatorInfo aInfo = WeixinActivatorInfo.create(nameKey, loader, logger, MpTokenServiceRest.class.getPackage().getName(), type);
        aInfo.setMulitMode(isMulitMode);
        aInfo.setNameKey(nameKey);

        JdeJst jj = new JdeJst();
        if (aInfo.getSourcePath() != null) {
            // 暂时生成的代码
            jj.setShowSrc(true);
            jj.setTarget(new File(aInfo.getSourcePath()));
        }
        // 构建Activator
        buildActivator(jj, aInfo, MpActivator.class, jjc -> {
            
            // getApiImplement
            JJMethod jjm = jjc.method(JMod.PUBLIC, Object.class, "getApiImplement");
            JParamDeclaration jpd = jjm.getJMethodDef().param(Class.class, "type");
            JBlock jbk = jjm.getJMethodDef().body();
            JCall jcl = JExprs.call("getApiImplement2");
            jcl.arg(JExprs.$v(jpd));
            jbk._return(jcl);
            
            // protected MainServiceRest createTokenRest()
            String named = null;
            if (isMulitMode) {
                named = aInfo.getActivatorName() + JaxrsConsts.separator + MpTokenServiceRest.class.getName();
            }
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, named, "createTokenRest", MpTokenServiceRest.class);
        });
        // 构建Binding
        Class<?> restApi = aInfo.getConfigNamed() == null ? WxBindingRest.class : WxKeyBindingRest.class;
        buildBindingRest(jj, aInfo, MpWxBinding.class, restApi, aInfo.getBindingClassName(), "微信公众号消息回调接口", 
                aInfo.getNameKey() + "/binding", aInfo.isSwaggerApi(), null);

        // 将代码写出部署环境中
        jj.getClassMap().values().forEach(JJClass::imports);
        Map<Object, CtClass> ctClasses = jj.writeSource();
        // 构建接口信息
        ClientServiceFactory.processIndex(aInfo, index, ctClasses::put, "_" + nameKey, true, aInfo.getSourcePath());
        // 代码写出到部署环境中
        ctClasses.entrySet().forEach(v -> this.processCtClass2Archive(loader, archive, (Class<?>) v.getKey(), v.getValue()));
    }
    
    /**
     * 企业号配置
     * @param index
     * @param archive
     * @param nameKey
     * @param isMulitMode
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void processQm(IndexView index, JARArchive archive, String nameKey, boolean isMulitMode, String type) throws Exception {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        
        WeixinActivatorInfo aInfo = WeixinActivatorInfo.create(nameKey, loader, logger, QyMainServiceRest.class.getPackage().getName(), type);
        aInfo.setMulitMode(isMulitMode);
        aInfo.setNameKey(nameKey);

        JdeJst jj = new JdeJst();
        if (aInfo.getSourcePath() != null) {
            // 暂时生成的代码
            jj.setShowSrc(true);
            jj.setTarget(new File(aInfo.getSourcePath()));
        }
        // 构建Activator
        buildActivator(jj, aInfo, QyThreeActivator.class, jjc -> {
            
            // getApiImplement
            JJMethod jjm = jjc.method(JMod.PUBLIC, Object.class, "getApiImplement");
            JParamDeclaration jpd = jjm.getJMethodDef().param(Class.class, "type");
            JBlock jbk = jjm.getJMethodDef().body();
            JCall jcl = JExprs.call("getApiImplement2");
            jcl.arg(JExprs.$v(jpd));
            jbk._return(jcl);
            
            // protected MainServiceRest createTokenRest()
            String named = null;
            if (isMulitMode) {
                named = aInfo.getActivatorName() + JaxrsConsts.separator + QyMainServiceRest.class.getName();
            }
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, named, "createTokenRest", QyMainServiceRest.class);
        });
        // 构建Binding
        Class<?> restApi = aInfo.getConfigNamed() == null ? WxBindingRest.class : WxKeyBindingRest.class;
        buildBindingRest(jj, aInfo, QmWxBinding.class, restApi, aInfo.getBindingClassName(), "企业微信消息回调接口", 
                aInfo.getNameKey() + "/binding", aInfo.isSwaggerApi(), null);

        // 将代码写出部署环境中
        jj.getClassMap().values().forEach(JJClass::imports);
        Map<Object, CtClass> ctClasses = jj.writeSource();
        // 构建接口信息
        ClientServiceFactory.processIndex(aInfo, index, ctClasses::put, "_" + nameKey, true, aInfo.getSourcePath());
        // 代码写出到部署环境中
        ctClasses.entrySet().forEach(v -> this.processCtClass2Archive(loader, archive, (Class<?>) v.getKey(), v.getValue()));
    }

    /**
     * 企业应用配置
     * @param index
     * @param archive
     * @param nameKey
     * @param isMulitMode
     * @throws Exception 
     */
    @SuppressWarnings("unused")
    @Deprecated
    private void processQt(IndexView index, JARArchive archive, String nameKey, boolean isMulitMode, String type) throws Exception {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        
        WeixinActivatorInfo aInfo = WeixinActivatorInfo.create(nameKey, loader, logger, QyThreeServiceRest.class.getPackage().getName(), type);
        aInfo.setMulitMode(isMulitMode);
        aInfo.setNameKey(nameKey);

        JdeJst jj = new JdeJst();
        if (aInfo.getSourcePath() != null) {
            // 暂时生成的代码
            jj.setShowSrc(true);
            jj.setTarget(new File(aInfo.getSourcePath()));
        }
        // 构建Activator
        buildActivator(jj, aInfo, QyThreeActivator.class, jjc -> {
            
            // getApiImplement
            JJMethod jjm = jjc.method(JMod.PUBLIC, Object.class, "getApiImplement");
            JParamDeclaration jpd = jjm.getJMethodDef().param(Class.class, "type");
            JBlock jbk = jjm.getJMethodDef().body();
            JCall jcl = JExprs.call("getApiImplement2");
            jcl.arg(JExprs.$v(jpd));
            jbk._return(jcl);
            
            // protected ThreeServiceRest createTokenRest()
            String named = null;
            if (isMulitMode) {
                named = aInfo.getActivatorName() + JaxrsConsts.separator + QyThreeServiceRest.class.getName();
            }
            buildCallCdiInjectMethod(JMod.PROTECTED, jjc, named, "createTokenRest", QyThreeServiceRest.class);
            // protected PermanentHandler createPermanentHandler
            if (aInfo.getPermanentNamed() != null) {
                buildCallCdiInjectMethod(JMod.PROTECTED, jjc, aInfo.getPermanentNamed(), "createPermanentHandler", PermanentHandler.class);
            }
        });
        // 构建Binding
        Class<?> restApi = aInfo.getConfigNamed() == null ? WxBindingRest.class : WxKeyBindingRest.class;
        buildBindingRest(jj, aInfo, QyWxBinding.class, restApi, aInfo.getBindingClassName(), "企业微信应用消息回调接口", 
                aInfo.getNameKey() + "/binding", aInfo.isSwaggerApi(), null);
        // 构建Receive
        restApi = aInfo.getConfigNamed() == null ? QySuiteReceiveRest.class : QyKeySuiteReceiveRest.class;
        buildBindingRest(jj, aInfo, QySuiteReceive.class, restApi, aInfo.getReceiveClassName(), "企业微信应用指令回调接口", 
                aInfo.getNameKey() + "/suite", aInfo.isSwaggerApi(), null);

        // 将代码写出部署环境中
        jj.getClassMap().values().forEach(JJClass::imports);
        Map<Object, CtClass> ctClasses = jj.writeSource();
        // 构建接口信息
        ClientServiceFactory.processIndex(aInfo, index, ctClasses::put, "_" + nameKey, true, aInfo.getSourcePath());
        // 代码写出到部署环境中
        ctClasses.entrySet().forEach(v -> this.processCtClass2Archive(loader, archive, (Class<?>) v.getKey(), v.getValue()));
    }
    
}
