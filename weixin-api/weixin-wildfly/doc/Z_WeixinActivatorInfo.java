package com.suisrc.weixin.wildfly.runtime;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;

import com.suisrc.core.annotation.ScConfig;
import com.suisrc.core.utils.ComUtils;
import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.jaxrsapi.core.ApiActivatorInfo;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.three.weixin.core.CoreWxConsts;
import net.icgear.three.weixin.core.annotation.WxApi;

/**
 * 微信激活器信息
 * @author Y13
 *
 */
public class WeixinActivatorInfo implements ApiActivatorInfo {
    

    /**
     * 获取企业微信信息内容
     * @param nameKey
     * @param loader
     * @throws Exception 
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    static WeixinActivatorInfo create(String nameKey, ClassLoader loader, Logger logger, String pkg, String type) throws Exception {
        WeixinActivatorInfo aInfo = new WeixinActivatorInfo();
        
        ComUtils.setConfigProperty(aInfo, (config, clazz) -> {
            // 构建检索KEY
            String key = CoreWxConsts.WEIXIN_PRE + nameKey + "." + config.value();
            
            if ("remote-api".equals(config.name())) {
                // 该内容需要特殊处理
                try {
                    return getRemoteApi(key, loader, pkg, type);
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
            Class cls = clazz;
            if (cls == String.class) {
                String content = System.getProperty(key);
                if (content != null) {
                    return content;
                }
                if (config.defaultValue().isEmpty()) {
                    return null;
                }

                String name = nameKey.substring(0, 1).toUpperCase() + nameKey.substring(1);
                String value = config.defaultValue().replace("{name}", name);
                return value;
            }
            if (cls == boolean.class) {
                if (config.defaultValue().isEmpty()) {
                    return Boolean.getBoolean(key);
                } else {
                    if (System.getProperty(key) == null) {
                        return Boolean.valueOf(config.defaultValue());
                    }
                    return Boolean.getBoolean(key);
                }
            }
            return null;
        });
        String[] contentArray = ComUtils.getSystemProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.remote-api.*");
        if (contentArray == null) {
            // 需要处理的接口
            Set<Class<?>> classes = ReflectionUtils.getTypesAnnotatedWith(RemoteApi.class, true, pkg);
            if (type != null) {
                // 类型验证失败
                // c.getAnnotation(WxApi.class)
                // 该方法无效
                // 由于运行时加载WxApi和加载RestClass是两个classloader,似乎WxApi被加载了两次
                // 所以这里执行了特殊的写法
                Class classWxApi = loader.loadClass(WxApi.class.getName());
                Set<Class<?>> clazzes = classes.stream().filter(c -> {
                    Annotation wxapi = c.getAnnotation(classWxApi); // 该方法无法正确获取注解内容
                    if (wxapi == null) {
                        return true;
                    }
                    String[] types = (String[]) ReflectionUtils.invokeGetMethod(wxapi, "types");
                    if (types == null || types.length == 0) {
                        return true;
                    }
                    return Arrays.asList(types).stream().anyMatch(type::equals);
                }).collect(Collectors.toSet());
                aInfo.setRestClasses(clazzes);
            } else {
                aInfo.setRestClasses(classes);
            }
        } else {
            Set<Class<?>> classes = new HashSet<>();
            for (String classStr : contentArray) {
                try {
                    classes.add(loader.loadClass(classStr));
                } catch (ClassNotFoundException e) {
                    logger.warning("加载微信接口类发生异常：" + e.getMessage());
                }
            }
            aInfo.setRestClasses(classes);
        }
        String name = nameKey.substring(0, 1).toUpperCase() + nameKey.substring(1);
        
        String content = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.named");
        if (content != null) {
            aInfo.setActivatorName(content);
        } else {
            aInfo.setActivatorName(name + "Activator");
        }
        content = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.activator-class");
        if (content != null) {
            aInfo.setActivatorClassName(content);
        } else {
            aInfo.setActivatorClassName(aInfo.getActivatorName());
        }
        content = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.package-name");
        if (content != null) {
            aInfo.setActivatorPackageName(content);
        } else {
            aInfo.setActivatorPackageName("com.suisrc.weixin.wildfly." + nameKey);
        }
        content = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.binding-class");
        if (content != null) {
            aInfo.setBindingClassName(content);
        } else {
            aInfo.setBindingClassName(name + "BindingRest");
        }
        content = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.receive-class");
        if (content != null) {
            aInfo.setReceiveClassName(content);
        } else {
            aInfo.setReceiveClassName(name + "ReceiveRest");
        }
        if (Boolean.getBoolean(CoreWxConsts.WEIXIN_PRE + nameKey + ".debug")) {
            aInfo.setDebug(true);
        }
        content = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.test-source");
        if (content != null) {
            aInfo.setSourcePath(content);
        }
        if (Boolean.getBoolean(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.swagger-api")) {
            aInfo.setSwaggerApi(true);
        }
        content = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.topology-named");
        if (content != null) {
            aInfo.setTopologyNamed(content);
        }
        content = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.permanent-named");
        if (content != null) {
            aInfo.setPermanentNamed(content);
        }
        content = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.config-named");
        if (content != null) {
            aInfo.setConfigNamed(content);
        }
        content = System.getProperty(CoreWxConsts.WEIXIN_PRE + nameKey + ".build.pay-config-named");
        if (content != null) {
            aInfo.setPayConfigNamed(content);
        }
        
        return aInfo;
    }
    
    /**
     * 获取远程API接口
     * @return
     * @throws ClassNotFoundException 
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    private static Set<Class<?>> getRemoteApi(String key, ClassLoader loader, String pkg, String type) throws ClassNotFoundException {
        String[] contentArray = ComUtils.getSystemProperty(key);
        if (contentArray == null) {
            // 需要处理的接口
            Set<Class<?>> classes = ReflectionUtils.getTypesAnnotatedWith(RemoteApi.class, true, pkg);
            if (type != null) {
                // 类型验证失败
                // c.getAnnotation(WxApi.class)
                // 该方法无效
                // 由于运行时加载WxApi和加载RestClass是两个classloader,似乎WxApi被加载了两次
                // 所以这里执行了特殊的写法
                Class classWxApi = loader.loadClass(WxApi.class.getName());
                Set<Class<?>> clazzes = classes.stream().filter(c -> {
                    Annotation wxapi = c.getAnnotation(classWxApi); // 该方法无法正确获取注解内容
                    if (wxapi == null) {
                        return true;
                    }
                    String[] types = (String[]) ReflectionUtils.invokeGetMethod(wxapi, "types");
                    if (types == null || types.length == 0) {
                        return true;
                    }
                    return Arrays.asList(types).stream().anyMatch(type::equals);
                }).collect(Collectors.toSet());
                return clazzes;
            } else {
                return classes;
            }
        } else {
            Set<Class<?>> classes = new HashSet<>();
            for (String classStr : contentArray) {
                classes.add(loader.loadClass(classStr));
            }
            return classes;
        }
    }
    
    /**
     * 绑定到激活器上的接口列表
     */
    @ScConfig(value="build.remote-api.*", name="remote-api")
    private Set<Class<?>> restClasses;
    
    /**
     * 部署模式，是否为多激活器模式
     */
    private boolean mulitMode = true;
    
    /**
     * 激活器的名称
     */
    @ScConfig(value="build.named", defaultValue="{name}Activator")
    private String activatorName;
    
    /**
     * 激活器的类名
     */
    private String activatorClassName;
    
    /**
     * 激活器所在的包名
     */
    private String activatorPackageName;
    
    /**
     * 绑定数据接口类型
     */
    private String bindingClassName;
    
    /**
     * 绑定指令接口类型
     */
    private String receiveClassName;
    
    /**
     * 调试
     */
    private boolean debug = false;
    
    /**
     * 源代码位置
     */
    private String sourcePath = null;
    
    /**
     * 关键字
     */
    private String nameKey;
    
    /**
     * 是否暴露swagger接口
     */
    private boolean swaggerApi = false;
    
    /**
     * 微信配置别名
     */
    private String configNamed;
    
    /**
     * 注入的集群内容控制器
     */
    private String topologyNamed = null;
    
    /**
     * 注入的企业信息控制器
     */
    private String permanentNamed = null;
    
    /**
     * 支付配置别名
     */
    private String payConfigNamed = null;
    
    
    public String getTopologyNamed() {
        return topologyNamed;
    }

    public void setTopologyNamed(String topologyNamed) {
        this.topologyNamed = topologyNamed;
    }

    public String getPermanentNamed() {
        return permanentNamed;
    }

    public void setPermanentNamed(String permanentNamed) {
        this.permanentNamed = permanentNamed;
    }

    @Override
    public Set<Class<?>> getClasses() {
        return restClasses;
    }

    @Override
    public boolean isMulitMode() {
        return mulitMode;
    }

    @Override
    public String getActivatorName() {
        return activatorName;
    }

    @Override
    public String getActivatorClassName() {
        return activatorClassName;
    }

    @Override
    public String getActivatorPackageName() {
        return activatorPackageName;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(String key, Class<T> type) {
        if (type == String.class) {
            return (T) System.getProperty(key);
        }
        if (type == Boolean.class) {
            boolean res = Boolean.getBoolean(key);
            return (T) Boolean.valueOf(res);
        }
        return null;
    }

    @Override
    public boolean isStdInject() {
        return true;
    }

    public Set<Class<?>> getRestClasses() {
        return restClasses;
    }

    public void setRestClasses(Set<Class<?>> restClasses) {
        this.restClasses = restClasses;
    }


    public void setMulitMode(boolean mulitMode) {
        this.mulitMode = mulitMode;
    }

    public void setActivatorName(String activatorName) {
        this.activatorName = activatorName;
    }

    public void setActivatorClassName(String activatorClassName) {
        this.activatorClassName = activatorClassName;
    }

    public void setActivatorPackageName(String activatorPackageName) {
        this.activatorPackageName = activatorPackageName;
    }

    public String getBindingClassName() {
        return bindingClassName;
    }

    public void setBindingClassName(String bindingClassName) {
        this.bindingClassName = bindingClassName;
    }

    public String getReceiveClassName() {
        return receiveClassName;
    }

    public void setReceiveClassName(String receiveClassName) {
        this.receiveClassName = receiveClassName;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public void setSourcePath(String sourcePath) {
        this.sourcePath = sourcePath;
    }

    public void setNameKey(String nameKey) {
        this.nameKey = nameKey;
    }
    
    public String getNameKey() {
        return this.nameKey;
    }

    public void setSwaggerApi(boolean swaggerApi) {
        this.swaggerApi = swaggerApi;
    }
    
    public boolean isSwaggerApi() {
        return swaggerApi;
    }

    public String getConfigNamed() {
        return configNamed;
    }

    public void setConfigNamed(String configNamed) {
        this.configNamed = configNamed;
    }

    public String getPayConfigNamed() {
        return payConfigNamed;
    }

    public void setPayConfigNamed(String payConfigNamed) {
        this.payConfigNamed = payConfigNamed;
    }
    
}
