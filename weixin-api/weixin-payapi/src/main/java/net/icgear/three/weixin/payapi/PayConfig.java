package net.icgear.three.weixin.payapi;

import javax.ws.rs.client.Client;

/**
 * 支付配置信息
 * @author Y13
 *
 */
public interface PayConfig {
    
    /**
     * 商铺ID
     * 
     * String(32)
     * 
     * @return
     */
    String getMchId();
    
    /**
     * 商铺密钥
     * @return
     */
    String getMchKey();
    
    /**
     * 商铺名称
     */
    String getSendName();
    
    /**
     * 客户端IP
     */
    String getClientIP();
    
    /**
     * 自动生成订单编号
     * 
     * String(28)
     */
    String getAutoMchBillno();
    
    /**
     * 获取随机数
     */
    String getRandomStr();
    
    /**
     * ClientBuilder: 配置证书
     * Client: 注册客户端信息
     * 
     * 目前只会回调这两种类型
     */
    void registerByConfig(String key, Object component);
    
    /**
     * 获取缓存中的客户端
     * @return
     */
    Client getClientByConfig(String key);

}
