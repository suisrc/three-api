package net.icgear.three.weixin.payapi.handler;

import net.icgear.three.weixin.payapi.PayConfig;

/**
 * 微信配置获取助手
 * 
 * 只有在使用多配置信息截获器时候，该内容才有效
 * 
 * @author Y13
 *
 */
public interface PayConfigHandler {
    
    /**
     * 获取微信配置信息
     * @param appKey
     * @return
     */
    PayConfig findByAppKey(String appKey);

}
