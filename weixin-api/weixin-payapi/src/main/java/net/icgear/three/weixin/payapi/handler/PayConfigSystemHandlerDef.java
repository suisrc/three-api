package net.icgear.three.weixin.payapi.handler;

import javax.enterprise.context.Dependent;
import javax.inject.Named;

import com.suisrc.jaxrsapi.core.ApiActivator;

import net.icgear.three.weixin.core.AbstractCommonActivator;

/**
 * 微信支付配置获取助手
 * 
 * 只有在使用多配置信息截获器时候，该内容才有效
 * 
 * @author Y13
 *
 */
@Named("weixin-pay-config-default")
@Dependent
public class PayConfigSystemHandlerDef extends PayConfigSystemHandler implements PayConfigHandler2 {
    
    private String uniqueKey;

    @Override
    public void setActivator(ApiActivator activator) {
        this.uniqueKey = activator.as(AbstractCommonActivator.class).getAppUniqueKey();
    }

    @Override
    protected String getAppUniqueKey() {
        return uniqueKey;
    }

}
