package net.icgear.three.weixin.payapi;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Base64;

import javax.ws.rs.client.ClientBuilder;

import com.suisrc.core.annotation.ScConfig;
import com.suisrc.core.utils.CoreUtils;
import com.suisrc.jaxrsapi.client.ClientUtils;

import net.icgear.three.weixin.core.WxConsts;

/**
 * 微信配置接口
 * @author Y13
 *
 */
public class PayConfigSystem extends PayConfigBase {
    
    private String uniqueKey;
    
    @ScConfig("mch-id")
    private String mchId;

    @ScConfig("mch-key")
    private String mchKey;

    @ScConfig("send-name")
    private String sendName;

    @ScConfig("client-ip")
    private String clientIP;

    @ScConfig("pkcs12")
    private byte[] pkcs12 = null;
    
    /**
     * 构造方法
     */
    public PayConfigSystem(String uniqueKey) {
        this.uniqueKey = uniqueKey;
        doPostConstruct();
    }
    
    /**
     * 绑定的对唯一标识
     */
    public String getAppUniqueKey() {
        return uniqueKey;
    }

    /**
     * 获取配置信息
     * @param key
     * @return
     */
    protected String geConfigKey(String key) {
        return WxConsts.WEIXIN_PRE + getAppUniqueKey() + "." + key;
    }

    /**
     * 执行单配置初始化
     */
    protected void doPostConstruct() {
        CoreUtils.setConfigProperty(this, (config, type) -> {
            String key = geConfigKey(config.value());
            if ("pkcs12".equals(config.name())) {
                String pkcs12Str = System.getProperty(key);
                // 删除空白符，在base64中是没有空白符的
                if (pkcs12Str != null && !pkcs12Str.isEmpty()) {
                    pkcs12 = Base64.getDecoder().decode(pkcs12Str.replaceAll("\\s", ""));
                }
                return null;
            } else {
                if (config.defaultValue().isEmpty()) {
                    return System.getProperty(key);
                } else {
                    return System.getProperty(key, config.defaultValue());
                }
            }
        });
        billnoPrefix = System.getProperty(geConfigKey("billno-prefix"));
    }

    /**
     * 
     */
    public String getMchId() {
        return mchId;
    }

    /**
     * 
     */
    public String getMchKey() {
        return mchKey;
    }

    /**
     * 
     */
    public String getSendName() {
        return sendName;
    }

    /**
     * 
     */
    public String getClientIP() {
        if (clientIP == null) {
            clientIP = ClientUtils.getLocalIpByRemoteUrlRex();
        }
        // if (clientIP == null) {
        //     clientIP = getLocalIP();
        // }
        return clientIP;
    }
    
    /**
     * 获取本地IP
     * @return
     */
    protected String getLocalIP() {
      try {
          return InetAddress.getLocalHost().getHostAddress();
      } catch (UnknownHostException e) {
          throw new RuntimeException(e);
      }
    }

    /**
     * 注册加密方式
     */
    @Override
    protected void registerKeyStore(String key, ClientBuilder builder) {
        if (PayConsts.PKCS12.equals(key)) {
            // 客户端需要双向认证
            try {
                KeyStore keyStore = KeyStore.getInstance("PKCS12");
                InputStream instream = new ByteArrayInputStream(pkcs12);
                keyStore.load(instream, getMchId().toCharArray());
                instream.close();
                builder.keyStore(keyStore, getMchId());
            } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
                throw new RuntimeException(e); // 读取证书的时候发生异常
            }
        }
    }

    /**
     * 加密文件
     * @param file
     * @throws IOException
     */
    public static String encode(String file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[fis.available()];
        fis.read(data);
        fis.close();
        return Base64.getEncoder().encodeToString(data);
    }

    /**
     * 加密文件
     * @param file
     * @throws IOException
     */
    public static String encodeLine(String file, int len) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[fis.available()];
        fis.read(data);
        fis.close();
        String msg = Base64.getEncoder().encodeToString(data);
        if (msg.length() <= len) {
            return msg;
        }
        StringBuilder sbir = new StringBuilder();
        int offset = 0;
        while (offset + len < msg.length()) {
            if (sbir.length() > 0) {
                sbir.append("\n");
            }
            sbir.append(msg.substring(offset, offset + len));
            offset += len;
        }
        if (offset < msg.length()) {
            sbir.append("\n");
            sbir.append(msg.substring(offset));
        }
        return sbir.toString();
    }
}
