package net.icgear.three.weixin.payapi;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import net.icgear.three.weixin.core.crypto.WxCrypto;

/**
 * 微信支付配置接口
 * 
 * @author Y13
 *
 */
public abstract class PayConfigBase implements PayConfig {

    /**
     * 格式化句柄
     */
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");

    /**
     * 纠错码
     * 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz
     */
    private final String base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    /**
     * 
     */
    private final int baseLen2 = base.length() * base.length();
    
    /**
     * 
     */
    private final int mchIdLen = 9;
    
    /**
     * 上次执行生成的时间
     */
    private volatile long preTime = 0;
    
    /**
     * 偏移
     */
    private volatile int offset = 0;
    
    
    /**
     * 上次执行生成的时间
     */
    
    /**
     * 远程访问的客户端
     */
    protected Map<String, Client> clients = null;
    
    /**
     * 订单前缀
     * 
     * 如果没有，商品ID后9位
     */
    protected String billnoPrefix = null;
    
    /**
     * 
     * @return
     */
    private String getBillnoPrefix() {
        if (billnoPrefix == null) {
            billnoPrefix = getMchId();
            if (billnoPrefix == null) {
                billnoPrefix = "xxxxx"; // 无法获取店铺号
            } else if (billnoPrefix.length() > mchIdLen) {
                // 取门店后10位
                billnoPrefix = billnoPrefix.substring(billnoPrefix.length() - mchIdLen);
            }
        }
        return billnoPrefix;
    }

    /**
     * String(28)
     * 
     * 微信说明：
     * 商户订单号（每个订单号必须唯一）组成： mch_id+yyyymmdd+10位一天内不能重复的数字。接口根据商户订单号支持重入， 如出现超时可再调用。
     * 生成说明：
     *  mch_id+yyyyMMddHHmmssSSS+1
     */
    @Override
    public synchronized String getAutoMchBillno() {
        long now = System.currentTimeMillis();
        if (now == preTime) {
            if (++offset >= baseLen2) {
                try {
                    // 暂停1毫秒
                    // 注意，该操作是危险的，慎用
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    // 暂停被中断
                } 
                now = System.currentTimeMillis();
                preTime = now;
                offset = 0;
            }
        } else {
            preTime = now;
            offset = 0;
        }
        int offset1 = offset / base.length();
        int offset2 = offset % base.length();
        return getBillnoPrefix() + sdf.format(new Date(now)) + base.charAt(offset1) + base.charAt(offset2);
    }

    /**
     * 
     */
    @Override
    public String getRandomStr() {
        return WxCrypto.genRandomStr();
    }

    /**
     * 
     */
    @Override
    public void registerByConfig(String key, Object component) {
        if (component instanceof ClientBuilder) {
            registerKeyStore(key, (ClientBuilder)component);
        } else if (component instanceof Client) {
            setClient(key, (Client)component);
        }
    }

    /**
     * 注册加密方式
     * @param builder
     */
    protected abstract void registerKeyStore(String key, ClientBuilder builder);

    /**
     * 对Client进行缓存
     * @param client
     */
    public synchronized void setClient(String key, Client client) {
        if (client == null && clients == null) {
            return;
        }
        if (client == null) {
            Client old = clients.remove(key);
            if (old != null) {
                old.close();
            }
            if (clients.isEmpty()) {
                clients = null;
            }
        }
        if (clients == null) {
            clients = new ConcurrentHashMap<>();
        }
        Client old = clients.put(key, client);
        if (old != null) {
            old.close();
        }
        
    }

    /**
     * 获取缓存的Client
     */
    @Override
    public Client getClientByConfig(String key) {
        if (key == null) {
            // 防止空指针
            key = "";
        }
        return clients == null ? null : clients.get(key);
    }

}
