package net.icgear.three.weixin.payapi.handler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import net.icgear.three.weixin.payapi.PayConfig;
import net.icgear.three.weixin.payapi.PayConfigSystem;

/**
 * 微信配置获取助手
 * 
 * 只有在使用多配置信息截获器时候，该内容才有效
 * 
 * @author Y13
 *
 */
public abstract class PayConfigSystemHandler implements PayConfigHandler {
    
    /**
     * 配置缓存
     */
    private Map<String, PayConfig> configs = new ConcurrentHashMap<>();
    
    /**
     * 获取微信配置信息
     * @param appKey
     * @return
     */
    public PayConfig findByAppKey(String appKey) {
        PayConfig config = configs.get(appKey);
        if (config == null) {
            config = new PayConfigSystem(getAppUniqueKey() + ".applications." + appKey);
            configs.put(appKey, config);
        }
        return config;
    }
    
    /**
     * 获取应用关键字
     * @return
     */
    protected abstract String getAppUniqueKey();

}
