package net.icgear.three.weixin.payapi;

import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.jboss.resteasy.spi.ResteasyProviderFactory;

import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.three.weixin.core.AbstractCommonActivator;
import net.icgear.three.weixin.core.handler.WxTokenHandler;
import net.icgear.three.weixin.payapi.handler.PayConfigHandler;
import net.icgear.three.weixin.payapi.handler.PayConfigHandler2;
import net.icgear.three.weixin.payapi.rest.api.MmpaymkttransfersRest;

/**
 * 支付系统激活器
 * 
 * 支付部分没有永久驻留Client
 * 
 * Client是按照商户独立存储
 * 
 * @author Y13
 */
public abstract class PayActivator extends AbstractCommonActivator implements PayConfig {

    /**
     * 系统默认支付信息
     */
    private PayConfig payConfig = null;

    /**
     * 微信支付配置信息句柄
     */
    private PayConfigHandler payConfigHandler;
    
    /**
     * 暴露的接口方法
     */
    @Override
    public Set<Class<?>> getClasses() {
        return ReflectionUtils.getTypesAnnotatedWith(RemoteApi.class, true, MmpaymkttransfersRest.class.getPackage().getName());
    }

    /**
     * 获取默认基础链接地址
     */
    @Override
    protected String getDefaultBaseUrl() {
        return "https://api.mch.weixin.qq.com";
    }
    
    /**
     * 支付平台没有token
     */
    @Override
    protected WxTokenHandler getWxTokenHandler(String tokenKey) {
        throw new RuntimeException("支付平台没有token: getWxTokenHandler");
    }

    /**
     * 支付平台没有token
     */
    @Override
    public TokenAtom findTokenAtom(String tokenKey) {
        throw new RuntimeException("支付平台没有token: findTokenAtom");
    }

    /**
     * 支付平台没有token
     */
    @Override
    public TokenAtom saveTokenAtom(String tokenKey, TokenAtom tokenAtom) {
        throw new RuntimeException("支付平台没有token: saveTokenAtom");
    }
    
    // --------------------------------------------重写内容

    /**
     * 生成微信支付配置信息内容
     * 
     * @return
     */
    protected PayConfigHandler createPayConfigHandler() {
        return null;
    }

    /**
     * 构造方法
     */
    @Override
    @PostConstruct
    public void doPostConstruct() {
        // 继续实现构造
        super.doPostConstruct();
        // 微信外挂配置句柄
        payConfigHandler = createPayConfigHandler();
        if (payConfigHandler != null && payConfigHandler instanceof PayConfigHandler2) {
            ((PayConfigHandler2)payConfigHandler).setActivator(this);
        }
        // 配置数据解析器
        // FIXME 当前环境下测试中发现，在2017.11.0版本的框架下，wildlfy-swarm中无法正确的给出xml解析器
        // 矫正resteasy-xml-provider的内容
        setAdapter(null, ResteasyProviderFactory.class, ResteasyProviderFactory.getInstance());
    }
    
    /**
     * 
     */
    @Override
    protected ClientBuilder createClientBuilder(String key) {
        ClientBuilder builder = super.createClientBuilder(key);
        registerByConfig(key, builder);
        return builder;
    }
    
    /**
     * 
     */
    @Override
    protected void registerTargetClient(String key, Client client) {
        super.registerTargetClient(key, client);
        registerByConfig(key, client);
    }

    /**
     * 
     */
    @Override
    protected Client createTargetClient(String key) {
        if (key == null) {
            return null;
        }
        return super.createTargetClient(key);
    }

    /**
     * 
     */
    @Override
    protected Client getClient(String key) {
        if (key == null) {
            key = "";
        }
        Client client = getClientByConfig(key);
        if (client == null) {
            client = createTargetClient(key);
        }
        return client;
    }
    
    /**
     * 适配器
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T getAdapter(String named, Class<T> type) {
        if (named != null && type == String.class) {
            switch (named) {
                case PayConsts.MCH_ID:
                    return (T) getMchId();
                case PayConsts.MCH_KEY:
                    return (T) getMchKey();
                case PayConsts.SEND_NAME:
                    return (T) getSendName();
                case PayConsts.CLIENT_IP:
                    return (T) getClientIP();
                case PayConsts.AUTO_MCH_BILLNO:
                    return (T) getAutoMchBillno();
                case PayConsts.AUTO_RANDOM_STR:
                    return (T) getRandomStr();
            }
        } else if (type == WebTarget.class) {
            return (T) getClient(named).target(getBaseUrl());
        } else if (type == Client.class) {
            return (T) getClient(named);
        }
        return super.getAdapter(named, type);
    }

    // --------------------------------------------PayConfig

    /**
     * 获取WxConfig内容
     * 
     * @return
     */
    protected PayConfig getPayConfig() {
        if (payConfigHandler == null) {
            if(payConfig == null) {
                payConfig = new PayConfigSystem(getAppUniqueKey() + ".endpoint");
            }
            return payConfig;
        }
        String appKey = getRequestAppKey();
        if (appKey == null) {
            return null;
        }
        PayConfig config = payConfigHandler.findByAppKey(appKey);
        if (config == null) {
            throw new RuntimeException("没有找到可用的支付配置信息:" + appKey);
        }
        return config;
    }
    
    /**
     * PayConfig
     */
    @Override
    public String getMchId() {
        PayConfig config = getPayConfig();
        return config == null ? null : config.getMchId();
    }

    /**
     * PayConfig
     */
    @Override
    public String getMchKey() {
        PayConfig config = getPayConfig();
        return config == null ? null : config.getMchKey();
    }

    /**
     * PayConfig
     */
    @Override
    public String getSendName() {
        PayConfig config = getPayConfig();
        return config == null ? null : config.getSendName();
    }

    /**
     * PayConfig
     */
    @Override
    public String getClientIP() {
        PayConfig config = getPayConfig();
        return config == null ? null : config.getClientIP();
    }

    /**
     * PayConfig
     */
    @Override
    public String getAutoMchBillno() {
        PayConfig config = getPayConfig();
        return config == null ? null : config.getAutoMchBillno();
    }

    /**
     * PayConfig
     */
    @Override
    public String getRandomStr() {
        PayConfig config = getPayConfig();
        return config == null ? null : config.getRandomStr();
    }

    /**
     * PayConfig
     */
    @Override
    public void registerByConfig(String key, Object component) {
        PayConfig config = getPayConfig();
        if (config != null) {
            config.registerByConfig(key, component);
        }
    }

    /**
     * PayConfig
     */
    @Override
    public Client getClientByConfig(String key) {
        PayConfig config = getPayConfig();
        return config == null ? null : config.getClientByConfig(key);
    }

}
