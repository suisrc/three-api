package net.icgear.three.weixin.payapi;

/**
 * 常数
 * 
 * @author Y13
 *
 */
public interface PayConsts {
    
    /**
     * 使用加密P12
     */
    String PKCS12 = "PKCS12";

    /**
     * 商铺ID
     */
    String MCH_ID = "MCH_ID";

    /**
     * 商铺密钥
     */
    String MCH_KEY = "MCH_KEY";

    /**
     * 商铺名称
     */
    String SEND_NAME = "SEND_NAME";

    /**
     * 客户端IP
     */
    String CLIENT_IP = "CLIENT_IP";

    /**
     * 订单编号
     */
    String AUTO_MCH_BILLNO = "AUTO_MCH_BILLNO";

    /**
     * 获取随机数
     */
    String AUTO_RANDOM_STR = "AUTO_RANDOM_STR";


}
