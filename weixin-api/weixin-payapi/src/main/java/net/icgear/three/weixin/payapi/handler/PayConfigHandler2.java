package net.icgear.three.weixin.payapi.handler;

import com.suisrc.jaxrsapi.core.ApiActivator;

/**
 * 
 * 回调配置
 * 
 * @author Y13
 *
 */
public interface PayConfigHandler2 {
    
    /**
     * 
     * @param activator
     */
    void setActivator(ApiActivator activator);

}
