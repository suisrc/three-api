    /**
    * 全量覆盖成员
    * 接口说明：
    * 本接口以userid为主键，全量覆盖企业的通讯录成员，任务完成后企业的通讯录成员与提交的文件完全保持一致。请先下载CSV文件(下载全量覆盖成员模版)，根据需求填写文件内容。
    * 注意事项：
    * 模板中的部门需填写部门ID，多个部门用分号分隔，部门ID必须为数字，根部门的部门id默认为1
    * 文件中存在、通讯录中也存在的成员，完全以文件为准
    * 文件中存在、通讯录中不存在的成员，执行添加操作
    * 通讯录中存在、文件中不存在的成员，执行删除操作。出于安全考虑，下面两种情形系统将中止导入并返回相应的错误码。
    * 需要删除的成员多于50人，且多于现有人数的20%以上
    * 需要删除的成员少于50人，且多于现有人数的80%以上
    * 成员字段更新规则：可自行添加扩展字段。文件中有指定的字段，以指定的字段值为准；文件中没指定的字段，不更新
    * -:replaceUser
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/replaceuser?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "media_id":"xxxxxx",
    *     "to_invite": true,
    *     "callback":
    *     {
    *          "url": "xxx",
    *          "token": "xxx",
    *          "encodingaeskey": "xxx"
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * media_id             是.        上传的csv文件的media_id
    * to_invite            否.        是否邀请新建的成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
    * callback             否.        回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
    * url                  否.        企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
    * token                否.        用于生成签名
    * encodingaeskey       否.        用于消息体的加密，是AES密钥的Base64编码
    * 说明：
    * 须拥有通讯录的写权限。
    * 
    * 返回结果：
    * {
    *     "errcode": 0,
    *     "errmsg": "ok",
    *     "jobid": "xxxxx"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * jobid                异步任务id，最大长度为64字节
    * 
    */
    @POST
    @Path("replaceuser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ReplaceUserResult replaceUser(@QueryParam("access_token") String accessToken, ReplaceUserBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("ReplaceUserBody")
public class ReplaceUserBody {
    @ApiModelProperty("上传的csv文件的media_id")
    @NotNull("media_id属性为空")
    @JsonProperty("media_id")
    private String mediaId;
    @ApiModelProperty("是否邀请新建的成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。")
    @JsonProperty("to_invite")
    private Boolean toInvite;
    @ApiModelProperty("回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项")
    @JsonProperty("callback")
    private Callback callback;
    /**
     * 上传的csv文件的media_id
     */
    public String getMediaId() {
        return mediaId;
    }
    /**
     * 上传的csv文件的media_id
     */
    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
    /**
     * 是否邀请新建的成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
     */
    public Boolean getToInvite() {
        return toInvite;
    }
    /**
     * 是否邀请新建的成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
     */
    public void setToInvite(Boolean toInvite) {
        this.toInvite = toInvite;
    }
    /**
     * 回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
     */
    public Callback getCallback() {
        return callback;
    }
    /**
     * 回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
     */
    public void setCallback(Callback callback) {
        this.callback = callback;
    }
}
@ApiModel("ReplaceUserBody.Callback")
public static class Callback {
    @ApiModelProperty("企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议")
    @JsonProperty("url")
    private String url;
    @ApiModelProperty("用于生成签名")
    @JsonProperty("token")
    private String token;
    @ApiModelProperty("用于消息体的加密，是AES密钥的Base64编码")
    @JsonProperty("encodingaeskey")
    private String encodingaeskey;
    /**
     * 企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
     */
    public String getUrl() {
        return url;
    }
    /**
     * 企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
     */
    public void setUrl(String url) {
        this.url = url;
    }
    /**
     * 用于生成签名
     */
    public String getToken() {
        return token;
    }
    /**
     * 用于生成签名
     */
    public void setToken(String token) {
        this.token = token;
    }
    /**
     * 用于消息体的加密，是AES密钥的Base64编码
     */
    public String getEncodingaeskey() {
        return encodingaeskey;
    }
    /**
     * 用于消息体的加密，是AES密钥的Base64编码
     */
    public void setEncodingaeskey(String encodingaeskey) {
        this.encodingaeskey = encodingaeskey;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("ReplaceUserResult")
public class ReplaceUserResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("异步任务id，最大长度为64字节")
    @JsonProperty("jobid")
    private String jobid;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 异步任务id，最大长度为64字节
     */
    public String getJobid() {
        return jobid;
    }
    /**
     * 异步任务id，最大长度为64字节
     */
    public void setJobid(String jobid) {
        this.jobid = jobid;
    }
}





-----------------------------over over over over over-------------------------