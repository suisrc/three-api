    /**
    * 邀请成员
    * 企业可通过接口批量邀请成员使用企业微信，邀请后将通过短信或邮件下发通知。
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/batch/invite?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "user" : ["UserID1", "UserID2", "UserID3"],
    *    "party" : ["PartyID1", "PartyID2"],
    *    "tag" : ["TagID1", "TagID2"]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * user                 否.        成员ID列表,
    * party                否.        部门ID列表，最多支持100个。
    * tag                  否.        标签ID列表，最多支持100个。
    * 说明：
    * 须拥有指定成员、部门或标签的查看权限。
    * user, party, tag三者不能同时为空；
    * 如果部分接收人无权限或不存在，邀请仍然执行，但会返回无效的部分（即invaliduser或invalidparty或invalidtag）;
    * 非认证企业每天邀请人数不能超过1000, 认证企业每天邀请人数不能超过5000；
    * 同一用户只须邀请一次，被邀请的用户如果未安装企业微信，在3天内每天会收到一次通知，最多持续3天。
    * 因为邀请频率是异步检查的，所以调用接口返回成功，并不代表接收者一定能收到邀请消息（可能受上述频率限制无法接收）。
    * 
    * 返回结果：
    * {
    *    "errcode" : 0,
    *    "errmsg" : "ok",
    *    "invaliduser" : ["UserID1", "UserID2"],
    *    "invalidparty" : ["PartyID1", "PartyID2"],
    *    "invalidtag": ["TagID1", "TagID2"]
    *  }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * invaliduser          非法成员列表
    * invalidparty         非法部门列表
    * invalidtag           非法标签列表
    * 
    */
    @POST
    @Path("invite")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    InviteResult invite(@QueryParam("access_token")@NotNull("内容为空") String accessToken, InviteBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("InviteBody")
public class InviteBody {
    @ApiModelProperty("成员ID列表,")
    @JsonProperty("user")
    private String[] user;
    @ApiModelProperty("部门ID列表，最多支持100个。")
    @JsonProperty("party")
    private String[] party;
    @ApiModelProperty("标签ID列表，最多支持100个。")
    @JsonProperty("tag")
    private String[] tag;
    /**
     * 成员ID列表,
     */
    public String[] getUser() {
        return user;
    }
    /**
     * 成员ID列表,
     */
    public void setUser(String[] user) {
        this.user = user;
    }
    /**
     * 部门ID列表，最多支持100个。
     */
    public String[] getParty() {
        return party;
    }
    /**
     * 部门ID列表，最多支持100个。
     */
    public void setParty(String[] party) {
        this.party = party;
    }
    /**
     * 标签ID列表，最多支持100个。
     */
    public String[] getTag() {
        return tag;
    }
    /**
     * 标签ID列表，最多支持100个。
     */
    public void setTag(String[] tag) {
        this.tag = tag;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("InviteResult")
public class InviteResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("非法成员列表")
    @JsonProperty("invaliduser")
    private String[] invaliduser;
    @ApiModelProperty("非法部门列表")
    @JsonProperty("invalidparty")
    private String[] invalidparty;
    @ApiModelProperty("非法标签列表")
    @JsonProperty("invalidtag")
    private String[] invalidtag;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 非法成员列表
     */
    public String[] getInvaliduser() {
        return invaliduser;
    }
    /**
     * 非法成员列表
     */
    public void setInvaliduser(String[] invaliduser) {
        this.invaliduser = invaliduser;
    }
    /**
     * 非法部门列表
     */
    public String[] getInvalidparty() {
        return invalidparty;
    }
    /**
     * 非法部门列表
     */
    public void setInvalidparty(String[] invalidparty) {
        this.invalidparty = invalidparty;
    }
    /**
     * 非法标签列表
     */
    public String[] getInvalidtag() {
        return invalidtag;
    }
    /**
     * 非法标签列表
     */
    public void setInvalidtag(String[] invalidtag) {
        this.invalidtag = invalidtag;
    }
}





-----------------------------over over over over over-------------------------