    /**
    * 全量覆盖部门
    * 接口说明：
    * 本接口以partyid为键，全量覆盖企业的通讯录组织架构，任务完成后企业的通讯录组织架构与提交的文件完全保持一致。请先下载CSV文件(下载全量覆盖部门模版)，根据需求填写文件内容。
    * 注意事项：
    * 文件中存在、通讯录中也存在的部门，执行修改操作
    * 文件中存在、通讯录中不存在的部门，执行添加操作
    * 文件中不存在、通讯录中存在的部门，当部门下没有任何成员或子部门时，执行删除操作
    * 文件中不存在、通讯录中存在的部门，当部门下仍有成员或子部门时，暂时不会删除，当下次导入成员把人从部门移出后自动删除
    * CSV文件中，部门名称、部门ID、父部门ID为必填字段，部门ID必须为数字，根部门的部门id默认为1；排序为可选字段，置空或填0不修改排序, order值大的排序靠前。
    * -:replaceParty
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/replaceparty?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "media_id":"xxxxxx",
    *     "callback":
    *     {
    *          "url": "xxx",
    *          "token": "xxx",
    *          "encodingaeskey": "xxx"
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * media_id             是.        上传的csv文件的media_id
    * callback             否.        回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
    * url                  否.        企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
    * token                否.        用于生成签名
    * encodingaeskey       否.        用于消息体的加密，是AES密钥的Base64编码
    * 说明：
    * 须拥有通讯录的写权限。
    * 
    * 返回结果：
    * {
    *     "errcode": 0,
    *     "errmsg": "ok",
    *     "jobid": "xxxxx"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * jobid                异步任务id，最大长度为64字节
    * 
    */
    @POST
    @Path("replaceparty")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ReplacePartyResult replaceParty(@QueryParam("access_token") String accessToken, ReplacePartyBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("ReplacePartyBody")
public class ReplacePartyBody {
    @ApiModelProperty("上传的csv文件的media_id")
    @NotNull("media_id属性为空")
    @JsonProperty("media_id")
    private String mediaId;
    @ApiModelProperty("回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项")
    @JsonProperty("callback")
    private Callback callback;
    /**
     * 上传的csv文件的media_id
     */
    public String getMediaId() {
        return mediaId;
    }
    /**
     * 上传的csv文件的media_id
     */
    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
    /**
     * 回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
     */
    public Callback getCallback() {
        return callback;
    }
    /**
     * 回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
     */
    public void setCallback(Callback callback) {
        this.callback = callback;
    }
}
@ApiModel("ReplacePartyBody.Callback")
public static class Callback {
    @ApiModelProperty("企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议")
    @JsonProperty("url")
    private String url;
    @ApiModelProperty("用于生成签名")
    @JsonProperty("token")
    private String token;
    @ApiModelProperty("用于消息体的加密，是AES密钥的Base64编码")
    @JsonProperty("encodingaeskey")
    private String encodingaeskey;
    /**
     * 企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
     */
    public String getUrl() {
        return url;
    }
    /**
     * 企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
     */
    public void setUrl(String url) {
        this.url = url;
    }
    /**
     * 用于生成签名
     */
    public String getToken() {
        return token;
    }
    /**
     * 用于生成签名
     */
    public void setToken(String token) {
        this.token = token;
    }
    /**
     * 用于消息体的加密，是AES密钥的Base64编码
     */
    public String getEncodingaeskey() {
        return encodingaeskey;
    }
    /**
     * 用于消息体的加密，是AES密钥的Base64编码
     */
    public void setEncodingaeskey(String encodingaeskey) {
        this.encodingaeskey = encodingaeskey;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("ReplacePartyResult")
public class ReplacePartyResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("异步任务id，最大长度为64字节")
    @JsonProperty("jobid")
    private String jobid;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 异步任务id，最大长度为64字节
     */
    public String getJobid() {
        return jobid;
    }
    /**
     * 异步任务id，最大长度为64字节
     */
    public void setJobid(String jobid) {
        this.jobid = jobid;
    }
}





-----------------------------over over over over over-------------------------