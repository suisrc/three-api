    /**
    * 获取异步任务结果
    * -:getResult
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/getresult?access_token=ACCESS_TOKEN&jobid=JOBID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * jobid                是.        异步任务id，最大长度为64字节
    * 说明：
    * 只能查询已经提交过的历史任务。
    * result结构：type为sync_user、replace_user时：
    * "result": [
    *     {
    *          "userid":"lisi",
    *          "errcode":0,
    *          "errmsg":"ok"
    *     },
    *     {
    *          "userid":"zhangsan",
    *          "errcode":0,
    *          "errmsg":"ok"
    *     }
    * ]
    * 参数	说明
    * userid：成员UserID。对应管理端的帐号
    * ------------------------------------------------------
    * result结构：type为replace_party时：
    * "result": [
    *     {
    *          "action":1,
    *          "partyid":1,
    *          "errcode":0,
    *          "errmsg":"ok"
    *     },
    *     {
    *          "action":4,
    *          "partyid":2,
    *          "errcode":0,
    *          "errmsg":"ok"
    *     }
    * ]
    * action：操作类型（按位或）：1 新建部门 ，2 更改部门名称， 4 移动部门， 8 修改部门排序
    * partyid：部门ID
    * 
    * 返回结果：
    * {
    *     "errcode": 0,
    *     "errmsg": "ok",
    *     "status": 1,
    *     "type": "replace_user",
    *     "total": 3,
    *     "percentage": 33,
    *     "result": [{},{}]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容，例如无权限错误，键值冲突，格式错误等
    * status               任务状态，整型，1表示任务开始，2表示任务进行中，3表示任务已完成
    * type                 操作类型，字节串，目前分别有：1.
    * total                任务运行总条数
    * percentage           目前运行百分比，当任务完成时为100
    * result               详细的处理结果，具体格式参考下面说明。当任务完成后此字段有效
    * 
    */
    @GET
    @Path("getresult")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetResultResult getResult(@QueryParam("access_token")@NotNull("内容为空") String accessToken, @QueryParam("jobid")@NotNull("内容为空") String jobid);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetResultResult")
public class GetResultResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容，例如无权限错误，键值冲突，格式错误等")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("任务状态，整型，1表示任务开始，2表示任务进行中，3表示任务已完成")
    @JsonProperty("status")
    private Integer status;
    @ApiModelProperty("操作类型，字节串，目前分别有：1.")
    @JsonProperty("type")
    private String type;
    @ApiModelProperty("任务运行总条数")
    @JsonProperty("total")
    private Integer total;
    @ApiModelProperty("目前运行百分比，当任务完成时为100")
    @JsonProperty("percentage")
    private Integer percentage;
    @ApiModelProperty("详细的处理结果，具体格式参考下面说明。当任务完成后此字段有效")
    @JsonProperty("result")
    private Result[] result;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容，例如无权限错误，键值冲突，格式错误等
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容，例如无权限错误，键值冲突，格式错误等
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 任务状态，整型，1表示任务开始，2表示任务进行中，3表示任务已完成
     */
    public Integer getStatus() {
        return status;
    }
    /**
     * 任务状态，整型，1表示任务开始，2表示任务进行中，3表示任务已完成
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
    /**
     * 操作类型，字节串，目前分别有：1.
     */
    public String getType() {
        return type;
    }
    /**
     * 操作类型，字节串，目前分别有：1.
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * 任务运行总条数
     */
    public Integer getTotal() {
        return total;
    }
    /**
     * 任务运行总条数
     */
    public void setTotal(Integer total) {
        this.total = total;
    }
    /**
     * 目前运行百分比，当任务完成时为100
     */
    public Integer getPercentage() {
        return percentage;
    }
    /**
     * 目前运行百分比，当任务完成时为100
     */
    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
    /**
     * 详细的处理结果，具体格式参考下面说明。当任务完成后此字段有效
     */
    public Result[] getResult() {
        return result;
    }
    /**
     * 详细的处理结果，具体格式参考下面说明。当任务完成后此字段有效
     */
    public void setResult(Result[] result) {
        this.result = result;
    }
}
@ApiModel("GetResultResult.Result")
public static class Result {

}





-----------------------------over over over over over-------------------------