增量更新成员
接口说明 ：
本接口以userid（帐号）为主键，增量更新企业微信通讯录成员。请先下载CSV模板(下载增量更新成员模版)，根据需求填写文件内容。
注意事项：
模板中的部门需填写部门ID，多个部门用分号分隔，部门ID必须为数字，根部门的部门id默认为1
文件中存在、通讯录中也存在的成员，更新成员在文件中指定的字段值
文件中存在、通讯录中不存在的成员，执行添加操作
通讯录中存在、文件中不存在的成员，保持不变
成员字段更新规则：可自行添加扩展字段。文件中有指定的字段，以指定的字段值为准；文件中没指定的字段，不更新
-:syncUser

请求方式：POST（HTTPS）

请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/syncuser?access_token=ACCESS_TOKEN

请求包体：
{
    "media_id":"xxxxxx",
    "to_invite": true,
    "callback":
    {
         "url": "xxx",
         "token": "xxx",
         "encodingaeskey": "xxx"
    }
}
参数说明：
参数	必须	说明
media_id	是	上传的csv文件的media_id
to_invite	否	是否邀请新建的成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
callback	否	回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
url	否	企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
token	否	用于生成签名
encodingaeskey	否	用于消息体的加密，是AES密钥的Base64编码

说明：
须拥有通讯录的写权限。

返回结果：
{
    "errcode": 0,
    "errmsg": "ok",
    "jobid": "xxxxx"
}
参数说明：
参数	说明
errcode	返回码
errmsg	对返回码的文本描述内容
jobid	异步任务id，最大长度为64字节