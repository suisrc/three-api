    /**
    * 二次验证
    * 此接口可以满足安全性要求高的企业进行成员加入验证。开启二次验证后，用户加入企业时需要跳转企业自定义的页面进行验证。   
    * 企业在开启二次验证时，必须在管理端填写企业二次验证页面的url。
    * 当成员登录企业微信或关注微工作台（原企业号）加入企业时，会自动跳转到企业的验证页面。在跳转到企业的验证页面时，会带上如下参数：code=CODE。
    * 企业收到code后，使用“通讯录同步助手”调用“根据code获取成员信息”接口获取成员的userid。然后在验证成员信息成功后，调用如下接口即可让成员成功加入企业。
    * auth2User
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/authsucc?access_token=ACCESS_TOKEN&userid=USERID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * userid               是.        成员UserID。对应管理端的帐号
    * 说明：
    * 
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @GET
    @Path("authsucc")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode authsucc(@QueryParam("access_token")@NotNull("内容为空") String accessToken, @QueryParam("userid")@NotNull("内容为空") String userid);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------