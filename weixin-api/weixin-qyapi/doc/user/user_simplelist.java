    /**
    * 获取部门成员
    * -:simplelistUser
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 
    * 说明：
    * 应用须拥有指定部门的查看权限。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "userlist": [
    *            {
    *                   "userid": "zhangsan",
    *                   "name": "李四",
    *                   "department": [1, 2]
    *            }
    *      ]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * userlist             成员列表
    * userid               成员UserID。对应管理端的帐号
    * name                 成员名称
    * department           成员所属部门
    * 
    */
    @GET
    @Path("simplelist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SimplelistUserResult simplelistUser(@QueryParam("access_token") String accessToken, @QueryParam("department_id") String departmentId, @QueryParam("fetch_child") String fetchChild);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("SimplelistUserResult")
public class SimplelistUserResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("成员列表")
    @JsonProperty("userlist")
    private Userlist[] userlist;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 成员列表
     */
    public Userlist[] getUserlist() {
        return userlist;
    }
    /**
     * 成员列表
     */
    public void setUserlist(Userlist[] userlist) {
        this.userlist = userlist;
    }
}
@ApiModel("SimplelistUserResult.Userlist")
public static class Userlist {
    @ApiModelProperty("成员UserID。对应管理端的帐号")
    @JsonProperty("userid")
    private String userid;
    @ApiModelProperty("成员名称")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty("成员所属部门")
    @JsonProperty("department")
    private Integer[] department;
    /**
     * 成员UserID。对应管理端的帐号
     */
    public String getUserid() {
        return userid;
    }
    /**
     * 成员UserID。对应管理端的帐号
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }
    /**
     * 成员名称
     */
    public String getName() {
        return name;
    }
    /**
     * 成员名称
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 成员所属部门
     */
    public Integer[] getDepartment() {
        return department;
    }
    /**
     * 成员所属部门
     */
    public void setDepartment(Integer[] department) {
        this.department = department;
    }
}





-----------------------------over over over over over-------------------------