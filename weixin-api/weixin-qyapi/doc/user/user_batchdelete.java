    /**
    * 批量删除成员
    * -:batchdeleteUser
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "useridlist": ["zhangsan", "lisi"]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * useridlist           是.        成员UserID列表。对应管理端的帐号。最多支持200个。若存在无效UserID，直接返回错误
    * 说明：
    * 应用须拥有指定成员的管理权限。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @POST
    @Path("batchdelete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode batchdeleteUser(@QueryParam("access_token")@NotNull("内容为空") String accessToken, BatchdeleteUserBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("BatchdeleteUserBody")
public class BatchdeleteUserBody {
    @ApiModelProperty("成员UserID列表。对应管理端的帐号。最多支持200个。若存在无效UserID，直接返回错误")
    @NotNull("useridlist属性为空")
    @JsonProperty("useridlist")
    private String[] useridlist;
    /**
     * 成员UserID列表。对应管理端的帐号。最多支持200个。若存在无效UserID，直接返回错误
     */
    public String[] getUseridlist() {
        return useridlist;
    }
    /**
     * 成员UserID列表。对应管理端的帐号。最多支持200个。若存在无效UserID，直接返回错误
     */
    public void setUseridlist(String[] useridlist) {
        this.useridlist = useridlist;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------