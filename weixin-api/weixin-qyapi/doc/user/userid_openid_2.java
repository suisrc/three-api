    /**
    * openid转userid
    * 该接口主要应用于使用企业支付之后的结果查询。
    * 开发者需要知道某个结果事件的openid对应企业微信内成员的信息时，可以通过调用该接口进行转换查询。
    * -:convert2Userid
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_userid?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "openid": "oDOGms-6yCnGrRovBj2yHij5JL6E"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * openid               是.        在使用企业支付之后，返回结果的openid
    * 说明：
    * 管理组需对openid对应的企业微信成员有查看权限。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "userid": "zhangsan"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * userid               该openid在企业微信对应的成员userid
    * 
    */
    @POST
    @Path("convert_to_userid")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Convert2UseridResult convert2Userid(@QueryParam("access_token")@NotNull("内容为空") String accessToken, Convert2UseridBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("Convert2UseridBody")
public class Convert2UseridBody {
    @ApiModelProperty("在使用企业支付之后，返回结果的openid")
    @NotNull("openid属性为空")
    @JsonProperty("openid")
    private String openid;
    /**
     * 在使用企业支付之后，返回结果的openid
     */
    public String getOpenid() {
        return openid;
    }
    /**
     * 在使用企业支付之后，返回结果的openid
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("Convert2UseridResult")
public class Convert2UseridResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("该openid在企业微信对应的成员userid")
    @JsonProperty("userid")
    private String userid;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 该openid在企业微信对应的成员userid
     */
    public String getUserid() {
        return userid;
    }
    /**
     * 该openid在企业微信对应的成员userid
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }
}





-----------------------------over over over over over-------------------------