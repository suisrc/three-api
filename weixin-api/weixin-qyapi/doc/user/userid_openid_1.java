    /**
    * userid转openid
    * 该接口使用场景为企业支付，在使用企业红包和向员工付款时，需要自行将企业微信的userid转成openid。
    * -:convert2Openid
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "userid": "zhangsan"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * userid               是.        企业内的成员id
    * 说明：
    * 成员必须处于应用的可见范围内
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "openid": "oDOGms-6yCnGrRovBj2yHij5JL6E"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * openid               企业微信成员userid对应的openid
    * 
    */
    @POST
    @Path("convert_to_openid")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Convert2OpenidResult convert2Openid(@QueryParam("access_token")@NotNull("内容为空") String accessToken, Convert2OpenidBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("Convert2OpenidBody")
public class Convert2OpenidBody {
    @ApiModelProperty("企业内的成员id")
    @NotNull("userid属性为空")
    @JsonProperty("userid")
    private String userid;
    /**
     * 企业内的成员id
     */
    public String getUserid() {
        return userid;
    }
    /**
     * 企业内的成员id
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("Convert2OpenidResult")
public class Convert2OpenidResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("企业微信成员userid对应的openid")
    @JsonProperty("openid")
    private String openid;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 企业微信成员userid对应的openid
     */
    public String getOpenid() {
        return openid;
    }
    /**
     * 企业微信成员userid对应的openid
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }
}





-----------------------------over over over over over-------------------------