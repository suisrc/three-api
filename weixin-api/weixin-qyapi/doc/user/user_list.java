    /**
    * 获取部门成员详情
    * -:getUserList
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * department_id        是.        获取的部门id
    * fetch_child          否.        1/0：是否递归获取子部门下面的成员
    * 说明：
    * 应用须拥有指定部门的查看权限。
    * 
    * 返回结果：
    * {
    *     "errcode": 0,
    *     "errmsg": "ok",
    *     "userlist": [{
    *         "userid": "zhangsan",
    *         "name": "李四",
    *         "department": [1, 2],
    *         "order": [1, 2],
    *         "position": "后台工程师",
    *         "mobile": "15913215421",
    *         "gender": "1",
    *         "email": "zhangsan@gzdev.com",
    *         "isleader": 0,
    *         "avatar": "http://wx.qlogo.cn/mmopen/ajNVdqHZLLA3WJ6DSZUfiakYe37PKnQhBIeOQBO4czqrnZDS79FH5Wm5m4X69TBicnHFlhiafvDwklOpZeXYQQ2icg/0",
    *         "telephone": "020-123456",
    *         "enable": 1,
    *         "english_name": "jackzhang",
    *         "status": 1,
    *         "extattr": {
    *             "attrs": [{
    *                 "name": "爱好",
    *                 "value": "旅游"
    *             }, {
    *                 "name": "卡号",
    *                 "value": "1234567234"
    *             }]
    *         },
    *         "qr_code": "https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=xxx",
    *         "external_profile": {
    *             "external_attr": [{
    *                     "type": 0,
    *                     "name": "文本名称",
    *                     "text": {
    *                         "value": "文本"
    *                     }
    *                 },
    *                 {
    *                     "type": 1,
    *                     "name": "网页名称",
    *                     "web": {
    *                         "url": "http://www.test.com",
    *                         "title": "标题"
    *                     }
    *                 },
    *                 {
    *                     "type": 2,
    *                     "name": "测试app",
    *                     "miniprogram": {
    *                         "appid": "wx8bd80126147df384",
    *                         "pagepath": "/index",
    *                         "title": "my miniprogram"
    *                     }
    *                 }
    *             ]
    *         }
    *     }]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * userlist             成员列表
    * userid               成员UserID。对应管理端的帐号
    * name                 成员名称
    * mobile               手机号码，第三方仅通讯录套件可获取
    * department           成员所属部门id列表
    * order                部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0,
    * position             职位信息；第三方仅通讯录应用可获取
    * gender               性别。0表示未定义，1表示男性，2表示女性
    * email                邮箱，第三方仅通讯录应用可获取
    * isleader             标示是否为上级；第三方仅通讯录应用可获取
    * avatar               头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。第三方仅通讯录应用可获取
    * telephone            座机。第三方仅通讯录应用可获取
    * enable               成员启用状态。1表示启用的成员，0表示被禁用。服务商调用接口不会返回此字段
    * english_name         英文名；第三方仅通讯录应用可获取
    * status               激活状态:
    * extattr              扩展属性，第三方仅通讯录套件可获取
    * qr_code              员工个人二维码，扫描可添加为外部联系人；第三方仅通讯录应用可获取
    * external_profile     成员对外属性，字段详情见对外属性；第三方仅通讯录应用可获取
    * 
    */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetUserListResult getUserList(@QueryParam("access_token")@NotNull("内容为空") String accessToken, @QueryParam("department_id")@NotNull("内容为空") String departmentId, @QueryParam("fetch_child")@NotNull("内容为空") String fetchChild);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetUserListResult")
public class GetUserListResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("成员列表")
    @JsonProperty("userlist")
    private Userlist[] userlist;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 成员列表
     */
    public Userlist[] getUserlist() {
        return userlist;
    }
    /**
     * 成员列表
     */
    public void setUserlist(Userlist[] userlist) {
        this.userlist = userlist;
    }
}
@ApiModel("GetUserListResult.Userlist")
public static class Userlist {
    @ApiModelProperty("成员UserID。对应管理端的帐号")
    @JsonProperty("userid")
    private String userid;
    @ApiModelProperty("成员名称")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty("成员所属部门id列表")
    @JsonProperty("department")
    private Integer[] department;
    @ApiModelProperty("部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0,")
    @JsonProperty("order")
    private Integer[] order;
    @ApiModelProperty("职位信息；第三方仅通讯录应用可获取")
    @JsonProperty("position")
    private String position;
    @ApiModelProperty("手机号码，第三方仅通讯录套件可获取")
    @JsonProperty("mobile")
    private String mobile;
    @ApiModelProperty("性别。0表示未定义，1表示男性，2表示女性")
    @JsonProperty("gender")
    private String gender;
    @ApiModelProperty("邮箱，第三方仅通讯录应用可获取")
    @JsonProperty("email")
    private String email;
    @ApiModelProperty("标示是否为上级；第三方仅通讯录应用可获取")
    @JsonProperty("isleader")
    private Integer isleader;
    @ApiModelProperty("头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。第三方仅通讯录应用可获取")
    @JsonProperty("avatar")
    private String avatar;
    @ApiModelProperty("座机。第三方仅通讯录应用可获取")
    @JsonProperty("telephone")
    private String telephone;
    @ApiModelProperty("成员启用状态。1表示启用的成员，0表示被禁用。服务商调用接口不会返回此字段")
    @JsonProperty("enable")
    private Integer enable;
    @ApiModelProperty("英文名；第三方仅通讯录应用可获取")
    @JsonProperty("english_name")
    private String englishName;
    @ApiModelProperty("激活状态:")
    @JsonProperty("status")
    private Integer status;
    @ApiModelProperty("扩展属性，第三方仅通讯录套件可获取")
    @JsonProperty("extattr")
    private Extattr extattr;
    @ApiModelProperty("员工个人二维码，扫描可添加为外部联系人；第三方仅通讯录应用可获取")
    @JsonProperty("qr_code")
    private String qrCode;
    @ApiModelProperty("成员对外属性，字段详情见对外属性；第三方仅通讯录应用可获取")
    @JsonProperty("external_profile")
    private ExternalProfile externalProfile;
    /**
     * 成员UserID。对应管理端的帐号
     */
    public String getUserid() {
        return userid;
    }
    /**
     * 成员UserID。对应管理端的帐号
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }
    /**
     * 成员名称
     */
    public String getName() {
        return name;
    }
    /**
     * 成员名称
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 成员所属部门id列表
     */
    public Integer[] getDepartment() {
        return department;
    }
    /**
     * 成员所属部门id列表
     */
    public void setDepartment(Integer[] department) {
        this.department = department;
    }
    /**
     * 部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0,
     */
    public Integer[] getOrder() {
        return order;
    }
    /**
     * 部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0,
     */
    public void setOrder(Integer[] order) {
        this.order = order;
    }
    /**
     * 职位信息；第三方仅通讯录应用可获取
     */
    public String getPosition() {
        return position;
    }
    /**
     * 职位信息；第三方仅通讯录应用可获取
     */
    public void setPosition(String position) {
        this.position = position;
    }
    /**
     * 手机号码，第三方仅通讯录套件可获取
     */
    public String getMobile() {
        return mobile;
    }
    /**
     * 手机号码，第三方仅通讯录套件可获取
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    /**
     * 性别。0表示未定义，1表示男性，2表示女性
     */
    public String getGender() {
        return gender;
    }
    /**
     * 性别。0表示未定义，1表示男性，2表示女性
     */
    public void setGender(String gender) {
        this.gender = gender;
    }
    /**
     * 邮箱，第三方仅通讯录应用可获取
     */
    public String getEmail() {
        return email;
    }
    /**
     * 邮箱，第三方仅通讯录应用可获取
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * 标示是否为上级；第三方仅通讯录应用可获取
     */
    public Integer getIsleader() {
        return isleader;
    }
    /**
     * 标示是否为上级；第三方仅通讯录应用可获取
     */
    public void setIsleader(Integer isleader) {
        this.isleader = isleader;
    }
    /**
     * 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。第三方仅通讯录应用可获取
     */
    public String getAvatar() {
        return avatar;
    }
    /**
     * 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。第三方仅通讯录应用可获取
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    /**
     * 座机。第三方仅通讯录应用可获取
     */
    public String getTelephone() {
        return telephone;
    }
    /**
     * 座机。第三方仅通讯录应用可获取
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    /**
     * 成员启用状态。1表示启用的成员，0表示被禁用。服务商调用接口不会返回此字段
     */
    public Integer getEnable() {
        return enable;
    }
    /**
     * 成员启用状态。1表示启用的成员，0表示被禁用。服务商调用接口不会返回此字段
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    /**
     * 英文名；第三方仅通讯录应用可获取
     */
    public String getEnglishName() {
        return englishName;
    }
    /**
     * 英文名；第三方仅通讯录应用可获取
     */
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }
    /**
     * 激活状态:
     */
    public Integer getStatus() {
        return status;
    }
    /**
     * 激活状态:
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
    /**
     * 扩展属性，第三方仅通讯录套件可获取
     */
    public Extattr getExtattr() {
        return extattr;
    }
    /**
     * 扩展属性，第三方仅通讯录套件可获取
     */
    public void setExtattr(Extattr extattr) {
        this.extattr = extattr;
    }
    /**
     * 员工个人二维码，扫描可添加为外部联系人；第三方仅通讯录应用可获取
     */
    public String getQrCode() {
        return qrCode;
    }
    /**
     * 员工个人二维码，扫描可添加为外部联系人；第三方仅通讯录应用可获取
     */
    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
    /**
     * 成员对外属性，字段详情见对外属性；第三方仅通讯录应用可获取
     */
    public ExternalProfile getExternalProfile() {
        return externalProfile;
    }
    /**
     * 成员对外属性，字段详情见对外属性；第三方仅通讯录应用可获取
     */
    public void setExternalProfile(ExternalProfile externalProfile) {
        this.externalProfile = externalProfile;
    }
}
@ApiModel("GetUserListResult.Userlist.Extattr")
public static class Extattr {
    @JsonProperty("attrs")
    private Attrs[] attrs;
    public Attrs[] getAttrs() {
        return attrs;
    }
    public void setAttrs(Attrs[] attrs) {
        this.attrs = attrs;
    }
}
@ApiModel("GetUserListResult.Userlist.Extattr.Attrs")
public static class Attrs {
    @ApiModelProperty("成员名称")
    @JsonProperty("name")
    private String name;
    @JsonProperty("value")
    private String value;
    /**
     * 成员名称
     */
    public String getName() {
        return name;
    }
    /**
     * 成员名称
     */
    public void setName(String name) {
        this.name = name;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
@ApiModel("GetUserListResult.Userlist.ExternalProfile")
public static class ExternalProfile {
    @JsonProperty("external_attr")
    private ExternalAttr[] externalAttr;
    public ExternalAttr[] getExternalAttr() {
        return externalAttr;
    }
    public void setExternalAttr(ExternalAttr[] externalAttr) {
        this.externalAttr = externalAttr;
    }
}
@ApiModel("GetUserListResult.Userlist.ExternalProfile.ExternalAttr")
public static class ExternalAttr {
    @JsonProperty("type")
    private Integer type;
    @ApiModelProperty("成员名称")
    @JsonProperty("name")
    private String name;
    @JsonProperty("text")
    private Text text;
    public Integer getType() {
        return type;
    }
    public void setType(Integer type) {
        this.type = type;
    }
    /**
     * 成员名称
     */
    public String getName() {
        return name;
    }
    /**
     * 成员名称
     */
    public void setName(String name) {
        this.name = name;
    }
    public Text getText() {
        return text;
    }
    public void setText(Text text) {
        this.text = text;
    }
}
@ApiModel("GetUserListResult.Userlist.ExternalProfile.ExternalAttr.Text")
public static class Text {
    @JsonProperty("value")
    private String value;
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}





-----------------------------over over over over over-------------------------