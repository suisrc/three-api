    /**
    * 使用user_ticket获取成员详情
    * -:getUserDetail
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/getuserdetail?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "user_ticket": "USER_TICKET"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证，第三方服务商通过企业的永久授权码获取，参见“获取企业access_token”
    * user_ticket          是.        成员票据
    * 说明：
    * 需要有对应应用的使用权限，且成员必须在授权应用的可见范围内。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "userid":"lisi",
    *    "name":"李四",
    *    "mobile":"13050495892",
    *    "gender":"1",
    *    "email":"xxx@xx.com",
    *    "avatar":"http://shp.qpic.cn/bizmp/xxxxxxxxxxx/0",
    *    "qr_code":"http://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vcfc13b01dfs78e981c"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * userid               成员UserID
    * name                 成员姓名
    * mobile               成员手机号，仅在用户同意snsapi_privateinfo授权时返回
    * gender               性别。0表示未定义，1表示男性，2表示女性
    * email                成员邮箱，仅在用户同意snsapi_privateinfo授权时返回
    * avatar               头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。仅在用户同意snsapi_privateinfo授权时返回
    * qr_code              员工个人二维码（扫描可添加为外部联系人），仅在用户同意snsapi_privateinfo授权时返回
    * 
    */
    @POST
    @Path("getuserdetail")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetUserDetailResult getUserDetail(@QueryParam("access_token")@NotNull("内容为空") String accessToken, GetUserDetailBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("GetUserDetailBody")
public class GetUserDetailBody {
    @ApiModelProperty("成员票据")
    @NotNull("user_ticket属性为空")
    @JsonProperty("user_ticket")
    private String userTicket;
    /**
     * 成员票据
     */
    public String getUserTicket() {
        return userTicket;
    }
    /**
     * 成员票据
     */
    public void setUserTicket(String userTicket) {
        this.userTicket = userTicket;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetUserDetailResult")
public class GetUserDetailResult {
    @JsonProperty("errcode")
    private Integer errcode;
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("成员UserID")
    @JsonProperty("userid")
    private String userid;
    @ApiModelProperty("成员姓名")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty("成员手机号，仅在用户同意snsapi_privateinfo授权时返回")
    @JsonProperty("mobile")
    private String mobile;
    @ApiModelProperty("性别。0表示未定义，1表示男性，2表示女性")
    @JsonProperty("gender")
    private String gender;
    @ApiModelProperty("成员邮箱，仅在用户同意snsapi_privateinfo授权时返回")
    @JsonProperty("email")
    private String email;
    @ApiModelProperty("头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。仅在用户同意snsapi_privateinfo授权时返回")
    @JsonProperty("avatar")
    private String avatar;
    @ApiModelProperty("员工个人二维码（扫描可添加为外部联系人），仅在用户同意snsapi_privateinfo授权时返回")
    @JsonProperty("qr_code")
    private String qrCode;
    public Integer getErrcode() {
        return errcode;
    }
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    public String getErrmsg() {
        return errmsg;
    }
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 成员UserID
     */
    public String getUserid() {
        return userid;
    }
    /**
     * 成员UserID
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }
    /**
     * 成员姓名
     */
    public String getName() {
        return name;
    }
    /**
     * 成员姓名
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 成员手机号，仅在用户同意snsapi_privateinfo授权时返回
     */
    public String getMobile() {
        return mobile;
    }
    /**
     * 成员手机号，仅在用户同意snsapi_privateinfo授权时返回
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    /**
     * 性别。0表示未定义，1表示男性，2表示女性
     */
    public String getGender() {
        return gender;
    }
    /**
     * 性别。0表示未定义，1表示男性，2表示女性
     */
    public void setGender(String gender) {
        this.gender = gender;
    }
    /**
     * 成员邮箱，仅在用户同意snsapi_privateinfo授权时返回
     */
    public String getEmail() {
        return email;
    }
    /**
     * 成员邮箱，仅在用户同意snsapi_privateinfo授权时返回
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。仅在用户同意snsapi_privateinfo授权时返回
     */
    public String getAvatar() {
        return avatar;
    }
    /**
     * 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。仅在用户同意snsapi_privateinfo授权时返回
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    /**
     * 员工个人二维码（扫描可添加为外部联系人），仅在用户同意snsapi_privateinfo授权时返回
     */
    public String getQrCode() {
        return qrCode;
    }
    /**
     * 员工个人二维码（扫描可添加为外部联系人），仅在用户同意snsapi_privateinfo授权时返回
     */
    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
}





-----------------------------over over over over over-------------------------