    /**
    * 更新成员
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "userid": "zhangsan",
    *     "name": "李四",
    *     "department": [1],
    *     "order": [10],
    *     "position": "后台工程师",
    *     "mobile": "15913215421",
    *     "gender": "1",
    *     "email": "zhangsan@gzdev.com",
    *     "isleader": 0,
    *     "enable": 1,
    *     "avatar_mediaid": "2-G6nrLmr5EC3MNb_-zL1dDdzkd0p7cNliYu9V5w7o8K0",
    *     "telephone": "020-123456",
    *     "english_name": "jackzhang",
    *     "extattr": {"attrs":[{"name":"爱好","value":"旅游"},{"name":"卡号","value":"1234567234"}]},
    *     "external_profile": {
    *         "external_attr": [
    *             {
    *                 "type": 0,
    *                 "name": "文本名称",
    *                 "text": {
    *                     "value": "文本"
    *                 }
    *             },
    *             {
    *                 "type": 1,
    *                 "name": "网页名称",
    *                 "web": {
    *                     "url": "http://www.test.com",
    *                     "title": "标题"
    *                 }
    *             },
    *             {
    *                 "type": 2,
    *                 "name": "测试app",
    *                 "miniprogram": {
    *                     "appid": "wx8bd80126147df384",
    *                     "pagepath": "/index",
    *                     "title": "my miniprogram"
    *                 }
    *             }
    *         ]
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * userid               是.        成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
    * name                 否.        成员名称。长度为1~64个字符
    * english_name         否.        英文名。长度为1-64个字节，由字母、数字、点(.)、减号(-)、空格或下划线(_)组成
    * mobile               否.        手机号码。企业内必须唯一。若成员已激活企业微信，则需成员自行修改（此情况下该参数被忽略，但不会报错）
    * department           否.        成员所属部门id列表，不超过20个
    * order                否.        部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。有效的值范围是[0,
    * position             否.        职位信息。长度为0~128个字符
    * gender               否.        性别。1表示男性，2表示女性
    * email                否.        邮箱。长度不超过64个字节，且为有效的email格式。企业内必须唯一
    * telephone            否.        座机。由1-32位的纯数字或’-‘号组成
    * isleader             否.        上级字段，标识是否为上级。
    * avatar_mediaid       否.        成员头像的mediaid，通过素材管理接口上传图片获得的mediaid
    * enable               否.        启用/禁用成员。1表示启用成员，0表示禁用成员
    * extattr              否.        自定义字段。自定义字段需要先在WEB管理端添加，见扩展属性添加方法，否则忽略未知属性的赋值。自定义字段长度为0~32个字符，超过将被截断
    * external_profile     否.        成员对外属性，字段详情见对外属性特别地，如果userid由系统自动生成，则仅允许修改一次。新值可由new_userid字段指定。
    * 说明：
    * 应用须拥有指定部门、成员的管理权限。
    * 注意，每个部门下的部门、成员总数不能超过3万个。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "updated"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode update(@QueryParam("access_token")@NotNull("内容为空") String accessToken, UpdateBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("UpdateBody")
public class UpdateBody {
    @ApiModelProperty("成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节")
    @NotNull("userid属性为空")
    @JsonProperty("userid")
    private String userid;
    @ApiModelProperty("成员名称。长度为1~64个字符")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty("成员所属部门id列表，不超过20个")
    @JsonProperty("department")
    private Integer[] department;
    @ApiModelProperty("部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。有效的值范围是[0,")
    @JsonProperty("order")
    private Integer[] order;
    @ApiModelProperty("职位信息。长度为0~128个字符")
    @JsonProperty("position")
    private String position;
    @ApiModelProperty("手机号码。企业内必须唯一。若成员已激活企业微信，则需成员自行修改（此情况下该参数被忽略，但不会报错）")
    @JsonProperty("mobile")
    private String mobile;
    @ApiModelProperty("性别。1表示男性，2表示女性")
    @JsonProperty("gender")
    private String gender;
    @ApiModelProperty("邮箱。长度不超过64个字节，且为有效的email格式。企业内必须唯一")
    @JsonProperty("email")
    private String email;
    @ApiModelProperty("上级字段，标识是否为上级。")
    @JsonProperty("isleader")
    private Integer isleader;
    @ApiModelProperty("启用/禁用成员。1表示启用成员，0表示禁用成员")
    @JsonProperty("enable")
    private Integer enable;
    @ApiModelProperty("成员头像的mediaid，通过素材管理接口上传图片获得的mediaid")
    @JsonProperty("avatar_mediaid")
    private String avatarMediaid;
    @ApiModelProperty("座机。由1-32位的纯数字或’-‘号组成")
    @JsonProperty("telephone")
    private String telephone;
    @ApiModelProperty("英文名。长度为1-64个字节，由字母、数字、点(.)、减号(-)、空格或下划线(_)组成")
    @JsonProperty("english_name")
    private String englishName;
    @ApiModelProperty("自定义字段。自定义字段需要先在WEB管理端添加，见扩展属性添加方法，否则忽略未知属性的赋值。自定义字段长度为0~32个字符，超过将被截断")
    @JsonProperty("extattr")
    private Extattr extattr;
    @ApiModelProperty("成员对外属性，字段详情见对外属性特别地，如果userid由系统自动生成，则仅允许修改一次。新值可由new_userid字段指定。")
    @JsonProperty("external_profile")
    private ExternalProfile externalProfile;
    /**
     * 成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
     */
    public String getUserid() {
        return userid;
    }
    /**
     * 成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }
    /**
     * 成员名称。长度为1~64个字符
     */
    public String getName() {
        return name;
    }
    /**
     * 成员名称。长度为1~64个字符
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 成员所属部门id列表，不超过20个
     */
    public Integer[] getDepartment() {
        return department;
    }
    /**
     * 成员所属部门id列表，不超过20个
     */
    public void setDepartment(Integer[] department) {
        this.department = department;
    }
    /**
     * 部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。有效的值范围是[0,
     */
    public Integer[] getOrder() {
        return order;
    }
    /**
     * 部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。有效的值范围是[0,
     */
    public void setOrder(Integer[] order) {
        this.order = order;
    }
    /**
     * 职位信息。长度为0~128个字符
     */
    public String getPosition() {
        return position;
    }
    /**
     * 职位信息。长度为0~128个字符
     */
    public void setPosition(String position) {
        this.position = position;
    }
    /**
     * 手机号码。企业内必须唯一。若成员已激活企业微信，则需成员自行修改（此情况下该参数被忽略，但不会报错）
     */
    public String getMobile() {
        return mobile;
    }
    /**
     * 手机号码。企业内必须唯一。若成员已激活企业微信，则需成员自行修改（此情况下该参数被忽略，但不会报错）
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    /**
     * 性别。1表示男性，2表示女性
     */
    public String getGender() {
        return gender;
    }
    /**
     * 性别。1表示男性，2表示女性
     */
    public void setGender(String gender) {
        this.gender = gender;
    }
    /**
     * 邮箱。长度不超过64个字节，且为有效的email格式。企业内必须唯一
     */
    public String getEmail() {
        return email;
    }
    /**
     * 邮箱。长度不超过64个字节，且为有效的email格式。企业内必须唯一
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * 上级字段，标识是否为上级。
     */
    public Integer getIsleader() {
        return isleader;
    }
    /**
     * 上级字段，标识是否为上级。
     */
    public void setIsleader(Integer isleader) {
        this.isleader = isleader;
    }
    /**
     * 启用/禁用成员。1表示启用成员，0表示禁用成员
     */
    public Integer getEnable() {
        return enable;
    }
    /**
     * 启用/禁用成员。1表示启用成员，0表示禁用成员
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
    }
    /**
     * 成员头像的mediaid，通过素材管理接口上传图片获得的mediaid
     */
    public String getAvatarMediaid() {
        return avatarMediaid;
    }
    /**
     * 成员头像的mediaid，通过素材管理接口上传图片获得的mediaid
     */
    public void setAvatarMediaid(String avatarMediaid) {
        this.avatarMediaid = avatarMediaid;
    }
    /**
     * 座机。由1-32位的纯数字或’-‘号组成
     */
    public String getTelephone() {
        return telephone;
    }
    /**
     * 座机。由1-32位的纯数字或’-‘号组成
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    /**
     * 英文名。长度为1-64个字节，由字母、数字、点(.)、减号(-)、空格或下划线(_)组成
     */
    public String getEnglishName() {
        return englishName;
    }
    /**
     * 英文名。长度为1-64个字节，由字母、数字、点(.)、减号(-)、空格或下划线(_)组成
     */
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }
    /**
     * 自定义字段。自定义字段需要先在WEB管理端添加，见扩展属性添加方法，否则忽略未知属性的赋值。自定义字段长度为0~32个字符，超过将被截断
     */
    public Extattr getExtattr() {
        return extattr;
    }
    /**
     * 自定义字段。自定义字段需要先在WEB管理端添加，见扩展属性添加方法，否则忽略未知属性的赋值。自定义字段长度为0~32个字符，超过将被截断
     */
    public void setExtattr(Extattr extattr) {
        this.extattr = extattr;
    }
    /**
     * 成员对外属性，字段详情见对外属性特别地，如果userid由系统自动生成，则仅允许修改一次。新值可由new_userid字段指定。
     */
    public ExternalProfile getExternalProfile() {
        return externalProfile;
    }
    /**
     * 成员对外属性，字段详情见对外属性特别地，如果userid由系统自动生成，则仅允许修改一次。新值可由new_userid字段指定。
     */
    public void setExternalProfile(ExternalProfile externalProfile) {
        this.externalProfile = externalProfile;
    }
}
@ApiModel("UpdateBody.Extattr")
public static class Extattr {
    @JsonProperty("attrs")
    private Attrs[] attrs;
    public Attrs[] getAttrs() {
        return attrs;
    }
    public void setAttrs(Attrs[] attrs) {
        this.attrs = attrs;
    }
}
@ApiModel("UpdateBody.Extattr.Attrs")
public static class Attrs {
    @ApiModelProperty("成员名称。长度为1~64个字符")
    @JsonProperty("name")
    private String name;
    @JsonProperty("value")
    private String value;
    /**
     * 成员名称。长度为1~64个字符
     */
    public String getName() {
        return name;
    }
    /**
     * 成员名称。长度为1~64个字符
     */
    public void setName(String name) {
        this.name = name;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
@ApiModel("UpdateBody.ExternalProfile")
public static class ExternalProfile {
    @JsonProperty("external_attr")
    private ExternalAttr[] externalAttr;
    public ExternalAttr[] getExternalAttr() {
        return externalAttr;
    }
    public void setExternalAttr(ExternalAttr[] externalAttr) {
        this.externalAttr = externalAttr;
    }
}
@ApiModel("UpdateBody.ExternalProfile.ExternalAttr")
public static class ExternalAttr {
    @JsonProperty("type")
    private Integer type;
    @ApiModelProperty("成员名称。长度为1~64个字符")
    @JsonProperty("name")
    private String name;
    @JsonProperty("text")
    private Text text;
    public Integer getType() {
        return type;
    }
    public void setType(Integer type) {
        this.type = type;
    }
    /**
     * 成员名称。长度为1~64个字符
     */
    public String getName() {
        return name;
    }
    /**
     * 成员名称。长度为1~64个字符
     */
    public void setName(String name) {
        this.name = name;
    }
    public Text getText() {
        return text;
    }
    public void setText(Text text) {
        this.text = text;
    }
}
@ApiModel("UpdateBody.ExternalProfile.ExternalAttr.Text")
public static class Text {
    @JsonProperty("value")
    private String value;
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------