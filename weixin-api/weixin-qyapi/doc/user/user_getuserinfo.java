    /**
    * 根据code获取成员信息
    * -:getUserInfo
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=ACCESS_TOKEN&code=CODE
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * code                 是.        通过成员授权获取到的code，最大为512字节。每次成员授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
    * 说明：
    * 跳转的域名须完全匹配access_token对应应用的可信域名，否则会返回50001错误。
    * 返回结果：
    * a) 当用户为企业成员时返回示例如下：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "UserId":"USERID",
    *    "DeviceId":"DEVICEID",
    *    "user_ticket": "USER_TICKET"，
    *    "expires_in":7200
    * }
    * 参数	说明
    * errcode	返回码
    * errmsg	对返回码的文本描述内容
    * UserId	成员UserID
    * DeviceId	手机设备号(由企业微信在安装时随机生成，删除重装会改变，升级不受影响)
    * user_ticket	成员票据，最大为512字节。
    * scope为snsapi_userinfo或snsapi_privateinfo，且用户在应用可见范围之内时返回此参数。
    * 后续利用该参数可以获取用户信息或敏感信息。
    * expires_in	user_ticket的有效时间（秒），随user_ticket一起返回
    * b) 非企业成员授权时返回示例如下：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "OpenId":"OPENID",
    *    "DeviceId":"DEVICEID"
    * }
    * 参数	说明
    * errcode	返回码
    * errmsg	对返回码的文本描述内容
    * OpenId	非企业成员的标识，对当前企业唯一
    * DeviceId	手机设备号(由企业微信在安装时随机生成，删除重装会改变，升级不受影响)
    * 出错返回示例：
    * {
    *    "errcode": 40029,
    *    "errmsg": "invalid code"
    * }
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @GET
    @Path("getuserinfo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode getUserInfo(@QueryParam("access_token")@NotNull("内容为空") String accessToken, @QueryParam("code")@NotNull("内容为空") String code);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------