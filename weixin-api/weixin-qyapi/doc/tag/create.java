    /**
    * 创建标签
    * -:createTag
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "tagname": "UI",
    *    "tagid": 12
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * tagname              是.        标签名称，长度限制为32个字（汉字或英文字母），标签名不可与其他标签重名。
    * tagid                否.        标签id，非负整型，指定此参数时新增的标签会生成对应的标签id，不指定时则以目前最大的id自增。
    * 说明：
    * 创建的标签属于该应用，只有该应用才可以增删成员。
    * 注意，标签总数不能超过3000个。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "created",
    *    "tagid": 12
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * tagid                标签id
    * 
    */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    CreateTagResult createTag(@QueryParam("access_token")@NotNull("内容为空") String accessToken, CreateTagBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("CreateTagBody")
public class CreateTagBody {
    @ApiModelProperty("标签名称，长度限制为32个字（汉字或英文字母），标签名不可与其他标签重名。")
    @NotNull("tagname属性为空")
    @JsonProperty("tagname")
    private String tagname;
    @ApiModelProperty("标签id，非负整型，指定此参数时新增的标签会生成对应的标签id，不指定时则以目前最大的id自增。")
    @JsonProperty("tagid")
    private Integer tagid;
    /**
     * 标签名称，长度限制为32个字（汉字或英文字母），标签名不可与其他标签重名。
     */
    public String getTagname() {
        return tagname;
    }
    /**
     * 标签名称，长度限制为32个字（汉字或英文字母），标签名不可与其他标签重名。
     */
    public void setTagname(String tagname) {
        this.tagname = tagname;
    }
    /**
     * 标签id，非负整型，指定此参数时新增的标签会生成对应的标签id，不指定时则以目前最大的id自增。
     */
    public Integer getTagid() {
        return tagid;
    }
    /**
     * 标签id，非负整型，指定此参数时新增的标签会生成对应的标签id，不指定时则以目前最大的id自增。
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("CreateTagResult")
public class CreateTagResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("标签id")
    @JsonProperty("tagid")
    private Integer tagid;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 标签id
     */
    public Integer getTagid() {
        return tagid;
    }
    /**
     * 标签id
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
}





-----------------------------over over over over over-------------------------