    /**
    * 删除标签成员
    * -:delTagUsers
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "tagid": 12,
    *    "userlist":[ "user1","user2"],
    *    "partylist":[2,4]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * tagid                是.        标签ID
    * userlist             否.        企业成员ID列表，注意：userlist、partylist不能同时为空
    * partylist            否.        企业部门ID列表，注意：userlist、partylist不能同时为空
    * 说明：
    * 调用者必须是指定标签的创建者；成员属于应用的可见范围。
    * a)正确时返回
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted"
    * }
    * b)若部分userid、partylist非法，则返回
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted",
    *    "invalidlist"："usr1|usr2|usr",
    *    "invalidparty": [2,4]
    * }
    * c)当包含的userid、partylist全部非法时返回
    * {
    *    "errcode": 40031,
    *    "errmsg": "all list invalid"
    * }
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted",
    *    "invalidlist": "usr1|usr2|usr",
    *    "invalidparty": [2,4]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * invalidlist          非法的成员帐号列表
    * invalidparty         非法的部门id列表
    * 
    */
    @POST
    @Path("deltagusers")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    DelTagUsersResult delTagUsers(@QueryParam("access_token")@NotNull("内容为空") String accessToken, DelTagUsersBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("DelTagUsersBody")
public class DelTagUsersBody {
    @ApiModelProperty("标签ID")
    @NotNull("tagid属性为空")
    @JsonProperty("tagid")
    private Integer tagid;
    @ApiModelProperty("企业成员ID列表，注意：userlist、partylist不能同时为空")
    @JsonProperty("userlist")
    private String[] userlist;
    @ApiModelProperty("企业部门ID列表，注意：userlist、partylist不能同时为空")
    @JsonProperty("partylist")
    private Integer[] partylist;
    /**
     * 标签ID
     */
    public Integer getTagid() {
        return tagid;
    }
    /**
     * 标签ID
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
    /**
     * 企业成员ID列表，注意：userlist、partylist不能同时为空
     */
    public String[] getUserlist() {
        return userlist;
    }
    /**
     * 企业成员ID列表，注意：userlist、partylist不能同时为空
     */
    public void setUserlist(String[] userlist) {
        this.userlist = userlist;
    }
    /**
     * 企业部门ID列表，注意：userlist、partylist不能同时为空
     */
    public Integer[] getPartylist() {
        return partylist;
    }
    /**
     * 企业部门ID列表，注意：userlist、partylist不能同时为空
     */
    public void setPartylist(Integer[] partylist) {
        this.partylist = partylist;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("DelTagUsersResult")
public class DelTagUsersResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("非法的成员帐号列表")
    @JsonProperty("invalidlist")
    private String invalidlist;
    @ApiModelProperty("非法的部门id列表")
    @JsonProperty("invalidparty")
    private Integer[] invalidparty;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 非法的成员帐号列表
     */
    public String getInvalidlist() {
        return invalidlist;
    }
    /**
     * 非法的成员帐号列表
     */
    public void setInvalidlist(String invalidlist) {
        this.invalidlist = invalidlist;
    }
    /**
     * 非法的部门id列表
     */
    public Integer[] getInvalidparty() {
        return invalidparty;
    }
    /**
     * 非法的部门id列表
     */
    public void setInvalidparty(Integer[] invalidparty) {
        this.invalidparty = invalidparty;
    }
}





-----------------------------over over over over over-------------------------