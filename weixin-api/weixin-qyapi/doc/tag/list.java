    /**
    * 获取标签列表
    * -:getTagList
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/list?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * 说明：
    * 自建应用或通讯同步助手可以获取所有标签列表；第三方应用仅可获取自己创建的标签。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "taglist":[
    *       {"tagid":1,"tagname":"a"},
    *       {"tagid":2,"tagname":"b"}
    *    ]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * taglist              标签列表
    * tagid                标签id
    * tagname              标签名
    * 
    */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetTagListResult getTagList(@QueryParam("access_token")@NotNull("内容为空") String accessToken);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetTagListResult")
public class GetTagListResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("标签列表")
    @JsonProperty("taglist")
    private Taglist[] taglist;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 标签列表
     */
    public Taglist[] getTaglist() {
        return taglist;
    }
    /**
     * 标签列表
     */
    public void setTaglist(Taglist[] taglist) {
        this.taglist = taglist;
    }
}
@ApiModel("GetTagListResult.Taglist")
public static class Taglist {
    @ApiModelProperty("标签id")
    @JsonProperty("tagid")
    private Integer tagid;
    @ApiModelProperty("标签名")
    @JsonProperty("tagname")
    private String tagname;
    /**
     * 标签id
     */
    public Integer getTagid() {
        return tagid;
    }
    /**
     * 标签id
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
    /**
     * 标签名
     */
    public String getTagname() {
        return tagname;
    }
    /**
     * 标签名
     */
    public void setTagname(String tagname) {
        this.tagname = tagname;
    }
}





-----------------------------over over over over over-------------------------