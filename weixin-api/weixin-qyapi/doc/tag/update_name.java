    /**
    * 更新标签名字
    * -:updateTag
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/update?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "tagid": 12,
    *    "tagname": "UI design"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * tagid                是.        标签ID
    * tagname              是.        标签名称，长度限制为32个字（汉字或英文字母），标签不可与其他标签重名。
    * 说明：
    * 调用者必须是指定标签的创建者。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "updated"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode updateTag(@QueryParam("access_token")@NotNull("内容为空") String accessToken, UpdateTagBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("UpdateTagBody")
public class UpdateTagBody {
    @ApiModelProperty("标签ID")
    @NotNull("tagid属性为空")
    @JsonProperty("tagid")
    private Integer tagid;
    @ApiModelProperty("标签名称，长度限制为32个字（汉字或英文字母），标签不可与其他标签重名。")
    @NotNull("tagname属性为空")
    @JsonProperty("tagname")
    private String tagname;
    /**
     * 标签ID
     */
    public Integer getTagid() {
        return tagid;
    }
    /**
     * 标签ID
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
    /**
     * 标签名称，长度限制为32个字（汉字或英文字母），标签不可与其他标签重名。
     */
    public String getTagname() {
        return tagname;
    }
    /**
     * 标签名称，长度限制为32个字（汉字或英文字母），标签不可与其他标签重名。
     */
    public void setTagname(String tagname) {
        this.tagname = tagname;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------