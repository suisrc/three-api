    /**
    * 获取标签成员
    * -:getTag
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token=ACCESS_TOKEN&tagid=TAGID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * tagid                是.        标签ID
    * 说明：
    * 无限制，但返回列表仅包含应用可见范围的成员；第三方可获取自己创建的标签及应用可见范围内的标签详情
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "tagname": "乒乓球协会",
    *    "userlist": [
    *          {
    *              "userid": "zhangsan",
    *              "name": "李四"
    *          }
    *      ],
    *    "partylist": [2]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * tagname              标签名
    * userlist             标签中包含的成员列表
    * userid               成员帐号
    * name                 成员名
    * partylist            标签中包含的部门id列表
    * 
    */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetTagResult getTag(@QueryParam("access_token")@NotNull("内容为空") String accessToken, @QueryParam("tagid")@NotNull("内容为空") String tagid);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetTagResult")
public class GetTagResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("标签名")
    @JsonProperty("tagname")
    private String tagname;
    @ApiModelProperty("标签中包含的成员列表")
    @JsonProperty("userlist")
    private Userlist[] userlist;
    @ApiModelProperty("标签中包含的部门id列表")
    @JsonProperty("partylist")
    private Integer[] partylist;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 标签名
     */
    public String getTagname() {
        return tagname;
    }
    /**
     * 标签名
     */
    public void setTagname(String tagname) {
        this.tagname = tagname;
    }
    /**
     * 标签中包含的成员列表
     */
    public Userlist[] getUserlist() {
        return userlist;
    }
    /**
     * 标签中包含的成员列表
     */
    public void setUserlist(Userlist[] userlist) {
        this.userlist = userlist;
    }
    /**
     * 标签中包含的部门id列表
     */
    public Integer[] getPartylist() {
        return partylist;
    }
    /**
     * 标签中包含的部门id列表
     */
    public void setPartylist(Integer[] partylist) {
        this.partylist = partylist;
    }
}
@ApiModel("GetTagResult.Userlist")
public static class Userlist {
    @ApiModelProperty("成员帐号")
    @JsonProperty("userid")
    private String userid;
    @ApiModelProperty("成员名")
    @JsonProperty("name")
    private String name;
    /**
     * 成员帐号
     */
    public String getUserid() {
        return userid;
    }
    /**
     * 成员帐号
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }
    /**
     * 成员名
     */
    public String getName() {
        return name;
    }
    /**
     * 成员名
     */
    public void setName(String name) {
        this.name = name;
    }
}





-----------------------------over over over over over-------------------------