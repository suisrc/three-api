    /**
    * 获取外部联系人详情
    * 企业可通过此接口，根据外部联系人的userid（如何获取?），拉取外部联系人详情。
    * -:getExternalContact
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/crm/get_external_contact?access_token=ACCESS_TOKEN&external_userid=EXTERNAL_USERID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * external_userid      是.        外部联系人的userid，注意不是企业成员的帐号
    * 说明：
    * 企业需要使用外部联系人管理secret所获取的accesstoken来调用（accesstoken如何获取？）；
    * 第三方应用需拥有“企业客户”权限。
    * 第三方应用调用时，返回的跟进人follow_user仅包含应用可见范围之内的成员。
    * 如何绑定微信开发者ID
    * 1、登录企业的管理后台-外部联系人-api，点击绑定去到微信公众平台进行授权，支持绑定公众号和小程序（需要同时绑定微信开放平台）；绑定的公众号或小程序主体需与企业微信主体一致，暂且支持绑定一个
    * 2、绑定完成，即可通过接口获取微信联系人所对应的微信union id
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "external_contact":
    *    {
    *        "external_userid":"woAJ2GCAAAXtWyujaWJHDDGi0mACH71w",
    *        "name":"李四",
    *        "position":"Mangaer",
    *        "avatar":"http://p.qlogo.cn/bizmail/IcsdgagqefergqerhewSdage/0",
    *        "corp_name":"腾讯",
    *        "corp_full_name":"腾讯科技有限公司",
    *        "type":2,
    *        "gender":1,
    *        "unionid":"ozynqsulJFCZ2z1aYeS8h-nuasdfR1",
    *     "external_profile":{
    *       "external_attr":[
    *         {
    *           "type":0,
    *           "name":"文本名称",
    *           "text":
    *                   {
    *             "value":"文本"
    *           }
    *         },
    *         {
    *           "type":1,
    *           "name":"网页名称",
    *           "web":
    *                   {
    *             "url":"http://www.test.com",
    *             "title":"标题"
    *           }
    *         },
    *         {
    *           "type":2,
    *           "name":"测试app",
    *           "miniprogram":
    *                    {
    *                       "appid": "wx8bd80126147df384",
    *                       "pagepath": "/index",
    *                       "title": "my miniprogram"
    *           }
    *         }
    *       ]
    *     }
    *    },
    *     "follow_user":
    *     [
    *         {
    *         "userid":"rocky",
    *         "remark":"李部长",
    *         "description":"对接采购事物",
    *         "createtime":1525779812
    *         },
    *         {
    *         "userid":"tommy",
    *         "remark":"李总",
    *         "description":"采购问题咨询",
    *         "createtime":1525881637
    *         }
    *     ]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * external_userid      外部联系人的userid
    * name                 外部联系人的姓名
    * avatar               外部联系人头像，第三方不可获取
    * type                 外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户
    * gender               外部联系人性别
    * unionid              外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业绑定了微信开发者ID有此字段。查看绑定方法
    * position             外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
    * corp_name            外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
    * corp_full_name       外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
    * external_profile     外部联系人的自定义展示信息，可以有多个字段和多种类型，包括文本，网页和小程序，仅当联系人类型是企业微信用户时有此字段，字段详情见对外属性；
    * follow_user.userid   添加了此外部联系人的企业成员userid
    * follow_user.remark   该成员对此外部联系人的备注
    * follow_user.description 该成员对此外部联系人的描述
    * follow_user.createtime 该成员添加此外部联系人的时间
    * 
    */
    @GET
    @Path("get_external_contact")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetExternalContactResult getExternalContact(@QueryParam("access_token")@NotNull("内容为空") String accessToken, @QueryParam("external_userid")@NotNull("内容为空") String externalUserid);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetExternalContactResult")
public class GetExternalContactResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @JsonProperty("external_contact")
    private ExternalContact externalContact;
    @JsonProperty("follow_user")
    private FollowUser[] followUser;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    public ExternalContact getExternalContact() {
        return externalContact;
    }
    public void setExternalContact(ExternalContact externalContact) {
        this.externalContact = externalContact;
    }
    public FollowUser[] getFollowUser() {
        return followUser;
    }
    public void setFollowUser(FollowUser[] followUser) {
        this.followUser = followUser;
    }
}
@ApiModel("GetExternalContactResult.ExternalContact")
public static class ExternalContact {
    @ApiModelProperty("外部联系人的userid")
    @JsonProperty("external_userid")
    private String externalUserid;
    @ApiModelProperty("外部联系人的姓名")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty("外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段")
    @JsonProperty("position")
    private String position;
    @ApiModelProperty("外部联系人头像，第三方不可获取")
    @JsonProperty("avatar")
    private String avatar;
    @ApiModelProperty("外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段")
    @JsonProperty("corp_name")
    private String corpName;
    @ApiModelProperty("外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段")
    @JsonProperty("corp_full_name")
    private String corpFullName;
    @ApiModelProperty("外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户")
    @JsonProperty("type")
    private Integer type;
    @ApiModelProperty("外部联系人性别")
    @JsonProperty("gender")
    private Integer gender;
    @ApiModelProperty("外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业绑定了微信开发者ID有此字段。查看绑定方法")
    @JsonProperty("unionid")
    private String unionid;
    @ApiModelProperty("外部联系人的自定义展示信息，可以有多个字段和多种类型，包括文本，网页和小程序，仅当联系人类型是企业微信用户时有此字段，字段详情见对外属性；")
    @JsonProperty("external_profile")
    private ExternalProfile externalProfile;
    /**
     * 外部联系人的userid
     */
    public String getExternalUserid() {
        return externalUserid;
    }
    /**
     * 外部联系人的userid
     */
    public void setExternalUserid(String externalUserid) {
        this.externalUserid = externalUserid;
    }
    /**
     * 外部联系人的姓名
     */
    public String getName() {
        return name;
    }
    /**
     * 外部联系人的姓名
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
     */
    public String getPosition() {
        return position;
    }
    /**
     * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
     */
    public void setPosition(String position) {
        this.position = position;
    }
    /**
     * 外部联系人头像，第三方不可获取
     */
    public String getAvatar() {
        return avatar;
    }
    /**
     * 外部联系人头像，第三方不可获取
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    /**
     * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
     */
    public String getCorpName() {
        return corpName;
    }
    /**
     * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
     */
    public void setCorpName(String corpName) {
        this.corpName = corpName;
    }
    /**
     * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
     */
    public String getCorpFullName() {
        return corpFullName;
    }
    /**
     * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
     */
    public void setCorpFullName(String corpFullName) {
        this.corpFullName = corpFullName;
    }
    /**
     * 外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户
     */
    public Integer getType() {
        return type;
    }
    /**
     * 外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户
     */
    public void setType(Integer type) {
        this.type = type;
    }
    /**
     * 外部联系人性别
     */
    public Integer getGender() {
        return gender;
    }
    /**
     * 外部联系人性别
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }
    /**
     * 外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业绑定了微信开发者ID有此字段。查看绑定方法
     */
    public String getUnionid() {
        return unionid;
    }
    /**
     * 外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业绑定了微信开发者ID有此字段。查看绑定方法
     */
    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }
    /**
     * 外部联系人的自定义展示信息，可以有多个字段和多种类型，包括文本，网页和小程序，仅当联系人类型是企业微信用户时有此字段，字段详情见对外属性；
     */
    public ExternalProfile getExternalProfile() {
        return externalProfile;
    }
    /**
     * 外部联系人的自定义展示信息，可以有多个字段和多种类型，包括文本，网页和小程序，仅当联系人类型是企业微信用户时有此字段，字段详情见对外属性；
     */
    public void setExternalProfile(ExternalProfile externalProfile) {
        this.externalProfile = externalProfile;
    }
}
@ApiModel("GetExternalContactResult.ExternalContact.ExternalProfile")
public static class ExternalProfile {
    @JsonProperty("external_attr")
    private ExternalAttr[] externalAttr;
    public ExternalAttr[] getExternalAttr() {
        return externalAttr;
    }
    public void setExternalAttr(ExternalAttr[] externalAttr) {
        this.externalAttr = externalAttr;
    }
}
@ApiModel("GetExternalContactResult.ExternalContact.ExternalProfile.ExternalAttr")
public static class ExternalAttr {
    @ApiModelProperty("外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户")
    @JsonProperty("type")
    private Integer type;
    @ApiModelProperty("外部联系人的姓名")
    @JsonProperty("name")
    private String name;
    @JsonProperty("text")
    private Text text;
    /**
     * 外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户
     */
    public Integer getType() {
        return type;
    }
    /**
     * 外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户
     */
    public void setType(Integer type) {
        this.type = type;
    }
    /**
     * 外部联系人的姓名
     */
    public String getName() {
        return name;
    }
    /**
     * 外部联系人的姓名
     */
    public void setName(String name) {
        this.name = name;
    }
    public Text getText() {
        return text;
    }
    public void setText(Text text) {
        this.text = text;
    }
}
@ApiModel("GetExternalContactResult.ExternalContact.ExternalProfile.ExternalAttr.Text")
public static class Text {
    @JsonProperty("value")
    private String value;
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
@ApiModel("GetExternalContactResult.FollowUser")
public static class FollowUser {
    @JsonProperty("userid")
    private String userid;
    @JsonProperty("remark")
    private String remark;
    @JsonProperty("description")
    private String description;
    @JsonProperty("createtime")
    private Integer createtime;
    public String getUserid() {
        return userid;
    }
    public void setUserid(String userid) {
        this.userid = userid;
    }
    public String getRemark() {
        return remark;
    }
    public void setRemark(String remark) {
        this.remark = remark;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public Integer getCreatetime() {
        return createtime;
    }
    public void setCreatetime(Integer createtime) {
        this.createtime = createtime;
    }
}





-----------------------------over over over over over-------------------------