    /**
    * 应用支持推送文本、图片、视频、文件、图文等类型。
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * 说明：
    * 各个消息类型的具体POST格式参考以下文档。
    * 如果部分接收人无权限或不存在，发送仍然执行，但会返回无效的部分（即invaliduser或invalidparty或invalidtag），常见的原因是接收人不在应用的可见范围内。
    * 
    * 返回结果：
    * {
    *    "errcode" : 0,
    *    "errmsg" : "ok",
    *    "invaliduser" : "userid1|userid2",
    *    "invalidparty" : "partyid1|partyid2",
    *    "invalidtag":"tagid1|tagid2"
    *  }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              异常code
    * errmsg               异常信息
    * invaliduser          无效用户，不区分大小写，返回的列表都统一转为小写
    * invalidparty         无效部门
    * invalidtag           无效标签
    * 
    */
    @POST
    @Path("send")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SendResult send(@QueryParam("access_token")@NotNull("内容为空") String accessToken);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("SendResult")
public class SendResult {
    @ApiModelProperty("异常code")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("异常信息")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("无效用户，不区分大小写，返回的列表都统一转为小写")
    @JsonProperty("invaliduser")
    private String invaliduser;
    @ApiModelProperty("无效部门")
    @JsonProperty("invalidparty")
    private String invalidparty;
    @ApiModelProperty("无效标签")
    @JsonProperty("invalidtag")
    private String invalidtag;
    /**
     * 异常code
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 异常code
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 异常信息
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 异常信息
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 无效用户，不区分大小写，返回的列表都统一转为小写
     */
    public String getInvaliduser() {
        return invaliduser;
    }
    /**
     * 无效用户，不区分大小写，返回的列表都统一转为小写
     */
    public void setInvaliduser(String invaliduser) {
        this.invaliduser = invaliduser;
    }
    /**
     * 无效部门
     */
    public String getInvalidparty() {
        return invalidparty;
    }
    /**
     * 无效部门
     */
    public void setInvalidparty(String invalidparty) {
        this.invalidparty = invalidparty;
    }
    /**
     * 无效标签
     */
    public String getInvalidtag() {
        return invalidtag;
    }
    /**
     * 无效标签
     */
    public void setInvalidtag(String invalidtag) {
        this.invalidtag = invalidtag;
    }
}





-----------------------------over over over over over-------------------------