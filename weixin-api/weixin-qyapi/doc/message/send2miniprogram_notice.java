    /**
    * 应用支持推送文本、图片、视频、文件、图文等类型。
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "touser" : "zhangsan|lisi",
    *    "toparty": "1|2",
    *    "totag": "1|2",
    *    "msgtype" : "miniprogram_notice",
    *    "miniprogram_notice" : {
    *         "appid": "wx123123123123123",
    *         "page": "pages/index?userid=zhangsan&orderid=123123123",
    *         "title": "会议室预订成功通知",
    *         "description": "4月27日 16:16",
    *         "emphasis_first_item": true,
    *         "content_item": [
    *             {
    *                 "key": "会议室",
    *                 "value": "402"
    *             },
    *             {
    *                 "key": "会议地点",
    *                 "value": "广州TIT-402会议室"
    *             },
    *             {
    *                 "key": "会议时间",
    *                 "value": "2018年8月1日 09:00-09:30"
    *             },
    *             {
    *                 "key": "参与人员",
    *                 "value": "周剑轩"
    *             }
    *         ]
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）
    * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。
    * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。
    * msgtype              是.        消息类型，此时固定为：miniprogram_notice
    * appid                是.        小程序appid，必须是与当前小程序应用关联的小程序
    * page                 否.        点击消息卡片后的小程序页面，仅限本小程序内的页面。该字段不填则消息点击后不跳转。
    * title                是.        消息标题，长度限制4-12个汉字
    * description          否.        消息描述，长度限制4-12个汉字
    * emphasis_first_item  否.        是否放大第一个content_item
    * content_item         否.        消息内容键值对，最多允许10个item
    * key                  是.        长度10个汉字以内
    * value                是.        长度30个汉字以内
    * 说明：
    * 
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @POST
    @Path("send")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode send(@QueryParam("access_token") String accessToken, SendBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("SendBody")
public class SendBody {
    @ApiModelProperty("成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）")
    @JsonProperty("touser")
    private String touser;
    @ApiModelProperty("部门ID列表，多个接收者用‘|’分隔，最多支持100个。")
    @JsonProperty("toparty")
    private String toparty;
    @ApiModelProperty("标签ID列表，多个接收者用‘|’分隔，最多支持100个。")
    @JsonProperty("totag")
    private String totag;
    @ApiModelProperty("消息类型，此时固定为：miniprogram_notice")
    @NotNull("msgtype属性为空")
    @JsonProperty("msgtype")
    private String msgtype;
    @JsonProperty("miniprogram_notice")
    private MiniprogramNotice miniprogramNotice;
    /**
     * 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）
     */
    public String getTouser() {
        return touser;
    }
    /**
     * 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）
     */
    public void setTouser(String touser) {
        this.touser = touser;
    }
    /**
     * 部门ID列表，多个接收者用‘|’分隔，最多支持100个。
     */
    public String getToparty() {
        return toparty;
    }
    /**
     * 部门ID列表，多个接收者用‘|’分隔，最多支持100个。
     */
    public void setToparty(String toparty) {
        this.toparty = toparty;
    }
    /**
     * 标签ID列表，多个接收者用‘|’分隔，最多支持100个。
     */
    public String getTotag() {
        return totag;
    }
    /**
     * 标签ID列表，多个接收者用‘|’分隔，最多支持100个。
     */
    public void setTotag(String totag) {
        this.totag = totag;
    }
    /**
     * 消息类型，此时固定为：miniprogram_notice
     */
    public String getMsgtype() {
        return msgtype;
    }
    /**
     * 消息类型，此时固定为：miniprogram_notice
     */
    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }
    public MiniprogramNotice getMiniprogramNotice() {
        return miniprogramNotice;
    }
    public void setMiniprogramNotice(MiniprogramNotice miniprogramNotice) {
        this.miniprogramNotice = miniprogramNotice;
    }
}
@ApiModel("SendBody.MiniprogramNotice")
public static class MiniprogramNotice {
    @ApiModelProperty("小程序appid，必须是与当前小程序应用关联的小程序")
    @NotNull("appid属性为空")
    @JsonProperty("appid")
    private String appid;
    @ApiModelProperty("点击消息卡片后的小程序页面，仅限本小程序内的页面。该字段不填则消息点击后不跳转。")
    @JsonProperty("page")
    private String page;
    @ApiModelProperty("消息标题，长度限制4-12个汉字")
    @NotNull("title属性为空")
    @JsonProperty("title")
    private String title;
    @ApiModelProperty("消息描述，长度限制4-12个汉字")
    @JsonProperty("description")
    private String description;
    @ApiModelProperty("是否放大第一个content_item")
    @JsonProperty("emphasis_first_item")
    private Boolean emphasisFirstItem;
    @ApiModelProperty("消息内容键值对，最多允许10个item")
    @JsonProperty("content_item")
    private ContentItem[] contentItem;
    /**
     * 小程序appid，必须是与当前小程序应用关联的小程序
     */
    public String getAppid() {
        return appid;
    }
    /**
     * 小程序appid，必须是与当前小程序应用关联的小程序
     */
    public void setAppid(String appid) {
        this.appid = appid;
    }
    /**
     * 点击消息卡片后的小程序页面，仅限本小程序内的页面。该字段不填则消息点击后不跳转。
     */
    public String getPage() {
        return page;
    }
    /**
     * 点击消息卡片后的小程序页面，仅限本小程序内的页面。该字段不填则消息点击后不跳转。
     */
    public void setPage(String page) {
        this.page = page;
    }
    /**
     * 消息标题，长度限制4-12个汉字
     */
    public String getTitle() {
        return title;
    }
    /**
     * 消息标题，长度限制4-12个汉字
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * 消息描述，长度限制4-12个汉字
     */
    public String getDescription() {
        return description;
    }
    /**
     * 消息描述，长度限制4-12个汉字
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * 是否放大第一个content_item
     */
    public Boolean getEmphasisFirstItem() {
        return emphasisFirstItem;
    }
    /**
     * 是否放大第一个content_item
     */
    public void setEmphasisFirstItem(Boolean emphasisFirstItem) {
        this.emphasisFirstItem = emphasisFirstItem;
    }
    /**
     * 消息内容键值对，最多允许10个item
     */
    public ContentItem[] getContentItem() {
        return contentItem;
    }
    /**
     * 消息内容键值对，最多允许10个item
     */
    public void setContentItem(ContentItem[] contentItem) {
        this.contentItem = contentItem;
    }
}
@ApiModel("SendBody.MiniprogramNotice.ContentItem")
public static class ContentItem {
    @ApiModelProperty("长度10个汉字以内")
    @NotNull("key属性为空")
    @JsonProperty("key")
    private String key;
    @ApiModelProperty("长度30个汉字以内")
    @NotNull("value属性为空")
    @JsonProperty("value")
    private String value;
    /**
     * 长度10个汉字以内
     */
    public String getKey() {
        return key;
    }
    /**
     * 长度10个汉字以内
     */
    public void setKey(String key) {
        this.key = key;
    }
    /**
     * 长度30个汉字以内
     */
    public String getValue() {
        return value;
    }
    /**
     * 长度30个汉字以内
     */
    public void setValue(String value) {
        this.value = value;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------