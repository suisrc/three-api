    /**
    * 应用支持推送文本、图片、视频、文件、图文等类型。
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "touser" : "UserID1|UserID2|UserID3",
    *    "toparty" : "PartyID1|PartyID2",
    *    "totag" : "TagID1 | TagID2",
    *    "msgtype" : "text",
    *    "agentid" : 1,
    *    "text" : {
    *        "content" : "你的快递已到，请携带工卡前往邮件中心领取。\n出发前可查看<a href=\"http://work.weixin.qq.com\">邮件中心视频实况</a>，聪明避开排队。"
    *    },
    *    "safe":0
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向该企业应用的全部成员发送
    * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * msgtype              是.        消息类型，此时固定为：text
    * agentid              是.        企业应用的id，整型。可在应用的设置页面查看
    * content              是.        消息内容，最长不超过2048个字节
    * safe                 否.        表示是否是保密消息，0表示否，1表示是，默认0
    * 说明：
    * 
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @POST
    @Path("send")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode send(@QueryParam("access_token") String accessToken, SendBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("SendBody")
public class SendBody {
    @ApiModelProperty("成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向该企业应用的全部成员发送")
    @JsonProperty("touser")
    private String touser;
    @ApiModelProperty("部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数")
    @JsonProperty("toparty")
    private String toparty;
    @ApiModelProperty("标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数")
    @JsonProperty("totag")
    private String totag;
    @ApiModelProperty("消息类型，此时固定为：text")
    @NotNull("msgtype属性为空")
    @JsonProperty("msgtype")
    private String msgtype;
    @ApiModelProperty("企业应用的id，整型。可在应用的设置页面查看")
    @NotNull("agentid属性为空")
    @JsonProperty("agentid")
    private Integer agentid;
    @JsonProperty("text")
    private Text text;
    @ApiModelProperty("表示是否是保密消息，0表示否，1表示是，默认0")
    @JsonProperty("safe")
    private Integer safe;
    /**
     * 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为&#64;all，则向该企业应用的全部成员发送
     */
    public String getTouser() {
        return touser;
    }
    /**
     * 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为&#64;all，则向该企业应用的全部成员发送
     */
    public void setTouser(String touser) {
        this.touser = touser;
    }
    /**
     * 部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public String getToparty() {
        return toparty;
    }
    /**
     * 部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public void setToparty(String toparty) {
        this.toparty = toparty;
    }
    /**
     * 标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public String getTotag() {
        return totag;
    }
    /**
     * 标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public void setTotag(String totag) {
        this.totag = totag;
    }
    /**
     * 消息类型，此时固定为：text
     */
    public String getMsgtype() {
        return msgtype;
    }
    /**
     * 消息类型，此时固定为：text
     */
    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }
    /**
     * 企业应用的id，整型。可在应用的设置页面查看
     */
    public Integer getAgentid() {
        return agentid;
    }
    /**
     * 企业应用的id，整型。可在应用的设置页面查看
     */
    public void setAgentid(Integer agentid) {
        this.agentid = agentid;
    }
    public Text getText() {
        return text;
    }
    public void setText(Text text) {
        this.text = text;
    }
    /**
     * 表示是否是保密消息，0表示否，1表示是，默认0
     */
    public Integer getSafe() {
        return safe;
    }
    /**
     * 表示是否是保密消息，0表示否，1表示是，默认0
     */
    public void setSafe(Integer safe) {
        this.safe = safe;
    }
}
@ApiModel("SendBody.Text")
public static class Text {
    @ApiModelProperty("消息内容，最长不超过2048个字节")
    @NotNull("content属性为空")
    @JsonProperty("content")
    private String content;
    /**
     * 消息内容，最长不超过2048个字节
     */
    public String getContent() {
        return content;
    }
    /**
     * 消息内容，最长不超过2048个字节
     */
    public void setContent(String content) {
        this.content = content;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------