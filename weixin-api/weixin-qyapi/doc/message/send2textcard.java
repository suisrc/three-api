    /**
    * 应用支持推送文本、图片、视频、文件、图文等类型。
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "touser" : "UserID1|UserID2|UserID3",
    *    "toparty" : "PartyID1 | PartyID2",
    *    "totag" : "TagID1 | TagID2",
    *    "msgtype" : "textcard",
    *    "agentid" : 1,
    *    "textcard" : {
    *             "title" : "领奖通知",
    *             "description" : "<div class=\"gray\">2016年9月26日</div> <div class=\"normal\">恭喜你抽中iPhone 7一台，领奖码：xxxx</div><div class=\"highlight\">请于2016年10月10日前联系行政同事领取</div>",
    *             "url" : "URL",
    *             "btntxt":"更多"
    *    }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
    * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * msgtype              是.        消息类型，此时固定为：textcard
    * agentid              是.        企业应用的id，整型。可在应用的设置页面查看
    * title                是.        标题，不超过128个字节，超过会自动截断
    * description          是.        描述，不超过512个字节，超过会自动截断
    * url                  是.        点击后跳转的链接。
    * btntxt               否.        按钮文字。
    * 说明：
    * 
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @POST
    @Path("send")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode send(@QueryParam("access_token") String accessToken, SendBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("SendBody")
public class SendBody {
    @ApiModelProperty("成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送")
    @JsonProperty("touser")
    private String touser;
    @ApiModelProperty("部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数")
    @JsonProperty("toparty")
    private String toparty;
    @ApiModelProperty("标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数")
    @JsonProperty("totag")
    private String totag;
    @ApiModelProperty("消息类型，此时固定为：textcard")
    @NotNull("msgtype属性为空")
    @JsonProperty("msgtype")
    private String msgtype;
    @ApiModelProperty("企业应用的id，整型。可在应用的设置页面查看")
    @NotNull("agentid属性为空")
    @JsonProperty("agentid")
    private Integer agentid;
    @JsonProperty("textcard")
    private Textcard textcard;
    /**
     * 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为&#64;all，则向关注该企业应用的全部成员发送
     */
    public String getTouser() {
        return touser;
    }
    /**
     * 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为&#64;all，则向关注该企业应用的全部成员发送
     */
    public void setTouser(String touser) {
        this.touser = touser;
    }
    /**
     * 部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public String getToparty() {
        return toparty;
    }
    /**
     * 部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public void setToparty(String toparty) {
        this.toparty = toparty;
    }
    /**
     * 标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public String getTotag() {
        return totag;
    }
    /**
     * 标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public void setTotag(String totag) {
        this.totag = totag;
    }
    /**
     * 消息类型，此时固定为：textcard
     */
    public String getMsgtype() {
        return msgtype;
    }
    /**
     * 消息类型，此时固定为：textcard
     */
    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }
    /**
     * 企业应用的id，整型。可在应用的设置页面查看
     */
    public Integer getAgentid() {
        return agentid;
    }
    /**
     * 企业应用的id，整型。可在应用的设置页面查看
     */
    public void setAgentid(Integer agentid) {
        this.agentid = agentid;
    }
    public Textcard getTextcard() {
        return textcard;
    }
    public void setTextcard(Textcard textcard) {
        this.textcard = textcard;
    }
}
@ApiModel("SendBody.Textcard")
public static class Textcard {
    @ApiModelProperty("标题，不超过128个字节，超过会自动截断")
    @NotNull("title属性为空")
    @JsonProperty("title")
    private String title;
    @ApiModelProperty("描述，不超过512个字节，超过会自动截断")
    @NotNull("description属性为空")
    @JsonProperty("description")
    private String description;
    @ApiModelProperty("点击后跳转的链接。")
    @NotNull("url属性为空")
    @JsonProperty("url")
    private String url;
    @ApiModelProperty("按钮文字。")
    @JsonProperty("btntxt")
    private String btntxt;
    /**
     * 标题，不超过128个字节，超过会自动截断
     */
    public String getTitle() {
        return title;
    }
    /**
     * 标题，不超过128个字节，超过会自动截断
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * 描述，不超过512个字节，超过会自动截断
     */
    public String getDescription() {
        return description;
    }
    /**
     * 描述，不超过512个字节，超过会自动截断
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * 点击后跳转的链接。
     */
    public String getUrl() {
        return url;
    }
    /**
     * 点击后跳转的链接。
     */
    public void setUrl(String url) {
        this.url = url;
    }
    /**
     * 按钮文字。
     */
    public String getBtntxt() {
        return btntxt;
    }
    /**
     * 按钮文字。
     */
    public void setBtntxt(String btntxt) {
        this.btntxt = btntxt;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------