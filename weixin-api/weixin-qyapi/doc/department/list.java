    /**
    * 获取部门列表
    * -:getDepartmentList
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=ACCESS_TOKEN&id=ID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * id                   否.        部门id。获取指定部门及其下的子部门。
    * 说明：
    * 只能拉取token对应的应用的权限范围内的部门列表
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "department": [
    *        {
    *            "id": 2,
    *            "name": "广州研发中心",
    *            "parentid": 1,
    *            "order": 10
    *        },
    *        {
    *            "id": 3,
    *            "name": "邮箱产品部",
    *            "parentid": 2,
    *            "order": 40
    *        }
    *    ]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * department           部门列表数据。
    * id                   创建的部门id
    * name                 部门名称
    * parentid             父亲部门id。根部门为1
    * order                在父部门中的次序值。order值大的排序靠前。值范围是[0,
    * 
    */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetDepartmentListResult getDepartmentList(@QueryParam("access_token")@NotNull("内容为空") String accessToken, @QueryParam("id")@NotNull("内容为空") String id);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetDepartmentListResult")
public class GetDepartmentListResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("部门列表数据。")
    @JsonProperty("department")
    private Department[] department;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 部门列表数据。
     */
    public Department[] getDepartment() {
        return department;
    }
    /**
     * 部门列表数据。
     */
    public void setDepartment(Department[] department) {
        this.department = department;
    }
}
@ApiModel("GetDepartmentListResult.Department")
public static class Department {
    @ApiModelProperty("创建的部门id")
    @JsonProperty("id")
    private Integer id;
    @ApiModelProperty("部门名称")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty("父亲部门id。根部门为1")
    @JsonProperty("parentid")
    private Integer parentid;
    @ApiModelProperty("在父部门中的次序值。order值大的排序靠前。值范围是[0,")
    @JsonProperty("order")
    private Integer order;
    /**
     * 创建的部门id
     */
    public Integer getId() {
        return id;
    }
    /**
     * 创建的部门id
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 部门名称
     */
    public String getName() {
        return name;
    }
    /**
     * 部门名称
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 父亲部门id。根部门为1
     */
    public Integer getParentid() {
        return parentid;
    }
    /**
     * 父亲部门id。根部门为1
     */
    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }
    /**
     * 在父部门中的次序值。order值大的排序靠前。值范围是[0,
     */
    public Integer getOrder() {
        return order;
    }
    /**
     * 在父部门中的次序值。order值大的排序靠前。值范围是[0,
     */
    public void setOrder(Integer order) {
        this.order = order;
    }
}





-----------------------------over over over over over-------------------------