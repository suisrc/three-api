    /**
    * 创建部门
    * -:createDepartment
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "name": "广州研发中心",
    *    "parentid": 1,
    *    "order": 1,
    *    "id": 2
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * name                 是.        部门名称。长度限制为1~32个字符，字符不能包括\:?”<>｜
    * parentid             是.        父部门id，32位整型
    * order                否.        在父部门中的次序值。order值大的排序靠前。有效的值范围是[0, 2^32)
    * id                   否.        部门id，32位整型，指定时必须大于1。若不填该参数，将自动生成id
    * 说明：
    * 应用须拥有父部门的管理权限。
    * 注意，部门的最大层级为15层；部门总数不能超过3万个；每个部门下的节点不能超过3万个。建议保证创建的部门和对应部门成员是串行化处理。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "created",
    *    "id": 2
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * id                   创建的部门id
    * 
    */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    CreateDepartmentResult createDepartment(@QueryParam("access_token")@NotNull("内容为空") String accessToken, CreateDepartmentBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("CreateDepartmentBody")
public class CreateDepartmentBody {
    @ApiModelProperty("部门名称。长度限制为1~32个字符，字符不能包括\\:?”<>｜")
    @NotNull("name属性为空")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty("父部门id，32位整型")
    @NotNull("parentid属性为空")
    @JsonProperty("parentid")
    private Integer parentid;
    @ApiModelProperty("在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,")
    @JsonProperty("order")
    private Integer order;
    @ApiModelProperty("部门id，32位整型，指定时必须大于1。若不填该参数，将自动生成id")
    @JsonProperty("id")
    private Integer id;
    /**
     * 部门名称。长度限制为1~32个字符，字符不能包括\:?”&lt;&gt;｜
     */
    public String getName() {
        return name;
    }
    /**
     * 部门名称。长度限制为1~32个字符，字符不能包括\:?”&lt;&gt;｜
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 父部门id，32位整型
     */
    public Integer getParentid() {
        return parentid;
    }
    /**
     * 父部门id，32位整型
     */
    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }
    /**
     * 在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,
     */
    public Integer getOrder() {
        return order;
    }
    /**
     * 在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,
     */
    public void setOrder(Integer order) {
        this.order = order;
    }
    /**
     * 部门id，32位整型，指定时必须大于1。若不填该参数，将自动生成id
     */
    public Integer getId() {
        return id;
    }
    /**
     * 部门id，32位整型，指定时必须大于1。若不填该参数，将自动生成id
     */
    public void setId(Integer id) {
        this.id = id;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("CreateDepartmentResult")
public class CreateDepartmentResult {
    @ApiModelProperty("返回码")
    @JsonProperty("errcode")
    private Integer errcode;
    @ApiModelProperty("对返回码的文本描述内容")
    @JsonProperty("errmsg")
    private String errmsg;
    @ApiModelProperty("创建的部门id")
    @JsonProperty("id")
    private Integer id;
    /**
     * 返回码
     */
    public Integer getErrcode() {
        return errcode;
    }
    /**
     * 返回码
     */
    public void setErrcode(Integer errcode) {
        this.errcode = errcode;
    }
    /**
     * 对返回码的文本描述内容
     */
    public String getErrmsg() {
        return errmsg;
    }
    /**
     * 对返回码的文本描述内容
     */
    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }
    /**
     * 创建的部门id
     */
    public Integer getId() {
        return id;
    }
    /**
     * 创建的部门id
     */
    public void setId(Integer id) {
        this.id = id;
    }
}





-----------------------------over over over over over-------------------------