    /**
    * 更新部门
    * -:updateDepartment
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "id": 2,
    *    "name": "广州研发中心",
    *    "parentid": 1,
    *    "order": 1
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * id                   是.        部门id
    * name                 否.        部门名称。长度限制为1~32个字符，字符不能包括\:?”<>｜
    * parentid             否.        父部门id
    * order                否.        在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,
    * 说明：
    * 
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "updated"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode updateDepartment(@QueryParam("access_token")@NotNull("内容为空") String accessToken, UpdateDepartmentBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("UpdateDepartmentBody")
public class UpdateDepartmentBody {
    @ApiModelProperty("部门id")
    @NotNull("id属性为空")
    @JsonProperty("id")
    private Integer id;
    @ApiModelProperty("部门名称。长度限制为1~32个字符，字符不能包括\\:?”<>｜")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty("父部门id")
    @JsonProperty("parentid")
    private Integer parentid;
    @ApiModelProperty("在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,")
    @JsonProperty("order")
    private Integer order;
    /**
     * 部门id
     */
    public Integer getId() {
        return id;
    }
    /**
     * 部门id
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 部门名称。长度限制为1~32个字符，字符不能包括\:?”&lt;&gt;｜
     */
    public String getName() {
        return name;
    }
    /**
     * 部门名称。长度限制为1~32个字符，字符不能包括\:?”&lt;&gt;｜
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 父部门id
     */
    public Integer getParentid() {
        return parentid;
    }
    /**
     * 父部门id
     */
    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }
    /**
     * 在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,
     */
    public Integer getOrder() {
        return order;
    }
    /**
     * 在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,
     */
    public void setOrder(Integer order) {
        this.order = order;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------