package net.icgear.three.weixin.qyapi.rest.api;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.suisrc.core.fasterxml.FasterFactory.Type;
import com.suisrc.core.fasterxml.FasterFactoryImpl;

import net.icgear.three.weixin.qyapi.rest.dto.message.IMessage;
import net.icgear.three.weixin.qyapi.rest.dto.message.MiniprogramNoticeMessage;
import net.icgear.three.weixin.qyapi.rest.dto.message.MiniprogramNoticeMessage.ContentItem;
import net.icgear.three.weixin.qyapi.rest.dto.message.MpnewsMessage;
import net.icgear.three.weixin.qyapi.rest.dto.message.MpnewsMessage.Articles;
import net.icgear.three.weixin.qyapi.rest.dto.message.SendBody;
import net.icgear.three.weixin.qyapi.rest.dto.message.TextMessage;

public class T1MessageManagerRest {

    /**
     * 
     */
    private ObjectMapper getObjectMapper(Type type) {
        return new ObjectMapper();
    }

    /**
     * 
     */
    public <T> String convert2String(ObjectMapper mapper, T bean) {
        try {
            ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
            return writer.writeValueAsString(bean);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }
    
    /**
     * 
     */
    public <T> String bean2String(T bean, Type type) {
        return convert2String(getObjectMapper(type), bean);
    }
    
    @Before
    public void setUp() throws Exception {}

    @Test
    public void test1() {
        TextMessage msg = new TextMessage();
        msg.setContent("hello, world");
        SendBody<IMessage> body = new SendBody<>();
        body.setTouser("小明");
        body.setData(msg);
        
        String content = new FasterFactoryImpl().bean2String(body, Type.JSON);
        System.out.println(content);
    }
    
    @Test
    public void test2() {
        Articles[] arts = new Articles[1];
        arts[0] = new Articles();
        arts[0].setContent("hello, word");
        MpnewsMessage msg = new MpnewsMessage();
        msg.setArticles(arts);
        SendBody<IMessage> body = new SendBody<>();
        body.setTouser("小明");
        body.setData(msg);
        
        String content = bean2String(body, Type.JSON);
        System.out.println(content);
    }
    
    @Test
    public void test3() {
        ContentItem[] items = new ContentItem[1];
        items[0] = new ContentItem();
        items[0].setKey("时间");
        items[0].setValue("2018");
        MiniprogramNoticeMessage msg = new MiniprogramNoticeMessage();
        msg.setContentItem(items);
        msg.setAppid("123");
        SendBody<IMessage> body = new SendBody<>();
        body.setTouser("小明");
        body.setData(msg);
        
        String content = bean2String(body, Type.JSON);
        System.out.println(content);
    }

}
