package net.icgear.three.weixin.qyapi.src;

import org.junit.Test;

import com.suisrc.core.Global;
import com.suisrc.jaxrsapi.tools.Utils;

public class T1User {
    
    private String getFolder() {
        return "Z:\\zero\\icgear\\three-api\\weixin-api\\weixin-qyapi\\doc\\user\\";
    }

    @Test
    public void test1() {
        String name = "user_create";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test2() {
        String name = "user_get";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test3() {
        String name = "user_update";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test4() {
        String name = "user_delete";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test5() {
        String name = "user_batchdelete";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test6() {
        String name = "user_simplelist";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test7() {
        String name = "user_list";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test8() {
        String name = "userid_openid_1";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test9() {
        String name = "userid_openid_2";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test10() {
        String name = "user_authsucc";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test11() {
        String name = "user_getuserinfo";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test12() {
        String name = "user_getuserdetail";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

}
