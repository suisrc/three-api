package net.icgear.three.weixin.qyapi.src;

import org.junit.Test;

import com.suisrc.core.Global;
import com.suisrc.jaxrsapi.tools.Utils;

public class T4Tag {
    
    private String getFolder() {
        return "Z:\\zero\\icgear\\three-api\\weixin-api\\weixin-qyapi\\doc\\tag\\";
    }

    @Test
    public void test1() {
        String name = "create";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test2() {
        String name = "update_name";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test3() {
        String name = "delete";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test4() {
        String name = "get";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test5() {
        String name = "addtagusers";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test6() {
        String name = "deltagusers";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test7() {
        String name = "list";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

}
