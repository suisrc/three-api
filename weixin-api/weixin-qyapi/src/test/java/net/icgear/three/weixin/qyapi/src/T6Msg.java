package net.icgear.three.weixin.qyapi.src;

import org.junit.Test;

import com.suisrc.core.Global;
import com.suisrc.jaxrsapi.tools.Utils;

public class T6Msg {
    
    private String getFolder() {
        return "Z:\\zero\\icgear\\three-api\\weixin-api\\weixin-qyapi\\doc\\message\\";
    }

    @Test
    public void test1() {
        String name = "send";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

    @Test
    public void test2() {
        String name = "send2text";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    
    @Test
    public void test3() {
        String name = "send2image";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    
    @Test
    public void test4() {
        String name = "send2video";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    
    @Test
    public void test5() {
        String name = "send2textcard";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }


    @Test
    public void test6() {
        String name = "send2mpnews";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    
    @Test
    public void test7() {
        String name = "send2miniprogram_notice";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }


}
