package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 获取部门成员
 * 
 * @author Y13
 *
 */
@ApiModel("SimplelistUserResult")
public class SimplelistUserResult extends WxErrCode {
    private static final long serialVersionUID = -1104929354756694713L;

    @ApiModel("SimplelistUserResult.Userlist")
    public static class Userlist {

        @ApiModelProperty("成员UserID。对应管理端的帐号")
        @JsonProperty("userid")
        private String userid;

        @ApiModelProperty("成员名称")
        @JsonProperty("name")
        private String name;

        @ApiModelProperty("成员所属部门")
        @JsonProperty("department")
        private Integer[] department;

        /**
         * 成员UserID。对应管理端的帐号
         */
        public String getUserid() {
            return userid;
        }

        /**
         * 成员UserID。对应管理端的帐号
         */
        public void setUserid(String userid) {
            this.userid = userid;
        }

        /**
         * 成员名称
         */
        public String getName() {
            return name;
        }

        /**
         * 成员名称
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * 成员所属部门
         */
        public Integer[] getDepartment() {
            return department;
        }

        /**
         * 成员所属部门
         */
        public void setDepartment(Integer[] department) {
            this.department = department;
        }
    }

    @ApiModelProperty("成员列表")
    @JsonProperty("userlist")
    private Userlist[] userlist;

    /**
     * 成员列表
     */
    public Userlist[] getUserlist() {
        return userlist;
    }

    /**
     * 成员列表
     */
    public void setUserlist(Userlist[] userlist) {
        this.userlist = userlist;
    }
}
