package net.icgear.three.weixin.qyapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsConsts;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.qyapi.QyConsts;
import net.icgear.three.weixin.qyapi.rest.dto.batch.InviteBody;
import net.icgear.three.weixin.qyapi.rest.dto.batch.InviteResult;
import net.icgear.three.weixin.qyapi.rest.dto.batch.SyncJobBody;
import net.icgear.three.weixin.qyapi.rest.dto.batch.SyncJobResult;
import net.icgear.three.weixin.qyapi.rest.dto.batch.SyncResult;

/**
 * 批量处理服务
 * 
 * 异步批量接口说明
 * 异步批量接口用于大批量数据的处理，提交后接口即返回，企业微信会在后台继续执行任务。
 * 执行完成后，企业微信后台会通过任务事件通知企业获取结果。事件的内容是加密的，解密过程请参考 消息的加解密处理，任务事件请参考异步任务完成事件推送。
 * 目前，仅为通讯录更新提供了异步批量接口
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin/batch")
public interface BatchServiceRest {
    final String NAMED = JaxrsConsts.separator + "net.icgear.three.weixin.qyapi.rest.api.BatchServiceRest";
    
    /**
    * 邀请成员
    * 企业可通过接口批量邀请成员使用企业微信，邀请后将通过短信或邮件下发通知。
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/batch/invite?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "user" : ["UserID1", "UserID2", "UserID3"],
    *    "party" : ["PartyID1", "PartyID2"],
    *    "tag" : ["TagID1", "TagID2"]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * user                 否.        成员ID列表,
    * party                否.        部门ID列表，最多支持100个。
    * tag                  否.        标签ID列表，最多支持100个。
    * 说明：
    * 须拥有指定成员、部门或标签的查看权限。
    * user, party, tag三者不能同时为空；
    * 如果部分接收人无权限或不存在，邀请仍然执行，但会返回无效的部分（即invaliduser或invalidparty或invalidtag）;
    * 非认证企业每天邀请人数不能超过1000, 认证企业每天邀请人数不能超过5000；
    * 同一用户只须邀请一次，被邀请的用户如果未安装企业微信，在3天内每天会收到一次通知，最多持续3天。
    * 因为邀请频率是异步检查的，所以调用接口返回成功，并不代表接收者一定能收到邀请消息（可能受上述频率限制无法接收）。
    * 
    * 返回结果：
    * {
    *    "errcode" : 0,
    *    "errmsg" : "ok",
    *    "invaliduser" : ["UserID1", "UserID2"],
    *    "invalidparty" : ["PartyID1", "PartyID2"],
    *    "invalidtag": ["TagID1", "TagID2"]
    *  }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * invaliduser          非法成员列表
    * invalidparty         非法部门列表
    * invalidtag           非法标签列表
    * 
    */
    @POST
    @Path("invite")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    InviteResult invite(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @NotNull("请求包体为空")InviteBody body);
    default InviteResult invite(InviteBody body) {
        return invite((String)null, body);
    }
    
    /**
     * 通讯录更新
     * 通讯录更新接口提供三种更新方法：1) 增量更新成员 2）全量覆盖成员 3) 全量覆盖部门。后面具体描述更新逻辑。
     * 如果企业要做到与企业微信通讯录完全一致，可先调用全量覆盖部门接口，再调用全量覆盖成员接口，即可保持通讯录完全一致。
     * 
     * 使用步骤：
     * 1.下载接口对应的csv模板，如果有扩展属性，请自行添加（扩展属性需要在WEB管理端创建后才生效，否则忽略未知属性的赋值。见创建扩展属性）
     * 2.按模板的格式，生成接口所需的数据文件（必须保持为utf-8格式）
     * 3.通过上传媒体文件接口上传数据文件，获取media_id
     * 4.调用通讯录更新接口，传入media_id参数
     * 5.接收任务完成事件，并获取任务执行结果
     */
    
    /**
    * 增量更新成员
    * 接口说明 ：
    * 本接口以userid（帐号）为主键，增量更新企业微信通讯录成员。请先下载CSV模板(下载增量更新成员模版)，根据需求填写文件内容。
    * 注意事项：
    * 模板中的部门需填写部门ID，多个部门用分号分隔，部门ID必须为数字，根部门的部门id默认为1
    * 文件中存在、通讯录中也存在的成员，更新成员在文件中指定的字段值
    * 文件中存在、通讯录中不存在的成员，执行添加操作
    * 通讯录中存在、文件中不存在的成员，保持不变
    * 成员字段更新规则：可自行添加扩展字段。文件中有指定的字段，以指定的字段值为准；文件中没指定的字段，不更新
    * -:syncUser
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/syncuser?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "media_id":"xxxxxx",
    *     "to_invite": true,
    *     "callback":
    *     {
    *          "url": "xxx",
    *          "token": "xxx",
    *          "encodingaeskey": "xxx"
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * media_id             是.        上传的csv文件的media_id
    * to_invite            否.        是否邀请新建的成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
    * callback             否.        回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
    * url                  否.        企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
    * token                否.        用于生成签名
    * encodingaeskey       否.        用于消息体的加密，是AES密钥的Base64编码
    * 说明：
    * 须拥有通讯录的写权限。
    * 
    * 返回结果：
    * {
    *     "errcode": 0,
    *     "errmsg": "ok",
    *     "jobid": "xxxxx"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * jobid                异步任务id，最大长度为64字节
    * 
    */
    @POST
    @Path("syncuser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SyncJobResult syncUser(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @NotNull("请求包体为空") SyncJobBody body);
    default SyncJobResult syncUser(SyncJobBody body) {
        return syncUser(body);
    }
    
    /**
    * 全量覆盖成员
    * 接口说明：
    * 本接口以userid为主键，全量覆盖企业的通讯录成员，任务完成后企业的通讯录成员与提交的文件完全保持一致。请先下载CSV文件(下载全量覆盖成员模版)，根据需求填写文件内容。
    * 注意事项：
    * 模板中的部门需填写部门ID，多个部门用分号分隔，部门ID必须为数字，根部门的部门id默认为1
    * 文件中存在、通讯录中也存在的成员，完全以文件为准
    * 文件中存在、通讯录中不存在的成员，执行添加操作
    * 通讯录中存在、文件中不存在的成员，执行删除操作。出于安全考虑，下面两种情形系统将中止导入并返回相应的错误码。
    * 需要删除的成员多于50人，且多于现有人数的20%以上
    * 需要删除的成员少于50人，且多于现有人数的80%以上
    * 成员字段更新规则：可自行添加扩展字段。文件中有指定的字段，以指定的字段值为准；文件中没指定的字段，不更新
    * -:replaceUser
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/replaceuser?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "media_id":"xxxxxx",
    *     "to_invite": true,
    *     "callback":
    *     {
    *          "url": "xxx",
    *          "token": "xxx",
    *          "encodingaeskey": "xxx"
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * media_id             是.        上传的csv文件的media_id
    * to_invite            否.        是否邀请新建的成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
    * callback             否.        回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
    * url                  否.        企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
    * token                否.        用于生成签名
    * encodingaeskey       否.        用于消息体的加密，是AES密钥的Base64编码
    * 说明：
    * 须拥有通讯录的写权限。
    * 
    * 返回结果：
    * {
    *     "errcode": 0,
    *     "errmsg": "ok",
    *     "jobid": "xxxxx"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * jobid                异步任务id，最大长度为64字节
    * 
    */
    @POST
    @Path("replaceuser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SyncJobResult replaceUser(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @NotNull("请求包体为空") SyncJobBody body);
    default SyncJobResult replaceUser(SyncJobBody body) {
        return replaceUser(body);
    }
    
    /**
    * 全量覆盖部门
    * 接口说明：
    * 本接口以partyid为键，全量覆盖企业的通讯录组织架构，任务完成后企业的通讯录组织架构与提交的文件完全保持一致。请先下载CSV文件(下载全量覆盖部门模版)，根据需求填写文件内容。
    * 注意事项：
    * 文件中存在、通讯录中也存在的部门，执行修改操作
    * 文件中存在、通讯录中不存在的部门，执行添加操作
    * 文件中不存在、通讯录中存在的部门，当部门下没有任何成员或子部门时，执行删除操作
    * 文件中不存在、通讯录中存在的部门，当部门下仍有成员或子部门时，暂时不会删除，当下次导入成员把人从部门移出后自动删除
    * CSV文件中，部门名称、部门ID、父部门ID为必填字段，部门ID必须为数字，根部门的部门id默认为1；排序为可选字段，置空或填0不修改排序, order值大的排序靠前。
    * -:replaceParty
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/replaceparty?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "media_id":"xxxxxx",
    *     "callback":
    *     {
    *          "url": "xxx",
    *          "token": "xxx",
    *          "encodingaeskey": "xxx"
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * media_id             是.        上传的csv文件的media_id
    * callback             否.        回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
    * url                  否.        企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
    * token                否.        用于生成签名
    * encodingaeskey       否.        用于消息体的加密，是AES密钥的Base64编码
    * 说明：
    * 须拥有通讯录的写权限。
    * 
    * 返回结果：
    * {
    *     "errcode": 0,
    *     "errmsg": "ok",
    *     "jobid": "xxxxx"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * jobid                异步任务id，最大长度为64字节
    * 
    */
    @POST
    @Path("replaceparty")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SyncJobResult replaceParty(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @NotNull("请求包体为空") SyncJobBody body);
    default SyncJobResult replaceParty(SyncJobBody body) {
        return replaceParty(body);
    }
    
    /**
    * 获取异步任务结果
    * -:getResult
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/batch/getresult?access_token=ACCESS_TOKEN&jobid=JOBID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * jobid                是.        异步任务id，最大长度为64字节
    * 说明：
    * 只能查询已经提交过的历史任务。
    * result结构：type为sync_user、replace_user时：
    * "result": [
    *     {
    *          "userid":"lisi",
    *          "errcode":0,
    *          "errmsg":"ok"
    *     },
    *     {
    *          "userid":"zhangsan",
    *          "errcode":0,
    *          "errmsg":"ok"
    *     }
    * ]
    * 参数    说明
    * userid：成员UserID。对应管理端的帐号
    * ------------------------------------------------------
    * result结构：type为replace_party时：
    * "result": [
    *     {
    *          "action":1,
    *          "partyid":1,
    *          "errcode":0,
    *          "errmsg":"ok"
    *     },
    *     {
    *          "action":4,
    *          "partyid":2,
    *          "errcode":0,
    *          "errmsg":"ok"
    *     }
    * ]
    * action：操作类型（按位或）：1 新建部门 ，2 更改部门名称， 4 移动部门， 8 修改部门排序
    * partyid：部门ID
    * 
    * 返回结果：
    * {
    *     "errcode": 0,
    *     "errmsg": "ok",
    *     "status": 1,
    *     "type": "replace_user",
    *     "total": 3,
    *     "percentage": 33,
    *     "result": [{},{}]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容，例如无权限错误，键值冲突，格式错误等
    * status               任务状态，整型，1表示任务开始，2表示任务进行中，3表示任务已完成
    * type                 操作类型，字节串，目前分别有：1.
    * total                任务运行总条数
    * percentage           目前运行百分比，当任务完成时为100
    * result               详细的处理结果，具体格式参考下面说明。当任务完成后此字段有效
    * 
    */
    @GET
    @Path("getresult")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SyncResult getResult(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken,
            @QueryParam("jobid")@NotNull("jobid为空") String jobid);
    default SyncResult getResult(String jobid) {
        return getResult((String)null, jobid);
    }
}
