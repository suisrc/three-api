package net.icgear.three.weixin.qyapi.rest.dto.three;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 获取应用的管理员列表
 * @author Y13
 *
 */
@ApiModel("AdminList")
public class AdminListResult extends WxErrCode {
    private static final long serialVersionUID = 1L;

    @ApiModel("AdminList.Admin")
    public static class Admin {
        
        @ApiModelProperty("管理员的userid")
        @JsonProperty("userid")
        private String userid;
        
        @ApiModelProperty("该管理员对应用的权限：0=发消息权限，1=管理权限")
        @JsonProperty("auth_type")
        private String authType;

        public String getUserid() {
            return userid;
        }
        public void setUserid(String userid) {
            this.userid = userid;
        }
        public String getAuthType() {
            return authType;
        }
        public void setAuthType(String authType) {
            this.authType = authType;
        }
    }

    @ApiModelProperty("应用的管理员列表（不包括外部管理员）")
    @JsonProperty("admin")
    private Admin[] admin;

    public Admin[] getAdmin() {
        return admin;
    }
    public void setAdmin(Admin[] admin) {
        this.admin = admin;
    }
}
