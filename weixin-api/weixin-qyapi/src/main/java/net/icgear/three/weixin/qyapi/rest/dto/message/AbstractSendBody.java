package net.icgear.three.weixin.qyapi.rest.dto.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.Value;

import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.qyapi.QyConsts;

/**
 * {
 *    "touser" : "UserID1|UserID2|UserID3",
 *    "toparty" : "PartyID1|PartyID2",
 *    "totag" : "TagID1 | TagID2",
 *    "msgtype" : "?",
 *    "agentid" : 1,
 *    "?" : { }, // 未知
 *    "safe":0
 * }
 * @author Y13
 *
 */
@JsonInclude(Include.NON_NULL)
public abstract class AbstractSendBody {

    @ApiModelProperty("成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向该企业应用的全部成员发送")
    @JsonProperty("touser")
    private String touser;
    
    @ApiModelProperty("部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数")
    @JsonProperty("toparty")
    private String toparty;
    
    @ApiModelProperty("标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数")
    @JsonProperty("totag")
    private String totag;
    
    @ApiModelProperty("消息类型，有构建时候指定")
    @NotNull("msgtype属性为空")
    @JsonProperty("msgtype")
    private String msgtype;
    
    @Value(QyConsts.T_APP_AGENT_ID)
    @ApiModelProperty("企业应用的id，整型。可在应用的设置页面查看")
    @NotNull("agentid属性为空")
    @JsonProperty("agentid")
    private Integer agentid;
    
    @ApiModelProperty("表示是否是保密消息，0表示否，1表示是，默认0")
    @JsonProperty("safe")
    private Integer safe;

    /**
     * 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为&#64;all，则向该企业应用的全部成员发送
     */
    public String getTouser() {
        return touser;
    }

    /**
     * 成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为&#64;all，则向该企业应用的全部成员发送
     */
    public void setTouser(String touser) {
        this.touser = touser;
    }

    /**
     * 部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public String getToparty() {
        return toparty;
    }

    /**
     * 部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public void setToparty(String toparty) {
        this.toparty = toparty;
    }

    /**
     * 标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public String getTotag() {
        return totag;
    }

    /**
     * 标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为&#64;all时忽略本参数
     */
    public void setTotag(String totag) {
        this.totag = totag;
    }

    /**
     * 消息类型
     */
    public String getMsgtype() {
        return msgtype;
    }

    /**
     * 消息类型
     */
    public void setMsgtype(String msgtype) {
        this.msgtype = msgtype;
    }

    /**
     * 企业应用的id，整型。可在应用的设置页面查看
     */
    public Integer getAgentid() {
        return agentid;
    }

    /**
     * 企业应用的id，整型。可在应用的设置页面查看
     */
    public void setAgentid(Integer agentid) {
        this.agentid = agentid;
    }

    /**
     * 表示是否是保密消息，0表示否，1表示是，默认0
     */
    public Integer getSafe() {
        return safe;
    }

    /**
     * 表示是否是保密消息，0表示否，1表示是，默认0
     */
    public void setSafe(Integer safe) {
        this.safe = safe;
    }
}
