package net.icgear.three.weixin.qyapi.rest.dto.three;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 获取企业永久授权码
 * 
 * @author Y13
 *
 */
@ApiModel("PermanentCodeResult")
public class PermanentCodeResult extends WxErrCode {
    private static final long serialVersionUID = 1L;
    
    @ApiModel("PermanentCodeResult.AuthCorpInfo")
    public static class AuthCorpInfo {

        @ApiModelProperty("授权方企业微信id")
        @JsonProperty("corpid")
        private String corpId;

        @ApiModelProperty("授权方企业微信名称")
        @JsonProperty("corp_name")
        private String corpName;

        @ApiModelProperty("授权方企业微信类型，认证号：verified, 注册号：unverified")
        @JsonProperty("corp_type")
        private String corpType;

        @ApiModelProperty("授权方企业微信方形头像")
        @JsonProperty("corp_square_logo_url")
        private String corpSquareLogoUrl;
        
        @ApiModelProperty("授权方企业微信圆形头像")
        @JsonProperty("corp_round_logo_url")
        private String corpRoundLogoUrl;

        @ApiModelProperty("授权方企业微信用户规模")
        @JsonProperty("corp_user_max")
        private int corpUserMax;

        @ApiModelProperty("未知")
        @JsonProperty("corp_agent_max")
        private int corpAgentMax;

        @ApiModelProperty("所绑定的企业微信主体名称(仅认证过的企业有)")
        @JsonProperty("corp_full_name")
        private String corpFullName;

        @ApiModelProperty("认证到期时间")
        @JsonProperty("verified_end_time")
        private long verifiedEndTime;

        @ApiModelProperty("企业类型，1. 企业; 2. 政府以及事业单位; 3. 其他组织, 4.团队号")
        @JsonProperty("subject_type")
        private int subjectType;

        @ApiModelProperty("授权企业在微工作台（原企业号）的二维码，可用于关注微工作台")
        @JsonProperty("corp_wxqrcode")
        private String corpWxqrcode;

        @ApiModelProperty("企业规模。当企业未设置该属性时，值为空")
        @JsonProperty("corp_scale")
        private String corpScale;

        @ApiModelProperty("企业所属行业。当企业未设置该属性时，值为空")
        @JsonProperty("corp_industry")
        private String corpIndustry;

        @ApiModelProperty("企业所属子行业。当企业未设置该属性时，值为空")
        @JsonProperty("corp_sub_industry")
        private String corpSubIndustry;

        @ApiModelProperty("企业所在地信息, 为空时表示未知")
        @JsonProperty("location")
        private String location;

        public String getCorpId() {
            return corpId;
        }
        public void setCorpId(String corpId) {
            this.corpId = corpId;
        }
        public String getCorpName() {
            return corpName;
        }
        public void setCorpName(String corpName) {
            this.corpName = corpName;
        }
        public String getCorpType() {
            return corpType;
        }
        public void setCorpType(String corpType) {
            this.corpType = corpType;
        }
        public String getCorpSquareLogoUrl() {
            return corpSquareLogoUrl;
        }
        public void setCorpSquareLogoUrl(String corpSquareLogoUrl) {
            this.corpSquareLogoUrl = corpSquareLogoUrl;
        }
        public String getCorpRoundLogoUrl() {
            return corpRoundLogoUrl;
        }
        public void setCorpRoundLogoUrl(String corpRoundLogoUrl) {
            this.corpRoundLogoUrl = corpRoundLogoUrl;
        }
        public int getCorpUserMax() {
            return corpUserMax;
        }
        public void setCorpUserMax(int corpUserMax) {
            this.corpUserMax = corpUserMax;
        }
        public int getCorpAgentMax() {
            return corpAgentMax;
        }
        public void setCorpAgentMax(int corpAgentMax) {
            this.corpAgentMax = corpAgentMax;
        }
        public String getCorpFullName() {
            return corpFullName;
        }
        public void setCorpFullName(String corpFullName) {
            this.corpFullName = corpFullName;
        }
        public long getVerifiedEndTime() {
            return verifiedEndTime;
        }
        public void setVerifiedEndTime(long verifiedEndTime) {
            this.verifiedEndTime = verifiedEndTime;
        }
        public int getSubjectType() {
            return subjectType;
        }
        public void setSubjectType(int subjectType) {
            this.subjectType = subjectType;
        }
        public String getCorpWxqrcode() {
            return corpWxqrcode;
        }
        public void setCorpWxqrcode(String corpWxqrcode) {
            this.corpWxqrcode = corpWxqrcode;
        }
        public String getCorpScale() {
            return corpScale;
        }
        public void setCorpScale(String corpScale) {
            this.corpScale = corpScale;
        }
        public String getCorpIndustry() {
            return corpIndustry;
        }
        public void setCorpIndustry(String corpIndustry) {
            this.corpIndustry = corpIndustry;
        }
        public String getCorpSubIndustry() {
            return corpSubIndustry;
        }
        public void setCorpSubIndustry(String corpSubIndustry) {
            this.corpSubIndustry = corpSubIndustry;
        }
        public String getLocation() {
            return location;
        }
        public void setLocation(String location) {
            this.location = location;
        }
    }

    @ApiModel("PermanentCodeResult.AuthInfo")
    public static class AuthInfo {

        @ApiModelProperty("授权的应用信息，注意是一个数组，但仅旧的多应用套件授权时会返回多个agent，对新的单应用授权，永远只返回一个agent")
        @JsonProperty("agent")
        private Agent[] agent;

        public Agent[] getAgent() {
            return agent;
        }
        public void setAgent(Agent[] agent) {
            this.agent = agent;
        }
        
    }

    @ApiModel("PermanentCodeResult.Agent")
    public static class Agent {

        @ApiModelProperty("授权方应用id")
        @JsonProperty("agentid")
        private int agentId;

        @ApiModelProperty("授权方应用名字")
        @JsonProperty("name")
        private String name;

        @ApiModelProperty("授权方应用方形头像")
        @JsonProperty("round_logo_url")
        private String roundLogoUrl;

        @ApiModelProperty("授权方应用圆形头像")
        @JsonProperty("square_logo_url")
        private String squareLogoUrl;

        @ApiModelProperty("旧的多应用套件中的对应应用id，新开发者请忽略")
        @JsonProperty("appid")
        private int appid;

        @ApiModelProperty("应用对应的权限")
        @JsonProperty("privilege")
        private Privilege privilege;

        public int getAgentId() {
            return agentId;
        }
        public void setAgentId(int agentId) {
            this.agentId = agentId;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getRoundLogoUrl() {
            return roundLogoUrl;
        }
        public void setRoundLogoUrl(String roundLogoUrl) {
            this.roundLogoUrl = roundLogoUrl;
        }
        public String getSquareLogoUrl() {
            return squareLogoUrl;
        }
        public void setSquareLogoUrl(String squareLogoUrl) {
            this.squareLogoUrl = squareLogoUrl;
        }
        public int getAppid() {
            return appid;
        }
        public void setAppid(int appid) {
            this.appid = appid;
        }
        public Privilege getPrivilege() {
            return privilege;
        }
        public void setPrivilege(Privilege privilege) {
            this.privilege = privilege;
        }
        
    }

    @ApiModel("PermanentCodeResult.Privilege")
    public static class Privilege {

        @ApiModelProperty("权限等级。1:通讯录基本信息只读，2:通讯录全部信息只读，3:通讯录全部信息读写，4:单个基本信息只读，5:通讯录全部信息只写")
        @JsonProperty("level")
        private int level;

        @ApiModelProperty("应用可见范围（部门）")
        @JsonProperty("allow_party")
        private int[] allowParty;

        @ApiModelProperty("应用可见范围（成员）")
        @JsonProperty("allow_user")
        private String[] allowUser;

        @ApiModelProperty("应用可见范围（标签）")
        @JsonProperty("allow_tag")
        private int[] allowTag;

        @ApiModelProperty("额外通讯录（部门）")
        @JsonProperty("extra_party")
        private int[] extraParty;

        @ApiModelProperty("额外通讯录（成员）")
        @JsonProperty("extra_user")
        private String[] extraUser;

        @ApiModelProperty("额外通讯录（标签）")
        @JsonProperty("extra_tag")
        private int[] extraTag;

        public int getLevel() {
            return level;
        }
        public void setLevel(int level) {
            this.level = level;
        }
        public int[] getAllowParty() {
            return allowParty;
        }
        public void setAllowParty(int[] allowParty) {
            this.allowParty = allowParty;
        }
        public String[] getAllowUser() {
            return allowUser;
        }
        public void setAllowUser(String[] allowUser) {
            this.allowUser = allowUser;
        }
        public int[] getAllowTag() {
            return allowTag;
        }
        public void setAllowTag(int[] allowTag) {
            this.allowTag = allowTag;
        }
        public int[] getExtraParty() {
            return extraParty;
        }
        public void setExtraParty(int[] extraParty) {
            this.extraParty = extraParty;
        }
        public String[] getExtraUser() {
            return extraUser;
        }
        public void setExtraUser(String[] extraUser) {
            this.extraUser = extraUser;
        }
        public int[] getExtraTag() {
            return extraTag;
        }
        public void setExtraTag(int[] extraTag) {
            this.extraTag = extraTag;
        }
    }

    @ApiModel("PermanentCodeResult.AuthUserInfo")
    public static class AuthUserInfo {

        @ApiModelProperty("授权管理员的userid，可能为空（内部管理员一定有，不可更改）")
        @JsonProperty("userid")
        private String userId;

        @ApiModelProperty("授权管理员的name，可能为空（内部管理员一定有，不可更改）")
        @JsonProperty("name")
        private String name;

        @ApiModelProperty("授权管理员的头像url")
        @JsonProperty("avatar")
        private String avatar;

        public String getUserId() {
            return userId;
        }
        public void setUserId(String userId) {
            this.userId = userId;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getAvatar() {
            return avatar;
        }
        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }
    }
    
    @ApiModelProperty("授权方（企业）access_token,最长为512字节")
    @JsonProperty("access_token")
    private String accessToken;
    
    @ApiModelProperty("授权方（企业）access_token超时时间")
    @JsonProperty("expires_in")
    private long expiresIn = -1;

    @ApiModelProperty("企业微信永久授权码,最长为512字节")
    @JsonProperty("permanent_code")
    private String permanentCode;

    @ApiModelProperty("授权方企业信息")
    @JsonProperty("auth_corp_info")
    private AuthCorpInfo authCorpInfo;

    @ApiModelProperty("授权信息。如果是通讯录应用，且没开启实体应用，是没有该项的。通讯录应用拥有企业通讯录的全部信息读写权限")
    @JsonProperty("auth_info")
    private AuthInfo authInfo;

    @ApiModelProperty("额外通讯录（标签）")
    @JsonProperty("auth_user_info")
    private AuthUserInfo authUserInfo;

    public String getAccessToken() {
        return accessToken;
    }
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    public long getExpiresIn() {
        return expiresIn;
    }
    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }
    public String getPermanentCode() {
        return permanentCode;
    }
    public void setPermanentCode(String permanentCode) {
        this.permanentCode = permanentCode;
    }
    public AuthCorpInfo getAuthCorpInfo() {
        return authCorpInfo;
    }
    public void setAuthCorpInfo(AuthCorpInfo authCorpInfo) {
        this.authCorpInfo = authCorpInfo;
    }
    public AuthInfo getAuthInfo() {
        return authInfo;
    }
    public void setAuthInfo(AuthInfo authInfo) {
        this.authInfo = authInfo;
    }
    public AuthUserInfo getAuthUserInfo() {
        return authUserInfo;
    }
    public void setAuthUserInfo(AuthUserInfo authUserInfo) {
        this.authUserInfo = authUserInfo;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
