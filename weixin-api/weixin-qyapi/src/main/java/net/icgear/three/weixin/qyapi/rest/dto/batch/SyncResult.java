package net.icgear.three.weixin.qyapi.rest.dto.batch;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 获取同步结果
 * 
 * @author Y13
 *
 */
@ApiModel("SyncResult")
public class SyncResult extends WxErrCode {
    private static final long serialVersionUID = 5318133079540040665L;

    @ApiModel("SyncResult.Result")
    public static class Result extends WxErrCode {
        private static final long serialVersionUID = 8932912592763490524L;
        
        /**
         * 成员UserID。对应管理端的帐号
         */
        private String userid;
        
        /**
         * 操作类型（按位或）：1 新建部门 ，2 更改部门名称， 4 移动部门， 8 修改部门排序
         */
        private Integer action;
        
        /**
         * 部门ID
         */
        private Integer partyid;

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public Integer getAction() {
            return action;
        }

        public void setAction(Integer action) {
            this.action = action;
        }

        public Integer getPartyid() {
            return partyid;
        }

        public void setPartyid(Integer partyid) {
            this.partyid = partyid;
        }
        
    }
    
    @ApiModelProperty("任务状态，整型，1表示任务开始，2表示任务进行中，3表示任务已完成")
    @JsonProperty("status")
    private Integer status;
    
    @ApiModelProperty("操作类型，字节串，目前分别有：1.")
    @JsonProperty("type")
    private String type;
    
    @ApiModelProperty("任务运行总条数")
    @JsonProperty("total")
    private Integer total;
    
    @ApiModelProperty("目前运行百分比，当任务完成时为100")
    @JsonProperty("percentage")
    private Integer percentage;
    
    @ApiModelProperty("详细的处理结果，具体格式参考下面说明。当任务完成后此字段有效")
    @JsonProperty("result")
    private Result[] result;
    
    /**
     * 任务状态，整型，1表示任务开始，2表示任务进行中，3表示任务已完成
     */
    public Integer getStatus() {
        return status;
    }
    /**
     * 任务状态，整型，1表示任务开始，2表示任务进行中，3表示任务已完成
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
    /**
     * 操作类型，字节串，目前分别有：1.
     */
    public String getType() {
        return type;
    }
    /**
     * 操作类型，字节串，目前分别有：1.
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * 任务运行总条数
     */
    public Integer getTotal() {
        return total;
    }
    /**
     * 任务运行总条数
     */
    public void setTotal(Integer total) {
        this.total = total;
    }
    /**
     * 目前运行百分比，当任务完成时为100
     */
    public Integer getPercentage() {
        return percentage;
    }
    /**
     * 目前运行百分比，当任务完成时为100
     */
    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
    /**
     * 详细的处理结果，具体格式参考下面说明。当任务完成后此字段有效
     */
    public Result[] getResult() {
        return result;
    }
    /**
     * 详细的处理结果，具体格式参考下面说明。当任务完成后此字段有效
     */
    public void setResult(Result[] result) {
        this.result = result;
    }
}