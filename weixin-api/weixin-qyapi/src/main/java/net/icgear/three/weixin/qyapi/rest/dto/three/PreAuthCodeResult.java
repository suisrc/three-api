package net.icgear.three.weixin.qyapi.rest.dto.three;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 获取预授权码返回值
 * @author Y13
 *
 */
@ApiModel("PreAuthCodeResult")
public class PreAuthCodeResult extends WxErrCode {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("预授权码,最长为512字节")
    @JsonProperty("pre_auth_code")
    private String preAuthCode;
    
    @ApiModelProperty("有效期")
    @JsonProperty("expires_in")
    private long expiresIn = -1;

    public String getPreAuthCode() {
        return preAuthCode;
    }

    public void setPreAuthCode(String preAuthCode) {
        this.preAuthCode = preAuthCode;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }
    
}
