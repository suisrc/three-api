package net.icgear.three.weixin.qyapi.rest.dto.tag;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 增加标签成员
 * 
 * @author Y13
 *
 */
@ApiModel("TagUsersResult")
public class TagUsersResult extends WxErrCode {
    private static final long serialVersionUID = 7205169092757542465L;

    @ApiModelProperty("非法的成员帐号列表")
    @JsonProperty("invalidlist")
    private String invalidlist;

    @ApiModelProperty("非法的部门id列表")
    @JsonProperty("invalidparty")
    private Integer[] invalidparty;

    /**
     * 非法的成员帐号列表
     */
    public String getInvalidlist() {
        return invalidlist;
    }

    /**
     * 非法的成员帐号列表
     */
    public void setInvalidlist(String invalidlist) {
        this.invalidlist = invalidlist;
    }

    /**
     * 非法的部门id列表
     */
    public Integer[] getInvalidparty() {
        return invalidparty;
    }

    /**
     * 非法的部门id列表
     */
    public void setInvalidparty(Integer[] invalidparty) {
        this.invalidparty = invalidparty;
    }
}
