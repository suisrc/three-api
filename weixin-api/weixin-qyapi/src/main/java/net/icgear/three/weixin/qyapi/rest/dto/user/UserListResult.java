package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("UserListResult")
public class UserListResult extends WxErrCode {
    private static final long serialVersionUID = 2146873525745268940L;
    
    @ApiModelProperty("成员列表")
    @JsonProperty("userlist")
    private UserResult[] userlist;
    
    /**
     * 成员列表
     */
    public UserResult[] getUserlist() {
        return userlist;
    }
    /**
     * 成员列表
     */
    public void setUserlist(UserResult[] userlist) {
        this.userlist = userlist;
    }
}