package net.icgear.three.weixin.qyapi.rest.dto.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 文本消息
 * 
 * @author Y13
 *
 */
@ApiModel("SendBody.TextMessage")
public class TextMessage implements IMessage {

    @Override
    public String type() {
        return "text";
    }
    
    @ApiModelProperty("消息内容，最长不超过2048个字节")
    @NotNull("content属性为空")
    @JsonProperty("content")
    private String content;

    /**
     * 消息内容，最长不超过2048个字节
     */
    public String getContent() {
        return content;
    }

    /**
     * 消息内容，最长不超过2048个字节
     */
    public void setContent(String content) {
        this.content = content;
    }
}