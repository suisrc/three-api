package net.icgear.three.weixin.qyapi.rest.dto.message;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModelProperty;

/**
 * {
 *    "touser" : "UserID1|UserID2|UserID3",
 *    "toparty" : "PartyID1|PartyID2",
 *    "totag" : "TagID1 | TagID2",
 *    "msgtype" : "?",
 *    "agentid" : 1,
 *    "?" : { }, // 未知
 *    "safe":0
 * }
 * @author Y13
 *
 */
public class SendBody<T extends IMessage> extends AbstractSendBody {

    
    @ApiModelProperty("发送的消息内容")
    @JsonIgnore
    private T data;

    /**
     * 发送的消息内容
     * @return the data
     */
    public T getData() {
        return data;
    }
    
    /**
     * 发送的消息内容
     * @param data the data to set
     */
    public void setData(T data) {
        this.data = data;
    }
    
    
    
    @Override
    public String getMsgtype() {
        if(data == null) {
            throw new RuntimeException("消息的内容为空，无法获取消息的类型");
        }
        return data.type();
    }
    
    /**
     * 该方法被禁用，方法类型不可用
     */
    @Override
    @Deprecated
    public void setMsgtype(String msgtype) {
    }
    
    /**
     * 获取其他配置属性
     * @return
     */
    @JsonAnyGetter
    public Map<String, Object> getOther() {
        if (data == null) {
            return null;
        }
        String key = data.type();
        T value = data;
        Map<String, Object> other = new HashMap<>(1);
        other.put(key, value);
        return other;
    }
}
