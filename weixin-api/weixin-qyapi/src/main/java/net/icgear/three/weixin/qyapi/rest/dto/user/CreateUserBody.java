package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.qyapi.rest.dto.external.Extattr;
import net.icgear.three.weixin.qyapi.rest.dto.external.ExternalProfile;

/**
 * 创建成员请求包体
 * 
 * @author Y13
 *
 */
@ApiModel("CreateUserBody")
public class CreateUserBody {

    @ApiModelProperty("成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节")
    @NotNull("userid属性为空")
    @JsonProperty("userid")
    private String userid;

    @ApiModelProperty("成员名称。长度为1~64个字符")
    @NotNull("name属性为空")
    @JsonProperty("name")
    private String name;

    @ApiModelProperty("英文名。长度为1-64个字节，由字母、数字、点(.)、减号(-)、空格或下划线(_)组成")
    @JsonProperty("english_name")
    private String englishName;

    @ApiModelProperty("手机号码。企业内必须唯一，mobile/email二者不能同时为空")
    @JsonProperty("mobile")
    private String mobile;

    @ApiModelProperty("成员所属部门id列表,不超过20个")
    @NotNull("department属性为空")
    @JsonProperty("department")
    private Integer[] department;

    @ApiModelProperty("部门内的排序值，默认为0，成员次序以创建时间从小到大排列。数量必须和department一致，数值越大排序越前面。有效的值范围是[0,2^32)")
    @JsonProperty("order")
    private Integer[] order;

    @ApiModelProperty("职位信息。长度为0~128个字符")
    @JsonProperty("position")
    private String position;

    @ApiModelProperty("性别。1表示男性，2表示女性")
    @JsonProperty("gender")
    private String gender;

    @ApiModelProperty("邮箱。长度不超过64个字节，且为有效的email格式。企业内必须唯一，mobile/email二者不能同时为空")
    @JsonProperty("email")
    private String email;

    @ApiModelProperty("上级字段，标识是否为上级。在审批等应用里可以用来标识上级审批人")
    @JsonProperty("isleader")
    private Integer isleader;

    @ApiModelProperty("启用/禁用成员。1表示启用成员，0表示禁用成员")
    @JsonProperty("enable")
    private Integer enable;

    @ApiModelProperty("成员头像的mediaid，通过素材管理接口上传图片获得的mediaid")
    @JsonProperty("avatar_mediaid")
    private String avatarMediaid;

    @ApiModelProperty("座机。由1-32位的纯数字或’-‘号组成。")
    @JsonProperty("telephone")
    private String telephone;

    @ApiModelProperty("自定义字段。自定义字段需要先在WEB管理端添加，见扩展属性添加方法，否则忽略未知属性的赋值。自定义字段长度为0~32个字符，超过将被截断")
    @JsonProperty("extattr")
    private Extattr extattr;

    @ApiModelProperty("是否邀请该成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。")
    @JsonProperty("to_invite")
    private Boolean toInvite;

     @ApiModelProperty("成员对外属性，字段详情见对外属性")
     @JsonProperty("external_profile")
     private ExternalProfile externalProfile;

    /**
     * 成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
     */
    public String getUserid() {
        return userid;
    }

    /**
     * 成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * 成员名称。长度为1~64个字符
     */
    public String getName() {
        return name;
    }

    /**
     * 成员名称。长度为1~64个字符
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 英文名。长度为1-64个字节，由字母、数字、点(.)、减号(-)、空格或下划线(_)组成
     */
    public String getEnglishName() {
        return englishName;
    }

    /**
     * 英文名。长度为1-64个字节，由字母、数字、点(.)、减号(-)、空格或下划线(_)组成
     */
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    /**
     * 手机号码。企业内必须唯一，mobile/email二者不能同时为空
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 手机号码。企业内必须唯一，mobile/email二者不能同时为空
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 成员所属部门id列表,不超过20个
     */
    public Integer[] getDepartment() {
        return department;
    }

    /**
     * 成员所属部门id列表,不超过20个
     */
    public void setDepartment(Integer[] department) {
        this.department = department;
    }

    /**
     * 部门内的排序值，默认为0，成员次序以创建时间从小到大排列。数量必须和department一致，数值越大排序越前面。有效的值范围是[0,
     */
    public Integer[] getOrder() {
        return order;
    }

    /**
     * 部门内的排序值，默认为0，成员次序以创建时间从小到大排列。数量必须和department一致，数值越大排序越前面。有效的值范围是[0,
     */
    public void setOrder(Integer[] order) {
        this.order = order;
    }

    /**
     * 职位信息。长度为0~128个字符
     */
    public String getPosition() {
        return position;
    }

    /**
     * 职位信息。长度为0~128个字符
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * 性别。1表示男性，2表示女性
     */
    public String getGender() {
        return gender;
    }

    /**
     * 性别。1表示男性，2表示女性
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * 邮箱。长度不超过64个字节，且为有效的email格式。企业内必须唯一，mobile/email二者不能同时为空
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱。长度不超过64个字节，且为有效的email格式。企业内必须唯一，mobile/email二者不能同时为空
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 上级字段，标识是否为上级。在审批等应用里可以用来标识上级审批人
     */
    public Integer getIsleader() {
        return isleader;
    }

    /**
     * 上级字段，标识是否为上级。在审批等应用里可以用来标识上级审批人
     */
    public void setIsleader(Integer isleader) {
        this.isleader = isleader;
    }

    /**
     * 启用/禁用成员。1表示启用成员，0表示禁用成员
     */
    public Integer getEnable() {
        return enable;
    }

    /**
     * 启用/禁用成员。1表示启用成员，0表示禁用成员
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    /**
     * 成员头像的mediaid，通过素材管理接口上传图片获得的mediaid
     */
    public String getAvatarMediaid() {
        return avatarMediaid;
    }

    /**
     * 成员头像的mediaid，通过素材管理接口上传图片获得的mediaid
     */
    public void setAvatarMediaid(String avatarMediaid) {
        this.avatarMediaid = avatarMediaid;
    }

    /**
     * 座机。由1-32位的纯数字或’-‘号组成。
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * 座机。由1-32位的纯数字或’-‘号组成。
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * 自定义字段。自定义字段需要先在WEB管理端添加，见扩展属性添加方法，否则忽略未知属性的赋值。自定义字段长度为0~32个字符，超过将被截断
     */
    public Extattr getExtattr() {
        return extattr;
    }

    /**
     * 自定义字段。自定义字段需要先在WEB管理端添加，见扩展属性添加方法，否则忽略未知属性的赋值。自定义字段长度为0~32个字符，超过将被截断
     */
    public void setExtattr(Extattr extattr) {
        this.extattr = extattr;
    }

    /**
     * 是否邀请该成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
     */
    public Boolean getToInvite() {
        return toInvite;
    }

    /**
     * 是否邀请该成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
     */
    public void setToInvite(Boolean toInvite) {
        this.toInvite = toInvite;
    }
    
     /**
     * 成员对外属性，字段详情见对外属性
     */
     public ExternalProfile getExternalProfile() {
     return externalProfile;
     }
    
     /**
     * 成员对外属性，字段详情见对外属性
     */
     public void setExternalProfile(ExternalProfile externalProfile) {
     this.externalProfile = externalProfile;
     }
}
