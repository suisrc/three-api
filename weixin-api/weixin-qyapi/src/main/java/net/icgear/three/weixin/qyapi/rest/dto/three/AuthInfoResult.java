package net.icgear.three.weixin.qyapi.rest.dto.three;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.qyapi.rest.dto.three.PermanentCodeResult.AuthCorpInfo;
import net.icgear.three.weixin.qyapi.rest.dto.three.PermanentCodeResult.AuthInfo;

/**
 * 获取企业授权信息
 * @author Y13
 *
 */
@ApiModel("AuthInfoResult")
public class AuthInfoResult {

    @ApiModelProperty("授权方企业信息")
    @JsonProperty("auth_corp_info")
    private AuthCorpInfo authCorpInfo;

    @ApiModelProperty("授权信息。如果是通讯录应用，且没开启实体应用，是没有该项的。通讯录应用拥有企业通讯录的全部信息读写权限")
    @JsonProperty("auth_info")
    private AuthInfo authInfo;

    public AuthCorpInfo getAuthCorpInfo() {
        return authCorpInfo;
    }

    public void setAuthCorpInfo(AuthCorpInfo authCorpInfo) {
        this.authCorpInfo = authCorpInfo;
    }

    public AuthInfo getAuthInfo() {
        return authInfo;
    }

    public void setAuthInfo(AuthInfo authInfo) {
        this.authInfo = authInfo;
    }

}
