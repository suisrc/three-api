package net.icgear.three.weixin.qyapi.rest.dto.tag;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 创建标签
 * @author Y13
 *
 */
@ApiModel("CreateTagResult")
public class CreateTagResult extends WxErrCode {
    private static final long serialVersionUID = -1888110571313851743L;

    @ApiModelProperty("标签id")
    @JsonProperty("tagid")
    private Integer tagid;

    /**
     * 标签id
     */
    public Integer getTagid() {
        return tagid;
    }

    /**
     * 标签id
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
}
