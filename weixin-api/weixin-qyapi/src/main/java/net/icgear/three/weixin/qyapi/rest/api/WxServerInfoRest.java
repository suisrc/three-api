package net.icgear.three.weixin.qyapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsConsts;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.RetryProxy;
import com.suisrc.jaxrsapi.core.annotation.Value;
import com.suisrc.jaxrsapi.core.retry.ProxyRetryPredicate;
import com.suisrc.jaxrsapi.core.runtime.RetryPredicate;

import net.icgear.three.weixin.qyapi.QyApiHelper;
import net.icgear.three.weixin.qyapi.QyConsts;
import net.icgear.three.weixin.qyapi.rest.dto.ServerIpResult;
import net.icgear.three.weixin.qyapi.retry.WxAccessTokenReTryFilter;

/**
 * 获取微信服务器信息
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin")
public interface WxServerInfoRest {
    final String NAMED = JaxrsConsts.separator + "net.icgear.three.weixin.qyapi.rest.api.WxServerInfoRest";

    /**
     * 获取微信服务器IP地址 
     * 企业微信在回调企业指定的URL时，是通过特定的IP发送出去的。如果企业需要做防火墙配置，那么可以通过这个接口获取到所有相关的IP段。
     * 
     * https://qyapi.weixin.qq.com/cgi-bin/getcallbackip?access_token=ACCESS_TOKEN
     * 
     * @param accessToken 公众号的access_token
     * @return
     */
    @GET
    @Path("getcallbackip")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ServerIpResult getCallbackIp(@QueryParam("access_token") @Value(QyConsts.T_ACCESS_TOKEN) @NotNull("访问令牌为空") String accessToken);
    @RetryProxy
    @GET
    @Path("getcallbackip")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ServerIpResult getCallbackIpRetry(@QueryParam("access_token") @Value(QyConsts.T_ACCESS_TOKEN) @NotNull("访问令牌为空") String accessToken);
    default ServerIpResult getCallbackIpDef(String corpId) {
        return QyApiHelper.call(corpId, () -> getCallbackIp(null));
    }
    /**
     * 带有返回值内容检测重试
     * @param retry
     * @param corpId
     * @return
     */
    default ServerIpResult getCallbackIpDef(RetryPredicate<ServerIpResult> retry, String corpId) {
        return QyApiHelper.call(corpId, () -> ProxyRetryPredicate.proxy(retry, () -> getCallbackIp(null)));
    }
    /**
     * ACT: auto clear token
     * 
     * 该内容仅仅作为参考使用
     * @param retry
     * @param corpId
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    default ServerIpResult getCallbackIpDefByACT(String corpId) {
        return getCallbackIpDef((RetryPredicate)new WxAccessTokenReTryFilter(null), corpId);
    }
}
