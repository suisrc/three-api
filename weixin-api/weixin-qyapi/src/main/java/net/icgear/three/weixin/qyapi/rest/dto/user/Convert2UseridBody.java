package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * openid转userid
 * @author Y13
 *
 */
@ApiModel("Convert2UseridBody")
public class Convert2UseridBody {
    
    @ApiModelProperty("在使用企业支付之后，返回结果的openid")
    @NotNull("openid属性为空")
    @JsonProperty("openid")
    private String openid;
    /**
     * 在使用企业支付之后，返回结果的openid
     */
    public String getOpenid() {
        return openid;
    }
    /**
     * 在使用企业支付之后，返回结果的openid
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }
}