package net.icgear.three.weixin.qyapi.rest.dto.batch;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("InviteResult")
public class InviteResult extends WxErrCode {
    private static final long serialVersionUID = 6708277040163744195L;

    @ApiModelProperty("非法成员列表")
    @JsonProperty("invaliduser")
    private String[] invaliduser;

    @ApiModelProperty("非法部门列表")
    @JsonProperty("invalidparty")
    private String[] invalidparty;

    @ApiModelProperty("非法标签列表")
    @JsonProperty("invalidtag")
    private String[] invalidtag;

    /**
     * 非法成员列表
     */
    public String[] getInvaliduser() {
        return invaliduser;
    }

    /**
     * 非法成员列表
     */
    public void setInvaliduser(String[] invaliduser) {
        this.invaliduser = invaliduser;
    }

    /**
     * 非法部门列表
     */
    public String[] getInvalidparty() {
        return invalidparty;
    }

    /**
     * 非法部门列表
     */
    public void setInvalidparty(String[] invalidparty) {
        this.invalidparty = invalidparty;
    }

    /**
     * 非法标签列表
     */
    public String[] getInvalidtag() {
        return invalidtag;
    }

    /**
     * 非法标签列表
     */
    public void setInvalidtag(String[] invalidtag) {
        this.invalidtag = invalidtag;
    }
}
