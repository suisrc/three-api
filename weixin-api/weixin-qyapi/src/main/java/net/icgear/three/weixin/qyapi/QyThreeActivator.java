package net.icgear.three.weixin.qyapi;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import com.suisrc.core.Global;
import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.three.weixin.core.AbstractWeixinActivator;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxAccessToken;
import net.icgear.three.weixin.core.handler.CallbackActivatorHandler;
import net.icgear.three.weixin.core.handler.WxTokenHandler;
import net.icgear.three.weixin.core.suite.TicketSuite;
import net.icgear.three.weixin.qyapi.handler.DefaultQyPermanentHandler;
import net.icgear.three.weixin.qyapi.handler.PermanentHandler;
import net.icgear.three.weixin.qyapi.rest.api.QyThreeServiceRest;
import net.icgear.three.weixin.qyapi.rest.dto.three.CorpTokenResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.PermanentCodeResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.PreAuthCodeResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.SuiteTokenResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.UserInfo3rdResult;

/**
 * 第三方应用接入激活器
 * 
 * 当对企业应用绑定时候，需要该激活器
 * 
 * @author Y13
 */
public abstract class QyThreeActivator extends AbstractWeixinActivator {

  @Override
  public Set<Class<?>> getClasses() {
    return ReflectionUtils.getTypesAnnotatedWith(RemoteApi.class, true, QyThreeServiceRest.class.getPackage().getName());
  }

  /**
   * 获取默认基础链接地址
   */
  @Override
  protected String getDefaultBaseUrl() {
    return "https://qyapi.weixin.qq.com";
  }

  /**
   * 认证服务接口
   */
  private QyThreeServiceRest threeServiceRest;

  /**
   * 数据缓存
   */
  private Map<String, Object> cache;

  /**
   * 企业授权内容
   */
  private PermanentHandler permanentHandler;

  @PostConstruct
  @Override
  public void doPostConstruct() {
    cache = new ConcurrentHashMap<>(); // 缓存
    super.doPostConstruct();

    permanentHandler = createPermanentHandler();
    if (permanentHandler != null && permanentHandler instanceof CallbackActivatorHandler) {
      ((CallbackActivatorHandler) permanentHandler).setActivator(this);
    }
    try {
      // 注入企业授权内容
      permanentHandler = createPermanentHandler();
    } catch (Exception e) {
      Global.getLogger().info(e.getMessage());
    }
  }

  /**
   * 获取服务控制器
   * 
   * @return
   */
  public QyThreeServiceRest getThreeServiceRest() {
    if (threeServiceRest == null) {
      threeServiceRest = createTokenRest();
    }
    return threeServiceRest;
  }

  /**
   * 获取访问控制器
   * 
   * @return
   */
  protected QyThreeServiceRest createTokenRest() {
    return getApiImplement(QyThreeServiceRest.class);
  }

  /**
   * 数据获取适配器
   * 
   * SUITE_TICKET 第三方应用票据 SUITE_ACCESS_TOKEN 第三方应用凭证 PRE_AUTH_CODE 第三方预授权码 T_PERMANENT_CODE 永久授权码
   * T_ACCESS_TOKEN 企业访问令牌
   */
  @SuppressWarnings("unchecked")
  @Override
  public <T> T getAdapter(String key, Class<T> type) {
    if (type == QyThreeServiceRest.class) {
      return (T) getThreeServiceRest();
    }
    if (type == String.class) {
      if (key.equals(QyConsts.SUITE_TICKET)) {
        return (T) getSuiteTicket(); // 获取令牌票信息
      }
      if (key.equals(QyConsts.SUITE_ACCESS_TOKEN)) {
        return (T) getToken(QyConsts.SUITE_ACCESS_TOKEN);
      }
      if (key.equals(QyConsts.PRE_AUTH_CODE)) {
        return (T) getToken(QyConsts.PRE_AUTH_CODE);
      }
      if (key.startsWith(QyConsts.PERMANENT_CODE) && !key.equals(QyConsts.T_PERMANENT_CODE)) {
        String corpId = key.substring(QyConsts.PERMANENT_CODE.length());
        return (T) getPermanentCode(corpId);
      }
      if (key.startsWith(WxConsts.ACCESS_TOKEN) && !key.equals(QyConsts.T_ACCESS_TOKEN)) {
        return (T) getToken(key);
      }
      if (key.equals(WxConsts.COOKIE_OPEN_ID)) {
        return (T) getUserOpenId();
      }
    }
    if (type == Integer.class) {
      if (key.startsWith(WxConsts.APP_AGENT_ID) && !key.equals(QyConsts.T_APP_AGENT_ID)) {
        if (key.equals(WxConsts.APP_AGENT_ID)) {
          // 无法直接获取应用id
          // 第三方引用平台的agentid是根据不同公司做验证的，这里无法直接提供agentid
          return null;
        } else {
          String corpId = key.substring(WxConsts.APP_AGENT_ID.length());
          return (T) getAgentId(corpId);
        }
      }
    }
    return super.getAdapter(key, type);
  }

  /**
   * 获取用户openid信息
   * 
   * @return
   */
  public String getUserOpenId() {
    // 经常发生code值无法找到的问题，为了解决这个问题，code使用手动获取方式
    String code = Global.getCacheSafe(Global.getThreadCache(), WxConsts.OAUTH2_CODE, String.class);
    UserInfo3rdResult result = getThreeServiceRest().getUserInfo3rd(code);
    if (result.isErr()) {
      // 访问发生了异常
      logger.warning(result.printErr());
      return null;
    }

    if (result.getUserTicket() != null) {
      // 将UserTicket临时存储到当前缓存中
      Global.putCacheSafe(Global.getThreadCache(), WxConsts.OAUTH2_USER_TICKET, result.getUserTicket());
    }
    // 给出原始回调数据
    Global.putCacheSafe(Global.getThreadCache(), WxConsts.OAUTH2_USER_INFO, result);

    if (result.getUserId() != null) {
      return result.getCorpId() + ":" + result.getUserId();
    } else {
      return result.getOpenId();
    }
  }

  /**
   * 数据设定控制器
   */
  @Override
  public <T> void setAdapter(String key, Class<T> type, T value) {
    if (type == TicketSuite.class) {
      setSuiteTicket((TicketSuite) value);
      return;
    }
    if (type == PermanentCodeResult.class) {
      setPermanentCodeResult((PermanentCodeResult) value);
      return;
    }
    super.setAdapter(key, type, value);
  }

  /**
   * 配置企业授权内容
   */
  protected PermanentHandler createPermanentHandler() {
    return new DefaultQyPermanentHandler();
  }

  /**
   * 获取应用agentid
   * 
   * @param corpId
   * @return
   */
  public Integer getAgentId(String corpId) {
    if (permanentHandler != null) {
      return permanentHandler.getAgentId(corpId);
    }
    // 无法处理
    return null;
  }

  /**
   * 获取企业对应的永久授权码
   * 
   */
  public String getPermanentCode(String corpId) {
    if (permanentHandler != null) {
      return permanentHandler.getPermanentCode(corpId);
    }
    // 无法处理
    return null;
  }

  /**
   * 当完成企业授权后，会回调该方法，对该方法中的内容完成调用和赋值
   * 
   * 默认方法会在target文件中创建一个corp_permanent_code文件夹，然后一个文件的形式保存 其中文件的名称为企业ID
   * 
   * @param result
   */
  public void setPermanentCodeResult(PermanentCodeResult result) {
    String corpId = result.getAuthCorpInfo().getCorpId();
    if (permanentHandler != null) {
      permanentHandler.setPermanentCodeResult(result);
    }
    // 当前有access_token信息，保存
    String key = WxConsts.ACCESS_TOKEN + corpId;
    String preKey = getRequestAppKey();
    if (preKey != null) {
      key = preKey + key;
    }
    WxAccessToken accessToken = new WxAccessToken();
    accessToken.setAccessToken(result.getAccessToken());
    accessToken.setExpiresIn(result.getExpiresIn());
    cache.put(key, accessToken);
  }

  /**
   * 获取令牌表信息
   */
  public String getSuiteTicket() {
    String key = QyConsts.SUITE_TICKET;
    String appKey = getRequestAppKey();
    if (appKey != null) {
      key += appKey;
    }
    Object value = cache.get(key);
    return value != null ? value.toString() : null;
  }

  /**
   * 设定令牌票信息
   * 
   * @param value
   */
  public void setSuiteTicket(TicketSuite value) {
    String key = QyConsts.SUITE_TICKET;
    String appKey = getRequestAppKey();
    if (appKey != null) {
      key += appKey;
    }
    cache.put(key, value.getSuiteTicket());
  }

  /**
   * 执行token
   * 
   * tokenKey格式
   * 
   * 企业微信号的token有：
   * 
   * suite_access_token：第三方应用凭证 需要：suite_id，suite_secret， suite_ticket换取，其中suite_ticket来自企业微信服务器回调。
   * tokenKey=SUITE_ACCESS_TOKEN
   * 
   * pre_auth_code: 预授权码 需要：suite_access_token tokenKey=PRE_AUTH_CODE
   * 
   * access_token：企业访问令牌 需要：suite_access_token，auth_code，auth_corpid tokenKey: ACCESS_TOKEN{corp_id}
   * 注意：企业访问令牌会带有企业DI作为结尾
   * 
   * 
   */
  @Override
  protected WxTokenHandler getWxTokenHandler(String tokenKey) {
    if (tokenKey == null || tokenKey.isEmpty()) {
      // 无法处理
      throw new RuntimeException("无法获取令牌的类型，令牌类型为空，请确认后重试");
    }
    if (tokenKey.equals(QyConsts.SUITE_ACCESS_TOKEN)) {
      // 第三方应用凭证
      return () -> {
        SuiteTokenResult result = getThreeServiceRest().getSuiteToken();
        if (result.getErrcode() != null && result.getErrcode() != 0) {
          // 发生异常
          String error = String.format("获取第三方应用凭证发生异常[%s]：%s", tokenKey, result.getErrcode());
          throw new RuntimeException(error);
        }
        WxAccessToken token = new WxAccessToken();
        token.setAccessToken(result.getSuiteAccessToken());
        token.setExpiresIn(result.getExpiresIn());
        return token;
      };
    }

    if (tokenKey.equals(QyConsts.PRE_AUTH_CODE)) {
      // 预授权码
      return () -> {
        PreAuthCodeResult result = getThreeServiceRest().getPreAuthCode();
        if (result.getErrcode() != null && result.getErrcode() != 0) {
          // 发生异常
          throw new RuntimeException("获取预授权码发生异常：" + result.getErrcode());
        }
        WxAccessToken token = new WxAccessToken();
        token.setAccessToken(result.getPreAuthCode());
        token.setExpiresIn(result.getExpiresIn());
        return token;
      };
    }

    if (tokenKey.startsWith(WxConsts.ACCESS_TOKEN)) {
      return () -> {
        String key = tokenKey;
        String preKey = getRequestAppKey();
        if (preKey != null) {
          key = preKey + key;
        }
        WxAccessToken qyToken = (WxAccessToken) cache.remove(key);
        if (qyToken != null) {
          // 该令牌在获取企业永久授权码是生成的
          long now = System.currentTimeMillis() / 1000;
          if (qyToken.getCreateTime() + qyToken.getExpiresIn() > now) {
            // 该令牌还没有过期，可以使用
            return qyToken;
          }
        }

        String corpId = tokenKey.substring(WxConsts.ACCESS_TOKEN.length());
        CorpTokenResult result = getThreeServiceRest().getCorpToken(corpId);
        if (result.getErrcode() != null && result.getErrcode() != 0) {
          // 发生异常
          throw new RuntimeException("获取预授权码发生异常：" + result.getErrcode());
        }
        WxAccessToken token = new WxAccessToken();
        token.setAccessToken(result.getAccessToken());
        token.setExpiresIn(result.getExpiresIn());
        return token;
      };
    }
    throw new RuntimeException("无法获取令牌的类型，令牌类型无法识别：" + tokenKey);
  }
}
