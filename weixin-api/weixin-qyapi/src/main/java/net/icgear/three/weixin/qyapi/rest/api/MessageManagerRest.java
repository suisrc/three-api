package net.icgear.three.weixin.qyapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.qyapi.QyApiHelper;
import net.icgear.three.weixin.qyapi.QyConsts;
import net.icgear.three.weixin.qyapi.rest.dto.message.IMessage;
import net.icgear.three.weixin.qyapi.rest.dto.message.SendBody;
import net.icgear.three.weixin.qyapi.rest.dto.message.SendResult;

/**
 * 
 * 微信消息管理
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin/message")
public interface MessageManagerRest {
    
    /**
    * TODO 发送文本消息
    * 
    * 应用支持推送文本、图片、视频、文件、图文等类型。
    * 
    * 请求描述：https://work.weixin.qq.com/api/doc#10167
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * 
    * 说明：
    * 各个消息类型的具体POST格式参考以下文档。
    * 如果部分接收人无权限或不存在，发送仍然执行，但会返回无效的部分（即invaliduser或invalidparty或invalidtag），常见的原因是接收人不在应用的可见范围内。
    * 
    * 返回结果：
    * {
    *    "errcode" : 0,
    *    "errmsg" : "ok",
    *    "invaliduser" : "userid1|userid2",
    *    "invalidparty" : "partyid1|partyid2",
    *    "invalidtag":"tagid1|tagid2"
    *  }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              异常code
    * errmsg               异常信息
    * invaliduser          无效用户，不区分大小写，返回的列表都统一转为小写
    * invalidparty         无效部门
    * invalidtag           无效标签
    * 
    * -----------------------------------------TODO 文本消息
    * 请求包体：
    * {
    *    "touser" : "UserID1|UserID2|UserID3",
    *    "toparty" : "PartyID1|PartyID2",
    *    "totag" : "TagID1 | TagID2",
    *    "msgtype" : "text",
    *    "agentid" : 1,
    *    "text" : {
    *        "content" : "你的快递已到，请携带工卡前往邮件中心领取。\n出发前可查看<a href=\"http://work.weixin.qq.com\">邮件中心视频实况</a>，聪明避开排队。"
    *    },
    *    "safe":0
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向该企业应用的全部成员发送
    * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * msgtype              是.        消息类型，此时固定为：text
    * agentid              是.        企业应用的id，整型。可在应用的设置页面查看
    * content              是.        消息内容，最长不超过2048个字节
    * safe                 否.        表示是否是保密消息，0表示否，1表示是，默认0
    * 
    * ------------------------------------------------TODO 图片消息
    * 请求包体：
    * {
    *    "touser" : "UserID1|UserID2|UserID3",
    *    "toparty" : "PartyID1|PartyID2",
    *    "totag" : "TagID1 | TagID2",
    *    "msgtype" : "image",
    *    "agentid" : 1,
    *    "image" : {
    *         "media_id" : "MEDIA_ID"
    *    },
    *    "safe":0
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
    * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * msgtype              是.        消息类型，此时固定为：image
    * agentid              是.        企业应用的id，整型。可在应用的设置页面查看
    * media_id             是.        图片媒体文件id，可以调用上传临时素材接口获取
    * safe                 否.        表示是否是保密消息，0表示否，1表示是，默认0
    * 
    * ------------------------------------TODO 语音消息
    * 同图片消息
    * 
    * ------------------------------------TODO 视频消息
    * 请求包体：
    * {
    *    "touser" : "UserID1|UserID2|UserID3",
    *    "toparty" : "PartyID1|PartyID2",
    *    "totag" : "TagID1 | TagID2",
    *    "msgtype" : "video",
    *    "agentid" : 1,
    *    "video" : {
    *         "media_id" : "MEDIA_ID",
    *         "title" : "Title",
    *        "description" : "Description"
    *    },
    *    "safe":0
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
    * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * msgtype              是.        消息类型，此时固定为：video
    * agentid              是.        企业应用的id，整型。可在应用的设置页面查看
    * media_id             是.        视频媒体文件id，可以调用上传临时素材接口获取
    * title                否.        视频消息的标题，不超过128个字节，超过会自动截断
    * description          否.        视频消息的描述，不超过512个字节，超过会自动截断
    * safe                 否.        表示是否是保密消息，0表示否，1表示是，默认0
    * 
    * ------------------------------------TODO 文件消息
    * 同图片消息
    * 
    * ------------------------------------TODO 文本卡片消息
    * 请求包体：
    * {
    *    "touser" : "UserID1|UserID2|UserID3",
    *    "toparty" : "PartyID1 | PartyID2",
    *    "totag" : "TagID1 | TagID2",
    *    "msgtype" : "textcard",
    *    "agentid" : 1,
    *    "textcard" : {
    *             "title" : "领奖通知",
    *             "description" : "<div class=\"gray\">2016年9月26日</div> <div class=\"normal\">恭喜你抽中iPhone 7一台，领奖码：xxxx</div><div class=\"highlight\">请于2016年10月10日前联系行政同事领取</div>",
    *             "url" : "URL",
    *             "btntxt":"更多"
    *    }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
    * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * msgtype              是.        消息类型，此时固定为：textcard
    * agentid              是.        企业应用的id，整型。可在应用的设置页面查看
    * title                是.        标题，不超过128个字节，超过会自动截断
    * description          是.        描述，不超过512个字节，超过会自动截断
    * url                  是.        点击后跳转的链接。
    * btntxt               否.        按钮文字。
    * 
    * 特殊说明：
    * 卡片消息的展现形式非常灵活，支持使用br标签或者空格来进行换行处理，也支持使用div标签来使用不同的字体颜色，
    * 目前内置了3种文字颜色：灰色(gray)、高亮(highlight)、默认黑色(normal)，将其作为div标签的class属性即可，具体用法请参考上面的示例。
    * 
    * -----------------------------TODO 图文消息
    * 
    * 请求包体：
    * {
    *    "touser" : "UserID1|UserID2|UserID3",
    *    "toparty" : "PartyID1 | PartyID2",
    *    "totag": "TagID1 | TagID2",
    *    "msgtype" : "mpnews",
    *    "agentid" : 1,
    *    "mpnews" : {
    *        "articles":[
    *            {
    *                "title": "Title", 
    *                "thumb_media_id": "MEDIA_ID",
    *                "author": "Author",
    *                "content_source_url": "URL",
    *                "content": "Content",
    *                "digest": "Digest description"
    *             }
    *        ]
    *    },
    *    "safe":0
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
    * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
    * msgtype              是.        消息类型，此时固定为：mpnews
    * agentid              是.        企业应用的id，整型。可在应用的设置页面查看
    * articles             是.        图文消息，一个图文消息支持1到8条图文
    * title                是.        标题，不超过128个字节，超过会自动截断
    * thumb_media_id       是.        图文消息缩略图的media_id,
    * author               否.        图文消息的作者，不超过64个字节
    * content_source_url   否.        图文消息点击“阅读原文”之后的页面链接
    * content              是.        图文消息的内容，支持html标签，不超过666
    * digest               否.        图文消息的描述，不超过512个字节，超过会自动截断
    * safe                 否.        表示是否是保密消息，0表示可对外分享，1表示不能分享且内容显示水印，2表示仅限在企业内分享，默认为0；注意仅mpnews类型的消息支持safe值为2，其他消息类型不支持
    * 
    * ----------------------------------------小程序通知消息
    * 小程序通知消息只允许小程序应用发送，消息会通过【小程序通知】发送给用户。
    * 小程序应用仅支持发送小程序通知消息，暂不支持文本、图片、语音、视频、图文等其他类型的消息。
    * 不支持@all全员发送
    * 
    * 请求包体：
    * {
    *    "touser" : "zhangsan|lisi",
    *    "toparty": "1|2",
    *    "totag": "1|2",
    *    "msgtype" : "miniprogram_notice",
    *    "miniprogram_notice" : {
    *         "appid": "wx123123123123123",
    *         "page": "pages/index?userid=zhangsan&orderid=123123123",
    *         "title": "会议室预订成功通知",
    *         "description": "4月27日 16:16",
    *         "emphasis_first_item": true,
    *         "content_item": [
    *             {
    *                 "key": "会议室",
    *                 "value": "402"
    *             },
    *             {
    *                 "key": "会议地点",
    *                 "value": "广州TIT-402会议室"
    *             },
    *             {
    *                 "key": "会议时间",
    *                 "value": "2018年8月1日 09:00-09:30"
    *             },
    *             {
    *                 "key": "参与人员",
    *                 "value": "周剑轩"
    *             }
    *         ]
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）
    * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。
    * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。
    * msgtype              是.        消息类型，此时固定为：miniprogram_notice
    * appid                是.        小程序appid，必须是与当前小程序应用关联的小程序
    * page                 否.        点击消息卡片后的小程序页面，仅限本小程序内的页面。该字段不填则消息点击后不跳转。
    * title                是.        消息标题，长度限制4-12个汉字
    * description          否.        消息描述，长度限制4-12个汉字
    * emphasis_first_item  否.        是否放大第一个content_item
    * content_item         否.        消息内容键值对，最多允许10个item
    * key                  是.        长度10个汉字以内
    * value                是.        长度30个汉字以内
    * 
    */
    @POST
    @Path("send")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SendResult send(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @NotNull("发送的消息内容") SendBody<?> body);
    default SendResult sendMessage(String corpid, SendBody<?> body) {
        return QyApiHelper.call(corpid, () -> {
            return send((String)null, body);
        });
    }
    /**
     * 文本消息:TextMessage
     * 图片消息:Imagemessage
     * 语音消息:VoiceMessage
     * 视频消息:VideoMessage
     * 文件消息:FileMessage
     * 文本卡片消息:TextcardMessage
     */
    default SendResult sendMessage(String corpid, String touser, String toparty, String totag, IMessage text) {
        return QyApiHelper.call(corpid, () -> {
            SendBody<IMessage> body = new SendBody<>();
            body.setTouser(touser);
            body.setToparty(toparty);
            body.setTotag(totag);
            body.setData(text);
            return send((String)null, body);
        });
    }
}
