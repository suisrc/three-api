package net.icgear.three.weixin.qyapi.rest.dto.batch;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 邀请成员
 * 
 * @author Y13
 *
 */
@ApiModel("InviteBody")
public class InviteBody {

    @ApiModelProperty("成员ID列表,")
    @JsonProperty("user")
    private String[] user;

    @ApiModelProperty("部门ID列表，最多支持100个。")
    @JsonProperty("party")
    private String[] party;

    @ApiModelProperty("标签ID列表，最多支持100个。")
    @JsonProperty("tag")
    private String[] tag;

    /**
     * 成员ID列表,
     */
    public String[] getUser() {
        return user;
    }

    /**
     * 成员ID列表,
     */
    public void setUser(String[] user) {
        this.user = user;
    }

    /**
     * 部门ID列表，最多支持100个。
     */
    public String[] getParty() {
        return party;
    }

    /**
     * 部门ID列表，最多支持100个。
     */
    public void setParty(String[] party) {
        this.party = party;
    }

    /**
     * 标签ID列表，最多支持100个。
     */
    public String[] getTag() {
        return tag;
    }

    /**
     * 标签ID列表，最多支持100个。
     */
    public void setTag(String[] tag) {
        this.tag = tag;
    }
}
