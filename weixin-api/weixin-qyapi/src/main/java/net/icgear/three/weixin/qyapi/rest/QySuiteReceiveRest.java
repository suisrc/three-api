package net.icgear.three.weixin.qyapi.rest;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.ApiOperation;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxEncryptSignature;
import net.icgear.three.weixin.core.rest.WxWhitelistRest;

/**
 * 微信回调接口
 * 
 * @author Y13
 *
 */
public interface QySuiteReceiveRest extends WxWhitelistRest {

    @ApiOperation(nickname="sr10010", value="企业微信指令回调请求绑定")
    @GET
    @Path(WxConsts.REST_PATH_WX)
    @Produces(MediaType.TEXT_PLAIN + WxConsts.MediaType_UTF_8)
    String doSuiteGet(@BeanParam WxEncryptSignature sign);

    @ApiOperation(nickname="sr10011", value="企业微信指令回调请求绑定")
    @POST
    @Path(WxConsts.REST_PATH_WX)
    @Produces(MediaType.APPLICATION_XML + WxConsts.MediaType_UTF_8)
    Response doSuitePost(@BeanParam WxEncryptSignature sign, String data);
    
}
