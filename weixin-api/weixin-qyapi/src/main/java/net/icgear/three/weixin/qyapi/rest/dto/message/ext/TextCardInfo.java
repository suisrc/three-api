package net.icgear.three.weixin.qyapi.rest.dto.message.ext;

import java.util.ArrayList;
import java.util.List;

/**
 * 靠信息内容
 * @author Y13
 *
 */
public class TextCardInfo {

    public enum Color {
        gray, highlight, normal
    }

    /**
     * 卡消息内容
     * @author Y13
     *
     */
    public static class TextCardItem {
        
        /**
         * 消息左边
         */
        private String key;
        
        /**
         * 消息右边
         */
        private String value;
        
        /**
         * 消息颜色
         */
        private Color color;
        
        public TextCardItem() {}
        
        public TextCardItem(String key, String value, Color color) {
            this.key = key;
            this.value = value;
            this.color = color;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Color getColor() {
            return color;
        }

        public void setColor(Color color) {
            this.color = color;
        }

    }
    
    private List<TextCardItem> items = new ArrayList<>();
    
    /**
     * 获取描述的内容
     * @return
     */
    public String getDescription() {
        StringBuilder sb = new StringBuilder();
        for (TextCardItem item : items) {
            sb.append("<div");
            if (item.getColor() != null) {
                sb.append(" class=\"" + item.getColor().name() + "\"");
            }
            sb.append('>');
            if (item.getKey() != null) {
                sb.append(item.getKey() + ":");
            }
            sb.append(item.getValue());
            sb.append("</div>");
        }
        String description = sb.toString();
        return description;
    }
    
    /**
     * 增加一行
     * @param key
     * @param value
     * @param color
     * @return
     */
    public TextCardInfo add(String key, String value, Color color) {
        items.add(new TextCardItem(key, value, color));
        return this;
    }
    
    /**
     * 
     * @param item
     * @return
     */
    public TextCardInfo add(TextCardItem item) {
        items.add(item);
        return this;
    }
}
