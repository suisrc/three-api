package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 第三方使用user_ticket获取成员详情
 * @author Y13
 *
 */
@ApiModel("UserDetailBody")
public class UserDetailBody {
    
    @NotNull("成员票据为空")
    @ApiModelProperty("成员票据")
    @JsonProperty("user_ticket")
    private String userTicket;

    public String getUserTicket() {
        return userTicket;
    }

    public void setUserTicket(String userTicket) {
        this.userTicket = userTicket;
    }
}
