package net.icgear.three.weixin.qyapi.rest.dto.external;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 本文属性
 * @author Y13
 *
 */
@ApiModel("ExternalProfile.TextExternalAttr")
public class TextExternalAttr extends ExternalAttr {
    
    @ApiModel("ExternalProfile.TextExternalAttr.Text")
    @JsonInclude(Include.NON_NULL) 
    public static class Text {
        
        @ApiModelProperty("文本属性内容,长度限制12个UTF8字符")
        @JsonProperty("value")
        private String value;
        
        /**
         * 文本属性内容,长度限制12个UTF8字符
         * @return
         */
        public String getValue() {
            return value;
        }
        
        /**
         * 文本属性内容,长度限制12个UTF8字符
         * @param value
         */
        public void setValue(String value) {
            this.value = value;
        }
    }

    @ApiModelProperty("文本类型的属性")
    @JsonProperty("text")
    private Text text;

    /**
     * 文本类型的属性
     * @return
     */
    public Text getText() {
        return text;
    }

    /**
     * 文本类型的属性
     * @param text
     */
    public void setText(Text text) {
        this.text = text;
    }
    
}
