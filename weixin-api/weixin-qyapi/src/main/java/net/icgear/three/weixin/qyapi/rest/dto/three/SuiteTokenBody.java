package net.icgear.three.weixin.qyapi.rest.dto.three;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.Value;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.qyapi.QyConsts;

/**
 * 获取第三方应用凭证参数
 * @author Y13
 *
 */
@ApiModel("SuiteTokenBody")
public class SuiteTokenBody {

    @NotNull("应用id为空")
    @ApiModelProperty("以ww或wx开头应用id（对应于旧的以tj开头的套件id）")
    @Value(WxConsts.APP_SUITE_ID)
    @JsonProperty("suite_id")
    private String suiteId;

    @NotNull("应用secret为空")
    @ApiModelProperty("应用secret")
    @Value(WxConsts.APP_SECRET)
    @JsonProperty("suite_secret")
    private String suiteSecret;

    @NotNull("应用ticket为空")
    @ApiModelProperty("企业微信后台推送的ticket")
    @Value(QyConsts.SUITE_TICKET)
    @JsonProperty("suite_ticket")
    private String suiteTicket;

    public String getSuiteId() {
        return suiteId;
    }

    public void setSuiteId(String suiteId) {
        this.suiteId = suiteId;
    }

    public String getSuiteSecret() {
        return suiteSecret;
    }

    public void setSuiteSecret(String suiteSecret) {
        this.suiteSecret = suiteSecret;
    }

    public String getSuiteTicket() {
        return suiteTicket;
    }

    public void setSuiteTicket(String suiteTicket) {
        this.suiteTicket = suiteTicket;
    }

}
