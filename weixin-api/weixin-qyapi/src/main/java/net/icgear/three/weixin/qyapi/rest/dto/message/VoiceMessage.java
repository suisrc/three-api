package net.icgear.three.weixin.qyapi.rest.dto.message;

import io.swagger.annotations.ApiModel;

/**
 * 语音消息
 * 
 * 同图片消息
 * 
 * @author Y13
 *
 */
@ApiModel("SendBody.VoiceMessage")
public class VoiceMessage extends ImageMessage {

    @Override
    public String type() {
        return "voice";
    }
}
