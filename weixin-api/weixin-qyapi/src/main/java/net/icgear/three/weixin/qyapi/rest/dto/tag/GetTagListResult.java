package net.icgear.three.weixin.qyapi.rest.dto.tag;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 获取标签列表
 * 
 * @author Y13
 *
 */
@ApiModel("GetTagListResult")
public class GetTagListResult extends WxErrCode {
    private static final long serialVersionUID = 5626095800835500064L;

    @ApiModel("GetTagListResult.Taglist")
    public static class Taglist {

        @ApiModelProperty("标签id")
        @JsonProperty("tagid")
        private Integer tagid;

        @ApiModelProperty("标签名")
        @JsonProperty("tagname")
        private String tagname;

        /**
         * 标签id
         */
        public Integer getTagid() {
            return tagid;
        }

        /**
         * 标签id
         */
        public void setTagid(Integer tagid) {
            this.tagid = tagid;
        }

        /**
         * 标签名
         */
        public String getTagname() {
            return tagname;
        }

        /**
         * 标签名
         */
        public void setTagname(String tagname) {
            this.tagname = tagname;
        }
    }

    @ApiModelProperty("标签列表")
    @JsonProperty("taglist")
    private Taglist[] taglist;

    /**
     * 标签列表
     */
    public Taglist[] getTaglist() {
        return taglist;
    }

    /**
     * 标签列表
     */
    public void setTaglist(Taglist[] taglist) {
        this.taglist = taglist;
    }
}
