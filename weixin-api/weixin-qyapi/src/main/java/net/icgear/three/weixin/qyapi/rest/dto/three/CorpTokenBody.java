package net.icgear.three.weixin.qyapi.rest.dto.three;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.Value;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.qyapi.QyConsts;

/**
 * 获取企业访问令牌
 * @author Y13
 *
 */
@ApiModel("CorpTokenBody")
public class CorpTokenBody {

    @NotNull("应用的授权方corpid为空")
    @ApiModelProperty("授权方corpid")
    @JsonProperty("auth_corpid")
    @Value(QyConsts.APP_CORP_ID_VALUE)
    private String authCorpid;

    @NotNull("应用的永久授权码为空")
    @ApiModelProperty("永久授权码，通过get_permanent_code获取")
    @JsonProperty("permanent_code")
    @Value(QyConsts.T_PERMANENT_CODE)
    private String permanentCode;

    public String getAuthCorpid() {
        return authCorpid;
    }

    public void setAuthCorpid(String authCorpid) {
        this.authCorpid = authCorpid;
    }

    public String getPermanentCode() {
        return permanentCode;
    }

    public void setPermanentCode(String permanentCode) {
        this.permanentCode = permanentCode;
    }
}
