package net.icgear.three.weixin.qyapi;

import net.icgear.three.weixin.core.WxConsts;

/**
 * 常量
 * @author Y13
 *
 */
public interface QyConsts {
   
    /**
     * 调用api时候，企业关键字
     */
    final String APP_CORP_ID = "APP_CORP_ID";
    
    /**
     * 直接获取企业ID的值
     * 默认需要充线程缓存中获取
     */
    final String APP_CORP_ID_VALUE = "T:" + APP_CORP_ID;
    
    /**
     * 企业线程上的信息
     */
    final String T_APP_CORP_ID = "{" + APP_CORP_ID_VALUE + "}";
    
    //--------------------------------授权key信息
    
    /**
     * 第三方应用票据
     * 
     * 需要处理
     */
    final String SUITE_TICKET = "SUITE_TICKET";
    
    /**
     * 第三方应用凭证
     * 
     * 需要处理
     */
    final String SUITE_ACCESS_TOKEN = "SUITE_ACCESS_TOKEN";
    
    /**
     * 第三方预授权码
     * 
     * 需要处理
     */
    final String PRE_AUTH_CODE = "PRE_AUTH_CODE";
    
    /**
     * 永久授权码
     * 
     * 不需要处理
     */
    final String PERMANENT_CODE = "PERMANENT_CODE";
    
    /**
     * 企业永久授权码
     * 
     * 需要处理
     */
    final String T_PERMANENT_CODE = PERMANENT_CODE + T_APP_CORP_ID;
    
    /**
     * 需要通过线程变量重建
     */
    final String T_APP_AGENT_ID = WxConsts.APP_AGENT_ID + T_APP_CORP_ID;
    
    /**
     * 企业访问令牌
     * 
     * 需要处理
     */
    final String T_ACCESS_TOKEN = WxConsts.ACCESS_TOKEN + T_APP_CORP_ID;
    //--------------------------------END
}
