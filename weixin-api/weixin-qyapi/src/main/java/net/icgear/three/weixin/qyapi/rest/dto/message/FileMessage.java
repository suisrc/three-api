package net.icgear.three.weixin.qyapi.rest.dto.message;

import io.swagger.annotations.ApiModel;

/**
 * 文件消息
 * 
 * 同图片消息
 * 
 * @author Y13
 *
 */
@ApiModel("SendBody.FileMessage")
public class FileMessage extends ImageMessage {

    @Override
    public String type() {
        return "file";
    }
}
