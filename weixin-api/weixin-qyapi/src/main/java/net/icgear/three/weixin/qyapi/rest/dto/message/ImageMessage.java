package net.icgear.three.weixin.qyapi.rest.dto.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 图片消息
 * 
 * 请求包体：
 * {
 *    "touser" : "UserID1|UserID2|UserID3",
 *    "toparty" : "PartyID1|PartyID2",
 *    "totag" : "TagID1 | TagID2",
 *    "msgtype" : "image",
 *    "agentid" : 1,
 *    "image" : {
 *         "media_id" : "MEDIA_ID"
 *    },
 *    "safe":0
 * }
 * 
 * 参数说明：
 * 参数.                必须.       说明
 * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
 * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
 * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
 * msgtype              是.        消息类型，此时固定为：image
 * agentid              是.        企业应用的id，整型。可在应用的设置页面查看
 * media_id             是.        图片媒体文件id，可以调用上传临时素材接口获取
 * safe                 否.        表示是否是保密消息，0表示否，1表示是，默认0
 * 
 * @author Y13
 *
 */
@ApiModel("SendBody.ImageMessage")
public class ImageMessage implements IMessage {

    @Override
    public String type() {
        return "image";
    }
    
    @ApiModelProperty("图片媒体文件id，可以调用上传临时素材接口获取")
    @NotNull("media_id属性为空")
    @JsonProperty("media_id")
    private String mediaId;

    /**
     * 图片媒体文件id，可以调用上传临时素材接口获取
     */
    public String getMediaId() {
        return mediaId;
    }

    /**
     * 图片媒体文件id，可以调用上传临时素材接口获取
     */
    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
