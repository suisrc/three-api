package net.icgear.three.weixin.qyapi.handler;

import net.icgear.three.weixin.qyapi.rest.dto.three.PermanentCodeResult;

/**
 * 企业授权信息处理
 * @author Y13
 *
 */
public interface PermanentHandler {
    
    /**
     * 存储企业授权码
     * @param result
     */
    void setPermanentCodeResult(PermanentCodeResult result);

    /**
     * 获取企业授权码
     * @param corpId
     * @return
     */
    String getPermanentCode(String corpId);

    /**
     * 获取应用agentid
     * @param corpId
     * @return
     */
    Integer getAgentId(String corpId);

    /**
     * 通过适配器获取属性，用于扩展
     * @param corpId
     * @param key
     * @param type
     * @return
     */
    String getAdapter(String corpId, String key, Class<?> type);
}
