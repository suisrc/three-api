package net.icgear.three.weixin.qyapi.rest.dto.three;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 获取企业访问令牌
 * 
 * @author Y13
 *
 */
@ApiModel("CorpTokenResult")
public class CorpTokenResult extends WxErrCode {
    private static final long serialVersionUID = -7556063866777012821L;

    /**
     * 获取到的凭证
     */
    @ApiModelProperty("授权方（企业）access_token,最长为512字节")
    @JsonProperty("access_token")
    private String accessToken;

    /**
     * 凭证有效时间，单位：秒
     */
    @ApiModelProperty("授权方（企业）access_token超时时间")
    @JsonProperty("expires_in")
    private long expiresIn = -1;

    public String getAccessToken() {
        return this.accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public long getExpiresIn() {
        return this.expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }
    
}
