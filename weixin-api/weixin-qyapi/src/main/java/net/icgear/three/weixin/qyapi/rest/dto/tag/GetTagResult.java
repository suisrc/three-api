package net.icgear.three.weixin.qyapi.rest.dto.tag;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 获取标签成员
 * 
 * @author Y13
 *
 */
@ApiModel("GetTagResult")
public class GetTagResult extends WxErrCode {
    private static final long serialVersionUID = -4525495934874177465L;

    @ApiModel("GetTagResult.Userlist")
    public static class Userlist {

        @ApiModelProperty("成员帐号")
        @JsonProperty("userid")
        private String userid;

        @ApiModelProperty("成员名")
        @JsonProperty("name")
        private String name;

        /**
         * 成员帐号
         */
        public String getUserid() {
            return userid;
        }

        /**
         * 成员帐号
         */
        public void setUserid(String userid) {
            this.userid = userid;
        }

        /**
         * 成员名
         */
        public String getName() {
            return name;
        }

        /**
         * 成员名
         */
        public void setName(String name) {
            this.name = name;
        }
    }


    @ApiModelProperty("标签名")
    @JsonProperty("tagname")
    private String tagname;

    @ApiModelProperty("标签中包含的成员列表")
    @JsonProperty("userlist")
    private Userlist[] userlist;

    @ApiModelProperty("标签中包含的部门id列表")
    @JsonProperty("partylist")
    private Integer[] partylist;

    /**
     * 标签名
     */
    public String getTagname() {
        return tagname;
    }

    /**
     * 标签名
     */
    public void setTagname(String tagname) {
        this.tagname = tagname;
    }

    /**
     * 标签中包含的成员列表
     */
    public Userlist[] getUserlist() {
        return userlist;
    }

    /**
     * 标签中包含的成员列表
     */
    public void setUserlist(Userlist[] userlist) {
        this.userlist = userlist;
    }

    /**
     * 标签中包含的部门id列表
     */
    public Integer[] getPartylist() {
        return partylist;
    }

    /**
     * 标签中包含的部门id列表
     */
    public void setPartylist(Integer[] partylist) {
        this.partylist = partylist;
    }
}
