package net.icgear.three.weixin.qyapi.rest.dto.three;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.Value;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.qyapi.QyConsts;

/**
 * 设置授权配置参数
 * 
 * @author Y13
 *
 */
@ApiModel("SessionInfoBody")
public class SessionInfoBody {
    
    @ApiModel("SessionInfoBody.SessionInfo")
    public static class SessionInfo {
        
        @ApiModelProperty("允许进行授权的应用id，如1、2、3， 不填或者填空数组都表示允许授权套件内所有应用（仅旧的多应用套件可传此参数，新开发者可忽略）")
        @JsonProperty("appid")
        private int[] appid;
        
        @ApiModelProperty("授权类型：0 正式授权， 1 测试授权。 默认值为0。注意，请确保应用在正式发布后的授权类型为“正式授权”")
        @JsonProperty("auth_type")
        private int authType;

        public int[] getAppid() {
            return appid;
        }

        public void setAppid(int[] appid) {
            this.appid = appid;
        }

        public int getAuthType() {
            return authType;
        }

        public void setAuthType(int authType) {
            this.authType = authType;
        }
    }
    
    @NotNull("应用的预授权码为空")
    @Value(QyConsts.PRE_AUTH_CODE)
    @ApiModelProperty("预授权码")
    @JsonProperty("pre_auth_code")
    private String preAuthCode;
    
    @ApiModelProperty("本次授权过程中需要用到的会话信息")
    @JsonProperty("session_info")
    private SessionInfo sessionInfo;

    public String getPreAuthCode() {
        return preAuthCode;
    }

    public void setPreAuthCode(String preAuthCode) {
        this.preAuthCode = preAuthCode;
    }

    public SessionInfo getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(SessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

}
