package net.icgear.three.weixin.qyapi.rest.dto.tag;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 增加标签成员
 * 
 * @author Y13
 *
 */
@ApiModel("TagUsersBody")
public class TagUsersBody {

    @ApiModelProperty("标签ID")
    @NotNull("tagid属性为空")
    @JsonProperty("tagid")
    private Integer tagid;

    @ApiModelProperty("企业成员ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过1000")
    @JsonProperty("userlist")
    private String[] userlist;

    @ApiModelProperty("企业部门ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过100")
    @JsonProperty("partylist")
    private Integer[] partylist;

    /**
     * 标签ID
     */
    public Integer getTagid() {
        return tagid;
    }

    /**
     * 标签ID
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }

    /**
     * 企业成员ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过1000
     */
    public String[] getUserlist() {
        return userlist;
    }

    /**
     * 企业成员ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过1000
     */
    public void setUserlist(String[] userlist) {
        this.userlist = userlist;
    }

    /**
     * 企业部门ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过100
     */
    public Integer[] getPartylist() {
        return partylist;
    }

    /**
     * 企业部门ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过100
     */
    public void setPartylist(Integer[] partylist) {
        this.partylist = partylist;
    }
}
