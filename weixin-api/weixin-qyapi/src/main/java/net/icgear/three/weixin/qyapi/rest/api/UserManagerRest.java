package net.icgear.three.weixin.qyapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsConsts;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.qyapi.QyConsts;
import net.icgear.three.weixin.qyapi.rest.dto.user.BatchdeleteUserBody;
import net.icgear.three.weixin.qyapi.rest.dto.user.Convert2OpenidBody;
import net.icgear.three.weixin.qyapi.rest.dto.user.Convert2OpenidResult;
import net.icgear.three.weixin.qyapi.rest.dto.user.Convert2UseridBody;
import net.icgear.three.weixin.qyapi.rest.dto.user.Convert2UseridResult;
import net.icgear.three.weixin.qyapi.rest.dto.user.CreateUserBody;
import net.icgear.three.weixin.qyapi.rest.dto.user.SimplelistUserResult;
import net.icgear.three.weixin.qyapi.rest.dto.user.UpdateBody;
import net.icgear.three.weixin.qyapi.rest.dto.user.UserDetailBody;
import net.icgear.three.weixin.qyapi.rest.dto.user.UserDetailResult;
import net.icgear.three.weixin.qyapi.rest.dto.user.UserInfoResult;
import net.icgear.three.weixin.qyapi.rest.dto.user.UserListResult;
import net.icgear.three.weixin.qyapi.rest.dto.user.UserResult;

/**
 * 成员管理
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin/user")
public interface UserManagerRest {
    final String NAMED = JaxrsConsts.separator + "net.icgear.three.weixin.qyapi.rest.api.UserManagerRest";
    
    /**
    * 创建成员
    * -:createUser
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "userid": "zhangsan",
    *     "name": "张三",
    *     "english_name": "jackzhang",
    *     "mobile": "15913215421",
    *     "department": [1, 2],
    *     "order":[10,40],
    *     "position": "产品经理",
    *     "gender": "1",
    *     "email": "zhangsan@gzdev.com",
    *     "isleader": 1,
    *     "enable":1,
    *     "avatar_mediaid": "2-G6nrLmr5EC3MNb_-zL1dDdzkd0p7cNliYu9V5w7o8K0",
    *     "telephone": "020-123456",
    *     "extattr": {"attrs":[{"name":"爱好","value":"旅游"},{"name":"卡号","value":"1234567234"}]},
    *     "to_invite": false,
    *     "external_profile": {
    *         "external_attr": [
    *             {
    *                 "type": 0,
    *                 "name": "文本名称",
    *                 "text": {
    *                     "value": "文本"
    *                 }
    *             },
    *             {
    *                 "type": 1,
    *                 "name": "网页名称",
    *                 "web": {
    *                     "url": "http://www.test.com",
    *                     "title": "标题"
    *                 }
    *             },
    *             {
    *                 "type": 2,
    *                 "name": "测试app",
    *                 "miniprogram": {
    *                     "appid": "wx8bd80126147df384",
    *                     "pagepath": "/index",
    *                     "title": "my miniprogram"
    *                 }
    *             }
    *         ]
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证。获取方法查看“获取access_token”
    * userid               是.        成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
    * name                 是.        成员名称。长度为1~64个字符
    * english_name         否.        英文名。长度为1-64个字节，由字母、数字、点(.)、减号(-)、空格或下划线(_)组成
    * mobile               否.        手机号码。企业内必须唯一，mobile/email二者不能同时为空
    * department           是.        成员所属部门id列表,不超过20个
    * order                否.        部门内的排序值，默认为0，成员次序以创建时间从小到大排列。数量必须和department一致，数值越大排序越前面。有效的值范围是[0,
    * position             否.        职位信息。长度为0~128个字符
    * gender               否.        性别。1表示男性，2表示女性
    * email                否.        邮箱。长度不超过64个字节，且为有效的email格式。企业内必须唯一，mobile/email二者不能同时为空
    * telephone            否.        座机。由1-32位的纯数字或’-‘号组成。
    * isleader             否.        上级字段，标识是否为上级。在审批等应用里可以用来标识上级审批人
    * avatar_mediaid       否.        成员头像的mediaid，通过素材管理接口上传图片获得的mediaid
    * enable               否.        启用/禁用成员。1表示启用成员，0表示禁用成员
    * extattr              否.        自定义字段。自定义字段需要先在WEB管理端添加，见扩展属性添加方法，否则忽略未知属性的赋值。自定义字段长度为0~32个字符，超过将被截断
    * to_invite            否.        是否邀请该成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
    * external_profile     否.        成员对外属性，字段详情见对外属性
    * 说明：
    * 应用须拥有指定部门的管理权限。
    * 注意，每个部门下的部门、成员总数不能超过3万个。建议保证创建department对应的部门和创建成员是串行化处理。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "created"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode createUser(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @NotNull("请求包体为空")CreateUserBody body);
    default WxErrCode createUser(CreateUserBody body) {
        return createUser((String)null, body);
    }
    
    /**
    * 读取成员
    * 在通讯录同步助手中此接口可以读取企业通讯录的所有成员信息，而自建应用可以读取该应用设置的可见范围内的成员信息。
    * -:getUser
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid=USERID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * userid               是.        成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
    * 说明：
    * 应用须拥有指定部门的管理权限。
    * 
    * 返回结果：
    * {
    *     "errcode": 0,
    *     "errmsg": "ok",
    *     "userid": "zhangsan",
    *     "name": "李四",
    *     "department": [1, 2],
    *     "order": [1, 2],
    *     "position": "后台工程师",
    *     "mobile": "15913215421",
    *     "gender": "1",
    *     "email": "zhangsan@gzdev.com",
    *     "isleader": 1,
    *     "avatar": "http://wx.qlogo.cn/mmopen/ajNVdqHZLLA3WJ6DSZUfiakYe37PKnQhBIeOQBO4czqrnZDS79FH5Wm5m4X69TBicnHFlhiafvDwklOpZeXYQQ2icg/0",
    *     "telephone": "020-123456",
    *     "enable": 1,
    *     "english_name": "jackzhang",
    *     "extattr": {
    *         "attrs": [{
    *             "name": "爱好",
    *             "value": "旅游"
    *         }, {
    *             "name": "卡号",
    *             "value": "1234567234"
    *         }]
    *     },
    *     "status": 1,
    *     "qr_code": "https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=xxx",
    *     "external_profile": {
    *         "external_attr": [{
    *                 "type": 0,
    *                 "name": "文本名称",
    *                 "text": {
    *                     "value": "文本"
    *                 }
    *             },
    *             {
    *                 "type": 1,
    *                 "name": "网页名称",
    *                 "web": {
    *                     "url": "http://www.test.com",
    *                     "title": "标题"
    *                 }
    *             },
    *             {
    *                 "type": 2,
    *                 "name": "测试app",
    *                 "miniprogram": {
    *                     "appid": "wx8bd80126147df384",
    *                     "pagepath": "/index",
    *                     "title": "my miniprogram"
    *                 }
    *             }
    *         ]
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * userid               成员UserID。对应管理端的帐号
    * name                 成员名称
    * mobile               手机号码，第三方仅通讯录应用可获取
    * department           成员所属部门id列表
    * order                部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0,
    * position             职位信息；第三方仅通讯录应用可获取
    * gender               性别。0表示未定义，1表示男性，2表示女性
    * email                邮箱，第三方仅通讯录套件可获取
    * isleader             上级字段，标识是否为上级；第三方仅通讯录应用可获取
    * avatar               头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。第三方仅通讯录应用可获取
    * telephone            座机。第三方仅通讯录应用可获取
    * enable               成员启用状态。1表示启用的成员，0表示被禁用。注意，服务商调用接口不会返回此字段
    * english_name         英文名；第三方仅通讯录应用可获取
    * extattr              扩展属性，第三方仅通讯录应用可获取
    * status               激活状态:
    * qr_code              员工个人二维码，扫描可添加为外部联系人；第三方仅通讯录应用可获取
    * external_profile     成员对外属性，字段详情见对外属性；第三方仅通讯录应用可获取
    * 
    */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserResult getUser(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @QueryParam("userid")@NotNull("用户为空") String userid);
    default UserResult getUser(String userid) {
        return getUser((String)null, userid);
    }
    
    /**
    * 更新成员
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "userid": "zhangsan",
    *     "name": "李四",
    *     "department": [1],
    *     "order": [10],
    *     "position": "后台工程师",
    *     "mobile": "15913215421",
    *     "gender": "1",
    *     "email": "zhangsan@gzdev.com",
    *     "isleader": 0,
    *     "enable": 1,
    *     "avatar_mediaid": "2-G6nrLmr5EC3MNb_-zL1dDdzkd0p7cNliYu9V5w7o8K0",
    *     "telephone": "020-123456",
    *     "english_name": "jackzhang",
    *     "extattr": {"attrs":[{"name":"爱好","value":"旅游"},{"name":"卡号","value":"1234567234"}]},
    *     "external_profile": {
    *         "external_attr": [
    *             {
    *                 "type": 0,
    *                 "name": "文本名称",
    *                 "text": {
    *                     "value": "文本"
    *                 }
    *             },
    *             {
    *                 "type": 1,
    *                 "name": "网页名称",
    *                 "web": {
    *                     "url": "http://www.test.com",
    *                     "title": "标题"
    *                 }
    *             },
    *             {
    *                 "type": 2,
    *                 "name": "测试app",
    *                 "miniprogram": {
    *                     "appid": "wx8bd80126147df384",
    *                     "pagepath": "/index",
    *                     "title": "my miniprogram"
    *                 }
    *             }
    *         ]
    *     }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * userid               是.        成员UserID。对应管理端的帐号，企业内必须唯一。不区分大小写，长度为1~64个字节
    * name                 否.        成员名称。长度为1~64个字符
    * english_name         否.        英文名。长度为1-64个字节，由字母、数字、点(.)、减号(-)、空格或下划线(_)组成
    * mobile               否.        手机号码。企业内必须唯一。若成员已激活企业微信，则需成员自行修改（此情况下该参数被忽略，但不会报错）
    * department           否.        成员所属部门id列表，不超过20个
    * order                否.        部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。有效的值范围是[0,
    * position             否.        职位信息。长度为0~128个字符
    * gender               否.        性别。1表示男性，2表示女性
    * email                否.        邮箱。长度不超过64个字节，且为有效的email格式。企业内必须唯一
    * telephone            否.        座机。由1-32位的纯数字或’-‘号组成
    * isleader             否.        上级字段，标识是否为上级。
    * avatar_mediaid       否.        成员头像的mediaid，通过素材管理接口上传图片获得的mediaid
    * enable               否.        启用/禁用成员。1表示启用成员，0表示禁用成员
    * extattr              否.        自定义字段。自定义字段需要先在WEB管理端添加，见扩展属性添加方法，否则忽略未知属性的赋值。自定义字段长度为0~32个字符，超过将被截断
    * external_profile     否.        成员对外属性，字段详情见对外属性特别地，如果userid由系统自动生成，则仅允许修改一次。新值可由new_userid字段指定。
    * 说明：
    * 应用须拥有指定部门、成员的管理权限。
    * 注意，每个部门下的部门、成员总数不能超过3万个。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "updated"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode updateUser(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @NotNull("请求包体为空")UpdateBody body);
    default WxErrCode updateUser(UpdateBody body) {
        return updateUser((String)null, body);
    }
    
    /**
    * 删除成员
    * -:deleteUser
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token=ACCESS_TOKEN&userid=USERID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * userid               是.        成员UserID。对应管理端的帐号
    * 说明：
    * 应用须拥有指定成员的管理权限。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @GET
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode deleteUser(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @QueryParam("userid")@NotNull("用户为空") String userid);
    default WxErrCode deleteUser(String userid) {
        return deleteUser(userid);
    }
    
    /**
    * 批量删除成员
    * -:batchdeleteUser
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "useridlist": ["zhangsan", "lisi"]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * useridlist           是.        成员UserID列表。对应管理端的帐号。最多支持200个。若存在无效UserID，直接返回错误
    * 说明：
    * 应用须拥有指定成员的管理权限。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @POST
    @Path("batchdelete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode batchdeleteUser(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @NotNull("请求包体为空") BatchdeleteUserBody body);
    default WxErrCode batchdeleteUser(BatchdeleteUserBody body) {
        return batchdeleteUser((String)null, body);
    }
    
    /**
    * 获取部门成员
    * -:simplelistUser
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.          必须.   说明
    * access_token    是.   调用接口凭证
    * department_id   是.   获取的部门id
    * fetch_child     否.   1/0：是否递归获取子部门下面的成员
    * 
    * 说明：
    * 应用须拥有指定部门的查看权限。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "userlist": [
    *            {
    *                   "userid": "zhangsan",
    *                   "name": "李四",
    *                   "department": [1, 2]
    *            }
    *      ]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * userlist             成员列表
    * userid               成员UserID。对应管理端的帐号
    * name                 成员名称
    * department           成员所属部门
    * 
    */
    @GET
    @Path("simplelist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SimplelistUserResult simplelistUser(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @QueryParam("department_id")@NotNull("部门为空") String departmentId, @QueryParam("fetch_child") Integer fetchChild);
    default SimplelistUserResult simplelistUser(String departmentId, Integer fetchChild) {
        return simplelistUser((String)null, departmentId, fetchChild);
    }

    /**
    * 获取部门成员详情
    * -:getUserList
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token=ACCESS_TOKEN&department_id=DEPARTMENT_ID&fetch_child=FETCH_CHILD
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * department_id        是.        获取的部门id
    * fetch_child          否.        1/0：是否递归获取子部门下面的成员
    * 说明：
    * 应用须拥有指定部门的查看权限。
    * 
    * 返回结果：
    * {
    *     "errcode": 0,
    *     "errmsg": "ok",
    *     "userlist": [{
    *         "userid": "zhangsan",
    *         "name": "李四",
    *         "department": [1, 2],
    *         "order": [1, 2],
    *         "position": "后台工程师",
    *         "mobile": "15913215421",
    *         "gender": "1",
    *         "email": "zhangsan@gzdev.com",
    *         "isleader": 0,
    *         "avatar": "http://wx.qlogo.cn/mmopen/ajNVdqHZLLA3WJ6DSZUfiakYe37PKnQhBIeOQBO4czqrnZDS79FH5Wm5m4X69TBicnHFlhiafvDwklOpZeXYQQ2icg/0",
    *         "telephone": "020-123456",
    *         "enable": 1,
    *         "english_name": "jackzhang",
    *         "status": 1,
    *         "extattr": {
    *             "attrs": [{
    *                 "name": "爱好",
    *                 "value": "旅游"
    *             }, {
    *                 "name": "卡号",
    *                 "value": "1234567234"
    *             }]
    *         },
    *         "qr_code": "https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=xxx",
    *         "external_profile": {
    *             "external_attr": [{
    *                     "type": 0,
    *                     "name": "文本名称",
    *                     "text": {
    *                         "value": "文本"
    *                     }
    *                 },
    *                 {
    *                     "type": 1,
    *                     "name": "网页名称",
    *                     "web": {
    *                         "url": "http://www.test.com",
    *                         "title": "标题"
    *                     }
    *                 },
    *                 {
    *                     "type": 2,
    *                     "name": "测试app",
    *                     "miniprogram": {
    *                         "appid": "wx8bd80126147df384",
    *                         "pagepath": "/index",
    *                         "title": "my miniprogram"
    *                     }
    *                 }
    *             ]
    *         }
    *     }]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * userlist             成员列表
    * userid               成员UserID。对应管理端的帐号
    * name                 成员名称
    * mobile               手机号码，第三方仅通讯录套件可获取
    * department           成员所属部门id列表
    * order                部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0,
    * position             职位信息；第三方仅通讯录应用可获取
    * gender               性别。0表示未定义，1表示男性，2表示女性
    * email                邮箱，第三方仅通讯录应用可获取
    * isleader             标示是否为上级；第三方仅通讯录应用可获取
    * avatar               头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。第三方仅通讯录应用可获取
    * telephone            座机。第三方仅通讯录应用可获取
    * enable               成员启用状态。1表示启用的成员，0表示被禁用。服务商调用接口不会返回此字段
    * english_name         英文名；第三方仅通讯录应用可获取
    * status               激活状态:
    * extattr              扩展属性，第三方仅通讯录套件可获取
    * qr_code              员工个人二维码，扫描可添加为外部联系人；第三方仅通讯录应用可获取
    * external_profile     成员对外属性，字段详情见对外属性；第三方仅通讯录应用可获取
    * 
    */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserListResult getUserList(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @QueryParam("department_id")@NotNull("部门为空") String departmentId, @QueryParam("fetch_child") Integer fetchChild);
    default UserListResult getUserList(String departmentId, Integer fetchChild) {
        return getUserList((String)null, departmentId, fetchChild);
    }

    /**
    * userid转openid
    * 该接口使用场景为企业支付，在使用企业红包和向员工付款时，需要自行将企业微信的userid转成openid。
    * -:convert2Openid
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "userid": "zhangsan"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * userid               是.        企业内的成员id
    * 说明：
    * 成员必须处于应用的可见范围内
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "openid": "oDOGms-6yCnGrRovBj2yHij5JL6E",
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * openid               企业微信成员userid对应的openid
    * 
    */
    @POST
    @Path("convert_to_openid")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Convert2OpenidResult convert2Openid(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @NotNull("请求包体为空")Convert2OpenidBody body);
    default Convert2OpenidResult convert2Openid(String userid) {
        Convert2OpenidBody body = new Convert2OpenidBody();
        body.setUserid(userid);
        return convert2Openid((String)null, body);
    }
    
    /**
    * openid转userid
    * 该接口主要应用于使用企业支付之后的结果查询。
    * 开发者需要知道某个结果事件的openid对应企业微信内成员的信息时，可以通过调用该接口进行转换查询。
    * -:convert2Userid
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_userid?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "openid": "oDOGms-6yCnGrRovBj2yHij5JL6E"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * openid               是.        在使用企业支付之后，返回结果的openid
    * 说明：
    * 管理组需对openid对应的企业微信成员有查看权限。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "userid": "zhangsan"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * userid               该openid在企业微信对应的成员userid
    * 
    */
    @POST
    @Path("convert_to_userid")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    Convert2UseridResult convert2Userid(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @NotNull("请求包体为空")Convert2UseridBody body);
    default Convert2UseridResult convert2Userid(String openid) {
        Convert2UseridBody body = new Convert2UseridBody();
        body.setOpenid(openid);
        return convert2Userid((String)null, body);
    }
    
    /**
    * 二次验证
    * 此接口可以满足安全性要求高的企业进行成员加入验证。开启二次验证后，用户加入企业时需要跳转企业自定义的页面进行验证。   
    * 企业在开启二次验证时，必须在管理端填写企业二次验证页面的url。
    * 当成员登录企业微信或关注微工作台（原企业号）加入企业时，会自动跳转到企业的验证页面。在跳转到企业的验证页面时，会带上如下参数：code=CODE。
    * 企业收到code后，使用“通讯录同步助手”调用“根据code获取成员信息”接口获取成员的userid。然后在验证成员信息成功后，调用如下接口即可让成员成功加入企业。
    * auth2User
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/authsucc?access_token=ACCESS_TOKEN&userid=USERID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * userid               是.        成员UserID。对应管理端的帐号
    * 说明：
    * 
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @GET
    @Path("authsucc")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode authsucc(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken,
            @QueryParam("userid")@NotNull("userid为空") String userid);
    default WxErrCode authsucc(String userid) {
        return authsucc((String)null, userid);
    }
    
    /**
    * 根据code获取成员信息
    * -:getUserInfo
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=ACCESS_TOKEN&code=CODE
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * code                 是.        通过成员授权获取到的code，最大为512字节。每次成员授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
    * 说明：
    * 跳转的域名须完全匹配access_token对应应用的可信域名，否则会返回50001错误。
    * 返回结果：
    * a) 当用户为企业成员时返回示例如下：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "UserId":"USERID",
    *    "DeviceId":"DEVICEID",
    *    "user_ticket": "USER_TICKET"，
    *    "expires_in":7200
    * }
    * 参数    说明
    * errcode   返回码
    * errmsg    对返回码的文本描述内容
    * UserId    成员UserID
    * DeviceId  手机设备号(由企业微信在安装时随机生成，删除重装会改变，升级不受影响)
    * user_ticket   成员票据，最大为512字节。
    * scope为snsapi_userinfo或snsapi_privateinfo，且用户在应用可见范围之内时返回此参数。
    * 后续利用该参数可以获取用户信息或敏感信息。
    * expires_in    user_ticket的有效时间（秒），随user_ticket一起返回
    * b) 非企业成员授权时返回示例如下：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "OpenId":"OPENID",
    *    "DeviceId":"DEVICEID"
    * }
    * 参数    说明
    * errcode   返回码
    * errmsg    对返回码的文本描述内容
    * OpenId    非企业成员的标识，对当前企业唯一
    * DeviceId  手机设备号(由企业微信在安装时随机生成，删除重装会改变，升级不受影响)
    * 出错返回示例：
    * {
    *    "errcode": 40029,
    *    "errmsg": "invalid code"
    * }
    * 
    */
    @GET
    @Path("getuserinfo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserInfoResult getUserInfo(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @QueryParam("code")@Value(WxConsts.T_OAUTH2_CODE)@NotNull("成员授权获取到的code为空") String code);
    default UserInfoResult getUserInfo(String code) {
        return getUserInfo((String)null, code);
    }
    
    /**
    * 使用user_ticket获取成员详情
    * -:getUserDetail
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/user/getuserdetail?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "user_ticket": "USER_TICKET"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证，第三方服务商通过企业的永久授权码获取，参见“获取企业access_token”
    * user_ticket          是.        成员票据
    * 说明：
    * 需要有对应应用的使用权限，且成员必须在授权应用的可见范围内。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "userid":"lisi",
    *    "name":"李四",
    *    "mobile":"13050495892",
    *    "gender":"1",
    *    "email":"xxx@xx.com",
    *    "avatar":"http://shp.qpic.cn/bizmp/xxxxxxxxxxx/0",
    *    "qr_code":"http://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vcfc13b01dfs78e981c"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * userid               成员UserID
    * name                 成员姓名
    * mobile               成员手机号，仅在用户同意snsapi_privateinfo授权时返回
    * gender               性别。0表示未定义，1表示男性，2表示女性
    * email                成员邮箱，仅在用户同意snsapi_privateinfo授权时返回
    * avatar               头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。仅在用户同意snsapi_privateinfo授权时返回
    * qr_code              员工个人二维码（扫描可添加为外部联系人），仅在用户同意snsapi_privateinfo授权时返回
    * 
    */
    @POST
    @Path("getuserdetail")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserDetailResult getUserDetail(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            UserDetailBody body);
    default UserDetailResult getUserDetail(String userTicket) {
        UserDetailBody body = new UserDetailBody();
        body.setUserTicket(userTicket);
        return getUserDetail((String)null, body);
    }


}
