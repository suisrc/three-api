package net.icgear.three.weixin.qyapi.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.qyapi.rest.dto.external.ExternalProfile;

/**
 * 获取外部联系人详情
 * 
 * @author Y13
 *
 */
@ApiModel("ExternalContactResult")
public class ExternalContactResult extends WxErrCode {
    private static final long serialVersionUID = 3402018506491199363L;

    @ApiModel("ExternalContactResult.ExternalContact")
    public static class ExternalContact {

        @ApiModelProperty("外部联系人的userid")
        @JsonProperty("external_userid")
        private String externalUserid;

        @ApiModelProperty("外部联系人的姓名")
        @JsonProperty("name")
        private String name;

        @ApiModelProperty("外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段")
        @JsonProperty("position")
        private String position;

        @ApiModelProperty("外部联系人头像，第三方不可获取")
        @JsonProperty("avatar")
        private String avatar;

        @ApiModelProperty("外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段")
        @JsonProperty("corp_name")
        private String corpName;

        @ApiModelProperty("外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段")
        @JsonProperty("corp_full_name")
        private String corpFullName;

        @ApiModelProperty("外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户")
        @JsonProperty("type")
        private Integer type;

        @ApiModelProperty("外部联系人性别")
        @JsonProperty("gender")
        private Integer gender;

        @ApiModelProperty("外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业绑定了微信开发者ID有此字段。查看绑定方法")
        @JsonProperty("unionid")
        private String unionid;

        @ApiModelProperty("外部联系人的自定义展示信息，可以有多个字段和多种类型，包括文本，网页和小程序，仅当联系人类型是企业微信用户时有此字段，字段详情见对外属性；")
        @JsonProperty("external_profile")
        private ExternalProfile externalProfile;

        /**
         * 外部联系人的userid
         */
        public String getExternalUserid() {
            return externalUserid;
        }

        /**
         * 外部联系人的userid
         */
        public void setExternalUserid(String externalUserid) {
            this.externalUserid = externalUserid;
        }

        /**
         * 外部联系人的姓名
         */
        public String getName() {
            return name;
        }

        /**
         * 外部联系人的姓名
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
         */
        public String getPosition() {
            return position;
        }

        /**
         * 外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
         */
        public void setPosition(String position) {
            this.position = position;
        }

        /**
         * 外部联系人头像，第三方不可获取
         */
        public String getAvatar() {
            return avatar;
        }

        /**
         * 外部联系人头像，第三方不可获取
         */
        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        /**
         * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
         */
        public String getCorpName() {
            return corpName;
        }

        /**
         * 外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
         */
        public void setCorpName(String corpName) {
            this.corpName = corpName;
        }

        /**
         * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
         */
        public String getCorpFullName() {
            return corpFullName;
        }

        /**
         * 外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
         */
        public void setCorpFullName(String corpFullName) {
            this.corpFullName = corpFullName;
        }

        /**
         * 外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户
         */
        public Integer getType() {
            return type;
        }

        /**
         * 外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户
         */
        public void setType(Integer type) {
            this.type = type;
        }

        /**
         * 外部联系人性别
         */
        public Integer getGender() {
            return gender;
        }

        /**
         * 外部联系人性别
         */
        public void setGender(Integer gender) {
            this.gender = gender;
        }

        /**
         * 外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业绑定了微信开发者ID有此字段。查看绑定方法
         */
        public String getUnionid() {
            return unionid;
        }

        /**
         * 外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业绑定了微信开发者ID有此字段。查看绑定方法
         */
        public void setUnionid(String unionid) {
            this.unionid = unionid;
        }

        /**
         * 外部联系人的自定义展示信息，可以有多个字段和多种类型，包括文本，网页和小程序，仅当联系人类型是企业微信用户时有此字段，字段详情见对外属性；
         */
        public ExternalProfile getExternalProfile() {
            return externalProfile;
        }

        /**
         * 外部联系人的自定义展示信息，可以有多个字段和多种类型，包括文本，网页和小程序，仅当联系人类型是企业微信用户时有此字段，字段详情见对外属性；
         */
        public void setExternalProfile(ExternalProfile externalProfile) {
            this.externalProfile = externalProfile;
        }
    }

    @ApiModel("ExternalContactResult.FollowUser")
    public static class FollowUser {

        @ApiModelProperty("添加了此外部联系人的企业成员userid")
        @JsonProperty("userid")
        private String userid;

        @ApiModelProperty("该成员对此外部联系人的备注")
        @JsonProperty("remark")
        private String remark;

        @ApiModelProperty("该成员对此外部联系人的描述")
        @JsonProperty("description")
        private String description;

        @ApiModelProperty("该成员添加此外部联系人的时间")
        @JsonProperty("createtime")
        private Integer createtime;

        /**
         * 添加了此外部联系人的企业成员userid
         */
        public String getUserid() {
            return userid;
        }

        /**
         * 添加了此外部联系人的企业成员userid
         */
        public void setUserid(String userid) {
            this.userid = userid;
        }

        /**
         * 该成员对此外部联系人的备注
         */
        public String getRemark() {
            return remark;
        }

        /**
         * 该成员对此外部联系人的备注
         */
        public void setRemark(String remark) {
            this.remark = remark;
        }

        /**
         * 该成员对此外部联系人的描述
         */
        public String getDescription() {
            return description;
        }

        /**
         * 该成员对此外部联系人的描述
         */
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         * 该成员添加此外部联系人的时间
         */
        public Integer getCreatetime() {
            return createtime;
        }

        /**
         * 该成员添加此外部联系人的时间
         */
        public void setCreatetime(Integer createtime) {
            this.createtime = createtime;
        }
    }

    @JsonProperty("external_contact")
    private ExternalContact externalContact;

    @JsonProperty("follow_user")
    private FollowUser[] followUser;

    public ExternalContact getExternalContact() {
        return externalContact;
    }

    public void setExternalContact(ExternalContact externalContact) {
        this.externalContact = externalContact;
    }

    public FollowUser[] getFollowUser() {
        return followUser;
    }

    public void setFollowUser(FollowUser[] followUser) {
        this.followUser = followUser;
    }
}
