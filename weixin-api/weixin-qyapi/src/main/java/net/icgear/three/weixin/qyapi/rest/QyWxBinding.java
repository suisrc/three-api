package net.icgear.three.weixin.qyapi.rest;

import net.icgear.three.weixin.core.crypto.WxCrypto;

/**
 * 企业回调接口内容
 * 
 * @author Y13
 *
 * @param <T>
 */
public abstract class QyWxBinding extends QmWxBinding {

    /**
     * 解密时候，记录消息来源的企业ID
     */
    @Override
    protected String decryptMessage(WxCrypto wxCrypt, String encrypt) {
        return decryptMessage2(wxCrypt, encrypt);
    }

}
