package net.icgear.three.weixin.qyapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsConsts;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.qyapi.QyConsts;
import net.icgear.three.weixin.qyapi.rest.dto.department.CreateDepartmentBody;
import net.icgear.three.weixin.qyapi.rest.dto.department.CreateDepartmentResult;
import net.icgear.three.weixin.qyapi.rest.dto.department.GetDepartmentListResult;
import net.icgear.three.weixin.qyapi.rest.dto.department.UpdateDepartmentBody;

/**
 * 
 * 部门管理
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin/department")
public interface DepartmentManagerRest {
    final String NAMED = JaxrsConsts.separator + "net.icgear.three.weixin.qyapi.rest.api.DepartmentRest";
    
    /**
    * 创建部门
    * -:createDepartment
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "name": "广州研发中心",
    *    "parentid": 1,
    *    "order": 1,
    *    "id": 2
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * name                 是.        部门名称。长度限制为1~32个字符，字符不能包括\:?”<>｜
    * parentid             是.        父部门id，32位整型
    * order                否.        在父部门中的次序值。order值大的排序靠前。有效的值范围是[0, 2^32)
    * id                   否.        部门id，32位整型，指定时必须大于1。若不填该参数，将自动生成id
    * 说明：
    * 应用须拥有父部门的管理权限。
    * 注意，部门的最大层级为15层；部门总数不能超过3万个；每个部门下的节点不能超过3万个。建议保证创建的部门和对应部门成员是串行化处理。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "created",
    *    "id": 2
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * id                   创建的部门id
    * 
    */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    CreateDepartmentResult createDepartment(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @NotNull("请求包体为空")CreateDepartmentBody body);
    default CreateDepartmentResult createDepartment(CreateDepartmentBody body) {
        return createDepartment((String)null, body);
    }
    
    /**
    * 更新部门
    * -:updateDepartment
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "id": 2,
    *    "name": "广州研发中心",
    *    "parentid": 1,
    *    "order": 1
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * id                   是.        部门id
    * name                 否.        部门名称。长度限制为1~32个字符，字符不能包括\:?”<>｜
    * parentid             否.        父部门id
    * order                否.        在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,
    * 说明：
    * 
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "updated"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode updateDepartment(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @NotNull("请求包体为空")UpdateDepartmentBody body);
    default WxErrCode updateDepartment(UpdateDepartmentBody body) {
        return updateDepartment((String)null, body);
    }
    
    /**
    * 删除部门
    * -:deleteDepartment
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token=ACCESS_TOKEN&id=ID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * id                   否.        部门id。（注：不能删除根部门；不能删除含有子部门、成员的部门）
    * 说明：
    * 应用须拥有指定部门的管理权限。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @GET
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode deleteDepartment(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @QueryParam("id")@NotNull("部门id为空") String id);
    default WxErrCode deleteDepartment(String id) {
        return deleteDepartment((String)null, id);
    }
    
    /**
    * 获取部门列表
    * -:getDepartmentList
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=ACCESS_TOKEN&id=ID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * id                   否.        部门id。获取指定部门及其下的子部门。
    * 说明：
    * 只能拉取token对应的应用的权限范围内的部门列表
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "department": [
    *        {
    *            "id": 2,
    *            "name": "广州研发中心",
    *            "parentid": 1,
    *            "order": 10
    *        },
    *        {
    *            "id": 3,
    *            "name": "邮箱产品部",
    *            "parentid": 2,
    *            "order": 40
    *        }
    *    ]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * department           部门列表数据。
    * id                   创建的部门id
    * name                 部门名称
    * parentid             父亲部门id。根部门为1
    * order                在父部门中的次序值。order值大的排序靠前。值范围是[0,
    * 
    */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetDepartmentListResult getDepartmentList(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            @QueryParam("id")@NotNull("部门id为空") String id);
    default GetDepartmentListResult getDepartmentList(String id) {
        return getDepartmentList((String)null, id);
    }
}
