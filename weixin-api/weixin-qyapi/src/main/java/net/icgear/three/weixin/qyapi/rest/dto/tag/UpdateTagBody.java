package net.icgear.three.weixin.qyapi.rest.dto.tag;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 更新标签名字
 * 
 * @author Y13
 *
 */
@ApiModel("UpdateTagBody")
public class UpdateTagBody {

    @ApiModelProperty("标签ID")
    @NotNull("tagid属性为空")
    @JsonProperty("tagid")
    private Integer tagid;

    @ApiModelProperty("标签名称，长度限制为32个字（汉字或英文字母），标签不可与其他标签重名。")
    @NotNull("tagname属性为空")
    @JsonProperty("tagname")
    private String tagname;

    /**
     * 标签ID
     */
    public Integer getTagid() {
        return tagid;
    }

    /**
     * 标签ID
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }

    /**
     * 标签名称，长度限制为32个字（汉字或英文字母），标签不可与其他标签重名。
     */
    public String getTagname() {
        return tagname;
    }

    /**
     * 标签名称，长度限制为32个字（汉字或英文字母），标签不可与其他标签重名。
     */
    public void setTagname(String tagname) {
        this.tagname = tagname;
    }
}
