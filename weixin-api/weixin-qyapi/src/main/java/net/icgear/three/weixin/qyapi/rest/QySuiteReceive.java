package net.icgear.three.weixin.qyapi.rest;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Function;

import javax.annotation.PostConstruct;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.three.core.listener.inf.Listener;
import com.suisrc.three.core.msg.IMessage;

import net.icgear.three.weixin.core.WxConfig;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.EncryptMessage;
import net.icgear.three.weixin.core.bean.WxEncryptSignature;
import net.icgear.three.weixin.core.crypto.WxCrypto;
import net.icgear.three.weixin.core.rest.AbstractWxBindingOne;
import net.icgear.three.weixin.core.suite.TicketSuite;
import net.icgear.three.weixin.core.type.suite.DefaultSuiteTypeParser;
import net.icgear.three.weixin.qyapi.listerner.SuiteCreateAuthListener;
import net.icgear.three.weixin.qyapi.listerner.SuiteTicketListener;
import net.icgear.three.weixin.qyapi.rest.api.QyThreeServiceRest;
import net.icgear.three.weixin.qyapi.rest.dto.three.PermanentCodeResult;

/**
 * 企业回调接口内容
 * 
 * @author Y13
 *
 * @param <T>
 */
public abstract class QySuiteReceive extends AbstractWxBindingOne {

    /**
     * 
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    protected Collection<Class<Listener<?>>> getAppendListenerClasses() {
        Collection collection = Arrays.asList(SuiteTicketListener.class, SuiteCreateAuthListener.class);
        return collection;
    }
    
    /**
     * 
     */
    @Override
    protected Collection<Class<? extends IMessage>> getMsgTypes() {
        return null;
    }

    /**
     * 
     */
    protected Collection<Class<? extends IMessage>> getSuiteTypes() {
        return ReflectionUtils.getSubclasses(IMessage.class, TicketSuite.class.getPackage().getName());
    }
    /**
     * 
     */
    @Override
    @PostConstruct
    protected void doPostConstruct() {
        super.doPostConstruct();
        Collection<Class<? extends IMessage>> suiteTypes = getSuiteTypes();
        if (suiteTypes != null) {
            setMsgParser(new DefaultSuiteTypeParser(suiteTypes, getBindingCategory()));
        }
    }
    
    /**
     * 
     */
    @Override
    protected String getBindingCategory() {
        return WxConsts.QY;
    }
    
    /**
     * 获取监听的包的路径
     */
    protected String getListenerPackagesKey() {
        return WxConsts.WEIXIN_PRE + getAppUniqueKey() + ".suite.packages";
    }
    
    /**
     * 获取监听的类的路径
     */
    protected String getListenerClassesKey() {
        return WxConsts.WEIXIN_PRE + getAppUniqueKey() + ".suite.classes";
    }

    /**
     * 获取一个新的加密解密工具对象
     * @param encryptMsg 
     * @param sign 
     * @return
     */
    protected WxCrypto getNewWxCrypto(WxConfig config, WxEncryptSignature sign, EncryptMessage encryptMsg) {
        WxCrypto crypto;
        if (encryptMsg == null) {
            crypto = new WxCrypto(config.getWxToken(), config.getWxEncodingKey(), config.getWxAppId());
        } else {
            crypto = new WxCrypto(config.getWxToken(), config.getWxEncodingKey(), config.getWxSuiteId());
        }
        return crypto;
    }
    
    /**
     * 
     */
    public String doSuiteGet(WxEncryptSignature sign) {
        return doGet(sign);
    }

    /**
     * 
     */
    public Response doSuitePost(WxEncryptSignature sign, String data) {
        return doPost(sign, data);
    }
    
    @Override
    protected Function<String, String> getResultAdapter(WxCrypto crypto, boolean isJson) {
        return null;
    }
    
    /**
     * 该服务返回应该总是sucess,但是由于可能有特殊情况发生，这里直接返回业务处理字符串，
     * 因此业务返回时候请直接返回sucess
     */
    @Override
    protected Function<String, Response> getResponseAdapter() {
        return content -> Response.ok().entity(content).type(MediaType.TEXT_PLAIN).build();
    }
    
    /**
     * 适配器设定属性
     */
    public <U> void setAdapter(String named, Class<U> type, U obj) {
        if (type == TicketSuite.class || type == PermanentCodeResult.class) {
            getConfig().setAdapter(named, type, obj);
            return;
        }
        super.setAdapter(named, type, obj);
    };
    
    /**
     * 适配器获取属性
     */
    @Override
    public <U> U getAdatper(String named, Class<U> type) {
        if (type == QyThreeServiceRest.class) {
            return getConfig().getAdapter(named, type);
        }
        return super.getAdatper(named, type);
    }

}
