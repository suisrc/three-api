package net.icgear.three.weixin.qyapi.listerner;

import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;

import com.suisrc.core.reference.RefVal;
import com.suisrc.three.core.ThreeBinding;
import com.suisrc.three.core.exception.UnHandleException;
import com.suisrc.three.core.listener.inf.Listener;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.suite.CreateAuthSuite;
import net.icgear.three.weixin.qyapi.rest.api.QyThreeServiceRest;
import net.icgear.three.weixin.qyapi.rest.dto.three.PermanentCodeResult;

/**
 * 推送授权信息
 * 
 * 确实是否允许授权，可以通过查看是否可以获取企业永久授权进行判断
 * 
 * 加载等级为等级为"R"， 加载等级低于标准的"N"级别
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class SuiteCreateAuthListener implements Listener<CreateAuthSuite> {
    
    /**
     * 该方法内容废弃，使用accept(Refval<Object>, TicketSuite, Object)代替其实现
     */
    @Deprecated
    @Override
    public boolean accept(RefVal<Object> result, CreateAuthSuite bean) {
        return false;
    }    
    
    /**
     * 监听到的ticket同时对内容进行处理
     */
    @Override
    public boolean accept(RefVal<Object> result, CreateAuthSuite bean, Object owner) {
        if (owner instanceof ThreeBinding) {
            try {
                // 将监听到的suite_ticket传入绑定控制器中。
                ThreeBinding binding = (ThreeBinding) owner;
                QyThreeServiceRest rest = binding.getAdatper(null, QyThreeServiceRest.class);
                PermanentCodeResult pcResult = rest.getPermanentCode(bean.getAuthCode());
                if (result == null) {
                    throw new Exception("无法获取企业永久授权信息");
                } else if (pcResult.getErrcode() != null && pcResult.getErrcode() != 0) {
                    String error = String.format("获取企业永久授权信息失败：[%d]", pcResult.getErrcode());
                    throw new Exception(error);
                }
                binding.setAdapter(null, PermanentCodeResult.class, pcResult);
                result.set(WxConsts.SUCCESS);
                return false;
            } catch (Exception e) {
                UnHandleException uhe;
                if (e instanceof UnHandleException) {
                    uhe = (UnHandleException)e;
                } else {
                    String errid = UUID.randomUUID().toString();
                    String error = e.getMessage() == null ? "未知异常" : e.getMessage();
                    error = "无法完成授权处理：[" + errid + "]" + error;
                    uhe = new UnHandleException(error, e);
                }
                throw uhe;
            }
        } else {
            // 无法处理授权，跳过
            return true;
        }
    }

    @Override
    public String priority(CreateAuthSuite bean) {
        return "R";
    }
    
}
