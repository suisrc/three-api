package net.icgear.three.weixin.qyapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsConsts;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.qyapi.QyConsts;
import net.icgear.three.weixin.qyapi.rest.dto.ExternalContactResult;

/**
 * 获取外部联系人详情
 * @author Y13
 *
 */
@RemoteApi("cgi-bin/crm")
public interface CrmManagerRest {
    final String NAMED = JaxrsConsts.separator + "net.icgear.three.weixin.qyapi.rest.api.CrmManagerRest";
    
    /**
    * 获取外部联系人详情
    * 企业可通过此接口，根据外部联系人的userid（如何获取?），拉取外部联系人详情。
    * -:getExternalContact
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/crm/get_external_contact?access_token=ACCESS_TOKEN&external_userid=EXTERNAL_USERID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * external_userid      是.        外部联系人的userid，注意不是企业成员的帐号
    * 说明：
    * 企业需要使用外部联系人管理secret所获取的accesstoken来调用（accesstoken如何获取？）；
    * 第三方应用需拥有“企业客户”权限。
    * 第三方应用调用时，返回的跟进人follow_user仅包含应用可见范围之内的成员。
    * 如何绑定微信开发者ID
    * 1、登录企业的管理后台-外部联系人-api，点击绑定去到微信公众平台进行授权，支持绑定公众号和小程序（需要同时绑定微信开放平台）；绑定的公众号或小程序主体需与企业微信主体一致，暂且支持绑定一个
    * 2、绑定完成，即可通过接口获取微信联系人所对应的微信union id
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "external_contact":
    *    {
    *        "external_userid":"woAJ2GCAAAXtWyujaWJHDDGi0mACH71w",
    *        "name":"李四",
    *        "position":"Mangaer",
    *        "avatar":"http://p.qlogo.cn/bizmail/IcsdgagqefergqerhewSdage/0",
    *        "corp_name":"腾讯",
    *        "corp_full_name":"腾讯科技有限公司",
    *        "type":2,
    *        "gender":1,
    *        "unionid":"ozynqsulJFCZ2z1aYeS8h-nuasdfR1",
    *     "external_profile":{
    *       "external_attr":[
    *         {
    *           "type":0,
    *           "name":"文本名称",
    *           "text":
    *                   {
    *             "value":"文本"
    *           }
    *         },
    *         {
    *           "type":1,
    *           "name":"网页名称",
    *           "web":
    *                   {
    *             "url":"http://www.test.com",
    *             "title":"标题"
    *           }
    *         },
    *         {
    *           "type":2,
    *           "name":"测试app",
    *           "miniprogram":
    *                    {
    *                       "appid": "wx8bd80126147df384",
    *                       "pagepath": "/index",
    *                       "title": "my miniprogram"
    *           }
    *         }
    *       ]
    *     }
    *    },
    *     "follow_user":
    *     [
    *         {
    *         "userid":"rocky",
    *         "remark":"李部长",
    *         "description":"对接采购事物",
    *         "createtime":1525779812
    *         },
    *         {
    *         "userid":"tommy",
    *         "remark":"李总",
    *         "description":"采购问题咨询",
    *         "createtime":1525881637
    *         }
    *     ]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * external_userid      外部联系人的userid
    * name                 外部联系人的姓名
    * avatar               外部联系人头像，第三方不可获取
    * type                 外部联系人的类型，1表示该外部联系人是微信用户（暂时仅内测企业有此类型），2表示该外部联系人是企业微信用户
    * gender               外部联系人性别
    * unionid              外部联系人在微信开放平台的唯一身份标识（微信unionid），通过此字段企业可将外部联系人与公众号/小程序用户关联起来。仅当联系人类型是微信用户，且企业绑定了微信开发者ID有此字段。查看绑定方法
    * position             外部联系人的职位，如果外部企业或用户选择隐藏职位，则不返回，仅当联系人类型是企业微信用户时有此字段
    * corp_name            外部联系人所在企业的简称，仅当联系人类型是企业微信用户时有此字段
    * corp_full_name       外部联系人所在企业的主体名称，仅当联系人类型是企业微信用户时有此字段
    * external_profile     外部联系人的自定义展示信息，可以有多个字段和多种类型，包括文本，网页和小程序，仅当联系人类型是企业微信用户时有此字段，字段详情见对外属性；
    * follow_user.userid   添加了此外部联系人的企业成员userid
    * follow_user.remark   该成员对此外部联系人的备注
    * follow_user.description 该成员对此外部联系人的描述
    * follow_user.createtime 该成员添加此外部联系人的时间
    * 
    */
    @GET
    @Path("get_external_contact")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ExternalContactResult getExternalContact(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken, 
            @QueryParam("external_userid")@NotNull("external_userid为空") String userid);
    default ExternalContactResult getExternalContact(String userid) {
        return getExternalContact(userid);
    }
}
