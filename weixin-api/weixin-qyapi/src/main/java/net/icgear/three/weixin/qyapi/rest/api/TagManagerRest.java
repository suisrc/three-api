package net.icgear.three.weixin.qyapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsConsts;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.qyapi.QyConsts;
import net.icgear.three.weixin.qyapi.rest.dto.tag.CreateTagBody;
import net.icgear.three.weixin.qyapi.rest.dto.tag.CreateTagResult;
import net.icgear.three.weixin.qyapi.rest.dto.tag.GetTagListResult;
import net.icgear.three.weixin.qyapi.rest.dto.tag.GetTagResult;
import net.icgear.three.weixin.qyapi.rest.dto.tag.TagUsersBody;
import net.icgear.three.weixin.qyapi.rest.dto.tag.TagUsersResult;
import net.icgear.three.weixin.qyapi.rest.dto.tag.UpdateTagBody;

/**
 * 
 * 标签管理
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin/tag")
public interface TagManagerRest {
    final String NAMED = JaxrsConsts.separator + "net.icgear.three.weixin.qyapi.rest.api.TagManagerRest";
    
    /**
    * 创建标签
    * -:createTag
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "tagname": "UI",
    *    "tagid": 12
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * tagname              是.        标签名称，长度限制为32个字（汉字或英文字母），标签名不可与其他标签重名。
    * tagid                否.        标签id，非负整型，指定此参数时新增的标签会生成对应的标签id，不指定时则以目前最大的id自增。
    * 说明：
    * 创建的标签属于该应用，只有该应用才可以增删成员。
    * 注意，标签总数不能超过3000个。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "created",
    *    "tagid": 12
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * tagid                标签id
    * 
    */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    CreateTagResult createTag(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @NotNull("请求包体为空") CreateTagBody body);
    default CreateTagResult createTag(CreateTagBody body) {
        return createTag((String)null, body);
    }
    
    /**
    * 更新标签名字
    * -:updateTag
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/update?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "tagid": 12,
    *    "tagname": "UI design"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * tagid                是.        标签ID
    * tagname              是.        标签名称，长度限制为32个字（汉字或英文字母），标签不可与其他标签重名。
    * 说明：
    * 调用者必须是指定标签的创建者。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "updated"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode updateTag(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @NotNull("请求包体为空") UpdateTagBody body);
    default WxErrCode updateTag(Integer tagid, String tagname) {
        UpdateTagBody body = new UpdateTagBody();
        body.setTagid(tagid);
        body.setTagname(tagname);
        return updateTag((String)null, body);
    }
    
    /**
    * 删除标签
    * -:deleteTag
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/delete?access_token=ACCESS_TOKEN&tagid=TAGID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * tagid                是.        标签ID
    * 说明：
    * 调用者必须是指定标签的创建者。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * 
    */
    @GET
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode deleteTag(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @QueryParam("tagid")@NotNull("tagid为空") String tagid);
    default WxErrCode deleteTag(String tagid) {
        return deleteTag((String)null, tagid);
    }
    
    /**
    * 获取标签成员
    * -:getTag
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token=ACCESS_TOKEN&tagid=TAGID
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * tagid                是.        标签ID
    * 说明：
    * 无限制，但返回列表仅包含应用可见范围的成员；第三方可获取自己创建的标签及应用可见范围内的标签详情
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "tagname": "乒乓球协会",
    *    "userlist": [
    *          {
    *              "userid": "zhangsan",
    *              "name": "李四"
    *          }
    *      ],
    *    "partylist": [2]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * tagname              标签名
    * userlist             标签中包含的成员列表
    * userid               成员帐号
    * name                 成员名
    * partylist            标签中包含的部门id列表
    * 
    */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetTagResult getTag(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @QueryParam("tagid")@NotNull("tagid为空") String tagid);
    default GetTagResult getTag(String tagid) {
        return getTag((String)null, tagid);
    }
    
    /**
    * 增加标签成员
    * -:addTagUsers
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "tagid": 12,
    *    "userlist":[ "user1","user2"],
    *    "partylist": [4]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * tagid                是.        标签ID
    * userlist             否.        企业成员ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过1000
    * partylist            否.        企业部门ID列表，注意：userlist、partylist不能同时为空，单次请求长度不超过100
    * 说明：
    * 调用者必须是指定标签的创建者；成员属于应用的可见范围。
    * 注意，每个标签下部门、人员总数不能超过3万个。
    * a)正确时返回
    * {
    *    "errcode": 0,
    *    "errmsg": "ok"
    * }
    * b)若部分userid、partylist非法，则返回
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "invalidlist"："usr1|usr2|usr",
    *    "invalidparty"：[2,4]
    * }
    * c)当包含userid、partylist全部非法时返回
    * {
    *    "errcode": 40070,
    *    "errmsg": "all list invalid "
    * }
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "invalidlist": "usr1|usr2|usr",
    *    "invalidparty": [2,4]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * invalidlist          非法的成员帐号列表
    * invalidparty         非法的部门id列表
    * 
    */
    @POST
    @Path("addtagusers")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TagUsersResult addTagUsers(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @NotNull("请求包体为空") TagUsersBody body);
    default TagUsersResult addTagUsers(TagUsersBody body) {
        return addTagUsers((String)null, body);
    }
    
    /**
    * 删除标签成员
    * -:delTagUsers
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *    "tagid": 12,
    *    "userlist":[ "user1","user2"],
    *    "partylist":[2,4]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * tagid                是.        标签ID
    * userlist             否.        企业成员ID列表，注意：userlist、partylist不能同时为空
    * partylist            否.        企业部门ID列表，注意：userlist、partylist不能同时为空
    * 说明：
    * 调用者必须是指定标签的创建者；成员属于应用的可见范围。
    * a)正确时返回
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted"
    * }
    * b)若部分userid、partylist非法，则返回
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted",
    *    "invalidlist"："usr1|usr2|usr",
    *    "invalidparty": [2,4]
    * }
    * c)当包含的userid、partylist全部非法时返回
    * {
    *    "errcode": 40031,
    *    "errmsg": "all list invalid"
    * }
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "deleted",
    *    "invalidlist": "usr1|usr2|usr",
    *    "invalidparty": [2,4]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * invalidlist          非法的成员帐号列表
    * invalidparty         非法的部门id列表
    * 
    */
    @POST
    @Path("deltagusers")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TagUsersResult delTagUsers(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @NotNull("请求包体为空") TagUsersBody body);
    default TagUsersResult delTagUsers(TagUsersBody body) {
        return delTagUsers((String)null, body);
    }
    
    /**
    * 获取标签列表
    * -:getTagList
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/tag/list?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * 说明：
    * 自建应用或通讯同步助手可以获取所有标签列表；第三方应用仅可获取自己创建的标签。
    * 
    * 返回结果：
    * {
    *    "errcode": 0,
    *    "errmsg": "ok",
    *    "taglist":[
    *       {"tagid":1,"tagname":"a"},
    *       {"tagid":2,"tagname":"b"}
    *    ]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * errcode              返回码
    * errmsg               对返回码的文本描述内容
    * taglist              标签列表
    * tagid                标签id
    * tagname              标签名
    * 
    */
    @GET
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetTagListResult getTagList(@QueryParam("access_token")@Value(QyConsts.T_ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken);
    default GetTagListResult getTagList() {
        return getTagList((String)null);
    }
}
