package net.icgear.three.weixin.qyapi.rest.dto.tag;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 创建标签
 * 
 * @author Y13
 *
 */
@ApiModel("CreateTagBody")
public class CreateTagBody {

    @ApiModelProperty("标签名称，长度限制为32个字（汉字或英文字母），标签名不可与其他标签重名。")
    @NotNull("tagname属性为空")
    @JsonProperty("tagname")
    private String tagname;

    @ApiModelProperty("标签id，非负整型，指定此参数时新增的标签会生成对应的标签id，不指定时则以目前最大的id自增。")
    @JsonProperty("tagid")
    private Integer tagid;

    /**
     * 标签名称，长度限制为32个字（汉字或英文字母），标签名不可与其他标签重名。
     */
    public String getTagname() {
        return tagname;
    }

    /**
     * 标签名称，长度限制为32个字（汉字或英文字母），标签名不可与其他标签重名。
     */
    public void setTagname(String tagname) {
        this.tagname = tagname;
    }

    /**
     * 标签id，非负整型，指定此参数时新增的标签会生成对应的标签id，不指定时则以目前最大的id自增。
     */
    public Integer getTagid() {
        return tagid;
    }

    /**
     * 标签id，非负整型，指定此参数时新增的标签会生成对应的标签id，不指定时则以目前最大的id自增。
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
}
