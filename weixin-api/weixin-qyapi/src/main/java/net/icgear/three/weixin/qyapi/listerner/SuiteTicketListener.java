package net.icgear.three.weixin.qyapi.listerner;

import javax.enterprise.context.ApplicationScoped;

import com.suisrc.core.reference.RefVal;
import com.suisrc.three.core.ThreeBinding;
import com.suisrc.three.core.listener.inf.Listener;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.suite.TicketSuite;

/**
 * 推送suite_ticket监听
 * 
 * 企业号在使用微信api接口时候需要suite_ticket,所以这里监听回调监听内容
 * 
 * 这里值给默认实现，同时会对所有的rest接口的内容产生监听，监听是强制增加的
 * 加载等级为等级为"R"， 加载等级低于标准的"N"级别
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class SuiteTicketListener implements Listener<TicketSuite> {
    
    /**
     * 该方法内容废弃，使用accept(Refval<Object>, TicketSuite, Object)代替其实现
     */
    @Deprecated
    @Override
    public boolean accept(RefVal<Object> result, TicketSuite bean) {
        return false;
    }    
    
    /**
     * 监听到的ticket同时对内容进行处理
     */
    @Override
    public boolean accept(RefVal<Object> result, TicketSuite bean, Object owner) {
        if (owner instanceof ThreeBinding) {
            // 将监听到的suite_ticket传入绑定控制器中。
            ThreeBinding binding = (ThreeBinding) owner;
            binding.setAdapter(null, TicketSuite.class, bean);
        }
        result.set(WxConsts.SUCCESS);
        return false;
    }

    @Override
    public String priority(TicketSuite bean) {
        return "R";
    }
    
}
