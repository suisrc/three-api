package net.icgear.three.weixin.qyapi.rest.dto.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 文本卡片消息
 * 
 * 请求包体：
 * {
 *    "touser" : "UserID1|UserID2|UserID3",
 *    "toparty" : "PartyID1 | PartyID2",
 *    "totag" : "TagID1 | TagID2",
 *    "msgtype" : "textcard",
 *    "agentid" : 1,
 *    "textcard" : {
 *             "title" : "领奖通知",
 *             "description" : "<div class=\"gray\">2016年9月26日</div> <div class=\"normal\">恭喜你抽中iPhone 7一台，领奖码：xxxx</div><div class=\"highlight\">请于2016年10月10日前联系行政同事领取</div>",
 *             "url" : "URL",
 *             "btntxt":"更多"
 *    }
 * }
 * 
 * 参数说明：
 * 参数.                必须.       说明
 * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
 * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
 * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
 * msgtype              是.        消息类型，此时固定为：textcard
 * agentid              是.        企业应用的id，整型。可在应用的设置页面查看
 * title                是.        标题，不超过128个字节，超过会自动截断
 * description          是.        描述，不超过512个字节，超过会自动截断
 * url                  是.        点击后跳转的链接。
 * btntxt               否.        按钮文字。
 * 
 * 特殊说明：
 * 卡片消息的展现形式非常灵活，支持使用br标签或者空格来进行换行处理，也支持使用div标签来使用不同的字体颜色，
 * 目前内置了3种文字颜色：灰色(gray)、高亮(highlight)、默认黑色(normal)，将其作为div标签的class属性即可，具体用法请参考上面的示例。
 * @author Y13
 *
 */
@ApiModel("SendBody.TextcardMessage")
public class TextcardMessage implements IMessage {
    
    @Override
    public String type() {
        return "textcard";
    }

    @ApiModelProperty("标题，不超过128个字节，超过会自动截断")
    @NotNull("title属性为空")
    @JsonProperty("title")
    private String title;

    @ApiModelProperty("描述，不超过512个字节，超过会自动截断")
    @NotNull("description属性为空")
    @JsonProperty("description")
    private String description;

    @ApiModelProperty("点击后跳转的链接。")
    @NotNull("url属性为空")
    @JsonProperty("url")
    private String url;

    @ApiModelProperty("按钮文字。")
    @JsonProperty("btntxt")
    private String btntxt;

    /**
     * 标题，不超过128个字节，超过会自动截断
     */
    public String getTitle() {
        return title;
    }

    /**
     * 标题，不超过128个字节，超过会自动截断
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 描述，不超过512个字节，超过会自动截断
     */
    public String getDescription() {
        return description;
    }

    /**
     * 描述，不超过512个字节，超过会自动截断
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 点击后跳转的链接。
     */
    public String getUrl() {
        return url;
    }

    /**
     * 点击后跳转的链接。
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * 按钮文字。
     */
    public String getBtntxt() {
        return btntxt;
    }

    /**
     * 按钮文字。
     */
    public void setBtntxt(String btntxt) {
        this.btntxt = btntxt;
    }
}
