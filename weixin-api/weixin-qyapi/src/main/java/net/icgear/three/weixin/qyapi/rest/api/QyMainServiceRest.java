package net.icgear.three.weixin.qyapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsConsts;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.annotation.WxApi;
import net.icgear.three.weixin.core.bean.WxAccessToken;

/**
 * 公众号可以使用AppID和AppSecret调用本接口来获取access_token。
 * 
 * AppID和AppSecret可在微信公众平台官网-开发页中获得（需要已经成为开发者，且帐号没有异常状态）。
 * 
 * 注意调用所有微信接口时均需使用https协议。如果第三方不使用中控服务器，而是选择各个业务逻辑点各 自去刷新access_token，那么就可能会产生冲突，导致服务不稳定。
 * 
 * 注意，该接口只提供给本公司绑定的激活器使用，如果是应用，请使用ThreeServiceRest
 * 
 * @author Y13
 *
 */
@WxApi(types=WxConsts.QM)
@RemoteApi("cgi-bin")
public interface QyMainServiceRest {
    final String NAMED = JaxrsConsts.separator + "net.icgear.three.weixin.qyapi.rest.api.QyMainServiceRest";

    /**
     * https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=ID&corpsecret=SECRECT
     * 
     * @param appid 是必须, 第三方用户唯一凭证
     * @param secret 是必须, 第三方用户唯一凭证密钥，即appsecret
     * @return
     */
    @GET
    @Path("gettoken")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxAccessToken getToken(
            @QueryParam("corpid")@NotNull("应用主键为空") String appid, 
            @QueryParam("corpsecret")@NotNull("应用秘钥为空") String secret);
}
