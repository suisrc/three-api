package net.icgear.three.weixin.qyapi.rest.dto.external;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 本文属性
 * @author Y13
 *
 */
@ApiModel("ExternalProfile.MiniprogramExternalAttr")
public class MiniprogramExternalAttr extends ExternalAttr {
    
    @ApiModel("ExternalProfile.MiniprogramExternalAttr.Miniprogram")
    @JsonInclude(Include.NON_NULL) 
    public static class Miniprogram {
        
        @ApiModelProperty("小程序appid，必须是有在本企业安装授权的小程序，否则会被忽略")
        @JsonProperty("appid")
        private String appid;
        
        @ApiModelProperty("小程序的展示标题,长度限制12个UTF8字符")
        @JsonProperty("title")
        private String title;
        
        @ApiModelProperty("小程序的页面路径")
        @JsonProperty("pagepath")
        private String pagepath;

        /**
         * 小程序appid，必须是有在本企业安装授权的小程序，否则会被忽略"
         * @return
         */
        public String getAppid() {
            return appid;
        }

        /**
         * 小程序appid，必须是有在本企业安装授权的小程序，否则会被忽略"
         * @param appid
         */
        public void setAppid(String appid) {
            this.appid = appid;
        }

        /**
         * 小程序的展示标题,长度限制12个UTF8字符
         * @return
         */
        public String getTitle() {
            return title;
        }

        /**
         * 小程序的展示标题,长度限制12个UTF8字符
         * @param title
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /**
         * 小程序的页面路径
         * @return
         */
        public String getPagepath() {
            return pagepath;
        }

        /**
         * 小程序的页面路径
         * @param pagepath
         */
        public void setPagepath(String pagepath) {
            this.pagepath = pagepath;
        }
        
    }

    @ApiModelProperty("小程序类型的属性，appid和title字段要么同时为空表示清除改属性，要么同时不为空")
    @JsonProperty("miniprogram")
    private Miniprogram miniprogram;

    /**
     * 小程序类型的属性，appid和title字段要么同时为空表示清除改属性，要么同时不为空
     * @return
     */
    public Miniprogram getMiniprogram() {
        return miniprogram;
    }

    /**
     * 小程序类型的属性，appid和title字段要么同时为空表示清除改属性，要么同时不为空
     * @param miniprogram
     */
    public void setMiniprogram(Miniprogram miniprogram) {
        this.miniprogram = miniprogram;
    }
}
