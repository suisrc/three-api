package net.icgear.three.weixin.qyapi.rest.dto.external;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("ExternalProfile.ExternalAttr")
@JsonInclude(Include.NON_NULL)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property="type", visible=true)
@JsonSubTypes({
    @JsonSubTypes.Type(value=TextExternalAttr.class,name = "0"),
    @JsonSubTypes.Type(value=WebExternalAttr.class,name = "1"),
    @JsonSubTypes.Type(value=MiniprogramExternalAttr.class,name = "2")
})
public abstract class ExternalAttr {
    
    @ApiModelProperty("属性类型: 0-本文 1-网页 2-小程序")
    @JsonProperty("type")
    private Integer type;
    
    @ApiModelProperty("属性名称： 需要先确保在管理端有创建改属性，否则会忽略")
    @JsonProperty("name")
    private String name;
    
    @SuppressWarnings("unchecked")
    public <T> T as(Class<T> type) {
        if (type.isAssignableFrom(getClass())) {
            return (T) this;
        }
        return null;
    }
    
    /**
     * 属性类型: 0-本文 1-网页 2-小程序
     * @return
     */
    public Integer getType() {
        return type;
    }
    
    /**
     * 属性类型: 0-本文 1-网页 2-小程序
     * @param type
     */
    public void setType(Integer type) {
        this.type = type;
    }
    
    /**
     * 属性名称： 需要先确保在管理端有创建改属性，否则会忽略
     * @return
     */
    public String getName() {
        return name;
    }
    
    /**
     * 属性名称： 需要先确保在管理端有创建改属性，否则会忽略
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
}