package net.icgear.three.weixin.qyapi.rest.dto.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 图文消息
 * 
 * 请求包体：
 * {
 *    "touser" : "UserID1|UserID2|UserID3",
 *    "toparty" : "PartyID1 | PartyID2",
 *    "totag": "TagID1 | TagID2",
 *    "msgtype" : "mpnews",
 *    "agentid" : 1,
 *    "mpnews" : {
 *        "articles":[
 *            {
 *                "title": "Title", 
 *                "thumb_media_id": "MEDIA_ID",
 *                "author": "Author",
 *                "content_source_url": "URL",
 *                "content": "Content",
 *                "digest": "Digest description"
 *             }
 *        ]
 *    },
 *    "safe":0
 * }
 * 
 * 参数说明：
 * 参数.                必须.       说明
 * touser               否.        成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向关注该企业应用的全部成员发送
 * toparty              否.        部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
 * totag                否.        标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
 * msgtype              是.        消息类型，此时固定为：mpnews
 * agentid              是.        企业应用的id，整型。可在应用的设置页面查看
 * articles             是.        图文消息，一个图文消息支持1到8条图文
 * title                是.        标题，不超过128个字节，超过会自动截断
 * thumb_media_id       是.        图文消息缩略图的media_id,
 * author               否.        图文消息的作者，不超过64个字节
 * content_source_url   否.        图文消息点击“阅读原文”之后的页面链接
 * content              是.        图文消息的内容，支持html标签，不超过666
 * digest               否.        图文消息的描述，不超过512个字节，超过会自动截断
 * safe                 否.        表示是否是保密消息，0表示可对外分享，1表示不能分享且内容显示水印，2表示仅限在企业内分享，默认为0；注意仅mpnews类型的消息支持safe值为2，其他消息类型不支持
 * 
 * 
 * @author Y13
 *
 */
@ApiModel("SendBody.MpnewsMessage")
public class MpnewsMessage implements IMessage {
    
    public String type() {
        return "mpnews";
    }; 

    @JsonInclude(Include.NON_NULL)
    @ApiModel("SendBody.MpnewsMessage.Articles")
    public static class Articles {
        
        @ApiModelProperty("标题，不超过128个字节，超过会自动截断")
        @NotNull("title属性为空")
        @JsonProperty("title")
        private String title;
        
        @ApiModelProperty("图文消息缩略图的media_id,")
        @NotNull("thumb_media_id属性为空")
        @JsonProperty("thumb_media_id")
        private String thumbMediaId;
        
        @ApiModelProperty("图文消息的作者，不超过64个字节")
        @JsonProperty("author")
        private String author;
        
        @ApiModelProperty("图文消息点击“阅读原文”之后的页面链接")
        @JsonProperty("content_source_url")
        private String contentSourceUrl;
        
        @ApiModelProperty("图文消息的内容，支持html标签，不超过666")
        @NotNull("content属性为空")
        @JsonProperty("content")
        private String content;
        
        @ApiModelProperty("图文消息的描述，不超过512个字节，超过会自动截断")
        @JsonProperty("digest")
        private String digest;

        /**
         * 标题，不超过128个字节，超过会自动截断
         */
        public String getTitle() {
            return title;
        }

        /**
         * 标题，不超过128个字节，超过会自动截断
         */
        public void setTitle(String title) {
            this.title = title;
        }

        /**
         * 图文消息缩略图的media_id,
         */
        public String getThumbMediaId() {
            return thumbMediaId;
        }

        /**
         * 图文消息缩略图的media_id,
         */
        public void setThumbMediaId(String thumbMediaId) {
            this.thumbMediaId = thumbMediaId;
        }

        /**
         * 图文消息的作者，不超过64个字节
         */
        public String getAuthor() {
            return author;
        }

        /**
         * 图文消息的作者，不超过64个字节
         */
        public void setAuthor(String author) {
            this.author = author;
        }

        /**
         * 图文消息点击“阅读原文”之后的页面链接
         */
        public String getContentSourceUrl() {
            return contentSourceUrl;
        }

        /**
         * 图文消息点击“阅读原文”之后的页面链接
         */
        public void setContentSourceUrl(String contentSourceUrl) {
            this.contentSourceUrl = contentSourceUrl;
        }

        /**
         * 图文消息的内容，支持html标签，不超过666
         */
        public String getContent() {
            return content;
        }

        /**
         * 图文消息的内容，支持html标签，不超过666
         */
        public void setContent(String content) {
            this.content = content;
        }

        /**
         * 图文消息的描述，不超过512个字节，超过会自动截断
         */
        public String getDigest() {
            return digest;
        }

        /**
         * 图文消息的描述，不超过512个字节，超过会自动截断
         */
        public void setDigest(String digest) {
            this.digest = digest;
        }
    }

    @ApiModelProperty("图文消息，一个图文消息支持1到8条图文")
    @NotNull("articles属性为空")
    @JsonProperty("articles")
    private Articles[] articles;

    /**
     * 图文消息，一个图文消息支持1到8条图文
     */
    public Articles[] getArticles() {
        return articles;
    }

    /**
     * 图文消息，一个图文消息支持1到8条图文
     */
    public void setArticles(Articles[] articles) {
        this.articles = articles;
    }
}
