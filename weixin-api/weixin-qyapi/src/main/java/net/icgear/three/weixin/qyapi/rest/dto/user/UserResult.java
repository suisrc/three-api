package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.qyapi.rest.dto.external.Extattr;
import net.icgear.three.weixin.qyapi.rest.dto.external.ExternalProfile;

/**
 * 读取成员 在通讯录同步助手中此接口可以读取企业通讯录的所有成员信息，而自建应用可以读取该应用设置的可见范围内的成员信息。
 * 
 * @author Y13
 *
 */
@ApiModel("UserResult")
public class UserResult extends WxErrCode {
    private static final long serialVersionUID = 5366459359312200247L;

    @ApiModelProperty("成员UserID。对应管理端的帐号")
    @JsonProperty("userid")
    private String userid;
    
    @ApiModelProperty("成员名称")
    @JsonProperty("name")
    private String name;
    
    @ApiModelProperty("成员所属部门id列表")
    @JsonProperty("department")
    private Integer[] department;
    
    @ApiModelProperty("部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0,")
    @JsonProperty("order")
    private Integer[] order;
    
    @ApiModelProperty("职位信息；第三方仅通讯录应用可获取")
    @JsonProperty("position")
    private String position;
    
    @ApiModelProperty("手机号码，第三方仅通讯录应用可获取")
    @JsonProperty("mobile")
    private String mobile;
    
    @ApiModelProperty("手机号码，隐藏手机号码")
    @JsonProperty("hide_mobile")
    private String hideMobile;
    
    @ApiModelProperty("别名；第三方仅通讯录应用可获取")
    @JsonProperty("alias")
    private String alias;
    
    @ApiModelProperty("性别。0表示未定义，1表示男性，2表示女性")
    @JsonProperty("gender")
    private String gender;
    
    @ApiModelProperty("邮箱，第三方仅通讯录套件可获取")
    @JsonProperty("email")
    private String email;
    
    @ApiModelProperty("上级字段，标识是否为上级；第三方仅通讯录应用可获取")
    @JsonProperty("isleader")
    private Integer isleader;
    
    @ApiModelProperty("头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。第三方仅通讯录应用可获取")
    @JsonProperty("avatar")
    private String avatar;
    
    @ApiModelProperty("座机。第三方仅通讯录应用可获取")
    @JsonProperty("telephone")
    private String telephone;
    
    @ApiModelProperty("成员启用状态。1表示启用的成员，0表示被禁用。注意，服务商调用接口不会返回此字段")
    @JsonProperty("enable")
    private Integer enable;
    
    @ApiModelProperty("英文名；第三方仅通讯录应用可获取")
    @JsonProperty("english_name")
    private String englishName;
    
    @ApiModelProperty("扩展属性，第三方仅通讯录应用可获取")
    @JsonProperty("extattr")
    private Extattr extattr;
    
    @ApiModelProperty("激活状态:")
    @JsonProperty("status")
    private Integer status;
    
    @ApiModelProperty("员工个人二维码，扫描可添加为外部联系人；第三方仅通讯录应用可获取")
    @JsonProperty("qr_code")
    private String qrCode;
    
    @ApiModelProperty("成员对外属性，字段详情见对外属性；第三方仅通讯录应用可获取")
    @JsonProperty("external_profile")
    private ExternalProfile externalProfile;

    /**
     * 成员UserID。对应管理端的帐号
     */
    public String getUserid() {
        return userid;
    }

    /**
     * 成员UserID。对应管理端的帐号
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * 成员名称
     */
    public String getName() {
        return name;
    }

    /**
     * 成员名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 成员所属部门id列表
     */
    public Integer[] getDepartment() {
        return department;
    }

    /**
     * 成员所属部门id列表
     */
    public void setDepartment(Integer[] department) {
        this.department = department;
    }

    /**
     * 部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0,
     */
    public Integer[] getOrder() {
        return order;
    }

    /**
     * 部门内的排序值，默认为0。数量必须和department一致，数值越大排序越前面。值范围是[0,
     */
    public void setOrder(Integer[] order) {
        this.order = order;
    }

    /**
     * 职位信息；第三方仅通讯录应用可获取
     */
    public String getPosition() {
        return position;
    }

    /**
     * 职位信息；第三方仅通讯录应用可获取
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * 手机号码，第三方仅通讯录应用可获取
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 手机号码，第三方仅通讯录应用可获取
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 性别。0表示未定义，1表示男性，2表示女性
     */
    public String getGender() {
        return gender;
    }

    /**
     * 性别。0表示未定义，1表示男性，2表示女性
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * 邮箱，第三方仅通讯录套件可获取
     */
    public String getEmail() {
        return email;
    }

    /**
     * 邮箱，第三方仅通讯录套件可获取
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 上级字段，标识是否为上级；第三方仅通讯录应用可获取
     */
    public Integer getIsleader() {
        return isleader;
    }

    /**
     * 上级字段，标识是否为上级；第三方仅通讯录应用可获取
     */
    public void setIsleader(Integer isleader) {
        this.isleader = isleader;
    }

    /**
     * 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。第三方仅通讯录应用可获取
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。第三方仅通讯录应用可获取
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 座机。第三方仅通讯录应用可获取
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * 座机。第三方仅通讯录应用可获取
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    /**
     * 成员启用状态。1表示启用的成员，0表示被禁用。注意，服务商调用接口不会返回此字段
     */
    public Integer getEnable() {
        return enable;
    }

    /**
     * 成员启用状态。1表示启用的成员，0表示被禁用。注意，服务商调用接口不会返回此字段
     */
    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    /**
     * 英文名；第三方仅通讯录应用可获取
     */
    public String getEnglishName() {
        return englishName;
    }

    /**
     * 英文名；第三方仅通讯录应用可获取
     */
    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    /**
     * 扩展属性，第三方仅通讯录应用可获取
     */
    public Extattr getExtattr() {
        return extattr;
    }

    /**
     * 扩展属性，第三方仅通讯录应用可获取
     */
    public void setExtattr(Extattr extattr) {
        this.extattr = extattr;
    }

    /**
     * 激活状态:
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * 激活状态:
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 员工个人二维码，扫描可添加为外部联系人；第三方仅通讯录应用可获取
     */
    public String getQrCode() {
        return qrCode;
    }

    /**
     * 员工个人二维码，扫描可添加为外部联系人；第三方仅通讯录应用可获取
     */
    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    /**
     * 成员对外属性，字段详情见对外属性；第三方仅通讯录应用可获取
     */
    public ExternalProfile getExternalProfile() {
        return externalProfile;
    }

    /**
     * 成员对外属性，字段详情见对外属性；第三方仅通讯录应用可获取
     */
    public void setExternalProfile(ExternalProfile externalProfile) {
        this.externalProfile = externalProfile;
    }

    public String getHideMobile() {
        return hideMobile;
    }

    public void setHideMobile(String hideMobile) {
        this.hideMobile = hideMobile;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
    
}
