package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 第三方使用user_ticket获取成员详情
 * 
 * @author Y13
 *
 */
@ApiModel("UserDetailResult")
public class UserDetailResult extends WxErrCode {
    private static final long serialVersionUID = -4389891563174846673L;

    @ApiModelProperty("用户所属企业的corpid")
    @JsonProperty("corpid")
    private String corpid;
    
    @ApiModelProperty("成员UserID")
    @JsonProperty("userid")
    private String userid;
    
    @ApiModelProperty("成员姓名")
    @JsonProperty("name")
    private String name;
    
    @ApiModelProperty("成员手机号，仅在用户同意snsapi_privateinfo授权时返回")
    @JsonProperty("mobile")
    private String mobile;
    
    @ApiModelProperty("性别。0表示未定义，1表示男性，2表示女性")
    @JsonProperty("gender")
    private int gender;
    
    @ApiModelProperty("成员邮箱，仅在用户同意snsapi_privateinfo授权时返回")
    @JsonProperty("email")
    private String email;
    
    @ApiModelProperty("头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。仅在用户同意snsapi_privateinfo授权时返回")
    @JsonProperty("avatar")
    private String avatar;
    
    @ApiModelProperty("员工个人二维码（扫描可添加为外部联系人），仅在用户同意snsapi_privateinfo授权时返回")
    @JsonProperty("qr_code")
    private String qrCode;

    public String getCorpid() {
        return corpid;
    }

    public void setCorpid(String corpid) {
        this.corpid = corpid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 头像url。注：如果要获取小图将url最后的”/0”改成”/100”即可。仅在用户同意snsapi_privateinfo授权时返回
     * @return
     */
    public String getAvatarSmall() {
        if (avatar == null || !avatar.endsWith("/0")) {
            return null;
        }
        return avatar.substring(0, avatar.length() - 2) + "/100";
    }

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
}
