package net.icgear.three.weixin.qyapi.rest.dto.external;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("Extattr")
public class Extattr {
    
    @ApiModel("Extattr.Attrs")
    public static class Attrs {

        @JsonProperty("name")
        private String name;

        @JsonProperty("value")
        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    @JsonProperty("attrs")
    private Attrs[] attrs;

    public Attrs[] getAttrs() {
        return attrs;
    }

    public void setAttrs(Attrs[] attrs) {
        this.attrs = attrs;
    }
}