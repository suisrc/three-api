package net.icgear.three.weixin.qyapi;

import java.util.function.Supplier;

import com.suisrc.core.Global;

/**
 * 企业微信号API接口调用辅助器
 * 
 * 用于辅助调用接口使用
 * @author Y13
 *
 */
public class QyApiHelper {

    /**
     * Call Api
     * 
     * 用于调用微信api使用
     * 
     * 我们在调用api时候，需要执行调用的企业号，这个使用通过该方法可以实现快速调用
     * 
     * 注意：api递归调用深度不能超过1000层
     * 
     * @param corpId
     * @param api
     * @return
     */
    public static <T> T call(String corpId, Supplier<T> api) {
        return Global.execByTempValue(QyConsts.APP_CORP_ID, corpId, api::get);
    }
    
    /**
     * 发送消息时候，对多用户进行拼接
     */
    public static String joinStr(String split, String... strs) {
        if (strs.length == 0) {
            return null;
        }
        if (strs.length == 1) {
            return strs[0];
        }
        StringBuilder sb = new StringBuilder();
        for (String str : strs) {
            sb.append(str).append('|');
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }
}
