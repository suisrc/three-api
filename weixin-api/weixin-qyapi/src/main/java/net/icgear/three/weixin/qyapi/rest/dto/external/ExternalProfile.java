package net.icgear.three.weixin.qyapi.rest.dto.external;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@JsonInclude(Include.NON_NULL) 
@ApiModel("ExternalProfile")
public class ExternalProfile {
    
    @ApiModelProperty("属性列表，目前支持文本、网页、小程序（需参与小程序内测）三种类型")
    @JsonProperty("external_attr")
    private ExternalAttr[] externalAttr;
    
    /**
     * 属性列表，目前支持文本、网页、小程序（需参与小程序内测）三种类型
     * @return
     */
    public ExternalAttr[] getExternalAttr() {
        return externalAttr;
    }
    
    /**
     * 属性列表，目前支持文本、网页、小程序（需参与小程序内测）三种类型
     * @param externalAttr
     */
    public void setExternalAttr(ExternalAttr[] externalAttr) {
        this.externalAttr = externalAttr;
    }
}