package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 第三方根据code获取企业成员信息
 * @author Y13
 *
 */
@ApiModel("UserInfoResult")
public class UserInfoResult extends WxErrCode {
    private static final long serialVersionUID = 6302581418740397155L;
    
    @ApiModelProperty("用户所属企业的corpid")
    @JsonProperty("CorpId")
    private String corpId;
    
    @ApiModelProperty("用户在企业内的UserID，如果该企业与第三方应用有授权关系时，返回明文UserId，否则返回密文UserId")
    @JsonProperty("UserId")
    private String userId;
    
    @ApiModelProperty("手机设备号(由企业微信在安装时随机生成，删除重装会改变，升级不受影响)")
    @JsonProperty("DeviceId")
    private String deviceId;
    
    @ApiModelProperty(
            "成员票据，最大为512字节。scope为snsapi_userinfo或snsapi_privateinfo，且用户在应用可见范围之内时返回此参数。"
            + "后续利用该参数可以获取用户信息或敏感信息，参见“第三方使用user_ticket获取成员详情”。")
    @JsonProperty("user_ticket")
    private String userTicket;
    
    @ApiModelProperty("user_ticket的有效时间（秒），随user_ticket一起返回")
    @JsonProperty("expires_in")
    private String expiresIn;
    
    /**
     * 当用户不属于任何一家公司的时候，userid和corpid无效，openid有效，同时user_ticket和expires_in也不存在
     */
    @ApiModelProperty("非企业成员的标识，对当前服务商唯一")
    @JsonProperty("OpenId")
    private String openId;

    public String getCorpId() {
        return corpId;
    }

    public void setCorpId(String corpId) {
        this.corpId = corpId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserTicket() {
        return userTicket;
    }

    public void setUserTicket(String userTicket) {
        this.userTicket = userTicket;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(String expiresIn) {
        this.expiresIn = expiresIn;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
    
}

