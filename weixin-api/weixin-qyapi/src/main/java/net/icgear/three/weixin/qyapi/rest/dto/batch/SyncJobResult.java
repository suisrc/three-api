package net.icgear.three.weixin.qyapi.rest.dto.batch;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 增量更新成员
 * 
 * @author Y13
 *
 */
@ApiModel("SyncJobResult")
public class SyncJobResult extends WxErrCode {
    private static final long serialVersionUID = 6669136360859315271L;

    @ApiModelProperty("异步任务id，最大长度为64字节")
    @JsonProperty("jobid")
    private String jobid;

    /**
     * 异步任务id，最大长度为64字节
     */
    public String getJobid() {
        return jobid;
    }

    /**
     * 异步任务id，最大长度为64字节
     */
    public void setJobid(String jobid) {
        this.jobid = jobid;
    }
}
