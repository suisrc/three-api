package net.icgear.three.weixin.qyapi.rest.dto.three;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 获取第三方应用凭证返回值
 * @author Y13
 *
 */
@ApiModel("SuiteTokenResult")
public class SuiteTokenResult extends WxErrCode {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("第三方应用access_token,最长为512字节")
    @JsonProperty("suite_access_token")
    private String suiteAccessToken;
    
    @ApiModelProperty("有效期")
    @JsonProperty("expires_in")
    private long expiresIn = -1;

    public String getSuiteAccessToken() {
        return suiteAccessToken;
    }

    public void setSuiteAccessToken(String suiteAccessToken) {
        this.suiteAccessToken = suiteAccessToken;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(long expiresIn) {
        this.expiresIn = expiresIn;
    }
    
}
