package net.icgear.three.weixin.qyapi.rest.dto.department;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 创建部门
 * @author Y13
 *
 */
@ApiModel("CreateDepartmentResult")
public class CreateDepartmentResult extends WxErrCode {
    private static final long serialVersionUID = -197188621458564581L;

    @ApiModelProperty("创建的部门id")
    @JsonProperty("id")
    private Integer id;

    /**
     * 创建的部门id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 创建的部门id
     */
    public void setId(Integer id) {
        this.id = id;
    }
}
