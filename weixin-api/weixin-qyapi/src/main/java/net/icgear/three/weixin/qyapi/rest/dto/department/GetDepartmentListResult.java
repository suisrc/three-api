package net.icgear.three.weixin.qyapi.rest.dto.department;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 获取部门列表
 * 
 * @author Y13
 *
 */
@ApiModel("GetDepartmentListResult")
public class GetDepartmentListResult extends WxErrCode {
    private static final long serialVersionUID = 5931135861629822452L;

    @ApiModel("GetDepartmentListResult.Department")
    public static class Department {
        @ApiModelProperty("创建的部门id")
        @JsonProperty("id")
        private Integer id;

        @ApiModelProperty("部门名称")
        @JsonProperty("name")
        private String name;

        @ApiModelProperty("父亲部门id。根部门为1")
        @JsonProperty("parentid")
        private Integer parentid;

        @ApiModelProperty("在父部门中的次序值。order值大的排序靠前。值范围是[0,")
        @JsonProperty("order")
        private Integer order;

        /**
         * 创建的部门id
         */
        public Integer getId() {
            return id;
        }

        /**
         * 创建的部门id
         */
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * 部门名称
         */
        public String getName() {
            return name;
        }

        /**
         * 部门名称
         */
        public void setName(String name) {
            this.name = name;
        }

        /**
         * 父亲部门id。根部门为1
         */
        public Integer getParentid() {
            return parentid;
        }

        /**
         * 父亲部门id。根部门为1
         */
        public void setParentid(Integer parentid) {
            this.parentid = parentid;
        }

        /**
         * 在父部门中的次序值。order值大的排序靠前。值范围是[0,
         */
        public Integer getOrder() {
            return order;
        }

        /**
         * 在父部门中的次序值。order值大的排序靠前。值范围是[0,
         */
        public void setOrder(Integer order) {
            this.order = order;
        }
    }


    @ApiModelProperty("部门列表数据。")
    @JsonProperty("department")
    private Department[] department;

    /**
     * 部门列表数据。
     */
    public Department[] getDepartment() {
        return department;
    }

    /**
     * 部门列表数据。
     */
    public void setDepartment(Department[] department) {
        this.department = department;
    }
}
