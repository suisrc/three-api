package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("BatchdeleteUserBody")
public class BatchdeleteUserBody {
    
    @ApiModelProperty("成员UserID列表。对应管理端的帐号。最多支持200个。若存在无效UserID，直接返回错误")
    @NotNull("useridlist属性为空")
    @JsonProperty("useridlist")
    private String[] useridlist;
    /**
     * 成员UserID列表。对应管理端的帐号。最多支持200个。若存在无效UserID，直接返回错误
     */
    public String[] getUseridlist() {
        return useridlist;
    }
    /**
     * 成员UserID列表。对应管理端的帐号。最多支持200个。若存在无效UserID，直接返回错误
     */
    public void setUseridlist(String[] useridlist) {
        this.useridlist = useridlist;
    }
}