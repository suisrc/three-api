package net.icgear.three.weixin.qyapi.retry;

import com.suisrc.jaxrsapi.core.ApiActivator;

import net.icgear.three.weixin.core.filter.WxTokenReTryFilter;
import net.icgear.three.weixin.qyapi.QyConsts;

/**
 * 对微信中的Access Token 有效性记性验证 系统很容易形成恶性竞争，所以请谨慎使用。
 * 
 * @author Y13
 *
 */
public class WxSuiteTokenReTryFilter extends WxTokenReTryFilter {

    public WxSuiteTokenReTryFilter(ApiActivator activator) {
        super(activator);
    }
    
    protected String getTokenKey() {
        return QyConsts.SUITE_ACCESS_TOKEN;
    }
}
