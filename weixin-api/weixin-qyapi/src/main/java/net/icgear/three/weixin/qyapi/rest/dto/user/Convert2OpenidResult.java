package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * userid转openid
 * @author Y13
 *
 */
@ApiModel("Convert2OpenidResult")
public class Convert2OpenidResult extends WxErrCode {
    private static final long serialVersionUID = 659220028891800082L;
    
    @ApiModelProperty("企业微信成员userid对应的openid")
    @JsonProperty("openid")
    private String openid;
    
    /**
     * 企业微信成员userid对应的openid
     */
    public String getOpenid() {
        return openid;
    }
    /**
     * 企业微信成员userid对应的openid
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }
}