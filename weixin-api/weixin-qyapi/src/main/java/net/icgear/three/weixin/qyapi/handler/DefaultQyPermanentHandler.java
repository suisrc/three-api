package net.icgear.three.weixin.qyapi.handler;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.suisrc.core.fasterxml.FF;
import com.suisrc.core.fasterxml.FasterFactory;
import com.suisrc.core.fasterxml.FasterFactory.Type;
import com.suisrc.core.utils.FileUtils;

import net.icgear.three.weixin.core.AbstractWeixinActivator;
import net.icgear.three.weixin.core.handler.CallbackActivatorHandler;
import net.icgear.three.weixin.qyapi.rest.dto.three.PermanentCodeResult;

/**
 * 
 * 该内容作为临时数据内容，放弃注入方式加载
 * 
 * @author Y13
 *
 */
//@Dependent
public class DefaultQyPermanentHandler implements PermanentHandler, CallbackActivatorHandler {
    
    /**
     * 所属的激活器
     */
    private AbstractWeixinActivator activator;


    @Override
    public void setActivator(AbstractWeixinActivator activator) {
        this.activator = activator;
    }
    
    /**
     * 缓存
     */
    private Map<String, PermanentCodeResult> corpInfos = null;
    
    /**
     * 
     * @return
     */
    private Map<String, PermanentCodeResult> getCorpInfos() {
        return corpInfos != null ? corpInfos : (corpInfos = new HashMap<>());
    }
    
    /**
     * 获取存储的文件夹名称
     * @return
     */
    private String getFolderName() {
        // 备用处理方式
        String folder = "data/public/corp_permanent_code/" + activator.getAppUniqueKey() + "/";
        String key = activator.getRequestAppKey();
        if (key != null) {
            folder += key + "/";
        }
        return folder;
    }

    /**
     * 安装时候回调，设定公司信息
     */
    @Override
    public void setPermanentCodeResult(PermanentCodeResult result) {
        FasterFactory f = FF.getDefault();
        String content = f.convert2String(f.getObjectMapper(Type.JSON), true, result);
        String foldername = getFolderName();
        File folder = new File(foldername);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        String corpId = result.getAuthCorpInfo().getCorpId();
        String filename = foldername + corpId;
        FileUtils.writeConent(filename, content);
        // 放入缓存中
        getCorpInfos().put(filename, result);
    }

    /**
     * 获取公司永久授权码
     */
    @Override
    public String getPermanentCode(String corpId) {
        PermanentCodeResult result = getCorpInfo(corpId);
        if (result == null) {
            return null;
        }
        return result.getPermanentCode();
    }

    /**
     * 获取应用agentid
     */
    @Override
    public Integer getAgentId(String corpId) {
        PermanentCodeResult result = getCorpInfo(corpId);
        if (result == null) {
            return null;
        }
        return result.getAuthInfo().getAgent()[0].getAgentId();
    }

    /**
     * 获取公司信息
     * @param corpId
     * @return
     */
    private PermanentCodeResult getCorpInfo(String corpId) {
        PermanentCodeResult result = getCorpInfos().get(corpId);
        if (result != null) {
            return result;
        }
        // 备用处理方式
        String filename = getFolderName() + corpId;
        String content = FileUtils.readContent(filename);
        if (content == null) {
            return null;
        }
        result = FF.getDefault().string2Bean(content, PermanentCodeResult.class, Type.JSON);
        if (result == null) {
            return null;
        }
        getCorpInfos().put(filename, result);
        return result;
    }

    /**
     * 通用适配器， 用于扩展
     */
    @Override
    public String getAdapter(String corpId, String key, Class<?> type) {
        return null;
    }

}
