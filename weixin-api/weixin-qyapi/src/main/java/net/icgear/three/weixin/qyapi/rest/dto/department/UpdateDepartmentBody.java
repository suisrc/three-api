package net.icgear.three.weixin.qyapi.rest.dto.department;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 更新部门
 * 
 * @author Y13
 *
 */
@ApiModel("UpdateDepartmentBody")
public class UpdateDepartmentBody {

    @ApiModelProperty("部门id")
    @NotNull("id属性为空")
    @JsonProperty("id")
    private Integer id;

    @ApiModelProperty("部门名称。长度限制为1~32个字符，字符不能包括\\:?”<>｜")
    @JsonProperty("name")
    private String name;

    @ApiModelProperty("父部门id")
    @JsonProperty("parentid")
    private Integer parentid;

    @ApiModelProperty("在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,")
    @JsonProperty("order")
    private Integer order;

    /**
     * 部门id
     */
    public Integer getId() {
        return id;
    }

    /**
     * 部门id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 部门名称。长度限制为1~32个字符，字符不能包括\:?”&lt;&gt;｜
     */
    public String getName() {
        return name;
    }

    /**
     * 部门名称。长度限制为1~32个字符，字符不能包括\:?”&lt;&gt;｜
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 父部门id
     */
    public Integer getParentid() {
        return parentid;
    }

    /**
     * 父部门id
     */
    public void setParentid(Integer parentid) {
        this.parentid = parentid;
    }

    /**
     * 在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,
     */
    public Integer getOrder() {
        return order;
    }

    /**
     * 在父部门中的次序值。order值大的排序靠前。有效的值范围是[0,
     */
    public void setOrder(Integer order) {
        this.order = order;
    }
}
