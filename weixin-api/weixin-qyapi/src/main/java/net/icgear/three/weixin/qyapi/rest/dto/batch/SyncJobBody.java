package net.icgear.three.weixin.qyapi.rest.dto.batch;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 增量更新成员
 * 
 * @author Y13
 *
 */
@ApiModel("SyncJobBody")
public class SyncJobBody {
    @ApiModel("SyncJobBody.Callback")
    public static class Callback {
        
        @ApiModelProperty("企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议")
        @JsonProperty("url")
        private String url;
        
        @ApiModelProperty("用于生成签名")
        @JsonProperty("token")
        private String token;
        
        @ApiModelProperty("用于消息体的加密，是AES密钥的Base64编码")
        @JsonProperty("encodingaeskey")
        private String encodingaeskey;
        
        /**
         * 企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
         */
        public String getUrl() {
            return url;
        }
        /**
         * 企业应用接收企业微信推送请求的访问协议和地址，支持http或https协议
         */
        public void setUrl(String url) {
            this.url = url;
        }
        /**
         * 用于生成签名
         */
        public String getToken() {
            return token;
        }
        /**
         * 用于生成签名
         */
        public void setToken(String token) {
            this.token = token;
        }
        /**
         * 用于消息体的加密，是AES密钥的Base64编码
         */
        public String getEncodingaeskey() {
            return encodingaeskey;
        }
        /**
         * 用于消息体的加密，是AES密钥的Base64编码
         */
        public void setEncodingaeskey(String encodingaeskey) {
            this.encodingaeskey = encodingaeskey;
        }
    }

    @ApiModelProperty("上传的csv文件的media_id")
    @NotNull("media_id属性为空")
    @JsonProperty("media_id")
    private String mediaId;

    @ApiModelProperty("是否邀请新建的成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。")
    @JsonProperty("to_invite")
    private Boolean toInvite;

    @ApiModelProperty("回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项")
    @JsonProperty("callback")
    private Callback callback;

    /**
     * 上传的csv文件的media_id
     */
    public String getMediaId() {
        return mediaId;
    }

    /**
     * 上传的csv文件的media_id
     */
    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    /**
     * 是否邀请新建的成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
     */
    public Boolean getToInvite() {
        return toInvite;
    }

    /**
     * 是否邀请新建的成员使用企业微信（将通过微信服务通知或短信或邮件下发邀请，每天自动下发一次，最多持续3个工作日），默认值为true。
     */
    public void setToInvite(Boolean toInvite) {
        this.toInvite = toInvite;
    }

    /**
     * 回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
     */
    public Callback getCallback() {
        return callback;
    }

    /**
     * 回调信息。如填写该项则任务完成后，通过callback推送事件给企业。具体请参考应用回调模式中的相应选项
     */
    public void setCallback(Callback callback) {
        this.callback = callback;
    }
}
