package net.icgear.three.weixin.qyapi.rest.dto.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 消息类型
 * 
 * @author Y13
 *
 */
@JsonInclude(Include.NON_NULL)
public interface IMessage {
    
    /**
     * 消息类型
     * 
     * 该方法有违方法命名方式，主要不想让json序列化
     * 
     * @return
     */
    String type();
}
