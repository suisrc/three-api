package net.icgear.three.weixin.qyapi.rest.dto.message;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 
 * 返回结果：
 *  {
 *      "errcode" : 0, 
 *      "errmsg" : "ok", 
 *      "invaliduser" : "userid1|userid2", 
 *      "invalidparty" : "partyid1|partyid2", 
 *      "invalidtag":"tagid1|tagid2"
 *  }
 * 
 * 参数说明：
 *  参数.            说明 
 *  errcode         异常code 
 *  errmsg          异常信息 
 *  invaliduser     无效用户，不区分大小写，返回的列表都统一转为小写 
 *  invalidparty    无效部门
 * invalidtag       无效标签
 * 
 * @author Y13
 *
 */
@ApiModel("SendResult")
public class SendResult extends WxErrCode {
    private static final long serialVersionUID = 3927135471806284142L;

    @ApiModelProperty("无效用户，不区分大小写，返回的列表都统一转为小写")
    @JsonProperty("invaliduser")
    private String invaliduser;

    @ApiModelProperty("无效部门")
    @JsonProperty("invalidparty")
    private String invalidparty;

    @ApiModelProperty("无效标签")
    @JsonProperty("invalidtag")
    private String invalidtag;

    /**
     * 无效用户，不区分大小写，返回的列表都统一转为小写
     */
    public String getInvaliduser() {
        return invaliduser;
    }

    /**
     * 无效用户，不区分大小写，返回的列表都统一转为小写
     */
    public void setInvaliduser(String invaliduser) {
        this.invaliduser = invaliduser;
    }

    /**
     * 无效部门
     */
    public String getInvalidparty() {
        return invalidparty;
    }

    /**
     * 无效部门
     */
    public void setInvalidparty(String invalidparty) {
        this.invalidparty = invalidparty;
    }

    /**
     * 无效标签
     */
    public String getInvalidtag() {
        return invalidtag;
    }

    /**
     * 无效标签
     */
    public void setInvalidtag(String invalidtag) {
        this.invalidtag = invalidtag;
    }
}
