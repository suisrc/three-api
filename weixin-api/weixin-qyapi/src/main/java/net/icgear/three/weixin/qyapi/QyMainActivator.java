package net.icgear.three.weixin.qyapi;

import java.util.Set;

import com.suisrc.core.Global;
import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.three.weixin.core.AbstractWeixinActivator;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.handler.WxTokenHandler;
import net.icgear.three.weixin.qyapi.rest.api.QyMainServiceRest;
import net.icgear.three.weixin.qyapi.rest.api.UserManagerRest;
import net.icgear.three.weixin.qyapi.rest.dto.user.UserInfoResult;

/**
 * 对于本公司而已，存放了本公司信息直接访问的激活器
 * 
 * @author Y13
 */
public abstract class QyMainActivator extends AbstractWeixinActivator {

  @Override
  public Set<Class<?>> getClasses() {
    return ReflectionUtils.getTypesAnnotatedWith(RemoteApi.class, true, QyMainServiceRest.class.getPackage().getName());
  }

  /**
   * 获取默认基础链接地址
   */
  @Override
  protected String getDefaultBaseUrl() {
    return "https://qyapi.weixin.qq.com";
  }

  private QyMainServiceRest tokenRest;

  /**
   * 用户管理
   */
  private UserManagerRest userMngRest = null;

  /**
   * 获取用户openid信息
   * 
   * @return
   */
  public String getUserOpenId() {
    if (userMngRest == null) {
      // 多线程重复修改没有影响，只要一次成功即可
      userMngRest = getApiImplement(UserManagerRest.class);
      if (userMngRest == null) {
        throw new RuntimeException("无法获取用户内容管理器的实现：" + UserManagerRest.class.getName());
      }
    }
    // 经常发生code值无法找到的问题，为了解决这个问题，code使用手动获取方式
    String code = Global.getCacheSafe(Global.getThreadCache(), WxConsts.OAUTH2_CODE, String.class);
    UserInfoResult result = userMngRest.getUserInfo(code);
    if (result.isErr()) {
      // 访问发生了异常
      logger.warning(result.printErr());
      return null;
    }

    if (result.getUserTicket() != null) {
      // 将UserTicket临时存储到当前缓存中
      Global.putCacheSafe(Global.getThreadCache(), WxConsts.OAUTH2_USER_TICKET, result.getUserTicket());
    }
    // 给出原始回调数据
    Global.putCacheSafe(Global.getThreadCache(), WxConsts.OAUTH2_USER_INFO, result);

    if (result.getUserId() != null) {
      // return result.getCorpId() + ":" + result.getUserId();
      // 本公司内容，返回的corpid为空
      return getWxAppId() + ":" + result.getUserId();
    } else {
      return result.getOpenId();
    }
  }

  /**
   * 获取服务控制器
   * 
   * @return
   */
  public QyMainServiceRest getAccessTokenRest() {
    if (tokenRest == null) {
      tokenRest = createTokenRest();
    }
    return tokenRest;
  }

  /**
   * 获取访问控制器
   * 
   * @return
   */
  protected QyMainServiceRest createTokenRest() {
    return getApiImplement(QyMainServiceRest.class);
  }

  /**
   * 数据获取适配器
   * 
   * T_ACCESS_TOKEN 企业访问令牌
   */
  @SuppressWarnings("unchecked")
  @Override
  public <T> T getAdapter(String key, Class<T> type) {
    if (type == QyMainServiceRest.class) {
      return (T) getAccessTokenRest();
    }
    if (type == String.class) {
      if (key.equals(QyConsts.T_ACCESS_TOKEN)) {
        return super.getAdapter(WxConsts.ACCESS_TOKEN, type);
      }
      if (key.equals(WxConsts.COOKIE_OPEN_ID)) {
        return (T) getUserOpenId();
      }
    }
    if (type == Integer.class) {
      if (key.equals(QyConsts.T_APP_AGENT_ID)) {
        return super.getAdapter(WxConsts.APP_AGENT_ID, type);
      }
    }
    return super.getAdapter(key, type);

  }

  @Override
  protected WxTokenHandler getWxTokenHandler(String tokenKey) {
    // 对于本公司而已，其中tokenKey可以忽略
    return () -> getAccessTokenRest().getToken(getWxAppId(), getWxAppSecret());
  }
}
