package net.icgear.three.weixin.qyapi.rest.dto.three;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 获取应用的管理员列表
 * 
 * @author Y13
 *
 */
@ApiModel("AdminListBody")
public class AdminListBody {

    @NotNull("应用的授权方corpid为空")
    @ApiModelProperty("授权方corpid")
    @JsonProperty("auth_corpid")
    private String authCorpid;

    @NotNull("应用的授权方agentid为空")
    @ApiModelProperty("授权方安装的应用agentid")
    @JsonProperty("agentid")
    private String agentid;

    public String getAuthCorpid() {
        return authCorpid;
    }

    public void setAuthCorpid(String authCorpid) {
        this.authCorpid = authCorpid;
    }

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }
    
}
