package net.icgear.three.weixin.qyapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * userid转openid
 * @author Y13
 *
 */
@ApiModel("Convert2OpenidBody")
public class Convert2OpenidBody {
    
    @ApiModelProperty("企业内的成员id")
    @NotNull("userid属性为空")
    @JsonProperty("userid")
    private String userid;
    
    /**
     * 企业内的成员id
     */
    public String getUserid() {
        return userid;
    }
    /**
     * 企业内的成员id
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }
}