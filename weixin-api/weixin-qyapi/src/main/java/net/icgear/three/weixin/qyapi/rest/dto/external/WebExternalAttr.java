package net.icgear.three.weixin.qyapi.rest.dto.external;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 网页属性
 * @author Y13
 *
 */
@ApiModel("ExternalProfile.WebExternalAttr")
public class WebExternalAttr extends ExternalAttr {
    
    @ApiModel("ExternalProfile.WebExternalAttr.Web")
    @JsonInclude(Include.NON_NULL) 
    public static class Web {
        
        @ApiModelProperty("网页的url,必须包含http或者https头")
        @JsonProperty("url")
        private String url;
        
        @ApiModelProperty("网页的展示标题,长度限制12个UTF8字符")
        @JsonProperty("title")
        private String title;

        /**
         * 网页的url,必须包含http或者https头
         * @return
         */
        public String getUrl() {
            return url;
        }

        /**
         * 网页的url,必须包含http或者https头
         * @param url
         */
        public void setUrl(String url) {
            this.url = url;
        }

        /**
         * 网页的展示标题,长度限制12个UTF8字符
         * @return
         */
        public String getTitle() {
            return title;
        }

        /**
         * 网页的展示标题,长度限制12个UTF8字符
         * @param title
         */
        public void setTitle(String title) {
            this.title = title;
        }
    }

    @ApiModelProperty("网页类型的属性，url和title字段要么同时为空表示清除该属性，要么同时不为空")
    @JsonProperty("web")
    private Web web;

    public Web getWeb() {
        return web;
    }

    public void setWeb(Web web) {
        this.web = web;
    }

}
