package net.icgear.three.weixin.qyapi.retry;

import com.suisrc.core.Global;
import com.suisrc.core.cache.ScopedCache;
import com.suisrc.jaxrsapi.core.ApiActivator;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.filter.WxTokenReTryFilter;
import net.icgear.three.weixin.qyapi.QyConsts;

/**
 * 对微信中的Access Token 有效性记性验证 系统很容易形成恶性竞争，所以请谨慎使用。
 * 
 * @author Y13
 *
 */
public class WxAccessTokenReTryFilter extends WxTokenReTryFilter {

    public WxAccessTokenReTryFilter(ApiActivator activator) {
        super(activator);
    }
    
    /**
     * 获取企业Token信息
     * QyConsts.T_ACCESS_TOKEN
     */
    protected String getTokenKey() {
        ScopedCache cache = Global.getThreadCache();
        if (cache == null) {
            throw new RuntimeException("无法清空企业访问令牌，没有线程缓存");
        }
        // 将企业信息放入线程缓存
        Object appid = cache.get(QyConsts.APP_CORP_ID);
        if (appid == null) {
            throw new RuntimeException("无法清空企业访问令牌，线程上无企业信息");
        }
        String tokenKey = WxConsts.ACCESS_TOKEN + appid.toString();
        return tokenKey;
    }
}
