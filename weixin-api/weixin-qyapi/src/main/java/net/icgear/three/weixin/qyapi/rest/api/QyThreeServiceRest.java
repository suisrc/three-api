package net.icgear.three.weixin.qyapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsConsts;
import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Retry;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.annotation.WxApi;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.qyapi.QyApiHelper;
import net.icgear.three.weixin.qyapi.QyConsts;
import net.icgear.three.weixin.qyapi.rest.dto.three.AdminListBody;
import net.icgear.three.weixin.qyapi.rest.dto.three.AdminListResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.AuthInfoBody;
import net.icgear.three.weixin.qyapi.rest.dto.three.AuthInfoResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.CorpTokenBody;
import net.icgear.three.weixin.qyapi.rest.dto.three.CorpTokenResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.PermanentCodeBody;
import net.icgear.three.weixin.qyapi.rest.dto.three.PermanentCodeResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.PreAuthCodeResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.SessionInfoBody;
import net.icgear.three.weixin.qyapi.rest.dto.three.SuiteTokenBody;
import net.icgear.three.weixin.qyapi.rest.dto.three.SuiteTokenResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.UserDetail3rdBody;
import net.icgear.three.weixin.qyapi.rest.dto.three.UserDetail3rdResult;
import net.icgear.three.weixin.qyapi.rest.dto.three.UserInfo3rdResult;
import net.icgear.three.weixin.qyapi.retry.WxSuiteTokenReTryFilter;

/**
 * 第三方应用接口
 * 
 * 第三方服务商拥有自己的API集合，主要用于完成授权流程以及实现授权后的相关功能。
 *      特别注意，第三方服务商调用所有API（包括第三方API及通讯录、发消息等所有API）都需要验证来源IP。
 *      只有在服务商信息中填写的合法IP列表内才能合法调用，其他一律拒绝。
 *      https://work.weixin.qq.com/api/doc#10970/%E9%85%8D%E7%BD%AE%E5%BC%80%E5%8F%91%E4%BF%A1%E6%81%AF
 *      
 * 接口说明
 * 
 * 第三方API（即接口）如下：
 * 
 *      功能  API名称
 *      获取第三方应用凭证.                get_suite_token
 *      获取预授权码.                     get_pre_auth_code
 *      设置授权配置.                     set_session_info
 *      获取企业的永久授权码.              get_permanent_code
 *      获取企业access_token.             get_corp_token
 *      获取企业授权信息.                  get_auth_info
 *      获取应用的管理员列表.               get_admin_list
 *      第三方根据code获取企业成员信息.      getuserinfo3rd
 *      第三方使用user_ticket获取成员详情.  getuserdetail3rd
 *      
 * 第三方应用在获得企业授权后，服务商可以获取企业access_token，调用企业微信相关的API进行发消息、管理通讯录和应用等操作。
 * 
 * 
 * @author Y13
 *
 */
@WxApi(types=WxConsts.QT)
@RemoteApi("cgi-bin/service")
public interface QyThreeServiceRest {
    final String NAMED = JaxrsConsts.separator + "net.icgear.three.weixin.qyapi.rest.api.QyThreeServiceRest";

    /**
     * 获取第三方应用凭证
     * 
     * 该API用于获取第三方应用凭证（suite_access_token）。
     * 
     * 由于第三方服务商可能托管了大量的企业，其安全问题造成的影响会更加严重，故API中除了合法来源IP校验之外，还额外增加了suite_ticket作为安全凭证。
     * 获取suite_access_token时，需要suite_ticket参数。suite_ticket由企业微信后台定时推送给“指令回调URL”，每十分钟更新一次，见推送suite_ticket。
     * suite_ticket实际有效期为30分钟，可以容错连续两次获取suite_ticket失败的情况，但是请永远使用最新接收到的suite_ticket。
     * 通过本接口获取的suite_access_token有效期为2小时，开发者需要进行缓存，不可频繁获取。
     * 
     * 请求方式：POST（HTTPS）
     * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/get_suite_token
     * 请求包体：
     * {
     *     "suite_id":"id_value" ,
     *     "suite_secret": "secret_value", 
     *     "suite_ticket": "ticket_value" 
     * }
     * 返回结果：
     * {
     *     "errcode":0 ,
     *     "errmsg":"ok" ,
     *     "suite_access_token":"61W3mEpU66027wgNZ_MhGHNQDHnFATkDa9-2llMBjUwxRSNPbVsMmyD-yq8wZETSoE5NQgecigDrSHkPtIYA",
     *     "expires_in":7200
     * }
     * 
     */
    @POST
    @Path("get_suite_token")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SuiteTokenResult getSuiteToken(@NotNull("访问的BODY数据为空")SuiteTokenBody body);
    default SuiteTokenResult getSuiteToken() {
        return getSuiteToken(new SuiteTokenBody());
    }
    
    /**
     * 获取预授权码
     * 
     * 该API用于获取预授权码。预授权码用于企业授权时的第三方服务商安全验证。
     * 
     * 请求方式：GET（HTTPS）
     * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/get_pre_auth_code?suite_access_token=SUITE_ACCESS_TOKEN
     * 参数:
     * suite_access_token:第三方应用access_token,最长为512字节
     * 返回结果：
     * {
     *     "errcode":0 ,
     *     "errmsg":"ok" ,
     *     "pre_auth_code":"Cx_Dk6qiBE0Dmx4EmlT3oRfArPvwSQ-oa3NL_fwHM7VI08r52wazoZX2Rhpz1dEw",
     *     "expires_in":1200
     * }
     * 
     */
    @GET
    @Path("get_pre_auth_code")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Retry(value = WxSuiteTokenReTryFilter.class, master = JaxrsConsts.FIELD_ACTIVATOR) // 放生异常清空激活器上的satoken后重试
    PreAuthCodeResult getPreAuthCode(@QueryParam("suite_access_token")@Value(QyConsts.SUITE_ACCESS_TOKEN)@NotNull("应用的访问令牌为空")String satoken);
    default PreAuthCodeResult getPreAuthCode() {
        return getPreAuthCode(null);
    }
    
    /**
     * 设置授权配置
     * 该接口可对某次授权进行配置。可支持测试模式（应用未发布时）。
     * 
     * 请求方式：POST（HTTPS）
     * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/set_session_info?suite_access_token=SUITE_ACCESS_TOKEN
     * 
     * 请求包体：
     * {
     *     "pre_auth_code":"xxxxx",
     *     "session_info":
     *     {
     *         "appid":[1,2,3],
     *         "auth_type":1
     *     }
     * }
     * 返回结果：
     * {
     *     "errcode": 0,
     *     "errmsg": "ok"
     * }
     * 
     */
    @POST
    @Path("set_session_info")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Retry(value = WxSuiteTokenReTryFilter.class, master = JaxrsConsts.FIELD_ACTIVATOR) // 放生异常清空激活器上的satoken后重试
    WxErrCode setSessionInfo(@QueryParam("suite_access_token")@Value(QyConsts.SUITE_ACCESS_TOKEN)@NotNull("应用的访问令牌为空")String satoken, SessionInfoBody body);
    default WxErrCode setSessionInfo(SessionInfoBody body) {
        return setSessionInfo(null, body);
    }
    
    /**
     * 获取企业永久授权码
     * 
     * 该API用于使用临时授权码换取授权方的永久授权码，并换取授权信息、企业access_token，临时授权码一次有效。建议第三方以userid为主键，来建立自己的管理员账号。
     * 请求方式：POST（HTTPS）
     * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/get_permanent_code?suite_access_token=SUITE_ACCESS_TOKEN
     * 请求包体：
     * {
     *     "auth_code": "auth_code_value"
     * }
     * 返回结果：
     * {
     *     "errcode":0 ,
     *     "errmsg":"ok" ,
     *     "access_token": "xxxxxx", 
     *     "expires_in": 7200, 
     *     "permanent_code": "xxxx", 
     *     "auth_corp_info": 
     *     {
     *         "corpid": "xxxx",
     *         "corp_name": "name",
     *         "corp_type": "verified",
     *         "corp_square_logo_url": "yyyyy",
     *         "corp_user_max": 50,
     *         "corp_agent_max": 30,
     *         "corp_full_name":"full_name",
     *         "verified_end_time":1431775834,
     *         "subject_type": 1，
     *         "corp_wxqrcode": "zzzzz",
     *         "corp_scale": "1-50人",
     *         "corp_industry": "IT服务",
     *         "corp_sub_industry": "计算机软件/硬件/信息服务"
     *     },
     *     "auth_info":
     *     {
     *         "agent" :
     *         [
     *             {
     *                 "agentid":1,
     *                 "name":"NAME",
     *                 "round_logo_url":"xxxxxx",
     *                 "square_logo_url":"yyyyyy",
     *                 "appid":1,
     *                 "privilege":
     *                 {
     *                     "level":1,
     *                     "allow_party":[1,2,3],
     *                     "allow_user":["zhansan","lisi"],
     *                     "allow_tag":[1,2,3],
     *                     "extra_party":[4,5,6],
     *                     "extra_user":["wangwu"],
     *                     "extra_tag":[4,5,6]
     *                 }
     *             },
     *             {
     *                 "agentid":2,
     *                 "name":"NAME2",
     *                 "round_logo_url":"xxxxxx",
     *                 "square_logo_url":"yyyyyy",
     *                 "appid":5
     *             }
     *         ]
     *     },
     *     "auth_user_info":
     *     {
     *         "userid":"aa",
     *         "name":"xxx",
     *         "avatar":"http://xxx"
     *     }
     * }
     */
    @POST
    @Path("get_permanent_code")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Retry(value = WxSuiteTokenReTryFilter.class, master = JaxrsConsts.FIELD_ACTIVATOR) // 放生异常清空激活器上的satoken后重试
    PermanentCodeResult getPermanentCode(@QueryParam("suite_access_token")@Value(QyConsts.SUITE_ACCESS_TOKEN)@NotNull("应用的访问令牌为空")String satoken, PermanentCodeBody body);
    default PermanentCodeResult getPermanentCode(String authCode) {
        PermanentCodeBody body = new PermanentCodeBody();
        body.setAuthCode(authCode);
        return getPermanentCode(null, body);
    }
    
    /**
     * 获取企业授权信息
     * 该API用于通过永久授权码换取企业微信的授权信息。 永久code的获取，是通过临时授权码使用get_permanent_code 接口获取到的permanent_code。
     * 
     * 请求方式：POST（HTTPS）
     * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/get_auth_info?suite_access_token=SUITE_ACCESS_TOKEN
     * 
     * 请求包体：
     * {
     *     "auth_corpid": "auth_corpid_value",
     *     "permanent_code": "code_value"
     * }
     * 返回结果：
     * 同getPermanentCode
     */
    @POST
    @Path("get_auth_info")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Retry(value = WxSuiteTokenReTryFilter.class, master = JaxrsConsts.FIELD_ACTIVATOR) // 放生异常清空激活器上的satoken后重试
    AuthInfoResult getAuthInfo(@QueryParam("suite_access_token")@Value(QyConsts.SUITE_ACCESS_TOKEN)@NotNull("应用的访问令牌为空")String satoken, AuthInfoBody body);
    default AuthInfoResult getAuthInfo(String authCorpid) {
        return QyApiHelper.call(authCorpid, () -> {
            AuthInfoBody body = new AuthInfoBody();
            body.setAuthCorpid(authCorpid);
            return getAuthInfo(null, body);
        });
    }
    
    /**
     * 获取企业access_token
     * 第三方服务商在取得企业的永久授权码后，通过此接口可以获取到企业的access_token。
     * 获取后可通过通讯录、应用、消息等企业接口来运营这些应用。
     * 
     * 此处获得的企业access_token与企业获取access_token拿到的token，本质上是一样的，只不过获取方式不同。获取之后，就跟普通企业一样使用token调用API接口
     * 
     * 调用企业接口所需的access_token获取方法如下。
     * 请求方式：POST（HTTPS）
     * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/get_corp_token?suite_access_token=SUITE_ACCESS_TOKEN
     * 请求包体：
     *  {
     *       "auth_corpid": "auth_corpid_value",
     *       "permanent_code": "code_value"
     *  }
     * 返回结果：
     * {
     *     "errcode":0 ,
     *     "errmsg":"ok" ,
     *     "access_token": "xxxxxx", 
     *     "expires_in": 7200
     * }
     */
    @POST
    @Path("get_corp_token")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Retry(value = WxSuiteTokenReTryFilter.class, master = JaxrsConsts.FIELD_ACTIVATOR) // 放生异常清空激活器上的satoken后重试
    CorpTokenResult getCorpToken(@QueryParam("suite_access_token")@Value(QyConsts.SUITE_ACCESS_TOKEN)@NotNull("应用的访问令牌为空")String satoken, CorpTokenBody body);
    default CorpTokenResult getCorpToken(String authCorpid) {
        return QyApiHelper.call(authCorpid, () -> {
            CorpTokenBody body = new CorpTokenBody();
            body.setAuthCorpid(authCorpid);
            return getCorpToken(null, body);
        });
    }
    
    /**
     * 获取应用的管理员列表
     * 
     * 第三方服务商可以用此接口获取授权企业中某个第三方应用的管理员列表(不包括外部管理员)，以便服务商在用户进入应用主页之后根据是否管理员身份做权限的区分。
     * 
     * 该应用必须与SUITE_ACCESS_TOKEN对应的suiteid对应，否则没权限查看
     * 
     * 请求方式：POST（HTTPS）
     * 请求地址： https://qyapi.weixin.qq.com/cgi-bin/service/get_admin_list?suite_access_token=SUITE_ACCESS_TOKEN
     * 
     * 请求包体：
     * {
     *     "auth_corpid": "auth_corpid_value",
     *     "agentid": 1000046
     * }
     * 返回结果：
     * {
     *     "errcode": 0,
     *     "errmsg": "ok",
     *     "admin":[
     *         {"userid":"zhangsan","auth_type":1},
     *         {"userid":"lisi","auth_type":0}
     *     ]
     * }
     */
    @POST
    @Path("get_admin_list")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Retry(value = WxSuiteTokenReTryFilter.class, master = JaxrsConsts.FIELD_ACTIVATOR) // 放生异常清空激活器上的satoken后重试
    AdminListResult getAdminList(@QueryParam("suite_access_token")@Value(QyConsts.SUITE_ACCESS_TOKEN)@NotNull("应用的访问令牌为空")String satoken, AdminListBody body);
    default AdminListResult getAdminList(String authCorpid, String agentid) {
        AdminListBody body = new AdminListBody();
        body.setAuthCorpid(authCorpid);
        body.setAgentid(agentid);
        return getAdminList(null, body);
    }
    
    /**
     * 网页授权登录第三方
     * 第三方网页授权登录请使用此接口。关于授权流程，与企业内的“网页授权登录”基本一致，此处不再赘述。
     * 
     * 构造第三方oauth2链接
     * 如果第三方应用需要在打开的网页里面携带用户的身份信息，第一步需要构造如下的链接来获取code：
     * https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect
     * 
     *   appid                是.   第三方应用id（即ww或wx开头的suite_id）。注意与企业的网页授权登录不同
     *   redirect_uri         是.   授权后重定向的回调链接地址，请使用urlencode对链接进行处理 ，注意域名需要设置为第三方应用的可信域名
     *   response_type        是.   返回类型，此时固定为：code
     *   scope                是.   应用授权作用域。
     *                              snsapi_base：静默授权，可获取成员的基础信息（UserId与DeviceId）；
     *                              snsapi_userinfo：静默授权，可获取成员的详细信息，但不包含手机、邮箱等敏感信息；
     *                              snsapi_privateinfo：手动授权，可获取成员的详细信息，包含手机、邮箱等敏感信息。
     *   state                否.   重定向后会带上state参数，企业可以填写a-zA-Z0-9的参数值，长度不可超过128个字节
     *   #wechat_redirect.    是.   终端使用此参数判断是否需要带上身份信息
     *   
     * 企业员工点击后，页面将跳转至 redirect_uri?code=CODE&state=STATE，第三方应用可根据code参数获得企业员工的corpid与userid。code长度最大为512字节。
     * 
     * 权限说明：
     * 使用snsapi_privateinfo的scope时，第三方应用必须有’成员敏感信息授权’的权限。
     */
    
    /**
     * 第三方根据code获取企业成员信息
     * 
     * 请求方式：GET（HTTPS）
     * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/service/getuserinfo3rd?access_token=SUITE_ACCESS_TOKEN&code=CODE
     * 
     * 参数说明：
     * access_token     是.   第三方应用的suite_access_token，参见“获取第三方应用凭证”
     * code             是.   通过成员授权获取到的code，最大为512字节。每次成员授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
     * 
     * 权限说明：
     * 跳转的域名须完全匹配access_token对应第三方应用的可信域名，否则会返回50001错误。
     * 
     * 返回结果：
     * 
     * {
     *    "errcode": 0,
     *    "errmsg": "ok",
     *    "CorpId":"CORPID",
     *    "UserId":"USERID",
     *    "DeviceId":"DEVICEID",
     *    "user_ticket": "USER_TICKET"，
     *    "expires_in":7200
     * }
     */
    @GET
    @Path("getuserinfo3rd")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Retry(value = WxSuiteTokenReTryFilter.class, master = JaxrsConsts.FIELD_ACTIVATOR) // 异常清空激活器上的satoken后重试
    UserInfo3rdResult getUserInfo3rd(@QueryParam("suite_access_token")@Value(QyConsts.SUITE_ACCESS_TOKEN)@NotNull("应用的访问令牌为空") String satoken, 
            @QueryParam("code")@Value(WxConsts.T_OAUTH2_CODE)@NotNull("成员授权获取到的code为空") String code);
    default UserInfo3rdResult getUserInfo3rd(String code) {
        return getUserInfo3rd(null, code);
    }
    
    /**
     * 第三方使用user_ticket获取成员详情
     * 
     * 请求方式：POST（HTTPS）
     * 请求地址：https://qyapi.weixin.qq.com/cgi-bin/service/getuserdetail3rd?access_token=SUITE_ACCESS_TOKEN
     * 
     * 请求包体：
     * {
     *    "user_ticket": "USER_TICKET"
     * }
     * 返回结果：
     * {
     *   "errcode": 0,
     *   "errmsg": "ok",
     *   "corpid":"wwxxxxxxyyyyy",
     *   "userid":"lisi",
     *   "name":"李四",
     *   "mobile":"15913215421",
     *   "gender":"1",
     *   "email":"xxx@xx.com",
     *   "avatar":"http://shp.qpic.cn/bizmp/xxxxxxxxxxx/0",
     *   "qr_code":"https://open.work.weixin.qq.com/wwopen/userQRCode?vcode=vcfc13b01dfs78e981c"
     * }
     */
    @POST
    @Path("getuserdetail3rd")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Retry(value = WxSuiteTokenReTryFilter.class, master = JaxrsConsts.FIELD_ACTIVATOR) // 放生异常清空激活器上的satoken后重试
    UserDetail3rdResult getUserDetail3rd(@QueryParam("suite_access_token")@Value(QyConsts.SUITE_ACCESS_TOKEN)@NotNull("应用的访问令牌为空") String satoken, UserDetail3rdBody body);
    default UserDetail3rdResult getUserDetail3rd(String userTicket) {
        UserDetail3rdBody body = new UserDetail3rdBody();
        body.setUserTicket(userTicket);
        return getUserDetail3rd(null, body);
    }
}
