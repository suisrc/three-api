package net.icgear.three.weixin.open.handler;

import java.util.Random;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Cookie;

import com.suisrc.core.Global;
import com.suisrc.core.security.ScCrypto;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.oauth2.WxOAuth2RestHandler;
import net.icgear.three.weixin.core.utils.WxUtils;

@Named("weixin-oauth2-openapi")
@Dependent
public class DefaultWxOAuth2RestHandler implements WxOAuth2RestHandler {
    protected static final Logger logger = Logger.getLogger(WxOAuth2RestHandler.class.getName());
    
    /**
     * 获取授权的密钥
     */
    private String secret;

    /**
     * 操作oauth句柄
     */
    @Inject
    private WxOAuth2Handler oauth2Handler;
    
    /**
     * 初始化构建
     */
    @PostConstruct
    protected void doPostConstruct() {
        secret = ScCrypto.genRandomStr(new Random().nextInt(9) + 5);
        logger.info("获取临时OAuth2授权的密钥[OAuth2 Secret]：" + secret);
    }
    
    public String getOAuth2Secret() {
        return secret;
    }
    
    @Override
    public String getOpenIdByWxAuth2() {
      String openid = Global.getCacheSafe(Global.getThreadCache(), WxConsts.COOKIE_OPEN_ID, String.class);
        if (openid != null) {
            return openid;
        }
        // 访问信息
        ContainerRequestContext request = WxUtils.getContainerRequestContext();
        if (request == null) {
            return null;
        }
        Cookie cookie = request.getCookies().get(WxConsts.COOKIE_OPEN_ID);
        if(cookie == null) {
            return null;
        }
        openid = oauth2Handler.getOpenIdByCookie(cookie);
        return openid;
    }

    @Override
    public boolean doUserTicket2Redirect(Object owner, String openid, String scope, String key, String userTicket, boolean isFirst) {
        return false;
    }

    @Override
    public String getOAuth2State(String tSecret, String token) {
        // 获取验证密钥
        String sSecret = getOAuth2Secret();
        if (sSecret == null || sSecret.isEmpty()) {
            return "禁用：获取OAuth2重定向标识接口已被禁用";
        }
        if (!sSecret.equals(tSecret)) {
            return "错误：密钥不正确，默认密钥请从应用启动日志中获取，查询前缀：[OAuth2 Secret]";
        }
        return oauth2Handler.getOAuth2State(token);
    }

}
