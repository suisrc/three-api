package net.icgear.three.weixin.open.intercept;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;

/**
 * 执行weixin的oauth2认证验证
 * 
 * 只对令牌进行验证，不进行微信oauth2认证令牌进行获取。
 * 
 * 可以用于get, post, delete, put请求
 * 
 * 主要用于请求的权限验证
 * 
 * @author Y13
 *
 */
@InterceptorBinding
@Target({METHOD, TYPE})
@Retention(RUNTIME)
public @interface WxOAuth2 {
}
