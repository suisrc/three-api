package net.icgear.three.weixin.open;

/**
 * 常量
 * @author Y13
 *
 */
public interface OpenConsts {
    
    /**
     * OAuth跳转最后字符串
     */
    String WECHAT_REDIRECT = "#wechat_redirect";

}
