package net.icgear.three.weixin.open.handler;

import javax.interceptor.InvocationContext;

import com.suisrc.jaxrsapi.core.ApiActivator;

/**
 * 微信状态验证器
 * 
 * 请注意，该方法可以动态生成微信回调的状态和验证，该方法一次有效。方法执行时候会新建。
 *
 */
public interface WxOAuth2StateHandler {

    /**
     * 获取微信state值
     * @param ctx
     * @param activator 
     * @param appid
     * @param state
     * @return
     */
    String getState(InvocationContext ctx, ApiActivator activator, String annoState);

}
