package net.icgear.three.weixin.open.service;

import java.util.function.Supplier;

/**
 * 
 * 一个特殊的服务类，用于重定向到微信服务器
 * 
 * 该类不是远程服务器接口，值用于从定向到微信服务器
 * 
 * @author Y13
 *
 */
public interface RedirectWeixinService {
    
    /**
     * 重定向到微信服务器
     * @param message 输出的logger中的内容
     * @param redirectUriGetter 获取重定向地址
     */
    void redirectWeixinServer(String message, Supplier<String> redirectUriGetter);
    default void redirectWeixinServer(String message, String redirectUri) {
        redirectWeixinServer(message, redirectUri::toString);
    }

    /**
     * 
     * 网站应用微信登录是基于OAuth2.0协议标准构建的微信OAuth2.0授权登录系统。 在进行微信OAuth2.在进行微信OAuth2.0授权登录接入之前，在微信开放平台
     * 注册开发者帐号，并拥有一个已审核通过的网站应用，并获得相应的AppID和AppSecret， 申请微信登录且通过审核后，可开始接入流程。
     * 
     * 微信OAuth2.0授权登录目前支持authorization_code模式，适用于拥有server端的应用授权。该模式整体流程为： 
     * 1. 第三方发起微信授权登录请求，微信用户允许授权第三方应用后，微信会拉起应用或重定向到第三方网站，并且带上授权临时票据code参数； 
     * 2. 通过code参数加上AppID和AppSecret等，通过API换取access_token； 
     * 3. 通过access_token进行接口调用，获取用户基本数据资源或帮助用户实现基本操作。
     * 
     * https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect
     * 第三方使用网站应用授权登录前请注意已获取相应网页授权作用域（scope=snsapi_login），则可以通过在PC端打开以下链接：
     * 若提示“该链接无法访问”，请检查参数是否填写错误，如redirect_uri的域名与审核时填写的授权域名不一致或scope不为snsapi_login。
     * 
     * 返回说明 用户允许授权后，将会重定向到redirect_uri的网址上，并且带上code和state参数 redirect_uri?code=CODE&state=STATE
     * 若用户禁止授权，则重定向后不会带上code参数，仅会带上state参数 redirect_uri?state=STATE
     * 
     * 使用Oauth2AuthorizeProxy返回跳转的URL地址
     * 
     * @param appid 公众号/企业ID
     * @param redirectUri 授权后重定向的回调链接地址，请使用urlencode对链接进行处理
     * @param responseType 返回类型，此时固定为：code
     * @param scope 应用授权作用域。
     *        snsapi_base：静默授权，可获取成员的的基础信息（UserId与DeviceId）；
     *        snsapi_userinfo：静默授权，可获取成员的详细信息，但不包含手机、邮箱；
     *        snsapi_privateinfo：手动授权，可获取成员的详细信息，包含手机、邮箱
     *        
     *        注意：企业自建应用可以根据userid获取成员详情，无需使用snsapi_userinfo和snsapi_privateinfo两种scope。
     *        更多说明:https://work.weixin.qq.com/api/doc#10028/scope%E7%9A%84%E7%89%B9%E6%AE%8A%E6%83%85%E5%86%B5
     * @param agentid 应用ID, 只有企业号有效
     * @param state 重定向后会带上state参数，企业可以填写a-zA-Z0-9的参数值，长度不可超过128个字节
     * @return
     */
    String authorize(String appid, String redirectUri, String responseType, String scope, Integer agentid, String state);

    default String authorize(String appid, String redirectUri, String scope, Integer agentid, String state) {
        return authorize(appid, redirectUri, null, scope, agentid, state);
    }
    default String authorize(String appid, String redirectUri, String scope, String state) {
        return authorize(appid, redirectUri, null, scope, null, state);
    }
    default String authorize(String appid, String redirectUri, String state) {
        return authorize(appid, redirectUri, null, null, null, state);
    }
    
    /**
     * 
     * 步骤一：请求code
     * 需要接入企业微信网页登录授权的页面加入一个跳转链接引导，链接地址如下：
     * 
     * https://open.work.weixin.qq.com/wwopen/sso/qrConnect?appid=ww100000a5f2191&agentid=1000000&redirect_uri=http%3A%2F%2Fwww.oa.com&state=web_login@gyoss9
     * 
     * 若提示“该链接无法访问”，请检查参数是否填写错误，如redirect_uri的域名与网页应用的可信域名不一致
     * 
     * 参数说明
     * 
     * 参数  必须  说明
     * appid   是   企业微信的CorpID，在企业微信管理端查看
     * agentid 是   授权方的网页应用ID，在具体的网页应用中查看
     * redirect_uri    是   重定向地址，需要进行UrlEncode
     * state   否   用于保持请求和回调的状态，授权请求后原样带回给企业。该参数可用于防止csrf攻击（跨站请求伪造攻击），建议企业带上该参数，可设置为简单的随机数加session进行校验
     * 返回说明
     * 用户允许授权后，将会重定向到redirect_uri的网址上，并且带上code和state参数
     * 
     * redirect_uri?code=CODE&state=STATE
     * 
     * 若用户禁止授权，则重定向后不会带上code参数，仅会带上state参数
     * 
     * redirect_uri?state=STATE
     * 
     * @param appid
     * @param redirectUri
     * @param agentid
     * @param state
     * @return
     */
    String wqrConnect(String appid, String redirectUri, Integer agentid, String state);
}
