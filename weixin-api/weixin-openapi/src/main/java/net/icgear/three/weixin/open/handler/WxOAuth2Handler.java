package net.icgear.three.weixin.open.handler;

import javax.interceptor.InvocationContext;
import javax.ws.rs.core.Cookie;

/**
 * 拦截后执行的方法，进行 验证，验证的结果为null表示可以执行该方法
 * 
 * 因为放回结果的不确定性，所以，不进行结果放回，使用异常的方式记性处理
 * 
 * @author Y13
 *
 */
public interface WxOAuth2Handler {

    /**
     * 验证OAuth2令牌信息，如果没有，记性重定向，向微信服务器获取认证的令牌信息
     * 
     * WxOAuth2Get注解标记的内容进入的方法
     * 
     * @param ctx 
     * 
     */
    void checkAndGetOAuth2(InvocationContext ctx);

    /**
     * 验证服务器上是有有 OAuth2令牌信息，如果没有，直接失败
     * 
     * WxOAuth2注解标记的内容进入的方法
     * 
     * @param ctx 
     * 
     */
    void checkOAuth2(InvocationContext ctx);

    /**
     * 防止系统在使用cookie时候是加密的，获取openid内容可以在该方法中解密处理
     * @param cookie
     * @return
     */
    String getOpenIdByCookie(Cookie cookie);
    
    /**
     * 获取cookie值
     * @param cookie
     * @return
     */
    String getCookieValueByOpenId(String openid);

    /**
     * 获取临时授权内容
     * @param token 不为空，为永久授权
     * @return
     */
    String getOAuth2State(String token);
    
}
