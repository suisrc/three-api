package net.icgear.three.weixin.open.intercept;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.interceptor.InterceptorBinding;

/**
 * 执行weixin的oauth2认证
 * 
 * 微信oauth2认证，作用在GET请求上，用于在页面加载过程中验证当前用户请求是否进行过oauth2认证
 * 
 * 当返现没有OAuth2认证时候，会自动跳转到微信服务器获取认证，该方法只对GET请求有效。
 * 所以在设计时候，登录客户页面，需要首先调用GET接口，以获取对应的OAuth2认证信息。
 * 
 * 如果系统是多微信号模式下，需要通过@Named方法，在对应的方法上标记需要控制的微信号信息。
 * 
 * 注：该注解对接口和类型无效，只能增加到实现类型的方法上
 * 
 * 当前作用域固定为snsapi_base， 仅用于获取用于的openid或者userid
 * 
 * 在使用多态请求的情况下，如果请求需要标记配置的应用主键（appkey）,请使用“#”区分
 * 
 * 比如：
 * http://wx.suisrc.cn/sso/auth/#key1
 * http://wx.suisrc.cn/sso/auth/#key2
 * 
 * @author Y13
 *
 */
@InterceptorBinding
@Target({METHOD, TYPE})
@Retention(RUNTIME)
public @interface WxOAuth2Get {
}
