 package net.icgear.three.weixin.open.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.Response;

import com.suisrc.core.exception.ResponseException;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.open.OpenConsts;
import net.icgear.three.weixin.open.ScopeType;
import net.icgear.three.weixin.open.service.RedirectWeixinService;

/**
 *   Y13
 */
@ApplicationScoped
public class RedirectWeixinServiceImpl implements RedirectWeixinService {
    
    /**
     * 重定向地址的域名
     */
    protected String openBaseUrl;
    
    protected String openWorkBaseUrl;

    @PostConstruct
    public void doPostConstruct() {
        openBaseUrl = System.getProperty(WxConsts.WEIXIN_PRE + "open.urls.open", "https://open.weixin.qq.com");
        openWorkBaseUrl = System.getProperty(WxConsts.WEIXIN_PRE + "open.urls.open-work", "https://open.work.weixin.qq.com");
    }
    
    /**
     * 重定向
     */
    @Override
    public void redirectWeixinServer(String message, Supplier<String> redirectUriGetter) {
        String redirectUri = redirectUriGetter.get();
        // 使用抛出异常的方式通知框架，需要重定向
        throw new ResponseException(message, re -> Response.seeOther(URI.create(redirectUri)).build());
    }

    /**
     * 微信OAuth2.0授权重定向地址
     */
    @Override
    public String authorize(String appid, String redirectUri, String responseType, String scope, Integer agentid, String state) {
        // 
        if (responseType == null) {
            responseType = "code";
        }
        if (scope == null || scope.isEmpty()) {
            scope = ScopeType.snsapi_base.name();
        }
        // 构建重定向地址
        String url = redirectUri;
        try {
            url = URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e); // 重定向的地址无法进行编码
        }
        StringBuilder sbir = new StringBuilder(openBaseUrl);
        sbir.append("/connect/oauth2/authorize?");
        sbir.append("appid=").append(appid).append('&');
        sbir.append("redirect_uri=").append(url).append('&');
        sbir.append("response_type=").append(responseType).append('&');
        sbir.append("scope=").append(scope).append('&');
        if (agentid != null) {
            sbir.append("agentid=").append(agentid).append('&');
        }
        if (state != null) {
            sbir.append("state=").append(state);
        } else {
            sbir.setLength(sbir.length() - 1); // 删除最后一个“&”字符
        }
        sbir.append(OpenConsts.WECHAT_REDIRECT);
        return sbir.toString();
    }

    /**
     * 微信扫码授权登录
     */
    @Override
    public String wqrConnect(String appid, String redirectUri, Integer agentid, String state) {
        // 构建重定向地址
        String url = redirectUri;
        try {
            url = URLEncoder.encode(url, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e); // 重定向的地址无法进行编码
        }
        StringBuilder sbir = new StringBuilder(openWorkBaseUrl);
        sbir.append("/wwopen/sso/qrConnect?");
        sbir.append("appid=").append(appid).append('&');
        sbir.append("agentid=").append(agentid).append('&');
        sbir.append("redirect_uri=").append(url).append('&');
        if (state != null) {
            sbir.append("state=").append(state);
        } else {
            sbir.setLength(sbir.length() - 1); // 删除最后一个“&”字符
        }
        return sbir.toString();
    }
    
}
