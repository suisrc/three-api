  package net.icgear.three.weixin.open.intercept;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import net.icgear.three.weixin.open.handler.WxOAuth2StateHandler;

/**
 * 执行weixin的oauth2认证
 * 
 * 微信oauth2认证，作用在GET请求上，用于在页面加载过程中验证当前用户请求是否进行过oauth2认证
 * 
 * 当返现没有OAuth2认证时候，会自动跳转到微信服务器获取认证，该方法只对GET请求有效。
 * 所以在设计时候，登录客户页面，需要首先调用GET接口，以获取对应的OAuth2认证信息。
 * 
 * 如果系统是多微信号模式下，需要通过@Named方法，在对应的方法上标记需要控制的微信号信息。
 * 
 * 注：该注解对接口和类型无效，只能增加到实现类型的方法上
 * 
 * 当前作用域固定为snsapi_base， 仅用于获取用于的openid或者userid
 * 
 * @author Y13
 *
 */
@Target({METHOD, TYPE})
@Retention(RUNTIME)
public @interface WxOAuth2Config {
    
    /**
     * 用于区分类型
     */
    String value() default "";

    /**
     * 返回常量，可以用于服务器验证
     * 
     * 保留"{}", 如果如果使用"{}"进行标记，就表示该内容为动态内容，需要向缓冲区获取， 由于系统使用并发处理，所以这里使用的标识符应该具有全局性
     * 
     * 推荐使用接口定制化处理，即一个接口由于固定的表示，这样即减少了数据的缓冲处理，用于防止csrf攻击（跨站请求伪造攻击）
     * 
     * @return
     */
    String state() default DEFAULT;
    
    /**
     * 用于动态计算和验证state,如果handler不为WxOAuth2StateHandler.class，则优先使用handler的内容，从而无条件无聊state内容。
     * @return
     */
    Class<? extends WxOAuth2StateHandler> handler() default WxOAuth2StateHandler.class;
    
    /**
     * 默认请求下，会请求微信服务器，以获取用的openid(对于企业，openid = corpid:userid)
     * 
     * 当 openid = false, 系统仅仅会验证是否有code即可。不会获取openid
     * 
     * @return
     */
    boolean openid() default true;
    
    /**
     * 是否前置刷新当前的openid的内容
     * 
     * "true":表示强制刷新
     * 其他情况，取QueryParam中对应内容的boolean值
     * 
     * 强调：对应的内容必须是boolean类型，空值表示false
     */
    String refreshKey() default "";

    /**
     * 授权作用域
     */
    String scope() default "snsapi_base";
    
    /**
     * 授权作用域, 如果不为空，会从QueryParam中抽取
     * 
     * 优先级高于scope()
     */
    String scopeKey() default "";
    
    /**
     * 重定向等待时间，默认为系统默认提供的时间
     */
    long timeWait() default 0;
    
    /**
     * 默认的访问类型
     * 该内容优先于请求的内容
     */
    String type() default "";
    
    /**
     * state的默认值
     */
    final String DEFAULT = "weixin_web_oauth_2";
    
    
}
