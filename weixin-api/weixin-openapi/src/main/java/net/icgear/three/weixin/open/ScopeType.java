package net.icgear.three.weixin.open;

/**
 * 授权作用域
 * 
 * 注意：企业自建应用可以根据userid获取成员详情，无需使用snsapi_userinfo和snsapi_privateinfo两种scope。更多说明见scope
 * 
 * @author Y13
 *
 */
public enum ScopeType {
    /**
     * 静默授权，可获取成员的的基础信息（UserId与DeviceId）；
     */
    snsapi_base, 
    /**
     * 
     */
    snsapi_login, 
    /**
     * 静默授权，可获取成员的详细信息，但不包含手机、邮箱；
     */
    snsapi_userinfo, 
    /**
     * 手动授权，可获取成员的详细信息，包含手机、邮箱
     */
    snsapi_privateinfo
}
