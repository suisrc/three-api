package net.icgear.three.weixin.open.handler;

import java.lang.reflect.Method;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;
import javax.inject.Named;
import javax.interceptor.InvocationContext;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.MultivaluedMap;

import com.suisrc.core.Global;
import com.suisrc.core.exception.UUIDException;
import com.suisrc.core.message.ECM;
import com.suisrc.core.reference.RefVal;
import com.suisrc.core.security.ScCrypto;
import com.suisrc.core.utils.CdiUtils;
import com.suisrc.core.utils.Throwables;
import com.suisrc.jaxrsapi.core.ApiActivator;

import net.icgear.three.weixin.core.WxConfig;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.utils.WxUtils;
import net.icgear.three.weixin.open.intercept.WxOAuth2Config;
import net.icgear.three.weixin.open.service.RedirectWeixinService;

/**
 * 执行默认的OAuth2.0验证方法
 * 
 * @author Y13
 *
 */
@Alternative
@ApplicationScoped
public class AlternativeWxOAuth2Handler implements WxOAuth2Handler {
  protected static final Logger logger = Logger.getLogger(WxOAuth2Handler.class.getName());

  /**
   * 测试发现，在企业应用开发过程中，是无法使用Oauth2在web开发者工具中调试的
   * 
   * 这里只能增加如下补丁： 当debug模式开启，进行openid验证时候，会有限获取url连接上的QueryParam中
   * 的wxopenid对应的值作为openid的内容增加到cookie中，不会对系统进行重定向操作。
   */
  protected final boolean debug = Boolean.getBoolean(WxOAuth2Handler.class.getName());


  /**
   * 加密解密工具
   */
  protected final ScCrypto crypto;

  /**
   * 重定向等待时间
   * 
   * 实践中，发现微信服务器经常发送两条请求并且code内容相同 因此这里增加重定向缓存等待时间，当时间超过等待时间，重定向的内容将会被放弃
   * 
   */
  protected final long redirectWait;

  /**
   * 垃圾回收器执行时间
   */
  protected final long recycleWait;

  /**
   * 回收器执行任务
   */
  protected volatile ScheduledFuture<?> recycleFuture = null;

  /**
   * 验证缓存
   * 
   * 用户缓存两种数据 第一种，code：回想回调给出的内容，防止响应微信多次回调 第二种，state：识别当前请求的内容是否来来自微信
   * 
   * Map<命中KEY, 数据插入时间>
   */
  protected final Map<String, Long> cache;

  /**
   * 服务器节点名称
   */
  protected String appId;

  /**
   * 获取oauth认证连接信息
   */
  @Inject
  protected RedirectWeixinService redirectWeixinService;

  /**
   * 构造方法
   */
  public AlternativeWxOAuth2Handler() {
    // 加密cookie时候使用的token
    String cryptoToken = System.getProperty(WxConsts.COOKIE_OAUTH2_PRE + "crypto.token", "Y13@2018");
    // 加密cookie时候的随机token长度
    int cryptoCount = Integer.getInteger(WxConsts.COOKIE_OAUTH2_PRE + "crypto.count", 7);
    // 构建加密工具
    crypto = new ScCrypto(cryptoCount, cryptoToken);
    // 微信等待时间, 默认为30秒
    redirectWait = Integer.getInteger(WxConsts.COOKIE_OAUTH2_PRE + "delay.redirect", 30) * 1000;
    // 缓存的垃圾回收, 默认为300秒(5分钟)
    recycleWait = Integer.getInteger(WxConsts.COOKIE_OAUTH2_PRE + "delay.recycle", 300) * 1000;
    // 数据缓冲器
    cache = new ConcurrentHashMap<>();
    // 服务器节点名称
    appId = System.getProperty("swarm.node.id");
  }

  /**
   * 
   */
  @Override
  public void checkAndGetOAuth2(InvocationContext ctx) {
    checkAndGetOAuth2(ctx, true);
  }

  /**
   * 
   */
  @Override
  public void checkOAuth2(InvocationContext ctx) {
    checkAndGetOAuth2(ctx, false);
  }

  /**
   * 获取临时授权
   */
  @Override
  public String getOAuth2State(String token) {
    return "错误：不支持重定向授权进行令牌临时授权和永久授权";
  }

  /**
   * 防止cache中存在死锁垃圾
   * 
   * 激活回收垃圾操作
   * 
   */
  protected void recycleRubbish() {
    if (recycleFuture != null && !recycleFuture.isDone()) {
      // 回收器正确运行或者等待运行
      return;
    }
    recycleFuture = Global.getScExecutor().schedule(() -> {
      try {
        long now = System.currentTimeMillis();
        boolean res = cache.entrySet().removeIf(e -> e.getValue() < now);
        logger.info("OAuth2验证器的缓存执行完一次垃圾回收: " + (res ? "有" : "没有") + "内容被清除");
        // 执行完成
        recycleFuture = null;
        if (!cache.isEmpty()) {
          // 如果cache不为空，执行下一次回调的垃圾回收
          recycleRubbish();
        }
      } catch (Exception e2) {
        new UUIDException("OAuth2验证缓存回收器发生异常：", e2).printStackTrace();
      }
    }, recycleWait, TimeUnit.MILLISECONDS);
  }

  /**
   * 放入缓存
   */
  protected String markState(String state, long timeWait) {
    if (redirectWait <= 0) {
      // 不执行处理
      return state;
    }
    if (timeWait == 0) {
      // 使用默认
      timeWait = redirectWait;
    }
    String newState = crypto.encrypt(state);
    if (timeWait < 0) {
      // 不进行回调标记
      return newState;
    }

    String key = "state:" + newState;
    long expired = System.currentTimeMillis() + timeWait;
    cache.put(key, expired);
    // 定时移除
    Global.getScExecutor().schedule(() -> cache.remove(key), timeWait, TimeUnit.MILLISECONDS);
    // 启动垃圾回收
    recycleRubbish();

    return newState;
  }

  /**
   * 放入缓存
   */
  protected void markCode(String code) {
    if (redirectWait <= 0) {
      // 不执行处理
      return;
    }
    logger.info("来自微信身份验证请求，识别用户code: " + code);
    String key = "code:" + code;
    long expired = System.currentTimeMillis() + redirectWait;
    cache.put(key, expired);
    // 定时移除
    Global.getScExecutor().schedule(() -> cache.remove(key), redirectWait, TimeUnit.MILLISECONDS);
    // 启动垃圾回收
    recycleRubbish();
  }

  /**
   * 验证缓存
   */
  protected String verifyState(ApiActivator activator, String state, boolean ignoreCache) {
    if (state == null || redirectWait <= 0) {
      return state; // 不执行处理
    }
    if (!ignoreCache) {
      String key = "state:" + state;
      Long expired = cache.remove(key);
      if (expired == null) {
        return null; // 缓存中没有访问记录信息
      }
    }
    try {
      // 界面获取真实的页面跳转信息
      return crypto.decrypt(state);
    } catch (Exception e) {
      // 解密失败，就业为验证失败的一种
      return null;
    }
  }

  /**
   * 验证缓存
   */
  protected boolean verifyCode(ApiActivator activator, String code) {
    if (redirectWait <= 0) {
      return true; // 不执行处理
    }
    String key = "code:" + code;
    // 如果缓存中没有当前code信息，表示可以访问, 返回应该为验证成功true
    return !cache.containsKey(key);
  }


  /**
   * 拦截处理
   * 
   * @param ctx
   * @param ifNotAuthFormWeixin
   */
  protected void checkAndGetOAuth2(InvocationContext ctx, boolean ifNotAuthNew) {
    ContainerRequestContext request = WxUtils.getContainerRequestContext();
    if (request == null) {
      throw ECM.exception("-9", "无法在容器线程缓存中找到当前请求内容的上下文");
    }
    Cookie cookie = request.getCookies().get(WxConsts.COOKIE_OPEN_ID);
    if (!ifNotAuthNew && cookie == null) {
      throw ECM.exception("-9", "OAuth2认证失败，请求中没有用户信息");
    }
    WxUtils.setAppKey(request); // 设定用户访问环境
    RefVal<Boolean> newOpenId = new RefVal<>(false);
    String openid = getOpenId(ctx, request, cookie, newOpenId);
    if (openid != null) {
      // 没有使用session缓存，是为了适应应用的集群部署的情况
      Global.putCacheSafe(Global.getThreadCache(), WxConsts.COOKIE_OPEN_ID, openid);
      if (newOpenId.get()) {
        // 用于结束访问后，保存到cookies中, 也可以在处理过程中获取用户信息
        String value = getCookieValueByOpenId(openid);
        Global.putCacheSafe(Global.getThreadCache(), WxConsts.COOKIE_OPEN_ID2, value);
      }
    }
  }

  /**
   * 获取cookie value
   */
  @Override
  public String getCookieValueByOpenId(String openid) {
    String value = crypto.encrypt(openid);
    if (appId != null) {
      value += "." + appId;
    }
    return value;
  }

  /**
   * 获取openid
   */
  @Override
  public String getOpenIdByCookie(Cookie cookie) {
    try {
      String value = cookie.getValue();
      if (value != null && value.equals("invalid")) {
        // 因为某种原因，导致openid强制被无效指定
        return null; // openid无效
      }
      int offset = value.lastIndexOf('.');
      if (offset > 0) {
        value = value.substring(0, offset);
      }
      String openid = crypto.decrypt(value);
      return openid;
    } catch (Exception e) {
      String msg = "OAuth2认证失败，无法获取或者解密用户信息：" + e.getMessage();
      throw ECM.exception("-9", msg);
    }
  }

  /**
   * 获取openid
   * 
   * @param ctx
   * @param request
   * @param cookie
   * @param newOpenId
   * @return
   */
  protected String getOpenId(InvocationContext ctx, ContainerRequestContext request, Cookie cookie, RefVal<Boolean> newOpenId) {
    MultivaluedMap<String, String> params = request.getUriInfo().getQueryParameters();
    WxOAuth2Config config = getWxOAuth2Annotation(ctx);
    String openid = null;
    if (cookie != null) {
      if (config == null) {
        // 使用缓存获取openid
        openid = getOpenIdByCookie(cookie);
      } else if (config.openid()) {
        // 验证是否获取openid
        String refreshKey = config.refreshKey();
        if (refreshKey.isEmpty()) {
          openid = getOpenIdByCookie(cookie);
        } else if ("true".equals(refreshKey)) {
          // 执行强制刷新
          // do nothing
        } else {
          String refreshValue = params.getFirst(refreshKey);
          if (refreshValue == null || !"true".equals(refreshValue)) {
            // 不执行刷新openid操作
            openid = getOpenIdByCookie(cookie);
          }
        }
      }
    }
    if (openid != null) {
      return openid;
    }
    // 构建新的openid
    newOpenId.set(true); // openid是新构建的
    // ------------------------------执行重定向操作
    if (debug) {
      openid = params.getFirst(WxConsts.COOKIE_OPEN_ID);
      if (openid != null) {
        return openid;
      }
    }
    ApiActivator activator = getWxActivator(ctx);
    // appid
    String appid = activator.getAdapter(WxConsts.APP_OPEN_ID, String.class);
    if (appid == null) {
      throw ECM.exception("-9", "OAuth2认证失败，无法获取应用ID信息");
    }
    // -----------------------------------------
    String code = params.getFirst("code");
    String state = params.getFirst("state");
    // 验证
    // 注意:wxState="none",表示state不存在
    String wxState = null;
    if (code != null) {
      // 验证是否为微信服务器的重复回调
      if (!verifyCode(activator, code)) {
        throw ECM.exception("-9", "回调失败，微信服务器的回调已经被应答，不可重复应答，访问被禁止, code= " + code);
      }
      wxState = verifyState(activator, state, false);
      if (wxState == null) {
        // 确认是否为该验证器发出的验证
        throw ECM.exception("-9", "回调失败，请求接口中的“state”内容在OAuth2验证器中不存在访问记录，访问被禁止, state=" + state);
      } else if (wxState.equals("none")) {
        // state内容不进行验证
        wxState = null;
      }
    }
    // -----------------------------------------
    else if (state != null && code == null) {
      // 用户可能是禁止授权登录
      throw ECM.exception("-9", "回调失败，用户禁止授权, state=" + state);
    }
    // -----------------------------------------
    // state, 获取方法注解上的状态码
    state = getStateByAnnoConfig(ctx, activator, config);
    if (code == null) {
      String scope = null;
      if (config != null) {
        if (config.scopeKey().isEmpty()) {
          scope = config.scope();
        } else {
          scope = params.getFirst(config.scopeKey());
          if (scope != null && scope.startsWith("three_")) {
            // 自建作用域，不予启用
            scope = null;
          }
        }
      }
      // 重定向地址域
      String redirectDomain = activator.getAdapter(WxConsts.LOCAL_DOMAIN, String.class);
      String locationUri = getLocationUri(request, redirectDomain);
      // 标记验证内容，用户回调验证
      String newState = markState(state, config.timeWait());
      // 企业应用id
      Integer agentid = activator.getAdapter(WxConsts.APP_AGENT_ID, Integer.class);
      redirectWxOAuth2Server(activator, params::getFirst, config.type(), appid, scope, agentid, newState, locationUri);
      // 该逻辑分支不再执行下面的操作
      // 以下内容不执行
      // 以下内容不执行
      // 以下内容不执行， 重要事情说三遍， 上面的方法会抛出异常，使当前执行的内容中断
    }
    if (wxState != null && !wxState.equals(state)) {
      // 确定是否为本服务器主动请求微信服务器回调
      throw ECM.exception("-9", "回调失败，请求接口中的“state”内容和OAuth2验证器回调接口内容不符，访问被禁止: state=" + state + "<<系统|微信>>" + wxState);
    }
    // 标记访问的code
    markCode(code);
    // 获取openid
    return getOpenIdByCode(activator, config, code);
  }

  /**
   * 获取方法注解上的状态码
   * 
   * @param ctx
   * @param activator
   * @param config
   * @return
   */
  protected String getStateByAnnoConfig(InvocationContext ctx, ApiActivator activator, WxOAuth2Config config) {
    String state = null;
    if (config != null) {
      state = config.state();
      if (state.indexOf('{') >= 0) {
        state = activator.getAdapter(state, String.class);
      }
      Class<? extends WxOAuth2StateHandler> stateHandlerClass = config.handler();
      if (stateHandlerClass != WxOAuth2StateHandler.class) {
        try {
          WxOAuth2StateHandler stateHandler = stateHandlerClass.newInstance();
          state = stateHandler.getState(ctx, activator, state);
        } catch (Exception e) {
          throw Throwables.getRuntimeException(e);
        }
      }
    }
    return state != null && !state.isEmpty() ? state : WxOAuth2Config.DEFAULT;
  }

  /**
   * 获取用户的openid信息
   * 
   * @param activator
   * @param config
   * @param code
   * @return
   */
  protected String getOpenIdByCode(ApiActivator activator, WxOAuth2Config config, String code) {
    if (config == null || config.openid()) {
      // 尝试获取openid
      try {
        Global.putCacheSafe(Global.getThreadCache(), WxConsts.COOKIE_OPEN_ID2, code);
        // 执行页面授权认证
        String openid = activator.getAdapter(WxConsts.COOKIE_OPEN_ID, String.class);
        if (openid == null) {
          throw ECM.exception("-9", "OAuth2认证失败，无法从微信服务器获取当前用户信息");
        }
        return openid;
      } finally {
        // 移除code码 （code码会在getAdapter中使用）
        Global.removeCacheSafe(Global.getThreadCache(), WxConsts.OAUTH2_CODE);
      }
    } else {
      // 临时保存code
      Global.putCacheSafe(Global.getThreadCache(), WxConsts.OAUTH2_CODE, code);
      return null;
    }
  }

  /**
   * 重定向到微信服务器
   */
  protected void redirectWxOAuth2Server(ApiActivator activator, Function<String, Object> getter, String type, String appid,
      String scope, Integer agentid, String state, String locationUri) {

    String redirectUri = redirectWeixinService.authorize(appid, locationUri, scope, agentid, state);
    String messageInfo = "重定向到微信服务器进行OAuth2认证: " + redirectUri;
    redirectWeixinService.redirectWeixinServer(messageInfo, redirectUri);
  }

  /**
   * 获取重定向后的本地地址
   * 
   * @param request
   * @param redirectDomain
   * @return
   */
  protected String getLocationUri(ContainerRequestContext request, String redirectDomain) {
    // 当前请求地址
    URI uri = request.getUriInfo().getRequestUri();
    String locationUri = null; // uri.toString()
    if (redirectDomain == null) {
      locationUri = uri.toString();
    } else {
      locationUri = redirectDomain + WxUtils.getPathQueryFromURI(uri);
      String lowerUrl = redirectDomain.toLowerCase();
      if (!lowerUrl.startsWith("http://") && !lowerUrl.startsWith("https://")) {
        locationUri = uri.getScheme() + "://" + locationUri;
      }
    }
    return locationUri;
  }

  /**
   * 获取WxOAuth2Get注解
   * 
   * @param ctx
   * @return
   */
  protected WxOAuth2Config getWxOAuth2Annotation(InvocationContext ctx) {
    Method method = ctx.getMethod();
    return method.getAnnotation(WxOAuth2Config.class);
  }

  /**
   * 
   * @param ctx
   * @return
   */
  protected ApiActivator getWxActivator(InvocationContext ctx) {
    Method method = ctx.getMethod();
    Named named = method.getAnnotation(Named.class);
    // 激活器
    ApiActivator activator = named == null ? CdiUtils.select(ApiActivator.class) : CdiUtils.select(ApiActivator.class, named);
    if (activator == null) {
      // 发生异常，无法注入
      String methodInfo = method.getDeclaringClass().getName() + "." + method.getName();
      throw new RuntimeException("OAuth2认证失败，不能找到接口绑定的Activator内容: " + methodInfo);
    }
    // 验证AppKey是否有效
    if (!activator.as(WxConfig.class).checkRequestAppKey()) {
      throw ECM.exception("-9", "OAuth2认证失败，请求使用的应用唯一标识异常: " + WxUtils.getAppKey());
    }
    return activator;
  }

}
