package net.icgear.three.weixin.open.handler;

import java.util.function.Function;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.suisrc.core.exception.ResponseException;
import com.suisrc.core.security.ScCrypto;
import com.suisrc.jaxrsapi.core.ApiActivator;

import net.icgear.three.weixin.core.WxConfig;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.utils.WxUtils;

/**
 * 
 * 增加一下内容
 * 
 * 增加授权方式
 * type:
 * nul: 网页授权登录(空)
 * wqr: 企业扫描登录
 * cqr: 普通扫描登录
 * 
 * 
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class DefaultWxOAuth2Handler extends AlternativeWxOAuth2Handler {
    
    /**
     * 获取令牌的临时授权和永久授权
     */
    @Override
    public String getOAuth2State(String token) {
        String url = WxUtils.getContainerRequestContext().getUriInfo().getPath();
        int offset = url.indexOf('/', 1);
        if (offset <= 0) {
            return "错误：无法验证请求访问的地址";
        }
        String auk = url.substring(1, offset);
        WxUtils.setAppKey(); // 尝试获取访问key
        String key = WxUtils.getAppKey();
        String base = auk + "|" + (key != null ? key : "") + "|";
        //-------------------------------------------------------
        if (token != null && !token.isEmpty()) {
            // 永久授权令牌
            String state = crypto.encrypt(base + token);
            return "static_" + state;
        } else {
            // 临时授权令牌(包含过期时间)
            String tmp2 = ScCrypto.genRandomStr(7);
            String state = markState(base + tmp2, 0);
            return "tmp_" + state;
        }
    }
    
    /**
     * 验证授权标识的有效性
     */
    @Override
    protected String verifyState(ApiActivator activator, String state, boolean ignore) {
        if (state == null) {
            return null;
        }
        if (state.startsWith("static_")) {
            // 永久授权验证
            String state2 = state.substring("static_".length());
            String sState = super.verifyState(activator, state2, true);
            if (sState == null) {
                return null;
            }
            WxConfig config = activator.as(WxConfig.class);
            String auk = config.getAppUniqueKey();
            String key = WxUtils.getAppKey();
            String token = config.getWxToken();
            String tState = auk + "|" + (key != null ? key : "") + "|" + token;
            //-------------------------------------------------------
            if (sState.equals(tState)) {
                return "none";
            } else {
                logger.warning("重定向授权请求验证失败，请求使用的密匙：" + sState);
                return null;
            }
        } else if (state.startsWith("tmp_")) {
            // 临时授权验证
            String state2 = state.substring("tmp_".length());
            String sState = super.verifyState(activator, state2, ignore);
            if (sState == null) {
                return null;
            }
            WxConfig config = activator.as(WxConfig.class);
            String auk = config.getAppUniqueKey();
            String key = WxUtils.getAppKey();
            String tState = auk + "|" + (key != null ? key : "") + "|";
            //-------------------------------------------------------
            if (sState.startsWith(tState)) {
                return "none";
            } else {
                logger.warning("重定向授权请求验证失败，请求使用的密匙：" + sState);
                return null;
            }
        } else if (state.startsWith("qr_")) {
            // 扫码时间太长，忽略缓存
            String state2 = state.substring("qr_".length());
            String sState = super.verifyState(activator, state2, true);
            return sState;
        } else {
            // 普通授权验证
            return super.verifyState(activator, state, ignore);
        }
    }

    /**
     * 重定向到微信服务器
     * 
     * 根据type的不同，使用不用的重定向域
     */
    @Override
    protected void redirectWxOAuth2Server(ApiActivator activator, Function<String, Object> getter, String type,
            String appid, String scope, Integer agentid, String state, String locationUri) {
        if (type == null || type.isEmpty()) {
            // 如果type没有，使用请求中的type中查找
            Object typeObj = getter.apply("type");
            type = typeObj == null ? null : typeObj.toString();
        } else if (type.equals("none")) {
            // 强制为空指定
            type = null;
        }
        if (type == null) {
            // 普通重定向跳转
            super.redirectWxOAuth2Server(activator, getter, type, appid, scope, agentid, state, locationUri);
        } else if ("wqr".equals(type)) {
            // 企业微信扫码重定向跳转
            String redirectUri = redirectWeixinService.wqrConnect(appid, locationUri, agentid, "qr_" + state);
            String messageInfo = "重定向到微信扫码授权登录: " + redirectUri;
            redirectWeixinService.redirectWeixinServer(messageInfo, redirectUri);
        } else {
            // 无法处理
            String uri = WxUtils.getContainerRequestContext().getUriInfo().getAbsolutePath().toString();
            String errmsg = "不支持的重定向类型：" + type;
            throw new ResponseException(errmsg + "[uri:" + uri + "]", re -> 
                Response.ok(errmsg).type(MediaType.TEXT_PLAIN + WxConsts.MediaType_UTF_8).build());
        }
    }
}
