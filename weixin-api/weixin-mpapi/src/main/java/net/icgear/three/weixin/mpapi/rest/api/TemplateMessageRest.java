package net.icgear.three.weixin.mpapi.rest.api;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.TemplateDataInfo;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.core.utils.WxUtils;
import net.icgear.three.weixin.mpapi.rest.dto.template.ApiAddTemplateBody;
import net.icgear.three.weixin.mpapi.rest.dto.template.ApiAddTemplateResult;
import net.icgear.three.weixin.mpapi.rest.dto.template.AppPrivateTemplateListResult;
import net.icgear.three.weixin.mpapi.rest.dto.template.DelPrivateTemplateBody;
import net.icgear.three.weixin.mpapi.rest.dto.template.IndustryGetResult;
import net.icgear.three.weixin.mpapi.rest.dto.template.IndustrySetBody;
import net.icgear.three.weixin.mpapi.rest.dto.template.MiniprogramInfo;
import net.icgear.three.weixin.mpapi.rest.dto.template.SubscribeTemplateMsgBody;
import net.icgear.three.weixin.mpapi.rest.dto.template.TemplateMessageBody;
import net.icgear.three.weixin.mpapi.rest.dto.template.TemplateMessageResult;

/**
 * 模板消息接口
 * 
 * 模板消息仅用于公众号向用户发送重要的服务通知，只能用于符合其要求的服务场景中，如信用卡刷卡通知，商品购买成功通知等。不支持广告等营销类消息以及其它所有可能对用户造成骚扰的消息。
 * 
 * 关于使用规则，请注意：
 * 1、所有服务号都可以在功能->添加功能插件处看到申请模板消息功能的入口，但只有认证后的服务号才可以申请模板消息的使用权限并获得该权限；
 * 2、需要选择公众账号服务所处的2个行业，每月可更改1次所选行业；
 * 3、在所选择行业的模板库中选用已有的模板进行调用；
 * 4、每个账号可以同时使用25个模板。
 * 5、当前每个账号的模板消息的日调用上限为10万次，单个模板没有特殊限制。【2014年11月18日将接口调用频率从默认的日1万次提升为日10万次，可在MP登录后的开发者中心查看】。
 *   当账号粉丝数超过10W/100W/1000W时，模板消息的日调用上限会相应提升，以公众号MP后台开发者中心页面中标明的数字为准。
 *   
 * 关于接口文档，请注意：
 * 1、模板消息调用时主要需要模板ID和模板中各参数的赋值内容；
 * 2、模板中参数内容必须以".DATA"结尾，否则视为保留字；
 * 3、模板保留符号"{{ }}"
 * 
 * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1433751277
 * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1433751288
 * 事件推送
 * TEMPLATESENDJOBFINISH
 * 
 * @see com.suisrc.weixin.mp.msg.event.TemplatesendjobfinishEvent
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin")
public interface TemplateMessageRest {

    /**
     * 设置所属行业
     * 
     * 设置行业可在微信公众平台后台完成，每月可修改行业1次，帐号仅可使用所属行业中相关的模板，为方便第三方开发者，提供通过接口调用的方式来修改账号所属行业，具体如下：
     * 接口调用请求说明
     * http请求方式: POST
     * https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token=ACCESS_TOKEN
     * POST数据说明
     * POST数据示例如下：
     *       {
     *           "industry_id1":"1",
     *           "industry_id2":"4"
     *        }
     * 参数说明
     * 参数.            是否必须.    说明
     * access_token    是.         接口调用凭证
     * industry_id1    是.         公众号模板消息所属行业编号
     * industry_id2    是.         公众号模板消息所属行业编号
     * 
     * 行业代码查询详见： @see com.qq.weixin.mp.common.IndustryCode
     */
    @POST
    @Path("template/api_set_industry")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode setIndustry(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            IndustrySetBody body);
    
    default WxErrCode setIndustry(String industryId1, String industryId2) {
        IndustrySetBody body = new IndustrySetBody();
        body.setIndustryId1(industryId1);
        body.setIndustryId2(industryId2);
        return setIndustry(null, body);
    }
    
    /**
     * 获取设置的行业信息
     * 
     * 获取帐号设置的行业信息。可登录微信公众平台，在公众号后台中查看行业信息。为方便第三方开发者，提供通过接口调用的方式来获取帐号所设置的行业信息，具体如下:
     * 接口调用请求说明
     * http请求方式：GET
     * https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token=ACCESS_TOKEN
     * 参数说明
     * 参数.          是否必须 .   说明
     * access_token    是.       接口调用凭证
     * 返回说明
     * 正确调用后的返回示例：
     * {
     * "primary_industry":{"first_class":"运输与仓储","second_class":"快递"},
     * "secondary_industry":{"first_class":"IT科技","second_class":"互联网|电子商务"}
     * }
     * 返回参数说明
     * 参数  是否必填    说明
     * access_token    是   接口调用凭证
     * primary_industry    是   帐号设置的主营行业
     * secondary_industry  是   帐号设置的副营行业
     */
    @GET
    @Path("template/get_industry")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    IndustryGetResult getIndustry(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken);
    
    default IndustryGetResult getIndustry() {
        return getIndustry(null);
    }
    
    /**
     * 获得模板ID
     * 
     * 从行业模板库选择模板到帐号后台，获得模板ID的过程可在微信公众平台后台完成。为方便第三方开发者，提供通过接口调用的方式来获取模板ID，具体如下：
     * 接口调用请求说明
     * http请求方式: POST
     * https://api.weixin.qq.com/cgi-bin/template/api_add_template?access_token=ACCESS_TOKEN
     * 
     * POST数据说明
     * POST数据示例如下：
     *       {
     *            "template_id_short":"TM00015"
     *        }
     * 参数说明
     * 参数.              是否必须.    说明
     * access_token       是.        接口调用凭证
     * template_id_short  是.        模板库中模板的编号，有“TM**”和“OPENTMTM**”等形式
     * 
     * 返回码说明
     * 在调用模板消息接口后，会返回JSON数据包。正常时的返回JSON数据包示例：
     *     {
     *            "errcode":0,
     *            "errmsg":"ok",
     *            "template_id":"Doclyl5uP7Aciu-qZ7mJNPtWkbkYnWBWVja26EGbNyk"
     *     }
     */
    @POST
    @Path("template/api_add_template")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ApiAddTemplateResult getApiAddTemplate(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            ApiAddTemplateBody body);
    
    default ApiAddTemplateResult getApiAddTemplate(String templateIdShort) {
        ApiAddTemplateBody body = new ApiAddTemplateBody();
        body.setTemplateIdShort(templateIdShort);
        return getApiAddTemplate(null, body);
    }
    
    /**
     * 获取模板列表
     * 
     * 获取已添加至帐号下所有模板列表，可在微信公众平台后台中查看模板列表信息。为方便第三方开发者，提供通过接口调用的方式来获取帐号下所有模板信息，具体如下:
     * 接口调用请求说明
     * http请求方式：GET
     * https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token=ACCESS_TOKEN
     * 
     * 参数说明
     * 参数.           是否必须.    说明
     * access_token    是.        接口调用凭证
     * 
     * 返回说明
     * 正确调用后的返回示例：
     * {   
     *  "template_list": [{
     *       "template_id": "iPk5sOIt5X_flOVKn5GrTFpncEYTojx6ddbt8WYoV5s",
     *       "title": "领取奖金提醒",
     *       "primary_industry": "IT科技",
     *       "deputy_industry": "互联网|电子商务",
     *       "content": "{ {result.DATA} }\n\n领奖金额:{ {withdrawMoney.DATA} }\n领奖  时间:{ {withdrawTime.DATA} }\n银行信息:{ {cardInfo.DATA} }\n到账时间:  { {arrivedTime.DATA} }\n{ {remark.DATA} }",
     *       "example": "您已提交领奖申请\n\n领奖金额：xxxx元\n领奖时间：2013-10-10 12:22:22\n银行信息：xx银行(尾号xxxx)\n到账时间：预计xxxxxxx\n\n预计将于xxxx到达您的银行卡"
     *    }]
     * }
     */
    @GET
    @Path("template/get_all_private_template")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    AppPrivateTemplateListResult getAppPrivateTemplate(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken);
    
    default AppPrivateTemplateListResult getAppPrivateTemplate() {
        return getAppPrivateTemplate(null);
    }
    
    /**
     * 删除模板
     * 
     * 删除模板可在微信公众平台后台完成，为方便第三方开发者，提供通过接口调用的方式来删除某帐号下的模板，具体如下：
     * 接口调用请求说明
     * http请求方式：POST
     * https://api.weixin.qq.com/cgi-bin/template/del_private_template?access_token=ACCESS_TOKEN
     * POST数据说明如下：
     *  {
     *      "template_id" : "Dyvp3-Ff0cnail_CDSzk1fIc6-9lOkxsQE7exTJbwUE"
     *  }
     * 参数说明
     * 参数.          是否必须.    说明
     * access_token    是.       接口调用凭证
     * template_id     是.       公众帐号下模板消息ID
     * 返回说明
     * 在调用接口后，会返回JSON数据包。正常时的返回JSON数据包示例：
     * {
     *    "errcode" : 0,
     *    "errmsg" : "ok"
     * }
     */
    @POST
    @Path("template/del_private_template")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode delPrivateTemplate(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            DelPrivateTemplateBody body);
    
    default WxErrCode delPrivateTemplate(String templateId) {
        DelPrivateTemplateBody body = new DelPrivateTemplateBody();
        body.setTemplateId(templateId);
        return delPrivateTemplate(null, body);
    }
    
    /**
     * 
     * 发送模板消息
     * 
     * 接口调用请求说明
     * http请求方式: POST
     * https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN
     * POST数据说明
     * POST数据示例如下：
     *       {
     *            "touser":"OPENID",
     *            "template_id":"ngqIpbwh8bUfcSsECmogfXcV14J0tQlEpBO27izEYtY",
     *            "url":"http://weixin.qq.com/download",  
     *            "miniprogram":{
     *              "appid":"xiaochengxuappid12345",
     *              "pagepath":"index?foo=bar"
     *            },          
     *            "data":{
     *                    "first": {
     *                        "value":"恭喜你购买成功！",
     *                        "color":"#173177"
     *                    },
     *                    "keynote1":{
     *                        "value":"巧克力",
     *                        "color":"#173177"
     *                    },
     *                    "keynote2": {
     *                        "value":"39.8元",
     *                        "color":"#173177"
     *                    },
     *                    "keynote3": {
     *                        "value":"2014年9月22日",
     *                        "color":"#173177"
     *                    },
     *                    "remark":{
     *                        "value":"欢迎再次购买！",
     *                        "color":"#173177"
     *                    }
     *            }
     *        }
     *        
     * 返回码说明
     * 在调用模板消息接口后，会返回JSON数据包。正常时的返回JSON数据包示例：
     *     {
     *            "errcode":0,
     *            "errmsg":"ok",
     *            "msgid":200228332
     *     }
     */
    @POST
    @Path("message/template/send")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TemplateMessageResult sendTemplateMessage(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            TemplateMessageBody body);
    default TemplateMessageResult sendTemplateMessage(TemplateMessageBody body) {
        return sendTemplateMessage(null, body);
    }
    default TemplateMessageResult sendTemplateMessage1(String touser, String templateId, String url, 
            MiniprogramInfo miniprogram, Map<String, TemplateDataInfo> data) {
        TemplateMessageBody body = new TemplateMessageBody();
        body.setTouser(touser);
        body.setTemplateId(templateId);
        body.setUrl(url);
        body.setMiniprogram(miniprogram);
        body.setData(data);
        return sendTemplateMessage(null, body);
    }
    default TemplateMessageResult sendTemplateMessage(String touser, String url, MiniprogramInfo miniprogram, 
            Object data) {
        return WxUtils.sendTemplateMessage(this, data, (_id, _dd) -> 
            sendTemplateMessage1(touser, _id, url, miniprogram, _dd));
    }
    
    //----------------------------------------------------------------------------------------一次订阅模版消息

    /**
     * 一次性订阅消息
     * 开发者可以通过一次性订阅消息授权让微信用户授权第三方移动应用（接入说明）或公众号，获得发送一次订阅消息给到授权微信用户的机会。授权微信用户可以不需要关注公众号。
     * 微信用户每授权一次，开发者可获得一次下发消息的权限。对于已关注公众号的，消息将下发到公众号会话；未关注公众号的，将下发到服务通知。
     * 
     * 公众号或网页使用一次性订阅消息流程如下：
     * 
     * 第一步：需要用户同意授权，获取一次给用户推送一条订阅模板消息的机会
     *    通过subscribemsgUrl获取url让用户跳转授权
     * 
     * 第二步：通过API推送订阅模板消息给到授权微信用户
     *    通过subscribeMessage向用户发送消息
     * 
     */

    /**
     * 获取用户跳转地址
     * 
     * 在确保微信公众帐号拥有订阅消息授权的权限的前提下（已认证的公众号即有权限，可登陆公众平台在接口权限列表处查看），引导用户在微信客户端打开如下链接：
     * https://mp.weixin.qq.com/mp/subscribemsg?action=get_confirm&appid=wxaba38c7f163da69b&scene=1000&template_id=1uDxHNXwYQfBmXOfPJcjAS3FynHArD8aWMEFNRGSbCc&
     * redirect_url=http%3a%2f%2fsupport.qq.com&reserved=test#wechat_redirect
     * 
     * 参数说明
     * 参数.            是否必须.    说明
     * action           是.         直接填get_confirm即可
     * appid            是.         公众号的唯一标识
     * scene            是.         重定向后会带上scene参数，开发者可以填0-10000的整形值，用来标识订阅场景值
     * template_id      是.         订阅消息模板ID，登录公众平台后台，在接口权限列表处可查看订阅模板ID
     * redirect_url     是.         授权后重定向的回调地址，请使用UrlEncode对链接进行处理。注：要求redirect_url的域名要跟登记的业务域名一致，且业务域名不能带路径。业务域名需登录公众号，在设置-公众号设置-功能设置里面对业务域名设置。
     * reserved         否.         用于保持请求和回调的状态，授权请后原样带回给第三方。该参数可用于防止csrf攻击（跨站请求伪造攻击），建议第三方带上该参数，可设置为简单的随机数加session进行校验，开发者可以填写a-zA-Z0-9的参数值，最多128字节，要求做urlencode
     * #wechat_redirect 是.         无论直接打开还是做页面302重定向时，必须带此参数
     * 
     * 用户同意或取消授权后会返回相关信息
     * 如果用户点击同意或取消授权，页面将跳转至：
     * redirect_url/?openid=OPENID&template_id=TEMPLATE_ID&action=ACTION&scene=SCENE
     * 参数说明
     * 参数.            说明
     * openid          用户唯一标识，只在用户确认授权时才会带上
     * template_id     订阅消息模板ID
     * action          用户点击动作，”confirm”代表用户确认授权，”cancel”代表用户取消授权
     * scene           订阅场景值
     * reserved        请求带入原样返回
     */
    
    /**
     * 通过API推送订阅模板消息给到授权微信用户
     * 接口请求说明
     * http请求方式: post
     * https://api.weixin.qq.com/cgi-bin/message/template/subscribe?access_token=ACCESS_TOKEN
     * post数据示例
     * {
     * “touser”:”OPENID”,
     * “template_id”:”TEMPLATE_ID”,
     * “url”:”URL”,
     * “scene”:”SCENE”,
     * “title”:”TITLE”,
     * “data”:{
     *     “content”:{
     *         “value”:”VALUE”,
     *         “color”:”COLOR”
     *      }
     * }
     * }
     * 返回说明
     * 在调用接口后，会返回JSON数据包。正常时的返回JSON数据包示例：
     * {
     *   “errcode”:0,
     *   “errmsg”:”ok”
     * }
     */
    @POST
    @Path("message/template/subscribe")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode sendSubscribeMessage(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            SubscribeTemplateMsgBody body);
    default WxErrCode sendSubscribeMessage(SubscribeTemplateMsgBody body) {
        return sendSubscribeMessage(null, body);
    }
    default WxErrCode sendSubscribeMessage1(String title, String scene, String touser, String templateId, String url, MiniprogramInfo miniprogram, 
            Map<String, TemplateDataInfo> data) {
        SubscribeTemplateMsgBody body = new SubscribeTemplateMsgBody();
        body.setTitle(title);
        body.setScene(scene);
        body.setTouser(touser);
        body.setTemplateId(templateId);
        body.setUrl(url);
        body.setMiniprogram(miniprogram);
        body.setData(data);
        return sendSubscribeMessage((String)null, body);
    }
    default WxErrCode sendSubscribeMessage(String title, String scene, String touser, String url, MiniprogramInfo miniprogram, 
            Object data) {
        return WxUtils.sendTemplateMessage(this, data, (_id, _dd) -> 
            sendSubscribeMessage1(title, scene, touser, _id, url, miniprogram, _dd));
    }
}
