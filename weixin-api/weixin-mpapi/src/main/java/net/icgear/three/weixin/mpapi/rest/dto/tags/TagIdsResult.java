package net.icgear.three.weixin.mpapi.rest.dto.tags;

import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("TagIdsResult")
public class TagIdsResult extends WxErrCode {
    private static final long serialVersionUID = 6306566150752465315L;
    
    @ApiModelProperty("被置上的标签")
    @JsonProperty("tagid_list")
    private Integer[] tagidList;
    
    /**
     * 被置上的标签
     */
    public Integer[] getTagidList() {
        return tagidList;
    }
    /**
     * 被置上的标签
     */
    public void setTagidList(Integer[] tagidList) {
        this.tagidList = tagidList;
    }
}
