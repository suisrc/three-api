package net.icgear.three.weixin.mpapi.rest.dto.tags;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("TagUsersBody")
public class TagUsersBody {
    
    @ApiModelProperty("标签ID")
    @NotNull("tagid属性为空")
    @JsonProperty("tagid")
    private Integer tagid;
    
    @ApiModelProperty("第一个拉取的OPENID，不填默认从头开始拉取")
    @JsonProperty("next_openid")
    private String nextOpenid;
    /**
     * 标签ID
     */
    public Integer getTagid() {
        return tagid;
    }
    /**
     * 标签ID
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
    /**
     * 第一个拉取的OPENID，不填默认从头开始拉取
     */
    public String getNextOpenid() {
        return nextOpenid;
    }
    /**
     * 第一个拉取的OPENID，不填默认从头开始拉取
     */
    public void setNextOpenid(String nextOpenid) {
        this.nextOpenid = nextOpenid;
    }
}