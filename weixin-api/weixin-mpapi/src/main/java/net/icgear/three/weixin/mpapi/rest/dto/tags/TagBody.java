package net.icgear.three.weixin.mpapi.rest.dto.tags;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("TagBody")
public class TagBody {

    @JsonProperty("tag")
    private Tag tag;

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
