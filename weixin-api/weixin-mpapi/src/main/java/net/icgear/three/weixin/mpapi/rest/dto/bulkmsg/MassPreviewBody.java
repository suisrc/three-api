package net.icgear.three.weixin.mpapi.rest.dto.bulkmsg;

/**
 * 
 * 参数.       说明
 * touser     接收消息用户对应该公众号的openid，该字段也可以改为towxname，以实现对微信号的预览
 * msgtype    群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，视频为video，卡券为wxcard
 * media_id   用于群发的消息的media_id
 * content    发送文本消息时文本的内容
 * @author Y13
 *
 */
public class MassPreviewBody extends AbstractMassSendInfo {

    /**
     * 接收消息用户对应该公众号的openid，该字段也可以改为towxname，以实现对微信号的预览
     */
    private String touser;
    
    /**
     * 接收消息用户对应该公众号的openid，该字段也可以改为towxname，以实现对微信号的预览
     */
    private String towxname;

    /**
     * 获取接收消息用户对应该公众号的openid，该字段也可以改为towxname，以实现对微信号的预览
     * @return the touser
     */
    public String getTouser() {
        return touser;
    }

    /**
     * 设定接收消息用户对应该公众号的openid，该字段也可以改为towxname，以实现对微信号的预览
     * @param touser the touser to set
     */
    public void setTouser(String touser) {
        this.touser = touser;
    }

    /**
     * 获取接收消息用户对应该公众号的openid，该字段也可以改为towxname，以实现对微信号的预览
     * @return the towxname
     */
    public String getTowxname() {
        return towxname;
    }

    /**
     * 设定接收消息用户对应该公众号的openid，该字段也可以改为towxname，以实现对微信号的预览
     * @param towxname the towxname to set
     */
    public void setTowxname(String towxname) {
        this.towxname = towxname;
    }
}
