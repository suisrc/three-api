package net.icgear.three.weixin.mpapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("UserInfoByBatchResult")
public class UserInfoByBatchResult extends WxErrCode {
    private static final long serialVersionUID = -5208646990498534893L;
    
    @JsonProperty("user_info_list")
    private UserInfoResult[] userInfoList;
    
    public UserInfoResult[] getUserInfoList() {
        return userInfoList;
    }
    
    public void setUserInfoList(UserInfoResult[] userInfoList) {
        this.userInfoList = userInfoList;
    }
}