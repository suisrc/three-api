package net.icgear.three.weixin.mpapi;

import java.util.Set;

import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.three.weixin.core.AbstractWeixinActivator;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.core.handler.WxTokenHandler;
import net.icgear.three.weixin.mpapi.rest.api.MpTokenServiceRest;
import net.icgear.three.weixin.mpapi.rest.api.TemplateMessageRest;
import net.icgear.three.weixin.mpapi.rest.dto.template.ApiAddTemplateResult;

/**
 * 对于本公司而已，存放了本公司信息直接访问的激活器
 * 
 * @author Y13
 */
public abstract class MpActivator extends AbstractWeixinActivator {

    @Override
    public Set<Class<?>> getClasses() {
        return ReflectionUtils.getTypesAnnotatedWith(RemoteApi.class, true, MpTokenServiceRest.class.getPackage().getName());
    }

    /**
     * 获取默认基础链接地址
     */
    @Override
    protected String getDefaultBaseUrl() {
        return "https://api.weixin.qq.com";
    }
    
    private MpTokenServiceRest tokenRest;

    /**
     * 获取服务控制器
     * @return
     */
    public MpTokenServiceRest getAccessTokenRest() {
        if (tokenRest == null) {
            tokenRest = createTokenRest();
        }
        return tokenRest;
    }
    
    /**
     * 获取访问控制器
     * @return
     */
    protected MpTokenServiceRest createTokenRest() {
        return getApiImplement(MpTokenServiceRest.class);
    }
    
    /**
     * 获取访问令牌控制器
     */
    @Override
    protected WxTokenHandler getWxTokenHandler(String tokenKey) {
        // 对于本公司而已，其中tokenKey可以忽略
        return () -> getAccessTokenRest().getToken(getWxAppId(), getWxAppSecret());
    }
    
    /**
     * 增加模版到账户
     */
    @Override
    protected String getTemplateIdFromWeixin(String shortId) {
        TemplateMessageRest tmRest = getApiImplement(TemplateMessageRest.class);
        ApiAddTemplateResult res = tmRest.getApiAddTemplate(shortId);
        if (res.getErrcode() == 40102L) {
            // 由于无效的行业代码查导致的
            // 重置行业编码
            String[] industryIds = getIndustryIds();
            if (industryIds != null && industryIds.length > 0) {
                boolean can = isIndustryEdit();
                if (can) {
                    String industryId1 = industryIds[0];
                    String industryId2 = industryIds.length > 1 ? industryIds[1] : null;
                    WxErrCode wec = tmRest.setIndustry(industryId1, industryId2);
                    {
                        // log
                        String msg = "执行更新微信账户【" + getWxAppId() + "】的行业编码[";
                        msg += industryId1;
                        if (industryId2 != null) {
                            msg += ", " + industryId2;
                        }
                        msg += "]";
                        if (wec.isErr()) {
                            msg += "失败: " + wec.printErr();
                        } else {
                            msg += "成功";
                        }
                        logger.warning(msg);
                    }
                    if (!wec.isErr()) {
                        // 重新增加模版到账户中
                        res = tmRest.getApiAddTemplate(shortId);
                    }
                }
            }
        }
        
        if (res.getTemplateId() == null || res.getTemplateId().isEmpty()) {
            String msg = "增加模版'" + shortId + "'到微信账户【" + getWxAppId() + "】失败：";
            logger.warning(msg + res.printErr());
        }
        return res.getTemplateId();
    }
}
