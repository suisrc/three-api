package net.icgear.three.weixin.mpapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.mpapi.rest.dto.kf.msg.KfReplyBaseMessage;
import net.icgear.three.weixin.mpapi.rest.dto.kf.msg.KfReplyCustomTyping;

/**
 * 当用户和公众号产生特定动作的交互时（具体动作列表请见下方说明），微信将会把消息数据推送给开发者，
 * 开发者可以在一段时间内（目前修改为48小时）调用客服接口，通过POST一个JSON数据包来发送消息给普通用户。
 * 此接口主要用于客服等有人工消息处理环节的功能，方便开发者为用户提供更加优质的服务。
 * 
 * 
 * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140547
 * 
 * @author Y13
 */
@RemoteApi("cgi-bin/message/custom")
public interface WxKfAccount2Rest {
    
    /**
     * 客服接口-发消息
     * 接口调用请求说明
     * http请求方式: POST
     * https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=ACCESS_TOKEN
     * 各消息类型所需的JSON数据包如下：
     * 发送文本消息
     * {
     *     "touser":"OPENID",
     *     "msgtype":"text",
     *     "text":
     *     {
     *          "content":"Hello World"
     *     }
     * }
     * 
     * 参数.           是否必须.    说明
     * access_token   是.         调用接口凭证
     * touser         是.         普通用户openid
     * msgtype        是.         消息类型，文本为text，图片为image，语音为voice，视频消息为video，音乐消息为music，
     *                            图文消息（点击跳转到外链）为news，图文消息（点击跳转到图文消息页面）为mpnews，卡券为wxcard，小程序为miniprogrampage
     * content        是.         文本消息内容
     * media_id       是.         发送的图片/语音/视频/图文消息（点击跳转到图文消息页）的媒体ID
     * thumb_media_id 是.         缩略图/小程序卡片图片的媒体ID，小程序卡片图片建议大小为520*416
     * title          否.         图文消息/视频消息/音乐消息/小程序卡片的标题
     * description    否.         图文消息/视频消息/音乐消息的描述
     * musicurl       是.         音乐链接
     * hqmusicurl     是.         高品质音乐链接，wifi环境优先使用该链接播放音乐
     * url            否.         图文消息被点击后跳转的链接
     * picurl         否.         图文消息的图片链接，支持JPG、PNG格式，较好的效果为大图640*320，小图80*80
     * appid          是.         小程序的appid，要求小程序的appid需要与公众号有关联关系
     * pagepath       是.         小程序的页面路径，跟app.json对齐，支持参数，比如pages/index/index?foo=bar
     * 
     * @see com.qq.weixin.mp.param.kf
     */
    @POST
    @Path("send")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode sendKfMessage(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            KfReplyBaseMessage msg);
    default WxErrCode sendKfMessage(KfReplyBaseMessage msg) {
        return sendKfMessage(null, msg);
    }
    
    /**
     * 客服输入状态 开发者可通过调用“客服输入状态”接口，返回客服当前输入状态给用户。 微信客户端效果图如下：
     * 
     * 此接口需要客服消息接口权限。 
     * 1. 如果不满足发送客服消息的触发条件，则无法下发输入状态。 
     * 2. 下发输入状态，需要客服之前30秒内跟用户有过消息交互。 
     * 3. 在输入状态中（持续15s），不可重复下发输入态。 
     * 4. 在输入状态中，如果向用户下发消息，会同时取消输入状态。 接口调用请求说明
     * 
     * http请求方式: POST
     * https://api.weixin.qq.com/cgi-bin/message/custom/typing?access_token=ACCESS_TOKEN
     * 
     * JSON数据包如下：
     * { "touser":"OPENID", "command":"Typing"} 
     * 预期返回：
     * { "errcode":0, "errmsg":"ok"} 参数说明
     * 
     * 参数.          是否必须.  说明 
     * access_token  是.       调用接口凭证 
     * touser        是.       普通用户（openid） 
     * command       是.       "Typing"：对用户下发“正在输入"状态,"CancelTyping"：取消对用户的”正在输入"状态 返回码说明
     * 
     * 参数.    说明 
     * 45072   command字段取值不对 
     * 45080   下发输入状态，需要之前30秒内跟用户有过消息交互 
     * 45081   已经在输入状态，不可重复下发
     * 
     */
    @POST
    @Path("typing")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode customTyping(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            KfReplyCustomTyping msg);
    default WxErrCode customTyping(KfReplyCustomTyping msg) {
        return customTyping(null, msg);
    }
    default WxErrCode customTyping(String touser, String command) {
        KfReplyCustomTyping msg = new KfReplyCustomTyping();
        msg.setTouser(touser);
        msg.setCommand(command);
        return customTyping(null, msg);
    }
    default WxErrCode customTypingByTyping(String touser) {
        return customTyping(touser, "Typing");
    }
    default WxErrCode customTypingByCancelTyping(String touser) {
        return customTyping(touser, "CancelTyping");
    }

}
