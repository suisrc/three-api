package net.icgear.three.weixin.mpapi.rest.dto.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("OpenIdsBody")
public class OpenIdsBody {
    
    @ApiModelProperty("用户openid")
    @NotNull("openid属性为空")
    @JsonProperty("openid")
    private String openid;
    
    /**
     * 用户openid
     */
    public String getOpenid() {
        return openid;
    }
    /**
     * 用户openid
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }
}