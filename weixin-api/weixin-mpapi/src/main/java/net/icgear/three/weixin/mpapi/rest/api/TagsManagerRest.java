package net.icgear.three.weixin.mpapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.mpapi.rest.dto.tags.BatchTagBody;
import net.icgear.three.weixin.mpapi.rest.dto.tags.BatchblacklistBody;
import net.icgear.three.weixin.mpapi.rest.dto.tags.GetblacklistBody;
import net.icgear.three.weixin.mpapi.rest.dto.tags.GetblacklistResult;
import net.icgear.three.weixin.mpapi.rest.dto.tags.OpenIdsBody;
import net.icgear.three.weixin.mpapi.rest.dto.tags.Tag;
import net.icgear.three.weixin.mpapi.rest.dto.tags.TagBody;
import net.icgear.three.weixin.mpapi.rest.dto.tags.TagIdsResult;
import net.icgear.three.weixin.mpapi.rest.dto.tags.TagResult;
import net.icgear.three.weixin.mpapi.rest.dto.tags.TagsResult;

/**
 * 用户标签管理
 * 
 * 开发者可以使用用户标签管理的相关接口，实现对公众号的标签进行创建、查询、修改、删除等操作，也可以对用户进行打标签、取消标签等操作。
 * 
 * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140837
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin/tags")
public interface TagsManagerRest {
    
    /**
    * 1. 创建标签
    * 一个公众号，最多可以创建100个标签。
    * -:createTags
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/create?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {   "tag" : {     "name" : "广东"   } }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭据
    * name                 是.        标签名（30个字符以内）
    * 说明：
    * 
    * 
    * 返回结果：
    * {   "tag":{ "id":134,"name":"广东"   } }
    * 
    * 参数说明：
    * 参数.                说明
    * id                   标签id，由微信分配
    * name                 标签名，UTF8编码
    * 
    */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TagResult createTags(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken, 
            TagBody body);
    default TagResult createTags(String name) {
        TagBody body = new TagBody();
        Tag tag = new Tag();
        tag.setName(name);
        body.setTag(tag);
        return createTags((String)null, body);
    }
    
    /**
    * 2. 获取公众号已创建的标签
    * -:getTags
    * 
    * 请求方式：GET（请使用https协议） 
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/get?access_token=ACCESS_TOKEN
    * 
    * 返回结果：
    * {   "tags":[
    * {       "id":1,       "name":"每天一罐可乐星人",       "count":0 },
    * {   "id":2,   "name":"星标组",   "count":0 },
    * {   "id":127,   "name":"广东",   "count":5 }
    * ] }
    * 
    * 参数说明：
    * 参数.                说明
    * id                   标签ID
    * name                 标签名称
    * count                此标签下粉丝数
    * 
    */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TagsResult getTags(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken);
    default TagsResult getTags() {
        return getTags((String)null);
    }
    
    /**
     * 3. 编辑标签
     * -:updateTags
     * 
     * 请求方式：POST（请使用https协议）
     * 
     * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/update?access_token=ACCESS_TOKEN
     * 
     * 请求包体：
     * {   "tag" : {     "id" : 134,     "name" : "广东人"   } }
     * 
     * 返回结果：
     * {   "errcode":0,   "errmsg":"ok" }
     * 
     */
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode updateTags(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken, TagBody body);
    default WxErrCode updateTags(Integer id, String name) {
        TagBody body = new TagBody();
        Tag tag = new Tag();
        tag.setId(id);
        tag.setName(name);
        return updateTags((String)null, body);
    }
   
   /**
    * 4. 删除标签
    * 请注意，当某个标签下的粉丝超过10w时，后台不可直接删除标签。此时，开发者可以对该标签下的openid列表，先进行取消标签的操作，直到粉丝数不超过10w后，才可直接删除该标签。
    * -:deleteTags
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（请使用https协议） 
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 
    * 说明：
    * 请求包体：
    * {   "tag":{        "id" : 134   } }
    * 返回结果：
    * {   "errcode":0,   "errmsg":"ok" }
    * 
    * 返回结果：
    * 
    * 参数说明：
    */
    @POST
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode deleteTags(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken, TagBody body);
    default WxErrCode deleteTags(Integer id) {
        TagBody body = new TagBody();
        Tag tag = new Tag();
        tag.setId(id);
        return updateTags((String)null, body);
    }
      
   /**
   * 5. 获取标签下粉丝列表
   * -:getTagUsers
   * 
   * 请求方式：GET（请使用https协议） 
   * 
   * 请求地址：https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=ACCESS_TOKEN
   * 
   * 请求包体：
   * {   "tagid" : 134,   "next_openid":""}
   * 
   * 参数说明：
   * 参数.                必须.       说明
   * tagid                是.        标签ID
   * next_openid          否.        第一个拉取的OPENID，不填默认从头开始拉取
   * 
   * 返回结果：
   * {   "count":2,
   * "data":{
   * "openid":[  
   * "ocYxcuAEy30bX0NXmGn4ypqx3tI0",    
   * "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"  ]  
   * },  
   * "next_openid":"ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"
   * }
   * 
   * 参数说明：
   * 参数.                说明
   * count                这次获取的粉丝数量
   * data                 粉丝列表
   * next_openid          拉取列表最后一个用户的openid
   * 
   * @see net.icgear.three.weixin.mpapi.rest.api::getTagUsers
   */
   
   /**
    * 标签功能目前支持公众号为用户打上最多20个标签。
    * 1. 批量为用户打标签
    * -:batchTagging
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {   "openid_list" : [
    * "ocYxcuAEy30bX0NXmGn4ypqx3tI0",
    * "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"
    * ],
    * "tagid" : 134 
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * openid_list          是.        粉丝列表
    * tagid                是.        标签
    * 
    */
    @POST
    @Path("members/batchtagging")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode batchTagging(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken, BatchTagBody body);
    default WxErrCode batchTagging(Integer tagid, String... openids) {
        BatchTagBody body = new BatchTagBody();
        body.setOpenidList(openids);
        return batchTagging((String)null, body);
    }
   
   /**
    * 2. 批量为用户取消标签
    * 
    * 请求方式：POST（请使用https协议） 
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=ACCESS_TOKEN
    * 
    * 说明：
    * 请求包体：
    * {   "openid_list" : [
    * "ocYxcuAEy30bX0NXmGn4ypqx3tI0",
    * "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"
    * ],
    * "tagid" : 134 }
    * 返回结果：
    * {  
    * "errcode":0,   
    * "errmsg":"ok"
    * }
    * 错误码说明
    * 错误码   说明
    * -1    系统繁忙
    * 40032 每次传入的openid列表个数不能超过50个
    * 45159 非法的标签
    * 40003 传入非法的openid
    * 49003 传入的openid不属于此AppID
    * 
    */
    @POST
    @Path("members/batchuntagging")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode batchuntagging(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken, BatchTagBody body);
    default WxErrCode batchuntagging(Integer tagid, String... openids) {
        BatchTagBody body = new BatchTagBody();
        body.setOpenidList(openids);
        return batchuntagging((String)null, body);
    }
   
   /**
    * 3. 获取用户身上的标签列表
    * -:getIdList
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {   "openid" : "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y" }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * openid               是.        用户openid
    * 
    * 返回结果：
    * {   "tagid_list":[134, 2   ] }
    * 
    * 参数说明：
    * 参数.                说明
    * tagid_list           被置上的标签
    * 
    */
    @POST
    @Path("getidlist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TagIdsResult getIdList(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken, OpenIdsBody body);
    default TagIdsResult batchuntagging(String openid) {
        OpenIdsBody body = new OpenIdsBody();
        body.setOpenid(openid);
        return getIdList((String)null, body);
    }
    
    /**
    * 公众号可登录微信公众平台，对粉丝进行拉黑的操作。同时，我们也提供了一套黑名单管理API，以便开发者直接利用接口进行操作。
    * 1. 获取公众号的黑名单列表
    * 公众号可通过该接口来获取帐号的黑名单列表，黑名单列表由一串 OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
    * 该接口每次调用最多可拉取 10000 个OpenID，当列表数较多时，可以通过多次拉取的方式来满足需求。
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/members/getblacklist?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    * "begin_openid":"OPENID1"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * begin_openid         否.        为空时，默认从开头拉取。
    * 说明：
    * 
    * 返回结果：
    * {
    *  "total":23000,
    *  "count":10000,
    *  "data":{"openid":[
    *        "OPENID1",
    *        "OPENID2",
    *        "OPENID10000"
    *     ]
    *   },
    *   "next_openid":"OPENID10000"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * total                黑名单总数
    * count                当前拉取数量
    * data                 黑名单列表
    * next_openid          下一次拉取的位置
    * 
    * 同时，请注意：
    * 当公众号黑名单列表数量超过 10000 时，可通过填写 next_openid 的值，从而多次拉取列表的方式来满足需求。
    * 具体而言，就是在调用接口时，将上一次调用得到的返回中的 next_openid 的值，作为下一次调用中的 next_openid 值。
    * 
    */
    @POST
    @Path("members/getblacklist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetblacklistResult getblacklist(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken, GetblacklistBody body);
    default GetblacklistResult getblacklist(String beginOpenid) {
        GetblacklistBody body = new GetblacklistBody();
        body.setBeginOpenid(beginOpenid);
        return getblacklist((String)null, body);
    }
    
    /**
    * 2. 拉黑用户
    * 公众号可通过该接口来拉黑一批用户，黑名单列表由一串 OpenID （加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/members/batchblacklist?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *  "openid_list":["OPENID1","OPENID2"]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * openid_list          是.        需要拉入黑名单的用户的openid，一次拉黑最多允许20个
    * 
    */
    @POST
    @Path("members/batchblacklist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode batchblacklist(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken, BatchblacklistBody body);
    default WxErrCode batchblacklist(String... openids) {
        BatchblacklistBody body = new BatchblacklistBody(); 
        body.setOpenidList(openids);
        return batchblacklist((String)null, body);
    }
    
    /**
    * 3. 取消拉黑用户
    * 公众号可通过该接口来取消拉黑一批用户，黑名单列表由一串OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/members/batchunblacklist?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *  "openid_list":["OPENID1","OPENID2"]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * openid_list          是.        需要拉入黑名单的用户的openid，一次拉黑最多允许20个
    * 
    * 返回码说明
    * 
    * 返回码 说明
    * -1      系统繁忙
    * 40003   传入非法的openid
    * 49003   传入的openid不属于此AppID
    * 40032   一次只能拉黑20个用户
    */
    @POST
    @Path("batchunblacklist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode batchunblacklist(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("请求令牌为空") String accessToken, BatchblacklistBody body);
    default WxErrCode batchunblacklist(String... openids) {
        BatchblacklistBody body = new BatchblacklistBody(); 
        body.setOpenidList(openids);
        return batchunblacklist((String)null, body);
    }
}
