package net.icgear.three.weixin.mpapi.rest.api;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.mpapi.rest.dto.kf.KfAccount;
import net.icgear.three.weixin.mpapi.rest.dto.kf.KfAccountList;


/**
 * 当用户和公众号产生特定动作的交互时（具体动作列表请见下方说明），微信将会把消息数据推送给开发者，开发者可以在一段时间内（目前修改为48小时）调用客服接口，通过POST一个JSON数据包来发送消息给普通用户。
 * 此接口主要用于客服等有人工消息处理环节的功能，方便开发者为用户提供更加优质的服务。
 * 目前允许的动作列表如下（公众平台会根据运营情况更新该列表，不同动作触发后，允许的客服接口下发消息条数不同，下发条数达到上限后，会遇到错误返回码，具体请见返回码说明页）：
 * 1、用户发送信息
 * 2、点击自定义菜单（仅有点击推事件、扫码推事件、扫码推事件且弹出“消息接收中”提示框这3种菜单类型是会触发客服接口的）
 * 3、关注公众号
 * 4、扫描二维码
 * 5、支付成功
 * 6、用户维权
 * 为了帮助公众号使用不同的客服身份服务不同的用户群体，客服接口进行了升级，开发者可以管理客服账号，并设置客服账号的头像和昵称。该能力针对所有拥有客服接口权限的公众号开放。
 * 另外，请开发者注意，本接口中所有使用到media_id的地方，现在都可以使用素材管理中的永久素材media_id了。
 * 
 * ----------------------------------------------------------------------------------------------------------------------------------------
 * 客服帐号管理
 * 开发者在根据开发文档的要求完成开发后，使用6.0.2版及以上版本的微信用户在与公众号进行客服沟通，公众号使用不同的客服账号进行回复后，用户可以看到对应的客服头像和昵称。
 * 请注意，必须先在公众平台官网为公众号设置微信号后才能使用该能力。
 * @author Y13
 *
 */
@RemoteApi("customservice/kfaccount")
public interface WxKfAccountRest {
    
    /**
     * 添加客服帐号
     * 
     * 开发者可以通过本接口为公众号添加客服账号，每个公众号最多添加10个客服账号。该接口调用请求如下：
     * 
     * http请求方式: POST
     * https://api.weixin.qq.com/customservice/kfaccount/add?access_token=ACCESS_TOKEN
     * 
     * POST数据示例如下：
     * {
     *      "kf_account" : "test1@test",
     *      "nickname" : "客服1",
     *      "password" : "pswmd5",
     * }
     * 返回说明（正确时的JSON返回结果）：
     * {
     *      "errcode" : 0,
     *      "errmsg" : "ok",
     * }
     * 错误时微信会返回错误码等信息，请根据错误码查询错误信息
     * 
     * @param accessToken
     * @param account
     * @return
     */
    @POST
    @Path("customservice/kfaccount/add")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode addKfAccount(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            KfAccount account);
    default WxErrCode addKfAccount(KfAccount account) {
        return addKfAccount(null, account);
    }
    
    /**
     * 修改客服帐号
     * 
     * 开发者可以通过本接口为公众号修改客服账号。该接口调用请求如下：
     * 
     * http请求方式: POST
     * https://api.weixin.qq.com/customservice/kfaccount/update?access_token=ACCESS_TOKEN
     * 
     * POST数据示例如下：
     * {
     *      "kf_account" : "test1@test",
     *      "nickname" : "客服1",
     *      "password" : "pswmd5",
     * }
     * 返回说明（正确时的JSON返回结果）：
     * {
     *      "errcode" : 0,
     *      "errmsg" : "ok",
     * }
     * 错误时微信会返回错误码等信息，请根据错误码查询错误信息
     * 
     * @param accessToken
     * @param account
     * @return
     */
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode updateKfAccount(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            KfAccount account);
    default WxErrCode updateKfAccount(KfAccount account) {
        return updateKfAccount(null, account);
    }
    
    /**
     * 删除客服帐号
     * 
     * 开发者可以通过该接口为公众号删除客服帐号。该接口调用请求如下：
     * 
     * http请求方式: POST
     * https://api.weixin.qq.com/customservice/kfaccount/del?access_token=ACCESS_TOKEN
     * 
     * POST数据示例如下：
     * {
     *      "kf_account" : "test1@test",
     *      "nickname" : "客服1",
     *      "password" : "pswmd5",
     * }
     * 返回说明（正确时的JSON返回结果）：
     * {
     *      "errcode" : 0,
     *      "errmsg" : "ok",
     * }
     * 错误时微信会返回错误码等信息，请根据错误码查询错误信息
     * 
     * @param accessToken
     * @param account
     * @return
     */
    @POST
    @Path("del")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode delKfAccount(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            KfAccount account);
    default WxErrCode delKfAccount(KfAccount account) {
        return delKfAccount(null, account);
    }
    
    /**
     * 设置客服帐号的头像
     * 
     * 开发者可调用本接口来上传图片作为客服人员的头像，头像图片文件必须是jpg格式，推荐使用640*640大小的图片以达到最佳效果。该接口调用请求如下： 
     * 
     * http请求方式: POST/FORM
     * http://api.weixin.qq.com/customservice/kfaccount/uploadheadimg?access_token=ACCESS_TOKEN&kf_account=KFACCOUNT
     * 
     * 调用示例：使用curl命令，用FORM表单方式上传一个多媒体文件，curl命令的具体用法请自行了解 返回说明（正确时的JSON返回结果）： 
     * { 
     *      "errcode" : 0,
     *      "errmsg" : "ok", 
     * }
     *  
     * 错误时微信会返回错误码等信息，请根据错误码查询错误信息
     */
    @POST
    @Path("uploadheadimg")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    WxErrCode uploadKfAccountHeadimg(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空")String accessToken, 
            @QueryParam("kf_account")String kfAccount, @FormParam("img") InputStream inStream);
    default WxErrCode uploadKfAccountHeadimg(String kfAccount, InputStream inStream) {
        return uploadKfAccountHeadimg(null, kfAccount, inStream);
    }
    
    /**
     * 获取所有客服账号
     * 
     * 开发者通过本接口，获取公众号中所设置的客服基本信息，包括客服工号、客服昵称、客服登录账号。
     * http请求方式: GET
     * https://api.weixin.qq.com/cgi-bin/customservice/getkflist?access_token=ACCESS_TOKEN
     * 返回说明（正确时的JSON返回结果）：
     * {
     *     "kf_list": [
     *         {
     *             "kf_account": "test1@test", 
     *             "kf_nick": "ntest1", 
     *             "kf_id": "1001"
     *             "kf_headimgurl": " http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjfUS8Ym0GSaLic0FD3vN0V8PILcibEGb2fPfEOmw/0"
     *         }, 
     *         {
     *             "kf_account": "test2@test", 
     *             "kf_nick": "ntest2", 
     *             "kf_id": "1002"
     *             "kf_headimgurl": " http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjfUS8Ym0GSaLic0FD3vN0V8PILcibEGb2fPfEOmw /0"
     *         }, 
     *         {
     *             "kf_account": "test3@test", 
     *             "kf_nick": "ntest3", 
     *             "kf_id": "1003"
     *             "kf_headimgurl": " http://mmbiz.qpic.cn/mmbiz/4whpV1VZl2iccsvYbHvnphkyGtnvjfUS8Ym0GSaLic0FD3vN0V8PILcibEGb2fPfEOmw /0"
     *         }
     *     ]
     * }
     * 
     * 错误时微信会返回错误码等信息，请根据错误码查询错误信息
     */
    @POST
    @Path("getkflist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    KfAccountList getKfList(@QueryParam("ACCESS_TOKEN")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken);
    default KfAccountList getKfList() {
        return getKfList(null);
    }
}
