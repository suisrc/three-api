package net.icgear.three.weixin.mpapi.rest.dto.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("UserInfoByBatchBody")
public class UserInfoByBatchBody {
    
    @ApiModel("UserInfoByBatchBody.UserList")
    public static class UserList {
        
        @ApiModelProperty("用户的标识，对当前公众号唯一")
        @NotNull("openid属性为空")
        @JsonProperty("openid")
        private String openid;
        
        @ApiModelProperty("国家地区语言版本，zh_CN")
        @JsonProperty("lang")
        private String lang;
        /**
         * 用户的标识，对当前公众号唯一
         */
        public String getOpenid() {
            return openid;
        }
        /**
         * 用户的标识，对当前公众号唯一
         */
        public void setOpenid(String openid) {
            this.openid = openid;
        }
        /**
         * 国家地区语言版本，zh_CN
         */
        public String getLang() {
            return lang;
        }
        /**
         * 国家地区语言版本，zh_CN
         */
        public void setLang(String lang) {
            this.lang = lang;
        }
    }
    
    @JsonProperty("user_list")
    private UserList[] userList;
    
    public UserList[] getUserList() {
        return userList;
    }
    
    public void setUserList(UserList[] userList) {
        this.userList = userList;
    }
}