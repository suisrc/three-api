package net.icgear.three.weixin.mpapi.rest.dto.menu;

/**
 * 菜单的返回值内容
 * @see com.qq.weixin.mp.param.menu.MenuInfoParam
 * @author Y13
 *
 */
public class MenuInfoResult extends MenuInfoBody {

}
