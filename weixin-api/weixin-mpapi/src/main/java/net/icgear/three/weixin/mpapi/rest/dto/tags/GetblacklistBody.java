package net.icgear.three.weixin.mpapi.rest.dto.tags;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("GetblacklistBody")

public class GetblacklistBody {
    @ApiModelProperty("为空时，默认从开头拉取。")
    @JsonProperty("begin_openid")
    private String beginOpenid;
    /**
     * 为空时，默认从开头拉取。
     */
    public String getBeginOpenid() {
        return beginOpenid;
    }
    /**
     * 为空时，默认从开头拉取。
     */
    public void setBeginOpenid(String beginOpenid) {
        this.beginOpenid = beginOpenid;
    }
}