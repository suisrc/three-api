package net.icgear.three.weixin.mpapi.rest.dto.tags;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("BatchblacklistBody")
public class BatchblacklistBody {
    
    @ApiModelProperty("需要拉入黑名单的用户的openid，一次拉黑最多允许20个")
    @NotNull("openid_list属性为空")
    @JsonProperty("openid_list")
    private String[] openidList;
    
    /**
     * 需要拉入黑名单的用户的openid，一次拉黑最多允许20个
     */
    public String[] getOpenidList() {
        return openidList;
    }
    /**
     * 需要拉入黑名单的用户的openid，一次拉黑最多允许20个
     */
    public void setOpenidList(String[] openidList) {
        this.openidList = openidList;
    }
}