package net.icgear.three.weixin.mpapi.rest.dto.tags;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("TagsResult")
public class TagsResult extends WxErrCode {
    private static final long serialVersionUID = -1387578456669603107L;
    
    @JsonProperty("tags")
    private Tag[] tags;

    public Tag[] getTags() {
        return tags;
    }

    public void setTags(Tag[] tags) {
        this.tags = tags;
    }
}
