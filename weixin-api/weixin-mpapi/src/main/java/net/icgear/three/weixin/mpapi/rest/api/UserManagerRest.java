package net.icgear.three.weixin.mpapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.mpapi.rest.dto.tags.TagUsersBody;
import net.icgear.three.weixin.mpapi.rest.dto.tags.TagUsersResult;
import net.icgear.three.weixin.mpapi.rest.dto.user.UpdateRemarkBody;
import net.icgear.three.weixin.mpapi.rest.dto.user.UserInfoByBatchBody;
import net.icgear.three.weixin.mpapi.rest.dto.user.UserInfoByBatchResult;
import net.icgear.three.weixin.mpapi.rest.dto.user.UserInfoResult;
import net.icgear.three.weixin.mpapi.rest.dto.user.UserListResult;

/**
 * 微信公众号接口
 * 
 * 用户管理
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin/user")
public interface UserManagerRest {

    /**
     * 公众号可通过本接口来获取帐号的关注者列表，关注者列表由一串OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
     * 一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求。
     * 
     * http请求方式: GET
     * https://api.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&next_openid=NEXT_OPENID
     * 
     * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140840
     * 
     * @param accessToken 调用接口凭证, 当赋值为null时候，系统会通过SystemValue标识在对应的激活器中找到该常量
     * @param openid 第一个拉取的OPENID，不填默认从头开始拉取
     * @return
     */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserListResult getUserGet(
            @QueryParam("access_token") @Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken,
            @QueryParam("next_openid") String openid);

    default UserListResult getUserGet(String openid) {
        return getUserGet(null, openid);
    }

    /**
     * 开发者可通过OpenID来获取用户基本信息
     * 
     * http请求方式: GET
     * https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN
     * 
     * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140839
     * 
     * 参数   是否必须    说明
     * access_token    是   调用接口凭证
     * openid  是   普通用户的标识，对当前公众号唯一
     * lang    否   返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     * 
     * 
    * 参数说明：
    * 参数.                说明
    * subscribe            用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
    * openid               用户的标识，对当前公众号唯一
    * nickname             用户的昵称
    * sex                  用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
    * city                 用户所在城市
    * country              用户所在国家
    * province             用户所在省份
    * language             用户的语言，简体中文为zh_CN
    * headimgurl           用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
    * subscribe_time       用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
    * unionid              只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
    * remark               公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
    * groupid              用户所在的分组ID（暂时兼容用户分组旧接口）
    * tagid_list           用户被打上的标签ID列表
    * subscribe_scene      返回用户关注的渠道来源，ADD_SCENE_SEARCH
    * qr_scene             二维码扫码场景（开发者自定义）
    * qr_scene_str         二维码扫码场景描述（开发者自定义）
     */
    @GET
    @Path("info")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserInfoResult getUserInfo(
            @QueryParam("access_token") @Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken,
            @QueryParam("openid")@NotNull("用户的标识为空") String openid, @QueryParam("lang") @DefaultValue("zh_CN") String lang);

    default UserInfoResult getUserInfo(String openid) {
        return getUserInfo(null, openid, null);
    }

    /**
    * 批量获取用户基本信息
    * 开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条。
    * -:getUserInfoByBatch
    * 
    * 请求描述：null
    * 
    * 请求方式：POST
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "user_list": [
    *         {
    *             "openid": "otvxTs4dckWG7imySrJd6jSi0CWE", 
    *             "lang": "zh_CN"
    *         }, 
    *         {
    *             "openid": "otvxTs_JZ6SEiP0imdhpi50fuSZg", 
    *             "lang": "zh_CN"
    *         }
    *     ]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * openid               是.        用户的标识，对当前公众号唯一
    * lang                 否.        国家地区语言版本，zh_CN
    * 说明：
    * 正常情况下，微信会返回下述JSON数据包给公众号（示例中为一次性拉取了2个openid的用户基本信息，第一个是已关注的，第二个是未关注的）：
    * 
    * 返回结果：
    * {
    *    "user_info_list": [
    *        {
    *            "subscribe": 1, 
    *            "openid": "otvxTs4dckWG7imySrJd6jSi0CWE", 
    *            "nickname": "iWithery", 
    *            "sex": 1, 
    *            "language": "zh_CN", 
    *            "city": "揭阳", 
    *            "province": "广东", 
    *            "country": "中国", 
    *            "headimgurl": "http://thirdwx.qlogo.cn/mmopen/xbIQx1GRqdvyqkMMhEaGOX802l1CyqMJNgUzKP8MeAeHFicRDSnZH7FY4XB7p8XHXIf6uJA2SCunTPicGKezDC4saKISzRj3nz/0",
    *           "subscribe_time": 1434093047, 
    *            "unionid": "oR5GjjgEhCMJFyzaVZdrxZ2zRRF4", 
    *            "remark": "", 
    *            "groupid": 0,
    *            "tagid_list":[128,2],
    *            "subscribe_scene": "ADD_SCENE_QR_CODE",
    *            "qr_scene": 98765,
    *            "qr_scene_str": ""
    *       }, 
    *        {
    *            "subscribe": 0, 
    *            "openid": "otvxTs_JZ6SEiP0imdhpi50fuSZg"
    *        }
    *    ]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * subscribe            用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
    * openid               用户的标识，对当前公众号唯一
    * nickname             用户的昵称
    * sex                  用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
    * city                 用户所在城市
    * country              用户所在国家
    * province             用户所在省份
    * language             用户的语言，简体中文为zh_CN
    * headimgurl           用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
    * subscribe_time       用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
    * unionid              只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
    * remark               公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
    * groupid              用户所在的分组ID（暂时兼容用户分组旧接口）
    * tagid_list           用户被打上的标签ID列表
    * subscribe_scene      返回用户关注的渠道来源，ADD_SCENE_SEARCH
    * qr_scene             二维码扫码场景（开发者自定义）
    * qr_scene_str         二维码扫码场景描述（开发者自定义）
    * 
    */
    @POST
    @Path("info/batchget")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserInfoByBatchResult getUserInfoByBatch(@QueryParam("access_token") @Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken,
            @NotNull("用户信息为空")UserInfoByBatchBody body);
    default UserInfoByBatchResult getUserInfoByBatch(UserInfoByBatchBody body) {
        return getUserInfoByBatch((String)null, body);
    }
    
    /**
    * 设置用户备注名
    * 开发者可以通过该接口对指定用户设置备注名，该接口暂时开放给微信认证的服务号。
    * -:updateRemark
    * 
    * 请求描述：null
    * 
    * 请求方式：POST
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "openid":"oDF3iY9ffA-hqb2vVvbr7qxf6A0Q",
    *     "remark":"pangzi"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * openid               是.        用户标识
    * remark               是.        新的备注名，长度必须小于30字符
    * 
    */
    @POST
    @Path("info/updateremark")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode updateRemark(@QueryParam("access_token") @Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, 
            UpdateRemarkBody body);
    default WxErrCode updateRemark(String openid, String remark) {
        UpdateRemarkBody body = new UpdateRemarkBody();
        body.setOpenid(openid);
        body.setRemark(remark);
        return updateRemark((String) null, body);
    }
    
    /**
     * 请注意：请求需要改为POST,否则无法正确访问。
     * 
    * 5. 获取标签下粉丝列表
    * -:getTagUsers
    * 
    * 请求方式：GET（请使用https协议） 
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {   "tagid" : 134,   "next_openid":""}
    * 参数说明：
    * 参数.                必须.       说明
    * tagid                是.        标签ID
    * next_openid          否.        第一个拉取的OPENID，不填默认从头开始拉取
    * 
    * 返回结果：
    * {   "count":2,
    * "data":{
    * "openid":[  
    * "ocYxcuAEy30bX0NXmGn4ypqx3tI0",    
    * "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"  ]  
    * },  
    * "next_openid":"ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * count                这次获取的粉丝数量
    * data                 粉丝列表
    * next_openid          拉取列表最后一个用户的openid
    * 
    */
    @POST
    @Path("tag/get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TagUsersResult getTagUsers(@QueryParam("access_token") @Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken, TagUsersBody body);
    default TagUsersResult getTagUsers(Integer tagid, String nextOpenid) {
        TagUsersBody body = new TagUsersBody();
        body.setTagid(tagid);
        body.setNextOpenid(nextOpenid);
        return getTagUsers((String)null, body);
    }
}
