package net.icgear.three.weixin.mpapi.rest.dto.user;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("UpdateRemarkBody")
public class UpdateRemarkBody {
    
    @ApiModelProperty("用户标识")
    @NotNull("openid属性为空")
    @JsonProperty("openid")
    private String openid;
    
    @ApiModelProperty("新的备注名，长度必须小于30字符")
    @NotNull("remark属性为空")
    @JsonProperty("remark")
    private String remark;
    
    /**
     * 用户标识
     */
    public String getOpenid() {
        return openid;
    }
    /**
     * 用户标识
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }
    /**
     * 新的备注名，长度必须小于30字符
     */
    public String getRemark() {
        return remark;
    }
    /**
     * 新的备注名，长度必须小于30字符
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}
