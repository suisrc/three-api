package net.icgear.three.weixin.mpapi.rest.dto.tags;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

@ApiModel("GetblacklistResult")
public class GetblacklistResult extends WxErrCode {
    private static final long serialVersionUID = 7693558497676360886L;

    @ApiModel("GetblacklistResult.Data")
    public static class Data {

        @JsonProperty("openid")
        private String[] openid;

        public String[] getOpenid() {
            return openid;
        }

        public void setOpenid(String[] openid) {
            this.openid = openid;
        }
    }

    @ApiModelProperty("黑名单总数")
    @JsonProperty("total")
    private Integer total;
    
    @ApiModelProperty("当前拉取数量")
    @JsonProperty("count")
    private Integer count;
    
    @ApiModelProperty("黑名单列表")
    @JsonProperty("data")
    private Data data;
    
    @ApiModelProperty("下一次拉取的位置")
    @JsonProperty("next_openid")
    private String nextOpenid;

    /**
     * 黑名单总数
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * 黑名单总数
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * 当前拉取数量
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 当前拉取数量
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * 黑名单列表
     */
    public Data getData() {
        return data;
    }

    /**
     * 黑名单列表
     */
    public void setData(Data data) {
        this.data = data;
    }

    /**
     * 下一次拉取的位置
     */
    public String getNextOpenid() {
        return nextOpenid;
    }

    /**
     * 下一次拉取的位置
     */
    public void setNextOpenid(String nextOpenid) {
        this.nextOpenid = nextOpenid;
    }
}
