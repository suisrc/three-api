package net.icgear.three.weixin.mpapi.rest;

import java.util.Collection;

import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.three.core.msg.IMessage;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxEncryptSignature;
import net.icgear.three.weixin.core.event.UnknowEvent;
import net.icgear.three.weixin.core.msg.TextMessage;
import net.icgear.three.weixin.core.rest.AbstractWxBindingOne;

/**
 * 企业回调接口内容
 * 
 * @author Y13
 *
 * @param <T>
 */
public abstract class MpWxBinding extends AbstractWxBindingOne {

    /**
     * 
     */
    @Override
    protected Collection<Class<? extends IMessage>> getMsgTypes() {
        return ReflectionUtils.getSubclasses(IMessage.class, 
                TextMessage.class.getPackage().getName(),
                UnknowEvent.class.getPackage().getName());
    }
    
    /**
     * 
     */
    @Override
    protected String getBindingCategory() {
        return WxConsts.MP;
    }
    
    /**
     * 
     */
    @Override
    public String doGet(WxEncryptSignature sign) {
        return doGet2(sign);
    }

}
