package net.icgear.three.weixin.mpapi.rest.dto.tags;

import io.swagger.annotations.ApiModelProperty;
import net.icgear.three.weixin.core.bean.WxErrCode;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("TagUsersResult")
public class TagUsersResult extends WxErrCode {
    private static final long serialVersionUID = -2964868412648592586L;
    
    @ApiModel("TagUsersResult.Data")
    public static class Data {
        
        @JsonProperty("openid")
        private String[] openid;
        
        public String[] getOpenid() {
            return openid;
        }
        public void setOpenid(String[] openid) {
            this.openid = openid;
        }
    }
    
    @ApiModelProperty("这次获取的粉丝数量")
    @JsonProperty("count")
    private Integer count;
    
    @ApiModelProperty("粉丝列表")
    @JsonProperty("data")
    private Data data;
    
    @ApiModelProperty("拉取列表最后一个用户的openid")
    @JsonProperty("next_openid")
    private String nextOpenid;
    /**
     * 这次获取的粉丝数量
     */
    public Integer getCount() {
        return count;
    }
    /**
     * 这次获取的粉丝数量
     */
    public void setCount(Integer count) {
        this.count = count;
    }
    /**
     * 粉丝列表
     */
    public Data getData() {
        return data;
    }
    /**
     * 粉丝列表
     */
    public void setData(Data data) {
        this.data = data;
    }
    /**
     * 拉取列表最后一个用户的openid
     */
    public String getNextOpenid() {
        return nextOpenid;
    }
    /**
     * 拉取列表最后一个用户的openid
     */
    public void setNextOpenid(String nextOpenid) {
        this.nextOpenid = nextOpenid;
    }
}