package net.icgear.three.weixin.mpapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.mpapi.rest.dto.menu.ConditionalMenuInfoBody;
import net.icgear.three.weixin.mpapi.rest.dto.menu.ConditionalMenuOpBody;
import net.icgear.three.weixin.mpapi.rest.dto.menu.ConditionalMenuOpResult;
import net.icgear.three.weixin.mpapi.rest.dto.menu.ConditionalMenuUserBody;
import net.icgear.three.weixin.mpapi.rest.dto.menu.MenuInfoBody;
import net.icgear.three.weixin.mpapi.rest.dto.menu.MenuInfoListResult;
import net.icgear.three.weixin.mpapi.rest.dto.menu.MenuInfoResult;

/**
 * 自定义菜单接口
 * 
 * 自定义菜单能够帮助公众号丰富界面，让用户更好更快地理解公众号的功能。
 * 
 * 请注意： 1、自定义菜单最多包括3个一级菜单，每个一级菜单最多包含5个二级菜单。 2、一级菜单最多4个汉字，二级菜单最多7个汉字，多出来的部分将会以“...”代替。
 * 3、创建自定义菜单后，菜单的刷新策略是，在用户进入公众号会话页或公众号profile页时，如果发现上一次拉取菜单的请求在5分钟以前，
 * 就会拉取一下菜单，如果菜单有更新，就会刷新客户端的菜单。测试时可以尝试取消关注公众账号后再次关注，则可以看到创建后的效果。
 * 
 * 自定义菜单接口可实现多种类型按钮，如下：
 * 1、click：点击推事件用户点击click类型按钮后，微信服务器会通过消息接口推送消息类型为event的结构给开发者（参考消息接口指南），并且带上按钮中开发者填写的key值，
 * 开发者可以通过自定义的key值与用户进行交互；
 * 2、view：跳转URL用户点击view类型按钮后，微信客户端将会打开开发者在按钮中填写的网页URL，可与网页授权获取用户基本信息接口结合，获得用户基本信息。
 * 3、scancode_push：扫码推事件用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后显示扫描结果（如果是URL，将进入URL），且会将扫码的结果传给开发者，开发者可以下发消息。
 * 4、scancode_waitmsg：扫码推事件且弹出“消息接收中”提示框用户点击按钮后，微信客户端将调起扫一扫工具，完成扫码操作后，将扫码的结果传给开发者，同时收起扫一扫工具，
 * 然后弹出“消息接收中”提示框，随后可能会收到开发者下发的消息。
 * 5、pic_sysphoto：弹出系统拍照发图用户点击按钮后，微信客户端将调起系统相机，完成拍照操作后，会将拍摄的相片发送给开发者，并推送事件给开发者，同时收起系统相机，
 * 随后可能会收到开发者下发的消息。
 * 6、pic_photo_or_album：弹出拍照或者相册发图用户点击按钮后，微信客户端将弹出选择器供用户选择“拍照”或者“从手机相册选择”。用户选择后即走其他两种流程。
 * 7、pic_weixin：弹出微信相册发图器用户点击按钮后，微信客户端将调起微信相册，完成选择操作后，将选择的相片发送给开发者的服务器，并推送事件给开发者，同时收起相册，
 * 随后可能会收到开发者下发的消息。
 * 8、location_select：弹出地理位置选择器用户点击按钮后，微信客户端将调起地理位置选择工具，完成选择操作后，将选择的地理位置发送给开发者的服务器，同时收起位置选择工具，
 * 随后可能会收到开发者下发的消息。
 * 9、media_id：下发消息（除文本消息）用户点击media_id类型按钮后，微信服务器会将开发者填写的永久素材id对应的素材下发给用户，永久素材类型可以是图片、音频、视频、图文消息。
 * 请注意：永久素材id必须是在“素材管理/新增永久素材”接口上传后获得的合法id。
 * 10、view_limited：跳转图文消息URL用户点击view_limited类型按钮后，微信客户端将打开开发者在按钮中填写的永久素材id对应的图文消息URL，永久素材类型只支持图文消息。
 * 请注意：永久素材id必须是在“素材管理/新增永久素材”接口上传后获得的合法id。
 * 
 * 请注意， 3到8的所有事件，仅支持微信iPhone5.4.1以上版本，和Android5.4以上版本的微信用户，旧版本微信用户点击后将没有回应，开发者也不能正常接收到事件推送。
 * 9和10，是专门给第三方平台旗下未微信认证（具体而言，是资质认证未通过）的订阅号准备的事件类型，它们是没有事件推送的，能力相对受限，其他类型的公众号不必使用。
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 * --------------------------------------------------------------------------------------------------------------------------------------------------------
 * 个性化菜单接口 为了帮助公众号实现灵活的业务运营，微信公众平台新增了个性化菜单接口，开发者可以通过该接口，让公众号的不同用户群体看到不一样的自定义菜单。该接口开放给已认证订阅号和已认证服务号。
 * 开发者可以通过以下条件来设置用户看到的菜单： 1、用户标签（开发者的业务需求可以借助用户标签来完成） 2、性别 3、手机操作系统 4、地区（用户在微信客户端设置的地区）
 * 5、语言（用户在微信客户端设置的语言）
 * 
 * 个性化菜单接口说明： 1、个性化菜单要求用户的微信客户端版本在iPhone6.2.2，Android 6.2.4以上，暂时不支持其他版本微信
 * 2、菜单的刷新策略是，在用户进入公众号会话页或公众号profile页时，如果发现上一次拉取菜单的请求在5分钟以前，就会拉取一下菜单，如果菜单有更新，就会刷新客户端的菜单。
 * 测试时可以尝试取消关注公众账号后再次关注，则可以看到创建后的效果 3、普通公众号的个性化菜单的新增接口每日限制次数为2000次，删除接口也是2000次，测试个性化菜单匹配结果接口为20000次
 * 4、出于安全考虑，一个公众号的所有个性化菜单，最多只能设置为跳转到3个域名下的链接
 * 5、创建个性化菜单之前必须先创建默认菜单（默认菜单是指使用普通自定义菜单创建接口创建的菜单）。如果删除默认菜单，个性化菜单也会全部删除
 * 6、个性化菜单接口支持用户标签，请开发者注意，当用户身上的标签超过1个时，以最后打上的标签为匹配
 * 
 * 个性化菜单匹配规则说明： 个性化菜单的更新是会被覆盖的。
 * 例如公众号先后发布了默认菜单，个性化菜单1，个性化菜单2，个性化菜单3。那么当用户进入公众号页面时，将从个性化菜单3开始匹配，如果个性化菜单3匹配成功，则直接返回个性化菜单3，
 * 否则继续尝试匹配个性化菜单2，直到成功匹配到一个菜单。 根据上述匹配规则，为了避免菜单生效时间的混淆，决定不予提供个性化菜单编辑API，开发者需要更新菜单时，需将完整配置重新发布一轮。
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin/menu")
public interface CustomMenuManagerRest {

    /**
     * 自定义菜单创建接口 接口调用请求说明 http请求方式：POST（请使用https协议）
     * https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN click和view的请求示例
     * 
     * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141013
     * 
     * 说明：
     * click和view的请求示例
     *  {
     *      "button":[
     *      {    
     *           "type":"click",
     *           "name":"今日歌曲",
     *           "key":"V1001_TODAY_MUSIC"
     *       },
     *       {
     *            "name":"菜单",
     *            "sub_button":[
     *            {    
     *                "type":"view",
     *                "name":"搜索",
     *                "url":"http://www.soso.com/"
     *             },
     *             {
     *                  "type":"miniprogram",
     *                  "name":"wxa",
     *                  "url":"http://mp.weixin.qq.com",
     *                  "appid":"wx286b93c14bbf93aa",
     *                  "pagepath":"pages/lunar/index"
     *              },
     *             {
     *                "type":"click",
     *                "name":"赞一下我们",
     *                "key":"V1001_GOOD"
     *             }]
     *        }]
     *  }
     * 其他新增按钮类型的请求示例
     * {
     *     "button": [
     *         {
     *             "name": "扫码", 
     *             "sub_button": [
     *                 {
     *                     "type": "scancode_waitmsg", 
     *                     "name": "扫码带提示", 
     *                     "key": "rselfmenu_0_0", 
     *                     "sub_button": [ ]
     *                 }, 
     *                 {
     *                     "type": "scancode_push", 
     *                     "name": "扫码推事件", 
     *                     "key": "rselfmenu_0_1", 
     *                     "sub_button": [ ]
     *                 }
     *             ]
     *         }, 
     *         {
     *             "name": "发图", 
     *             "sub_button": [
     *                 {
     *                     "type": "pic_sysphoto", 
     *                     "name": "系统拍照发图", 
     *                     "key": "rselfmenu_1_0", 
     *                    "sub_button": [ ]
     *                  }, 
     *                 {
     *                     "type": "pic_photo_or_album", 
     *                     "name": "拍照或者相册发图", 
     *                     "key": "rselfmenu_1_1", 
     *                     "sub_button": [ ]
     *                 }, 
     *                 {
     *                     "type": "pic_weixin", 
     *                     "name": "微信相册发图", 
     *                     "key": "rselfmenu_1_2", 
     *                     "sub_button": [ ]
     *                 }
     *             ]
     *         }, 
     *         {
     *             "name": "发送位置", 
     *             "type": "location_select", 
     *             "key": "rselfmenu_2_0"
     *         },
     *         {
     *            "type": "media_id", 
     *            "name": "图片", 
     *            "media_id": "MEDIA_ID1"
     *         }, 
     *         {
     *            "type": "view_limited", 
     *            "name": "图文消息", 
     *            "media_id": "MEDIA_ID2"
     *         }
     *     ]
     * }
     * 
     * 参数说明：
     * 参数.                必须.       说明
     * button               是.        一级菜单数组，个数应为1~3个
     * sub_button           否.        二级菜单数组，个数应为1~5个
     * type                 是.        菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
     * name                 是.        菜单标题，不超过16个字节，子菜单不超过60个字节
     * key                  否.        菜单KEY值，用于消息接口推送，不超过128字节
     * url                  否.        网页
     * media_id             否.        调用新增永久素材接口返回的合法media_id
     * appid                否.        小程序的appid（仅认证公众号可配置）
     * pagepath             否.        小程序的页面路径
     */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode createMenu(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken,
            MenuInfoBody body);
    default WxErrCode createMenu(MenuInfoBody body) {
        return createMenu(null, body);
    }

    /**
     * 自定义菜单查询接口
     * 使用接口创建自定义菜单后，开发者还可使用接口查询自定义菜单的结构。另外请注意，在设置了个性化菜单后，使用本自定义菜单查询接口可以获取默认菜单和全部个性化菜单信息。
     * 
     * 请求方式：GET
     * 
     * 请求地址：https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN
     * 
     * 说明：
     * 返回说明（无个性化菜单时）
     * 对应创建接口，正确的Json返回结果:
     * {
     *     "menu": {
     *         "button": [
     *             {
     *                 "type": "click", 
     *                 "name": "今日歌曲", 
     *                 "key": "V1001_TODAY_MUSIC", 
     *                 "sub_button": [ ]
     *             }, 
     *             {
     *                 "type": "click", 
     *                 "name": "歌手简介", 
     *                 "key": "V1001_TODAY_SINGER", 
     *                 "sub_button": [ ]
     *             }, 
     *             {
     *                 "name": "菜单", 
     *                 "sub_button": [
     *                     {
     *                         "type": "view", 
     *                         "name": "搜索", 
     *                         "url": "http://www.soso.com/", 
     *                         "sub_button": [ ]
     *                     }, 
     *                     {
     *                         "type": "view", 
     *                         "name": "视频", 
     *                         "url": "http://v.qq.com/", 
     *                         "sub_button": [ ]
     *                     }, 
     *                     {
     *                         "type": "click", 
     *                         "name": "赞一下我们", 
     *                         "key": "V1001_GOOD", 
     *                         "sub_button": [ ]
     *                     }
     *                 ]
     *             }
     *         ]
     *     }
     * }
     * 返回说明（有个性化菜单时）
     * {
     *     "menu": {
     *         "button": [
     *             {
     *                 "type": "click", 
     *                 "name": "今日歌曲", 
     *                 "key": "V1001_TODAY_MUSIC", 
     *                 "sub_button": [ ]
     *             }
     *         ], 
     *         "menuid": 208396938
     *     }, 
     *     "conditionalmenu": [
     *         {
     *             "button": [
     *                 {
     *                     "type": "click", 
     *                     "name": "今日歌曲", 
     *                     "key": "V1001_TODAY_MUSIC", 
     *                     "sub_button": [ ]
     *                 }, 
     *                 {
     *                     "name": "菜单", 
     *                     "sub_button": [
     *                         {
     *                             "type": "view", 
     *                             "name": "搜索", 
     *                             "url": "http://www.soso.com/", 
     *                             "sub_button": [ ]
     *                         }, 
     *                         {
     *                             "type": "view", 
     *                             "name": "视频", 
     *                             "url": "http://v.qq.com/", 
     *                             "sub_button": [ ]
     *                         }, 
     *                         {
     *                             "type": "click", 
     *                             "name": "赞一下我们", 
     *                             "key": "V1001_GOOD", 
     *                             "sub_button": [ ]
     *                         }
     *                     ]
     *                 }
     *             ], 
     *             "matchrule": {
     *                 "group_id": 2, 
     *                 "sex": 1, 
     *                 "country": "中国", 
     *                 "province": "广东", 
     *                 "city": "广州", 
     *                 "client_platform_type": 2
     *             }, 
     *             "menuid": 208396993
     *         }
     *     ]
     * }
     * 注：menu为默认菜单，conditionalmenu为个性化菜单列表。字段说明请见个性化菜单接口页的说明。
     * 
     * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141014
     * 
     */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    MenuInfoListResult getMenu(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken);
    default MenuInfoListResult getMenu() {
        return getMenu(null);
    }

    /**
     * 自定义菜单删除接口
     * 
     * 使用接口创建自定义菜单后，开发者还可使用接口删除当前使用的自定义菜单。 另请注意，在个性化菜单时，调用此接口会删除默认菜单及全部个性化菜单。 请求说明 http请求方式：GET
     * https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=ACCESS_TOKEN 返回说明
     * 对应创建接口，正确的Json返回结果: {"errcode":0,"errmsg":"ok"}
     * 
     * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141015
     */
    @GET
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode deleteMenu(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken);
    default WxErrCode deleteMenu() {
        return deleteMenu(null);
    }

    /**
     * 创建个性化菜单
     * 
     * http请求方式：POST（请使用https协议）
     * 
     * https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=ACCESS_TOKEN
     *     /**
     * 创建个性化菜单
     * 
     * 请求方式：POST（请使用https协议）
     * 
     * 请求地址：https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=ACCESS_TOKEN
     * 
     * 请求包体：
     * {
     *      "button":[
     *      {    
     *         "type":"click",
     *         "name":"今日歌曲",
     *          "key":"V1001_TODAY_MUSIC" },
     *     {     "name":"菜单",
     *         "sub_button":[
     *         {            
     *             "type":"view",
     *             "name":"搜索",
     *             "url":"http://www.soso.com/"},
     *             {
     *                          "type":"miniprogram",
     *                          "name":"wxa",
     *                          "url":"http://mp.weixin.qq.com",
     *                          "appid":"wx286b93c14bbf93aa",
     *                          "pagepath":"pages/lunar/index"
     *             },
     *              {
     *         "type":"click",
     *         "name":"赞一下我们",
     *         "key":"V1001_GOOD"
     *            }]
     *  }],
     * "matchrule":{
     *   "tag_id":"2",
     *   "sex":"1",
     *   "country":"中国",
     *   "province":"广东",
     *   "city":"广州",
     *   "client_platform_type":"2",
     *   "language":"zh_CN"
     *   }
     * }
     * 
     * 参数说明：
     * 参数.                必须.       说明
     * button               是.        一级菜单数组，个数应为1~3个
     * sub_button           否.        二级菜单数组，个数应为1~5个
     * type                 是.        菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
     * name                 是.        菜单标题，不超过16个字节，子菜单不超过40个字节
     * key                  否.        菜单KEY值，用于消息接口推送，不超过128字节
     * url                  否.        网页链接，用户点击菜单可打开链接，不超过1024字节。当type为miniprogram时，不支持小程序的老版本客户端将打开本url
     * media_id             否.        调用新增永久素材接口返回的合法media_id
     * appid                否.        小程序的appid
     * pagepath             否.        小程序的页面路径
     * matchrule            是.        菜单匹配规则
     * tag_id               否.        用户标签的id，可通过用户标签管理接口获取
     * sex                  否.        性别：男（1）女（2），不填则不做匹配
     * client_platform_type 否.        客户端版本，当前只具体到系统型号：IOS(1),
     * country              否.        国家信息，是用户在微信中设置的地区，具体请参考地区信息表
     * province             否.        省份信息，是用户在微信中设置的地区，具体请参考地区信息表
     * city                 否.        城市信息，是用户在微信中设置的地区，具体请参考地区信息表
     * language             否.        语言信息，是用户在微信中设置的语言，具体请参考语言表：
     * 
     * language：
     * 1、简体中文 "zh_CN" 
     * 2、繁体中文TW "zh_TW" 
     * 3、繁体中文HK "zh_HK" 
     * 4、英文 "en" 
     * 5、印尼 "id" 
     * 6、马来 "ms" 
     * 7、西班牙 "es" 
     * 8、韩国 "ko" 
     * 9、意大利 "it" 
     * 10、日本 "ja" 
     * 11、波兰 "pl" 
     * 12、葡萄牙 "pt" 
     * 13、俄国 "ru" 
     * 14、泰文 "th" 
     * 15、越南 "vi" 
     * 16、阿拉伯语 "ar" 
     * 17、北印度 "hi" 
     * 18、希伯来 "he" 
     * 19、土耳其 "tr" 
     * 20、德语 "de" 
     * 21、法语 "fr"
     * 说明：
     * matchrule共六个字段，均可为空，但不能全部为空，至少要有一个匹配信息是不为空的。 country、province、city组成地区信息，将按照country、province、city的顺序进行验证，要符合地区信息表的内容。地区信息从大到小验证，小的可以不填，即若填写了省份信息，则国家信息也必填并且匹配，城市信息可以不填。 例如 “中国 广东省 广州市”、“中国 广东省”都是合法的地域信息，而“中国 广州市”则不合法，因为填写了城市信息但没有填写省份信息。 地区信息表请点击下载。
     * 
     * 返回结果 正确时的返回JSON数据包如下，错误时的返回码请见接口返回码说明。 { "menuid":"208379533" }
     * 
     * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296
     */
    @POST
    @Path("addconditional")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    ConditionalMenuOpResult addConditionalMenu(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken,
            ConditionalMenuInfoBody body);
    default ConditionalMenuOpResult addConditionalMenu(ConditionalMenuInfoBody body) {
        return addConditionalMenu((String)null, body);
    }

    /**
     * 删除个性化菜单
     * 
     * http请求方式：POST（请使用https协议）
     * https://api.weixin.qq.com/cgi-bin/menu/delconditional?access_token=ACCESS_TOKEN 
     * 
     * menuid为菜单id，可以通过自定义菜单查询接口获取。 
     * 
     * 返回码说明。： {"errcode":0,"errmsg":"ok"}
     * 
     * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296
     */
    @POST
    @Path("delconditional")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode delConditionalMenu(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken,
            ConditionalMenuOpBody param);
    default WxErrCode delConditionalMenu(String menuid) {
        ConditionalMenuOpBody body = new ConditionalMenuOpBody();
        body.setMenuid(menuid);
        return delConditionalMenu((String)null, body);
    }

    /**
     * 查询个性化菜单 使用普通自定义菜单查询接口可以获取默认菜单和全部个性化菜单信息，请见自定义菜单查询接口的说明。
     */
    default MenuInfoListResult getConditionalMenu() {
        return getMenu();
    }

    /**
     * 删除所有菜单 使用普通自定义菜单删除接口可以删除所有自定义菜单（包括默认菜单和全部个性化菜单），请见自定义菜单删除接口的说明。
     */
    default WxErrCode delAllMenu() {
        return deleteMenu();
    }

    /**
     * 测试个性化菜单匹配结果
     * 
     * http请求方式：POST（请使用https协议）
     * 
     * https://api.weixin.qq.com/cgi-bin/menu/trymatch?access_token=ACCESS_TOKEN 
     * 
     * 请求示例 {"user_id":"weixin" } 
     * user_id可以是粉丝的OpenID，也可以是粉丝的微信号。
     * 
     * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1455782296
     */
    @POST
    @Path("trymatch")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    MenuInfoResult tryMatchConditionalMenu(@QueryParam("access_token")@Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken,
            ConditionalMenuUserBody body);
    default MenuInfoResult tryMatchConditionalMenu(String userId) {
        ConditionalMenuUserBody body = new ConditionalMenuUserBody();
        body.setUserId(userId);
        return tryMatchConditionalMenu((String)null, body);
    }

}
