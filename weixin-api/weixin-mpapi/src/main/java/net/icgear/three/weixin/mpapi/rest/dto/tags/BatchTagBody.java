package net.icgear.three.weixin.mpapi.rest.dto.tags;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("BatchTagBody")
public class BatchTagBody {
    
    @ApiModelProperty("粉丝列表")
    @NotNull("openid_list属性为空")
    @JsonProperty("openid_list")
    private String[] openidList;
    
    @ApiModelProperty("标签")
    @NotNull("tagid属性为空")
    @JsonProperty("tagid")
    private Integer tagid;
    /**
     * 粉丝列表
     */
    public String[] getOpenidList() {
        return openidList;
    }
    /**
     * 粉丝列表
     */
    public void setOpenidList(String[] openidList) {
        this.openidList = openidList;
    }
    /**
     * 标签
     */
    public Integer getTagid() {
        return tagid;
    }
    /**
     * 标签
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
}