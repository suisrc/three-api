package net.icgear.three.weixin.mpapi.rest.dto.tags;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("Tag")
@JsonInclude(Include.NON_NULL)
public class Tag {
    
    @ApiModelProperty("标签id，由微信分配")
    @JsonProperty("id")
    private Integer id;
    
    @ApiModelProperty("标签名，UTF8编码")
    @JsonProperty("name")
    private String name;
    
    @ApiModelProperty("此标签下粉丝数")
    @JsonProperty("count")
    private Integer count;

    /**
     * 标签id，由微信分配
     */
    public Integer getId() {
        return id;
    }

    /**
     * 标签id，由微信分配
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 标签名，UTF8编码
     */
    public String getName() {
        return name;
    }

    /**
     * 标签名，UTF8编码
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * 此标签下粉丝数
     */
    public Integer getCount() {
        return count;
    }

    /**
     * 此标签下粉丝数
     */
    public void setCount(Integer count) {
        this.count = count;
    }
}