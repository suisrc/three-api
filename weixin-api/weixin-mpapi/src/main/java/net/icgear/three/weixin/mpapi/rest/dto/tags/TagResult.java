package net.icgear.three.weixin.mpapi.rest.dto.tags;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import net.icgear.three.weixin.core.bean.WxErrCode;

/**
 * 
 * @author Y13
 *
 */
@ApiModel("TagResult")
public class TagResult extends WxErrCode {
    private static final long serialVersionUID = 3931876743127868388L;
    
    @JsonProperty("tag")
    private Tag tag;

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
