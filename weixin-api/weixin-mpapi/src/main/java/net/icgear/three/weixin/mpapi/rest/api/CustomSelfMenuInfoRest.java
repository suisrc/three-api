package net.icgear.three.weixin.mpapi.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.NotNull;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.mpapi.rest.dto.menu2.SelfMenuConfigInfoByApiResult;
import net.icgear.three.weixin.mpapi.rest.dto.menu2.SelfMenuConfigInfoResult;

/**
 * 
 * @author Y13
 *
 */
@RemoteApi("cgi-bin")
public interface CustomSelfMenuInfoRest {
    /**
    * 获取自定义菜单配置接口
    * 
    * 请求方式：GET（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=ACCESS_TOKEN
    * 
    * 说明：
    * 本接口将会提供公众号当前使用的自定义菜单的配置，如果公众号是通过API调用设置的菜单，则返回菜单的开发配置，而如果公众号是在公众平台官网通过网站功能发布菜单，则本接口返回运营者设置的菜单配置。
    * 请注意：
    * 1、第三方平台开发者可以通过本接口，在旗下公众号将业务授权给你后，立即通过本接口检测公众号的自定义菜单配置，并通过接口再次给公众号设置好自动回复规则，以提升公众号运营者的业务体验。
    * 2、本接口与自定义菜单查询接口的不同之处在于，本接口无论公众号的接口是如何设置的，都能查询到接口，而自定义菜单查询接口则仅能查询到使用API设置的菜单配置。
    * 3、认证/未认证的服务号/订阅号，以及接口测试号，均拥有该接口权限。
    * 4、从第三方平台的公众号登录授权机制上来说，该接口从属于消息与菜单权限集。
    * 5、本接口中返回的图片/语音/视频为临时素材（临时素材每次获取都不同，3天内有效，通过素材管理-获取临时素材接口来获取这些素材），本接口返回的图文消息为永久素材素材（通过素材管理-获取永久素材接口来获取这些素材）。
    * 接口调用请求说明
    * 返回结果说明
    * 如果公众号是在公众平台官网通过网站功能发布菜单，则本接口返回的自定义菜单配置样例如下：
    * { 
    *    "is_menu_open": 1, 
    *    "selfmenu_info": { 
    *        "button": [ 
    *            { 
    *                "name": "button", 
    *                "sub_button": { 
    *                    "list": [ 
    *                        { 
    *                            "type": "view", 
    *                            "name": "view_url", 
    *                            "url": "http://www.qq.com"
    *                        }, 
    *                        { 
    *                            "type": "news", 
    *                            "name": "news", 
    *                            "value":"KQb_w_Tiz-nSdVLoTV35Psmty8hGBulGhEdbb9SKs-o",
    *                            "news_info": { 
    *                                "list": [ 
    *                                    { 
    *                                        "title": "MULTI_NEWS", 
    *                                        "author": "JIMZHENG", 
    *                                        "digest": "text", 
    *                                        "show_cover": 0, 
    *                                        "cover_url": "http://mmbiz.qpic.cn/mmbiz/GE7et87vE9vicuCibqXsX9GPPLuEtBfXfK0HKuBIa1A1cypS0uY1wickv70iaY1gf3I1DTszuJoS3lAVLvhTcm9sDA/0", 
    *                                        "content_url": "http://mp.weixin.qq.com/s?__biz=MjM5ODUwNTM3Ng==&mid=204013432&idx=1&sn=80ce6d9abcb832237bf86c87e50fda15#rd", 
    *                                        "source_url": ""
    *                                    }, 
    *                                    { 
    *                                        "title": "MULTI_NEWS1", 
    *                                        "author": "JIMZHENG", 
    *                                        "digest": "MULTI_NEWS1", 
    *                                        "show_cover": 1, 
    *                                        "cover_url": "http://mmbiz.qpic.cn/mmbiz/GE7et87vE9vicuCibqXsX9GPPLuEtBfXfKnmnpXYgWmQD5gXUrEApIYBCgvh2yHsu3ic3anDUGtUCHwjiaEC5bicd7A/0", 
    *                                        "content_url": "http://mp.weixin.qq.com/s?__biz=MjM5ODUwNTM3Ng==&mid=204013432&idx=2&sn=8226843afb14ecdecb08d9ce46bc1d37#rd", 
    *                                        "source_url": ""
    *                                    }
    *                                ]
    *                            }
    *                        },
    *                        {
    *                            "type": "video", 
    *                            "name": "video", 
    *                            "value": "http://61.182.130.30/vweixinp.tc.qq.com/1007_114bcede9a2244eeb5ab7f76d951df5f.f10.mp4?vkey=77A42D0C2015FBB0A3653D29C571B5F4BBF1D243FBEF17F09C24FF1F2F22E30881BD350E360BC53F&sha=0&save=1"
    *                        }, 
    *                        { 
    *                            "type": "voice",
    *                            "name": "voice", 
    *                            "value": "nTXe3aghlQ4XYHa0AQPWiQQbFW9RVtaYTLPC1PCQx11qc9UB6CiUPFjdkeEtJicn"
    *                        }
    *                    ]
    *                }
    *            }, 
    *            { 
    *                "type": "text", 
    *                "name": "text", 
    *                "value": "This is text!"
    *            }, 
    *            { 
    *                "type": "img", 
    *                "name": "photo", 
    *                "value": "ax5Whs5dsoomJLEppAvftBUuH7CgXCZGFbFJifmbUjnQk_ierMHY99Y5d2Cv14RD"
    *            }
    *        ]
    *    }
    * }
    * 如果公众号是通过API调用设置的菜单，自定义菜单配置样例如下：
    * { 
    *    "is_menu_open": 1, 
    *    "selfmenu_info": { 
    *        "button": [ 
    *            { 
    *                "type": "click", 
    *                "name": "今日歌曲", 
    *                "key": "V1001_TODAY_MUSIC"
    *            }, 
    *            { 
    *                "name": "菜单", 
    *                "sub_button": { 
    *                    "list": [ 
    *                        { 
    *                            "type": "view", 
    *                            "name": "搜索", 
    *                            "url": "http://www.soso.com/"
    *                        }, 
    *                        { 
    *                            "type": "view", 
    *                            "name": "视频", 
    *                            "url": "http://v.qq.com/"
    *                        }, 
    *                        { 
    *                            "type": "click", 
    *                            "name": "赞一下我们", 
    *                            "key": "V1001_GOOD"
    *                        }
    *                    ]
    *                }
    *            }
    *        ]
    *    }
    * }
    * 参数说明
    * 参数    说明
    * is_menu_open  菜单是否开启，0代表未开启，1代表开启
    * selfmenu_info 菜单信息
    * button    菜单按钮
    * type  菜单的类型，公众平台官网上能够设置的菜单类型有view（跳转网页）、text（返回文本，下同）、img、photo、video、voice。使用API设置的则有8种，详见《自定义菜单创建接口》
    * name  菜单名称
    * value、url、key等字段  对于不同的菜单类型，value的值意义不同。官网上设置的自定义菜单： Text:保存文字到value； Img、voice：保存mediaID到value； Video：保存视频下载链接到value； News：保存图文消息到news_info，同时保存mediaID到value； View：保存链接到url。 使用API设置的自定义菜单： click、scancode_push、scancode_waitmsg、pic_sysphoto、pic_photo_or_album、 pic_weixin、location_select：保存值到key；view：保存链接到url
    * news_info 图文消息的信息
    * title 图文消息的标题
    * digest    摘要
    * author    作者
    * show_cover    是否显示封面，0为不显示，1为显示
    * cover_url 封面图片的URL
    * content_url   正文的URL
    * source_url    原文的URL，若置空则无查看原文入口
    * 
    */
    @POST
    @Path("get_current_selfmenu_info")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SelfMenuConfigInfoResult getCurrentSelfMenuInfo( @QueryParam("access_token") @Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken);
    default SelfMenuConfigInfoResult getCurrentSelfMenuInfo() {
        return getCurrentSelfMenuInfo((String)null);
    }
    
    /**
     * 如果确定接口是api创建的，请使用该接口获取
     * 
     */
    @POST
    @Path("get_current_selfmenu_info")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    SelfMenuConfigInfoByApiResult getCurrentSelfMenuInfoByApi( @QueryParam("access_token") @Value(WxConsts.ACCESS_TOKEN)@NotNull("访问令牌为空") String accessToken);
    default SelfMenuConfigInfoByApiResult getCurrentSelfMenuInfoByApi() {
        return getCurrentSelfMenuInfoByApi((String)null);
    }
}
