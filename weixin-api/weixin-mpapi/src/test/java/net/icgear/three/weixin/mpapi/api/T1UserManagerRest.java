package net.icgear.three.weixin.mpapi.api;

import javax.ws.rs.client.Client;

import org.junit.Before;
import org.junit.Test;

import com.suisrc.core.fasterxml.FasterFactory.Type;
import com.suisrc.core.fasterxml.FasterFactoryImpl;
import com.suisrc.jaxrsapi.client.ClientUtils;

import net.icgear.three.weixin.core.bean.WxErrCode;
import net.icgear.three.weixin.mpapi.rest.api.UserManagerRest;
import net.icgear.three.weixin.mpapi.rest.dto.tags.TagUsersBody;
import net.icgear.three.weixin.mpapi.rest.dto.tags.TagUsersResult;
import net.icgear.three.weixin.mpapi.rest.dto.user.UpdateRemarkBody;
import net.icgear.three.weixin.mpapi.rest.dto.user.UserInfoByBatchBody;
import net.icgear.three.weixin.mpapi.rest.dto.user.UserInfoByBatchResult;
import net.icgear.three.weixin.mpapi.rest.dto.user.UserInfoResult;
import net.icgear.three.weixin.mpapi.rest.dto.user.UserListResult;

public class T1UserManagerRest {
    
    
    private String token = "13_PykJ3JzRs2NWCqSZtPwf902V8QKrMHXmakclxh-TI4hVFt_5-5IR6AcyzGuxfzNC2XwIplXIA7CzhddD4etXBdd2WiUu8JNV2L0Zu0fM3kHOjig0bzwJsukum9hdXnVszeL8_KRrsfbf3cBIDBOfAEAIGT";

    private String uri = "https://api.weixin.qq.com";
    
    private UserManagerRest rest;
    
    @Before
    public void setUp() throws Exception {
        Client client = ClientUtils.getClientWithProvider();
        rest = ClientUtils.getRestfulApiImpl(uri, UserManagerRest.class, client);
    }

    @Test
    public void getUserGet() {
        UserListResult result = rest.getUserGet(token, null);
        String msg = new FasterFactoryImpl().bean2String(result, Type.JSON);
        System.out.println(msg);
    }

    @Test
    public void getUserInfo() {
        UserInfoResult result = rest.getUserInfo(token, "oSIOX1KVT8NlqNAOeC1TOz8usAa8", "zh_CN");
        String msg = new FasterFactoryImpl().bean2String(result, Type.JSON);
        System.out.println(msg);
    }

    @Test
    public void getUserInfoByBatch() {
        UserInfoByBatchBody body = new UserInfoByBatchBody();
        UserInfoByBatchBody.UserList user = new UserInfoByBatchBody.UserList();
        user.setOpenid("oSIOX1KVT8NlqNAOeC1TOz8usAa8");
        body.setUserList(new UserInfoByBatchBody.UserList[] {user});
        UserInfoByBatchResult result = rest.getUserInfoByBatch(token, body);
        String msg = new FasterFactoryImpl().bean2String(result, Type.JSON);
        System.out.println(msg);
    }

    @Test
    public void updateRemark() {
        UpdateRemarkBody body = new UpdateRemarkBody();
        body.setOpenid("oSIOX1KVT8NlqNAOeC1TOz8usAa8");
        body.setRemark("X man");
        WxErrCode result = rest.updateRemark(token, body);
        String msg = new FasterFactoryImpl().bean2String(result, Type.JSON);
        System.out.println(msg);
    }

    @Test
    public void getTagUsers() {
        TagUsersBody body = new TagUsersBody();
        body.setTagid(1);
        TagUsersResult result = rest.getTagUsers(token, body);
        String msg = new FasterFactoryImpl().bean2String(result, Type.JSON);
        System.out.println(msg);
    }

}
