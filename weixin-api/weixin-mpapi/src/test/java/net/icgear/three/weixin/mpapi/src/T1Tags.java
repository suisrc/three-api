package net.icgear.three.weixin.mpapi.src;

import org.junit.Test;

import com.suisrc.core.Global;
import com.suisrc.jaxrsapi.tools.Utils;

public class T1Tags {
    
    private String getFolder() {
        return "Z:\\zero\\icgear\\three-api\\weixin-api\\weixin-mpapi\\doc\\tags\\";
    }
    @Test
    public void test1() {
        String name = "create";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test2() {
        String name = "get";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test3() {
        String name = "update";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test4() {
        String name = "delete";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test5() {
        String name = "user-get";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test6() {
        String name = "batchtagging";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test7() {
        String name = "batchuntagging";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test8() {
        String name = "getidlist";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test9() {
        String name = "getblacklist";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test10() {
        String name = "batchblacklist";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test11() {
        String name = "batchunblacklist";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

}
