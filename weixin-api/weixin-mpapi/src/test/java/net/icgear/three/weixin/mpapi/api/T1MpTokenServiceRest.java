package net.icgear.three.weixin.mpapi.api;

import javax.ws.rs.client.Client;

import org.junit.Before;
import org.junit.Test;

import com.suisrc.core.fasterxml.FasterFactory.Type;
import com.suisrc.core.fasterxml.FasterFactoryImpl;
import com.suisrc.jaxrsapi.client.ClientUtils;

import net.icgear.three.weixin.core.bean.WxAccessToken;
import net.icgear.three.weixin.mpapi.rest.api.MpTokenServiceRest;

public class T1MpTokenServiceRest {
    
    private String uri = "https://api.weixin.qq.com";
            
    private MpTokenServiceRest rest;
    
    @Before
    public void setUp() throws Exception {
        Client client = ClientUtils.getClientWithProvider();
        rest = ClientUtils.getRestfulApiImpl(uri, MpTokenServiceRest.class, client);
    }

    @Test
    public void getToken1() {
        WxAccessToken result = rest.getToken("client_credential", "wxc391f10a2b6bff12", "e35dcc52f297f5b0c3129e1f48a13e26");
        String msg = new FasterFactoryImpl().bean2String(result, Type.JSON);
        System.out.println(msg);
    }

}
