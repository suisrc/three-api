package net.icgear.three.weixin.mpapi.src;

import org.junit.Test;

import com.suisrc.core.Global;
import com.suisrc.jaxrsapi.tools.Utils;

public class T1Menu {
    
    private String getFolder() {
        return "Z:\\zero\\icgear\\three-api\\weixin-api\\weixin-mpapi\\doc\\menu\\";
    }
    @Test
    public void test1() {
        String name = "create";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test2() {
        String name = "get";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test3() {
        String name = "addconditional";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }
    @Test
    public void test4() {
        String name = "get_current_selfmenu_info";
        Utils.build2File(getFolder() + name + ".txt", getFolder() + name + ".java");
        Global.getLogger().info("生成完成:" + name);
    }

}
