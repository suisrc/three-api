    /**
    * 2. 批量为用户取消标签
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（请使用https协议） 
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 
    * 说明：
    * 请求包体：
    * {   "openid_list" : [//粉丝列表     
    * "ocYxcuAEy30bX0NXmGn4ypqx3tI0",     
    * "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"   ],   
    * "tagid" : 134 }
    * 返回结果：
    * {  
    * "errcode":0,   
    * "errmsg":"ok"
    * }
    * 错误码说明
    * 错误码	说明
    * -1	系统繁忙
    * 40032	每次传入的openid列表个数不能超过50个
    * 45159	非法的标签
    * 40003	传入非法的openid
    * 49003	传入的openid不属于此AppID
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @POST
    @Path("batchuntagging")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode batchuntagging(@QueryParam("access_token") String accessToken);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------