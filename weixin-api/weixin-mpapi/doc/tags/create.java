    /**
    * 1. 创建标签
    * 一个公众号，最多可以创建100个标签。
    * -:createTags
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/create?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {   "tag" : {     "name" : "广东"   } }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭据
    * name                 是.        标签名（30个字符以内）
    * 说明：
    * 
    * 
    * 返回结果：
    * {   "tag":{ "id":134,"name":"广东"   } }
    * 
    * 参数说明：
    * 参数.                说明
    * id                   标签id，由微信分配
    * name                 标签名，UTF8编码
    * name1                标签名，UTF8编码
    * 
    */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    CreateTagsResult createTags(@QueryParam("access_token")@NotNull("内容为空") String accessToken, CreateTagsBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("CreateTagsBody")
public class CreateTagsBody {
    @JsonProperty("tag")
    private Tag tag;
    public Tag getTag() {
        return tag;
    }
    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
@ApiModel("CreateTagsBody.Tag")
public static class Tag {
    @ApiModelProperty("标签名（30个字符以内）")
    @NotNull("name属性为空")
    @JsonProperty("name")
    private String name;
    /**
     * 标签名（30个字符以内）
     */
    public String getName() {
        return name;
    }
    /**
     * 标签名（30个字符以内）
     */
    public void setName(String name) {
        this.name = name;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("CreateTagsResult")
public class CreateTagsResult {
    @JsonProperty("tag")
    private Tag tag;
    public Tag getTag() {
        return tag;
    }
    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
@ApiModel("CreateTagsResult.Tag")
public static class Tag {
    @ApiModelProperty("标签id，由微信分配")
    @JsonProperty("id")
    private Integer id;
    @ApiModelProperty("标签名，UTF8编码")
    @JsonProperty("name")
    private String name;
    /**
     * 标签id，由微信分配
     */
    public Integer getId() {
        return id;
    }
    /**
     * 标签id，由微信分配
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 标签名，UTF8编码
     */
    public String getName() {
        return name;
    }
    /**
     * 标签名，UTF8编码
     */
    public void setName(String name) {
        this.name = name;
    }
}





-----------------------------over over over over over-------------------------