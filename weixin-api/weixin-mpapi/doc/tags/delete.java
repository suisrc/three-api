    /**
    * 4. 删除标签
    * 请注意，当某个标签下的粉丝超过10w时，后台不可直接删除标签。此时，开发者可以对该标签下的openid列表，先进行取消标签的操作，直到粉丝数不超过10w后，才可直接删除该标签。
    * -:deleteTags
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（请使用https协议） 
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 
    * 说明：
    * 请求包体：
    * {   "tag":{        "id" : 134   } }
    * 返回结果：
    * {   "errcode":0,   "errmsg":"ok" }
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @POST
    @Path("delete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode deleteTags(@QueryParam("access_token") String accessToken);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------