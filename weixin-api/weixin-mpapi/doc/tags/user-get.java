    /**
    * 5. 获取标签下粉丝列表
    * -:getTagUsers
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（请使用https协议） 
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {   "tagid" : 134,   "next_openid":""}
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * tagid                是.        标签ID
    * next_openid          否.        第一个拉取的OPENID，不填默认从头开始拉取
    * 说明：
    * 
    * 
    * 返回结果：
    * {   "count":2,
    * "data":{
    * "openid":[  
    * "ocYxcuAEy30bX0NXmGn4ypqx3tI0",    
    * "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"  ]  
    * },  
    * "next_openid":"ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * count                这次获取的粉丝数量
    * data                 粉丝列表
    * next_openid          拉取列表最后一个用户的openid
    * 
    */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetTagUsersResult getTagUsers(@QueryParam("access_token") String accessToken, GetTagUsersBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("GetTagUsersBody")
public class GetTagUsersBody {
    @ApiModelProperty("标签ID")
    @NotNull("tagid属性为空")
    @JsonProperty("tagid")
    private Integer tagid;
    @ApiModelProperty("第一个拉取的OPENID，不填默认从头开始拉取")
    @JsonProperty("next_openid")
    private String nextOpenid;
    /**
     * 标签ID
     */
    public Integer getTagid() {
        return tagid;
    }
    /**
     * 标签ID
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
    /**
     * 第一个拉取的OPENID，不填默认从头开始拉取
     */
    public String getNextOpenid() {
        return nextOpenid;
    }
    /**
     * 第一个拉取的OPENID，不填默认从头开始拉取
     */
    public void setNextOpenid(String nextOpenid) {
        this.nextOpenid = nextOpenid;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetTagUsersResult")
public class GetTagUsersResult {
    @ApiModelProperty("这次获取的粉丝数量")
    @JsonProperty("count")
    private Integer count;
    @ApiModelProperty("粉丝列表")
    @JsonProperty("data")
    private Data data;
    @ApiModelProperty("拉取列表最后一个用户的openid")
    @JsonProperty("next_openid")
    private String nextOpenid;
    /**
     * 这次获取的粉丝数量
     */
    public Integer getCount() {
        return count;
    }
    /**
     * 这次获取的粉丝数量
     */
    public void setCount(Integer count) {
        this.count = count;
    }
    /**
     * 粉丝列表
     */
    public Data getData() {
        return data;
    }
    /**
     * 粉丝列表
     */
    public void setData(Data data) {
        this.data = data;
    }
    /**
     * 拉取列表最后一个用户的openid
     */
    public String getNextOpenid() {
        return nextOpenid;
    }
    /**
     * 拉取列表最后一个用户的openid
     */
    public void setNextOpenid(String nextOpenid) {
        this.nextOpenid = nextOpenid;
    }
}
@ApiModel("GetTagUsersResult.Data")
public static class Data {
    @JsonProperty("openid")
    private String[] openid;
    public String[] getOpenid() {
        return openid;
    }
    public void setOpenid(String[] openid) {
        this.openid = openid;
    }
}





-----------------------------over over over over over-------------------------