3. 编辑标签
-:updateTags

请求方式：POST（请使用https协议）

请求地址：https://api.weixin.qq.com/cgi-bin/tags/update?access_token=ACCESS_TOKEN

请求包体：
{   "tag" : {     "id" : 134,     "name" : "广东人"   } }
参数说明：
参数 必要 说明

返回结果：
{   "errcode":0,   "errmsg":"ok" }
参数说明：
参数 说明