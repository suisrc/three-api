4. 删除标签
请注意，当某个标签下的粉丝超过10w时，后台不可直接删除标签。此时，开发者可以对该标签下的openid列表，先进行取消标签的操作，直到粉丝数不超过10w后，才可直接删除该标签。
-:deleteTags

请求方式：POST（请使用https协议） 

请求地址：https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=ACCESS_TOKEN

说明：
请求包体：
{   "tag":{        "id" : 134   } }
返回结果：
{   "errcode":0,   "errmsg":"ok" }