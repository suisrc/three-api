    /**
    * 3. 编辑标签
    * -:updateTags
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/update?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {   "tag" : {     "id" : 134,     "name" : "广东人"   } }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * 说明：
    * 
    * 
    * 返回结果：
    * {   "errcode":0,   "errmsg":"ok" }
    * 
    * 参数说明：
    * 参数.                说明
    * 
    */
    @POST
    @Path("update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode updateTags(@QueryParam("access_token") String accessToken, UpdateTagsBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModel;

@ApiModel("UpdateTagsBody")
public class UpdateTagsBody {
    @com.fasterxml.jackson.annotation.JsonProperty("tag")
    private Tag tag;
    public Tag getTag() {
        return tag;
    }
    public void setTag(Tag tag) {
        this.tag = tag;
    }
}
@ApiModel("UpdateTagsBody.Tag")
public static class Tag {
    @com.fasterxml.jackson.annotation.JsonProperty("id")
    private Integer id;
    @com.fasterxml.jackson.annotation.JsonProperty("name")
    private String name;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------