    /**
    * 标签功能目前支持公众号为用户打上最多20个标签。
    * 1. 批量为用户打标签
    * -:batchTagging
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {   "openid_list" : [
    * "ocYxcuAEy30bX0NXmGn4ypqx3tI0",
    * "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y"
    * ],
    * "tagid" : 134 
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * openid_list          是.        粉丝列表
    * tagid                是.        标签
    * 说明：
    * 
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @POST
    @Path("batchtagging")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode batchTagging(@QueryParam("access_token") String accessToken, BatchTaggingBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("BatchTaggingBody")
public class BatchTaggingBody {
    @ApiModelProperty("粉丝列表")
    @NotNull("openid_list属性为空")
    @JsonProperty("openid_list")
    private String[] openidList;
    @ApiModelProperty("标签")
    @NotNull("tagid属性为空")
    @JsonProperty("tagid")
    private Integer tagid;
    /**
     * 粉丝列表
     */
    public String[] getOpenidList() {
        return openidList;
    }
    /**
     * 粉丝列表
     */
    public void setOpenidList(String[] openidList) {
        this.openidList = openidList;
    }
    /**
     * 标签
     */
    public Integer getTagid() {
        return tagid;
    }
    /**
     * 标签
     */
    public void setTagid(Integer tagid) {
        this.tagid = tagid;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------