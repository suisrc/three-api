    /**
    * 3. 获取用户身上的标签列表
    * -:getIdList
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/getidlist?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {   "openid" : "ocYxcuBt0mRugKZ7tGAHPnUaOW7Y" }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * openid               是.        用户openid
    * 说明：
    * 
    * 
    * 返回结果：
    * {   "tagid_list":[134, 2   ] }
    * 
    * 参数说明：
    * 参数.                说明
    * tagid_list           被置上的标签
    * tagid_list1          被置上的标签
    * tagid_list2          被置上的标签
    * 
    */
    @POST
    @Path("getidlist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetIdListResult getIdList(@QueryParam("access_token") String accessToken, GetIdListBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("GetIdListBody")
public class GetIdListBody {
    @ApiModelProperty("用户openid")
    @NotNull("openid属性为空")
    @JsonProperty("openid")
    private String openid;
    /**
     * 用户openid
     */
    public String getOpenid() {
        return openid;
    }
    /**
     * 用户openid
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetIdListResult")
public class GetIdListResult {
    @ApiModelProperty("被置上的标签")
    @JsonProperty("tagid_list")
    private Integer[] tagidList;
    /**
     * 被置上的标签
     */
    public Integer[] getTagidList() {
        return tagidList;
    }
    /**
     * 被置上的标签
     */
    public void setTagidList(Integer[] tagidList) {
        this.tagidList = tagidList;
    }
}





-----------------------------over over over over over-------------------------