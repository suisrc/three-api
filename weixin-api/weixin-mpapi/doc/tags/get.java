    /**
    * 2. 获取公众号已创建的标签
    * -:getTags
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（请使用https协议） 
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/get?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 
    * 说明：
    * 
    * 
    * 返回结果：
    * {   "tags":[
    * {       "id":1,       "name":"每天一罐可乐星人",       "count":0 },
    * {   "id":2,   "name":"星标组",   "count":0 },
    * {   "id":127,   "name":"广东",   "count":5 }
    * ] }
    * 
    * 参数说明：
    * 参数.                说明
    * id                   标签ID
    * name                 标签名称
    * count                此标签下粉丝数
    * 
    */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetTagsResult getTags(@QueryParam("access_token") String accessToken);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetTagsResult")
public class GetTagsResult {
    @JsonProperty("tags")
    private Tags[] tags;
    public Tags[] getTags() {
        return tags;
    }
    public void setTags(Tags[] tags) {
        this.tags = tags;
    }
}
@ApiModel("GetTagsResult.Tags")
public static class Tags {
    @ApiModelProperty("标签ID")
    @JsonProperty("id")
    private Integer id;
    @ApiModelProperty("标签名称")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty("此标签下粉丝数")
    @JsonProperty("count")
    private Integer count;
    /**
     * 标签ID
     */
    public Integer getId() {
        return id;
    }
    /**
     * 标签ID
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * 标签名称
     */
    public String getName() {
        return name;
    }
    /**
     * 标签名称
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 此标签下粉丝数
     */
    public Integer getCount() {
        return count;
    }
    /**
     * 此标签下粉丝数
     */
    public void setCount(Integer count) {
        this.count = count;
    }
}





-----------------------------over over over over over-------------------------