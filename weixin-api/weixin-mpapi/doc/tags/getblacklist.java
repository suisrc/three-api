    /**
    * 公众号可登录微信公众平台，对粉丝进行拉黑的操作。同时，我们也提供了一套黑名单管理API，以便开发者直接利用接口进行操作。
    * 1. 获取公众号的黑名单列表
    * 公众号可通过该接口来获取帐号的黑名单列表，黑名单列表由一串 OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
    * 该接口每次调用最多可拉取 10000 个OpenID，当列表数较多时，可以通过多次拉取的方式来满足需求。
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/members/getblacklist?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    * "begin_openid":"OPENID1"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * begin_openid         否.        为空时，默认从开头拉取。
    * 说明：
    * 
    * 
    * 返回结果：
    * {
    *  "total":23000,
    *  "count":10000,
    *  "data":{"openid":[
    *        "OPENID1",
    *        "OPENID2",
    *        "OPENID10000"
    *     ]
    *   },
    *   "next_openid":"OPENID10000"
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * total                黑名单总数
    * count                当前拉取数量
    * data                 黑名单列表
    * next_openid          下一次拉取的位置
    * 
    */
    @POST
    @Path("getblacklist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetblacklistResult getblacklist(@QueryParam("access_token") String accessToken, GetblacklistBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetblacklistBody")
public class GetblacklistBody {
    @ApiModelProperty("为空时，默认从开头拉取。")
    @JsonProperty("begin_openid")
    private String beginOpenid;
    /**
     * 为空时，默认从开头拉取。
     */
    public String getBeginOpenid() {
        return beginOpenid;
    }
    /**
     * 为空时，默认从开头拉取。
     */
    public void setBeginOpenid(String beginOpenid) {
        this.beginOpenid = beginOpenid;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetblacklistResult")
public class GetblacklistResult {
    @ApiModelProperty("黑名单总数")
    @JsonProperty("total")
    private Integer total;
    @ApiModelProperty("当前拉取数量")
    @JsonProperty("count")
    private Integer count;
    @ApiModelProperty("黑名单列表")
    @JsonProperty("data")
    private Data data;
    @ApiModelProperty("下一次拉取的位置")
    @JsonProperty("next_openid")
    private String nextOpenid;
    /**
     * 黑名单总数
     */
    public Integer getTotal() {
        return total;
    }
    /**
     * 黑名单总数
     */
    public void setTotal(Integer total) {
        this.total = total;
    }
    /**
     * 当前拉取数量
     */
    public Integer getCount() {
        return count;
    }
    /**
     * 当前拉取数量
     */
    public void setCount(Integer count) {
        this.count = count;
    }
    /**
     * 黑名单列表
     */
    public Data getData() {
        return data;
    }
    /**
     * 黑名单列表
     */
    public void setData(Data data) {
        this.data = data;
    }
    /**
     * 下一次拉取的位置
     */
    public String getNextOpenid() {
        return nextOpenid;
    }
    /**
     * 下一次拉取的位置
     */
    public void setNextOpenid(String nextOpenid) {
        this.nextOpenid = nextOpenid;
    }
}
@ApiModel("GetblacklistResult.Data")
public static class Data {
    @JsonProperty("openid")
    private String[] openid;
    public String[] getOpenid() {
        return openid;
    }
    public void setOpenid(String[] openid) {
        this.openid = openid;
    }
}





-----------------------------over over over over over-------------------------