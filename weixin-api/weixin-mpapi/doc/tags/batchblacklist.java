    /**
    * 2. 拉黑用户
    * 公众号可通过该接口来拉黑一批用户，黑名单列表由一串 OpenID （加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/tags/members/batchblacklist?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *  "openid_list":["OPENID1","OPENID2"]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * openid_list          是.        需要拉入黑名单的用户的openid，一次拉黑最多允许20个
    * 说明：
    * 
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @POST
    @Path("batchblacklist")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode batchblacklist(@QueryParam("access_token")@NotNull("内容为空") String accessToken, BatchblacklistBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("BatchblacklistBody")
public class BatchblacklistBody {
    @ApiModelProperty("需要拉入黑名单的用户的openid，一次拉黑最多允许20个")
    @NotNull("openid_list属性为空")
    @JsonProperty("openid_list")
    private String[] openidList;
    /**
     * 需要拉入黑名单的用户的openid，一次拉黑最多允许20个
     */
    public String[] getOpenidList() {
        return openidList;
    }
    /**
     * 需要拉入黑名单的用户的openid，一次拉黑最多允许20个
     */
    public void setOpenidList(String[] openidList) {
        this.openidList = openidList;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------