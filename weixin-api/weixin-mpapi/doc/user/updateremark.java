    /**
    * 设置用户备注名
    * 开发者可以通过该接口对指定用户设置备注名，该接口暂时开放给微信认证的服务号。
    * -:updateRemark
    * 
    * 请求描述：null
    * 
    * 请求方式：POST
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "openid":"oDF3iY9ffA-hqb2vVvbr7qxf6A0Q",
    *     "remark":"pangzi"
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * access_token         是.        调用接口凭证
    * openid               是.        用户标识
    * remark               是.        新的备注名，长度必须小于30字符
    * 说明：
    * 
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @POST
    @Path("updateremark")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode updateRemark(@QueryParam("access_token")@NotNull("内容为空") String accessToken, UpdateRemarkBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("UpdateRemarkBody")
public class UpdateRemarkBody {
    @ApiModelProperty("用户标识")
    @NotNull("openid属性为空")
    @JsonProperty("openid")
    private String openid;
    @ApiModelProperty("新的备注名，长度必须小于30字符")
    @NotNull("remark属性为空")
    @JsonProperty("remark")
    private String remark;
    /**
     * 用户标识
     */
    public String getOpenid() {
        return openid;
    }
    /**
     * 用户标识
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }
    /**
     * 新的备注名，长度必须小于30字符
     */
    public String getRemark() {
        return remark;
    }
    /**
     * 新的备注名，长度必须小于30字符
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------