    /**
    * 批量获取用户基本信息
    * 开发者可通过该接口来批量获取用户基本信息。最多支持一次拉取100条。
    * -:getUserInfoByBatch
    * 
    * 请求描述：null
    * 
    * 请求方式：POST
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *     "user_list": [
    *         {
    *             "openid": "otvxTs4dckWG7imySrJd6jSi0CWE", 
    *             "lang": "zh_CN"
    *         }, 
    *         {
    *             "openid": "otvxTs_JZ6SEiP0imdhpi50fuSZg", 
    *             "lang": "zh_CN"
    *         }
    *     ]
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * openid               是.        用户的标识，对当前公众号唯一
    * lang                 否.        国家地区语言版本，zh_CN
    * 说明：
    * 正常情况下，微信会返回下述JSON数据包给公众号（示例中为一次性拉取了2个openid的用户基本信息，第一个是已关注的，第二个是未关注的）：
    * 
    * 返回结果：
    * {
    *    "user_info_list": [
    *        {
    *            "subscribe": 1, 
    *            "openid": "otvxTs4dckWG7imySrJd6jSi0CWE", 
    *            "nickname": "iWithery", 
    *            "sex": 1, 
    *            "language": "zh_CN", 
    *            "city": "揭阳", 
    *            "province": "广东", 
    *            "country": "中国", 
    *            "headimgurl": "http://thirdwx.qlogo.cn/mmopen/xbIQx1GRqdvyqkMMhEaGOX802l1CyqMJNgUzKP8MeAeHFicRDSnZH7FY4XB7p8XHXIf6uJA2SCunTPicGKezDC4saKISzRj3nz/0",
    *           "subscribe_time": 1434093047, 
    *            "unionid": "oR5GjjgEhCMJFyzaVZdrxZ2zRRF4", 
    *            "remark": "", 
    *            "groupid": 0,
    *            "tagid_list":[128,2],
    *            "subscribe_scene": "ADD_SCENE_QR_CODE",
    *            "qr_scene": 98765,
    *            "qr_scene_str": ""
    *       }, 
    *        {
    *            "subscribe": 0, 
    *            "openid": "otvxTs_JZ6SEiP0imdhpi50fuSZg"
    *        }
    *    ]
    * }
    * 
    * 参数说明：
    * 参数.                说明
    * subscribe            用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
    * openid               用户的标识，对当前公众号唯一
    * nickname             用户的昵称
    * sex                  用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
    * city                 用户所在城市
    * country              用户所在国家
    * province             用户所在省份
    * language             用户的语言，简体中文为zh_CN
    * headimgurl           用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
    * subscribe_time       用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
    * unionid              只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
    * remark               公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
    * groupid              用户所在的分组ID（暂时兼容用户分组旧接口）
    * tagid_list           用户被打上的标签ID列表
    * subscribe_scene      返回用户关注的渠道来源，ADD_SCENE_SEARCH
    * qr_scene             二维码扫码场景（开发者自定义）
    * qr_scene_str         二维码扫码场景描述（开发者自定义）
    * 
    */
    @POST
    @Path("batchget")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetUserInfoByBatchResult getUserInfoByBatch(@QueryParam("access_token") String accessToken, GetUserInfoByBatchBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("GetUserInfoByBatchBody")
public class GetUserInfoByBatchBody {
    @JsonProperty("user_list")
    private UserList[] userList;
    public UserList[] getUserList() {
        return userList;
    }
    public void setUserList(UserList[] userList) {
        this.userList = userList;
    }
}
@ApiModel("GetUserInfoByBatchBody.UserList")
public static class UserList {
    @ApiModelProperty("用户的标识，对当前公众号唯一")
    @NotNull("openid属性为空")
    @JsonProperty("openid")
    private String openid;
    @ApiModelProperty("国家地区语言版本，zh_CN")
    @JsonProperty("lang")
    private String lang;
    /**
     * 用户的标识，对当前公众号唯一
     */
    public String getOpenid() {
        return openid;
    }
    /**
     * 用户的标识，对当前公众号唯一
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }
    /**
     * 国家地区语言版本，zh_CN
     */
    public String getLang() {
        return lang;
    }
    /**
     * 国家地区语言版本，zh_CN
     */
    public void setLang(String lang) {
        this.lang = lang;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetUserInfoByBatchResult")
public class GetUserInfoByBatchResult {
    @JsonProperty("user_info_list")
    private UserInfoList[] userInfoList;
    public UserInfoList[] getUserInfoList() {
        return userInfoList;
    }
    public void setUserInfoList(UserInfoList[] userInfoList) {
        this.userInfoList = userInfoList;
    }
}
@ApiModel("GetUserInfoByBatchResult.UserInfoList")
public static class UserInfoList {
    @ApiModelProperty("用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。")
    @JsonProperty("subscribe")
    private Integer subscribe;
    @ApiModelProperty("用户的标识，对当前公众号唯一")
    @JsonProperty("openid")
    private String openid;
    @ApiModelProperty("用户的昵称")
    @JsonProperty("nickname")
    private String nickname;
    @ApiModelProperty("用户的性别，值为1时是男性，值为2时是女性，值为0时是未知")
    @JsonProperty("sex")
    private Integer sex;
    @ApiModelProperty("用户的语言，简体中文为zh_CN")
    @JsonProperty("language")
    private String language;
    @ApiModelProperty("用户所在城市")
    @JsonProperty("city")
    private String city;
    @ApiModelProperty("用户所在省份")
    @JsonProperty("province")
    private String province;
    @ApiModelProperty("用户所在国家")
    @JsonProperty("country")
    private String country;
    @ApiModelProperty("用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。")
    @JsonProperty("headimgurl")
    private String headimgurl;
    @ApiModelProperty("用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间")
    @JsonProperty("subscribe_time")
    private Integer subscribeTime;
    @ApiModelProperty("只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。")
    @JsonProperty("unionid")
    private String unionid;
    @ApiModelProperty("公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注")
    @JsonProperty("remark")
    private String remark;
    @ApiModelProperty("用户所在的分组ID（暂时兼容用户分组旧接口）")
    @JsonProperty("groupid")
    private Integer groupid;
    @ApiModelProperty("用户被打上的标签ID列表")
    @JsonProperty("tagid_list")
    private Integer[] tagidList;
    @ApiModelProperty("返回用户关注的渠道来源，ADD_SCENE_SEARCH")
    @JsonProperty("subscribe_scene")
    private String subscribeScene;
    @ApiModelProperty("二维码扫码场景（开发者自定义）")
    @JsonProperty("qr_scene")
    private Integer qrScene;
    @ApiModelProperty("二维码扫码场景描述（开发者自定义）")
    @JsonProperty("qr_scene_str")
    private String qrSceneStr;
    /**
     * 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
     */
    public Integer getSubscribe() {
        return subscribe;
    }
    /**
     * 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。
     */
    public void setSubscribe(Integer subscribe) {
        this.subscribe = subscribe;
    }
    /**
     * 用户的标识，对当前公众号唯一
     */
    public String getOpenid() {
        return openid;
    }
    /**
     * 用户的标识，对当前公众号唯一
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }
    /**
     * 用户的昵称
     */
    public String getNickname() {
        return nickname;
    }
    /**
     * 用户的昵称
     */
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    /**
     * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    public Integer getSex() {
        return sex;
    }
    /**
     * 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
     */
    public void setSex(Integer sex) {
        this.sex = sex;
    }
    /**
     * 用户的语言，简体中文为zh_CN
     */
    public String getLanguage() {
        return language;
    }
    /**
     * 用户的语言，简体中文为zh_CN
     */
    public void setLanguage(String language) {
        this.language = language;
    }
    /**
     * 用户所在城市
     */
    public String getCity() {
        return city;
    }
    /**
     * 用户所在城市
     */
    public void setCity(String city) {
        this.city = city;
    }
    /**
     * 用户所在省份
     */
    public String getProvince() {
        return province;
    }
    /**
     * 用户所在省份
     */
    public void setProvince(String province) {
        this.province = province;
    }
    /**
     * 用户所在国家
     */
    public String getCountry() {
        return country;
    }
    /**
     * 用户所在国家
     */
    public void setCountry(String country) {
        this.country = country;
    }
    /**
     * 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
     */
    public String getHeadimgurl() {
        return headimgurl;
    }
    /**
     * 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
     */
    public void setHeadimgurl(String headimgurl) {
        this.headimgurl = headimgurl;
    }
    /**
     * 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
     */
    public Integer getSubscribeTime() {
        return subscribeTime;
    }
    /**
     * 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
     */
    public void setSubscribeTime(Integer subscribeTime) {
        this.subscribeTime = subscribeTime;
    }
    /**
     * 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
     */
    public String getUnionid() {
        return unionid;
    }
    /**
     * 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
     */
    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }
    /**
     * 公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
     */
    public String getRemark() {
        return remark;
    }
    /**
     * 公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
    /**
     * 用户所在的分组ID（暂时兼容用户分组旧接口）
     */
    public Integer getGroupid() {
        return groupid;
    }
    /**
     * 用户所在的分组ID（暂时兼容用户分组旧接口）
     */
    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }
    /**
     * 用户被打上的标签ID列表
     */
    public Integer[] getTagidList() {
        return tagidList;
    }
    /**
     * 用户被打上的标签ID列表
     */
    public void setTagidList(Integer[] tagidList) {
        this.tagidList = tagidList;
    }
    /**
     * 返回用户关注的渠道来源，ADD_SCENE_SEARCH
     */
    public String getSubscribeScene() {
        return subscribeScene;
    }
    /**
     * 返回用户关注的渠道来源，ADD_SCENE_SEARCH
     */
    public void setSubscribeScene(String subscribeScene) {
        this.subscribeScene = subscribeScene;
    }
    /**
     * 二维码扫码场景（开发者自定义）
     */
    public Integer getQrScene() {
        return qrScene;
    }
    /**
     * 二维码扫码场景（开发者自定义）
     */
    public void setQrScene(Integer qrScene) {
        this.qrScene = qrScene;
    }
    /**
     * 二维码扫码场景描述（开发者自定义）
     */
    public String getQrSceneStr() {
        return qrSceneStr;
    }
    /**
     * 二维码扫码场景描述（开发者自定义）
     */
    public void setQrSceneStr(String qrSceneStr) {
        this.qrSceneStr = qrSceneStr;
    }
}





-----------------------------over over over over over-------------------------