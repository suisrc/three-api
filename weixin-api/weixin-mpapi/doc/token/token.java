    /**
    * 获取access_token
    * access_token是公众号的全局唯一接口调用凭据，公众号调用各接口时都需使用access_token。开发者需要进行妥善保存。
    * access_token的存储至少要保留512个字符空间。access_token的有效期目前为2个小时，需定时刷新，重复获取将导致上次获取的access_token失效。
    * 公众平台的API调用所需的access_token的使用及生成方式说明：
    * 1、建议公众号开发者使用中控服务器统一获取和刷新Access_token，其他业务逻辑服务器所使用的access_token均来自于该中控服务器，不应该各自去刷新，否则容易造成冲突，导致access_token覆盖而影响业务；
    * 2、目前Access_token的有效期通过返回的expire_in来传达，目前是7200秒之内的值。中控服务器需要根据这个有效时间提前去刷新新access_token。在刷新过程中，中控服务器可对外继续输出的老access_token，此时公众平台后台会保证在5分钟内，新老access_token都可用，这保证了第三方业务的平滑过渡；
    * 3、Access_token的有效时间可能会在未来有调整，所以中控服务器不仅需要内部定时主动刷新，还需要提供被动刷新access_token的接口，这样便于业务服务器在API调用获知access_token已超时的情况下，可以触发access_token的刷新流程。
    * 公众号和小程序均可以使用AppID和AppSecret调用本接口来获取access_token。AppID和AppSecret可在“微信公众平台-开发-基本配置”页中获得（需要已经成为开发者，且帐号没有异常状态）。调用接口时，请登录“微信公众平台-开发-基本配置”提前将服务器IP地址添加到IP白名单中，点击查看设置方法，否则将无法调用成功。小程序无需配置IP白名单。
    * -:getAccessToken
    * 
    * 请求描述：null
    * 
    * 请求方式：GET（HTTPS）
    * 
    * 请求地址： https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * grant_type           是.        获取access_token填写client_credential
    * appid                是.        第三方用户唯一凭证
    * secret               是.        第三方用户唯一凭证密钥，即appsecret
    * 说明：
    * 
    * 
    * 返回结果：
    * {"access_token":"ACCESS_TOKEN","expires_in":7200}
    * 
    * 参数说明：
    * 参数.                说明
    * access_token         获取到的凭证
    * expires_in           凭证有效时间，单位：秒
    * expires_in2          凭证有效时间，单位：秒
    * 
    */
    @GET
    @Path("token")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    GetAccessTokenResult getAccessToken(@QueryParam("grant_type")@NotNull("内容为空") String grantType, @QueryParam("appid")@NotNull("内容为空") String appid, @QueryParam("secret")@NotNull("内容为空") String secret);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;

@ApiModel("GetAccessTokenResult")
public class GetAccessTokenResult {
    @ApiModelProperty("获取到的凭证")
    @JsonProperty("access_token")
    private String accessToken;
    @ApiModelProperty("凭证有效时间，单位：秒")
    @JsonProperty("expires_in")
    private Integer expiresIn;
    /**
     * 获取到的凭证
     */
    public String getAccessToken() {
        return accessToken;
    }
    /**
     * 获取到的凭证
     */
    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    /**
     * 凭证有效时间，单位：秒
     */
    public Integer getExpiresIn() {
        return expiresIn;
    }
    /**
     * 凭证有效时间，单位：秒
     */
    public void setExpiresIn(Integer expiresIn) {
        this.expiresIn = expiresIn;
    }
}





-----------------------------over over over over over-------------------------