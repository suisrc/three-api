    /**
    * 创建个性化菜单
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * {
    *      "button":[
    *      {    
    *         "type":"click",
    *         "name":"今日歌曲",
    *          "key":"V1001_TODAY_MUSIC" },
    *     {     "name":"菜单",
    *         "sub_button":[
    *         {            
    *             "type":"view",
    *             "name":"搜索",
    *             "url":"http://www.soso.com/"},
    *             {
    *                          "type":"miniprogram",
    *                          "name":"wxa",
    *                          "url":"http://mp.weixin.qq.com",
    *                          "appid":"wx286b93c14bbf93aa",
    *                          "pagepath":"pages/lunar/index"
    *             },
    *              {
    *         "type":"click",
    *         "name":"赞一下我们",
    *         "key":"V1001_GOOD"
    *            }]
    *  }],
    * "matchrule":{
    *   "tag_id":"2",
    *   "sex":"1",
    *   "country":"中国",
    *   "province":"广东",
    *   "city":"广州",
    *   "client_platform_type":"2",
    *   "language":"zh_CN"
    *   }
    * }
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * button               是.        一级菜单数组，个数应为1~3个
    * sub_button           否.        二级菜单数组，个数应为1~5个
    * type                 是.        菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
    * name                 是.        菜单标题，不超过16个字节，子菜单不超过40个字节
    * key                  否.        菜单KEY值，用于消息接口推送，不超过128字节
    * url                  否.        网页链接，用户点击菜单可打开链接，不超过1024字节。当type为miniprogram时，不支持小程序的老版本客户端将打开本url
    * media_id             否.        调用新增永久素材接口返回的合法media_id
    * appid                否.        小程序的appid
    * pagepath             否.        小程序的页面路径
    * matchrule            是.        菜单匹配规则
    * tag_id               否.        用户标签的id，可通过用户标签管理接口获取
    * sex                  否.        性别：男（1）女（2），不填则不做匹配
    * client_platform_type 否.        客户端版本，当前只具体到系统型号：IOS(1),
    * country              否.        国家信息，是用户在微信中设置的地区，具体请参考地区信息表
    * province             否.        省份信息，是用户在微信中设置的地区，具体请参考地区信息表
    * city                 否.        城市信息，是用户在微信中设置的地区，具体请参考地区信息表
    * language             否.        语言信息，是用户在微信中设置的语言，具体请参考语言表：
    * 
    * language：
    * 1、简体中文 "zh_CN" 
    * 2、繁体中文TW "zh_TW" 
    * 3、繁体中文HK "zh_HK" 
    * 4、英文 "en" 
    * 5、印尼 "id" 
    * 6、马来 "ms" 
    * 7、西班牙 "es" 
    * 8、韩国 "ko" 
    * 9、意大利 "it" 
    * 10、日本 "ja" 
    * 11、波兰 "pl" 
    * 12、葡萄牙 "pt" 
    * 13、俄国 "ru" 
    * 14、泰文 "th" 
    * 15、越南 "vi" 
    * 16、阿拉伯语 "ar" 
    * 17、北印度 "hi" 
    * 18、希伯来 "he" 
    * 19、土耳其 "tr" 
    * 20、德语 "de" 
    * 21、法语 "fr"
    * 说明：
    * matchrule共六个字段，均可为空，但不能全部为空，至少要有一个匹配信息是不为空的。 country、province、city组成地区信息，将按照country、province、city的顺序进行验证，要符合地区信息表的内容。地区信息从大到小验证，小的可以不填，即若填写了省份信息，则国家信息也必填并且匹配，城市信息可以不填。 例如 “中国 广东省 广州市”、“中国 广东省”都是合法的地域信息，而“中国 广州市”则不合法，因为填写了城市信息但没有填写省份信息。 地区信息表请点击下载。
    * 
    */
    @POST
    @Path("addconditional")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode addconditional(@QueryParam("access_token") String accessToken, AddconditionalBody body);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




package pkg;

import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import com.suisrc.jaxrsapi.core.annotation.NotNull;

@ApiModel("AddconditionalBody")
public class AddconditionalBody {
    @ApiModelProperty("一级菜单数组，个数应为1~3个")
    @NotNull("button属性为空")
    @JsonProperty("button")
    private Button[] button;
    @ApiModelProperty("菜单匹配规则")
    @NotNull("matchrule属性为空")
    @JsonProperty("matchrule")
    private Matchrule matchrule;
    /**
     * 一级菜单数组，个数应为1~3个
     */
    public Button[] getButton() {
        return button;
    }
    /**
     * 一级菜单数组，个数应为1~3个
     */
    public void setButton(Button[] button) {
        this.button = button;
    }
    /**
     * 菜单匹配规则
     */
    public Matchrule getMatchrule() {
        return matchrule;
    }
    /**
     * 菜单匹配规则
     */
    public void setMatchrule(Matchrule matchrule) {
        this.matchrule = matchrule;
    }
}
@ApiModel("AddconditionalBody.Button")
public static class Button {
    @ApiModelProperty("菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型")
    @NotNull("type属性为空")
    @JsonProperty("type")
    private String type;
    @ApiModelProperty("菜单标题，不超过16个字节，子菜单不超过40个字节")
    @NotNull("name属性为空")
    @JsonProperty("name")
    private String name;
    @ApiModelProperty("菜单KEY值，用于消息接口推送，不超过128字节")
    @JsonProperty("key")
    private String key;
    /**
     * 菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
     */
    public String getType() {
        return type;
    }
    /**
     * 菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * 菜单标题，不超过16个字节，子菜单不超过40个字节
     */
    public String getName() {
        return name;
    }
    /**
     * 菜单标题，不超过16个字节，子菜单不超过40个字节
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * 菜单KEY值，用于消息接口推送，不超过128字节
     */
    public String getKey() {
        return key;
    }
    /**
     * 菜单KEY值，用于消息接口推送，不超过128字节
     */
    public void setKey(String key) {
        this.key = key;
    }
}
@ApiModel("AddconditionalBody.Matchrule")
public static class Matchrule {
    @ApiModelProperty("用户标签的id，可通过用户标签管理接口获取")
    @JsonProperty("tag_id")
    private String tagId;
    @ApiModelProperty("性别：男（1）女（2），不填则不做匹配")
    @JsonProperty("sex")
    private String sex;
    @ApiModelProperty("国家信息，是用户在微信中设置的地区，具体请参考地区信息表")
    @JsonProperty("country")
    private String country;
    @ApiModelProperty("省份信息，是用户在微信中设置的地区，具体请参考地区信息表")
    @JsonProperty("province")
    private String province;
    @ApiModelProperty("城市信息，是用户在微信中设置的地区，具体请参考地区信息表")
    @JsonProperty("city")
    private String city;
    @ApiModelProperty("客户端版本，当前只具体到系统型号：IOS(1),")
    @JsonProperty("client_platform_type")
    private String clientPlatformType;
    @ApiModelProperty("语言信息，是用户在微信中设置的语言，具体请参考语言表：")
    @JsonProperty("language")
    private String language;
    /**
     * 用户标签的id，可通过用户标签管理接口获取
     */
    public String getTagId() {
        return tagId;
    }
    /**
     * 用户标签的id，可通过用户标签管理接口获取
     */
    public void setTagId(String tagId) {
        this.tagId = tagId;
    }
    /**
     * 性别：男（1）女（2），不填则不做匹配
     */
    public String getSex() {
        return sex;
    }
    /**
     * 性别：男（1）女（2），不填则不做匹配
     */
    public void setSex(String sex) {
        this.sex = sex;
    }
    /**
     * 国家信息，是用户在微信中设置的地区，具体请参考地区信息表
     */
    public String getCountry() {
        return country;
    }
    /**
     * 国家信息，是用户在微信中设置的地区，具体请参考地区信息表
     */
    public void setCountry(String country) {
        this.country = country;
    }
    /**
     * 省份信息，是用户在微信中设置的地区，具体请参考地区信息表
     */
    public String getProvince() {
        return province;
    }
    /**
     * 省份信息，是用户在微信中设置的地区，具体请参考地区信息表
     */
    public void setProvince(String province) {
        this.province = province;
    }
    /**
     * 城市信息，是用户在微信中设置的地区，具体请参考地区信息表
     */
    public String getCity() {
        return city;
    }
    /**
     * 城市信息，是用户在微信中设置的地区，具体请参考地区信息表
     */
    public void setCity(String city) {
        this.city = city;
    }
    /**
     * 客户端版本，当前只具体到系统型号：IOS(1),
     */
    public String getClientPlatformType() {
        return clientPlatformType;
    }
    /**
     * 客户端版本，当前只具体到系统型号：IOS(1),
     */
    public void setClientPlatformType(String clientPlatformType) {
        this.clientPlatformType = clientPlatformType;
    }
    /**
     * 语言信息，是用户在微信中设置的语言，具体请参考语言表：
     */
    public String getLanguage() {
        return language;
    }
    /**
     * 语言信息，是用户在微信中设置的语言，具体请参考语言表：
     */
    public void setLanguage(String language) {
        this.language = language;
    }
}





-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------