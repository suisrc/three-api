    /**
    * 自定义菜单查询接口
    * 使用接口创建自定义菜单后，开发者还可使用接口查询自定义菜单的结构。另外请注意，在设置了个性化菜单后，使用本自定义菜单查询接口可以获取默认菜单和全部个性化菜单信息。
    * 
    * 请求方式：GET
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN
    * 
    * 说明：
    * 返回说明（无个性化菜单时）
    * 对应创建接口，正确的Json返回结果:
    * {
    *     "menu": {
    *         "button": [
    *             {
    *                 "type": "click", 
    *                 "name": "今日歌曲", 
    *                 "key": "V1001_TODAY_MUSIC", 
    *                 "sub_button": [ ]
    *             }, 
    *             {
    *                 "type": "click", 
    *                 "name": "歌手简介", 
    *                 "key": "V1001_TODAY_SINGER", 
    *                 "sub_button": [ ]
    *             }, 
    *             {
    *                 "name": "菜单", 
    *                 "sub_button": [
    *                     {
    *                         "type": "view", 
    *                         "name": "搜索", 
    *                         "url": "http://www.soso.com/", 
    *                         "sub_button": [ ]
    *                     }, 
    *                     {
    *                         "type": "view", 
    *                         "name": "视频", 
    *                         "url": "http://v.qq.com/", 
    *                         "sub_button": [ ]
    *                     }, 
    *                     {
    *                         "type": "click", 
    *                         "name": "赞一下我们", 
    *                         "key": "V1001_GOOD", 
    *                         "sub_button": [ ]
    *                     }
    *                 ]
    *             }
    *         ]
    *     }
    * }
    * 返回说明（有个性化菜单时）
    * {
    *     "menu": {
    *         "button": [
    *             {
    *                 "type": "click", 
    *                 "name": "今日歌曲", 
    *                 "key": "V1001_TODAY_MUSIC", 
    *                 "sub_button": [ ]
    *             }
    *         ], 
    *         "menuid": 208396938
    *     }, 
    *     "conditionalmenu": [
    *         {
    *             "button": [
    *                 {
    *                     "type": "click", 
    *                     "name": "今日歌曲", 
    *                     "key": "V1001_TODAY_MUSIC", 
    *                     "sub_button": [ ]
    *                 }, 
    *                 {
    *                     "name": "菜单", 
    *                     "sub_button": [
    *                         {
    *                             "type": "view", 
    *                             "name": "搜索", 
    *                             "url": "http://www.soso.com/", 
    *                             "sub_button": [ ]
    *                         }, 
    *                         {
    *                             "type": "view", 
    *                             "name": "视频", 
    *                             "url": "http://v.qq.com/", 
    *                             "sub_button": [ ]
    *                         }, 
    *                         {
    *                             "type": "click", 
    *                             "name": "赞一下我们", 
    *                             "key": "V1001_GOOD", 
    *                             "sub_button": [ ]
    *                         }
    *                     ]
    *                 }
    *             ], 
    *             "matchrule": {
    *                 "group_id": 2, 
    *                 "sex": 1, 
    *                 "country": "中国", 
    *                 "province": "广东", 
    *                 "city": "广州", 
    *                 "client_platform_type": 2
    *             }, 
    *             "menuid": 208396993
    *         }
    *     ]
    * }
    * 注：menu为默认菜单，conditionalmenu为个性化菜单列表。字段说明请见个性化菜单接口页的说明。
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @GET
    @Path("get")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode get(@QueryParam("access_token") String accessToken);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------