    /**
    * 自定义菜单创建接口
    * 
    * 请求描述：null
    * 
    * 请求方式：POST（请使用https协议）
    * 
    * 请求地址：https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN
    * 
    * 请求包体：
    * 
    * 
    * 参数说明：
    * 参数.                必须.       说明
    * button               是.        一级菜单数组，个数应为1~3个
    * sub_button           否.        二级菜单数组，个数应为1~5个
    * type                 是.        菜单的响应动作类型，view表示网页类型，click表示点击类型，miniprogram表示小程序类型
    * name                 是.        菜单标题，不超过16个字节，子菜单不超过60个字节
    * key                  否.        菜单KEY值，用于消息接口推送，不超过128字节
    * url                  否.        网页
    * media_id             否.        调用新增永久素材接口返回的合法media_id
    * appid                否.        小程序的appid（仅认证公众号可配置）
    * pagepath             否.        小程序的页面路径
    * 说明：
    * click和view的请求示例
    *  {
    *      "button":[
    *      {    
    *           "type":"click",
    *           "name":"今日歌曲",
    *           "key":"V1001_TODAY_MUSIC"
    *       },
    *       {
    *            "name":"菜单",
    *            "sub_button":[
    *            {    
    *                "type":"view",
    *                "name":"搜索",
    *                "url":"http://www.soso.com/"
    *             },
    *             {
    *                  "type":"miniprogram",
    *                  "name":"wxa",
    *                  "url":"http://mp.weixin.qq.com",
    *                  "appid":"wx286b93c14bbf93aa",
    *                  "pagepath":"pages/lunar/index"
    *              },
    *             {
    *                "type":"click",
    *                "name":"赞一下我们",
    *                "key":"V1001_GOOD"
    *             }]
    *        }]
    *  }
    * 其他新增按钮类型的请求示例
    * {
    *     "button": [
    *         {
    *             "name": "扫码", 
    *             "sub_button": [
    *                 {
    *                     "type": "scancode_waitmsg", 
    *                     "name": "扫码带提示", 
    *                     "key": "rselfmenu_0_0", 
    *                     "sub_button": [ ]
    *                 }, 
    *                 {
    *                     "type": "scancode_push", 
    *                     "name": "扫码推事件", 
    *                     "key": "rselfmenu_0_1", 
    *                     "sub_button": [ ]
    *                 }
    *             ]
    *         }, 
    *         {
    *             "name": "发图", 
    *             "sub_button": [
    *                 {
    *                     "type": "pic_sysphoto", 
    *                     "name": "系统拍照发图", 
    *                     "key": "rselfmenu_1_0", 
    *                    "sub_button": [ ]
    *                  }, 
    *                 {
    *                     "type": "pic_photo_or_album", 
    *                     "name": "拍照或者相册发图", 
    *                     "key": "rselfmenu_1_1", 
    *                     "sub_button": [ ]
    *                 }, 
    *                 {
    *                     "type": "pic_weixin", 
    *                     "name": "微信相册发图", 
    *                     "key": "rselfmenu_1_2", 
    *                     "sub_button": [ ]
    *                 }
    *             ]
    *         }, 
    *         {
    *             "name": "发送位置", 
    *             "type": "location_select", 
    *             "key": "rselfmenu_2_0"
    *         },
    *         {
    *            "type": "media_id", 
    *            "name": "图片", 
    *            "media_id": "MEDIA_ID1"
    *         }, 
    *         {
    *            "type": "view_limited", 
    *            "name": "图文消息", 
    *            "media_id": "MEDIA_ID2"
    *         }
    *     ]
    * }
    * 
    * 返回结果：
    * 
    * 
    * 参数说明：
    * 
    * 
    */
    @POST
    @Path("create")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    WxErrCode create(@QueryParam("access_token") String accessToken);




-----------------------------讨厌的分割线，你在这里做什么呢?---------------------------------------------------就算我讨厌，你能拿我怎么办---------------------------




none




-----------------------------爱的那舍难分，爱的奋不顾身--------------------------------------------------------对不起，我是分割线，不要抄歌词-----------------------




none




-----------------------------over over over over over-------------------------