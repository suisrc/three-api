package net.icgear.product.three.module.rest.dto;

/**
 * 执行的结果
 * 
 * @author Y13
 *
 */
public class ErrMsg {
    
    private String code;
    
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
