package net.icgear.product.three.module.rest.dto;

/**
 * 
 * 执行结果
 * 
 * @author Y13
 *
 */
public class TestResult extends ErrMsg {
    
    private TestData data;

    public TestData getData() {
        return data;
    }

    public void setData(TestData data) {
        this.data = data;
    }

    public static class TestData {
        
        private String value;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        
    }
}
