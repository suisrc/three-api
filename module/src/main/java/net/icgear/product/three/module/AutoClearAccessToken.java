package net.icgear.product.three.module;

import javax.ws.rs.NotAuthorizedException;

import com.suisrc.jaxrsapi.core.ApiActivator;

public class AutoClearAccessToken extends com.suisrc.jaxrsapi.core.AutoClearAccessToken {

    public AutoClearAccessToken(ApiActivator activator) {
        super(activator);
    }

    @Override
    protected boolean isTokenExpired(Object result, Exception e) {
        return e != null && e.getClass() == NotAuthorizedException.class;
    }

}
