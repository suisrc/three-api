package net.icgear.product.three.module.rest.api;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsapiConsts;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Retry;

import net.icgear.product.three.module.AutoClearAccessToken;
import net.icgear.product.three.module.rest.dto.TestBody;
import net.icgear.product.three.module.rest.dto.TestParam;
import net.icgear.product.three.module.rest.dto.TestResult;

/**
 * 测试接口
 * 
 * 会员 APIs
 * 
 * @author Y13
 *  
 */
@RemoteApi("test1")
public interface TestRest {
    
    /**
     * 测试用例
     * 
     */
    @Retry(value=AutoClearAccessToken.class, master=JaxrsapiConsts.FIELD_ACTIVATOR)
    @POST
    @Path("test2")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    TestResult postTest2(@BeanParam TestParam param, TestBody body);

}
