package net.icgear.product.three.module.rest.api.impl;

import javax.inject.Named;
import net.icgear.product.three.module.rest.dto.TestBody;
import net.icgear.product.three.module.rest.dto.TestParam;
import com.suisrc.core.ScCDI;
import net.icgear.product.three.module.rest.dto.TestResult;
import java.lang.String;
import javax.enterprise.context.ApplicationScoped;
import net.icgear.product.three.module.AutoClearAccessToken;
import java.lang.Exception;
import com.suisrc.jaxrsapi.core.ApiActivator;
import com.suisrc.jaxrsapi.core.ServiceClient;
import net.icgear.product.three.module.rest.api.TestRest;
import javax.ws.rs.client.WebTarget;
import java.lang.Override;
import com.suisrc.jaxrsapi.core.proxy.ProxyBuilder;

/**
 * Follow the implementation of the restful 2.0 standard remote access agent.
 * <see>
 *   https://suisrc.github.io/jaxrsapi
 * <generateBy>
 *   com.suisrc.jaxrsapi.core.factory.ClientServiceFactory
 * <time>
 *   2018-07-11T14:04:06.632
 * <author>
 *   Y13
 */
@ApplicationScoped
public class TestRestModule1 implements TestRest, ServiceClient {
    /*
     * 远程代理访问客户端控制器
     */
    private TestRest proxy;
    /*
     * 远程服务器控制器，具有服务器信息
     */
    private ApiActivator activator;
    /**
     * 初始化
     */
    public void postConstruct() {
        WebTarget target = ((WebTarget)activator.getAdapter(WebTarget.class)).path("test1");
        proxy = ProxyBuilder.builder(TestRest.class, target).build();
    }
    /**
     * 获取远程服务器控制器
     */
    public ApiActivator getActivator() {
        return activator;
    }
    /**
     * 配置远程服务器控制器
     */
    @Named("ModuleActivator")
    public void setActivator() {
        activator = ScCDI.injectWithNamed(ApiActivator.class);
        if (activator != null) postConstruct();

    }
    /**
     * 构造方法
     */
    public TestRestModule1() {
        setActivator();
    }
    /**
     * 接口实现
     */
    @Override
    public TestResult postTest2(TestParam pm0, TestBody pm1) {
        if (pm0.getToken() == null) {
            String temp = (String)activator.getAdapter("module-token", String.class);
            if (temp != null) pm0.setToken(temp);

        }
        AutoClearAccessToken predicate = new AutoClearAccessToken(activator);
        int count = 0x2;
        TestResult result;
        Exception exception;
        do {
            result = null;
            exception = null;
            try {
                if (count != 0x2) pm0.setToken(activator.getAdapter("module-token", String.class));

                result = proxy.postTest2(pm0, pm1);
            } catch (Exception e) {
                exception = e;
            }
        } while (predicate.test(0x2, --count, result, exception) && count > 0);
        return result;
    }
}
