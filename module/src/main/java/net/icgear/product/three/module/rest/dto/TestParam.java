package net.icgear.product.three.module.rest.dto;

import javax.ws.rs.HeaderParam;

import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.product.three.module.Consts;

/**
 * 门店信息
 * 
 * @author Y13
 *
 */
public class TestParam extends ErrMsg {
    
    @HeaderParam("X-token")
    @Value(value=Consts.APP_TOKEN, retry=true)
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
}
