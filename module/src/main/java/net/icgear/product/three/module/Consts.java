package net.icgear.product.three.module;

/**
 * 常量
 * @author Y13
 *
 */
public interface Consts {
    
    /**
     * 启动器标识
     */
    final String NAMED = "ModuleActivator";
    
    /**
     * Token
     */
    final String APP_TOKEN = "module-token";
    
    /**
     * 公司
     */
    final String APP_ORG = "module-organisation";
    
    /**
     * 用户
     */
    final String APP_USR = "module-username";
    
    /**
     * 密码
     */
    final String APP_PWD = "module-password";

    /**
     * 应用账户
     */
    final String KEY_APP_USR = "net.icgear.product.module.username";

    
    /**
     * 应用密码
     */
    final String KEY_APP_PWD = "net.icgear.product.module.password";

    
    /**
     * 共享服务器地址
     */
    final String KEY_APP_URL = "net.icgear.product.module.url";

    /**
     * 应用使用代理访问主机地址
     */
    final String KEY_APP_PROXY_HOST = "net.icgear.product.module.proxy.host";

    /**
     * 应用使用代理访问主机端口
     */
    final String KEY_APP_PROXY_PORT = "net.icgear.product.module.proxy.port";

}
