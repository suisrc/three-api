package net.icgear.product.three.module;

import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import com.google.common.collect.Sets;
import com.suisrc.jaxrsapi.core.AccessTokenActivator;
import com.suisrc.jaxrsapi.core.token.Token;

import net.icgear.product.three.module.rest.api.TestRest;

/**
 * 程序入口配置抽象
 * 
 * @author Y13
 */
@Named(Consts.NAMED)
@ApplicationScoped
public class ModuleActivator extends AccessTokenActivator {

    /**
     * 账户
     */
    private String username;
    
    /**
     * 密码
     */
    private String password;
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(String key, Class<T> type) {
        if (type == String.class && Consts.APP_TOKEN.equals(key)) {
            return (T) getToken();
        }
        return super.getAdapter(key);
    }

    @Override
    public String getAppName() {
        return "Module";
    }
    
    @Override
    public boolean isStdInject() {
        return false;
    }
    
    @Override
    public void postConstruct() {
        username = System.getProperty(Consts.KEY_APP_USR);
        password = System.getProperty(Consts.KEY_APP_PWD);
        super.postConstruct();
    }

    @Override
    protected String getBaseUrlKey() {
        return Consts.KEY_APP_URL;
    }

    @Override
    protected Token getTokenByRemote() {
        // 测试
        Token token = new Token();
        token.setAccessToken("123456");
        token.setExpiresIn(7200);
        return token;
    }

    @Override
    public Set<Class<?>> getClasses() {
        return Sets.newHashSet(
                TestRest.class
            );
    }

    /**
     * 临时保留token
     */
    @Override
    protected String getTempFileName() {
        return "target/token.obj";
    }
}
