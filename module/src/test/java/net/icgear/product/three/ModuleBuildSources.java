package net.icgear.product.three;

import com.suisrc.jaxrsapi.core.factory.NSCF;

import net.icgear.product.three.module.ModuleActivator;

/**
 * 生成接口的实现内容
 * 
 * @author Y13
 *
 */
class ModuleBuildSources {
    
    /**
     * 构建调用代码
     * @param args
     */
    public static void main(String[] args) {
        NSCF.buildSources("Module", ModuleActivator.class);
    }

}
