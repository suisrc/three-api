package net.icgear.product.three.module.rest.api;

import org.junit.Before;
import org.junit.Test;

import com.suisrc.core.simple.SimpleCdiAdapter;
import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.jaxrsapi.core.JaxrsapiConsts;
import com.suisrc.jaxrsapi.core.factory.NSCF;
import com.suisrc.jaxrsapi.core.token.Token;

import net.icgear.product.three.module.Consts;
import net.icgear.product.three.module.ModuleActivator;
import net.icgear.product.three.module.rest.api.impl.TestRestModule1;
import net.icgear.product.three.module.rest.dto.TestBody;
import net.icgear.product.three.module.rest.dto.TestParam;
import net.icgear.product.three.module.rest.dto.TestResult;

/**
 * 测试
 * 
 * @author Y13
 *
 */
public class T_TestRest {

    private TestRest rest;
    
    @Before
    public void setUp() throws Exception {
        System.setProperty(JaxrsapiConsts.DEBUG, "true");
        SimpleCdiAdapter.buildEnvironment();

        System.setProperty(Consts.KEY_APP_USR, "C1009");
        System.setProperty(Consts.KEY_APP_PWD, "123456");
        System.setProperty(Consts.KEY_APP_URL, "http://127.0.0.1:8771/api/");

        ModuleActivator activator = new ModuleActivator(){
            int offset = 123456;
            @Override
            protected Token getTokenByRemote() {
                Token token = new Token();
                token.setAccessToken(String.valueOf(offset++));
                token.setExpiresIn(2);
                System.out.println("获取新令牌:\n" + ReflectionUtils.testPrint(token));
                return token;
            }
        };
        NSCF.build(activator);
        // 向全局注入激活器
        SimpleCdiAdapter.putInjectBean(activator, Consts.NAMED);

        rest = new TestRestModule1();
    }
    
    @Test
    public void testShopInfo() {
        TestResult result = rest.postTest2(new TestParam(), new TestBody());
        System.out.println(ReflectionUtils.testPrint(result));
    }
    
}
