package net.icgear.three.yunding.rest.dto;

/**
 * 放回的参数
 * 
 * @author Y13
 *
 */
public class AuthenticateBody {
    
    private String organisation;

    private String username;

    private String password;

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
