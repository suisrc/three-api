package net.icgear.three.yunding.rest.dto;

/**
 * 门店信息
 * 
 *{
 *    "result": true, //true 或者 false 表明是否请求成功
 *    "msg": "message", //消息
 *    "data": [{
 *        "id": "", //门店ID
 *        "name": "name1", //门店名称
 *        "groupId": "",
 *        "shopNo": "", //门店编号
 *        "city": "", //城市
 *        "shopType": "", //类型
 *        "shopStatus": "", //门店状态
 *        "phone": "", //电话
 *        "address": "", //地址
 *        "openTime": "", //时间
 *        "area": "", //面积
 *        "lat": 0.0, //门店位置：纬度
 *        "lng": 0.0, //门店位置：经度
 *        "spec": { //门店支持的特性
 *            "peopleFlow": true, //是否客流分析
 *            "faceReco": true, //是否支持人脸识别
 *            "hotspot": false //是否支持热点分析
 *        }
 *    }]
 *}
 * @author Y13
 *
 */
public class ShopInfoResult extends ResultMessage {
    
    private ShopInfoData[] data;
    
    public ShopInfoData[] getData() {
        return data;
    }

    public void setData(ShopInfoData[] data) {
        this.data = data;
    }

    public static class ShopInfoData {
        /**
         * 门店ID
         */
        private String id;
        /**
         * 门店名称
         */
        private String name;
        
        private String groupId;
        /**
         * 门店编号
         */
        private String shopNo;
        /**
         * 城市
         */
        private String city;
        /**
         * 类型
         */
        private String shopType;
        /**
         * 门店状态
         */
        private String shopStatus;
        /**
         * 电话
         */
        private String phone;
        /**
         * 地址
         */
        private String address;
        /**
         * 时间
         */
        private String openTime;
        /**
         * 面积
         */
        private String area;
        /**
         * 门店位置：纬度
         */
        private Double lat;
        /**
         * 门店位置：经度
         */
        private Double lng;
        /**
         * 门店支持的特性
         */
        private ShopInfoSpec spec;
        
        public String getId() {
            return id;
        }
        public void setId(String id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getGroupId() {
            return groupId;
        }
        public void setGroupId(String groupId) {
            this.groupId = groupId;
        }
        public String getShopNo() {
            return shopNo;
        }
        public void setShopNo(String shopNo) {
            this.shopNo = shopNo;
        }
        public String getCity() {
            return city;
        }
        public void setCity(String city) {
            this.city = city;
        }
        public String getShopType() {
            return shopType;
        }
        public void setShopType(String shopType) {
            this.shopType = shopType;
        }
        public String getShopStatus() {
            return shopStatus;
        }
        public void setShopStatus(String shopStatus) {
            this.shopStatus = shopStatus;
        }
        public String getPhone() {
            return phone;
        }
        public void setPhone(String phone) {
            this.phone = phone;
        }
        public String getAddress() {
            return address;
        }
        public void setAddress(String address) {
            this.address = address;
        }
        public String getOpenTime() {
            return openTime;
        }
        public void setOpenTime(String openTime) {
            this.openTime = openTime;
        }
        public String getArea() {
            return area;
        }
        public void setArea(String area) {
            this.area = area;
        }
        public Double getLat() {
            return lat;
        }
        public void setLat(Double lat) {
            this.lat = lat;
        }
        public Double getLng() {
            return lng;
        }
        public void setLng(Double lng) {
            this.lng = lng;
        }
        public ShopInfoSpec getSpec() {
            return spec;
        }
        public void setSpec(ShopInfoSpec spec) {
            this.spec = spec;
        }
        
    }
    
    public static class ShopInfoSpec {
        /**
         * 是否客流分析
         */
        private Boolean peopleFlow;
        /**
         * 是否支持人脸识别
         */
        private Boolean faceReco;
        /**
         * 是否支持热点分析
         */
        private Boolean hotspot;
        
        public Boolean getPeopleFlow() {
            return peopleFlow;
        }
        public void setPeopleFlow(Boolean peopleFlow) {
            this.peopleFlow = peopleFlow;
        }
        public Boolean getFaceReco() {
            return faceReco;
        }
        public void setFaceReco(Boolean faceReco) {
            this.faceReco = faceReco;
        }
        public Boolean getHotspot() {
            return hotspot;
        }
        public void setHotspot(Boolean hotspot) {
            this.hotspot = hotspot;
        }
    }

}
