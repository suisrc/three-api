package net.icgear.three.yunding.rest.dto;

/**
 * 执行的结果
 * 
 * @author Y13
 *
 */
public class ResultMessage {
    
    private boolean result;
    
    private String msg;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    
    

}
