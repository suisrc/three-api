package net.icgear.three.yunding.rest.dto;

/**
 * {
 *  "result": true, //true 或者 false
 *  "msg": "message", //消息
 *  "data": { //数据，如果请求成功，携带该字段，并且包含token字段。
 *      "token": "token"
 *   }
 * }
 * @author Y13
 *
 */
public class AuthenticateResult extends ResultMessage {
    
    private AuthenticateData data;

    public AuthenticateData getData() {
        return data;
    }

    public void setData(AuthenticateData data) {
        this.data = data;
    }

    public static class AuthenticateData {
        
        private String token;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
        
    }
}
