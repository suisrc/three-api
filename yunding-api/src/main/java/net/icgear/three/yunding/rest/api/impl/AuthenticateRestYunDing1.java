package net.icgear.three.yunding.rest.api.impl;

import javax.inject.Named;
import com.suisrc.jaxrsapi.core.ApiActivator;
import com.suisrc.jaxrsapi.core.ServiceClient;
import com.suisrc.core.ScCDI;

import javax.ws.rs.client.WebTarget;
import javax.enterprise.context.ApplicationScoped;
import java.lang.Override;

import com.suisrc.jaxrsapi.core.proxy.ProxyBuilder;

import net.icgear.three.yunding.rest.api.AuthenticateRest;
import net.icgear.three.yunding.rest.dto.AuthenticateBody;
import net.icgear.three.yunding.rest.dto.AuthenticateResult;

/**
 * Follow the implementation of the restful 2.0 standard remote access agent.
 * <see>
 *   https://suisrc.github.io/jaxrsapi
 * <generateBy>
 *   com.suisrc.jaxrsapi.core.factory.ClientServiceFactory
 * <time>
 *   2018-07-11T12:17:14.191
 * <author>
 *   Y13
 */
@ApplicationScoped
public class AuthenticateRestYunDing1 implements AuthenticateRest, ServiceClient {
    /*
     * 远程代理访问客户端控制器
     */
    private AuthenticateRest proxy;
    /*
     * 远程服务器控制器，具有服务器信息
     */
    private ApiActivator activator;
    /**
     * 初始化
     */
    public void postConstruct() {
        WebTarget target = (WebTarget)activator.getAdapter(WebTarget.class);
        proxy = ProxyBuilder.builder(AuthenticateRest.class, target).build();
    }
    /**
     * 获取远程服务器控制器
     */
    public ApiActivator getActivator() {
        return activator;
    }
    /**
     * 配置远程服务器控制器
     */
    @Named("YunDingActivator")
    public void setActivator() {
        activator = ScCDI.injectWithNamed(ApiActivator.class);
        if (activator != null) postConstruct();

    }
    /**
     * 构造方法
     */
    public AuthenticateRestYunDing1() {
        setActivator();
    }
    /**
     * 接口实现
     */
    @Override
    public AuthenticateResult postAuthenticate(AuthenticateBody pm0) {
        return (AuthenticateResult)proxy.postAuthenticate(pm0);
    }
}
