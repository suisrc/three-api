package net.icgear.three.yunding.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.JaxrsapiConsts;
import com.suisrc.jaxrsapi.core.annotation.RemoteApi;
import com.suisrc.jaxrsapi.core.annotation.Retry;
import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.three.yunding.AutoClearAccessToken;
import net.icgear.three.yunding.Consts;
import net.icgear.three.yunding.rest.dto.ShopInfoResult;

/**
 * 测试接口
 * 
 * 会员 APIs
 * 
 * @author Y13
 *  
 */
@RemoteApi("shops")
public interface ShopsRest {
    
    /**
     * 
     * @param param
     * @return
     */
    @Retry(value = AutoClearAccessToken.class, master = JaxrsapiConsts.FIELD_ACTIVATOR)
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    ShopInfoResult getShopInfo(
            @HeaderParam("X-Token")@Value(value = Consts.APP_TOKEN, retry = true) String token,
            @QueryParam("withShopInfo") Boolean wsi);

}
