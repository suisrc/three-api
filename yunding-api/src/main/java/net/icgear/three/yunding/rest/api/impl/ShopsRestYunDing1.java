package net.icgear.three.yunding.rest.api.impl;

import javax.inject.Named;
import com.suisrc.core.ScCDI;

import java.lang.String;
import javax.enterprise.context.ApplicationScoped;

import java.lang.Exception;
import com.suisrc.jaxrsapi.core.ApiActivator;
import com.suisrc.jaxrsapi.core.ServiceClient;
import javax.ws.rs.client.WebTarget;
import java.lang.Boolean;
import java.lang.Override;
import com.suisrc.jaxrsapi.core.proxy.ProxyBuilder;

import net.icgear.three.yunding.AutoClearAccessToken;
import net.icgear.three.yunding.rest.api.ShopsRest;
import net.icgear.three.yunding.rest.dto.ShopInfoResult;

/**
 * Follow the implementation of the restful 2.0 standard remote access agent.
 * <see>
 *   https://suisrc.github.io/jaxrsapi
 * <generateBy>
 *   com.suisrc.jaxrsapi.core.factory.ClientServiceFactory
 * <time>
 *   2018-07-11T13:42:39.085
 * <author>
 *   Y13
 */
@ApplicationScoped
public class ShopsRestYunDing1 implements ShopsRest, ServiceClient {
    /*
     * 远程代理访问客户端控制器
     */
    private ShopsRest proxy;
    /*
     * 远程服务器控制器，具有服务器信息
     */
    private ApiActivator activator;
    /**
     * 初始化
     */
    public void postConstruct() {
        WebTarget target = ((WebTarget)activator.getAdapter(WebTarget.class)).path("shops");
        proxy = ProxyBuilder.builder(ShopsRest.class, target).build();
    }
    /**
     * 获取远程服务器控制器
     */
    public ApiActivator getActivator() {
        return activator;
    }
    /**
     * 配置远程服务器控制器
     */
    @Named("YunDingActivator")
    public void setActivator() {
        activator = ScCDI.injectWithNamed(ApiActivator.class);
        if (activator != null) postConstruct();

    }
    /**
     * 构造方法
     */
    public ShopsRestYunDing1() {
        setActivator();
    }
    /**
     * 接口实现
     */
    @Override
    public ShopInfoResult getShopInfo(String pm0, Boolean pm1) {
        if (pm0 == null) {
            String temp = (String)activator.getAdapter("yunding-token", String.class);
            if (temp != null) pm0 = temp;

        }
        AutoClearAccessToken predicate = new AutoClearAccessToken(activator);
        int count = 0x2;
        ShopInfoResult result;
        Exception exception;
        do {
            result = null;
            exception = null;
            try {
                if (count != 0x2) pm0 = activator.getAdapter("yunding-token", String.class);

                result = proxy.getShopInfo(pm0, pm1);
            } catch (Exception e) {
                exception = e;
            }
        } while (predicate.test(0x2, --count, result, exception) && count > 0);
        return result;
    }
}
