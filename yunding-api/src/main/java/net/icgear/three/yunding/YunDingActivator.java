package net.icgear.three.yunding;

import java.util.Set;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.google.common.collect.Sets;
import com.suisrc.jaxrsapi.core.AccessTokenActivator;
import com.suisrc.jaxrsapi.core.token.Token;

import net.icgear.three.yunding.rest.api.AuthenticateRest;
import net.icgear.three.yunding.rest.api.ShopsRest;
import net.icgear.three.yunding.rest.dto.AuthenticateBody;
import net.icgear.three.yunding.rest.dto.AuthenticateResult;

/**
 * 程序入口配置抽象
 * 
 * @author Y13
 */
@Named(Consts.NAMED)
@ApplicationScoped
public class YunDingActivator extends AccessTokenActivator {

    /**
     * 认证
     */
    private AuthenticateRest authenticate;
    
    /**
     * 组织
     */
    private String organisation;
    
    /**
     * 账户
     */
    private String username;
    
    /**
     * 密码
     */
    private String password;
    
    @Inject
    public void setAuthenticate(AuthenticateRest authenticate) {
        this.authenticate = authenticate;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(String key, Class<T> type) {
        if (type == String.class && Consts.APP_TOKEN.equals(key)) {
            return (T) getToken();
        }
        return super.getAdapter(key);
    }

    @Override
    public String getAppName() {
        return "YunDing";
    }
    
    @Override
    public boolean isStdInject() {
        return false;
    }
    
    @Override
    public void doPostConstruct() {
        organisation = System.getProperty(Consts.KEY_APP_ORG);
        username = System.getProperty(Consts.KEY_APP_USR);
        password = System.getProperty(Consts.KEY_APP_PWD);
        super.doPostConstruct();
    }

    @Override
    protected String getBaseUrlKey() {
        return Consts.KEY_APP_URL;
    }

    @Override
    protected Token getTokenByRemote() {
        AuthenticateBody body = new AuthenticateBody();
        body.setOrganisation(organisation);
        body.setUsername(username);
        body.setPassword(password);
        AuthenticateResult result = authenticate.postAuthenticate(body);
        if (result.isResult()) {
            Token token = new Token();
            token.setAccessToken(result.getData().getToken());
            token.setExpiresIn(7200);
            return token;
        } else {
            throw new RuntimeException("获取认证发生异常: " + result.getMsg());
        }
    }

    @Override
    public Set<Class<?>> getClasses() {
        return Sets.newHashSet(
//                AuthenticateRest.class,
                ShopsRest.class
            );
    }

    /**
     * 临时保留token
     */
    @Override
    protected String getTempFileName() {
        return "target/token.obj";
    }
}
