package net.icgear.three.yunding;

/**
 * 常量
 * @author Y13
 *
 */
public interface Consts {
    
    /**
     * 启动器标识
     */
    final String NAMED = "YunDingActivator";
    
    /**
     * Token
     */
    final String APP_TOKEN = "yunding-token";
    
    /**
     * 公司
     */
    final String APP_ORG = "yunding-organisation";
    
    /**
     * 用户
     */
    final String APP_USR = "yunding-username";
    
    /**
     * 密码
     */
    final String APP_PWD = "yunding-password";

    /**
     * 应用ID主键
     */
    final String KEY_APP_ORG = "net.icgear.product.yunding.organisation";

    /**
     * 应用密钥
     */
    final String KEY_APP_USR = "net.icgear.product.yunding.username";

    
    /**
     * 共享服务器地址
     */
    final String KEY_APP_PWD = "net.icgear.product.yunding.password";

    
    /**
     * 共享服务器地址
     */
    final String KEY_APP_URL = "net.icgear.product.yunding.url";

    /**
     * 应用使用代理访问主机地址
     */
    final String KEY_APP_PROXY_HOST = "net.icgear.product.yunding.proxy.host";

    /**
     * 应用使用代理访问主机端口
     */
    final String KEY_APP_PROXY_PORT = "net.icgear.product.yunding.proxy.port";

}
