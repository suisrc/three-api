package net.icgear.three.yunding.rest.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.three.yunding.rest.dto.AuthenticateBody;
import net.icgear.three.yunding.rest.dto.AuthenticateResult;

/**
 * 测试接口
 * 
 * 会员 APIs
 * 
 * @author Y13
 *  
 */
@RemoteApi
public interface AuthenticateRest {
    
    /**
     * 
     * POST /api/Authenticate
     * Host: dingding.yunding360.com
     * Content-Type: application/json
     * 
     * body:
     * {
     * "organisation": "企业号",
     * "username": "用户名",
     * "password": "xxxxxxxx"
     * }
     * result:
     * {
     * "result": true, //true 或者 false
     * "msg": "message", //消息
     * "data": { //数据，如果请求成功，携带该字段，并且包含token字段。
     * "token": "token"
     * }
     * }
     * 
     *    参数名 .         类型.      可选.      说明
     *    organisation    String    必填.      企业号
     *    username        String    必填.      用户名
     *    password        String    必填.      用户密码
     *    
     *    参数名.          类型.                说明
     *    result          Boolean             请求结果
     *    msg             String              返回消息
     *    data            object              数据字段，可为空
     *    token           String              身份认证令牌
     * 
     */
    @POST
    @Path("Authenticate")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    AuthenticateResult postAuthenticate(AuthenticateBody body);

}
