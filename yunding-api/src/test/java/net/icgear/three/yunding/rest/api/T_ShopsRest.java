package net.icgear.three.yunding.rest.api;

import org.junit.Before;
import org.junit.Test;

import com.suisrc.core.simple.SimpleCdiAdapter;
import com.suisrc.core.utils.ReflectionUtils;
import com.suisrc.jaxrsapi.core.JaxrsapiConsts;
import com.suisrc.jaxrsapi.core.factory.NSCF;
import com.suisrc.jaxrsapi.core.token.Token;

import net.icgear.three.yunding.Consts;
import net.icgear.three.yunding.YunDingActivator;
import net.icgear.three.yunding.rest.api.ShopsRest;
import net.icgear.three.yunding.rest.api.impl.AuthenticateRestYunDing1;
import net.icgear.three.yunding.rest.api.impl.ShopsRestYunDing1;

public class T_ShopsRest {

    private ShopsRest rest;
    
    @Before
    public void setUp() throws Exception {
        System.setProperty(JaxrsapiConsts.DEBUG, "true");
        SimpleCdiAdapter.buildEnvironment();

        System.setProperty(Consts.KEY_APP_ORG, "ys_r3ee0757ca6k");
        System.setProperty(Consts.KEY_APP_USR, "123456");
        System.setProperty(Consts.KEY_APP_PWD, "C1009");
//        System.setProperty(Consts.KEY_APP_ORG, "丝域连锁");
//        System.setProperty(Consts.KEY_APP_USR, "丝域连锁");
//        System.setProperty(Consts.KEY_APP_PWD, "123456");
//        System.setProperty(Consts.KEY_APP_URL, "https://dingding.yunding360.com/api/");
        System.setProperty(Consts.KEY_APP_URL, "http://127.0.0.1:8771/api/");

        YunDingActivator activator = new YunDingActivator(){
            int offset = 123456;
            @Override
            protected Token getTokenByRemote() {
                Token token = new Token();
                token.setAccessToken(String.valueOf(offset++));
                token.setExpiresIn(2);
                System.out.println("获取新令牌:\n" + ReflectionUtils.testPrint(token));
                return token;
            }
        };
        NSCF.build(activator);
        // 向全局注入激活器
        SimpleCdiAdapter.putInjectBean(activator, Consts.NAMED);
        activator.setAuthenticate(new AuthenticateRestYunDing1());

        rest = new ShopsRestYunDing1();
    }
    
    @Test
    public void testShopInfo() {
        Object result = rest.getShopInfo(null, false);
        System.out.println(ReflectionUtils.testPrint(result));
    }
    
}
