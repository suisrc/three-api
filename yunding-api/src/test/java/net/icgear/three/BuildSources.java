package net.icgear.three;

import com.suisrc.jaxrsapi.core.factory.NSCF;

import net.icgear.three.yunding.YunDingActivator;

/**
 * 生成接口的实现内容
 * 
 * @author Y13
 *
 */
class BuildSources {
    
    /**
     * 构建调用代码
     * @param args
     */
    public static void main(String[] args) {
        NSCF.buildSources("YunDing", YunDingActivator.class);
    }

}
