package net.icgear.product.hairology.yoorstore.server.rest.api;

import org.junit.Before;
import org.junit.Test;

import com.suisrc.jaxrsapi.core.factory.NSCF;

import net.icgear.product.hairology.yoorstore.server.Consts;
import net.icgear.product.hairology.yoorstore.server.YoorStoreActivator;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberAccountBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberAcctResult;

public class T_MemAcctRest {

    private MemberAccountRest rest;
    
    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        System.setProperty(Consts.KEY_APP_ID, "ys_r3ee0757ca6k");
        System.setProperty(Consts.KEY_APP_SECRET, "123456");
        System.setProperty(Consts.KEY_APP_CODE, "C1009");
//        System.setProperty(Consts.KEY_BASE_URL, "http://127.0.0.1:8771/v4");
        System.setProperty(Consts.KEY_BASE_URL, "http://partner.api.yoorstore.cn/v4");

        NSCF.build((String) null, YoorStoreActivator.class);

        rest = NSCF.get(MemberAccountRest.class);
    }
    
    @Test
    public void test() {
        QueryMemberAccountBody body = new QueryMemberAccountBody();
        body.setVipNo("425235");
        body.setOutletCode("44254");
        body.setCouponId("10091524641352845214");
//        CreateMemberParam param = new CreateMemberParam();
//        param.setTs(123456L);
//        CreateMemberResult result = rest.createMember(param, body);
        QueryMemberAcctResult result = rest.queryMemberAccount(body);

        System.out.println(result);
        System.out.println(result.getErrmsg());
        System.out.println(result.getErrcode());
        System.out.println(result.getMessage());
    }
    
    public static void main(String[] args) throws Exception {
        T_MemAcctRest yyc = new T_MemAcctRest();
        yyc.setUp();
        yyc.test();
    }
}
