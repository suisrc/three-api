package net.icgear.product.hairology.yoorstore.server.rest.api;

import org.junit.Before;
import org.junit.Test;

import com.suisrc.jaxrsapi.core.factory.NSCF;

import net.icgear.product.hairology.yoorstore.server.Consts;
import net.icgear.product.hairology.yoorstore.server.YoorStoreActivator;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberResult;

public class T_TestRest {

    private TestRest rest;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        System.setProperty(Consts.KEY_APP_ID, "ys_r3ee0757ca6k");
        System.setProperty(Consts.KEY_APP_SECRET, "123456");
        System.setProperty(Consts.KEY_APP_CODE, "C1002");
        System.setProperty(Consts.KEY_BASE_URL, "http://127.0.0.1:8771/v4");

        NSCF.build((String) null, YoorStoreActivator.class);

        rest = NSCF.get(TestRest.class);
    }

    @Test
    public void test() {
        CreateMemberBody body = new CreateMemberBody();
        body.setNickName("L");
        body.setFullName("小林");
//        CreateMemberParam param = new CreateMemberParam();
//        param.setTs(123456L);
//        CreateMemberResult result = rest.createMember(param, body);
        CreateMemberResult result = rest.createMember(body);

        System.out.println(result.getErrmsg());
    }
    
    public static void main(String[] args) throws Exception {
        T_TestRest tt = new T_TestRest();
        tt.setUp();
        
        tt.test();
    }

}
