package net.icgear.product.hairology.yoorstore.server.rest.api;

import org.junit.Before;
import org.junit.Test;

import com.suisrc.jaxrsapi.core.factory.NSCF;

import net.icgear.product.hairology.yoorstore.server.Consts;
import net.icgear.product.hairology.yoorstore.server.YoorStoreActivator;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberResult;

public class T_MemberRest {

    private MemberServerRest rest;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        System.setProperty(Consts.KEY_APP_ID, "ys_r3ee0757ca6k");
        System.setProperty(Consts.KEY_APP_SECRET, "123456");
        System.setProperty(Consts.KEY_APP_CODE, "C1009");
        System.setProperty(Consts.KEY_BASE_URL, "http://partner.api.yoorstore.cn/v4");
//        System.setProperty(Consts.KEY_BASE_URL, "http://127.0.0.1:8771/v4");

        NSCF.build((String) null, YoorStoreActivator.class);

        rest = NSCF.get(MemberServerRest.class);
    }

    @Test
    public void test() {
        QueryMemberBody body = new QueryMemberBody();
        body.setVipNo("HY0000001");
//        body.setCompanyCode("C1009");
//        body.setFullName("小林");
//        CreateMemberParam param = new CreateMemberParam();
//        param.setTs(123456L);
//        CreateMemberResult result = rest.createMember(param, body);
        QueryMemberResult result = rest.queryMember(body);

        System.out.println(result);
        System.out.println(result.getErrmsg());
    }
    
    public static void main(String[] args) throws Exception {
        T_MemberRest tt = new T_MemberRest();
        tt.setUp();
        
        tt.test();
    }

}
