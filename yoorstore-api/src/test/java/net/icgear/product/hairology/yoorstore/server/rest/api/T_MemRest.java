package net.icgear.product.hairology.yoorstore.server.rest.api;


import org.junit.Before;
import org.junit.Test;

import com.suisrc.jaxrsapi.core.factory.NSCF;

import net.icgear.product.hairology.yoorstore.server.Consts;
import net.icgear.product.hairology.yoorstore.server.YoorStoreActivator;
import net.icgear.product.hairology.yoorstore.server.rest.dto.AdjustPointBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.AdjustPointResult;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberResult;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberResult;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberTagsBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberTagsResult;
import net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberResult;

public class T_MemRest {

    private MemberServerRest rest;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() throws Exception {
        System.setProperty(Consts.KEY_APP_ID, "ys_r3ee0757ca6k");
        System.setProperty(Consts.KEY_APP_SECRET, "123456");
        System.setProperty(Consts.KEY_APP_CODE, "C1009");
        // System.setProperty(Consts.KEY_BASE_URL, "http://127.0.0.1:8771/v4");
        System.setProperty(Consts.KEY_BASE_URL, "http://partner.api.yoorstore.cn/v4");

        NSCF.build((String) null, YoorStoreActivator.class);
        rest = NSCF.get(MemberServerRest.class);
    }
    
    @Test
    public void test_create() {

        // 会员创建
        CreateMemberBody body = new CreateMemberBody();

        // body.setVipNo("12345678");
        // body.setRefVipNo("sssss");
        body.setOutletCode("02010101001");
        body.setFullName("小油壶");
        body.setMobile("12345554675");
//        body.setRegDate("2018-08-19 08:09:01");
        // body.setNickName("strassssssssssssssssssssssssssssssssssssssssssssssssssss");
        // body.setSubSrcType("ssssssss");
        body.setGrade("0");
        body.setGender(2);
        // body.setVipNo("201799827");
        // body.setMobile("13801010202");
//        body.setAddress("辽宁省大连市高新园区");
//        body.setBirthday("2017-01-01");
        CreateMemberResult result = rest.createMember(body);
        System.out.println("errcode : " + result.getErrcode());
        System.out.println("message : " + result.getMessage());
        System.out.println( "ref_id : "+ result.getData().getRef_id());
        System.out.println( "vip_no : "+ result.getData().getVip_no());
      
    }

    @Test
    public void test_query() {

        // 会员查询
        QueryMemberBody body = new QueryMemberBody();
         body.setVipNo("S100019");
//        body.setMobile("15599996699");
        QueryMemberResult result = rest.queryMember(body);

        System.out.println(result);
        System.out.println(result.getErrcode());
        System.out.println(result.getMessage());
    }

    @Test
    public void test_update() {

        // 会员更新
        UpdateMemberBody body = new UpdateMemberBody();
        body.setVipNo("S100017");
        body.setFullName("aaaassssssssdxxxxxx");
        body.setBirthday("1992-08-01");
        body.setAddress("sssssssss");
        body.setGender(2);
        body.setOccupation("家庭主妇");
        // body.setVipNo("201799827");
        // body.setMobile("13801010202");
        body.setAddress("辽宁省");
        UpdateMemberResult result = rest.updateMember(body);
        System.out.println(result);
        System.out.println(result.getData());
        System.out.println(result.getErrcode());
        System.out.println(result.getMessage());
    }
    
    @Test
    public void test_memberTags() {

        // 会员标签查询
        QueryMemberTagsBody body = new QueryMemberTagsBody();
        // body.setVipNo("S100008");
        body.setVipNo("S100008");

        // body.setMobile("15599996699");
        QueryMemberTagsResult result = rest.queryMemberTags(body);
        // Object result = rest.queryMemberTags(body);

        System.out.println(result);
        // System.out.println(result.getErrcode());
        // System.out.println(result.getMessage());
    }

    /**
     * "vipNo": "201799827", "actionType": 11, "value": 100, "serialNum":
     * "123456789abcdefghi", "remark": "消费赠送", "actionTime": "2017-12-08
     * 14:20:59"
     */
    @Test
    public void test_adjustPoints() {

        // 积分变更接口
        AdjustPointBody body = new AdjustPointBody();

        body.setVipNo("S100008");
        body.setActionType("11");
        body.setValue("888");
        body.setSerialNum("123");
        body.setRemark("sssssssss");
        body.setActionTime("2017-12-08 14:20:59");

        AdjustPointResult result = rest.adjustPoints(body);

        System.out.println(result);
        // System.out.println(result.getErrcode());
        // System.out.println(result.getMessage());
    }

    public static void main(String[] args) throws Exception {

        T_MemRest tt = new T_MemRest();
        tt.setUp();
//         tt.test_create();
//         tt.test_query();
        // tt.test_update();
         tt.test_memberTags();
//        tt.test_adjustPoints();
    }

}
