package net.icgear.product.hairology.yoorstore.server.rest.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import net.icgear.common.bean.BCU;
import net.icgear.common.bean.OriginIgnore;
import net.icgear.common.bean.OriginMethod;
import net.icgear.common.bean.OriginType;
import net.icgear.product.hairology.db.base.Constant.CardType;
import net.icgear.product.hairology.db.base.Constant.CommodityType;
import net.icgear.product.hairology.db.base.Constant.OrderPayType;
import net.icgear.product.hairology.db.entity.CardInternalCommodityPrice;
import net.icgear.product.hairology.db.entity.Commodity;
import net.icgear.product.hairology.db.entity.DiscountUse;
import net.icgear.product.hairology.db.entity.Order;
import net.icgear.product.hairology.db.entity.OrderDetail;
import net.icgear.product.hairology.db.entity.OrderDiscount;
import net.icgear.product.hairology.db.entity.OrderPay;
import net.icgear.product.hairology.db.entity.SingleDiscount;
import net.icgear.product.hairology.db.entity.UserCard;

@OriginType(clazz = Order.class)
public class PosBody {

    /** 门店代码 门店UUID */
    private String outletCode;

    /** 会员卡号 CrmCode */
    private String vipNo;

    /** 零售类型 1 - 正常， 2 - 退货 默认1 */
    private Integer type = 1;

    /** pos单据日期 订单更新时间 */
    private String posDate;

    /** POS单据号 订单编号 */
    private String posNo;

    /** 优惠 */
    private List<Coupon> coupons = new ArrayList<>();

//    /** 付款明细 */
//    private List<Payment> payments = new ArrayList<>();

    /** 行项目 */
    private List<LineItem> lineItems = new ArrayList<>();

    /** 单据总金额 订单实付金额 */
    private BigDecimal totalAmount;

    /**
     * 获取门店代码 门店UUID
     * @return the outletCode
     */
    public String getOutletCode() {
        return outletCode;
    }

    /**
     * 设定门店代码 门店UUID
     * @param outletCode the outletCode to set
     */
    @OriginMethod("getStore.getUuid")
    public void setOutletCode(String outletCode) {
        this.outletCode = outletCode;
    }

    /**
     * 获取会员卡号 CrmCode
     * @return the vipNo
     */
    public String getVipNo() {
        return vipNo;
    }

    /**
     * 设定会员卡号 CrmCode
     * @param vipNo the vipNo to set
     */
    @OriginMethod("getUser.getCrmCode")
    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

    /**
     * 获取零售类型 1 - 正常， 2 - 退货 默认1
     * @return the type
     */
    public Integer getType() {
        return type;
    }

    /**
     * 设定零售类型 1 - 正常， 2 - 退货 默认1
     * @param type the type to set
     */
    @OriginIgnore
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 获取pos单据日期 订单更新时间
     * @return the posDate
     */
    public String getPosDate() {
        return posDate;
    }

    /**
     * 设定pos单据日期 订单更新时间
     * @param posDate the posDate to set
     */
    @OriginMethod("getUpdateTime")
    public void setPosDate(Timestamp updateTime) {
        this.posDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(updateTime);
    }

    /**
     * 获取POS单据号 订单编号
     * @return the posNo
     */
    public String getPosNo() {
        return posNo;
    }

    /**
     * 设定POS单据号 订单编号
     * @param posNo the posNo to set
     */
    @OriginMethod("getCode")
    public void setPosNo(String posNo) {
        this.posNo = posNo;
    }

    /**
     * 获取优惠
     * @return the coupons
     */
    public List<Coupon> getCoupons() {
        return coupons;
    }

    /**
     * 设定整单优惠
     * @param coupons the coupons to set
     */
    @OriginMethod("getDiscountUses")
    public void setCoupons(List<DiscountUse> discountUses) {
        if (discountUses != null) {
            this.coupons.addAll(discountUses.stream().map(Coupon::new).collect(Collectors.toList()));
        }
    }

//    /**
//     * 获取付款明细
//     * @return the payments
//     */
//    public List<Payment> getPayments() {
//        return payments;
//    }
//
//    /**
//     * 设定付款明细
//     * @param payments the payments to set
//     */
//    @OriginMethod("getOrderPays")
//    public void setPayments(List<OrderPay> orderPays) {
//        if (orderPays != null) {
//            this.payments = orderPays.stream().map(Payment::new).collect(Collectors.toList());
//        }
//    }

    /**
     * 获取行项目
     * @return the lineItems
     */
    public List<LineItem> getLineItems() {
        return lineItems;
    }

    /**
     * 设定行项目
     * @param lineItems the lineItems to set
     */
    @OriginMethod("getOrderDetails")
    public void setLineItems(List<OrderDetail> orderDetails) {
        if (orderDetails != null) {
            this.lineItems = BCU.P2E(orderDetails, LineItem.class, OrderDetail.class, 0);
        }
    }

    /**
     * 获取单据总金额 订单实付金额
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * 设定单据总金额 订单实付金额
     * @param totalAmount the totalAmount to set
     */
    @OriginMethod("getShouldPayPrice")
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "{\"outletCode\":\"" + outletCode + "\", \"vipNo\":\"" + vipNo + "\", \"type\":" + type + ", \"posDate\":\""
                + posDate + "\", \"posNo\":\"" + posNo + "\", \"coupons\":" + coupons /* + ", \"payments\":" + payments */
                + ", \"lineItems\":" + lineItems + ", \"totalAmount\":" + totalAmount + "}";
    }

    public static class Coupon {

        /** 优惠key */
        private String couponId;

        /** 优惠金额 */
        private BigDecimal amount;

        public Coupon() {
            super();
        }

        public Coupon(SingleDiscount singleDiscount) {
            super();
            this.couponId = singleDiscount.getKey();
            this.amount = singleDiscount.getAmount();
        }

        public Coupon(OrderDiscount orderDiscount) {
            super();
            this.couponId = orderDiscount.getKey();
            this.amount = orderDiscount.getAmount();
        }

        public Coupon(DiscountUse discountUse) {
            super();
            this.couponId = discountUse.getCouponId();
            this.amount = discountUse.getAmount();
        }

        /**
         * 获取优惠key
         * @return the couponId
         */
        public String getCouponId() {
            return couponId;
        }

        /**
         * 设定优惠key
         * @param couponId the couponId to set
         */
        public void setCouponId(String couponId) {
            this.couponId = couponId;
        }

        /**
         * 获取优惠金额
         * @return the amount
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * 设定优惠金额
         * @param amount the amount to set
         */
        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "{\"couponId\":\"" + couponId + "\", \"amount\":" + amount + "}";
        }
    }

    public static class Payment {

        public static final String ALIPAY = "2";
        public static final String WECHAT = "1";

        /** 支付方式 */
        private String payMethod;

        /** 支付金额 */
        private BigDecimal amount;

        public Payment() {
            super();
        }

        public Payment(OrderPay orderPay) {
            super();
            switch (orderPay.getPayTp()) {
                case 1:
                    this.payMethod = WECHAT;
                    break;
                case 2:
                    this.payMethod = ALIPAY;
                    break;
                default:
                    break;
            }
            this.amount = orderPay.getAmount();
        }

        /**
         * 获取支付方式
         * @return the payMethod
         */
        public String getPayMethod() {
            return payMethod;
        }

        /**
         * 设定支付方式
         * @param payMethod the payMethod to set
         */
        public void setPayMethod(String payMethod) {
            this.payMethod = payMethod;
        }

        /**
         * 获取支付金额
         * @return the amount
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * 设定支付金额
         * @param amount the amount to set
         */
        public void setAmount(BigDecimal amount) {
            this.amount = amount;
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "{\"payMethod\":\"" + payMethod + "\", \"amount\":" + amount + "}";
        }
    }

    @OriginType(clazz = OrderDetail.class)
    public static class LineItem {

        public static final String PROJECT = "80";
        public static final String PRODUCT = "20";
        public static final String COMBO = "100";
        public static final String SAVING = "110";

        public static enum Method {
            /** 划卡 */
            DRAW(" ", OrderPayType.DRAW.id()),
            /** 划套餐卡 */
            COMBO("6", null),
            /** 划储值类卡 */
            SAVING("8", null),
            /** 店长签单 */
            WRITTEN("9", OrderPayType.WRITTEN.id()),
            /** 团购 */
            GROUP("10", OrderPayType.GROUP.id());
            private String outNo;
            private Integer inNo;
            private Method(String outNo, Integer inNo) {
                this.outNo = outNo;
                this.inNo = inNo;
            }
            /** 获取对小优惠类型 */
            public String out() {
                return outNo;
            }
            /** 获取内部支付类型 */
            public Integer in() {
                return inNo;
            }
        }

        /** 行id 订单详情id */
        private Integer detailId;

        /** 明细行号 index */
        private Integer lineNo;

        /** 商品代码 商品UUID */
        private String itemCode;

        /** 商品名称 */
        private String itemName;

        /** 商品大类 功效类别 */
        private String category;

        /** 零售价 */
        private BigDecimal linePrice;

        /** 折后价格 实际售价 */
        private BigDecimal lineNetPrice;

        /** 数量 */
        private Integer qty;

        /** 标准金额 零售价*数量 */
        @SuppressWarnings("unused")
        private BigDecimal lineAmount;

        /** 折后金额 实际售价*数量 */
        @SuppressWarnings("unused")
        private BigDecimal lineNetAmount;

        /** 成本价 */
        private BigDecimal costPrice;

        /** 卡号 所划卡项id */
        private String cardNo;

        /** 商品类型 */
        private String materialType;

        /** 备注 */
        private String comments;

        /** 所划卡项 */
        private UserCard userCard;

        /** 购买商品 */
        private Commodity commodity;

        /** 补差价 */
        private BigDecimal differenceAmount;

        /** 划卡金额 */
        private BigDecimal payAmount;
        
        /** 子项付款方式 */
        private String paymentMethod;

        /**
         * 获取行id 订单详情id
         * @return the detailId
         */
        public Integer getDetailId() {
            return detailId;
        }

        /**
         * 设定行id 订单详情id
         * @param detailId the detailId to set
         */
        @OriginMethod("getId")
        public void setDetailId(Integer detailId) {
            this.detailId = detailId;
        }

        /**
         * 获取明细行号 index
         * @return the lineNo
         */
        public Integer getLineNo() {
            return lineNo;
        }

        /**
         * 设定明细行号 index
         * @param lineNo the lineNo to set
         */
        @OriginIgnore
        public void setLineNo(Integer lineNo) {
            this.lineNo = lineNo;
        }

        /**
         * 获取商品代码 商品UUID
         * @return the itemCode
         */
        public String getItemCode() {
            return itemCode;
        }

        /**
         * 设定商品代码 商品UUID
         * @param itemCode the itemCode to set
         */
        @OriginMethod("getCommodity.getUuid")
        public void setItemCode(String itemCode) {
            this.itemCode = itemCode;
        }

        /**
         * 获取商品名称
         * @return the itemName
         */
        public String getItemName() {
            return itemName;
        }

        /**
         * 设定商品名称
         * @param itemName the itemName to set
         */
        @OriginMethod("getCommodity.getName")
        public void setItemName(String itemName) {
            this.itemName = itemName;
        }

        /**
         * 获取商品大类 功效类别
         * @return the category
         */
        public String getCategory() {
            return category;
        }

        /**
         * 设定商品大类 功效类别
         * @param category the category to set
         */
        @OriginMethod("getCommodity.getCommodityI.getEfficacyClass.getName")
        public void setCategory(String category) {
            this.category = category;
        }

        /**
         * 获取零售价
         * @return the linePrice
         */
        public BigDecimal getLinePrice() {
            return linePrice;
        }

        /**
         * 设定零售价
         * @param linePrice the linePrice to set
         */
        @OriginMethod("getRetailPrice")
        public void setLinePrice(BigDecimal linePrice) {
            this.linePrice = linePrice;
        }

        /**
         * 获取折后价格 实际售价
         * @return the lineNetPrice
         */
        public BigDecimal getLineNetPrice() {
            if (userCard != null) {// 划卡
                if (userCard.getCardType().equals(CardType.COMBO.id())) {// 套餐卡
                    return userCard.getCard().getInternalPrices().stream()
                            .filter(h -> h.getCommodity().getId().equals(commodity.getParent().getId()))
                            .map(CardInternalCommodityPrice::getStaffCardPrice).findAny().orElse(BigDecimal.ZERO)
                            .add(Optional.ofNullable(this.differenceAmount).orElse(BigDecimal.ZERO));
                } else {// 储值类
                    return Optional.ofNullable(this.payAmount).orElse(BigDecimal.ZERO)
                            .add(Optional.ofNullable(this.differenceAmount).orElse(BigDecimal.ZERO));
                }
            }
            return lineNetPrice;
        }

        /**
         * 设定折后价格 实际售价
         * @param lineNetPrice the lineNetPrice to set
         */
        @OriginMethod("getOriginPrice")
        public void setLineNetPrice(BigDecimal lineNetPrice) {
            this.lineNetPrice = lineNetPrice;
        }

        /**
         * 获取数量
         * @return the qty
         */
        public Integer getQty() {
            return qty;
        }

        /**
         * 设定数量
         * @param qty the qty to set
         */
        @OriginMethod("getNum")
        public void setQty(Integer qty) {
            this.qty = qty;
        }

        /**
         * 获取标准金额 零售价 数量
         * @return the lineAmount
         */
        public BigDecimal getLineAmount() {
            return linePrice == null ? BigDecimal.ZERO : linePrice.multiply(new BigDecimal(qty.toString()));
        }

        /**
         * 设定标准金额 零售价 数量
         * @param lineAmount the lineAmount to set
         */
        @OriginIgnore
        public void setLineAmount(BigDecimal lineAmount) {
            this.lineAmount = lineAmount;
        }

        /**
         * 获取折后金额 实际售价 数量
         * @return the lineNetAmount
         */
        public BigDecimal getLineNetAmount() {
            return lineNetPrice == null ? BigDecimal.ZERO : lineNetPrice.multiply(new BigDecimal(qty.toString()));
        }

        /**
         * 设定折后金额 实际售价 数量
         * @param lineNetAmount the lineNetAmount to set
         */
        @OriginIgnore
        public void setLineNetAmount(BigDecimal lineNetAmount) {
            this.lineNetAmount = lineNetAmount;
        }

        /**
         * 获取成本价
         * @return the costPrice
         */
        public BigDecimal getCostPrice() {
            return costPrice;
        }

        /**
         * 设定成本价
         * @param costPrice the costPrice to set
         */
        @OriginMethod("getCommodity.getCost")
        public void setCostPrice(BigDecimal costPrice) {
            this.costPrice = costPrice;
        }

        /**
         * 获取卡号 所划卡项id
         * @return the cardNo
         */
        public String getCardNo() {
            return cardNo;
        }

        /**
         * 设定卡号 所划卡项id
         * @param cardNo the cardNo to set
         */
        @OriginMethod("getUserCard.getId")
        public void setCardNo(Integer cardNo) {
            this.cardNo = cardNo.toString();
        }

        /**
         * 获取商品类型
         * @return the materialType
         */
        public String getMaterialType() {
            return materialType;
        }

        /**
         * 设定商品类型
         * @param materialType the materialType to set
         */
        @OriginMethod("getCommodity")
        public void setMaterialType(Commodity commodity) {
            if (CommodityType.CARD.id().equals(commodity.getType())) {
                if (CardType.COMBO.id().equals(commodity.getCard().getType())) {
                    this.materialType = COMBO;
                } else {
                    this.materialType = SAVING;
                }
            } else if (CommodityType.PROJECT.id().equals(commodity.getType())) {
                this.materialType = PROJECT;
            } else if (CommodityType.PRODUCT.id().equals(commodity.getType())) {
                this.materialType = PRODUCT;
            }
        }

        /**
         * 获取备注
         * @return the comments
         */
        public String getComments() {
            return comments;
        }

        /**
         * 设定备注
         * @param comments the comments to set
         */
        @OriginMethod("getType")
        public void setComments(Integer comments) {
            for (OrderPayType type : OrderPayType.values()) {
                if (type.id().equals(comments)) {
                    this.comments = type.value();
                }
            }
        }

        /**
         * 设定所划卡项
         * @param userCard the userCard to set
         */
        @OriginMethod("getUserCard")
        public void setUserCard(UserCard userCard) {
            this.userCard = userCard;
        }

        /**
         * 设定购买商品
         * @param commodity the commodity to set
         */
        @OriginMethod("getCommodity")
        public void setCommodity(Commodity commodity) {
            this.commodity = commodity;
        }

        /**
         * 设定补差价
         * @param differenceAmount the differenceAmount to set
         */
        @OriginMethod("getDifferenceAmount")
        public void setDifferenceAmount(BigDecimal differenceAmount) {
            this.differenceAmount = differenceAmount;
        }

        /**
         * 设定划卡金额
         * @param payAmount the payAmount to set
         */
        @OriginMethod("getPayAmount")
        public void setPayAmount(BigDecimal payAmount) {
            this.payAmount = payAmount;
        }

        /**
         * 获取子项付款方式
         * @return the paymentMethod
         */
        public String getPaymentMethod() {
            if (Method.DRAW.out().equals(this.paymentMethod)) {
                if (this.userCard != null) {
                    if (CardType.COMBO.id().equals(this.userCard.getCardType())) {
                        return Method.COMBO.out();
                    } else {
                        return Method.SAVING.out();
                    }
                }
            }
            return paymentMethod;
        }

        /**
         * 设定子项付款方式
         * @param paymentMethod the paymentMethod to set
         */
        @OriginMethod("getPayType")
        public void setPaymentMethod(Integer payType) {
            for (Method method : Method.values()) {
                if (payType.equals(method.in())) {
                    this.paymentMethod = method.out();
                    return;
                }
            }
        }

        /* (non-Javadoc)
         * @see java.lang.Object#toString()
         */
        @Override
        public String toString() {
            return "{\"detailId\":" + detailId + ", \"lineNo\":" + lineNo + ", \"itemCode\":\"" + itemCode
                    + "\", \"itemName\":\"" + itemName + "\", \"category\":\"" + category + "\", \"linePrice\":" + linePrice
                    + ", \"lineNetPrice\":" + lineNetPrice + ", \"qty\":" + qty + ", \"lineAmount\":" + getLineAmount()
                    + ", \"lineNetAmount\":" + getLineNetAmount() + ", \"paymentMethod\":" + getPaymentMethod()
                    + ", \"costPrice\":" + costPrice + ", \"cardNo\":\"" + cardNo + "\", \"materialType\":\"" + materialType
                    + "\", \"comments\":\"" + comments + "\"}";
        }
    }

}
