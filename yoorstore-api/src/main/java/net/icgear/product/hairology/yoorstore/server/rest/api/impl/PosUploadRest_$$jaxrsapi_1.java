package net.icgear.product.hairology.yoorstore.server.rest.api.impl;

import javax.inject.Inject;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberParam;
import javax.inject.Named;
import net.icgear.product.hairology.yoorstore.server.rest.api.PosUploadRest;
import com.suisrc.jaxrsapi.core.ApiActivator;
import com.suisrc.jaxrsapi.core.ServiceClient;
import java.util.List;
import javax.ws.rs.client.WebTarget;
import java.lang.String;
import javax.enterprise.context.ApplicationScoped;
import java.lang.Override;
import com.suisrc.jaxrsapi.core.proxy.ProxyBuilder;

/**
 * Follow the implementation of the restful 2.0 standard remote access agent.
 * <see>
 *   https://suisrc.github.io/jaxrsapi
 * <generateBy>
 *   com.suisrc.jaxrsapi.core.factory.ClientServiceFactory
 * <time>
 *   2018-05-04T14:35:16.549
 * <author>
 *   Y13
 */
@ApplicationScoped
public class PosUploadRest_$$jaxrsapi_1 implements PosUploadRest, ServiceClient {
    /*
     * 远程代理访问客户端控制器
     */
    private PosUploadRest proxy;
    /*
     * 远程服务器控制器，具有服务器信息
     */
    private ApiActivator activator;
    /**
     * 初始化
     */
    public void init() {
        WebTarget target = (WebTarget)activator.getAdapter(WebTarget.class);
        proxy = ProxyBuilder.builder(PosUploadRest.class, target).build();
    }
    /**
     * 获取远程服务器控制器
     */
    public ApiActivator getActivator() {
        return activator;
    }
    /**
     * 配置远程服务器控制器
     */
    @Inject
    @Named("YoorStoreActivator")
    public void setActivator(ApiActivator pm) {
        activator = pm;
        if (pm != null) init();

    }
    /**
     * 接口实现
     */
    @Override
    public net.icgear.product.hairology.yoorstore.server.rest.dto.YoorstoreErrMsg ccPosData(CreateMemberParam pm0, List pm1) {
        if (pm0.getSign() == null) {
            String temp = (String)activator.getAdapter("SIGN-DEFAULT", String.class);
            if (temp != null) pm0.setSign(temp);

        }
        return (net.icgear.product.hairology.yoorstore.server.rest.dto.YoorstoreErrMsg)proxy.ccPosData(pm0, pm1);
    }
    /**
     * 接口实现
     */
    @Override
    public net.icgear.product.hairology.yoorstore.server.rest.dto.YoorstoreErrMsg ccWriteOff(CreateMemberParam pm0, List pm1) {
        if (pm0.getSign() == null) {
            String temp = (String)activator.getAdapter("SIGN-DEFAULT", String.class);
            if (temp != null) pm0.setSign(temp);

        }
        return (net.icgear.product.hairology.yoorstore.server.rest.dto.YoorstoreErrMsg)proxy.ccWriteOff(pm0, pm1);
    }
}
