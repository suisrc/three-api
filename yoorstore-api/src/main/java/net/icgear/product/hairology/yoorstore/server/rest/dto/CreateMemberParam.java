package net.icgear.product.hairology.yoorstore.server.rest.dto;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;

import com.suisrc.jaxrsapi.core.annotation.Value;

import net.icgear.product.hairology.yoorstore.server.Consts;

/**
 * ts=1517477053493&sign=DEFAULT
 * 
 * 这里ts隐藏，交由拦截去处理，sign=DEFAULT具有标记作用，不用赋值
 * @author Y13
 *
 */
public class CreateMemberParam {
    
    /**
     * 不用赋值
     * 
     * 如果赋值也可以，不会影响系统
     */
    @QueryParam("ts")
    private Long ts;
    
    /**
     * 不用赋值，如果赋值，就以程序赋值为准，认证拦截去不会做处理
     */
    @QueryParam("sign")
    @Value(Consts.SIGN_DEFAULT)
    //@DefaultValue(Consts.SIGN_DEFAULT)
    private String sign;

    public Long getTs() {
        return ts;
    }

    public void setTs(Long ts) {
        this.ts = ts;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
