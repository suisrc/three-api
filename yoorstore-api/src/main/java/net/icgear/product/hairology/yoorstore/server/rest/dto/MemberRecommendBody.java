package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * 查询会员商品推荐参数
 * @author AmyHabor
 */
public class MemberRecommendBody {

    private String vipNo;

    public String getVipNo() {
        return vipNo;
    }

    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

    @Override
    public String toString() {
        return "MemberRecommendBody [vipNo=" + vipNo + "]";
    }
}
