package net.icgear.product.hairology.yoorstore.server.rest.api.impl;

import net.icgear.product.hairology.yoorstore.server.rest.api.MemberServerRest;
import javax.inject.Inject;
import javax.inject.Named;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberBody;
import java.lang.String;
import javax.enterprise.context.ApplicationScoped;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberParam;
import net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberPhoneBody;
import com.suisrc.jaxrsapi.core.ApiActivator;
import net.icgear.product.hairology.yoorstore.server.rest.dto.MemberRecommendBody;
import com.suisrc.jaxrsapi.core.ServiceClient;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberTagsBody;
import javax.ws.rs.client.WebTarget;
import net.icgear.product.hairology.yoorstore.server.rest.dto.AdjustPointBody;
import java.lang.Override;
import com.suisrc.jaxrsapi.core.proxy.ProxyBuilder;

/**
 * Follow the implementation of the restful 2.0 standard remote access agent.
 * <see>
 *   https://suisrc.github.io/jaxrsapi
 * <generateBy>
 *   com.suisrc.jaxrsapi.core.factory.ClientServiceFactory
 * <time>
 *   2018-06-25T09:07:58.113
 * <author>
 *   Y13
 */
@ApplicationScoped
public class MemberServerRest_$$jaxrsapi_1 implements MemberServerRest, ServiceClient {
    /*
     * 远程代理访问客户端控制器
     */
    private MemberServerRest proxy;
    /*
     * 远程服务器控制器，具有服务器信息
     */
    private ApiActivator activator;
    /**
     * 初始化
     */
    public void init() {
        WebTarget target = (WebTarget)activator.getAdapter(WebTarget.class);
        proxy = ProxyBuilder.builder(MemberServerRest.class, target).build();
    }
    /**
     * 获取远程服务器控制器
     */
    public ApiActivator getActivator() {
        return activator;
    }
    /**
     * 配置远程服务器控制器
     */
    @Inject
    @Named("YoorStoreActivator")
    public void setActivator(ApiActivator pm) {
        activator = pm;
        if (pm != null) init();

    }
    /**
     * 接口实现
     */
    @Override
    public net.icgear.product.hairology.yoorstore.server.rest.dto.AdjustPointResult adjustPoints(CreateMemberParam pm0, AdjustPointBody pm1) {
        if (pm0.getSign() == null) {
            String temp = (String)activator.getAdapter("SIGN-DEFAULT", String.class);
            if (temp != null) pm0.setSign(temp);

        }
        return (net.icgear.product.hairology.yoorstore.server.rest.dto.AdjustPointResult)proxy.adjustPoints(pm0, pm1);
    }
    /**
     * 接口实现
     */
    @Override
    public net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberResult createMember(CreateMemberParam pm0, CreateMemberBody pm1) {
        if (pm0.getSign() == null) {
            String temp = (String)activator.getAdapter("SIGN-DEFAULT", String.class);
            if (temp != null) pm0.setSign(temp);

        }
        return (net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberResult)proxy.createMember(pm0, pm1);
    }
    /**
     * 接口实现
     */
    @Override
    public net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberResult queryMember(CreateMemberParam pm0, QueryMemberBody pm1) {
        if (pm0.getSign() == null) {
            String temp = (String)activator.getAdapter("SIGN-DEFAULT", String.class);
            if (temp != null) pm0.setSign(temp);

        }
        return (net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberResult)proxy.queryMember(pm0, pm1);
    }
    /**
     * 接口实现
     */
    @Override
    public net.icgear.product.hairology.yoorstore.server.rest.dto.MemberRecommendResult queryMemberRecommend(CreateMemberParam pm0, MemberRecommendBody pm1) {
        if (pm0.getSign() == null) {
            String temp = (String)activator.getAdapter("SIGN-DEFAULT", String.class);
            if (temp != null) pm0.setSign(temp);

        }
        return (net.icgear.product.hairology.yoorstore.server.rest.dto.MemberRecommendResult)proxy.queryMemberRecommend(pm0, pm1);
    }
    /**
     * 接口实现
     */
    @Override
    public net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberTagsResult queryMemberTags(CreateMemberParam pm0, QueryMemberTagsBody pm1) {
        if (pm0.getSign() == null) {
            String temp = (String)activator.getAdapter("SIGN-DEFAULT", String.class);
            if (temp != null) pm0.setSign(temp);

        }
        return (net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberTagsResult)proxy.queryMemberTags(pm0, pm1);
    }
    /**
     * 接口实现
     */
    @Override
    public net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberResult updateMember(CreateMemberParam pm0, UpdateMemberBody pm1) {
        if (pm0.getSign() == null) {
            String temp = (String)activator.getAdapter("SIGN-DEFAULT", String.class);
            if (temp != null) pm0.setSign(temp);

        }
        return (net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberResult)proxy.updateMember(pm0, pm1);
    }
    /**
     * 接口实现
     */
    @Override
    public net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberPhoneResult updateMemberPhone(CreateMemberParam pm0, UpdateMemberPhoneBody pm1) {
        if (pm0.getSign() == null) {
            String temp = (String)activator.getAdapter("SIGN-DEFAULT", String.class);
            if (temp != null) pm0.setSign(temp);

        }
        return (net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberPhoneResult)proxy.updateMemberPhone(pm0, pm1);
    }
}
