package net.icgear.product.hairology.yoorstore.server.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 创建会员时候，使用的body内容
 * 
{
  "vipNo": "201799827",
  "refVipNo": "11110001",
  "outletCode": "44254",
  "fullName": "王小二",
  "nickName": "StarWang",
  "subSrcType": "15",
  "referrer": "张倩",
  "avatar": "http://xxxxxxx.com/avatar/w-logo.jpg",
  "gender": 1,
  "wxMpId":"gh_d3ee0757ca65",
  "weChatOpenId":"o8hR6s8gCzH1-hMlsF449Lk75XNs",
  "regDate": "2017-07-01 13:00:24",
  "mobile": "13801010202",
  "birthday": "1998-01-01",
  "occupation": "IT",
  "address":"广州市黄埔大道中309号",
  "email": "xiaoer.wang@yoorstore.cn",
  "grade": 3
}
 * @author Y13
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateMemberBody {
    
    private String vipNo;
    
    private String refVipNo;
    
    private String outletCode;
    
    private String fullName;
    
    private String nickName;
    
    private String subSrcType;
    
    private String referrer;
    
    private String avatar;
    
    private int gender;
    
    private String wxMpId;
    
    private String weChatOpendId;
    
    private String regDate;
    
    private String mobile;
    
    private String areaCode;
    
    private String birthday;
    
    private String occupation;
    
    private String address;
    
    private String email;
    
    private String grade;

    public String getVipNo() {
        return vipNo;
    }

    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

    public String getRefVipNo() {
        return refVipNo;
    }

    public void setRefVipNo(String refVipNo) {
        this.refVipNo = refVipNo;
    }

    public String getOutletCode() {
        return outletCode;
    }

    public void setOutletCode(String outletCode) {
        this.outletCode = outletCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSubSrcType() {
        return subSrcType;
    }

    public void setSubSrcType(String subSrcType) {
        this.subSrcType = subSrcType;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getWxMpId() {
        return wxMpId;
    }

    public void setWxMpId(String wxMpId) {
        this.wxMpId = wxMpId;
    }

    public String getWeChatOpendId() {
        return weChatOpendId;
    }

    public void setWeChatOpendId(String weChatOpendId) {
        this.weChatOpendId = weChatOpendId;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取
     * @return the areaCode
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 设定
     * @param areaCode the areaCode to set
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CreateMemberBody [vipNo=" + vipNo + ", refVipNo=" + refVipNo + ", outletCode=" + outletCode
                + ", fullName=" + fullName + ", nickName=" + nickName + ", subSrcType=" + subSrcType + ", referrer="
                + referrer + ", avatar=" + avatar + ", gender=" + gender + ", wxMpId=" + wxMpId + ", weChatOpendId="
                + weChatOpendId + ", regDate=" + regDate + ", mobile=" + mobile + ", birthday=" + birthday
                + ", occupation=" + occupation + ", address=" + address + ", email=" + email + ", grade=" + grade + "]";
    }

}
