package net.icgear.product.hairology.yoorstore.server.rest.api;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.product.hairology.yoorstore.server.rest.dto.AdjustPointBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.AdjustPointResult;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberParam;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberResult;
import net.icgear.product.hairology.yoorstore.server.rest.dto.MemberRecommendResult;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.MemberRecommendBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberResult;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberTagsBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberTagsResult;
import net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberPhoneBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberPhoneResult;
import net.icgear.product.hairology.yoorstore.server.rest.dto.UpdateMemberResult;

@RemoteApi
@ApplicationScoped
public interface MemberServerRest {

    /**
     * 会员查询<br/>
     * Method: POST<br/>
     * Endpoint: /v4/query-member<br/>
     * 请使用QueryMemberResult queryMember(QueryMemberBody body)
     */
    // @Deprecated
    @POST
    @Path("query-member")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    QueryMemberResult queryMember(@BeanParam CreateMemberParam param, QueryMemberBody body);

    default QueryMemberResult queryMember(QueryMemberBody body) {
        return queryMember(new CreateMemberParam(), body);
    }
    
    /**
     * 会员创建
     * Method: POST Endpoint: /v4/create-member
     * 请使用CreateMemberResult createMember(CreateMemberBody body)
     */
    @Deprecated
    @POST
    @Path("create-member")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    CreateMemberResult createMember(@BeanParam CreateMemberParam param, CreateMemberBody body);

    default CreateMemberResult createMember(CreateMemberBody body) {
        return createMember(new CreateMemberParam(), body);
    }

    /**
     * 会员更新 Method: POST Endpoint: /v4/update-member 请使用UpdateMemberResult
     * updateMember(UpdateMemberBody body)
     */
    @Deprecated
    @POST
    @Path("update-member")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    UpdateMemberResult updateMember(@BeanParam CreateMemberParam param, UpdateMemberBody body);

    default UpdateMemberResult updateMember(UpdateMemberBody body) {
        return updateMember(new CreateMemberParam(), body);
    }
    
    /**
     * 会员标签<br/>
     * Method: POST<br/>
     * Endpoint: /v4/query-member-tags<br/>
     * 请使用QueryMemberTagsResult queryMemberTags(QueryMemberTagsBody body)
     */
    // @Deprecated
    @POST
    @Path("query-member-tags")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    QueryMemberTagsResult queryMemberTags(@BeanParam CreateMemberParam param, QueryMemberTagsBody body);

    default QueryMemberTagsResult queryMemberTags(QueryMemberTagsBody body) {
        return queryMemberTags(new CreateMemberParam(), body);
    }
    
    /**
     * 积分变更接口<br/>
     * Method: POST<br/>
     * Endpoint: /v4/adjust-points<br/>
     * 请使用QueryMemberTagsResult queryMemberTags(QueryMemberTagsBody body)
     */
    // @Deprecated
    @POST
    @Path("adjust-points")
    @Consumes(MediaType.APPLICATION_JSON) 
    @Produces(MediaType.APPLICATION_JSON)
    AdjustPointResult adjustPoints(@BeanParam CreateMemberParam param, AdjustPointBody body);

    default AdjustPointResult adjustPoints(AdjustPointBody body) {
        return adjustPoints(new CreateMemberParam(), body);
    }

    /**
     * 第三方系统在修改会员手机号码时，可调用此接口在小优会平台修改相应的会员手机号码信息<br/>
     * Method: POST<br/>
     * Endpoint: /v4/update-member-cell-phone<br/>
     * 请使用UpdateMemberPhoneBody update-member-cell-phone(UpdateMemberPhoneBody
     * body)
     */
    // @Deprecated
    @POST
    @Path("update-member-cell-phone")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    UpdateMemberPhoneResult updateMemberPhone(@BeanParam CreateMemberParam param, UpdateMemberPhoneBody body);

    default UpdateMemberPhoneResult updateMemberPhone(UpdateMemberPhoneBody body) {
        return updateMemberPhone(new CreateMemberParam(), body);
    }

    /**
     * 会员商品推荐<br/>
     * 第三方系统可调用此接口，查询针对此会员推荐的商品列表。
     * @return
     * @author AmyHabor
     */
    @POST
    @Path("query-member-recommend-item")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    MemberRecommendResult queryMemberRecommend(@BeanParam CreateMemberParam param, MemberRecommendBody body);

    default MemberRecommendResult queryMemberRecommend(MemberRecommendBody body) {
        return queryMemberRecommend(new CreateMemberParam(), body);
    }
}
