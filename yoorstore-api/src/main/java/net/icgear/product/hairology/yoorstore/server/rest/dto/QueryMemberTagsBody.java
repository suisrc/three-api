package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * {
 *  "wxMpId":"gh_d3ee0757cy75",
 *  "weChatOpenId": "o8hR6s8gCzH1-hMlsF449Lk75XNs",
 *  "mobile": "18621809769", 
 *  "vipNo": "33031188", }
 * 
 */
public class QueryMemberTagsBody {

    private String wxMpId;

    private String weChatOpenId;

    private String mobile;

    private String vipNo;

    /**
     * 获取
     * @return the wxMpId
     */
    public String getWxMpId() {
        return wxMpId;
    }

    /**
     * 设定
     * @param wxMpId the wxMpId to set
     */
    
    public void setWxMpId(String wxMpId) {
        this.wxMpId = wxMpId;
    }

    /**
     * 获取
     * @return the weChatOpenId
     */
    public String getWeChatOpenId() {
        return weChatOpenId;
    }

    /**
     * 设定
     * @param weChatOpenId the weChatOpenId to set
     */
    
    public void setWeChatOpenId(String weChatOpenId) {
        this.weChatOpenId = weChatOpenId;
    }

    /**
     * 获取
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * 设定
     * @param mobile the mobile to set
     */
    
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取
     * @return the vipNo
     */
    public String getVipNo() {
        return vipNo;
    }

    /**
     * 设定
     * @param vipNo the vipNo to set
     */
    
    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

}
