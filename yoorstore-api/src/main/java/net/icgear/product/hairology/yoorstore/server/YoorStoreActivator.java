package net.icgear.product.hairology.yoorstore.server;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.ws.rs.client.Client;

import com.google.common.collect.Sets;

import net.icgear.product.hairology.yoorstore.server.filter.BasisAuth2ClientRequestFilter;
import net.icgear.product.hairology.yoorstore.server.rest.api.TestRest;

/**
 * 程序入口配置抽象
 * 
 * @author Y13
 */
@Named(Consts.NAMED)
@ApplicationScoped
public class YoorStoreActivator extends AbstractActivator {

    /**
     * 公司代码
     */
    protected String code;
    
    public String getCode() {
        return code;
    }

    // ----------------------------------------------------------------ZERO ApiActivator
    /**
     * 构造后被系统调用 进行内容初始化
     */
    @PostConstruct
    @Override
    public void init() {
        code = System.getProperty(Consts.KEY_APP_CODE, "未指定");
        super.doPostConstruct();
    }
    
    /**
     * 构建私有访问线程的数量
     */
    @Override
    protected ExecutorService initExecutorService() {
        String countStr = System.getProperty(Consts.KEY_ACTIVATOR_THREAD_COUNT);
        int count = countStr == null ? 10 : Integer.valueOf(countStr);
        return Executors.newFixedThreadPool(count);
    }

    // ----------------------------------------------------------------ZERO Adapter
    protected Map<String, Supplier<?>> registerProperties() {
        Map<String, Supplier<?>> map = super.registerProperties();
        map.put(Consts.APP_CODE, this::getCode);
        
        return map;
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T> T getAdapter(String key) {
        if (Consts.SIGN_DEFAULT.equals(key)) {
            return (T) Consts.SIGN_DEFAULT;
        }
        return super.getAdapter(key);
    }

    protected Client getTargetClient() {
        Client client = super.getTargetClient();
        // 加入凭证处理
        client.register(new BasisAuth2ClientRequestFilter(this));
        return client;
    }

    @Override
    protected String[] getConfigKeys() {
        return new String[]{
                Consts.KEY_APP_ID, 
                Consts.KEY_APP_SECRET, 
                Consts.KEY_BASE_URL, 
                Consts.KEY_APP_PROXY_HOST, 
                Consts.KEY_APP_PROXY_PORT
            };
    }

    @Override
    protected String[] getPropertyKeys() {
        return new String[]{Consts.APP_ID, Consts.APP_SECRET, Consts.BASE_URL};
    }

    /**
     * 暴露接口
     */
    @Override
    public Set<Class<?>> getClasses() {
        return Sets.newHashSet(
                TestRest.class
            );
    }

}
