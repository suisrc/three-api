package net.icgear.product.hairology.yoorstore.server.filter;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.commons.codec.digest.DigestUtils;

import com.suisrc.jaxrsapi.core.ApiActivator;

import net.icgear.common.util.Reference;
import net.icgear.common.util.ReflectionUtils;
import net.icgear.product.hairology.yoorstore.server.Consts;

/**
 * 凭证不具有BasisAuth全部特点，可以说是一种变种，使用如下拦截器进行访问信息的处理
 * 
 * ## 概要
 * **接口文档版本**: ``V3.0.4``
 * 
 * **API 服务器地址:** ``http(s)://partner.api.yoorstore.com``
 * 
 * **API 版本:** ``v4``
 * 
 * **编码格式:**  ``"Content-Type": "application/json;charset=UTF-8"``
 * 
 * **API 请求路径格式为:** ``[API Host]/[Api Version]/[Endpoint]`` 如：
 * 
 * ``` bash
 * http://partner.api.yoorstore.com/v4/create-member?ts=1517477053493&sign=669dbb422efbd3d6c6a68b4fc4e4107a
 * ```
 * 
 * 请求头
 * 
 * ``` bash
 * X-YS-Company: C1002      # 公司代码
 * X-YS-AppId: ys_r3ee0757ca6k      # 由优思得系统提供的app_id
 * Content-Type: application/json;charset=UTF-8
 * ```
 * 
 * 
 * 所有的请求的内容和回复的body都是JSON格式的数据, 返回数据的格式为:
 * 
 * ``` json
 * {
 *   "errcode": 0,     
 *   "data": {},       
 *   "message": null   
 * }
 * ```
 * 
 * **测试环境地址及用户密码**
 * 
 * ```
 * API服务器地址: http://partner.api.yoorstore.cn
 * 调用的路径格式参考： http://partner.api.yoorstore.cn/v4/create-member
 * ```
 * 
 * ## 全局权限处理
 * 
 * > 基于安全考虑，所有的请求会做权限验证, 相关请求头和参数设置如下:
 * 
 * ### 请求头设置(Request Header)
 * 
 * ``` bash
 * X-YS-Company: [Company Code]     # 公司代码, 如: C1002
 * X-YS-AppId: [App Id]     # 由优思得系统提供的app_id, 如: ys_r3ee0757ca6k
 * Content-Type: application/json;charset=UTF-8
 * ```
 * 
 * ### URL参数(Request Parameter)
 * 请求URL需额外带上时间戳参数``ts``以及验证码``sign``
 * 
 * 如原请求若为： ``http://partner.api.yoorstore.cn/v4/create-member?c=100``, 需对此请求参数做以下修改
 * 
 * ``` bash
 * # 1. 需加上ts参数: ts=[当前时间戳（毫秒)] 如:
 * c=100&ts=1517477053493
 * 
 * # 2. 需加上参数: sign=[md5 hash值]， 该md5 hash值的生成规则如下：
 * # 获取所有请求参数再拼接上&secret=[secret] (secret由优思得提供)，对该字符串进行md5加密
 * # 比如: secret若为123456, 则需md5加密的字符串为
 * c=100&ts=1517477053493&secret=123456
 * # 对上述字符串进行md5加密获得
 * 16869b5602a4f7e60944a55b27f891be
 * # 以上述加密字符串为sign的值，生成新的请求参数
 * c=100&ts=1517477053493&sign=16869b5602a4f7e60944a55b27f891be
 * 
 * # 3. 完整请求参数为: (注意，发送请求参数时只需加上``ts``和``sign``参数，不可添加``secret``参数)
 * http://partner.api.yoorstore.cn/v4/create-member?c=100&ts=1517477053493&sign=16869b5602a4f7e60944a55b27f891be
 * 
 * # 为了方便调试，如果参数里无sign，在测试环境将不对URL访问权限进行验证。但生产环境会强制检查该参数的有效性。
 * ```
 * 
 * @author Y13
 *
 */
public class BasisAuth2ClientRequestFilter implements ClientRequestFilter {
    
    private final String key = "sign=" + Consts.SIGN_DEFAULT;
    
    /**
     * 服务器激活器
     */
    private ApiActivator activator;
    
    public BasisAuth2ClientRequestFilter(ApiActivator activator) {
        this.activator = activator;
    }

    @Override
    public void filter(ClientRequestContext rtx) throws IOException {
        String query = rtx.getUri().getRawQuery();
        if (query != null && query.contains(key)) {
            // 这里就是一个开关的作用，该接口需要进行默认权限赋值
            setHeaders(rtx);
            setSign(rtx);
        }
        System.out.println(rtx.getUri().getRawQuery());
    }

    /**
     * 
     * 默认权限会带上Header内容，如果header内容有，就会直接覆盖
     * 
     * X-YS-Company: [Company Code] # 公司代码, 如: C1002 
     * X-YS-AppId: [App Id] # 由优思得系统提供的app_id, 如:ys_r3ee0757ca6k 
     * Content-Type: application/json;charset=UTF-8
     * 
     * @param rtx
     */
    private void setHeaders(ClientRequestContext rtx) {
        MultivaluedMap<String, Object> headers = rtx.getHeaders();
        headers.putSingle("X-YS-Company", activator.getAdapter(Consts.APP_CODE));
        headers.putSingle("X-YS-AppId", activator.getAdapter(Consts.APP_ID));
        headers.putSingle("Content-Type", "application/json;charset=UTF-8");
    }
    
    /**
     * 计算设置签名
     * @param rtx
     */
    private void setSign(ClientRequestContext rtx) {
        // 移除标识sign
        String query = rtx.getUri().getRawQuery();
        String[] queryAttrs = query.split("&");
        Reference<Long> timeRef = new Reference<>();
        StringBuilder sbir = new StringBuilder();
        Arrays.asList(queryAttrs).stream().sorted().forEach(v -> {
            if (v.equals(key)) {
                // sign 过滤掉
            } else if (v.startsWith("ts=")) {
                // 时间单独取出
                String value = v.substring(3);
                if (!value.isEmpty()) {
                    timeRef.set(Long.valueOf(value));
                }
            } else {
                sbir.append(v).append('&');
            }
        });
        
        // 请求发起的时间
        long time = timeRef.get() != null ? timeRef.get() : System.currentTimeMillis();
        // 服务器密钥
        String secret = activator.getAdapter(Consts.APP_SECRET).toString();
        
        // 增加时间和密钥
        sbir.append("ts=").append(time).append('&');
        // 保留query除sign内容
        String newQuery = sbir.toString();
        
        sbir.append("secret=").append(secret);
        // 计算签名的内容
        String sign = DigestUtils.md5Hex(sbir.toString());
        // 获取新query内容
        newQuery += "sign=" + sign;
        
        // 替换原有的检索条件
        ReflectionUtils.setField(rtx.getUri(), URI.class, "query", newQuery);
        ReflectionUtils.setField(rtx.getUri(), URI.class, "string", null);
    }

}
