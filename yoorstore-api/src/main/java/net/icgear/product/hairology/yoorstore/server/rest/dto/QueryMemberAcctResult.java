package net.icgear.product.hairology.yoorstore.server.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryMemberAcctResult extends YoorstoreErrMsg {

    private QueryMemberAcctData data;

    /**
     * 获取
     * @return the data
     */
    public QueryMemberAcctData getData() {
        return data;
    }

    /**
     * 设定
     * @param data the data to set
     */
    public void setData(QueryMemberAcctData data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "QueryMemberResult [data=" + data + ", getErrcode()=" + getErrcode() + ", getMessage()=" + getMessage()
                + ", getErrmsg()=" + getErrmsg() + "]";
    }
    
}
