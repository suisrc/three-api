package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * { "vipNo": "201799827", "actionType": 11, "value": 100, "serialNum":
 * "123456789abcdefghi", "remark": "消费赠送", "actionTime": "2017-12-08 14:20:59" }
 * 
 */
public class AdjustPointBody {

    private String vipNo;

    private String actionType;

    private String value;

    private String serialNum;
    
    private String remark;
    
    private String actionTime;

    /**
     * 获取
     * @return the vipNo
     */
    public String getVipNo() {
        return vipNo;
    }

    /**
     * 设定
     * @param vipNo the vipNo to set
     */
    
    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

    /**
     * 获取
     * @return the actionType
     */
    public String getActionType() {
        return actionType;
    }

    /**
     * 设定
     * @param actionType the actionType to set
     */
    
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    /**
     * 获取
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * 设定
     * @param value the value to set
     */
    
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * 获取
     * @return the serialNum
     */
    public String getSerialNum() {
        return serialNum;
    }

    /**
     * 设定
     * @param serialNum the serialNum to set
     */
    
    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    /**
     * 获取
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 设定
     * @param remark the remark to set
     */
    
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取
     * @return the actionTime
     */
    public String getActionTime() {
        return actionTime;
    }

    /**
     * 设定
     * @param actionTime the actionTime to set
     */
    
    public void setActionTime(String actionTime) {
        this.actionTime = actionTime;
    }

}
