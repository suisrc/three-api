package net.icgear.product.hairology.yoorstore.server.rest.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * &nbsp;&nbsp;{<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"vipNo": "201799827",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"memberId": "892586942791700480"<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"outletCode": "44254",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"fullName": "王小二",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"nickName": "StarWang",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"subSrcType":"15",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"referrer": "张倩",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"avatar": "http://xxxxxxx.com/avatar/w-logo.jpg",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"gender": 1,<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"wxMpId":"gh_d3ee0757ca65",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"weChatOpenId":"o8hR6s8gCzH1-hMlsF449Lk75XNs",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"regDate": "2017-07-01 13:00:24",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mobile": "13801010202",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"birthday": "1998-01-01",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"occupation": "it",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"address":"广州市黄埔大道中309号",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"email": "xiaoer.wang@yoorstore.cn",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"grade": 3,<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"points": 1050,<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"balance": 20.00<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"isSafety": 1<br/>
 * &nbsp;&nbsp;}
 * @author AmyHabor
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryMemberData {

    private String vipNo;

    private String memberId;

    private String outletCode;

    private String fullName;

    private String nickName;

    private String subSrcType;

    private String referrer;

    private String avatar;

    private Integer gender;

    private String wxMpId;

    private String weChatOpendId;

    private String regDate;

    private String mobile;
    
    private String areaCode;

    private String birthday;

    private String occupation;

    private String address;

    private String email;

    private Integer grade;

    private BigDecimal balance;

    public QueryMemberData() {}

    public String getVipNo() {
        return vipNo;
    }

    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOutletCode() {
        return outletCode;
    }

    public void setOutletCode(String outletCode) {
        this.outletCode = outletCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSubSrcType() {
        return subSrcType;
    }

    public void setSubSrcType(String subSrcType) {
        this.subSrcType = subSrcType;
    }

    public String getReferrer() {
        return referrer;
    }

    public void setReferrer(String referrer) {
        this.referrer = referrer;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getWxMpId() {
        return wxMpId;
    }

    public void setWxMpId(String wxMpId) {
        this.wxMpId = wxMpId;
    }

    public String getWeChatOpendId() {
        return weChatOpendId;
    }

    public void setWeChatOpendId(String weChatOpendId) {
        this.weChatOpendId = weChatOpendId;
    }

    public String getRegDate() {
        return regDate;
    }

    public void setRegDate(String regDate) {
        this.regDate = regDate;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取
     * @return the areaCode
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 设定
     * @param areaCode the areaCode to set
     */
    
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "QueryMemberData [vipNo=" + vipNo + ", memberId=" + memberId + ", outletCode=" + outletCode
                + ", fullName=" + fullName + ", nickName=" + nickName + ", subSrcType=" + subSrcType + ", referrer="
                + referrer + ", avatar=" + avatar + ", gender=" + gender + ", wxMpId=" + wxMpId + ", weChatOpendId="
                + weChatOpendId + ", regDate=" + regDate + ", mobile=" + mobile + ", areaCode=" + areaCode
                + ", birthday=" + birthday + ", occupation=" + occupation + ", address=" + address + ", email=" + email
                + ", grade=" + grade + ", balance=" + balance + "]";
    }
   
}