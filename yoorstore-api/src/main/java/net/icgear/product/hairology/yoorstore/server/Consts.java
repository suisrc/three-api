package net.icgear.product.hairology.yoorstore.server;

/**
 * 常量
 * @author Y13
 *
 */
public interface Consts {
    
    /**
     * 启动器标识
     */
    final String NAMED = "YoorStoreActivator";
    
    /**
     * 应用主键
     */
    final String APP_ID = "APP-ID";
    
    /**
     * 应用密钥
     */
    final String APP_SECRET = "APP-SECRET";
    
    /**
     * 应用编码
     */
    final String APP_CODE = "APP-CODE";

    /**
     * 路径基址
     */
    final String BASE_URL = "BASE-URL";
    
    /**
     * 数据签名，用于验证访问的权限
     */
    final String SIGN_DEFAULT = "SIGN-DEFAULT";

    /**
     * 应用ID主键
     */
    final String KEY_APP_ID = "net.icgear.product.yoorstore.app-id";

    /**
     * 应用密钥
     */
    final String KEY_APP_SECRET = "net.icgear.product.yoorstore.app-secret";

    
    /**
     * 共享服务器地址
     */
    final String KEY_BASE_URL = "net.icgear.product.yoorstore.base-url";

    /**
     * 应用的节点编码
     */
    final String KEY_APP_CODE = "net.icgear.product.yoorstore.code";

    /**
     * 应用使用代理访问主机地址
     */
    final String KEY_APP_PROXY_HOST = "net.icgear.product.yoorstore.proxy.host";

    /**
     * 应用使用代理访问主机端口
     */
    final String KEY_APP_PROXY_PORT = "net.icgear.product.yoorstore.proxy.port";

    /**
     * 共享服务器最大连接线程数量
     */
    final String KEY_ACTIVATOR_THREAD_COUNT = "net.icgear.product.yoorstore.thread-count";



}
