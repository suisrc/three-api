package net.icgear.product.hairology.yoorstore.server.rest.api;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberParam;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberResult;

/**
 * 测试接口
 * 
 * 会员 APIs
 * 
 * @author Y13
 *  
 */
@RemoteApi
public interface TestRest {
    
    /**
     * 会员创建
     * 
     * Method: POST
     * Endpoint: /v4/create-member
     * 
     * 请使用CreateMemberResult createMember(CreateMemberBody body)
     */
    @Deprecated
    @POST
    @Path("create-member")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    CreateMemberResult createMember(@BeanParam CreateMemberParam param, CreateMemberBody body);
    
    default CreateMemberResult createMember(CreateMemberBody body) {
        return createMember(new CreateMemberParam(), body);
    }

}
