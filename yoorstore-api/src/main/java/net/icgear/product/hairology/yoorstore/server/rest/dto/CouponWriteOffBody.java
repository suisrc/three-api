package net.icgear.product.hairology.yoorstore.server.rest.dto;

public class CouponWriteOffBody {

    /** 会员crm编码 */
    private String vipNo;

    /** 门店统一编码 */
    private String outletCode;

    /** 卡券编码 */
    private String couponId;

    /**
     * 获取会员crm编码
     * @return the vipNo
     */
    public String getVipNo() {
        return vipNo;
    }

    /**
     * 设定会员crm编码
     * @param vipNo the vipNo to set
     */
    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

    /**
     * 获取门店统一编码
     * @return the outletCode
     */
    public String getOutletCode() {
        return outletCode;
    }

    /**
     * 设定门店统一编码
     * @param outletCode the outletCode to set
     */
    public void setOutletCode(String outletCode) {
        this.outletCode = outletCode;
    }

    /**
     * 获取卡券编码
     * @return the couponId
     */
    public String getCouponId() {
        return couponId;
    }

    /**
     * 设定卡券编码
     * @param couponId the couponId to set
     */
    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CouponWriteOffBody [vipNo=" + vipNo + ", outletCode=" + outletCode + ", couponId=" + couponId + "]";
    }
}
