package net.icgear.product.hairology.yoorstore.server.rest.dto;

import java.util.List;

public class MemberRecommendResult extends YoorstoreErrMsg {

    private List<MemberRecommendData> data;

    public List<MemberRecommendData> getData() {
        return data;
    }

    public void setData(List<MemberRecommendData> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MemberRecommendResult [data=" + data + ", getErrcode()=" + getErrcode() + ", getMessage()=" + getMessage()
                + ", getErrmsg()=" + getErrmsg() + "]";
    }
}
