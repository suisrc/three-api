package net.icgear.product.hairology.yoorstore.runtime;

import com.suisrc.jaxrsapi.core.factory.NSCF;

import net.icgear.product.hairology.yoorstore.server.YoorStoreActivator;

/**
 * 生成接口的实现内容
 * 
 * 由于发现接口部署过程中动态生成对于注入有问题，所以这里构建接口的实现内容
 * 
 * 基于jarsapi框架可以自动生成接口的调用实现
 * 
 * @author Y13
 *
 */
@Deprecated
class AutoFacotry {
    
    /**
     * 构建调用代码
     * @param args
     */
    @SuppressWarnings("unchecked")
    @Deprecated
    public static void main(String[] args) {
        String path = ClassLoader.getSystemResource("").getPath();
        path = path.substring(1, path.length() - "target/classes/".length()) + "src/main/java";
        NSCF.buildFile(path, YoorStoreActivator.class);
        System.out.println("build over!");
    }

}
