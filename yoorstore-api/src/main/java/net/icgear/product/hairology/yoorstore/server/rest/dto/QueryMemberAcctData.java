package net.icgear.product.hairology.yoorstore.server.rest.dto;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import net.icgear.product.hairology.db.entityDto.GoodsDiscountInfo;
import net.icgear.product.hairology.db.entityDto.GoodsDto;

/**
 * "couponId": "1002869427917105", "couponName": "新⼈福利券", "couponType": 2,
 * "promotionCode":"TP1001" "cumulative": 0, "amount": 100, "discount": 95,
 * "amtType": 1, "effectiveDate","2017-09-06 14:30:30", "expireDate","2017-09-09
 * 23:59:59", "couponDesc":"每满300减10元", "status": 4
 * 
 * @author yyc
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryMemberAcctData extends GoodsDiscountInfo {

	private String couponId;

	private String couponName;

	private String couponType;

	private String promotionCode;

	// 方案明细id
	private Integer planDetailId;

	private String cumulative;

	private String amount;

	private String discount;

	private String amtType;

	private String effectiveDate;

	private String expireDate;

	private String couponDesc;

	private String status;

	private Boolean flag = false;

	// 显示还是不显示
	private Boolean display = false;
	// true 是自动
	private Boolean isZd = false;

	// 9是整单 单项10
	private Integer type;

	private BigDecimal discountAmout;

	private List<Long> goodsOnlyLst;

	private Long goodsOnly;

	private Long only;
	
	/**
	 * true是赠送
	 */
	private Boolean giftFlag;

	/**
	 * 选择那张券的标识
	 */
	private Boolean chooseFlag = false;

	/**
	 * 商品的lst
	 */
	private List<GoodsDto> GoodsDtoLst;

	/**
	 * 优惠
	 */
	private CouponAllDto counponsDto;

	/**
	 * 商品
	 */
	private GoodsDto goodsDto;

	public Long getGoodsOnly() {
		return goodsOnly;
	}

	public void setGoodsOnly(Long goodsOnly) {
		this.goodsOnly = goodsOnly;
	}

	/**
	 * 获取
	 * 
	 * @return the couponId
	 */
	public String getCouponId() {
		return couponId;
	}

	/**
	 * 设定
	 * 
	 * @param couponId
	 *            the couponId to set
	 */
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	/**
	 * 获取
	 * 
	 * @return the couponName
	 */
	public String getCouponName() {
		return couponName;
	}

	/**
	 * 设定
	 * 
	 * @param couponName
	 *            the couponName to set
	 */
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	/**
	 * 获取
	 * 
	 * @return the couponType
	 */
	public String getCouponType() {
		return couponType;
	}

	/**
	 * 设定
	 * 
	 * @param couponType
	 *            the couponType to set
	 */
	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}

	/**
	 * 获取
	 * 
	 * @return the promotionCode
	 */
	public String getPromotionCode() {
		return promotionCode;
	}

	/**
	 * 设定
	 * 
	 * @param promotionCode
	 *            the promotionCode to set
	 */
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	/**
	 * 获取
	 * 
	 * @return the cumulative
	 */
	public String getCumulative() {
		return cumulative;
	}

	/**
	 * 设定
	 * 
	 * @param cumulative
	 *            the cumulative to set
	 */
	public void setCumulative(String cumulative) {
		this.cumulative = cumulative;
	}

	/**
	 * 获取
	 * 
	 * @return the amount
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * 设定
	 * 
	 * @param amount
	 *            the amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * 获取
	 * 
	 * @return the discount
	 */
	public String getDiscount() {
		return discount;
	}

	/**
	 * 设定
	 * 
	 * @param discount
	 *            the discount to set
	 */
	public void setDiscount(String discount) {
		this.discount = discount;
	}

	/**
	 * 获取
	 * 
	 * @return the amtType
	 */
	public String getAmtType() {
		return amtType;
	}

	/**
	 * 设定
	 * 
	 * @param amtType
	 *            the amtType to set
	 */
	public void setAmtType(String amtType) {
		this.amtType = amtType;
	}

	/**
	 * 获取
	 * 
	 * @return the effectiveDate
	 */
	public String getEffectiveDate() {
		return effectiveDate;
	}

	/**
	 * 设定
	 * 
	 * @param effectiveDate
	 *            the effectiveDate to set
	 */
	public void setEffectiveDate(String effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	/**
	 * 获取
	 * 
	 * @return the expireDate
	 */
	public String getExpireDate() {
		return expireDate;
	}

	/**
	 * 设定
	 * 
	 * @param expireDate
	 *            the expireDate to set
	 */
	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	/**
	 * 获取
	 * 
	 * @return the couponDesc
	 */
	public String getCouponDesc() {
		return couponDesc;
	}

	/**
	 * 设定
	 * 
	 * @param couponDesc
	 *            the couponDesc to set
	 */
	public void setCouponDesc(String couponDesc) {
		this.couponDesc = couponDesc;
	}

	/**
	 * 获取
	 * 
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * 设定
	 * 
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	public List<Long> getGoodsOnlyLst() {
		return goodsOnlyLst;
	}

	public void setGoodsOnlyLst(List<Long> goodsOnlyLst) {
		this.goodsOnlyLst = goodsOnlyLst;
	}

	/**
	 * 获取
	 * 
	 * @return the flag
	 */
	public Boolean getFlag() {
		return flag;
	}

	/**
	 * 设定
	 * 
	 * @param flag
	 *            the flag to set
	 */
	public void setFlag(Boolean flag) {
		this.flag = flag;
	}

	/**
	 * 获取
	 * 
	 * @return the isZd
	 */
	public Boolean getIsZd() {
		return isZd;
	}

	/**
	 * 设定
	 * 
	 * @param isZd
	 *            the isZd to set
	 */
	public void setIsZd(Boolean isZd) {
		this.isZd = isZd;
	}

	/**
	 * 获取
	 * 
	 * @return the type
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * 设定
	 * 
	 * @param type
	 *            the type to set
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * 获取
	 * 
	 * @return the discountAmout
	 */
	public BigDecimal getDiscountAmout() {
		return discountAmout;
	}

	/**
	 * 设定
	 * 
	 * @param discountAmout
	 *            the discountAmout to set
	 */
	public void setDiscountAmout(BigDecimal discountAmout) {
		this.discountAmout = discountAmout;
	}

	/**
	 * 获取
	 * 
	 * @return the display
	 */
	public Boolean getDisplay() {
		return display;
	}

	/**
	 * 设定
	 * 
	 * @param display
	 *            the display to set
	 */
	public void setDisplay(Boolean display) {
		this.display = display;
	}

	public List<GoodsDto> getGoodsDtoLst() {
		return GoodsDtoLst;
	}

	public void setGoodsDtoLst(List<GoodsDto> goodsDtoLst) {
		GoodsDtoLst = goodsDtoLst;
	}

	public Integer getPlanDetailId() {
		return planDetailId;
	}

	public void setPlanDetailId(Integer planDetailId) {
		this.planDetailId = planDetailId;
	}

	public GoodsDto getGoodsDto() {
		return goodsDto;
	}

	public void setGoodsDto(GoodsDto goodsDto) {
		this.goodsDto = goodsDto;
	}

	public Boolean getChooseFlag() {
		return chooseFlag;
	}

	public void setChooseFlag(Boolean chooseFlag) {
		this.chooseFlag = chooseFlag;
	}

	// public List<QueryMemberAcctData> getCouponAllDto() {
	// return couponAllDto;
	// }
	//
	// public void setCouponAllDto(List<QueryMemberAcctData> couponAllDto) {
	// this.couponAllDto = couponAllDto;
	// }

	public CouponAllDto getCounponsDto() {
		return counponsDto;
	}

	public void setCounponsDto(CouponAllDto counponsDto) {
		this.counponsDto = counponsDto;
	}

	public Long getOnly() {
		return only;
	}

	public void setOnly(Long only) {
		this.only = only;
	}
	
	public Boolean getGiftFlag() {
		return giftFlag;
	}

	public void setGiftFlag(Boolean giftFlag) {
		this.giftFlag = giftFlag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "QueryMemberAcctData [couponId=" + couponId + ", couponName=" + couponName + ", couponType=" + couponType
				+ ", promotionCode=" + promotionCode + ", cumulative=" + cumulative + ", amount=" + amount
				+ ", discount=" + discount + ", amtType=" + amtType + ", effectiveDate=" + effectiveDate
				+ ", expireDate=" + expireDate + ", couponDesc=" + couponDesc + ", status=" + status + "]";
	}

}
