package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * 
    "vipNo": "201799827",
    "memberId": "892586942791700480"
    
 * @author Y13
 *
 */
public class CreateMemberData {
    
    private String vip_no;
    
    private String ref_id;
  
    /**
     * 获取
     * @return the vip_no
     */
    public String getVip_no() {
        return vip_no;
    }

    /**
     * 设定
     * @param vip_no the vip_no to set
     */
    
    public void setVip_no(String vip_no) {
        this.vip_no = vip_no;
    }

    /**
     * 获取
     * @return the ref_id
     */
    public String getRef_id() {
        return ref_id;
    }

    /**
     * 设定
     * @param ref_id the ref_id to set
     */
    
    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CreateMemberData [vip_no=" + vip_no + ", ref_id=" + ref_id + "]";
    }
    
}
