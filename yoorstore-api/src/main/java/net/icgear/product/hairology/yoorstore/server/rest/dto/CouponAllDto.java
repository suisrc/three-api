package net.icgear.product.hairology.yoorstore.server.rest.dto;

import java.util.List;

/**
 * "couponId": "1002869427917105", "couponName": "新⼈福利券", "couponType": 2,
 * "promotionCode":"TP1001" "cumulative": 0, "amount": 100, "discount": 95,
 * "amtType": 1, "effectiveDate","2017-09-06 14:30:30", "expireDate","2017-09-09
 * 23:59:59", "couponDesc":"每满300减10元", "status": 4
 * 
 * @author yyc
 *
 */
public class CouponAllDto {

	/**
	 * 整单优惠dtolst
	 */
	private List<QueryMemberAcctData> couponDtoLst;

	// 优惠方案码
	private String promotionCode;

	// 优惠券conponId
	private String conponId;

	// 优惠券名称
	private String couponName;

	// 唯一
	private Long only;

	public List<QueryMemberAcctData> getCouponDtoLst() {
		return couponDtoLst;
	}

	public void setCouponDtoLst(List<QueryMemberAcctData> couponDtoLst) {
		this.couponDtoLst = couponDtoLst;
	}

	public String getPromotionCode() {
		return promotionCode;
	}

	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public String getConponId() {
		return conponId;
	}

	public void setConponId(String conponId) {
		this.conponId = conponId;
	}

	public Long getOnly() {
		return only;
	}

	public void setOnly(Long only) {
		this.only = only;
	}

}
