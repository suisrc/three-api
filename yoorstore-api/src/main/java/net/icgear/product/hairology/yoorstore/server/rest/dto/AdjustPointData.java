package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * 
 * @author 
 *
 */
public class AdjustPointData {
    
    private String points;
    
    private String serialNum;

    /**
     * 获取
     * @return the points
     */
    public String getPoints() {
        return points;
    }

    /**
     * 设定
     * @param points the points to set
     */
    
    public void setPoints(String points) {
        this.points = points;
    }

    /**
     * 获取
     * @return the serialNum
     */
    public String getSerialNum() {
        return serialNum;
    }

    /**
     * 设定
     * @param serialNum the serialNum to set
     */
    
    public void setSerialNum(String serialNum) {
        this.serialNum = serialNum;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AdjustPointData [points=" + points + ", serialNum=" + serialNum + "]";
    }
  
}
