package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * {
 *   "typeName": '深层清洁-热能活化',
 *   "imgUrl": 'http://uat.oss.yoorstore.com/item/XM006-4-1.png?x-oss-process=image/format,jpeg/quality,Q_80&v=1527503316571',
 *   "itemName": '香薰SPA',
 *   "itemCode": 'XM022',
 * }，
 * @author AmyHabor
 */
public class MemberRecommendData {

    private String typeName;

    private String imgUrl;

    private String itemName;

    private String itemCode;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    @Override
    public String toString() {
        return "MemberRecommendData [typeName=" + typeName + ", imgUrl=" + imgUrl + ", itemName=" + itemName + ", itemCode="
                + itemCode + "]";
    }
}
