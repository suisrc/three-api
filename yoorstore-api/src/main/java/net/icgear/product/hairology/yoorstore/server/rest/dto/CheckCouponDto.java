package net.icgear.product.hairology.yoorstore.server.rest.dto;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import net.icgear.product.hairology.db.entityDto.CouponAllDto;
import net.icgear.product.hairology.db.entityDto.CouponDto;
import net.icgear.product.hairology.db.entityDto.GoodsDiscountInfo;
import net.icgear.product.hairology.db.entityDto.GoodsDto;

/**
 * "couponId": "1002869427917105", "couponName": "新人福利券", "couponType": 2, "promotionCode":"TP1001"
 * "cumulative": 0, "amount": 100, "discount": 95, "amtType": 1, "effectiveDate","2017-09-06
 * 14:30:30", "expireDate","2017-09-09 23:59:59", "couponDesc":"每满300减10元", "status": 4
 * 
 * @author yyc
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CheckCouponDto {
	
//    private String couponId;
//
//    private String couponName;
//
//    private String couponType;
//
//    private String promotionCode;
//
//    private String status;
//
//    private Boolean flag = false;
//    
//    //显示还是不显示
//    private Boolean display = false;
//    //true 是自动
//    private Boolean isZd;
//
//    // 9是整单 单项10
//    private Integer type;
    
    /**
     * 商品的dtoLst
     */
    private List<GoodsDto> goodsDtoLst;
    
    /**
     * 整单优惠
     */
    private CouponAllDto couponAllDto;
    
	public List<GoodsDto> getGoodsDtoLst() {
		return goodsDtoLst;
	}

	public void setGoodsDtoLst(List<GoodsDto> goodsDtoLst) {
		this.goodsDtoLst = goodsDtoLst;
	}

	public CouponAllDto getCouponAllDto() {
		return couponAllDto;
	}

	public void setCouponAllDto(CouponAllDto couponAllDto) {
		this.couponAllDto = couponAllDto;
	}


	

}
