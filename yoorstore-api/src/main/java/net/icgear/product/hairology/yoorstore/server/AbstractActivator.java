package net.icgear.product.hairology.yoorstore.server;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.function.Supplier;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.jboss.resteasy.client.jaxrs.ClientBuilderFactory;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.spi.ResteasyProviderFactory;

import com.suisrc.jaxrsapi.core.ApiActivator;

/**
 * 程序入口配置抽象
 * 
 * @author Y13
 */
public abstract class AbstractActivator implements ApiActivator {
    
    /**
     * app id
     */
    protected String appId;

    /**
     * app secret
     */
    protected String appSecret;

    /**
     * 获取基础路径地址
     */
    protected String baseUrl;
    
    /**
     * 注册的属性
     */
    protected Map<String, Supplier<?>> properties;

    /**
     * 提供器工厂
     */
    protected ResteasyProviderFactory providerFactory = null;

    /**
     * 异步多线程访问缓冲
     */
    protected ExecutorService executor = null;

    /**
     * 访问的客户端
     */
    protected Client client;

    /**
     * 访问默认代理主机
     */
    protected String proxyHost = null;
    /**
     * 访问默认代理端口
     */
    protected int proxyPort = -1;
    
    /**
     * 构造方法
     */
    protected AbstractActivator() {
        /*
         * 写在这里是方便后面初始化方法进行替换,更新
         */
        initInternalProperty();
    }

    // ----------------------------------------------------------------ZERO ApiActivator
    
    protected void initInternalProperty() {
    }
    
    /**
     * 构造后被系统调用 进行内容初始化
     */
    protected void doPostConstruct() {
        String[] keys = getConfigKeys();
        appId = System.getProperty(keys[0]);
        appSecret = System.getProperty(keys[1]);
        baseUrl = System.getProperty(keys[2]);
        // 代理
        proxyHost = System.getProperty(keys[3]);
        Object obj = System.getProperties().get(keys[4]);
        if (obj != null) {
            if (obj.getClass() == int.class || obj.getClass() == Integer.class) {
                proxyPort = (int)obj;
            } else if (obj.getClass() == String.class) {
                proxyPort = Integer.valueOf(obj.toString());
            }
        }
        // 处理的线程池
        executor = initExecutorService();
        // 构建客户端创建器
        client = getTargetClient();
        // 注册属性
        registerProperties();
    }
    
    // ----------------------------------------------------------------ZERO 注册接口属性
    
    protected Map<String, Supplier<?>> registerProperties() {
        properties = new HashMap<>();
        String[] pks = getPropertyKeys();
        properties.put(pks[0], this::getAppId);
        properties.put(pks[1], this::getAppSecret);
        properties.put(pks[2], this::getBaseUrl);
        
        return properties;
    }
    
    /**
     * 获取key索引(配置)
     * 
     * 分别代表"app-id", "app-secret", "base-url"
     * @return
     */
    protected abstract String[] getConfigKeys();
    /**
     * 获取key索引(属性)
     * 分别代表"app-id", "app-secret", "base-url"
     * @return
     */
    protected abstract String[] getPropertyKeys();
    
    /**
     * 构建访问线程池, 默认不使用私有线程池
     * executor = Executors.newFixedThreadPool(10);
     */
    protected ExecutorService initExecutorService() {
        return null;
    }

    /**
     * 初始化远程访问的客户端
     */
    protected Client getTargetClient() {
        ClientBuilder clientBuilder = createClientBuilder();// 配置网络通信内容
        if (clientBuilder instanceof ResteasyClientBuilder) {
            ResteasyClientBuilder rcBuilder = (ResteasyClientBuilder) clientBuilder;
            if (proxyHost != null && proxyPort > 0) {
                // 代理服务器访问
                rcBuilder.defaultProxy(proxyHost, proxyPort);
            }
            if (executor != null) {
                // 配置线程池，默认使用线程池为固定大小最大10个线程
                rcBuilder.asyncExecutor(executor); 
            }
            if (providerFactory != null) {
                // 提供格式化工厂
                rcBuilder.providerFactory(providerFactory);
            }
            // 修正http client，使其达到多线程安全的目的
            ClientBuilderFactory.initHttpEngineThreadSaft(rcBuilder);
        }
        Client client = clientBuilder.build();
        return client;
    }

    /**
     * 获取一个Client Builder
     */
    protected ClientBuilder createClientBuilder() {
        // return ClientBuilder.newBuilder();
        return ClientBuilderFactory.newBuilder();
    }

    // ----------------------------------------------------------------ZERO Adapter
    /**
     * 获取系统中常用的数据配置 返回系统中常量数据
     */
    @SuppressWarnings("unchecked")
    public <T> T getAdapter(String key) {
        Supplier<?> sp = properties.get(key);
        if (sp != null) {
            return (T)(sp.get());
        }
        return ApiActivator.super.getAdapter(key);
    }

    /**
     * 获取系统的对象
     */
    @SuppressWarnings("unchecked")
    public <T> T getAdapter(Class<T> type) {
        if (type == WebTarget.class) {
            return (T) client.target(getBaseUrl());
        } else if (type == Client.class) {
            return (T) client;
        } else if (type == ResteasyProviderFactory.class) {
            return (T) providerFactory;
        }
        return null;
    }

    /**
     * 主要是为了防止不支持javaee7.0标准的反向内容注入
     */
    public <T> void setAdapter(Class<T> type, T value) {
        if (type == ResteasyProviderFactory.class) {
            providerFactory = (ResteasyProviderFactory) value;
        }
    }

    //--------------------------------------------------ZERO WxConfig
    
    public String getBaseUrl() {
        return baseUrl;
    }

    public String getAppId() {
        return appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

}
