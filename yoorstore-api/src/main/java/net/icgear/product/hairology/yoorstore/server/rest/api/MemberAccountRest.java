package net.icgear.product.hairology.yoorstore.server.rest.api;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberParam;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberAccountBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.QueryMemberAcctResult;

@RemoteApi
@ApplicationScoped
public interface MemberAccountRest {

    /**
     * 会员查询<br/>
     * Method: POST<br/>
     * Endpoint: /v4/get-coupon-info<br/>
     * 请使用QueryMemberResult queryMember(QueryMemberBody body)
     */
    @POST
    @Path("get-coupon-info")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    QueryMemberAcctResult queryMemberAccount(@BeanParam CreateMemberParam param, QueryMemberAccountBody body);

    default QueryMemberAcctResult queryMemberAccount(QueryMemberAccountBody body) {
        return queryMemberAccount(new CreateMemberParam(), body);
    }
}
