package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * 
{
  "errcode": 0,
  "data": {
    "vipNo": "201799827",
    "memberId": "892586942791700480"
  },
  "message": null
}
 * @author Y13
 *
 */
public class UpdateMemberResult extends YoorstoreErrMsg {

    private Boolean data;

    public Boolean getData() {
        return data;
    }

    public void setData(Boolean data) {
        this.data = data;
    }

}
