package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * 
 * {
 *    "vipNo": "201799827",
 *    "outletCode": "44254",
 *    "couponId": "1002869427917105""
 * }
 * @author yyc
 *
 */
public class QueryMemberAccountBody {

    private String vipNo;
    
    private String outletCode;
    
    private String couponId;

    /**
     * 获取
     * @return the vipNo
     */
    public String getVipNo() {
        return vipNo;
    }

    /**
     * 设定
     * @param vipNo the vipNo to set
     */
    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

    /**
     * 获取
     * @return the outletCode
     */
    public String getOutletCode() {
        return outletCode;
    }

    /**
     * 设定
     * @param outletCode the outletCode to set
     */
    public void setOutletCode(String outletCode) {
        this.outletCode = outletCode;
    }

    /**
     * 获取
     * @return the couponId
     */
    public String getCouponId() {
        return couponId;
    }

    /**
     * 设定
     * @param couponId the couponId to set
     */
    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }
    
    
}
