package net.icgear.product.hairology.yoorstore.server.rest.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * &nbsp;&nbsp;{<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"vipNo": "201799827",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"memberId": "892586942791700480"<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"outletCode": "44254",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"fullName": "王小二",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"nickName": "StarWang",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"subSrcType":"15",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"referrer": "张倩",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"avatar": "http://xxxxxxx.com/avatar/w-logo.jpg",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"gender": 1,<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"wxMpId":"gh_d3ee0757ca65",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"weChatOpenId":"o8hR6s8gCzH1-hMlsF449Lk75XNs",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"regDate": "2017-07-01 13:00:24",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mobile": "13801010202",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"birthday": "1998-01-01",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"occupation": "it",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"address":"广州市黄埔大道中309号",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"email": "xiaoer.wang@yoorstore.cn",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"grade": 3,<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"points": 1050,<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"balance": 20.00<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"isSafety": 1<br/>
 * &nbsp;&nbsp;}
 * @author AmyHabor
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryMemberTagsData {

    private String vipNo;

    private String memberId;

    private List<QueryMemberTagsInfoData> tagInfo;

    public QueryMemberTagsData() {}

    public String getVipNo() {
        return vipNo;
    }

    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    /**
     * 获取
     * @return the tagInfo
     */
    public List<QueryMemberTagsInfoData> getTagInfo() {
        return tagInfo;
    }

    /**
     * 设定
     * @param tagInfo the tagInfo to set
     */
    
    public void setTagInfo(List<QueryMemberTagsInfoData> tagInfo) {
        this.tagInfo = tagInfo;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "QueryMemberTagsData [vipNo=" + vipNo + ", memberId=" + memberId + ", tagInfo=" + tagInfo + "]";
    }
    
    
}