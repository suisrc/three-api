package net.icgear.product.hairology.yoorstore.server.rest.api;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.suisrc.jaxrsapi.core.annotation.RemoteApi;

import net.icgear.product.hairology.yoorstore.server.rest.dto.CouponWriteOffBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.CreateMemberParam;
import net.icgear.product.hairology.yoorstore.server.rest.dto.PosBody;
import net.icgear.product.hairology.yoorstore.server.rest.dto.YoorstoreErrMsg;

@RemoteApi
@ApplicationScoped
public interface PosUploadRest {

    /**
     * 会员消费明细上传<br/>
     * Method: POST<br/>
     * Endpoint: /v4/member-pos-detail<br/>
     * 请使用YoorstoreErrMsg ccPosData(PosBody body)
     */
    @Deprecated
    @POST
    @Path("member-pos-detail")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    YoorstoreErrMsg ccPosData(@BeanParam CreateMemberParam param, List<PosBody> bodys);

    /**
     * 会员消费明细上传
     */
    default YoorstoreErrMsg ccPosData(List<PosBody> bodys) {
        return ccPosData(new CreateMemberParam(), bodys);
    }

    /**
     * 优惠券核销<br/>
     * Method: POST<br/>
     * Endpoint: /v4/set-coupon-used<br/>
     * 请使用YoorstoreErrMsg ccPosData(PosBody body)
     */
    @Deprecated
    @POST
    @Path("set-coupon-used")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    YoorstoreErrMsg ccWriteOff(@BeanParam CreateMemberParam param, List<CouponWriteOffBody> bodys);

    /**
     * 优惠券核销
     */
    default YoorstoreErrMsg ccWriteOff(List<CouponWriteOffBody> bodys) {
        return ccWriteOff(new CreateMemberParam(), bodys);
    }
}