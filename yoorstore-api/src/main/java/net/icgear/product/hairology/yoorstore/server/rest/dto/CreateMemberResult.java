package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * 
{
  "errcode": 0,
  "data": {
    "vipNo": "201799827",
    "memberId": "892586942791700480"
  },
  "message": null
}
 * @author Y13
 *
 */
public class CreateMemberResult extends YoorstoreErrMsg {
    
    private CreateMemberData data;

    public CreateMemberData getData() {
        return data;
    }

    public void setData(CreateMemberData data) {
        this.data = data;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CreateMemberResult [data=" + data + "]";
    }
    
}
