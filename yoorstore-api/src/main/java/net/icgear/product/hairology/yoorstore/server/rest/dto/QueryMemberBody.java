package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * {
 *    "payCode": "2018145467851234509876901234",
 *    "vipNo": "201799827",
 *    "mobile": "13801010202",
 *    "wxMpId":"gh_d3ee0757cy75",
 *    "weChatOpenId":"o8hR6s8gCzH1-hMlsF449Lk75XNs"
 * }
 * @author AmyHabor
 */
public class QueryMemberBody {

    private String payCode;

    private String vipNo;

    private String mobile;
    
    private String areaCode;

    private String wxMpId;

    private String weChatOpenId;
    
    /**
     * 获取
     * @return the areaCode
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 设定
     * @param areaCode the areaCode to set
     */
    
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getPayCode() {
        return payCode;
    }

    public void setPayCode(String payCode) {
        this.payCode = payCode;
    }

    public String getVipNo() {
        return vipNo;
    }

    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getWxMpId() {
        return wxMpId;
    }

    public void setWxMpId(String wxMpId) {
        this.wxMpId = wxMpId;
    }

    public String getWeChatOpenId() {
        return weChatOpenId;
    }

    public void setWeChatOpenId(String weChatOpenId) {
        this.weChatOpenId = weChatOpenId;
    }
}
