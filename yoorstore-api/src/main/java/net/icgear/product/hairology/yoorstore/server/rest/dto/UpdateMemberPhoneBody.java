package net.icgear.product.hairology.yoorstore.server.rest.dto;

/**
 * {
 * "vipNo": "S100015",
 * "cellPhone": "13800000000"
}
 * @author AmyHabor
 */
public class UpdateMemberPhoneBody {

    private String vipNo;

    private String cellPhone;
    
    private String areaCode;

    public String getVipNo() {
        return vipNo;
    }

    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }
    
    /**
     * 获取
     * @return the areaCode
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 设定
     * @param areaCode the areaCode to set
     */
    
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * 获取
     * @return the cellPhone
     */
    public String getCellPhone() {
        return cellPhone;
    }

    /**
     * 设定
     * @param cellPhone
     *            the cellPhone to set
     */
    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

}
