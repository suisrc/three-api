package net.icgear.product.hairology.yoorstore.server.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * 创建会员时候，使用的body内容
 * 
 * { "vipNo": "201799827", "fullName": "王⼩小⼆二", "gender": 1, "birthday":
 * "1992-08-01", "email": "xiaoer.wang@yoorstore.cn",
 * "weChatOpenId":"o8hR6s8gCzH1-hMlsF449Lk75XNs", "outletCode": "31001" }
 * 
 * @author Y13
 *
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateMemberBody {
    
    private String vipNo;
    
    private String fullName;
    
    private int gender;
    
    private String birthday;
    
    private String email;
    
    private String weChatOpenId;
    
    private String outletCode;
    
    private String address;
    
    private String occupation;
    
//    private String areaCode;

    public String getVipNo() {
        return vipNo;
    }

    public void setVipNo(String vipNo) {
        this.vipNo = vipNo;
    }

    public String getOutletCode() {
        return outletCode;
    }

    public void setOutletCode(String outletCode) {
        this.outletCode = outletCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
   
    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取
     * @return the weChatOpenId
     */
    public String getWeChatOpenId() {
        return weChatOpenId;
    }

    /**
     * 设定
     * @param weChatOpenId the weChatOpenId to set
     */
    
    public void setWeChatOpenId(String weChatOpenId) {
        this.weChatOpenId = weChatOpenId;
    }

    /**
     * 获取
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * 设定
     * @param address the address to set
     */
    
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * 获取
     * @return the occupation
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * 设定
     * @param occupation the occupation to set
     */
    
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

//	/**
//     * 获取
//     * @return the areaCode
//     */
//    public String getAreaCode() {
//        return areaCode;
//    }

//    /**
//     * 设定
//     * @param areaCode the areaCode to set
//     */
//    
//    public void setAreaCode(String areaCode) {
//        this.areaCode = areaCode;
//    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UpdateMemberBody [vipNo=" + vipNo + ", fullName=" + fullName + ", gender=" + gender + ", birthday="
                + birthday + ", email=" + email + ", weChatOpenId=" + weChatOpenId + ", outletCode=" + outletCode
                + ", address=" + address + ", occupation=" + occupation + "]";
    }
    
}
