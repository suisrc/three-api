package net.icgear.product.hairology.yoorstore.server.rest.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * &nbsp;&nbsp;{<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"vipNo": "201799827",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"memberId": "892586942791700480"<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"outletCode": "44254",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"fullName": "王小二",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"nickName": "StarWang",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"subSrcType":"15",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"referrer": "张倩",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"avatar": "http://xxxxxxx.com/avatar/w-logo.jpg",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"gender": 1,<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"wxMpId":"gh_d3ee0757ca65",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"weChatOpenId":"o8hR6s8gCzH1-hMlsF449Lk75XNs",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"regDate": "2017-07-01 13:00:24",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"mobile": "13801010202",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"birthday": "1998-01-01",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"occupation": "it",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"address":"广州市黄埔大道中309号",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"email": "xiaoer.wang@yoorstore.cn",<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"grade": 3,<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"points": 1050,<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"balance": 20.00<br/>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"isSafety": 1<br/>
 * &nbsp;&nbsp;}
 * @author AmyHabor
 */
@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class QueryMemberTagsListData {

    private String tagName;

    private String value;

    /**
     * 获取
     * @return the tagName
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * 设定
     * @param tagName the tagName to set
     */
    
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    /**
     * 获取
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * 设定
     * @param value the value to set
     */
    
    public void setValue(String value) {
        this.value = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "QueryMemberTagsListData [tagName=" + tagName + ", value=" + value + "]";
    }
    
}