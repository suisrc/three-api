package net.icgear.product.att2005.xframe.iface;

import java.util.function.Function;

/**
 * 执行事务代理
 * 
 * @author Y13
 *
 */
public interface ProxyService {

    /**
     * 没有事务支持，无法更新
     * 
     * @param run
     */
    void executeR(Runnable run);

    /**
     * 有事务支持，可以更新
     * 
     * @param run
     */
    void executeRT(Runnable run);

    /**
     * 没有事务更新
     * 
     * @param sup
     * @return
     */
    <T, R> R executeS(T t, Function<T, R> sup);

    /**
     * 有事务支持，可以更新
     * 
     * @param sup
     * @return
     */
    <T, R> R executeST(T t, Function<T, R> sup);

}
