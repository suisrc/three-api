package net.icgear.product.att2005.att.service.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.im.core.persistence.service.AbstractService;
import net.icgear.im.core.persistence.service.RepoHandler;
import net.icgear.im.core.persistence.service.Service;
import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.A_TestBean;
import net.icgear.product.att2005.att.repo.api.A_TestBeanRepository;
import net.icgear.product.att2005.att.rsapi.A_TestBeanRsApi;
import net.icgear.product.att2005.att.service.api.A_TestBeanService;

/**
 * build by tools[0.1-alpha] on Tue Nov 06 13:58:38 CST 2018
 * @author Y13-Tools(auto)
 *
 */
@Service
@ApplicationScoped
public abstract class A_TestBeanServiceImpl extends AbstractService<A_TestBean, Integer> 
        implements A_TestBeanService, SuperService<A_TestBean, Integer>, A_TestBeanRsApi, SuperRsApi<A_TestBean, Integer> {
    
    @Inject
    @RepoHandler
    private A_TestBeanRepository repo;

}
