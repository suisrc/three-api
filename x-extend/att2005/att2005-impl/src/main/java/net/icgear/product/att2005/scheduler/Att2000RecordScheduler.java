package net.icgear.product.att2005.scheduler;

import java.time.LocalTime;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.cdise.api.ContextControl;

import com.suisrc.core.Global;
import com.suisrc.core.scheduler.ScheduleHandler;
import com.suisrc.core.scheduler.Scheduled;
import com.suisrc.core.service.AbstractScheduledService.Schedule;

import net.icgear.product.att2005.att.entity.AttendanceRecord;
import net.icgear.product.att2005.att.service.api.AttendanceRecordService;
import net.icgear.product.att2005.wx.service.api.WeixinService;

/**
 * <p> 定时统计考勤记录和内容 <p> 默认希望每天6点指定发送
 * 
 * <p> 每5分钟执行一次同步操作
 * 
 * @author Y13
 *
 */
//@Scheduled(first=10, delay=300,onStartupKey = "application.schedule.config.valid2")
@Scheduled(delayf = true, onStartupKey = "application.schedule.config.valid2")
@ApplicationScoped
public class Att2000RecordScheduler implements ScheduleHandler {
  
  @Inject
  private ContextControl control;
  @Inject
  private AttendanceRecordService recordService;
  @Inject
  private WeixinService wxService;
  
  private boolean isFrist = true;
  

  /**
   * 执行作业内容
   */
  @Override
  public void run() {
    try {
      Global.getLogger().info("开始执行同步考勤原始数据002");
      control.startContext(RequestScoped.class);
      List<AttendanceRecord> res = recordService.syncAttRecordAuto("系统同步考勤数据同步-定时器002");
      if (res != null) {
        wxService.sendAttendanceRecord(res);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }finally {
      control.stopContext(RequestScoped.class);
      Global.getLogger().info("结束执行同步考勤原始数据002");
    }
  }

  @Override
  public Long getNextDelay(Schedule pre) {
    if (isFrist) {
      // 第一次执行，间隔10s
      isFrist = false;
      return 10L;
    }
    LocalTime now = LocalTime.now();
    int hour = now.getHour();
    if (hour > 7 && hour < 10) {
      // 7点~10点，5分钟检测一次
      return 300L;
    }
    if (hour > 17 && hour < 21) {
      // 6点~9点，5分钟检测一次
      return 300L;
    }
    return 1800L;
  }
}
