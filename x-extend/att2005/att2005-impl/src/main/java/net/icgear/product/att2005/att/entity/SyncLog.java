package net.icgear.product.att2005.att.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import net.icgear.im.core.persistence.IA;

@Entity
@Table(name = "sync_log")
public class SyncLog implements IA<Integer> {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 10)
    private Integer id;
    
    /**
     * 同步开始时间
     */
    @Column(name = "begin_time")
    private Timestamp beginTime;
    
    /**
     * 同步结束时间
     */
    @Column(name = "end_time")
    private Timestamp endTime;
    
    /**
     * 同步数据的条数
     */
    private Integer count;
    
    /**
     * 同步说明
     */
    private String description;

    /**
     * 数据版本号
     */
    @Version
    private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Timestamp getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Timestamp beginTime) {
        this.beginTime = beginTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 内容
     * @return
     */
    public String toString2() {
        StringBuilder sb = new StringBuilder();
        sb.append("同步日志：").append(getDescription()).append(":");
        sb.append("时间：").append(getBeginTime()).append("~").append(getEndTime()).append(";");
        sb.append("数量：").append(getCount());
        return sb.toString();
    }
    
}
