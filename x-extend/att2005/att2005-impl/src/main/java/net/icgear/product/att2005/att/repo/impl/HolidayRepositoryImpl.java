package net.icgear.product.att2005.att.repo.impl;

import java.sql.Timestamp;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

import net.icgear.im.core.persistence.repo.AbstractRepository;
import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.Holiday;
import net.icgear.product.att2005.att.repo.api.HolidayRepository;
import net.icgear.product.att2005.att.rsapi.HolidayRsApi;

/**
 * build by tools[0.1-alpha] on Tue Sep 25 14:13:12 CST 2018
 * @author Y13-Tools(auto)
 * 
 */
@ApplicationScoped
@Repository(forEntity=Holiday.class)
public abstract class HolidayRepositoryImpl  extends AbstractRepository<Holiday, Timestamp> 
        implements  CriteriaSupport<Holiday>, HolidayRepository, SuperRepository<Holiday, Timestamp>,
        HolidayRsApi, SuperRsApi<Holiday, Timestamp> {
}
