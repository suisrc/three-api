package net.icgear.product.att2005.att.rsapi;

import java.util.List;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.UserInfo;

/**
 * build by tools[0.1-alpha] on Thu Jul 19 16:50:48 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface UserInfoRsApi extends SuperRsApi<UserInfo, Integer> {

    /**
     * <p> 更加考勤机ID查询用户
     * @param userId
     * @return
     */
    UserInfo findOptionalBySnId(Integer userId);
    
    /**
     * 
     * @param snNo
     * @return
     */
    UserInfo findOptionalBySnNo(String snNo);
    
    /**
     * 
     * @param name
     * @return
     */
    UserInfo findOptionalByName(String name);
    
    /**
     * <p> 
     * @param openid
     * @return
     */
    UserInfo findOptionalByWxOpenId(String openid);
    
    /**
     * 
     * @param mobile
     * @return
     */
    UserInfo findOptionalByPhone(String mobile);
    
    /**
     * 
     * @param b
     * @return
     */
    List<UserInfo> findByIsLoader(Boolean isLoader);
    
    /**
     * 用户关键字
     * @param wxUserId
     * @return
     */
    UserInfo findOptionalByWxUserId(String wxUserId);
    
}