package net.icgear.product.att2005.wx.service.api;

import java.sql.Timestamp;
import java.util.List;

import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.entity.AttendanceRecord;
import net.icgear.product.att2005.att.entity.CheckInOut;

/**
 * 微信服务处理
 * 
 * @author Y13
 *
 */
public interface WeixinService {

    /**
     * <p> 发送出勤提醒数据
     * @param cios
     */
    void sendCheckInOuts(List<CheckInOut> cios, Timestamp st, Timestamp et);

    /**
     * <p> 给上级发送审核通知
     * @param ard
     */
    void sendApproval2Loader(ApplyRecord ard);

    /**
     * <p> 提交的内容被驳回
     * @param pard
     */
    void sendApproval4Reject(ApplyRecord pard);

    /**
     * <p> 发送打开信息
     * @param res
     */
    void sendAttendanceRecord(List<AttendanceRecord> res);


}
