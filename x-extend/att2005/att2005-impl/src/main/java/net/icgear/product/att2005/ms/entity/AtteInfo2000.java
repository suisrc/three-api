package net.icgear.product.att2005.ms.entity;

import java.sql.Timestamp;

import javax.persistence.Column;

/**
 * 考勤纪律
 * @author Y13
 *
 */
public class AtteInfo2000 {

    /**
     * 用户ID
     */
    @Column(name = "USERID")
    private Integer userId;
    
    /**
     * 用户名称
     */
    @Column(name = "Name")
    private String userName;
    
    /**
     * 打卡时间
     */
    @Column(name = "CHECKTIME")
    private Timestamp checkTime;
    
    /**
     * 打卡类型
     * 
     * 1：指纹
     * 4：卡
     */
    @Column(name = "VERIFYCODE")
    private Integer verifyCode;
    
    /**
     * 考勤编号
     */
    @Column(name = "Badgenumber")
    private String badgenumber;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Timestamp getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Timestamp checkTime) {
        this.checkTime = checkTime;
    }

    public int getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(Integer verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getBadgenumber() {
        return badgenumber;
    }

    public void setBadgenumber(String badgenumber) {
        this.badgenumber = badgenumber;
    }
    
}
