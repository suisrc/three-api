package net.icgear.product.att2005.utils;

import java.util.HashMap;
import java.util.Map;

import com.suisrc.core.Global;

import net.icgear.im.core.mail.MailHandler;

public class MailUtils {

  /**
   * 
   * 发送下载报表的报表邮件通知
   *  
   * USER_NAME, EXPORT_DATE, SYSTEM_NAME, FILE_NAME, FILE_URI, EXPIRED_IN, SIGNATURE, DATE
   * 
   * @param handler 发送报表的句柄
   * @param email 发送邮件的地址
   * @param title 发送邮件的标题
   * @param userName 邮件内容，接受邮件的用户名称
   * @param exportTime 邮件内容，报表导出时间
   * @param systemName 邮件内容，系统名称
   * @param fileName 邮件内容，文件名称
   * @param fileUri 邮件内容，文件下载地址
   * @param expiredIn 邮件内容，下载路径的过期时间
   * @param signature 邮件内容，邮件签名
   * @param date
   */
  public static void sendReport2Email(MailHandler handler, String email, String title, 
          String userName, String exportTime, String systemName, String fileName, String fileUri, 
          String expiredIn, String signature, String date) {
      try {
          Map<String, Object> content = new HashMap<>();
          content.put("USER_NAME", userName);
          content.put("EXPORT_DATE", exportTime);
          content.put("SYSTEM_NAME", systemName);
          content.put("FILE_NAME", fileName);
          content.put("FILE_URI", fileUri);
          content.put("EXPIRED_IN", expiredIn);
          content.put("SIGNATURE", signature);
          content.put("DATE", date);
          // 发送邮件确认
          handler.send("报表邮件下载通知",null, email, title, content, null);
          // 发送结束，记录日志
          Global.getLogger().info(String.format("系统已经向指定的%s邮箱中发送报表邮件下载通知", email));
      } catch (Exception e) {
          e.printStackTrace();
          Global.getLogger().warning(String.format("系统向指定的%s邮箱发送报表邮件下载通知失败: ", email));
      }
  }
}
