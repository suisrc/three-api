package net.icgear.product.att2005.att.service.impl;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.commons.lang.time.DateUtils;
import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jboss.logging.Logger;

import com.suisrc.core.message.ECM;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.im.core.persistence.service.AbstractService;
import net.icgear.im.core.persistence.service.RepoHandler;
import net.icgear.im.core.persistence.service.Service;
import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.entity.AttendanceRecord;
import net.icgear.product.att2005.att.entity.CheckInOut;
import net.icgear.product.att2005.att.entity.CheckInOut.TYPE;
import net.icgear.product.att2005.att.entity.Holiday;
import net.icgear.product.att2005.att.entity.SyncLog;
import net.icgear.product.att2005.att.entity.SystemInfo;
import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.repo.api.CheckInOutRepository;
import net.icgear.product.att2005.att.rsapi.CheckInOutRsApi;
import net.icgear.product.att2005.att.service.api.AttendanceRecordService;
import net.icgear.product.att2005.att.service.api.CheckInOutService;
import net.icgear.product.att2005.att.service.api.HolidayService;
import net.icgear.product.att2005.att.service.api.SyncLogService;
import net.icgear.product.att2005.att.service.api.SystemInfoService;
import net.icgear.product.att2005.att.service.api.UserInfoService;
import net.icgear.product.att2005.dto.CheckInOutRuleDto;
import net.icgear.product.att2005.utils.AttUtils;
import net.icgear.product.att2005.wx.service.api.WeixinService;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * 
 * @author Y13-Tools(auto)
 *
 */
@Service
@ApplicationScoped
public abstract class CheckInOutServiceImpl extends AbstractService<CheckInOut, Integer>
    implements CheckInOutService, SuperService<CheckInOut, Integer>, CheckInOutRsApi, SuperRsApi<CheckInOut, Integer> {
  private static final Logger logger = Logger.getLogger(CheckInOutService.class);

  @Inject
  @RepoHandler
  private CheckInOutRepository repo;

  @Inject
  private AttendanceRecordService attService;

  @Inject
  private WeixinService wxService;

  @Inject
  private SystemInfoService sysService;

  @Inject
  private HolidayService holidayService;

  @Inject
  private UserInfoService userService;
  
  @Inject
  private SyncLogService logService;

  /**
   * <p> 同步出勤情况
   * 
   * <p> 会限定，只会统计前一天的记录
   */
  @Transactional
  @Override
  public void syncCheckInOut(Timestamp stateTime, Timestamp endTime, String desc) {
    long offsetTime = - 6 * 60 * 1000;
    // 执行时间偏移,为了保证0~6点是前一天的内容
    stateTime = new Timestamp(stateTime.getTime() + offsetTime);
    if (endTime != null) {
      endTime = new Timestamp(endTime.getTime() + offsetTime);
    }
    
    List<SyncLog> logs = logService.findByBeginTimeAndDescription(stateTime, desc);
    if (logs != null && !logs.isEmpty()) {
      logger.info("统计考勤已经被执行，当前拒绝重复计算：" + stateTime + "->" + desc);
      return ;
    }
    
    Timestamp statime = stateTime;
    Timestamp endtime = endTime != null ? endTime : Timestamp.valueOf(LocalDate.now().atStartOfDay());
    
    // 执行时间偏移6个小时
    
    SyncLog log = new SyncLog();
    log.setDescription(desc);
    log.setBeginTime(statime);
    log.setEndTime(endtime);
    log = logService.save(log);
    
    List<AttendanceRecord> atts = attService.findByTimeGreaterThanEqualsAndTimeLessThanOrderByTimeAsc(statime, endtime);
    // 依据用户进行分组
    Map<UserInfo, List<AttendanceRecord>> userAtts = new HashMap<>();
    for (AttendanceRecord record : atts) {
      List<AttendanceRecord> rds = userAtts.get(record.getUser());
      if (rds == null) {
        rds = new ArrayList<>();
        userAtts.put(record.getUser(), rds);
      }
      rds.add(record);
    }
    List<UserInfo> userAll = userService.findAll();
    // 增加今天没有打开人员信息
    userAll.stream().filter(v -> !userAtts.containsKey(v)).forEach(v -> userAtts.put(v, Collections.emptyList()));
    // 获取考勤执行使用的规则对象
    CheckInOutRuleDto dto = getCheckInOutRuleDto();
    // 回调考勤计算规则
    Consumer<CheckInOut> consumer = cio -> this.executeCheckInOutRule(dto, cio);
    // 处理单一用户考勤情况
    Integer logId = log.getId();
    userAtts.forEach((u, rs) -> this.processUserAttendances(u, rs, statime, endtime, consumer, logId, offsetTime));
    // 日志
    String msg = "统计考勤：" + desc + ";";
    msg += "统计时间：" + statime + ";";
    msg += "统计人数：" + userAtts.size();
    logger.info(msg);
    log.setCount(userAtts.size());
  }

  /**
   * 
   * @return
   */
  public CheckInOutRuleDto getCheckInOutRuleDto() {
    CheckInOutRuleDto dto = new CheckInOutRuleDto();

    // 标准出勤时间
    SystemInfo ss = sysService.findBy("standard_work_time");
    if (ss == null) {
      ss = new SystemInfo();
      ss.setKey("standard_work_time");
      ss.setValue("28800"); // 7.5小时
      sysService.save(ss);
    }
    dto.setWorktime(Integer.valueOf(ss.getValue()));

    // 上班时间
    ss = sysService.findBy("work_time_on");
    if (ss == null) {
      ss = new SystemInfo();
      ss.setKey("work_time_on");
      ss.setValue("9:00"); // 上班时间
      sysService.save(ss);
    }
    dto.setOnWorktime(Time.valueOf(ss.getValue() + ":00"));

    // 下班时间
    ss = sysService.findBy("work_time_off");
    if (ss == null) {
      ss = new SystemInfo();
      ss.setKey("work_time_off");
      ss.setValue("18:00"); // 下班时间
      sysService.save(ss);
    }
    dto.setOffWorktime(Time.valueOf(ss.getValue() + ":00"));

    // 午休时间
    ss = sysService.findBy("break_time_lunch");
    if (ss == null) {
      ss = new SystemInfo();
      ss.setKey("break_time_lunch");
      ss.setValue("12:00~13:00"); // 午休
      sysService.save(ss);
    }
    int offset = ss.getValue().indexOf('~');
    String value1 = ss.getValue().substring(0, offset);
    String value2 = ss.getValue().substring(offset + 1);
    dto.setLunchBreakStartTime(Time.valueOf(value1 + ":00"));
    dto.setLunchBreakEndTime(Time.valueOf(value2 + ":00"));

    // 晚上休息时间
    ss = sysService.findBy("break_time_night");
    if (ss == null) {
      ss = new SystemInfo();
      ss.setKey("break_time_night");
      ss.setValue("18:00~18:30"); // 午休
      sysService.save(ss);
    }
    offset = ss.getValue().indexOf('~');
    value1 = ss.getValue().substring(0, offset);
    value2 = ss.getValue().substring(offset + 1);
    dto.setNigthBreakStartTime(Time.valueOf(value1 + ":00"));
    dto.setNigthBreakEndTime(Time.valueOf(value2 + ":00"));

    // 用于统计节假日
    // 0工作日，1休息日，2带薪休假
    Map<Long, Integer> hmap = new HashMap<>();
    dto.setHolidayHander(time -> {
      long key = time.getTime();
      Integer type = hmap.get(key);
      if (type != null) {
        return type;
      }
      // 计算当天状态
      Holiday day = holidayService.findOptionalByIdAndStatus(time, 1);
      if (day != null) {
        // 特殊的日期
        type = day.getType();
      } else {
        // 正常日期， 1~5上班，6~7休息
        int value = time.toLocalDateTime().getDayOfWeek().getValue();
        if (value < 6) {
          type = 0;
        } else {
          type = 1;
        }
      }
      // 得到结果，返回
      hmap.put(key, type);
      return type;
    });
    return dto;
  }

  /**
   * <p> 执行出勤标准
   * 
   * @param worktime
   * @param cio
   * @return
   */
  private void executeCheckInOutRule(CheckInOutRuleDto rule, CheckInOut cio) {
    // 标准工作时间
    if (cio.getCheckIn() != null && cio.getCheckOut() == null
        && cio.getCheckIn().toLocalDateTime().toLocalTime().isAfter(rule.getOffWorktime().toLocalTime())) {
      // 仅有的打卡纪律是下班纪律
      cio.setCheckOut(cio.getCheckIn());
      cio.setCheckOutType(cio.getCheckInType());
      cio.setCheckIn(null);
      cio.setCheckInType(null);
    }

    // // 0工作日，1休息日，2带薪休假
    int dayType = rule.getHolidayType(cio.getAttDate());
    // 类型，0正常上班，1正常休息， 2加班，3换休，4休假，5病假，6事假，7缺勤，8带薪休假
    if (dayType == 2) {
      // 带薪休假， 不统计，所有的工作时间划算为标准工作时间
      cio.setWorkTime(rule.getWorktime());
      // 实际工时
      cio.setAttType(TYPE.PAIDLEAVE.ordinal());
      // 有效工作时间
      cio.setActualTime(cio.getWorkTime());
      // 时间结余
      cio.setBalanceTime(0);
      cio.setStatus(0); // 考勤记录没有问题
    }
    if (dayType == 1) {
      // 如果有时间，时间被认证为加班时间
      cio.setWorkTime(0);
      if (cio.getCheckIn() == null && cio.getCheckOut() == null) {
        // 正常休息
        cio.setAttType(TYPE.OFFDAY.ordinal());
        cio.setActualTime(0);
        cio.setBalanceTime(0);
        cio.setStatus(0); // 考勤记录没有问题
      } else if (cio.getCheckIn() != null && cio.getCheckOut() != null) {
        // 加班
        cio.setAttType(TYPE.OVERTIME.ordinal());
        int at = AttUtils.getActualTime(rule, cio.getCheckIn(), cio.getCheckOut());
        cio.setActualTime(at);
        cio.setBalanceTime(at);
        cio.setStatus(1); // 加班有问题，需要申请
      } else {
        // 加班考勤有问题，无法统计实际工时和结余工时
        cio.setAttType(TYPE.OVERTIME.ordinal());
        cio.setStatus(1); // 加班有问题，需要申请
      }
    } else {
      // 正常工作日
      cio.setWorkTime(rule.getWorktime());
      if (cio.getCheckIn() == null && cio.getCheckOut() == null) {
        // 缺勤
        cio.setAttType(TYPE.ABSENCE.ordinal());
        cio.setStatus(1); // 考勤记录没有问题
      } else if (cio.getCheckIn() != null && cio.getCheckOut() != null) {
        // 正常上班
        cio.setAttType(TYPE.WORKDAY.ordinal());
        int at = AttUtils.getActualTime(rule, cio.getCheckIn(), cio.getCheckOut());
        cio.setActualTime(at);
        cio.setBalanceTime(cio.getActualTime() - cio.getWorkTime());
        cio.setStatus(cio.getBalanceTime() < 0 ? 1 : 0); // 有效工时异常，需要确认，需要申请
        if (cio.getStatus() == 0) {
          // 附加，验证是否迟到
          if (cio.getCheckIn() != null) {
            LocalTime time = cio.getCheckIn().toLocalDateTime().toLocalTime();
            if (time.isAfter(LocalTime.of(9, 15))) {
              cio.setStatus(1); // 9:15分后推送通知
            }
          }
        }
      } else {
        // 加班考勤有问题，无法统计实际工时和结余工时
        cio.setAttType(TYPE.WORKDAY.ordinal());
        cio.setStatus(1); // 加班有问题，需要申请
      }
    }
  }

  /**
   * <p> 处理用户考勤
   * 
   * @param user
   * @param records
   * @param etime
   * @param stime
   */
  private void processUserAttendances(UserInfo user, List<AttendanceRecord> records, Timestamp stime, Timestamp etime,
      Consumer<CheckInOut> consumer, Integer logId, long offsetTime) {

    AttendanceRecord first = null;
    AttendanceRecord last = null;

    // 考勤数据
    List<CheckInOut> cios = new ArrayList<>();
    for (AttendanceRecord r : records) {
      if (first == null) {
        first = r;
        continue;
      }
      // 向后推迟时间偏移
      Date s = new Date(first.getTime().getTime() + offsetTime);
      Date e = new Date(r.getTime().getTime() + offsetTime);
      // if (DateUtils.isSameDay(first.getTime(), r.getTime())) {
      if (DateUtils.isSameDay(s, e)) {
        last = r;
        continue;
      }
      // 执行统计
      CheckInOut cio = processUserAttendances(user, null, first, last, consumer);
      cios.add(cio);
      first = r; // 下一个迭代周期
      last = null;
    }
    if (first != null) {
      CheckInOut cio = processUserAttendances(user, null, first, last, consumer);
      cios.add(cio); 
    }
    // 补齐空考勤
    int offset = 0;
    LocalDate curDate = stime.toLocalDateTime().toLocalDate();
    LocalDate endDate = etime.toLocalDateTime().toLocalDate();
    while (curDate.isBefore(endDate)) {
      if (cios.size() <= offset) {
        Timestamp attDateTmp = Timestamp.valueOf(curDate.atStartOfDay());
        CheckInOut cio = processUserAttendances(user, attDateTmp, null, null, consumer);
        cios.add(cio);
      } else {
        LocalDate attDate = cios.get(offset).getAttDate().toLocalDateTime().toLocalDate();
        if (!curDate.isEqual(attDate)) {
          while (curDate.isBefore(attDate)) {
            Timestamp attDateTmp = Timestamp.valueOf(curDate.atStartOfDay());
            CheckInOut cio = processUserAttendances(user, attDateTmp, null, null, consumer);
            cios.add(offset++, cio); // 插入

            curDate = curDate.plusDays(1); // 下一个循环迭代
          }
        }
      }

      curDate = curDate.plusDays(1);
      offset += 1;
    }
    // 保存
    List<CheckInOut> olds = new ArrayList<>();
    cios.forEach(co -> {
      co.setLogId(logId);
      // 验证数据是存在
      List<CheckInOut> olds0 = repo.findByAttDateAndUser(co.getAttDate(), co.getUser());
      if (olds0 == null || olds0.isEmpty()) {
        repo.save(co);
      } else {
        olds.add(co);
      }
    });
    if (!olds.isEmpty()) {
      olds.forEach(cios::remove);
    }
    try {
      wxService.sendCheckInOuts(cios, stime, etime);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * <p> 处理用户考勤
   * 
   * <p> 如果考勤异常，需要返回
   * 
   * @param user
   * @param first
   * @param last
   */
  private CheckInOut processUserAttendances(UserInfo user, Timestamp attDate, AttendanceRecord first, AttendanceRecord last,
      Consumer<CheckInOut> consumer) {
    CheckInOut cio = new CheckInOut();

    if (attDate == null && first != null) {
      LocalDate ldt = first.getTime().toLocalDateTime().toLocalDate();
      attDate = Timestamp.valueOf(ldt.atStartOfDay());
    }
    cio.setAttDate(attDate);
    // 考勤记录所有人
    cio.setUser(user);
    // 执行考勤时候的loader
    cio.setLoader(user.getLoader());
    // 考勤时候所在的项目组
    if (user.isLoader()) {
      cio.setProgram(user.getProgram());
    } else if (user.getLoader() != null) {
      cio.setProgram(user.getLoader().getProgram());
    } else {
      // 最后选择滴定仪项目
      cio.setProgram(user.getProgram());
    }
    // 出勤日志
    if (first != null) {
      // 签到
      cio.setCheckIn(first.getTime());
      cio.setCheckInType(first.getType());
    }
    if (last != null) {
      // 签退
      cio.setCheckOut(last.getTime());
      cio.setCheckOutType(last.getType());
    }
    // 统计
    consumer.accept(cio);
    return cio;
  }

  /**
   * <p>
   */
  @Transactional
  @Override
  public void removeByAttDateGreaterThanEquals(Timestamp time) {
    List<CheckInOut> list = repo.findByAttDateGreaterThanEquals(time);
    list.forEach(repo::remove);
  }


  /**
   * <p> 查询异常列表
   * 
   * @return
   */
  @Override
  public List<CheckInOut> searchErrList(String openid) {
    UserInfo user = userService.findOptionalByWxOpenId(openid);
    if (user == null) {
      throw ECM.exception(-11, "当前登录人员无法识别为企业人员:" + openid);
    }
    return repo.findByUserAndStatus(user, 1);
  }

  /**
   * <p> 重新计算考勤
   */
  @Transactional
  @Override
  public void recalculateAttendance(CheckInOut cio, ApplyRecord pard) {
    // 获取考勤执行使用的规则对象
    CheckInOutRuleDto rule = getCheckInOutRuleDto();
    // 考勤计算规则
    Timestamp checkIn = pard.getNewCheckIn() != null ? pard.getNewCheckIn() : pard.getOldCheckIn();
    Timestamp checkOut = pard.getNewCheckOut() != null ? pard.getNewCheckOut() : pard.getOldCheckOut();
    if (checkIn == null || checkOut == null) {
      return; // 不再计算
    }
    int workTime = AttUtils.getActualTime(rule, checkIn, checkOut);
    // 有效工时
    // cio.setActualTime(workTime);
    pard.setActualTime(workTime);
    // 当天时间结余
    // cio.setBalanceTime(cio.getActualTime() - cio.getWorkTime());
    pard.setBalanceTime(workTime - cio.getWorkTime());
  }
}
