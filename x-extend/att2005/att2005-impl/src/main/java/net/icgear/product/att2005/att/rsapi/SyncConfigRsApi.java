package net.icgear.product.att2005.att.rsapi;

import java.util.List;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.SyncConfig;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface SyncConfigRsApi extends SuperRsApi<SyncConfig, Integer> {

  /**
   * <p> 获取同步配置信息
   * @param status
   * @return
   */
  List<SyncConfig> findByStatus(Integer status);
}