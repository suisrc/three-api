package net.icgear.product.att2005.att.service.api;

import java.sql.Timestamp;
import java.util.List;

import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.AttendanceRecord;
import net.icgear.product.att2005.att.rsapi.AttendanceRecordRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 14:19:10 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface AttendanceRecordService extends SuperService<AttendanceRecord, Integer>, AttendanceRecordRsApi {

  /**
   * <p> 同步记录
   * @param object
   */
  void syncAttRecord(Timestamp start, Timestamp end, String desc);
  
  /**
   * <p> 同步记录
   * @param object
   */
  List<AttendanceRecord> syncAttRecordAuto(String desc);

}
