package net.icgear.product.att2005.wx.listener.msg;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.suisrc.core.reference.RefVal;
import com.suisrc.three.core.listener.inf.Listener;

import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.service.api.UserInfoService;
import net.icgear.three.weixin.core.msg.TextMessage;
import net.icgear.three.weixin.core.reply.ReplyTextMessage;

/**
 * 
 * <p> 编辑个人信息
 * 
 * <p> 目前支持
 * <br> 邮箱
 * 
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class EditUserInfoListener implements Listener<TextMessage> {
  private static final String KEY = "编辑我的";
  
  /**
   * 用户管理
   */
  @Inject private UserInfoService userService;
  

  /**
   * 
   */
  @Override
  public boolean accept(RefVal<Object> result, TextMessage bean) {
    ReplyTextMessage msg = bean.reverse(new ReplyTextMessage());
    result.set(msg);
    
    String content = bean.getContent().substring(KEY.length());
    int offset = content.indexOf(':');
    if (offset < 0) {
      msg.setContent("指令识别错误，请检测指令格式：“编辑我的{property}:{value}”");
      return false;
    }
    String key = content.substring(0, offset);
    String value = content.substring(offset + 1);
    
    UserInfo user = userService.findOptionalByWxUserId(bean.getFromUserName());
    if (user == null) {
      msg.setContent("对不起，无法确定你的身份，修改您的【" + key +"】信息失败。");
    } else if (key.equals("邮箱")) {
      String old = user.getEmail();
      user.setEmail(value);
      userService.merge2(user);
      msg.setContent(String.format("修改邮箱成功，邮箱由【%s】变更为【%s】", old != null ? old : "无", value));
    } else if (key.equals("项目")) {
      String old = user.getProgram();
      user.setProgram(value);
      userService.merge2(user);
      msg.setContent(String.format("修改项目成功，项目由【%s】变更为【%s】", old != null ? old : "无", value));
    } else {
      msg.setContent("对不起，暂时不支持修改您的【" + key +"】信息。");
    }
    return false;
  }

  @Override
  public boolean matches(boolean hasResult, TextMessage bean) {
    if (bean.getContent().startsWith(KEY)) {
      return true;
    }
    return false;
  }

  /**
   * 修改加载优先级
   */
  @Override
  public String priority(TextMessage bean) {
    return "M";
  }
}
