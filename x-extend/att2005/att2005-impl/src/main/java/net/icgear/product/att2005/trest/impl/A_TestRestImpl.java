package net.icgear.product.att2005.trest.impl;

import java.time.LocalDateTime;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.icgear.product.att2005.att.service.api.A_TestBeanService;
import net.icgear.product.att2005.trest.api.A_TestRest;

@ApplicationScoped
public class A_TestRestImpl implements A_TestRest {
  
  @Inject
  private A_TestBeanService service;

  @Override
  public String testServer() {
    return "服务器正常运行：" + LocalDateTime.now();
  }

  @Override
  public String testDatabase() {
    return "数据库【" + service.findBy(1).getName() + "】连接正常：" + LocalDateTime.now();
  }

}
