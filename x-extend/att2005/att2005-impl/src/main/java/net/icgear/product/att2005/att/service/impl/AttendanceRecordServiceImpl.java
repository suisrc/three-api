package net.icgear.product.att2005.att.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jboss.logging.Logger;

import com.suisrc.core.reference.RefVal;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.im.core.persistence.service.AbstractService;
import net.icgear.im.core.persistence.service.RepoHandler;
import net.icgear.im.core.persistence.service.Service;
import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.AttendanceRecord;
import net.icgear.product.att2005.att.entity.SyncLog;
import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.repo.api.AttendanceRecordRepository;
import net.icgear.product.att2005.att.rsapi.AttendanceRecordRsApi;
import net.icgear.product.att2005.att.service.api.AttendanceRecordService;
import net.icgear.product.att2005.att.service.api.SyncLogService;
import net.icgear.product.att2005.att.service.api.UserInfoService;
import net.icgear.product.att2005.ms.service.AtteInfo2000Service;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 14:19:10 CST 2018
 * 
 * @author Y13-Tools(auto)
 *
 */
@Service
@ApplicationScoped
public abstract class AttendanceRecordServiceImpl extends AbstractService<AttendanceRecord, Integer>
    implements AttendanceRecordService, SuperService<AttendanceRecord, Integer>, AttendanceRecordRsApi,
    SuperRsApi<AttendanceRecord, Integer> {
  private static final Logger logger = Logger.getLogger(AttendanceRecordService.class);

  @Inject
  @RepoHandler
  private AttendanceRecordRepository repo;

  @Inject
  private AtteInfo2000Service attService;

  @Inject
  private UserInfoService userService;

  @Inject
  private SyncLogService logService;

  /**
   * <p> 同步考勤记录到数据库中
   */
  @Transactional
  @Override
  public void syncAttRecord(Timestamp start, Timestamp end, String desc) {
    RefVal<Integer> count = new RefVal<>(0);

    if (end == null) {
      end = new Timestamp(System.currentTimeMillis());
    }
    
    // 修正时间
    List<AttendanceRecord> olds = repo.findTop1OrderByTimeDesc();
    if (olds != null && !olds.isEmpty()) {
      AttendanceRecord r = olds.get(0);
      if (start.before(r.getTime())) {
        start = new Timestamp(r.getTime().getTime() + 100);
      }
    }
    // 记录日志
    SyncLog log = new SyncLog();
    log.setDescription(desc);
    log.setBeginTime(start);
    log.setEndTime(end);
    log = logService.save(log);
    // 执行同步
    Integer logId = log.getId();
    attService.forEachAtteInfo200(start, end, v -> {
      UserInfo userInfo = userService.findOptionalBySnId(v.getUserId());
      if (userInfo == null) {
        // 用户没有，执行用户构建
        userInfo = new UserInfo();
        userInfo.setSnId(v.getUserId());
        userInfo.setSnName(v.getUserName());
        userInfo.setSnNo(v.getBadgenumber());
        // 保存没有的用户
        userInfo.setName(v.getUserName());
        userInfo = userService.save(userInfo);
      }
      // 避免考勤记录重复增加
      List<AttendanceRecord> records = repo.findByUserAndTime(userInfo, v.getCheckTime());
      if (records == null || records.isEmpty()) {
        AttendanceRecord record = new AttendanceRecord();
        record.setTime(v.getCheckTime());
        record.setType(v.getVerifyCode());
        record.setUser(userInfo);
        record.setLogId(logId);
        repo.save(record);
        // 数据记录
        count.set(count.get() + 1);
      }
    });
    
    log.setCount(count.get());

    logger.warn(log.toString2());
  }


  /**
   * <p> 同步考勤记录到数据库中
   */
  @Transactional
  @Override
  public List<AttendanceRecord> syncAttRecordAuto(String desc) {
    List<AttendanceRecord> res = new ArrayList<>();
    RefVal<Integer> count = new RefVal<>(0);

    Timestamp start = null;
    // 修正时间
    List<AttendanceRecord> olds = repo.findTop1OrderByTimeDesc();
    if (olds != null && !olds.isEmpty()) {
      AttendanceRecord f1 = olds.get(0);
      start = new Timestamp(f1.getTime().getTime() + 500);
    }
    // 记录日志
    SyncLog log = new SyncLog();
    log.setDescription(desc);
    log.setBeginTime(start);
    log.setEndTime(new Timestamp(System.currentTimeMillis()));
    log = logService.save(log);
    // 执行同步
    Integer logId = log.getId();
    attService.forEachAtteInfo200(start, null, v -> {
      UserInfo userInfo = userService.findOptionalBySnId(v.getUserId());
      if (userInfo == null) {
        // 用户没有，执行用户构建
        userInfo = new UserInfo();
        userInfo.setSnId(v.getUserId());
        userInfo.setSnName(v.getUserName());
        userInfo.setSnNo(v.getBadgenumber());
        // 保存没有的用户
        userInfo.setName(v.getUserName());
        userInfo = userService.save(userInfo);
      }
      // 避免考勤记录重复增加
      List<AttendanceRecord> records = repo.findByUserAndTime(userInfo, v.getCheckTime());
      if (records == null || records.isEmpty()) {
        AttendanceRecord record = new AttendanceRecord();
        record.setTime(v.getCheckTime());
        record.setType(v.getVerifyCode());
        record.setUser(userInfo);
        record.setLogId(logId);
        record = repo.save(record);
        // 数据记录
        count.set(count.get() + 1);
        res.add(record);
      }
    });
    
    log.setCount(count.get());
    logger.warn(log.toString2());
    return res;
  }
}
