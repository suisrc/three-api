package net.icgear.product.att2005.att.repo.api;

import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.product.att2005.att.entity.AttendanceRecord;
import net.icgear.product.att2005.att.rsapi.AttendanceRecordRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 14:19:10 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface AttendanceRecordRepository extends SuperRepository<AttendanceRecord, Integer>, AttendanceRecordRsApi {
}
