package net.icgear.product.att2005.trest.dto;

import java.sql.Timestamp;

import net.icgear.im.common.bean.OriginIgnore;
import net.icgear.im.common.bean.OriginMethod;
import net.icgear.im.common.bean.OriginType;
import net.icgear.product.att2005.att.entity.CheckInOut;

/**
 * <p> 申请内容
 * 
 * @author Y13
 *
 */
@OriginType(clazz = CheckInOut.class)
public class ApplyInfoDto {
  
  /**
   * <p> 数据主键
   */
  private Integer id;

  /**
   * <p> 异常数据时间
   */
  private String errDate;

  /**
   * <P> 异常类型
   */
  private String errType;

  /**
   * <p> 出勤时间
   */
  private String checkInTime;

  /**
   * <p> 退勤时间
   */
  private String checkOutTime;

  /**
   * <p> 用户
   */
  private String user;

  /**
   * <p> 上级
   */
  private String loader;

  /**
   * <p> 所属项目
   */
  private String project;

  /**
   * <p> 迟到原因
   */
  private String reason;

  /**
   * <p> 备注
   */
  private String comment;

  /**
   * 有效出勤时间
   */
  private String actual;

  /**
   * <p> 处理状态0， 已经处理， 1， 未处理，2，待处理， 3，异常
   */
  private Integer status;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getErrDate() {
    return errDate;
  }

  @OriginIgnore
  public void setErrDate(String errDate) {
    this.errDate = errDate;
  }

  @OriginMethod("setAttDate")
  public void setErrDate(Timestamp date) {
    this.errDate = date.toLocalDateTime().toLocalDate().toString();
  }

  public String getErrType() {
    return errType;
  }

  @OriginIgnore
  public void setErrType(String errType) {
    this.errType = errType;
  }

  public String getCheckInTime() {
    return checkInTime;
  }

  @OriginIgnore
  public void setCheckInTime(String checkInTime) {
    this.checkInTime = checkInTime;
  }

  public String getCheckOutTime() {
    return checkOutTime;
  }

  @OriginIgnore
  public void setCheckOutTime(String checkOutTime) {
    this.checkOutTime = checkOutTime;
  }

  public String getUser() {
    return user;
  }

  @OriginIgnore
  public void setUser(String user) {
    this.user = user;
  }

  public String getLoader() {
    return loader;
  }

  @OriginIgnore
  public void setLoader(String loader) {
    this.loader = loader;
  }

  public String getProject() {
    return project;
  }

  @OriginIgnore
  public void setProject(String project) {
    this.project = project;
  }

  public String getReason() {
    return reason;
  }

  @OriginIgnore
  public void setReason(String reason) {
    this.reason = reason;
  }

  public String getComment() {
    return comment;
  }

  @OriginIgnore
  public void setComment(String comment) {
    this.comment = comment;
  }

  public Integer getStatus() {
    return status;
  }

  @OriginIgnore
  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getActual() {
    return actual;
  }

  public void setActual(String actual) {
    this.actual = actual;
  }
  
  
}
