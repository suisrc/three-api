package net.icgear.product.att2005.ms.service;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.Column;

import com.suisrc.core.utils.Throwables;

import net.icgear.product.att2005.ms.entity.AtteInfo2000;

/**
 * 读取打卡器数据
 * 
 * 仅用于处理打卡器数据
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class AtteInfo2000ServiceImpl implements AtteInfo2000Service {
    private static final Logger logger = Logger.getLogger(AtteInfo2000ServiceImpl.class.getName());
    
    /**
     * 考勤机文件的位置
     */
    public static final String MDB_FILE = "file.path.att2000";
    
    /**
     * 验证是否激活
     */ 
    private boolean isActive = true;
    
    /**
     * 考勤机记录文件的位置
     */
    private File mdbFile = null;
    
    @PostConstruct
    public void doPostConstruct() {
        String file = System.getProperty(MDB_FILE);
        if (file == null) {
            isActive = false;
            logger.warning("没有配置考勤机数据库文件的位置，无法同步考勤机数据");
            return;
        }
        mdbFile = new File(file);
        try {
            Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
        } catch (ClassNotFoundException e) {
            isActive = false;
            logger.warning("UcanaccessDriver驱动加载失败，无法同步考勤机数据");
        }
    }

    @Override
    public List<AtteInfo2000> getAtteInfo2000List(Timestamp sTime, Timestamp eTime) {
        if (!isActive) {
            return null;
        }
        List<AtteInfo2000> res = new ArrayList<>();
        syncDataSource(sTime, eTime, AtteInfo2000.class, res::add);
        return res;
    }

    @Override
    public void forEachAtteInfo200(Timestamp sTime, Timestamp eTime, Consumer<AtteInfo2000> consumer) {
        if (!isActive) {
            return;
        }
        syncDataSource(sTime, eTime, AtteInfo2000.class, consumer);
    }
    
    /**
     * 获取同步数据使用的sql
     * @return
     */
    private String getSyncSql(Timestamp sTime, Timestamp eTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sql = "SELECT u.USERID, u.`Name`, u.Badgenumber, t.CHECKTIME, t.VERIFYCODE "
                   + "FROM CHECKINOUT t INNER JOIN USERINFO u ON t.USERID = u.USERID";
        if (sTime != null || eTime != null) {
            sql += " WHERE";
            if (sTime != null) {
                String time = sdf.format(sTime);
                sql += " t.CHECKTIME >= '" + time + "'";
            }
            if (eTime != null) {
                if (sTime != null) {
                    sql += " AND";
                }
                String time = sdf.format(eTime);
                sql += " t.CHECKTIME < '" + time + "'";
            }
        }
        logger.info("同步考勤机SQL:" + sql);
        return sql;
    }

    /**
     * 查询考勤机器内容集合
     */
    private <T> void syncDataSource(Timestamp sTime, Timestamp eTime, Class<T> type, Consumer<T> consumer) {
        // 数据库连接地址
        String url = "jdbc:ucanaccess://" + mdbFile.getAbsolutePath();  

        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(getSyncSql(sTime, eTime));
            // ResultSetMetaData meta = rs.getMetaData();
            Field[] fields = type.getDeclaredFields();
            Map<String, Method> methods = new HashMap<String, Method>();
            for (Method method : type.getMethods()) {
                if (method.getName().startsWith("set") && method.getParameterCount() == 1) {
                    String name = method.getName().substring(3);
                    name = name.substring(0, 1).toLowerCase() + name.substring(1);
                    methods.put(name, method);
                }
            }
            while (rs.next()) {
                T bean = type.newInstance();
                for (Field field : fields) {
                    Method method = methods.get(field.getName());
                    if (method == null) {
                        continue;
                    }
                    Column column = field.getAnnotation(Column.class);
                    if (column == null) {
                        continue;
                    }
                    String key = column.name().isEmpty() ? field.getName() : column.name();
                    Object obj = rs.getObject(key);
                    if (obj != null) {
                        if (method.getParameterTypes()[0].isAssignableFrom(obj.getClass())) {
                            method.invoke(bean, obj);
                        } else {
                            logger.info("类型无法转换：" + obj.getClass() + "->" + method.getParameterTypes()[0]);
                        }
                    }
                }
                consumer.accept(bean);
            }  
        } catch (Exception e) {
            throw Throwables.getRuntimeException(e);
        } finally {
            // 清理数据库连接
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
}
