package net.icgear.product.att2005.att.rsapi;

import java.sql.Timestamp;
import java.util.List;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.CheckInOut;
import net.icgear.product.att2005.att.entity.UserInfo;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface CheckInOutRsApi extends SuperRsApi<CheckInOut, Integer> {


    /**
     * <p> 起初计算过的考勤
     * 
     * @param valueOf
     */
    List<CheckInOut> findByAttDateGreaterThanEquals(Timestamp time);

    /**
     * <p> 
     * 
     * @param valueOf
     */
    List<CheckInOut> findByAttDateGreaterThanEqualsAndAttDateLessThan(Timestamp time1, Timestamp time2);

    /**
     * <p> 
     * 
     * @param valueOf
     */
    List<CheckInOut> findByAttDateGreaterThanEqualsAndAttDateLessThanAndLoader(Timestamp time1, Timestamp time2, UserInfo loader);

    /**
     * <p> 
     * 
     * @param valueOf
     */
    List<CheckInOut> findByAttDateGreaterThanEqualsAndAttDateLessThanAndUser(Timestamp time1, Timestamp time2, UserInfo user);
    
    /**
     * <p> 查看特殊状态的考勤内容
     * @param time
     * @param i
     * @return
     */
    List<CheckInOut> findByAttDateGreaterThanEqualsAndStatus(Timestamp time, Integer status);
    
    /**
     * <p> 查看特殊状态的考勤内容
     * @param time
     * @param i
     * @return
     */
    List<CheckInOut> findByUserAndAttDateGreaterThanEqualsAndStatus(UserInfo user, Timestamp time, Integer status);
    
    /**
     * <p> 查询考勤异常的人员
     * @param user
     * @param status
     * @return
     */
    List<CheckInOut> findByUserAndStatus(UserInfo user, Integer status);
    
    /**
     * <p> 
     * @param ids
     * @return
     */
    List<CheckInOut> findByIdIn(Integer[] ids);

    /**
     * 
     * @param openid
     * @param status
     * @return
     */
    List<CheckInOut> findByUser_wxOpenIdAndStatus(String openid, Integer status);
    
    /**
     * <p> 获取默认的考勤纪律
     * @param attDate
     * @return
     */
    List<CheckInOut> findByAttDateAndUser(Timestamp attDate, UserInfo user);
}