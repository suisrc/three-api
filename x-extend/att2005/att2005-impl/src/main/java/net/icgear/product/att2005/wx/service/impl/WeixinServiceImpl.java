package net.icgear.product.att2005.wx.service.impl;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import com.suisrc.jaxrsapi.core.ApiActivator;
import com.suisrc.jaxrsapi.core.ServiceClient;

import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.entity.AttendanceRecord;
import net.icgear.product.att2005.att.entity.CheckInOut;
import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.wx.service.api.WeixinService;
import net.icgear.three.weixin.core.AbstractWeixinActivator;
import net.icgear.three.weixin.qyapi.rest.api.MessageManagerRest;
import net.icgear.three.weixin.qyapi.rest.dto.message.TextMessage;
import net.icgear.three.weixin.qyapi.rest.dto.message.TextcardMessage;
import net.icgear.three.weixin.qyapi.rest.dto.message.ext.TextCardInfo;
import net.icgear.three.weixin.qyapi.rest.dto.message.ext.TextCardInfo.Color;

/**
 * <p> 微信服务处理
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class WeixinServiceImpl implements WeixinService {
  private static final Logger logger = Logger.getLogger(WeixinServiceImpl.class);
  private static final boolean isNotSend = Boolean.getBoolean("net.icgear.product.att.db.service.weixin.send");

  /**
   * <p> 微信发送消息
   */
  @Inject
  private MessageManagerRest msgMngService;
  
  @Named
  @Override
  public void sendCheckInOuts(List<CheckInOut> cios, Timestamp st, Timestamp et) {
    if (isNotSend) {
      return;
    }
    // 执行分组
    Map<UserInfo, List<CheckInOut>> hcios = new HashMap<>();

    cios.forEach(v -> {
      List<CheckInOut> list = hcios.get(v.getUser());
      if (list == null) {
        list = new ArrayList<>();
        hcios.put(v.getUser(), list);
      }
      list.add(v);
    });
    // 执行发送
    Timestamp etr = new Timestamp(et.getTime() - 1000);
    hcios.forEach((k, v) -> this.sendCheckInOuts(k, v, st, etr));
  }

  /**
   * <p> 发送
   * 
   * @param user
   * @param cios
   */
  private void sendCheckInOuts(UserInfo user, List<CheckInOut> cios, Timestamp st, Timestamp et) {
    // String openid = user.getWxOpenId();
    String userid = user.getWxUserId();
    String corpid = user.getWxCorpId();
    if (userid == null || corpid == null) {
      logger.error("用户没有跟微信服务器绑定，请先执行绑定：" + user.getSnNo());
      return;
    }
    SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日");
    SimpleDateFormat sdft = new SimpleDateFormat("HH:mm:ss");
    DecimalFormat df = new DecimalFormat("0.00");

    // 获取当前访问使用的激活器
    ApiActivator activator = ((ServiceClient) msgMngService).getActivator();
    AbstractWeixinActivator wxActivator = activator.as(AbstractWeixinActivator.class);

    if (cios.size() > 1) {
      // 如果是集合，只推送有异常的数据
      List<CheckInOut> newcios = cios.stream().filter(v -> v.getStatus() == 1).collect(Collectors.toList());
      if (newcios.isEmpty()) {
        cios = Arrays.asList(cios.get(cios.size() - 1));
      } else {
        cios = newcios;
      }
    }
    TextcardMessage msg = new TextcardMessage();
    if (cios.size() == 1) {
      CheckInOut cio = cios.get(0);
      // XX于xx月xx日出勤内容
      String date = sdf.format(cio.getAttDate());
      String title = "您于" + date + "出勤内容";
      msg.setTitle(title);
      // 申请uri
      String url = wxActivator.getWebIndex("apply2");
      url += url.indexOf('?') < 0 ? "?" : "&";
      url += "id=" + cio.getId();
      msg.setUrl(url);
      // 内容
      TextCardInfo info = new TextCardInfo();
      // 签到是否异常
      if (cio.getStatus() == 1) {
        info.add("考勤异常", CheckInOut.TYPE.values()[cio.getAttType()].desc, Color.highlight);
      }
      // 签到
      if (cio.getCheckIn() != null) {
        info.add("签到时间", sdft.format(cio.getCheckIn()), null);
      } else {
        info.add("签到时间", "无", Color.highlight);
      }
      // 签退
      if (cio.getCheckOut() != null) {
        info.add("签退时间", sdft.format(cio.getCheckOut()), null);
      } else {
        info.add("签退时间", "无", Color.highlight);
      }

      if (cio.getActualTime() != null) {
        double time = cio.getActualTime().doubleValue() / 3600;
        info.add("有效工时", df.format(time), null);
      } else {
        info.add("有效工时", "无法计算", Color.highlight);
      }
      if (cio.getBalanceTime() != null) {
        double time = cio.getBalanceTime().doubleValue() / 3600;
        info.add("结余工时", df.format(time), null);
      } else {
        info.add("结余工时", "无法计算", Color.highlight);
      }
      msg.setDescription(info.getDescription());
    } else {
      // XX出勤内容
      String title = "您出勤内容统计";
      msg.setTitle(title);
      // 申请uri
      String url = wxActivator.getWebIndex("apply2");
      CheckInOut first = cios.get(0);
      url += url.indexOf('?') < 0 ? "?" : "&";
      url += "id=" + first.getId();
      CheckInOut last = null;
      for (int i = 1; i < cios.size(); i++) {
        last = cios.get(i);
        url += "&id=" + last.getId();
      }
      msg.setUrl(url);
      // 内容
      TextCardInfo info = new TextCardInfo();
      // 签到是否异常
      //info.add("开始时间", sdf.format(first.getAttDate()), null);
      //info.add("结束时间", sdf.format(last.getAttDate()), null);
      info.add("开始时间", sdf.format(st), null);
      info.add("结束时间", sdf.format(et), null);
      if (cios.size() > 0) {
        info.add("异常数量", String.valueOf(cios.size()), Color.highlight);
      } else {
        info.add("异常数量", "无", Color.gray);
      }
      msg.setDescription(info.getDescription());
    }
    msg.setBtntxt("异常申报");
    // 发送通知
    msgMngService.sendMessage(corpid, userid, null, null, msg);
  }

  /**
   * <p> 发送审批通知
   */
  @Override
  public void sendApproval2Loader(ApplyRecord record) {
    if (isNotSend) {
      return;
    }
    UserInfo user = record.getWorker();
    if (user == null) {
      logger.error("审批没有下一步处理人员信息，请求ID：" + record.getId());
      return;
    }
    String userid = user.getWxUserId();
    String corpid = user.getWxCorpId();
    if (userid == null || corpid == null) {
      logger.error("审批上级没有跟微信服务器绑定，请先执行绑定：" + user.getSnNo());
      return;
    }

    // 获取当前访问使用的激活器
    ApiActivator activator = ((ServiceClient) msgMngService).getActivator();
    AbstractWeixinActivator wxActivator = activator.as(AbstractWeixinActivator.class);

    TextcardMessage msg = new TextcardMessage();
    // XX出勤内容
    String title = record.getUser().getName() + "异常审核";
    msg.setTitle(title);
    // 申请uri
    String url = wxActivator.getWebIndex("approval2");
    url += url.indexOf('?') < 0 ? "?" : "&";
    url += "id=" + record.getCheckInOut().getId();
    msg.setUrl(url);
    // 内容
    TextCardInfo info = new TextCardInfo();
    info.add("出勤时间", record.getCheckInOut().getAttDate().toLocalDateTime().toLocalDate().toString(), null);
    info.add("异常类型", CheckInOut.TYPE.values()[record.getReasonType()].desc, null);
    info.add("异常原因", record.getReason() == null ? "空" : record.getReason(), null);
    info.add("异常批注", record.getComment() == null ? "空" : record.getComment(), null);
    msg.setDescription(info.getDescription());
    msg.setBtntxt("进行审核");
    // 发送通知
    msgMngService.sendMessage(corpid, userid, null, null, msg);
  }

  /**
   * <p> 请求被驳回
   */
  @Override
  public void sendApproval4Reject(ApplyRecord record) {
    if (isNotSend) {
      return;
    }
    UserInfo user = record.getWorker();
    if (user == null) {
      logger.error("申请没有下一步处理人员信息，请求ID：" + record.getId());
      return;
    }
    String userid = user.getWxUserId();
    String corpid = user.getWxCorpId();
    if (userid == null || corpid == null) {
      logger.error("申请用户没有跟微信服务器绑定，请先执行绑定：" + user.getSnNo());
      return;
    }

    // 获取当前访问使用的激活器
    ApiActivator activator = ((ServiceClient) msgMngService).getActivator();
    AbstractWeixinActivator wxActivator = activator.as(AbstractWeixinActivator.class);

    TextcardMessage msg = new TextcardMessage();
    // XX出勤内容
    String title = "您的考勤申请驳回";
    msg.setTitle(title);
    // 申请uri
    String url = wxActivator.getWebIndex("apply2");
    url += url.indexOf('?') < 0 ? "?" : "&";
    url += "id=" + record.getCheckInOut().getId();
    msg.setUrl(url);
    // 内容
    TextCardInfo info = new TextCardInfo();
    info.add("出勤时间", record.getCheckInOut().getAttDate().toLocalDateTime().toLocalDate().toString(), null);
    info.add("异常类型", CheckInOut.TYPE.values()[record.getReasonType()].desc, null);
    info.add("异常原因", record.getReason() == null ? "空" : record.getReason(), null);
    info.add("异常批注", record.getComment() == null ? "空" : record.getComment(), null);
    msg.setDescription(info.getDescription());
    msg.setBtntxt("修改申请");
    // 发送通知
    msgMngService.sendMessage(corpid, userid, null, null, msg);
  }

  @Override
  public void sendAttendanceRecord(List<AttendanceRecord> res) {
    if (isNotSend) {
      return;
    }
    long time = System.currentTimeMillis();
    int delay = 1000 * 60 * 60;
    // 获取当前访问使用的激活器
    for (AttendanceRecord re : res) {
      if (time - re.getTime().getTime() > delay) {
        continue;
        // 同步超时的内容不进行发送
      }
      try {
        UserInfo user =re.getUser();
        if (user == null) {
          logger.error("没有找到用户，记录ID：" + re.getId());
          continue;
        }
        String userid = user.getWxUserId();
        String corpid = user.getWxCorpId();
        if (userid == null || corpid == null) {
          logger.error("考勤记录的用户没有跟微信服务器绑定，请先执行绑定：" + user.getSnNo());
          continue;
        }

        TextMessage msg = new TextMessage();
        String content = "您于" + re.getTime().toLocalDateTime().toString() + "进行了打卡操作";
        msg.setContent(content);
        // 发送通知
        msgMngService.sendMessage(corpid, userid, null, null, msg);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

}
