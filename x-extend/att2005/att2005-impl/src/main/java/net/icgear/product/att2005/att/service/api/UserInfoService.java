package net.icgear.product.att2005.att.service.api;

import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.rsapi.UserInfoRsApi;

/**
 * build by tools[0.1-alpha] on Thu Jul 19 16:50:48 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface UserInfoService extends SuperService<UserInfo, Integer>, UserInfoRsApi {

}
