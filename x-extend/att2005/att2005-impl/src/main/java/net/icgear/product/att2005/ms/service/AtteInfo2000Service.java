package net.icgear.product.att2005.ms.service;

import java.sql.Timestamp;
import java.util.List;
import java.util.function.Consumer;

import net.icgear.product.att2005.ms.entity.AtteInfo2000;

/**
 * 获取打卡器数据
 * 
 * @author Y13
 *
 */
public interface AtteInfo2000Service {

    /**
     * 获取打卡器数据
     * @param sTime 起始时间
     * @param eTime 终止时间
     * @return
     */
    List<AtteInfo2000> getAtteInfo2000List(Timestamp sTime, Timestamp eTime);
    
    /**
     * 变量打卡器数据
     * @param sTime
     * @param eTime
     */
    void forEachAtteInfo200(Timestamp sTime, Timestamp eTime, Consumer<AtteInfo2000> consumer);
    
}
