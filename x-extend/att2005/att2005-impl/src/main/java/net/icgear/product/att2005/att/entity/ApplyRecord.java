package net.icgear.product.att2005.att.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import net.icgear.im.core.persistence.IA;

@Entity
@Table(name = "apply_record")
public class ApplyRecord implements IA<Integer> {
  private static final long serialVersionUID = 1L;

  /**
   * <p> id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", length = 10)
  private Integer id;

  /**
   * <p> 出勤
   */
  @ManyToOne
  @JoinColumn(name = "check_in_out_id")
  private CheckInOut checkInOut;

  /**
   * <p> 出勤时间
   */
  @Column(name = "old_check_in")
  private Timestamp oldCheckIn;

  /**
   * <p> 退勤时间
   */
  @Column(name = "old_check_out")
  private Timestamp oldCheckOut;

  /**
   * <p> 修正出勤时间
   */
  @Column(name = "new_check_in")
  private Timestamp newCheckIn;

  /**
   * <p> 修正退勤时间
   */
  @Column(name = "new_check_out")
  private Timestamp newCheckOut;

  /**
   * <p> 异常原因
   */
  private String reason;

  /**
   * <p> 当前使用
   */
  @ManyToOne
  @JoinColumn(name = "worker_id")
  private UserInfo worker;

  /**
   * <p> 用户
   */
  @ManyToOne
  @JoinColumn(name = "user_id")
  private UserInfo user;

  /**
   * <p> 领导 <p> 如果没有领导，会直接通过修改
   */
  @ManyToOne
  @JoinColumn(name = "loader_id")
  private UserInfo loader;

  /**
   * <p> 状态
   */
  private Integer status;

  /**
   * <p> 批注
   */
  private String comment;

  /**
   * <p> 原因分类
   */
  @Column(name = "reason_type")
  private Integer reasonType;

  /**
   * <p> 数据版本号
   */
  @Version
  private Integer version;

  /**
   * <p> 实际工作时间
   */
  @Column(name = "actual_time")
  private Integer actualTime;

  /**
   * <p> 时间有效结余
   * 
   * <br>正常工作为0 <br>加班为正 <br>缺勤为负
   */
  @Column(name = "balance_time")
  private Integer balanceTime;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public CheckInOut getCheckInOut() {
    return checkInOut;
  }

  public void setCheckInOut(CheckInOut checkInOut) {
    this.checkInOut = checkInOut;
  }

  public Timestamp getOldCheckIn() {
    return oldCheckIn;
  }

  public void setOldCheckIn(Timestamp oldCheckIn) {
    this.oldCheckIn = oldCheckIn;
  }

  public Timestamp getOldCheckOut() {
    return oldCheckOut;
  }

  public void setOldCheckOut(Timestamp oldCheckOut) {
    this.oldCheckOut = oldCheckOut;
  }

  public Timestamp getNewCheckIn() {
    return newCheckIn;
  }

  public void setNewCheckIn(Timestamp newCheckIn) {
    this.newCheckIn = newCheckIn;
  }

  public Timestamp getNewCheckOut() {
    return newCheckOut;
  }

  public void setNewCheckOut(Timestamp newCheckOut) {
    this.newCheckOut = newCheckOut;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public UserInfo getUser() {
    return user;
  }

  public void setUser(UserInfo user) {
    this.user = user;
  }

  public UserInfo getLoader() {
    return loader;
  }

  public void setLoader(UserInfo loader) {
    this.loader = loader;
  }

  public UserInfo getWorker() {
    return worker;
  }

  public void setWorker(UserInfo worker) {
    this.worker = worker;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Integer getReasonType() {
    return reasonType;
  }

  public void setReasonType(Integer reasonType) {
    this.reasonType = reasonType;
  }

  public Integer getActualTime() {
    return actualTime;
  }

  public void setActualTime(Integer actualTime) {
    this.actualTime = actualTime;
  }

  public Integer getBalanceTime() {
    return balanceTime;
  }

  public void setBalanceTime(Integer balanceTime) {
    this.balanceTime = balanceTime;
  }

}
