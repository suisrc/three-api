package net.icgear.product.att2005.att.repo.impl;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

import net.icgear.im.core.persistence.repo.AbstractRepository;
import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.SyncLog;
import net.icgear.product.att2005.att.repo.api.SyncLogRepository;
import net.icgear.product.att2005.att.rsapi.SyncLogRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 * 
 */
@ApplicationScoped
@Repository(forEntity=SyncLog.class)
public abstract class SyncLogRepositoryImpl  extends AbstractRepository<SyncLog, Integer> 
        implements  CriteriaSupport<SyncLog>, SyncLogRepository, SuperRepository<SyncLog, Integer>,
        SyncLogRsApi, SuperRsApi<SyncLog, Integer> {
}
