package net.icgear.product.att2005.trest.dto;

import io.swagger.annotations.ApiModel;

/**
 * 变更定时器同步
 * @author Y13
 *
 */
@ApiModel("ConfigBody")
public class ConfigBody {
    
    private Integer id;
    
    private String name;
    
    private String description;
    
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
