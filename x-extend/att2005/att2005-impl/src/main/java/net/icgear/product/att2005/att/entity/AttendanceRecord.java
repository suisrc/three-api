package net.icgear.product.att2005.att.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import net.icgear.im.core.persistence.IA;

/**
 * 考勤原始记录
 * 
 * 同步与考勤机
 * 
 * @author Y13
 *
 */
@Entity
@Table(name = "record")
public class AttendanceRecord implements IA<Integer>  {
    private static final long serialVersionUID = 7384155786417912691L;

    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 10)
    private Integer id;
    
    // @Column(name = "su_user_id")
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "sn_user_id", referencedColumnName = "sn_user_id")
    private UserInfo user;
    
    @Column(name = "sn_check_type")
    private Integer type;
    
    @Column(name = "sn_in_out_time")
    private Timestamp time;
    
    @Column(name = "log_id")
    private Integer logId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public Integer getLogId() {
      return logId;
    }

    public void setLogId(Integer logId) {
      this.logId = logId;
    }

    @Override
    public Integer getVersion() {
        return IA.NOMER_VERSION;
    }
    
}
