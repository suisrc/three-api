package net.icgear.product.att2005.att.service.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.im.core.persistence.service.AbstractService;
import net.icgear.im.core.persistence.service.RepoHandler;
import net.icgear.im.core.persistence.service.Service;
import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.SyncLog;
import net.icgear.product.att2005.att.repo.api.SyncLogRepository;
import net.icgear.product.att2005.att.rsapi.SyncLogRsApi;
import net.icgear.product.att2005.att.service.api.SyncLogService;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 *
 */
@Service
@ApplicationScoped
public abstract class SyncLogServiceImpl extends AbstractService<SyncLog, Integer> 
        implements SyncLogService, SuperService<SyncLog, Integer>, SyncLogRsApi, SuperRsApi<SyncLog, Integer> {
    
    @Inject
    @RepoHandler
    private SyncLogRepository repo;

}
