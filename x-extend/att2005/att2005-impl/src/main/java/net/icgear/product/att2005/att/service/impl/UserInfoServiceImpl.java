package net.icgear.product.att2005.att.service.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.im.core.persistence.service.AbstractService;
import net.icgear.im.core.persistence.service.RepoHandler;
import net.icgear.im.core.persistence.service.Service;
import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.repo.api.UserInfoRepository;
import net.icgear.product.att2005.att.rsapi.UserInfoRsApi;
import net.icgear.product.att2005.att.service.api.UserInfoService;

/**
 * build by tools[0.1-alpha] on Thu Jul 19 16:50:48 CST 2018
 * @author Y13-Tools(auto)
 *
 */
@Service
@ApplicationScoped
public abstract class UserInfoServiceImpl extends AbstractService<UserInfo, Integer> 
        implements UserInfoService, SuperService<UserInfo, Integer>, UserInfoRsApi, SuperRsApi<UserInfo, Integer> {
    
    @Inject
    @RepoHandler
    private UserInfoRepository repo;

}
