package net.icgear.product.att2005.att.service.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.im.core.persistence.service.AbstractService;
import net.icgear.im.core.persistence.service.RepoHandler;
import net.icgear.im.core.persistence.service.Service;
import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.SyncConfig;
import net.icgear.product.att2005.att.repo.api.SyncConfigRepository;
import net.icgear.product.att2005.att.rsapi.SyncConfigRsApi;
import net.icgear.product.att2005.att.service.api.SyncConfigService;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * 
 * @author Y13-Tools(auto)
 *
 */
@Service
@ApplicationScoped
public abstract class SyncConfigServiceImpl extends AbstractService<SyncConfig, Integer>
        implements SyncConfigService, SuperService<SyncConfig, Integer>, SyncConfigRsApi, SuperRsApi<SyncConfig, Integer> {

    @Inject
    @RepoHandler
    private SyncConfigRepository repo;

    /**
     * 执行配置信息，挂载
     */
    @Override
    public void execute(SyncConfig sc) {
        
    }
}
