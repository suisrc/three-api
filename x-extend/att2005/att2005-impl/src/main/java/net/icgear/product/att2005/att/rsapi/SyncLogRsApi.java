package net.icgear.product.att2005.att.rsapi;

import java.sql.Timestamp;
import java.util.List;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.SyncLog;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface SyncLogRsApi extends SuperRsApi<SyncLog, Integer> {

  /**
   * 
   * @param beginTime
   * @param desc
   * @return
   */
  List<SyncLog> findByBeginTimeAndDescription(Timestamp beginTime, String desc);
}