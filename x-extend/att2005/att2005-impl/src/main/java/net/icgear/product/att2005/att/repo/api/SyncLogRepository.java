package net.icgear.product.att2005.att.repo.api;

import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.product.att2005.att.entity.SyncLog;
import net.icgear.product.att2005.att.rsapi.SyncLogRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface SyncLogRepository extends SuperRepository<SyncLog, Integer>, SyncLogRsApi {
}
