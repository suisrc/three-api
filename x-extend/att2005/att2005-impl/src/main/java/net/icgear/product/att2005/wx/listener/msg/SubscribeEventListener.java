package net.icgear.product.att2005.wx.listener.msg;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.suisrc.core.Global;
import com.suisrc.core.message.ECM;
import com.suisrc.core.reference.RefVal;
import com.suisrc.jaxrsapi.core.ApiActivator;
import com.suisrc.three.core.listener.inf.Listener;

import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.service.api.UserInfoService;
import net.icgear.product.att2005.xframe.iface.ProxyService;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.event.SubscribeEvent;
import net.icgear.three.weixin.core.rest.AbstractWxBindingOne;
import net.icgear.three.weixin.qyapi.rest.api.UserManagerRest;
import net.icgear.three.weixin.qyapi.rest.dto.user.UserResult;

/**
 * <p> 把关注的人员增加到考勤系统中
 * @author Y13
 *
 */
@ApplicationScoped
public class SubscribeEventListener implements Listener<SubscribeEvent> {
  
  @Inject
  private UserInfoService userService;
  @Inject
  private ProxyService proxyService;

  @Override
  public boolean accept(RefVal<Object> result, SubscribeEvent bean) {
    result.set(WxConsts.SUCCESS);
    return false;
  }

  @Override
  public boolean accept(RefVal<Object> result, SubscribeEvent bean, Object owner) {
    try {
      AbstractWxBindingOne one = (AbstractWxBindingOne) owner;
      if (bean.getToUserName().equals(one.getConfig().getWxAppId())) {
        // 本公司人员关注
        ApiActivator activator = (ApiActivator) one.getConfig();
        UserManagerRest userMngRest = activator.getApiImplement(UserManagerRest.class);
        UserResult rs = userMngRest.getUser(bean.getFromUserName());
        String msg = "同步账户信息：";
        if (!rs.isErr()) {
          // 数据发生异常
          String openid = bean.getToUserName() + ":" + bean.getFromUserName();
          boolean res = syncUserInfo(bean.getToUserName(), openid, rs);
          msg += String.format("openid: %s, 已完成关注用户【%s】电话号码“%s”的个人信息的操作。操作：%s", openid, rs.getName(), rs.getMobile(),
              res ? "同步成功" : "同步失败");
        } else {
          msg += "无法从微信服务器获取用户信息：" + rs.getErrmsg();
        }
        Global.getLogger().info(msg);
      }
    } catch (Exception e) {
      e.printStackTrace();
      // 不要影响系统正常运行
    }
    return accept(result, bean);
  }

  /**
   * 
   * @param openid
   * @param user
   * @return
   */
  private boolean syncUserInfo(String corpid, String openid, UserResult userRs) {
    return proxyService.executeST(userRs, rs -> {
      UserInfo user = userService.findOptionalByPhone(userRs.getMobile());
      if (user == null) {
        user = userService.findOptionalByName(userRs.getName());
      }
      if (user == null) {
        // logger.info("没有找到考勤人员：" + userRs.getName());
        // return false;
        throw ECM.exception(-11, "公司内部无法找到您，请确认你的名字是否正确：" + userRs.getName());
      }
      user.setWxOpenId(openid);
      user.setWxCorpId(corpid);
      user.setWxUserId(rs.getUserid());
      user.setWxUserName(rs.getName());
      user.setPhone(rs.getMobile());
      return true;
    });

  }
}
