package net.icgear.product.att2005.att.rsapi;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.SystemInfo;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:42 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface SystemInfoRsApi extends SuperRsApi<SystemInfo, String> {

}