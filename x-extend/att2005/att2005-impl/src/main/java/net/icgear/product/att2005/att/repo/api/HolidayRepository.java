package net.icgear.product.att2005.att.repo.api;

import java.sql.Timestamp;

import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.product.att2005.att.entity.Holiday;
import net.icgear.product.att2005.att.rsapi.HolidayRsApi;

/**
 * build by tools[0.1-alpha] on Tue Sep 25 14:13:12 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface HolidayRepository extends SuperRepository<Holiday, Timestamp>, HolidayRsApi {
}
