package net.icgear.product.att2005.att.repo.impl;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

import net.icgear.im.core.persistence.repo.AbstractRepository;
import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.A_TestBean;
import net.icgear.product.att2005.att.repo.api.A_TestBeanRepository;
import net.icgear.product.att2005.att.rsapi.A_TestBeanRsApi;

/**
 * build by tools[0.1-alpha] on Tue Nov 06 13:58:38 CST 2018
 * @author Y13-Tools(auto)
 * 
 */
@ApplicationScoped
@Repository(forEntity=A_TestBean.class)
public abstract class A_TestBeanRepositoryImpl  extends AbstractRepository<A_TestBean, Integer> 
        implements  CriteriaSupport<A_TestBean>, A_TestBeanRepository, SuperRepository<A_TestBean, Integer>,
        A_TestBeanRsApi, SuperRsApi<A_TestBean, Integer> {
}
