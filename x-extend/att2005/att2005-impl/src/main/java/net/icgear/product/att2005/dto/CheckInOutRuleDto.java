package net.icgear.product.att2005.dto;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.function.Function;

/**
 * 考勤规则使用的DTO
 * @author Y13
 *
 */
public class CheckInOutRuleDto {
    
    private int worktime;
    
    private Time lunchBreakStartTime;
    
    private Time lunchBreakEndTime;
    
    private Time nigthBreakStartTime;
    
    private Time nigthBreakEndTime;
    
    private Time onWorktime;
    
    private Time offWorktime;
    
    private Function<Timestamp, Integer> holidayHandler;

    
    public int getWorktime() {
        return worktime;
    }

    public void setWorktime(int worktime) {
        this.worktime = worktime;
    }

    public Time getLunchBreakStartTime() {
        return lunchBreakStartTime;
    }

    public void setLunchBreakStartTime(Time lunchBreakStartTime) {
        this.lunchBreakStartTime = lunchBreakStartTime;
    }

    public Time getLunchBreakEndTime() {
        return lunchBreakEndTime;
    }

    public void setLunchBreakEndTime(Time lunchBreakEndTime) {
        this.lunchBreakEndTime = lunchBreakEndTime;
    }

    public Time getNigthBreakStartTime() {
        return nigthBreakStartTime;
    }

    public void setNigthBreakStartTime(Time nigthBreakStartTime) {
        this.nigthBreakStartTime = nigthBreakStartTime;
    }

    public Time getNigthBreakEndTime() {
        return nigthBreakEndTime;
    }

    public void setNigthBreakEndTime(Time nigthBreakEndTime) {
        this.nigthBreakEndTime = nigthBreakEndTime;
    }

    public void setHolidayHander(Function<Timestamp, Integer> func) {
        this.holidayHandler = func;
    }
    
    /**
     * 获取节日标识
     */
    public Integer getHolidayType(Timestamp time) {
        return holidayHandler.apply(time);
    }

    public Time getOnWorktime() {
        return onWorktime;
    }

    public void setOnWorktime(Time onWorktime) {
        this.onWorktime = onWorktime;
    }

    public Time getOffWorktime() {
        return offWorktime;
    }

    public void setOffWorktime(Time offWorktime) {
        this.offWorktime = offWorktime;
    }

}
