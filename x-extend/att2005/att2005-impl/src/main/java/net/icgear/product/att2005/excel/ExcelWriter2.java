package net.icgear.product.att2005.excel;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.List;

import com.suisrc.core.Global;
import com.suisrc.core.utils.FileUtils;

import net.icgear.im.core.file.FileManager;
import net.icgear.im.ext.excel.IRowBeanParser;
import net.icgear.im.ext.excel.response.ExcelWriterResponse;

public class ExcelWriter2<T> extends ExcelWriterResponse<T> {
  private static final String TMP_FD_KEY = "application.config.excel.folder.temp";

  /**
   * 创建一个Excel写出对象
   * 
   * @param clazz
   * @return
   */
  public static <T> ExcelWriter2<T> EWR(Class<T> clazz) {
    return new ExcelWriter2<T>(clazz);
  }

  public ExcelWriter2(Class<T> beanType) {
    super(beanType);
  }

  public ExcelWriter2(IRowBeanParser<T> parser) {
    super(parser);
  }

  /**
   * 获取处理结果，缓存文件 把workbook存到缓存中，然后返回缓存的路径
   * 
   * @param beans
   * @param workbook
   * @param sheetname
   * @return String null： 导出失败
   */
  public String getExcelDownloadUri(String filename, long expiredIn) {
    // ----------------构建文件存储的位置
    String folderStr = System.getProperty(TMP_FD_KEY, "download/");
    SimpleDateFormat SDF = new SimpleDateFormat("yyMMddHHmmssSSS");
    String time = SDF.format(System.currentTimeMillis());
    String offset = String.valueOf(Thread.currentThread().getId());
    String file = filename + "_" + time + "_" + offset + ".xlsx";
    String filePath = FileUtils.getPath(folderStr, file);
    if (getFileManager().exists(filePath)) {
      Global.getLogger().info("导出Excel失败，无法创建导出地址：" + filePath);
      return null;
    }
    // -------------------------生成可用缓存文件-------------------------------------
    try {
      OutputStream os = getFileManager().getFileOutputStream(filePath);
      getWorkbook().write(os);
      os.close();
      String download = getFileManager().getResourceIndex(filePath, expiredIn);
      return download;
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }

  private FileManager getFileManager() {
    return FileManager.getDefault();
  }

  /**
   * 获取下载对象
   * 
   * @param filename
   * @param module
   * @param sheetname
   * @param beans
   * @return
   */
  public String process(long expiredIn, String filename, String module, String sheetname, List<T> beans) {
    initWorkBook(module);
    processSheet(sheetname, beans);
    String fileUri = getExcelDownloadUri(filename, expiredIn);
    freedWorkbook();
    return fileUri;
  }

  /**
   * 获取下载对象
   * 
   * @param filename
   * @param sheetname
   * @param beans
   * @return
   */
  public String process(long expiredIn, String filename, String sheetname, List<T> beans) {
    return process(expiredIn, filename, null, sheetname, beans);
  }
}
