package net.icgear.product.att2005.att.repo.impl;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

import net.icgear.im.core.persistence.repo.AbstractRepository;
import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.repo.api.UserInfoRepository;
import net.icgear.product.att2005.att.rsapi.UserInfoRsApi;

/**
 * build by tools[0.1-alpha] on Thu Jul 19 16:50:48 CST 2018
 * @author Y13-Tools(auto)
 * 
 */
@ApplicationScoped
@Repository(forEntity=UserInfo.class)
public abstract class UserInfoRepositoryImpl  extends AbstractRepository<UserInfo, Integer> 
        implements  CriteriaSupport<UserInfo>, UserInfoRepository, SuperRepository<UserInfo, Integer>,
        UserInfoRsApi, SuperRsApi<UserInfo, Integer> {
}
