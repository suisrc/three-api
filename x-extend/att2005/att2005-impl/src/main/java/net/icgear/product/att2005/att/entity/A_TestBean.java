package net.icgear.product.att2005.att.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import net.icgear.im.core.persistence.IA;

/**
 * 
 * <p> 则是一个测试Bean
 * 
 * <p> 当该Bean结构建立好后，可以通过net.icgear.im.tools.m构建repository和service层的内容
 * 
 * @author Y13
 *
 */
@Entity
@Table(name = "tbl_test")
public class A_TestBean implements IA<Integer> {
  private static final long serialVersionUID = 1L;

  /**
   * <p> id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", length = 10)
  private Integer id;


  /**
   * <p> name
   */
  private String name;
  
  /**
   * <p> first name
   */
  @Column(name = "first_name")
  private String firstName;
  
  /**
   * <p> 数据版本号
   */
  @Version
  private Integer version;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }
  
}
