package net.icgear.product.att2005.att.repo.api;

import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.product.att2005.att.entity.A_TestBean;
import net.icgear.product.att2005.att.rsapi.A_TestBeanRsApi;

/**
 * build by tools[0.1-alpha] on Tue Nov 06 13:58:38 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface A_TestBeanRepository extends SuperRepository<A_TestBean, Integer>, A_TestBeanRsApi {
}
