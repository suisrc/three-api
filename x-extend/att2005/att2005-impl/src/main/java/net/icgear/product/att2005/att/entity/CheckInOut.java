package net.icgear.product.att2005.att.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import net.icgear.im.core.persistence.IA;

/**
 * 考勤记录
 * 
 * @author Y13
 *
 */
@Entity
@Table(name = "check_in_out")
public class CheckInOut implements IA<Integer>  {
    private static final long serialVersionUID = 1L;
    
    public static enum TYPE {
        // 0正常上班，1正常休息， 2加班，3换休，4休假，5病假，6事假，7缺勤，8带薪休假
        WORKDAY("上班"), OFFDAY("休息"), OVERTIME("加班"), CHANGEDAY("换休"), 
        VACATION("休假"), SICKLEAVE("病假"), LEAVE("事假"), ABSENCE("缺勤"), PAIDLEAVE("带薪休假"),
        ATTERR("出勤异常"), OTHER("其他原因");
        
        public final String desc;
        private TYPE(String desc) {
            this.desc = desc;
        }
    }

    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 10)
    private Integer id;
    
    /**
     * <p> 日期
     */
    @Column(name = "att_date")
    private Timestamp attDate;
    
    /**
     * <p> 考勤状态
     */
    private Integer status;
    
    /**
     * <p> 考勤类型
     */
    @Column(name = "att_type")
    private Integer attType;
    
    /**
     * <p> 理论工作时间
     */
    @Column(name = "work_time")
    private Integer workTime;
    
    /**
     * <p> 实际工作时间
     */
    @Column(name = "actual_time")
    private Integer actualTime;
    
    /**
     * <p> 出勤时间
     */
    @Column(name = "check_in")
    private Timestamp checkIn;
    
    /**
     * <p> 退勤时间
     */
    @Column(name = "check_out")
    private Timestamp checkOut;
    
    /**
     * <p> 出勤类型
     */
    @Column(name = "check_in_type")
    private Integer checkInType;
    
    /**
     * <p> 退勤类型
     */
    @Column(name = "check_out_type")
    private Integer checkOutType;
    
    /**
     * <p> 时间有效结余
     * 
     * <br>正常工作为0 
     * <br>加班为正
     * <br>缺勤为负
     */
    @Column(name = "balance_time")
    private Integer balanceTime;
    
    /**
     * <p> 项目组
     */
    private String program;
    
    /**
     * <p> 领导
     */
    @ManyToOne
    @JoinColumn(name = "loader_id")
    private UserInfo loader;
    
    /**
     * <p> 用户
     */
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserInfo user;
    
    /**
     * <p> 日志id
     */
    @Column(name = "log_id")
    private Integer logId;
    
    /**
     * <p> 申请记录，正常情况下最多只有一个
     */
    @OneToMany(mappedBy = "checkInOut", cascade = CascadeType.ALL)
    private List<ApplyRecord> applyRecords;
    
    @Version
    private Integer version;

    @Override
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public Integer getVersion() {
        return version;
    }
    
    public void setVersion(Integer version) {
        this.version = version;
    }

    public Timestamp getAttDate() {
        return attDate;
    }

    public void setAttDate(Timestamp attDate) {
        this.attDate = attDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getAttType() {
        return attType;
    }

    public void setAttType(Integer attType) {
        this.attType = attType;
    }

    public Integer getWorkTime() {
        return workTime;
    }

    public void setWorkTime(Integer workTime) {
        this.workTime = workTime;
    }

    public Integer getActualTime() {
        return actualTime;
    }

    public void setActualTime(Integer actualTime) {
        this.actualTime = actualTime;
    }

    public Timestamp getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(Timestamp checkIn) {
        this.checkIn = checkIn;
    }

    public Timestamp getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(Timestamp checkOut) {
        this.checkOut = checkOut;
    }

    public Integer getCheckInType() {
        return checkInType;
    }

    public void setCheckInType(Integer checkInType) {
        this.checkInType = checkInType;
    }

    public Integer getCheckOutType() {
        return checkOutType;
    }

    public void setCheckOutType(Integer checkOutType) {
        this.checkOutType = checkOutType;
    }

    public Integer getBalanceTime() {
        return balanceTime;
    }

    public void setBalanceTime(Integer balanceTime) {
        this.balanceTime = balanceTime;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public UserInfo getLoader() {
        return loader;
    }

    public void setLoader(UserInfo loader) {
        this.loader = loader;
    }

    public UserInfo getUser() {
        return user;
    }

    public void setUser(UserInfo user) {
        this.user = user;
    }

    public List<ApplyRecord> getApplyRecords() {
      return applyRecords;
    }

    public void setApplyRecords(List<ApplyRecord> applyRecords) {
      this.applyRecords = applyRecords;
    }

    public Integer getLogId() {
      return logId;
    }

    public void setLogId(Integer logId) {
      this.logId = logId;
    }
    
}
