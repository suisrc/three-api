package net.icgear.product.att2005.att.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import net.icgear.im.core.persistence.IA;

/**
 * 店长月报表
 * 
 * @author user
 */
@Entity
@Table(name = "user_info")
public class UserInfo implements IA<Integer> {
  private static final long serialVersionUID = 1L;

  /**
   * id
   */
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", length = 10)
  private Integer id;

  /**
   * 姓名
   */
  private String name;

  /**
   * 电话
   */
  private String phone;

  /**
   * 考勤名称
   */
  @Column(name = "sn_name")
  private String snName;

  /**
   * 考勤ID
   */
  @Column(name = "sn_user_id")
  private Integer snId;

  /**
   * 考勤编号
   */
  @Column(name = "sn_number")
  private String snNo;

  /**
   * 微信公司id
   */
  @Column(name = "wx_corp_id")
  private String wxCorpId;

  /**
   * 微信部门id
   */
  @Column(name = "wx_party_id")
  private String wxPartyId;

  /**
   * 微信用户id
   */
  @Column(name = "wx_user_id")
  private String wxUserId;

  /**
   * 微信openid
   */
  @Column(name = "wx_open_id")
  private String wxOpenId;

  /**
   * 微信公司名称
   */
  @Column(name = "wx_corp_name")
  private String wxCorpName;

  /**
   * 微信部门名称
   */
  @Column(name = "wx_party_name")
  private String wxPartyName;

  /**
   * 微信用户名
   */
  @Column(name = "wx_user_name")
  private String wxUserName;

  /**
   * 直属领导
   */
  @ManyToOne
  @JoinColumn(name = "loader")
  private UserInfo loader;

  /**
   * 年休
   */
  @Column(name = "annual_leave")
  private Integer annualLeave;

  /**
   * 换休
   */
  @Column(name = "change_leave")
  private Integer changeLeave;

  /**
   * 是否为领导 只有领导才有修改项目组权限
   */
  @Column(name = "is_loader")
  private Boolean isLoader;

  /**
   * 项目组
   */
  private String program;
  
  /**
   * <p> 工作邮箱
   */
  private String email;

  /**
   * 是否为管理员
   */
  @Column(name = "is_admin")
  private Boolean isAdmin;

  /**
   * <p> 是否发送微信消息
   */
  @Column(name = "wx_send_msg")
  private Boolean isWxSendMsg;

  /**
   * 数据版本号
   */
  @Version
  private Integer version;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getSnName() {
    return snName;
  }

  public void setSnName(String snName) {
    this.snName = snName;
  }

  public Integer getSnId() {
    return snId;
  }

  public void setSnId(Integer snId) {
    this.snId = snId;
  }

  public String getSnNo() {
    return snNo;
  }

  public void setSnNo(String snNo) {
    this.snNo = snNo;
  }

  public String getWxCorpId() {
    return wxCorpId;
  }

  public void setWxCorpId(String wxCorpId) {
    this.wxCorpId = wxCorpId;
  }

  public String getWxPartyId() {
    return wxPartyId;
  }

  public void setWxPartyId(String wxPartyId) {
    this.wxPartyId = wxPartyId;
  }

  public String getWxUserId() {
    return wxUserId;
  }

  public void setWxUserId(String wxUserId) {
    this.wxUserId = wxUserId;
  }

  public String getWxOpenId() {
    return wxOpenId;
  }

  public void setWxOpenId(String wxOpenId) {
    this.wxOpenId = wxOpenId;
  }

  public String getWxCorpName() {
    return wxCorpName;
  }

  public void setWxCorpName(String wxCorpName) {
    this.wxCorpName = wxCorpName;
  }

  public String getWxPartyName() {
    return wxPartyName;
  }

  public void setWxPartyName(String wxPartyName) {
    this.wxPartyName = wxPartyName;
  }

  public String getWxUserName() {
    return wxUserName;
  }

  public void setWxUserName(String wxUserName) {
    this.wxUserName = wxUserName;
  }

  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public UserInfo getLoader() {
    return loader;
  }

  public void setLoader(UserInfo loader) {
    this.loader = loader;
  }

  public Integer getAnnualLeave() {
    return annualLeave;
  }

  public void setAnnualLeave(Integer annualLeave) {
    this.annualLeave = annualLeave;
  }

  public Integer getChangeLeave() {
    return changeLeave;
  }

  public void setChangeLeave(Integer changeLeave) {
    this.changeLeave = changeLeave;
  }

  public Boolean getIsLoader() {
    return isLoader;
  }

  public void setIsLoader(Boolean isLoader) {
    this.isLoader = isLoader;
  }

  public String getProgram() {
    return program;
  }

  public void setProgram(String program) {
    this.program = program;
  }

  public Boolean getIsAdmin() {
    return isAdmin;
  }

  public void setIsAdmin(Boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  public boolean isLoader() {
    return isLoader != null && isLoader;
  }

  public boolean isAdmin() {
    return isAdmin != null && isAdmin;
  }

  public Boolean getIsWxSendMsg() {
    return isWxSendMsg;
  }

  public void setIsWxSendMsg(Boolean isWxSendMsg) {
    this.isWxSendMsg = isWxSendMsg;
  }

  public boolean isWxSendMsg() {
    return isWxSendMsg == null || isWxSendMsg;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }
  
}
