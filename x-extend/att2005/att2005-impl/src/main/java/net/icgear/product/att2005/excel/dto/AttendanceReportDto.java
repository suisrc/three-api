package net.icgear.product.att2005.excel.dto;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import net.icgear.im.common.bean.OriginIgnore;
import net.icgear.im.common.bean.OriginMethod;
import net.icgear.im.common.bean.OriginType;
import net.icgear.im.ext.excel.ExcelColumn;
import net.icgear.product.att2005.att.entity.CheckInOut;

/**
 * 考勤报表使用
 * 
 * @author Y13
 *
 */
@OriginType(clazz = CheckInOut.class, e2p = false)
public class AttendanceReportDto {

  private Integer id;

  @ExcelColumn(column = "A", title = "项目组", width = 12)
  private String program;
  
  @ExcelColumn(column="B", title = "团队负责人", width = 14)
  private String loader;

  @ExcelColumn(column = "C", title = "姓名", width = 10)
  private String name;

  @ExcelColumn(column = "D", title = "日期", get = "getDateStr", width = 12)
  private Timestamp date;

  public String getDateStr() {
    if (date == null) {
      return null;
    }
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    return sdf.format(date);
  }

  //0x370xa
  @ExcelColumn(column = "E", title = "签到时间", get = "getCheckInStr", width = 12, color=0x37)
  private Timestamp checkIn;

  public String getCheckInStr() {
    return getTimeStr(checkIn);
  }

  @ExcelColumn(column = "F", title = "签退时间", get = "getCheckOutStr", width = 12, color=0x37)
  private Timestamp checkOut;

  public String getCheckOutStr() {
    return getTimeStr(checkOut);
  }

  @ExcelColumn(column = "G", title = "有效签到", get = "getCheckIn2Str", width = 12)
  private Timestamp checkIn2;

  public String getCheckIn2Str() {
    return getTimeStr(checkIn2);
  }

  @ExcelColumn(column = "H", title = "有效签退", get = "getCheckOut2Str", width = 12)
  private Timestamp checkOut2;

  public String getCheckOut2Str() {
    return getTimeStr(checkOut2);
  }

  @ExcelColumn(column = "I", title = "应到工时", get = "getWorkTimeByHour", width = 12, color=0x37)
  private Integer workTime;

  public String getWorkTimeByHour() {
    return getHourStr(workTime);
  }

  @ExcelColumn(column = "J", title = "有效工时", get = "getActualTimeByHour", width = 12)
  private Integer actualTime;

  public String getActualTimeByHour() {
    return getHourStr(actualTime);
  }

  @ExcelColumn(column = "K", title = "工时结余", get = "getBalanceTimeByHour", width = 12)
  private Integer balanceTime;

  public String getBalanceTimeByHour() {
    return getHourStr(balanceTime);
  }

  @ExcelColumn(column = "L", title = "考勤类型", get = "getAttTypeStr", valueType = String.class)
  private Integer attType;

  public String getAttTypeStr() {
    if (attType == null) {
      return null;
    }
    return CheckInOut.TYPE.values()[attType].desc;
  }

  @ExcelColumn(column = "M", title = "备注", get = "getStatusStr", valueType = String.class)
  private Integer status;

  public String getStatusStr() {
    if (status == 1) {
      return "待处理";
    } else if (status == 10) {
      return "已处理";
    } else if (status == 11) {
      return "正在处理";
    }else {
      return null;
    }
  }
  
  @ExcelColumn(column="N", title = "异常原因", width = 20)
  private String reason;
  
  @ExcelColumn(column="O", title = "负责人回复", width = 20)
  private String comment;

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public String getProgram() {
    return program;
  }

  public void setProgram(String program) {
    this.program = program;
  }

  public String getName() {
    return name;
  }

  @OriginMethod("getUser.getName")
  public void setName(String name) {
    this.name = name;
  }

  public Timestamp getDate() {
    return date;
  }

  @OriginMethod("getAttDate")
  public void setDate(Timestamp date) {
    this.date = date;
  }

  public Timestamp getCheckIn() {
    return checkIn;
  }

  public void setCheckIn(Timestamp checkIn) {
    this.checkIn = checkIn;
  }

  public Timestamp getCheckOut() {
    return checkOut;
  }

  public void setCheckOut(Timestamp checkOut) {
    this.checkOut = checkOut;
  }

  public Timestamp getCheckIn2() {
    return checkIn2;
  }

  @OriginMethod("getCheckIn")
  public void setCheckIn2(Timestamp checkIn2) {
    this.checkIn2 = checkIn2;
  }

  public Timestamp getCheckOut2() {
    return checkOut2;
  }

  @OriginMethod("getCheckOut")
  public void setCheckOut2(Timestamp checkOut2) {
    this.checkOut2 = checkOut2;
  }

  public Integer getWorkTime() {
    return workTime;
  }

  public void setWorkTime(Integer workTime) {
    this.workTime = workTime;
  }

  public Integer getActualTime() {
    return actualTime;
  }

  public void setActualTime(Integer actualTime) {
    this.actualTime = actualTime;
  }

  public Integer getBalanceTime() {
    return balanceTime;
  }

  public void setBalanceTime(Integer balanceTime) {
    this.balanceTime = balanceTime;
  }

  public Integer getAttType() {
    return attType;
  }

  public void setAttType(Integer attType) {
    this.attType = attType;
  }

  public String getTimeStr(Timestamp time) {
    if (time == null) {
      return null;
    }
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    return sdf.format(time);
  }

  public String getHourStr(Number n) {
    if (n == null) {
      return null;
    }
    BigDecimal bg = new BigDecimal(n.doubleValue() / 3600).setScale(1, RoundingMode.HALF_UP);
    return bg.toPlainString();
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getReason() {
    return reason;
  }
  @OriginIgnore
  public void setReason(String reason) {
    this.reason = reason;
  }

  public String getComment() {
    return comment;
  }

  @OriginIgnore
  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getLoader() {
    return loader;
  }

  @OriginMethod("getLoader.getName")
  public void setLoader(String loader) {
    this.loader = loader;
  }

}
