package net.icgear.product.att2005.att.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import net.icgear.im.core.persistence.IA;

@Entity
@Table(name = "holiday")
public class Holiday implements IA<Timestamp> {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "id_date")
    private Timestamp id;
    
    private Integer type;
    
    /**
     * 0无效，1有效
     */
    private Integer status;
    
    @ManyToOne
    @JoinColumn(name = "updater")
    private UserInfo updater;
    
    private String description;
    
    private Integer version;

    @Override
    public Timestamp getId() {
        return id;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public UserInfo getUpdater() {
        return updater;
    }

    public void setUpdater(UserInfo updater) {
        this.updater = updater;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(Timestamp id) {
        this.id = id;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

}
