package net.icgear.product.att2005.att.repo.impl;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

import net.icgear.im.core.persistence.repo.AbstractRepository;
import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.AttendanceRecord;
import net.icgear.product.att2005.att.repo.api.AttendanceRecordRepository;
import net.icgear.product.att2005.att.rsapi.AttendanceRecordRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 14:19:10 CST 2018
 * @author Y13-Tools(auto)
 * 
 */
@ApplicationScoped
@Repository(forEntity=AttendanceRecord.class)
public abstract class AttendanceRecordRepositoryImpl  extends AbstractRepository<AttendanceRecord, Integer> 
        implements  CriteriaSupport<AttendanceRecord>, AttendanceRecordRepository, SuperRepository<AttendanceRecord, Integer>,
        AttendanceRecordRsApi, SuperRsApi<AttendanceRecord, Integer> {
}
