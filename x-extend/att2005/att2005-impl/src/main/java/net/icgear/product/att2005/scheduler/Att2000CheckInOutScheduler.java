package net.icgear.product.att2005.scheduler;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.cdise.api.ContextControl;

import com.suisrc.core.Global;
import com.suisrc.core.corn.ICron;
import com.suisrc.core.scheduler.ScheduleHandler;
import com.suisrc.core.scheduler.Scheduled;
import com.suisrc.core.service.AbstractScheduledService.Schedule;

import net.icgear.product.att2005.att.entity.SyncConfig;
import net.icgear.product.att2005.att.service.api.CheckInOutService;
import net.icgear.product.att2005.att.service.api.SyncConfigService;
import net.icgear.product.att2005.xframe.iface.ProxyService;

/**
 * <p> 定时统计考勤记录和内容 <p> 默认希望每天6点指定发送
 * 
 * 当前定时器被放弃
 * 
 * @author Y13
 *
 */
@Scheduled(delayf = true, onStartupKey = "application.schedule.config.valid")
@ApplicationScoped
public class Att2000CheckInOutScheduler implements ScheduleHandler {

  @Inject
  private ContextControl control;

  @Inject
  private SyncConfigService configService;
  @Inject
  private CheckInOutService checkService;
  @Inject
  private ProxyService proxyService;

  /**
   * 执行作业内容
   */
  @Override
  public void run() {
    try {
      control.startContext(RequestScoped.class);
      Global.getLogger().info("开始执行考勤统计002");
      proxyService.executeRT(this::runIteration);
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      Global.getLogger().info("开始执行考勤统计002");
      control.stopContext(RequestScoped.class);
    }
  }

  /**
   * 执行同步操作
   */
  private void runIteration() {
    // 获取配置信息
    List<SyncConfig> configs = configService.findByStatus(0);
    if (configs.isEmpty()) {
      return; // 无法计算
    }
    SyncConfig sc = configs.get(0); // 支取第一个配置信息
    // 统计考勤数据
    LocalDate nowDate = LocalDate.now();
    Timestamp checkSTime = sc.getCheckInOut();
    if (checkSTime == null) {
      checkSTime = Timestamp.valueOf(nowDate.minusDays(1).atStartOfDay());
    }
    Timestamp checkETime = Timestamp.valueOf(nowDate.atStartOfDay());
    
    if (checkETime.getTime() - checkSTime.getTime() > 23 * 60 * 60 * 1000) {
      // 时间差必须保证1天，由于时间统计有毫秒误差，所以这里统计大于23小时，就认为大于等于1天
      checkService.syncCheckInOut(checkSTime, checkETime, "系统考勤数据计算-定时器001");
      sc.setCheckInOut(checkETime);
    }
    // 考勤记录
    sc.setExecCount(sc.getExecCount() + 1);
  }

  @Override
  public Long getNextDelay(Schedule pre) {
    try {
      control.startContext(RequestScoped.class);
      Long next = getNextExecTime();
      if (next == null) {
        return null;
      }
      return (next - System.currentTimeMillis()) / 1000;
    } finally {
      control.stopContext(RequestScoped.class);
    }
  }

  /**
   * <p> 同步执行时间
   * 
   * @return
   */
  public Long getNextExecTime() {
    List<SyncConfig> configs = configService.findByStatus(0);
    if (configs.isEmpty()) {
      return null; // 无法计算
    }
    SyncConfig sc = configs.get(0); // 支取第一个配置信息
    if (sc.getExecCron() == null) {
      return null;
    }
    String cron = sc.getExecCron();
    if (cron == null || cron.isEmpty()) {
      return null;
    }
    try {
      Timestamp now = new Timestamp(System.currentTimeMillis());
      ICron icron = ICron.build(cron);
      long time = icron.getTimeAfter(now).getTime();
      sc.setNextTime(new Timestamp(time));
      configService.merge2(sc);
      return time;
    } catch (ParseException e) {
      e.printStackTrace();
      return null;
    }
  }
}
