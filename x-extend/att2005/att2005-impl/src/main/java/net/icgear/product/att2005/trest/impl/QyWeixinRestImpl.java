package net.icgear.product.att2005.trest.impl;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.suisrc.core.message.ECM;
import com.suisrc.core.message.ErrCode;

import net.icgear.im.common.bean.BCU;
import net.icgear.im.core.persistence.IA;
import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.entity.CheckInOut;
import net.icgear.product.att2005.att.entity.CheckInOut.TYPE;
import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.service.api.ApplyRecordService;
import net.icgear.product.att2005.att.service.api.CheckInOutService;
import net.icgear.product.att2005.att.service.api.UserInfoService;
import net.icgear.product.att2005.trest.api.QyWeixinRest;
import net.icgear.product.att2005.trest.dto.ApplyInfoDto;
import net.icgear.product.att2005.trest.dto.ApprovalInfoDto;
import net.icgear.product.att2005.trest.dto.LoaderInfoDto;
import net.icgear.product.att2005.trest.dto.LoaderInfoDto.LoaderDto;
import net.icgear.product.att2005.wx.service.api.WeixinService;
import net.icgear.product.att2005.xframe.iface.ProxyService;
import net.icgear.three.weixin.core.utils.WxUtils;
import net.icgear.three.weixin.open.intercept.WxOAuth2;

/**
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class QyWeixinRestImpl implements QyWeixinRest {

  @Inject
  private CheckInOutService cioService;

  @Inject
  private ApplyRecordService applyService;

  @Inject
  private ProxyService proxyService;

  @Inject
  private WeixinService wxService;

  @Inject
  private UserInfoService userService;

  /**
   * <p> 拉取考勤申请数据内容
   */
  @WxOAuth2
  @Override
  public List<ApplyInfoDto> fetchApplyInfos(Integer[] ids) {
    String openid = WxUtils.getWxOpenId(); // 获取当前登录人员的openid
    if (ids != null && ids.length > 0) {
      // 查询指定内容的考勤内容
      List<CheckInOut> ciss = cioService.findByIdIn(ids);
      return copyApplyInfoDto(ciss);
    } else if (openid != null && !openid.isEmpty()) {
      // 没有指定考勤ID，查找所有有问题的考勤内容，不限定于时间
      // 用户的openid
      // List<CheckInOut> ciss = cioService.searchErrList(openid);
      List<CheckInOut> ciss = cioService.findByUser_wxOpenIdAndStatus(openid, 1);
      return copyApplyInfoDto(ciss);
    } else {
      // 无法给出考勤内容
      return Collections.emptyList();
    }
  }

  /**
   * <p> 格式化数据内容
   * 
   * @param ciss
   * @return
   */
  private List<ApplyInfoDto> copyApplyInfoDto(List<CheckInOut> ciss) {
    // List<ApplyInfoDto> dtos = BCU.E2P(ciss, CheckInOut.class, ApplyInfoDto.class, 0);
    List<ApplyInfoDto> dtos = new ArrayList<>();
    DecimalFormat df = new DecimalFormat("0.00");
    for (CheckInOut cio : ciss) {
      ApplyInfoDto dto = new ApplyInfoDto();
      dto.setId(cio.getId());
      dto.setErrDate(cio.getAttDate());
      dto.setProject(cio.getProgram());
      dto.setStatus(cio.getStatus());
      
      if (cio.getActualTime() != null) {
        double time = cio.getActualTime().doubleValue() / 3600;
        dto.setActual(df.format(time));
      }

      List<ApplyRecord> rds = cio.getApplyRecords();
      // ApplyRecord ard = rds.isEmpty() ? null : rds.get(0);
      ApplyRecord ard = rds.stream().findFirst().orElse(null);
      if (ard != null) {
        dto.setErrType(CheckInOut.TYPE.values()[ard.getReasonType()].desc);

        Timestamp time = ard.getNewCheckIn() != null ? ard.getNewCheckIn() : ard.getOldCheckIn();
        if (time != null) {
          dto.setCheckInTime(time.toLocalDateTime().toLocalTime().toString());
        }

        time = ard.getNewCheckOut() != null ? ard.getNewCheckOut() : ard.getOldCheckOut();
        if (time != null) {
          dto.setCheckOutTime(time.toLocalDateTime().toLocalTime().toString());
        }

        dto.setUser(ard.getUser().getName());
        if (ard.getLoader().getLoader() != null) {
          dto.setLoader(ard.getLoader().getName());
        }
        dto.setReason(ard.getReason());
        dto.setComment(ard.getComment());
        dto.setStatus(ard.getStatus());
      } else {
        dto.setErrType(CheckInOut.TYPE.values()[cio.getAttType()].desc);
        if (cio.getCheckIn() != null) {
          dto.setCheckInTime(cio.getCheckIn().toLocalDateTime().toLocalTime().toString());
        }
        if (cio.getCheckOut() != null) {
          dto.setCheckOutTime(cio.getCheckOut().toLocalDateTime().toLocalTime().toString());
        }

        dto.setUser(cio.getUser().getName());
        if (cio.getLoader() != null) {
          dto.setLoader(cio.getLoader().getName());
        }
      }
      dtos.add(dto);
    }
    return dtos;
  }

  /**
   * <p> 申请考勤记录变更
   * 
   * <p> 可修改的内容有 <br> 签到时间 <br> 签退时间 <br> 异常类型 <br> 申请原因
   */
  @WxOAuth2
  @Override
  public ApplyInfoDto editApplyInfo(ApplyInfoDto dto) {
    Integer status = proxyService.executeST(null, v -> {
      CheckInOut cio = cioService.findBy(dto.getId());
      ApplyRecord ard = applyService.findOptionalByCheckInOut(cio);
      if (ard == null) {
        // 没有申请记录，执行构建
        ard = new ApplyRecord();

        ard.setCheckInOut(cio);
        ard.setOldCheckIn(cio.getCheckIn());
        ard.setOldCheckOut(cio.getCheckOut());
        ard.setUser(cio.getUser());
        ard.setLoader(cio.getLoader() != null ? cio.getLoader() : cio.getUser().getLoader());
        ard.setReasonType(cio.getAttType());
        ard.setStatus(2);

        ard = applyService.save(ard);
      }
      if (dto.getCheckInTime() != null && dto.getCheckInTime().length() == 5) {
        LocalDateTime time = cio.getAttDate().toLocalDateTime().toLocalDate().atTime(LocalTime.parse(dto.getCheckInTime()));
        ard.setNewCheckIn(Timestamp.valueOf(time));
      }
      if (dto.getCheckOutTime() != null && dto.getCheckOutTime().length() == 5) {
        LocalDateTime time = cio.getAttDate().toLocalDateTime().toLocalDate().atTime(LocalTime.parse(dto.getCheckOutTime()));
        if (time.getHour() < 6) {
          // 6点签退，认为是第二天打卡
          time.plusDays(1);
        }
        ard.setNewCheckOut(Timestamp.valueOf(time));
      }
      if (ard.getReasonType() != null && !TYPE.values()[ard.getReasonType()].desc.equals(dto.getErrType())) {
        for (TYPE type : TYPE.values()) {
          if (type.desc.equals(dto.getErrType())) {
            ard.setReasonType(type.ordinal());
            break;
          }
        }
      }
      ard.setReason(dto.getReason());
      if (ard.getLoader() == null && ard.getUser().isLoader()) {
        // 如果没有领导表示，而且自己就是领导
        ard.setWorker(ard.getUser());
        cio.setStatus(0);
        return 0; // 已经处理
      } else {
        ard.setWorker(ard.getLoader());
        ard.setStatus(2);
        wxService.sendApproval2Loader(ard);
        return 2; // 待审批
      }
    });
    dto.setStatus(status);
    return dto;
  }

  /**
   * <p> 忽略考勤记录
   */
  @WxOAuth2
  @Override
  public ApplyInfoDto ignoreApplyInfo(ApplyInfoDto dto) {
    CheckInOut cio = new CheckInOut();
    cio.setId(dto.getId());
    cio.setStatus(0);
    cio.setVersion(IA.FORCE_VERSION);
    cioService.merge2(cio);
    dto.setStatus(0);
    return dto;
  }

  // ----------------------------------------------分割线0

  /**
   * 
   */
  @WxOAuth2
  @Override
  public List<ApprovalInfoDto> fetchApprovalInfos(Integer[] ids) {
    String openid = WxUtils.getWxOpenId(); // 获取当前登录人员的openid
    if (ids != null && ids.length > 0) {
      // 查询指定内容的考勤内容
      List<ApplyRecord> applys = applyService.findByCheckInOut_idIn(ids);
      return copyApprovalInfoDto(applys);
    } else if (openid != null && !openid.isEmpty()) {
      // 没有指定考勤ID，查找所有有问题的考勤内容，不限定于时间
      // 用户的openid
      List<ApplyRecord> applys = applyService.findByWorker_wxOpenIdAndCheckInOut_statusNotEqual(openid, 0);
      return copyApprovalInfoDto(applys);
    } else {
      // 无法给出考勤内容
      return Collections.emptyList();
    }
  }

  /**
   * <p> 拷贝数据
   * 
   * @param applys
   * @return
   */
  private List<ApprovalInfoDto> copyApprovalInfoDto(List<ApplyRecord> applys) {
    List<ApprovalInfoDto> dtos = new ArrayList<>();
    for (ApplyRecord ard : applys) {
      ApprovalInfoDto dto = new ApprovalInfoDto();

      dto.setId(ard.getId());

      // dto.setCid(ard.getCheckInOut().getId());
      dto.setErrDate(ard.getCheckInOut().getAttDate());
      dto.setProject(ard.getCheckInOut().getProgram());

      dto.setErrType(CheckInOut.TYPE.values()[ard.getReasonType()].desc);

      if (ard.getOldCheckIn() != null) {
        dto.setCheckInTime(ard.getOldCheckIn().toLocalDateTime().toLocalTime().toString());
      }
      if (ard.getOldCheckOut() != null) {
        dto.setCheckOutTime(ard.getOldCheckOut().toLocalDateTime().toLocalTime().toString());
      }
      if (ard.getNewCheckIn() != null) {
        dto.setCheckInNew(ard.getNewCheckIn().toLocalDateTime().toLocalTime().toString());
      }
      if (ard.getNewCheckOut() != null) {
        dto.setCheckOutNew(ard.getNewCheckOut().toLocalDateTime().toLocalTime().toString());
      }

      dto.setUser(ard.getUser().getName());
      if (ard.getLoader().getLoader() != null) {
        dto.setLoader(ard.getLoader().getName());
      }
      dto.setReason(ard.getReason());
      dto.setComment(ard.getComment());
      dto.setStatus(ard.getStatus());

      dtos.add(dto);
    }
    return dtos;
  }

  /**
   * <p> 变更内容 <br> 批注
   */
  @WxOAuth2
  @Override
  public ApprovalInfoDto editApprovalInfo(ApprovalInfoDto dto) {
    ApplyRecord ard = new ApplyRecord();
    ard.setId(dto.getId());
    ard.setComment(dto.getComment());
    ard.setVersion(IA.FORCE_VERSION);
    applyService.merge2(ard);
    return dto;
  }

  /**
   * <p> 同意申请
   */
  @WxOAuth2
  @Override
  public ApprovalInfoDto agreeApprovalInfo(ApprovalInfoDto dto) {
//    if (dto.getCheckInTime() == null && dto.getCheckInNew() == null
//        || dto.getCheckOutTime() == null && dto.getCheckOutNew() == null) {
//      throw ECM.exception(-11, "异常请求：" + dto.getId(), "缺少签到或签退时间，请驳回申请");
//    }
    Integer status = proxyService.executeST(null, v -> {
      ApplyRecord ard = new ApplyRecord();
      ard.setId(dto.getId());
      ard.setStatus(0);
      ard.setVersion(IA.FORCE_VERSION);
      ApplyRecord pard = applyService.merge2(ard);
      pard.getCheckInOut().setStatus(0);
      cioService.recalculateAttendance(pard.getCheckInOut(), pard);
      return 0; // 结束处理
    });
    dto.setStatus(status);
    return dto;
  }

  /**
   * <p> 驳回申请
   */
  @WxOAuth2
  @Override
  public ApprovalInfoDto rejectApprovalInfo(ApprovalInfoDto dto) {
    Integer status = proxyService.executeST(null, v -> {
      ApplyRecord ard = new ApplyRecord();
      ard.setId(dto.getId());
      ard.setStatus(3); // 内容驳回，重新处理
      ard.setVersion(IA.FORCE_VERSION);
      ApplyRecord pard = applyService.merge2(ard);
      pard.setWorker(pard.getUser()); // 内容被驳回，需要重新修改
      wxService.sendApproval4Reject(pard);
      return 3; // 内容驳回
    });
    dto.setStatus(status);
    return dto;
  }

  // ----------------------------------------------分割线1

  /**
   * 
   */
  @WxOAuth2
  @Override
  public LoaderInfoDto fetchLoaderInfos(Integer[] ids) {
    List<UserInfo> loaders = userService.findByIsLoader(true);
    List<LoaderDto> list = BCU.P2E(loaders, LoaderDto.class, UserInfo.class, 1);
    LoaderInfoDto dto = new LoaderInfoDto();
    dto.setList(list);
    String openid = WxUtils.getWxOpenId();
    UserInfo user = userService.findOptionalByWxOpenId(openid);
    if (user.getLoader() == null) {
      dto.setIndex(-1);
    } else {
      Integer loaderId = user.getLoader().getId();
      for (int index = 0; index < list.size(); index++) {
        if (loaderId == list.get(index).getId()) {
          dto.setIndex(index);
          break;
        }
      }
    }
    return dto;
  }

  /**
   * 
   */
  @WxOAuth2
  @Override
  public ErrCode editSelfLoader(LoaderDto dto) {
    proxyService.executeRT(() -> {
      String openid = WxUtils.getWxOpenId();
      UserInfo user = userService.findOptionalByWxOpenId(openid);
      UserInfo loader = userService.findBy(dto.getId());
      if (user.getId() == loader.getId()) {
        throw ECM.exception(-11, "自己选择自己为自己的领导：" + user.getId(), "不可以选择自己为自己的上级");
      }
      user.setLoader(loader);
    });
    return ECM.success();
  }

}
