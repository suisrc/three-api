package net.icgear.product.att2005.att.rsapi;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.A_TestBean;

/**
 * build by tools[0.1-alpha] on Tue Nov 06 13:58:38 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface A_TestBeanRsApi extends SuperRsApi<A_TestBean, Integer> {

}