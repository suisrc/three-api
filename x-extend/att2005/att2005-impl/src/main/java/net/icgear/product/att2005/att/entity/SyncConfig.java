package net.icgear.product.att2005.att.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import net.icgear.im.core.persistence.IA;

@Entity
@Table(name = "sync_config")
public class SyncConfig implements IA<Integer> {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 10)
    private Integer id;
    
    /**
     * 执行cron
     */
    @Column(name = "exec_cron")
    private String execCron;
    
    /**
     * 上次执行的时间
     */
    @Column(name = "prefix_time")
    private Timestamp prefixTime;
    
    /**
     * 上次执行考勤计算时间
     */
    @Column(name = "check_in_out")
    private Timestamp checkInOut;
    
    /**
     * 下次执行的时间
     */
    @Column(name = "next_time")
    private Timestamp nextTime;
    
    /**
     * 执行的次数
     */
    @Column(name = "exec_count")
    private Integer execCount;
    
    /**
     * 描述
     */
    private String description;
    
    /**
     * 状态
     */
    private Integer status;
    
    /**
     * 名称
     */
    private String name;
    
    /**
     * 第一次执行时间
     */
    @Column(name = "frist_time")
    private Timestamp fristTime;

    /**
     * 数据版本号
     */
    @Version
    private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getExecCron() {
        return execCron;
    }

    public void setExecCron(String execCron) {
        this.execCron = execCron;
    }

    public Timestamp getPrefixTime() {
        return prefixTime;
    }

    public void setPrefixTime(Timestamp prefixTime) {
        this.prefixTime = prefixTime;
    }

    public Timestamp getNextTime() {
        return nextTime;
    }

    public void setNextTime(Timestamp nextTime) {
        this.nextTime = nextTime;
    }

    public Integer getExecCount() {
        return execCount;
    }

    public void setExecCount(Integer execCount) {
        this.execCount = execCount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getFristTime() {
        return fristTime;
    }

    public void setFristTime(Timestamp fristTime) {
        this.fristTime = fristTime;
    }

    public Timestamp getCheckInOut() {
      return checkInOut;
    }

    public void setCheckInOut(Timestamp checkInOut) {
      this.checkInOut = checkInOut;
    }
    
}
