package net.icgear.product.att2005.trest.dto;

import java.io.InputStream;

import javax.ws.rs.FormParam;

/**
 * 上传文件的内容信息
 * 
 * @author Y13
 *
 */
public class UpdateParam {
    
    /**
     * 文件的名称
     */
    @FormParam("name")
    private String name;
    
    /**
     * 文件流
     */
    @FormParam("file")
    private InputStream file;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public InputStream getFile() {
        return file;
    }

    public void setFile(InputStream file) {
        this.file = file;
    }

}
