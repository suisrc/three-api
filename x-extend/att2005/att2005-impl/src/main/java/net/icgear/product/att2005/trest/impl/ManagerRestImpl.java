package net.icgear.product.att2005.trest.impl;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.suisrc.core.message.ErrCode;

import net.icgear.im.common.bean.BCU;
import net.icgear.im.core.file.FileManager;
import net.icgear.im.ext.excel.response.ExcelWriterResponse;
import net.icgear.product.att2005.att.entity.CheckInOut;
import net.icgear.product.att2005.att.entity.SyncConfig;
import net.icgear.product.att2005.att.entity.SystemInfo;
import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.service.api.AttendanceRecordService;
import net.icgear.product.att2005.att.service.api.CheckInOutService;
import net.icgear.product.att2005.att.service.api.SyncConfigService;
import net.icgear.product.att2005.att.service.api.SystemInfoService;
import net.icgear.product.att2005.att.service.api.UserInfoService;
import net.icgear.product.att2005.excel.dto.AttendanceReportDto;
import net.icgear.product.att2005.trest.api.ManagerRest;
import net.icgear.product.att2005.trest.dto.ConfigBody;
import net.icgear.product.att2005.trest.dto.SendAtterrBody;
import net.icgear.product.att2005.wx.service.api.WeixinService;
import net.icgear.product.att2005.xframe.iface.ProxyService;

/**
 * <p> 考勤
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class ManagerRestImpl implements ManagerRest {

  @Inject
  private AttendanceRecordService recordService;
  @Inject
  private SystemInfoService systemService;
  @Inject
  private SyncConfigService configService;
  @Inject
  private CheckInOutService checkService;
  @Inject
  private ProxyService proxyService;
  @Inject
  private WeixinService wxService;
  @Inject
  private UserInfoService userService;

  private volatile boolean isRunning = false;

  @Override
  public ErrCode syncData(String sercet, String time) {
    ErrCode err = checkSercet(sercet);
    if (err != null) {
      return err;
    }
    SystemInfo ssi = systemService.findBy("system_init");
    if (time == null && ssi != null && "true".equals(ssi.getValue())) {
      ErrCode ecode = new ErrCode();
      ecode.setErrcode("401");
      ecode.setErrmsg("服务器已经被初始，不要重复初始化");
      return ecode;
    }

    try {
      isRunning = true;
      // 记录开始同步时间
      Timestamp now = new Timestamp(System.currentTimeMillis());
      // 同步考勤记录到数据库中
      Timestamp start = null;
      if (time != null) {
        start = Timestamp.valueOf(time + " 00:00:00.0");
      }
      recordService.syncAttRecord(start, null, "系统初始化同步");
      // 获取当月1号时间
      // LocalDateTime.now().with(TemporalAdjusters.firstDayOfMonth());
      LocalDateTime day1 = LocalDate.now().withDayOfMonth(1).atStartOfDay();
      checkService.syncCheckInOut(Timestamp.valueOf(day1), null, "系统初始化同步");
      // 考勤定时器初始化
      SyncConfig sc = new SyncConfig();
      sc.setStatus(0);
      sc.setDescription("系统初始化默认产生执行任务");
      sc.setName("推送出勤");
      sc.setExecCron("0 0 6 * * ?");
      sc.setExecCount(1);
      sc.setFristTime(now);
      sc.setPrefixTime(now);
      configService.save(sc); // 初始化第一启动配置
      // 提交执行的内容
      configService.execute(sc);
    } finally {
      isRunning = false;
    }

    // 更新执行状态
    if (ssi == null) {
      ssi = new SystemInfo();
      ssi.setKey("system_init");
    }
    ssi.setValue("true");
    systemService.save(ssi);

    ErrCode ec = new ErrCode();
    ec.setErrcode("0");
    ec.setErrmsg("同步完成");
    return ec;
  }

  /**
   * 验证密钥
   * 
   * @param sercet
   * @return
   */
  private ErrCode checkSercet(String sercet) {
    if (isRunning) {
      ErrCode ecode = new ErrCode();
      ecode.setErrcode("401");
      ecode.setErrmsg("任务正在执行，请稍后重试");
      return ecode;
    }
    if (sercet == null || sercet.isEmpty()) {
      ErrCode ecode = new ErrCode();
      ecode.setErrcode("403");
      ecode.setErrmsg("没有检测到授权密钥，请给出授权密钥");
      return ecode;
    }
    SystemInfo ss = systemService.findBy("system_sercet");
    if (ss == null || !sercet.equals(ss.getValue())) {
      ErrCode ecode = new ErrCode();
      ecode.setErrcode("403");
      ecode.setErrmsg("密钥不匹配，请检测密钥后重试");
      return ecode;
    }
    return null;
  }

  /**
   * 
   */
  @Override
  public ErrCode syncCurrentMonth(String sercet, String time, boolean remove) {
    ErrCode err = checkSercet(sercet);
    if (err != null) {
      return err;
    }
    Timestamp start;
    if (time == null) {
      LocalDateTime day1 = LocalDate.now().withDayOfMonth(1).atStartOfDay();
      start = Timestamp.valueOf(day1);
    } else {
      start = Timestamp.valueOf(time + " 00:00:00.0");
    }
    if (remove) {
      checkService.removeByAttDateGreaterThanEquals(start);
    }
    checkService.syncCheckInOut(start, null, "手动重新计算考勤同步考勤");
    ErrCode ec = new ErrCode();
    ec.setErrcode("0");
    ec.setErrmsg("重新计算考勤完成");
    return ec;
  }

  /**
   * 
   */
  @Override
  public ErrCode editSysSyncConfig(String sercet, ConfigBody body) {
    ErrCode err = checkSercet(sercet);
    if (err != null) {
      return err;
    }
    SyncConfig scn = proxyService.executeST(body, b -> {
      SyncConfig sc;
      if (body.getId() == null) {
        // 构建新规则
        sc = configService.save(new SyncConfig());
      } else {
        // 更新配置
        sc = configService.findBy(body.getId());
      }
      if (body.getStatus() != null) {
        sc.setStatus(body.getStatus());
      }
      if (body.getName() != null) {
        sc.setName(body.getName());
      }
      if (body.getDescription() != null) {
        sc.setDescription(body.getDescription());
      }
      return sc;
    });
    configService.execute(scn);
    ErrCode ec = new ErrCode();
    ec.setErrcode("0");
    ec.setErrmsg("配置信息的内容已经更新");
    return ec;
  }

  /**
   * 推送考勤异常
   */
  @Override
  public ErrCode sendAtterr(String sercet, SendAtterrBody body) {
    ErrCode err = checkSercet(sercet);
    if (err != null) {
      return err;
    }
    if (body.getTime() != null) {
      ErrCode ecode = new ErrCode();
      ecode.setErrcode("453");
      ecode.setErrmsg("暂时不提供精确的某一天内容推送");
      return ecode;
    }
    UserInfo user = userService.findOptionalBySnNo(body.getSsn());
    if (user == null) {
      ErrCode ecode = new ErrCode();
      ecode.setErrcode("453");
      ecode.setErrmsg("无法查询到用户：" + body.getSsn());
      return ecode;
    }
    // 当月第一天
    Timestamp time = Timestamp.valueOf(LocalDate.now().withDayOfMonth(1).atStartOfDay());
    List<CheckInOut> cios = checkService.findByUserAndAttDateGreaterThanEqualsAndStatus(user, time, 1); // 查看有问题考勤
    if (cios.isEmpty()) {
      ErrCode ecode = new ErrCode();
      ecode.setErrcode("0");
      ecode.setErrmsg("该人当前无异常考勤");
      return ecode;
    }
    wxService.sendCheckInOuts(cios, time, new Timestamp(System.currentTimeMillis()));
    ErrCode ecode = new ErrCode();
    ecode.setErrcode("0");
    ecode.setErrmsg("考勤消息已经推送");
    return ecode;
  }

  @Override
  public Response exportReport1(String sercet, String time) {
    ErrCode err = checkSercet(sercet);
    if (err != null) {
      return Response.ok(err).type(MediaType.APPLICATION_JSON).build();
    }
    Timestamp start;
    Timestamp end = null;
    if (time == null) {
      // 当月第一次天
      start = Timestamp.valueOf(LocalDate.now().withDayOfMonth(1).atStartOfDay());
    } else {
      start = Timestamp.valueOf(time + "-01 00:00:00.0");
      LocalDateTime next = start.toLocalDateTime().plusMonths(1);
      end = Timestamp.valueOf(next);
    }
    // 获取当月所有考勤纪律
    List<CheckInOut> cios = end == null ? checkService.findByAttDateGreaterThanEquals(start) : // 查看有问题考勤
        checkService.findByAttDateGreaterThanEqualsAndAttDateLessThan(start, end);
    List<AttendanceReportDto> dtos = BCU.P2E(cios, AttendanceReportDto.class, CheckInOut.class, 1);
    List<AttendanceReportDto> dto1 =
        dtos.stream().sorted((l, r) -> l.getName().compareTo(r.getName())).collect(Collectors.toList());
    return ExcelWriterResponse.EWR(AttendanceReportDto.class, FileManager.getDefault()).process("考勤", "考勤", dto1);
  }


}
