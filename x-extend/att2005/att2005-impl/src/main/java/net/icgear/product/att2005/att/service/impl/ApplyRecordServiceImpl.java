package net.icgear.product.att2005.att.service.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.im.core.persistence.service.AbstractService;
import net.icgear.im.core.persistence.service.RepoHandler;
import net.icgear.im.core.persistence.service.Service;
import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.repo.api.ApplyRecordRepository;
import net.icgear.product.att2005.att.rsapi.ApplyRecordRsApi;
import net.icgear.product.att2005.att.service.api.ApplyRecordService;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 *
 */
@Service
@ApplicationScoped
public abstract class ApplyRecordServiceImpl extends AbstractService<ApplyRecord, Integer> 
        implements ApplyRecordService, SuperService<ApplyRecord, Integer>, ApplyRecordRsApi, SuperRsApi<ApplyRecord, Integer> {
    
    @Inject
    @RepoHandler
    private ApplyRecordRepository repo;

}
