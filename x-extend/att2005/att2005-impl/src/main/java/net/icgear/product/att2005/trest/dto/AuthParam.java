package net.icgear.product.att2005.trest.dto;

import javax.ws.rs.QueryParam;

/**
 * 上传文件的内容信息
 * 
 * @author Y13
 *
 */
public class AuthParam {
    
    /**
     * 查看文件列表token
     */
    @QueryParam("token")
    private String token;
    
    /**
     * 系统启动密钥
     */
    @QueryParam("secret")
    private String secret;
    
    /**
     * 文件位置
     */
    @QueryParam("path")
    private String path;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
