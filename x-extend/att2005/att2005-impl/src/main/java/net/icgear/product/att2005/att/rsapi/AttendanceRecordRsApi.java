package net.icgear.product.att2005.att.rsapi;

import java.sql.Timestamp;
import java.util.List;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.AttendanceRecord;
import net.icgear.product.att2005.att.entity.UserInfo;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 14:19:10 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface AttendanceRecordRsApi extends SuperRsApi<AttendanceRecord, Integer> {

    /**
     * 获取出勤记录
     * @param time
     * @return
     */
    List<AttendanceRecord> findByTimeGreaterThanEqualsOrderByTimeAsc(Timestamp time);
    
    /**
     * 获取出勤记录
     * @param time
     * @param endtime
     * @return
     */
    List<AttendanceRecord> findByTimeGreaterThanEqualsAndTimeLessThanOrderByTimeAsc(Timestamp time1, Timestamp time2);
    
    /**
     * 
     * @return
     */
    List<AttendanceRecord> findTop1OrderByTimeDesc();
    
    /**
     * <p> 获取用户指定时间下的考勤记录
     * @param user 用户
     * @param time 时间
     * @return
     */
    List<AttendanceRecord> findByUserAndTime(UserInfo user, Timestamp time);
}