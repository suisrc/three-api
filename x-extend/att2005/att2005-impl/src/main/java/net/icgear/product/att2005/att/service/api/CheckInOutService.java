package net.icgear.product.att2005.att.service.api;

import java.sql.Timestamp;
import java.util.List;

import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.entity.CheckInOut;
import net.icgear.product.att2005.att.rsapi.CheckInOutRsApi;
import net.icgear.product.att2005.dto.CheckInOutRuleDto;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * 
 * @author Y13-Tools(auto)
 *
 */
public interface CheckInOutService extends SuperService<CheckInOut, Integer>, CheckInOutRsApi {

  /**
   * <p> 同步出勤情况
   * 
   * @param time
   * @param desc
   */
  void syncCheckInOut(Timestamp stateTime, Timestamp endTime, String desc);

  /**
   * <p> 起初计算过的考勤
   * 
   * @param valueOf
   */
  void removeByAttDateGreaterThanEquals(Timestamp time);

  /**
   * <p> 查询异常列表
   * 
   * @return
   */
  List<CheckInOut> searchErrList(String openid);

  /**
   * 
   * @param checkInOut
   * @param pard 
   */
  void recalculateAttendance(CheckInOut checkInOut, ApplyRecord pard);
  
  /**
   * 
   * @return
   */
  CheckInOutRuleDto getCheckInOutRuleDto();

}
