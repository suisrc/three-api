package net.icgear.product.att2005.trest.api;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

import com.suisrc.core.message.ErrCode;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import net.icgear.product.att2005.trest.dto.AuthParam;
import net.icgear.product.att2005.trest.dto.FileInfoDto;
import net.icgear.product.att2005.trest.dto.UpdateParam;

/**
 * 获取文件配置API接口
 * 
 * @author Y13
 *
 */
@Api(value = "77.0 资源管理器")
@Path("resources")
public interface ResourceRest {


    /**
     * 通过临时资源索引下载文件
     * 
     * 请注意，这里的index是文件加密索引
     * 
     */
    @ApiOperation(value = "从服务器下载资源")
    @GET
    @Path("temp/download")
    @Produces(MediaType.APPLICATION_JSON)
    Response downloadResourceByIndex(@QueryParam("index") @NotNull(message = "请指定资源的索引") String index);
    
    /**
     * 从服务器下载公共文件
     */
    @ApiOperation(value = "从服务器下载公共资源")
    @GET
    @Path("public")
    @Produces(MediaType.APPLICATION_JSON)
    Response downloadPublicResource(@QueryParam("resource") @NotNull(message = "请指定资源的名称") String resource);
    
    /**
     * 向服务器上传文件（向服务器上次文件是危险的操作，所以需要授权，得到授权才可以操作，强调登录状态）
     */
    
    @ApiOperation(value = "向服务器上传文件")
    @ApiImplicitParams({ 
        @ApiImplicitParam(paramType = "form", name = "name", dataType = "string", value="文件名称，注意，中文会发生乱码"),
        @ApiImplicitParam(paramType = "form", name = "file", dataType = "java.io.File", required = false, value = "上传文件") })
    @POST
    @Path("upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA + "; charset=GBK")
    @Produces(MediaType.APPLICATION_JSON)
    ErrCode updateResource(@BeanParam AuthParam param1, @MultipartForm UpdateParam param2);
    
    /**
     * 获取服务器的文件列表（只能看到文件列表，不能下载数据）
     */
    @ApiOperation(value = "向服务器上传文件")
    @POST
    @Path("list")
    @Produces(MediaType.APPLICATION_JSON)
    List<FileInfoDto> getResourceList(@BeanParam AuthParam param);
}
