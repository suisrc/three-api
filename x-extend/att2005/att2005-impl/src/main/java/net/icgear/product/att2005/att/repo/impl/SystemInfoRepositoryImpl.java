package net.icgear.product.att2005.att.repo.impl;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

import net.icgear.im.core.persistence.repo.AbstractRepository;
import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.SystemInfo;
import net.icgear.product.att2005.att.repo.api.SystemInfoRepository;
import net.icgear.product.att2005.att.rsapi.SystemInfoRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:42 CST 2018
 * @author Y13-Tools(auto)
 * 
 */
@ApplicationScoped
@Repository(forEntity=SystemInfo.class)
public abstract class SystemInfoRepositoryImpl  extends AbstractRepository<SystemInfo, String> 
        implements  CriteriaSupport<SystemInfo>, SystemInfoRepository, SuperRepository<SystemInfo, String>,
        SystemInfoRsApi, SuperRsApi<SystemInfo, String> {
}
