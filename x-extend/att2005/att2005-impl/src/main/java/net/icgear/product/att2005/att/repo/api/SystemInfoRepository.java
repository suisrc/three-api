package net.icgear.product.att2005.att.repo.api;

import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.product.att2005.att.entity.SystemInfo;
import net.icgear.product.att2005.att.rsapi.SystemInfoRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:42 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface SystemInfoRepository extends SuperRepository<SystemInfo, String>, SystemInfoRsApi {
}
