package net.icgear.product.att2005.att.rsapi;

import java.sql.Timestamp;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.Holiday;

/**
 * build by tools[0.1-alpha] on Tue Sep 25 14:13:12 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface HolidayRsApi extends SuperRsApi<Holiday, Timestamp> {

    /**
     * 
     * @param time
     * @param status
     * @return
     */
    Holiday findOptionalByIdAndStatus(Timestamp time, Integer status);
}