package net.icgear.product.att2005.trest.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.apache.deltaspike.core.api.config.ConfigProperty;

import com.suisrc.core.message.ECM;
import com.suisrc.core.message.ErrCode;
import com.suisrc.core.reference.RefVal;
import com.suisrc.core.utils.FileUtils;

import net.icgear.im.core.file.FileManager;
import net.icgear.im.core.util.ResponseUtils;
import net.icgear.product.att2005.trest.api.ResourceRest;
import net.icgear.product.att2005.trest.dto.AuthParam;
import net.icgear.product.att2005.trest.dto.FileInfoDto;
import net.icgear.product.att2005.trest.dto.UpdateParam;

/**
 * 资源下载
 * @author Y13
 *
 */
@ApplicationScoped
public class ResourceRestImpl implements ResourceRest {
    
    /**
     * 应用
     */
    @Inject 
    private FileManager manager;
    
    @Inject @ConfigProperty(name="application.files.file-secret")
    private String fileSecret;
    
    @Inject @ConfigProperty(name="application.files.file-rule")
    private String fileRule;

    //---------------------------------------------------------------------小小分割线 XXX 白名单发生了变化
    /**
     * 通过资源临时访问索引下载资源
     * 
     * 临时文件访问和下载是不受白名单限制
     */
    @Override
    public Response downloadResourceByIndex(String index) {
        try {
            RefVal<String> name = new RefVal<>();
            InputStream inputStream = manager.getResourceStreamByIndex(index, name::set);
            if (inputStream == null) {
                throw ECM.exception("RESOURCE-404-001-1", "没有文件1", index);
            }
            int offset = name.get().lastIndexOf('/');
            String resource = offset < 0 ? name.get() : name.get().substring(offset + 1);
            return ResponseUtils.download(inputStream, resource).build();
        } catch (IOException e) {
            System.err.println(e.getMessage());
            throw ECM.exception("RESOURCE-404-002-1", "没有文件2", index);
        }
    }

    /**
     * 公共文件下载受默认白名单限制
     */
    @Override
    public Response downloadPublicResource(String resource) {
        String name = FileUtils.formatPath(FileUtils.getPath("public", resource));
        
        try {
            InputStream inputStream = manager.getFileInputStream(name);
            if (inputStream == null) {
                throw ECM.exception("RESOURCE-404-003-1", resource);
            }
            int offset = resource.lastIndexOf('/');
            String filename = offset < 0 ? resource : resource.substring(offset + 1);
            return ResponseUtils.download(inputStream, filename).build();
        } catch (IOException e) {
            System.err.println(e.getMessage());
            throw ECM.exception("RESOURCE-404-004-1", resource);
        }
    }

    /**
     * 验证权限
     * @param param
     */
    private void checkAuth(AuthParam param) {
        if (param.getToken() == null || param.getSecret() == null || fileSecret == null || fileRule == null) {
            throw ECM.exception("RESOURCE-403-001", "");
        }
        if (!param.getSecret().equals(fileSecret)) {
            // 判断密钥
            throw ECM.exception("RESOURCE-403-004", "");
        }
        try {
            InputStream rule = manager.getFileInputStream(FileUtils.getPath(param.getPath(), fileRule));
            if (rule == null) {
                throw ECM.exception("RESOURCE-403-002", "");
            }
            RefVal<Boolean> check = new RefVal<Boolean>(false);
            // 判断文件授权
            FileUtils.getContent(rule, v -> {
              if (v.equals(param.getToken())) {
                  check.set(true);
                  return null; // 结束读取内容
              } else {
                  return "1"; // 继续读取下一行
              }
            });
            rule.close(); // 结束处理
            if (!check.get()) {
                throw ECM.exception("RESOURCE-403-005", "");
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw ECM.exception("RESOURCE-403-003", "");
        }
    }

    /**
     * 上传文档  
     */
    @Override
    public ErrCode updateResource(AuthParam param1, UpdateParam param2) {
        checkAuth(param1);
        try {
            OutputStream out = manager.getFileOutputStream(FileUtils.getPath(param1.getPath(), param2.getName()));
            if (out == null) {
                throw ECM.exception("RESOURCE-404-006-2", param1.getPath(), param2.getName());
            }
            FileUtils.copyStream(param2.getFile(), out);
            out.close();
            param2.getFile().close();
        } catch (IOException e) {
            e.printStackTrace();
            throw ECM.exception("RESOURCE-404-005-2", param1.getPath(), param2.getName());
        }
        return ECM.success();
    }

    /**
     * 获取文件列表
     */
    @Override
    public List<FileInfoDto> getResourceList(AuthParam param) {
        checkAuth(param);
        List<FileInfoDto> list = new ArrayList<>();
        manager.getFolderInfo(param.getPath(), folder -> {
            for(File file : folder.listFiles()) {
                FileInfoDto fid = new FileInfoDto();
                fid.setName(file.getName());
                fid.setModifyTime(new Date(file.lastModified()).toString());
                if (file.isDirectory()) {
                    fid.setType("DIR");
                }
                list.add(fid);
            }
        });
        return list;
    }

}
