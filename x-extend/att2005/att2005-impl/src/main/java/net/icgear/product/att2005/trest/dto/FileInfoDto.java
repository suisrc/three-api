package net.icgear.product.att2005.trest.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 上传文件的内容信息
 * 
 * @author Y13
 *
 */
@ApiModel("resource.FileInfoDto")
public class FileInfoDto {
    
    /**
     * 文件名称
     */
    @ApiModelProperty("文件名称")
    private String name;
    
    /**
     * 文件类型
     */
    @ApiModelProperty("文件类型")
    private String type;
    
    /**
     * 文件大小
     */
    @ApiModelProperty("文件大小")
    private String length;
    
    /**
     * 最后修改时间
     */
    @ApiModelProperty("最后修改时间")
    private String modifyTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

}
