package net.icgear.product.att2005.att.service.api;

import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.SyncLog;
import net.icgear.product.att2005.att.rsapi.SyncLogRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface SyncLogService extends SuperService<SyncLog, Integer>, SyncLogRsApi {

}
