package net.icgear.product.att2005.xframe.impl;

import javax.enterprise.context.ApplicationScoped;

import net.icgear.im.core.user.iface.LoginUser;
import net.icgear.im.core.user.iface.LoginUserService;

/**
 * <p> 控制用户登录
 * @author Y13
 *
 */
@SuppressWarnings("rawtypes")
@ApplicationScoped
public class LoginUserServiceImpl implements LoginUserService {

  /**
   * 获取当前应用服务中的用户信息
   */
  @Override
  public LoginUser findOptionalByAccount(String userId) {
    // FIXME 获取用户登录信息
    return null;
  }

}
