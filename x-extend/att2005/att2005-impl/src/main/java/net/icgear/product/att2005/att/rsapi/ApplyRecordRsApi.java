package net.icgear.product.att2005.att.rsapi;

import java.util.List;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.entity.CheckInOut;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface ApplyRecordRsApi extends SuperRsApi<ApplyRecord, Integer> {

  /**
   * 
   * @param cio
   * @return
   */
  ApplyRecord findOptionalByCheckInOut(CheckInOut cio);
  
  /**
   * worker.wxOpenid, checkInOut.status
   * @param openid
   * @param i
   * @return
   */
  List<ApplyRecord> findByWorker_wxOpenIdAndCheckInOut_statusNotEqual(String openid, Integer status);
  
  /**
   * 
   * @param ids
   * @return
   */
  List<ApplyRecord> findByCheckInOut_idIn(Integer[] checkInOutIds);
  
  /**
   * 获取无统计异常时间的内容
   * @return
   */
  List<ApplyRecord> findByActualTimeIsNull();
}