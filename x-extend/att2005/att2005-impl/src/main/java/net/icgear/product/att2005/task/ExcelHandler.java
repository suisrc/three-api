package net.icgear.product.att2005.task;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.icgear.im.common.bean.BCU;
import net.icgear.im.core.mail.MailHandler;
import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.entity.CheckInOut;
import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.service.api.CheckInOutService;
import net.icgear.product.att2005.excel.ExcelWriter2;
import net.icgear.product.att2005.excel.dto.AttendanceReportDto;
import net.icgear.product.att2005.utils.MailUtils;
import net.icgear.three.weixin.core.WxConfig;

/**
 * <p> Excel导出
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class ExcelHandler {
  private static final Logger loger = Logger.getLogger(ExcelHandler.class.getName());

  @Inject
  private CheckInOutService checkService;

  /**
   * 邮件发送
   */
  @Inject
  private MailHandler mailService;


  /**
   * <p> 导出Excel
   * 
   * @param key
   * @param user
   * @param config
   * @return String 异常的内容
   */
  public String exportCheckOutInInfo(String key, UserInfo user, WxConfig config, String type) {
    if (user.getEmail() == null) {
      loger.info("用户没有绑定邮箱地址，无法发送导出结果，终止导出：" + user.getId());
      return "您没有绑定有效邮箱地址，执行失败，绑定邮箱地址请发送“编辑我的邮箱:xxx@icgear.net”。注意：邮箱必须是公司邮箱";
    }
    LocalDateTime now = LocalDateTime.now();
    Integer monthOff = Integer.valueOf(key);
    // 当月第一天
    LocalDateTime curMonth = now.toLocalDate().withDayOfMonth(1).atStartOfDay();
    // 跳转到需要计算月的第一天
    LocalDateTime staMonth = curMonth.plusMonths(monthOff.longValue());
    // 计算终止月第一天
    LocalDateTime endMonth = staMonth.plusMonths(1);
    // 取出考勤数据
    List<CheckInOut> cios = null;
    if ("all".equals(type) && user.isAdmin()) {
      // 所有人的信息
      cios = checkService.findByAttDateGreaterThanEqualsAndAttDateLessThan(Timestamp.valueOf(staMonth), Timestamp.valueOf(endMonth));
    } else if ("group".equals(type) && user.isLoader()) {
      // 本项目组的信息，不包含自己
      cios = checkService.findByAttDateGreaterThanEqualsAndAttDateLessThanAndLoader(
          Timestamp.valueOf(staMonth), Timestamp.valueOf(endMonth), user);
    } else if ("self".equals(type)){
      // 自己的信息
      cios = checkService.findByAttDateGreaterThanEqualsAndAttDateLessThanAndUser(
          Timestamp.valueOf(staMonth), Timestamp.valueOf(endMonth), user);
    } else {
      return "对不起，您无权进行该导出Excel操作。";
    }
    if (cios == null || cios.isEmpty()) {
      return "对不起，查询的月没有有效考勤数据，无法导出。";
    }
    // 构建申请记录查询索引
    Map<Integer, ApplyRecord> arm = new HashMap<>();
    Map<Integer, CheckInOut> cim = new HashMap<>();
    cios.forEach(v -> {
      if (!v.getApplyRecords().isEmpty()) {
        arm.put(v.getId(), v.getApplyRecords().get(0));
      }
      cim.put(v.getId(), v);
    });

    List<AttendanceReportDto> dtos = BCU.P2E(cios, AttendanceReportDto.class, CheckInOut.class, 1);
    // 增加申请纪律数据
    dtos.forEach(v -> {
      ApplyRecord ar = arm.get(v.getId());
      if (ar != null) {
        // 修正考勤纪律数据
        // 修正考勤时间，原因，批注，上级, 类型
        if (ar.getNewCheckIn() != null) {
          v.setCheckIn2(ar.getNewCheckIn());
        }
        if (ar.getNewCheckOut() != null) {
          v.setCheckOut2(ar.getNewCheckOut());
        }
        v.setReason(ar.getReason());
        v.setComment(ar.getComment());
        v.setAttType(ar.getReasonType());
        v.setLoader(ar.getLoader().getName());
        if (ar.getStatus() == 0) {
          v.setStatus(10); // 标记内容处理
        } else {
          v.setStatus(11); // 正在处理
        }
        if (v.getProgram() == null) {
          v.setProgram(ar.getLoader().getProgram());
        }
        if (ar.getActualTime() != null) {
          v.setActualTime(ar.getActualTime());
        }
        if (ar.getBalanceTime() != null) {
          v.setBalanceTime(ar.getActualTime());
        }
      }
      if (v.getProgram() == null || v.getLoader() == null) {
        CheckInOut ci = cim.get(v.getId());
        if (ci != null && ci.getUser() != null && ci.getUser().getLoader() != null) {
          if (v.getLoader() == null) {
            v.setLoader(ci.getUser().getLoader().getName());
          }
          if (v.getProgram() == null) {
            if (ci.getUser().isLoader()) {
              v.setProgram(ci.getUser().getProgram());
            } else {
              v.setProgram(ci.getUser().getLoader().getProgram());
            }
          }
        }
      }
    });

    List<AttendanceReportDto> dto1 =
        dtos.stream().sorted((l, r) -> l.getName().compareTo(r.getName())).collect(Collectors.toList());
    long expiredIn = 24 * 60 * 60; // 有效期为1天
    String fileUri = ExcelWriter2.EWR(AttendanceReportDto.class).process(expiredIn, "考勤", "考勤", dto1);
    if (fileUri == null) {
      return "对不起， 导出Excel过程中失败，详情请查看系统日志";
    }
    String month = staMonth.format(DateTimeFormatter.ofPattern("yyyy-MM"));
    // 发送邮件给用户
    String prefixKey = "application.report.key1.mail.";
    String title = System.getProperty(prefixKey + "title", "导出报表下载");
    String userName = "";
    String exportTime = now.toString();
    String systemName = System.getProperty(prefixKey + "system", "考勤通知");
    String fileName = "《考勤_" + month + "》";
    // String fileUri;
    String expiredTime = now.plusSeconds(expiredIn).toString();
    String signature = System.getProperty(prefixKey + "signature", "我就是小可爱的化身团队");
    String date = LocalDateTime.now().toString();

    MailUtils.sendReport2Email(mailService, user.getEmail(), title, userName, exportTime, systemName, fileName, fileUri,
        expiredTime, signature, date);
    // 完成导出操作
    return null;
  }

}
