package net.icgear.product.att2005.att.service.impl;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.im.core.persistence.service.AbstractService;
import net.icgear.im.core.persistence.service.RepoHandler;
import net.icgear.im.core.persistence.service.Service;
import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.SystemInfo;
import net.icgear.product.att2005.att.repo.api.SystemInfoRepository;
import net.icgear.product.att2005.att.rsapi.SystemInfoRsApi;
import net.icgear.product.att2005.att.service.api.SystemInfoService;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:42 CST 2018
 * @author Y13-Tools(auto)
 *
 */
@Service
@ApplicationScoped
public abstract class SystemInfoServiceImpl extends AbstractService<SystemInfo, String> 
        implements SystemInfoService, SuperService<SystemInfo, String>, SystemInfoRsApi, SuperRsApi<SystemInfo, String> {
    
    @Inject
    @RepoHandler
    private SystemInfoRepository repo;

}
