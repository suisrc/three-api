package net.icgear.product.att2005.trest.dto;

/**
 * <p> 申请内容
 * 
 * @author Y13
 *
 */
public class ApprovalInfoDto extends ApplyInfoDto {

  /**
   * <p> 考勤id
   */
  private Integer cid;
  
  /**
   * <p> 出勤时间
   */
  private String checkInNew;

  /**
   * <p> 退勤时间
   */
  private String checkOutNew;
  
  public Integer getCid() {
    return cid;
  }

  public void setCid(Integer cid) {
    this.cid = cid;
  }

  public String getCheckInNew() {
    return checkInNew;
  }

  public void setCheckInNew(String checkInNew) {
    this.checkInNew = checkInNew;
  }

  public String getCheckOutNew() {
    return checkOutNew;
  }

  public void setCheckOutNew(String checkOutNew) {
    this.checkOutNew = checkOutNew;
  }

}
