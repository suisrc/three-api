package net.icgear.product.att2005.trest.dto;

import java.util.List;

import net.icgear.im.common.bean.OriginType;
import net.icgear.product.att2005.att.entity.UserInfo;

/**
 * 
 * @author Y13
 *
 */
public class LoaderInfoDto {

  @OriginType(clazz = UserInfo.class, e2p = false)
  public static class LoaderDto {

    private Integer id;
    
    private String name;

    public Integer getId() {
      return id;
    }

    public void setId(Integer id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }
    
  }
  
  private List<LoaderDto> list;
  
  private Integer index;

  public List<LoaderDto> getList() {
    return list;
  }

  public void setList(List<LoaderDto> list) {
    this.list = list;
  }

  public Integer getIndex() {
    return index;
  }

  public void setIndex(Integer index) {
    this.index = index;
  }
  
}
