package net.icgear.product.att2005.trest.dto;

import java.sql.Timestamp;

/**
 * 推送考勤异常
 * @author Y13
 *
 */
public class SendAtterrBody {

    /**
     * 用户编码
     */
    private String ssn;
    
    /**
     * 日期
     * 如果为空，表示推送本月所有异常
     */
    private Timestamp time;

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
    
}
