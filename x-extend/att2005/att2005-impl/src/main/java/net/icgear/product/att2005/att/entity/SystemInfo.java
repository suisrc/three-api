package net.icgear.product.att2005.att.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import net.icgear.im.core.persistence.IA;

@Entity
@Table(name = "system_info")
public class SystemInfo implements IA<String> {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="si_key")
    private String key;

    @Column(name="si_value")
    private String value;
    
    @Version
    private Integer version;


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String getId() {
        return key;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

}
