package net.icgear.product.att2005.scheduler;

import java.sql.Timestamp;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.cdise.api.ContextControl;

import com.suisrc.core.Global;
import com.suisrc.core.scheduler.ScheduleHandler;
import com.suisrc.core.scheduler.Scheduled;

import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.service.api.ApplyRecordService;
import net.icgear.product.att2005.att.service.api.CheckInOutService;
import net.icgear.product.att2005.dto.CheckInOutRuleDto;
import net.icgear.product.att2005.utils.AttUtils;
import net.icgear.product.att2005.xframe.iface.ProxyService;

/**
 * <p> 只在启动时候执行一次，重新计算考勤时间结余
 * 
 * @author Y13
 *
 */
@Scheduled(first = 10, onStartup = false, onStartupKey = "application.schedule.config.valid2")
@ApplicationScoped
public class RecalculateActualScheduler implements ScheduleHandler {
  
  @Inject
  private ContextControl control;
  @Inject
  private ApplyRecordService recordService;
  @Inject
  private CheckInOutService checkInOutService;
  @Inject
  private ProxyService proxyService;
  

  /**
   * 执行作业内容
   */
  @Override
  public void run() {
    try {
      Global.getLogger().info("开始执行重新计算时间结余数据003");
      control.startContext(RequestScoped.class);
      proxyService.executeRT(() -> {
        // 计算出勤时间异常内容
        List<ApplyRecord> res = recordService.findByActualTimeIsNull();
        // 获取考勤执行使用的规则对象
        CheckInOutRuleDto rule = checkInOutService.getCheckInOutRuleDto();
        for (ApplyRecord pard : res) {
          // 考勤计算规则
          Timestamp checkIn = pard.getNewCheckIn() != null ? pard.getNewCheckIn() : pard.getOldCheckIn();
          Timestamp checkOut = pard.getNewCheckOut() != null ? pard.getNewCheckOut() : pard.getOldCheckOut();
          if (checkIn == null || checkOut == null) {
            continue; // 不再计算
          }
          int workTime = AttUtils.getActualTime(rule, checkIn, checkOut);
          // 有效工时
          pard.setActualTime(workTime);
          // 当天时间结余
          pard.setBalanceTime(workTime - pard.getCheckInOut().getWorkTime());
        }
      });
    } catch (Exception e) {
      e.printStackTrace();
    }finally {
      control.stopContext(RequestScoped.class);
      Global.getLogger().info("结束执行重新计算时间结余数据003");
    }
  }
}
