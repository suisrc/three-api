package net.icgear.product.att2005.att.repo.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

import net.icgear.im.core.persistence.IA;
import net.icgear.im.core.persistence.repo.AbstractRepository;
import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.entity.ApplyRecord_;
import net.icgear.product.att2005.att.entity.CheckInOut;
import net.icgear.product.att2005.att.repo.api.ApplyRecordRepository;
import net.icgear.product.att2005.att.rsapi.ApplyRecordRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 * 
 */
@ApplicationScoped
@Repository(forEntity=ApplyRecord.class)
public abstract class ApplyRecordRepositoryImpl  extends AbstractRepository<ApplyRecord, Integer> 
        implements  CriteriaSupport<ApplyRecord>, ApplyRecordRepository, SuperRepository<ApplyRecord, Integer>,
        ApplyRecordRsApi, SuperRsApi<ApplyRecord, Integer> {
  
  /**
   * <p>
   * 
   * @param ids
   * @return
   */
  @Override
  public List<ApplyRecord> findByCheckInOut_idIn(Integer[] checkInOutIds) {
    // return findEntitys(h -> Arrays.asList(h.r().get(ApplyRecord_.checkInOut)
    // .get(CheckInOut_.id).in(Arrays.asList(ids))), null, null);
    List<CheckInOut> cios = new ArrayList<>();
    for (Integer id : checkInOutIds) {
      CheckInOut cio = new CheckInOut();
      cio.setId(id);
      cio.setVersion(IA.NOMER_VERSION);
      cios.add(cio);
    }
    return findEntitys(h -> Arrays.asList(h.r().get(ApplyRecord_.checkInOut).in(cios)), null, null);
  }
}
