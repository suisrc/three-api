package net.icgear.product.att2005.xframe.impl;

import java.util.function.Function;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.jpa.api.transaction.Transactional;

import net.icgear.product.att2005.xframe.iface.ProxyService;

/**
 * 特殊需求的代理服务
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class ProxyServiceImpl implements ProxyService {

    @Override
    public void executeR(Runnable run) {
        run.run();
    }

    @Override
    @Transactional
    public void executeRT(Runnable run) {
        run.run();
    }

    @Override
    public <T, R> R executeS(T t, Function<T, R> sup) {
        return sup.apply(t);
    }

    @Override
    @Transactional
    public <T, R> R executeST(T t, Function<T, R> sup) {
        return sup.apply(t);
    }

}
