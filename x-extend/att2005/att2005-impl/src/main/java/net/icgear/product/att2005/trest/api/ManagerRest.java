package net.icgear.product.att2005.trest.api;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.suisrc.core.message.ErrCode;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.icgear.product.att2005.trest.dto.ConfigBody;
import net.icgear.product.att2005.trest.dto.SendAtterrBody;

/**
 * 微信权限管理
 * @author Y13
 */
@Api(value = "2.2 数据管理接口")
@Path("mng")
public interface ManagerRest {

    @ApiOperation(value = "将考勤机数据同步到数据库中")
    @GET
    @Path("system/sync/inout")
    @Produces(MediaType.APPLICATION_JSON)
    ErrCode syncData(@QueryParam("sercet") String sercet,
            @QueryParam("time") String time);


    @ApiOperation(value = "同步当月考勤数据,注意，用户系统初始化，不可用于运维时候调用")
    @GET
    @Path("system/sync/att")
    @Produces(MediaType.APPLICATION_JSON)
    ErrCode syncCurrentMonth(@QueryParam("sercet") String sercet,
            @QueryParam("time") String time,
            @QueryParam("remove") boolean remove); // 删除已经存在的内容

    @ApiOperation(value = "变更定时器内容")
    @POST
    @Path("system/sync/config")
    @Produces(MediaType.APPLICATION_JSON)
    ErrCode editSysSyncConfig(@QueryParam("sercet") String sercet, ConfigBody body);

    @ApiOperation(value = "推动考勤异常")
    @POST
    @Path("system/send/atterr")
    @Produces(MediaType.APPLICATION_JSON)
    ErrCode sendAtterr(@QueryParam("sercet") String sercet, SendAtterrBody body);

    @ApiOperation(value = "导出第一类考勤报表")
    @GET
    @Path("system/export/att/report1")
    @Produces(MediaType.APPLICATION_JSON)
    Response exportReport1(@QueryParam("sercet") String sercet, @QueryParam("time")String time);
}
