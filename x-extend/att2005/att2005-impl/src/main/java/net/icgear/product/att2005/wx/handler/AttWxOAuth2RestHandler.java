package net.icgear.product.att2005.wx.handler;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import com.suisrc.core.Global;
import com.suisrc.core.message.ECM;
import com.suisrc.jaxrsapi.core.ApiActivator;

import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.service.api.UserInfoService;
import net.icgear.product.att2005.xframe.iface.ProxyService;
import net.icgear.three.weixin.core.WxConfig;
import net.icgear.three.weixin.core.WxConsts;
import net.icgear.three.weixin.core.oauth2.WxOAuth2RestHandler;
import net.icgear.three.weixin.core.rest.AbstractWxBindingOne;
import net.icgear.three.weixin.open.handler.DefaultWxOAuth2RestHandler;
import net.icgear.three.weixin.qyapi.rest.api.UserManagerRest;
import net.icgear.three.weixin.qyapi.rest.dto.user.UserResult;

/**
 * 
 * @author Y13
 *
 */
@Named("weixin-oauth2-att")
@Dependent
public class AttWxOAuth2RestHandler extends DefaultWxOAuth2RestHandler implements WxOAuth2RestHandler {
  private static final Logger logger = Logger.getLogger(AttWxOAuth2RestHandler.class);

  @Inject
  private UserInfoService userService;
  @Inject
  private ProxyService proxyService;

  @Override
  public boolean doUserTicket2Redirect(Object owner, String openid, String scope, String key, String userTicket, boolean isFirst) {
    if (!isFirst) {
      return true;
    }
    AbstractWxBindingOne one = (AbstractWxBindingOne) owner;
    ApiActivator activator = (ApiActivator) one.getConfig();
    // 只有公司人员才能使用，公司人员的openid中“:”前是公司id
    UserManagerRest userMngRest = activator.getApiImplement(UserManagerRest.class);
    // 用于更新用户信息
    int offset = openid.indexOf(':');
    if (offset < 0) {
      return false; // 不是公司成员，不进行处理
    }
    String corpId = openid.substring(0, offset);
    String userid = openid.substring(offset + 1);
    UserResult rs = userMngRest.getUser(userid);
    // 同步用户信息
    boolean res = false;

    String msg = "同步账户信息：";
    if (!rs.isErr()) {
      // 数据发生异常
      res = syncUserInfo(corpId, openid, rs);
      msg += String.format("openid: %s, 已完成同步用户【%s】电话号码“%s”的个人信息的操作。操作：%s", openid, rs.getName(), rs.getMobile(),
          res ? "同步成功" : "同步失败");
    } else {
      msg += "无法从微信服务器获取用户信息：" + rs.getErrmsg();
    }
    logger.info(msg);
    if (!res) {
      // 同步失败， 放弃cookie内容,放弃权限认证
      Global.getThreadCache().remove(WxConsts.COOKIE_OPEN_ID2);
    } else {
      // 确定当前人员是否有领导，如果没有领导，跳转到领导选择页面
      // 放弃后面的跳转，进入直接跳转
      // checkHasLoader(openid, activator);
    }

    // 同步数据异常，无法执行
    return res;
  }
  
  /**
   * <p> 选择上级
   */
  @Override
  public String getRedirectUri(WxConfig config, String openid, String key) {
    UserInfo user = userService.findOptionalByWxOpenId(openid);
    if (user == null || user.getLoader() != null || user.isAdmin()) {
      // 有上级，或者是管理员是不需要选择上级的
      return null; 
    }
    // 没有领导，跳转到上级选择页面
    return config.getWebIndex("select-loader");
  }

  /**
   * 
   * @param openid
   * @param user
   * @return
   */
  private boolean syncUserInfo(String corpid, String openid, UserResult userRs) {
    return proxyService.executeST(userRs, rs -> {
      UserInfo user = userService.findOptionalByPhone(userRs.getMobile());
      if (user == null) {
        user = userService.findOptionalByName(userRs.getName());
      }
      if (user == null) {
        // logger.info("没有找到考勤人员：" + userRs.getName());
        // return false;
        throw ECM.exception(-11, "公司内部无法找到您，请确认你的名字是否正确：" + userRs.getName());
      }
      user.setWxOpenId(openid);
      user.setWxCorpId(corpid);
      user.setWxUserId(rs.getUserid());
      user.setWxUserName(rs.getName());
      user.setPhone(rs.getMobile());
      return true;
    });

  }
}
