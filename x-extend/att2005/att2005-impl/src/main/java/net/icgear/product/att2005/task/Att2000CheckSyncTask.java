package net.icgear.product.att2005.task;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.suisrc.core.Global;
import com.suisrc.core.corn.ICron;

import net.icgear.product.att2005.att.entity.SyncConfig;
import net.icgear.product.att2005.att.service.api.AttendanceRecordService;
import net.icgear.product.att2005.att.service.api.CheckInOutService;
import net.icgear.product.att2005.att.service.api.SyncConfigService;
import net.icgear.product.att2005.xframe.iface.ProxyService;

@ApplicationScoped
public class Att2000CheckSyncTask {


  @Inject
  private SyncConfigService configService;
  @Inject
  private AttendanceRecordService recordService;
  @Inject
  private CheckInOutService checkService;
  @Inject
  private ProxyService proxyService;

  /**
   * <p> 执行调用配置信息
   */
  private Timestamp nextTime = null;

  public Timestamp getNextTime() {
    return nextTime;
  }

  public void setNextTime(Timestamp nextTime) {
    this.nextTime = nextTime;
  }

  /**
   * <p> 考勤数据同步
   */
  public void run(boolean record, boolean checkInout) {
    try {
      Global.getLogger().info("开始执行同步考勤数据");
      proxyService.executeRT(() -> this.runIteration(record, checkInout));
    } catch (Exception e) {
      e.printStackTrace();
      // 不能抛出异常，否则定时器会终止执行
      // throw e;
      nextTime = null; // 防止死循环
    } finally {
      Global.getLogger().info("结束执行同步考勤数据");
    }
  }

  /**
   * <p> 执行考勤数据同步
   */
  private void runIteration(boolean record, boolean checkInout) {
    // 记录开始同步时间
    Timestamp currTime = new Timestamp(System.currentTimeMillis());

    // 获取配置信息
    List<SyncConfig> configs = configService.findByStatus(0);
    if (configs.isEmpty()) {
      return; // 无法计算
    }
    SyncConfig sc = configs.get(0); // 支取第一个配置信息
    if (record) {
      // 同步考勤记录
      Timestamp startTime = sc.getPrefixTime();
      recordService.syncAttRecord(startTime, currTime, "系统同步考勤数据同步-定时器001");
      sc.setPrefixTime(currTime);
    }
    if (checkInout) {
      // 统计考勤数据
      LocalDate nowDate = LocalDate.now();
      Timestamp checkSTime =
          sc.getCheckInOut() != null ? sc.getCheckInOut() : Timestamp.valueOf(nowDate.minusDays(1).atStartOfDay());

      Timestamp checkETime = Timestamp.valueOf(nowDate.atStartOfDay());
      if (checkETime.getTime() - checkSTime.getTime() > 23 * 60 * 60 * 1000) {
        // 时间差必须保证1天，由于时间统计有毫秒误差，所以这里统计大于23小时，就认为大于等于1天
        checkService.syncCheckInOut(checkSTime, checkETime, "系统考勤数据计算-定时器001");
        sc.setCheckInOut(checkETime);
      }
    }

    // 考勤记录
    sc.setExecCount(sc.getExecCount() + 1);
    nextTime = getNextExecTime(sc.getExecCron(), currTime);
    sc.setNextTime(nextTime);
    // 提交执行的内容
    configService.execute(sc);
  }

  /**
   * <p> 同步执行时间
   * 
   * @return
   */
  public Timestamp getNextExecTime(String cron, Timestamp currTime) {
    if (cron == null) {
      List<SyncConfig> configs = configService.findByStatus(0);
      if (configs.isEmpty()) {
        return null; // 无法计算
      }
      SyncConfig sc = configs.get(0); // 支取第一个配置信息
      if (sc.getExecCron() == null) {
        return null;
      }
      cron = sc.getExecCron();
    }
    if (cron == null || cron.isEmpty()) {
      return null;
    }
    if (currTime == null) {
      currTime = new Timestamp(System.currentTimeMillis());
    }
    try {
      ICron icron = ICron.build(cron);
      long time = icron.getTimeAfter(currTime).getTime();
      return new Timestamp(time);
    } catch (ParseException e) {
      e.printStackTrace();
      return null;
    }
  }
}
