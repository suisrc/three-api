package net.icgear.product.att2005.att.service.impl;

import java.sql.Timestamp;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.im.core.persistence.service.AbstractService;
import net.icgear.im.core.persistence.service.RepoHandler;
import net.icgear.im.core.persistence.service.Service;
import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.Holiday;
import net.icgear.product.att2005.att.repo.api.HolidayRepository;
import net.icgear.product.att2005.att.rsapi.HolidayRsApi;
import net.icgear.product.att2005.att.service.api.HolidayService;

/**
 * build by tools[0.1-alpha] on Tue Sep 25 14:13:12 CST 2018
 * @author Y13-Tools(auto)
 *
 */
@Service
@ApplicationScoped
public abstract class HolidayServiceImpl extends AbstractService<Holiday, Timestamp> 
        implements HolidayService, SuperService<Holiday, Timestamp>, HolidayRsApi, SuperRsApi<Holiday, Timestamp> {
    
    @Inject
    @RepoHandler
    private HolidayRepository repo;

}
