package net.icgear.product.att2005.att.repo.impl;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

import net.icgear.im.core.persistence.repo.AbstractRepository;
import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.SyncConfig;
import net.icgear.product.att2005.att.repo.api.SyncConfigRepository;
import net.icgear.product.att2005.att.rsapi.SyncConfigRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 * 
 */
@ApplicationScoped
@Repository(forEntity=SyncConfig.class)
public abstract class SyncConfigRepositoryImpl  extends AbstractRepository<SyncConfig, Integer> 
        implements  CriteriaSupport<SyncConfig>, SyncConfigRepository, SuperRepository<SyncConfig, Integer>,
        SyncConfigRsApi, SuperRsApi<SyncConfig, Integer> {
}
