package net.icgear.product.att2005.att.service.api;

import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.SyncConfig;
import net.icgear.product.att2005.att.rsapi.SyncConfigRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface SyncConfigService extends SuperService<SyncConfig, Integer>, SyncConfigRsApi {

    /**
     * 执行数据拉取
     * 
     * @param sc
     */
    void execute(SyncConfig sc);

}
