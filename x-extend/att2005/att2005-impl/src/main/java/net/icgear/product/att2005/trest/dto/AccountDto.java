package net.icgear.product.att2005.trest.dto;

/**
 * 用于信息
 * 
 * @author Y13
 *
 */
public class AccountDto {
    
    /**
     * 用于openid
     */
    private String openid;

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }
}
