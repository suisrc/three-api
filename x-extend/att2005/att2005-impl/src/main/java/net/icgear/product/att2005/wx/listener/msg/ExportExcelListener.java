package net.icgear.product.att2005.wx.listener.msg;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.suisrc.core.Global;
import com.suisrc.core.reference.RefVal;
import com.suisrc.three.core.listener.inf.Listener;

import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.service.api.UserInfoService;
import net.icgear.product.att2005.task.ExcelHandler;
import net.icgear.three.weixin.core.event.qy.menu.ClickEvent;
import net.icgear.three.weixin.core.listener.AbstractLockListener;
import net.icgear.three.weixin.core.reply.ReplyTextMessage;
import net.icgear.three.weixin.core.rest.AbstractWxBindingOne;

/**
 * TODO 仅仅写了个大概，需要完善功能
 * 
 * 导出Excel操作
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class ExportExcelListener extends AbstractLockListener<ClickEvent> implements Listener<ClickEvent> {

  private static final String KEY = "export_excel_";

  /**
   * 用户管理
   */
  @Inject
  private UserInfoService userService;

  @Inject
  private ExcelHandler excelService;

  /**
   * 处理结果
   */
  @Override
  protected boolean processResult(RefVal<Object> result, ClickEvent bean, AbstractWxBindingOne owner) {
    Global.getLogger().info("开始执行导出Excel操作");
    //
    if (!bean.getToUserName().equals(owner.getConfig().getWxAppId())) {
      return true; // 内容无法处理，需要跳过该处理内容
    }
    String userid = bean.getFromUserName();
    UserInfo user = userService.findOptionalByWxUserId(userid);
    if (user == null) {
      ReplyTextMessage msg = bean.reverse(new ReplyTextMessage());
      msg.setContent("对不起，无法验证您的身份，拒绝请求。");
      result.set(msg);
      return false;
    }
    String valKey = bean.getEventKey().substring(KEY.length());
    int offset = valKey.indexOf('_');
    if (offset < 0) {
      ReplyTextMessage msg = bean.reverse(new ReplyTextMessage());
      msg.setContent("对不起，接到到的执行无法解析，请联系系统管理员处理：" + valKey);
      result.set(msg);
      return false;
    }
    String key = valKey.substring(offset + 1);
    String type = valKey.substring(0, offset);

    // 执行导出Excel操作
    String content = excelService.exportCheckOutInInfo(key, user, owner.getConfig(), type);
    // 处理返回消息
    ReplyTextMessage msg = bean.reverse(new ReplyTextMessage());
    result.set(msg);
    if (content == null) {
      msg.setContent("Excel导出成功，下载地址已经发送到邮箱【" + user.getEmail() + "】,请注意查收");
    } else {
      msg.setContent(content);
    }
    return false;
  }

  /**
   * 处理异常
   */
  @Override
  protected boolean processException(String key, Exception e, RefVal<Object> result, ClickEvent bean, Object owner) {
    e.printStackTrace();
    // 不要影响系统正常运行
    ReplyTextMessage msg = bean.reverse(new ReplyTextMessage());
    msg.setContent("对不起，导出Excel发生异常，详情请查考系统日志");
    result.set(msg);
    return false;
  }

  /**
   * Excel导出事件
   */
  @Override
  public boolean matches(boolean hasResult, ClickEvent bean) {
    if (bean.getEventKey().startsWith(KEY)) {
      return true;
    }
    return false;
  }
}
