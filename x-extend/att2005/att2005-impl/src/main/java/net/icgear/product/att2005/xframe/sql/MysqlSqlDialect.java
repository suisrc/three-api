package net.icgear.product.att2005.xframe.sql;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;

import net.icgear.im.core.persistence.sql.AbstractSqlDialect;
import net.icgear.im.core.persistence.sql.SqlDialect;

/**
 * 用户直接sql内容的执行
 * 
 * globalAlternatives.net.icgear.im.core.persistence.sql.SqlDialect=net.icgear.product.lbhd.frame.sql.MysqlSqlDialect
 * 
 * @author Y13
 *
 */
@Alternative
@ApplicationScoped
public class MysqlSqlDialect extends AbstractSqlDialect implements SqlDialect {

}
