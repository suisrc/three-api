package net.icgear.product.att2005.att.service.api;

import net.icgear.im.core.persistence.service.SuperService;
import net.icgear.product.att2005.att.entity.A_TestBean;
import net.icgear.product.att2005.att.rsapi.A_TestBeanRsApi;

/**
 * build by tools[0.1-alpha] on Tue Nov 06 13:58:38 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface A_TestBeanService extends SuperService<A_TestBean, Integer>, A_TestBeanRsApi {

}
