package net.icgear.product.att2005.utils;

import java.sql.Time;
import java.sql.Timestamp;

import net.icgear.product.att2005.dto.CheckInOutRuleDto;

public class AttUtils {

  /**
   * <p> 获取有效工作时间
   * 
   * @param rule
   * @param checkIn
   * @param checkOut
   * @return
   */
  public static int getActualTime(CheckInOutRuleDto rule, Timestamp checkIn, Timestamp checkOut) {
    Time inTime = Time.valueOf(checkIn.toLocalDateTime().toLocalTime());
    Time outTime = Time.valueOf(checkOut.toLocalDateTime().toLocalTime());
    if (inTime.after(outTime) // 考勤时间异常
        || inTime.after(rule.getOffWorktime()) // 签到时间晚于下班时间
        || outTime.before(rule.getOnWorktime())) { // 签退时间早于上班时间
      // 签退早于签到，无法计算
      return 0;
    }
    Time offset = null;
    // -----------------------------------计算早退时间
    Time leval1 = null;
    Time leval2 = null;
    offset = null;
    if (outTime.before(rule.getNigthBreakStartTime())) {
      if (outTime.before(rule.getLunchBreakStartTime())) {
        // 在上午发生早退
        leval1 = outTime;
      } else if (outTime.before(rule.getLunchBreakEndTime())) {
        // 午休时间早退
        leval1 = rule.getLunchBreakStartTime();
      } else {
        // 下午发生早退
        leval2 = outTime;
      }
    }
    // -----------------------------------计算标准工时
    int worktime = 0;
    offset = null;
    if (inTime.before(rule.getOnWorktime())) {
      // 早班
      offset = rule.getOnWorktime();
    }
    if (offset == null && inTime.before(rule.getLunchBreakStartTime())) {
      // 上午迟到
      offset = inTime;
    }
    if (offset != null) {
      // 计算上午工时
      if (leval1 != null) {
        worktime += leval1.getTime() - offset.getTime();
        return worktime / 1000; // 上午已经签退
      } else {
        worktime += rule.getLunchBreakStartTime().getTime() - offset.getTime();
      }
    }
    offset = null;
    if (inTime.before(rule.getLunchBreakEndTime())) {
      // 午班
      offset = rule.getLunchBreakEndTime();
    }
    if (offset == null && inTime.before(rule.getNigthBreakStartTime())) {
      // 下午迟到
      offset = inTime;
    }
    if (offset != null) {
      // 计算下午工时
      if (leval2 != null) {
        worktime += leval2.getTime() - offset.getTime();
        return worktime / 1000; // 下午已经签退
      } else {
        worktime += rule.getNigthBreakStartTime().getTime() - offset.getTime();
      }
    }
    // 发生加班
    if (outTime.after(rule.getNigthBreakEndTime())) {
      worktime += outTime.getTime() - rule.getNigthBreakEndTime().getTime();
    }
    return worktime / 1000; // 结果集统计换算为秒
  }
}
