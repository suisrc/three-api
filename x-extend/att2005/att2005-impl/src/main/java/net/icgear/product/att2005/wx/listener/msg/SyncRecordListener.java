package net.icgear.product.att2005.wx.listener.msg;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.suisrc.core.reference.RefVal;
import com.suisrc.three.core.listener.inf.Listener;

import net.icgear.product.att2005.att.entity.UserInfo;
import net.icgear.product.att2005.att.service.api.UserInfoService;
import net.icgear.product.att2005.task.Att2000CheckSyncTask;
import net.icgear.three.weixin.core.msg.TextMessage;
import net.icgear.three.weixin.core.reply.ReplyTextMessage;
import net.icgear.three.weixin.core.rest.AbstractWxBindingOne;

/**
 * TODO 仅仅写了个大概，需要完善功能
 * @author Y13
 *
 */
@ApplicationScoped
public class SyncRecordListener implements Listener<TextMessage> {
  
  /**
   * 用户管理
   */
  @Inject private UserInfoService userService;
  
  /**
   * 同步服务
   */
  @Inject private Att2000CheckSyncTask taskService;

  /**
   * 
   */
  @Override
  public boolean accept(RefVal<Object> result, TextMessage bean) {
    return false;
  }

  /**
   * 
   */
  @Override
  public boolean accept(RefVal<Object> result, TextMessage bean, Object owner) {
    try {
      AbstractWxBindingOne one = (AbstractWxBindingOne) owner;
      if (bean.getToUserName().equals(one.getConfig().getWxAppId())) {
        accept(result, one, bean);
      }
    } catch (Exception e) {
      e.printStackTrace();
      // 不要影响系统正常运行
    }
    return accept(result, bean);
  }
  
  @Override
  public boolean matches(boolean hasResult, TextMessage bean) {
    if (bean.getContent().equals("同步考勤机原始数据")) {
      return true;
    }
    return false;
  }

  /**
   * 执行同步操作
   * @param result
   * @param one
   * @param bean
   */
  private void accept(RefVal<Object> result, AbstractWxBindingOne one, TextMessage bean) {
    String userid = bean.getFromUserName();
    UserInfo user = userService.findOptionalByWxUserId(userid);
    if (user == null || !user.isAdmin()) {
      ReplyTextMessage msg = bean.reverse(new ReplyTextMessage());
      msg.setContent("对不起，你不是管理员，无法执行同步操作");
      result.set(msg);
      return;
    }
    taskService.run(true, false);
    ReplyTextMessage msg = bean.reverse(new ReplyTextMessage());
    msg.setContent("已经执行过同步考勤记录操作");
    result.set(msg);
    
  }
}
