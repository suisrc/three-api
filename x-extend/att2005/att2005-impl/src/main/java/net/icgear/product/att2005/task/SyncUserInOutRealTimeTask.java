package net.icgear.product.att2005.task;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.enterprise.context.ApplicationScoped;

/**
 * 用于实时同步用户信息
 * 
 * @author Y13
 *
 */
@ApplicationScoped
public class SyncUserInOutRealTimeTask {

  public static void main(String[] args) throws UnknownHostException, IOException {
    // 要连接的服务端IP地址和端口
    String host = "192.168.0.8";
    // host = "127.0.0.1";
    int port = 4370;
    // 与服务端建立连接
    Socket socket = new Socket(host, port);

    // 建立连接后获得输出流
    byte p0[] = { /* Packet 4 */
        0x50, 0x50, (byte) 0x82, 0x7d, 0x08, 0x00, 0x00, 0x00, (byte) 0xe8, 0x03, 0x17, (byte) 0xfc, 0x00, 0x00, 0x00, 0x00};

    OutputStream output = socket.getOutputStream();
    output.write(p0);
    output.flush();

    InputStream input = socket.getInputStream();
    int buf;
    while ((buf = input.read()) != -1) {
      System.out.println(Integer.toHexString(buf));
    }
    input.close();
    socket.close();

    // System.out.println("OK Client");
  }

}
