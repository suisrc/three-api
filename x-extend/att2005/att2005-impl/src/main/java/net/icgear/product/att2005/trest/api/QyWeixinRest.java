package net.icgear.product.att2005.trest.api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.suisrc.core.message.ErrCode;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.icgear.product.att2005.trest.dto.ApplyInfoDto;
import net.icgear.product.att2005.trest.dto.ApprovalInfoDto;
import net.icgear.product.att2005.trest.dto.LoaderInfoDto;
import net.icgear.product.att2005.trest.dto.LoaderInfoDto.LoaderDto;

/**
 * 微信权限管理
 * 
 * @author Y13
 */
@Api(value = "2.1 企业微信数据通信接口")
@Path("qy")
public interface QyWeixinRest {

  @ApiOperation(value = "获取异常考勤申请")
  @GET
  @Path("applys")
  @Produces(MediaType.APPLICATION_JSON)
  List<ApplyInfoDto> fetchApplyInfos(@QueryParam("id") Integer[] ids);

  @ApiOperation(value = "变更异常考勤申请")
  @POST
  @Path("apply/att")
  @Produces(MediaType.APPLICATION_JSON)
  ApplyInfoDto editApplyInfo(ApplyInfoDto dto);

  @ApiOperation(value = "忽略异常考勤申请")
  @POST
  @Path("apply/ignore")
  @Produces(MediaType.APPLICATION_JSON)
  ApplyInfoDto ignoreApplyInfo(ApplyInfoDto dto);
  
  //---------------------------------------------------TODO 分割线0

  @ApiOperation(value = "获取异常考勤审核")
  @GET
  @Path("approvals")
  @Produces(MediaType.APPLICATION_JSON)
  List<ApprovalInfoDto> fetchApprovalInfos(@QueryParam("id") Integer[] ids);

  @ApiOperation(value = "变更异常考勤审核")
  @POST
  @Path("approval/update")
  @Produces(MediaType.APPLICATION_JSON)
  ApprovalInfoDto editApprovalInfo(ApprovalInfoDto dto);
  
  @ApiOperation(value = "同意异常考勤审核")
  @POST
  @Path("approval/agree")
  @Produces(MediaType.APPLICATION_JSON)
  ApprovalInfoDto agreeApprovalInfo(ApprovalInfoDto dto);

  @ApiOperation(value = "忽略异常考勤审核")
  @POST
  @Path("approval/reject")
  @Produces(MediaType.APPLICATION_JSON)
  ApprovalInfoDto rejectApprovalInfo(ApprovalInfoDto dto);
  
  //---------------------------------------------------TODO 分割线1
  
  @ApiOperation(value = "获取可以选择的上级")
  @GET
  @Path("user/loaders")
  @Produces(MediaType.APPLICATION_JSON)
  LoaderInfoDto fetchLoaderInfos(@QueryParam("id") Integer[] ids);

  @ApiOperation(value = "变更自己的上级")
  @POST
  @Path("user/loader/update")
  @Produces(MediaType.APPLICATION_JSON)
  ErrCode editSelfLoader(LoaderDto dto);
}
