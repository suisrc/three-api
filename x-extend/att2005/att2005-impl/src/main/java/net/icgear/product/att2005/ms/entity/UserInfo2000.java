package net.icgear.product.att2005.ms.entity;

import javax.persistence.Column;
import javax.persistence.Id;

/**
 * 店长月报表
 * 
 * @author user
 */
//@Entity
//@Table(name = "USERINFO")
public class UserInfo2000 {

    @Id
    @Column(name = "USERID")
    private Integer id;
    
    @Column(name = "SSN")
    private String ssn;
    
    @Column(name = "Name")
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
