package net.icgear.product.att2005.trest.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api("99.0 标准框架测试接口")
@Path("test")
public interface A_TestRest {

  @ApiOperation(nickname="ATR_0001", value = "测试服务器运行状况")
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  String testServer();
  
  @ApiOperation(nickname="ATR_0002", value = "测试数据库连接状况")
  @GET
  @Path("db")
  @Produces(MediaType.TEXT_PLAIN)
  String testDatabase();

}
