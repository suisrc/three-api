package net.icgear.product.att2005.att.repo.impl;

import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.apache.deltaspike.data.api.Repository;
import org.apache.deltaspike.data.api.criteria.CriteriaSupport;

import net.icgear.im.core.persistence.repo.AbstractRepository;
import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.im.core.persistence.repo.SuperRsApi;
import net.icgear.product.att2005.att.entity.CheckInOut;
import net.icgear.product.att2005.att.entity.CheckInOut_;
import net.icgear.product.att2005.att.repo.api.CheckInOutRepository;
import net.icgear.product.att2005.att.rsapi.CheckInOutRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * 
 * @author Y13-Tools(auto)
 * 
 */
@ApplicationScoped
@Repository(forEntity = CheckInOut.class)
public abstract class CheckInOutRepositoryImpl extends AbstractRepository<CheckInOut, Integer>
    implements CriteriaSupport<CheckInOut>, CheckInOutRepository, SuperRepository<CheckInOut, Integer>, CheckInOutRsApi,
    SuperRsApi<CheckInOut, Integer> {

  /**
   * <p>
   * 
   * @param ids
   * @return
   */
  public List<CheckInOut> findByIdIn(Integer[] ids) {
    return findEntitys(h -> Arrays.asList(h.r().get(CheckInOut_.id).in(Arrays.asList(ids))), null, null);
  }
}
