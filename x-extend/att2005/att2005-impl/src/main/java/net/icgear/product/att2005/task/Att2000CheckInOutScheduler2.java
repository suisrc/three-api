package net.icgear.product.att2005.task;

import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.apache.deltaspike.cdise.api.ContextControl;

import com.suisrc.core.scheduler.ScheduleHandler;
import com.suisrc.core.service.AbstractScheduledService.Schedule;

/**
 * <p> 定时统计考勤记录和内容 <p> 默认希望每天6点指定发送
 * 
 * 当前定时器被放弃
 * 
 * @author Y13
 *
 */
@Deprecated
//@Scheduled(cron="${application.schedule.sync-in-out}")
//@Scheduled(delayf = true, onStartupKey = "application.schedule.config.valid", onStartup=false)
//@Scheduled
//@ApplicationScoped
public class Att2000CheckInOutScheduler2 implements ScheduleHandler {
  
  @Inject
  private ContextControl control;
  
  @Inject
  private Att2000CheckSyncTask task;

  private boolean isFirst = true;

  /**
   * 执行作业内容
   */
  @Override
  public void run() {
    try {
      control.startContext(RequestScoped.class);
      task.run(true, true);
    } finally {
      control.stopContext(RequestScoped.class);
    }
  }

  /**
   * <p> 计算下次执行时间
   */
  @Override
  public Long getNextDelay(Schedule pre) {
    if (isFirst) {
      // 第一次立即执行
      isFirst = false;
      return 5L;
    }
    Timestamp nextTime = task.getNextTime();
    if (nextTime == null) {
      try {
        control.startContext(RequestScoped.class);
        nextTime = task.getNextExecTime(null, null);
      } finally {
        control.stopContext(RequestScoped.class);
      }
    }
    if (nextTime == null) {
      return null;
    }
    long delay = TimeUnit.SECONDS.convert(nextTime.getTime() - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    task.setNextTime(null);
    return delay;
  }
}
