package net.icgear.product.att2005.att.repo.api;

import net.icgear.im.core.persistence.repo.SuperRepository;
import net.icgear.product.att2005.att.entity.ApplyRecord;
import net.icgear.product.att2005.att.rsapi.ApplyRecordRsApi;

/**
 * build by tools[0.1-alpha] on Fri Sep 21 18:31:18 CST 2018
 * @author Y13-Tools(auto)
 *
 */
public interface ApplyRecordRepository extends SuperRepository<ApplyRecord, Integer>, ApplyRecordRsApi {
}
