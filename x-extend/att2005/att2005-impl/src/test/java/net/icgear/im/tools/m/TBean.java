package net.icgear.im.tools.m;

import java.util.HashSet;
import java.util.Set;

import net.icgear.im.tools.entity.ToolsBuilderByEntity;
import net.icgear.product.att2005.att.entity.A_TestBean;

public class TBean {

  public static void main(String[] args) throws Exception {
    Set<String> includes = null;
    includes = new HashSet<>();
    // 增加需要处理的Bean
    includes.add("A_TestBean");
    Set<String> excludes = null;
    // excludes = new HashSet<>();
    // // 增加需要处理的Bean
    // excludes.add("A_TestBean");

    ToolsBuilderByEntity builder = ToolsBuilderByEntity.build(A_TestBean.class, false, excludes, includes);
    builder.buildFolder = true;
    builder.rebuildSource = false;
    builder.execute();
  }

}
