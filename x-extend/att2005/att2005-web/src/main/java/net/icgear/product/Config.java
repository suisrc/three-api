package net.icgear.product;

import java.io.File;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.ClassLoaderAsset;
import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.jaxrs.JAXRSArchive;

/**
 * 配置共通操作 用于版本变更使用
 * 
 * @author Y13
 *
 */
public class Config {
  private static final String WAR = "webapp.war";
  private static final String ROOT_KEY = "swarm.deployment.[" + WAR + "].root-path";
  private static final String PERS_KEY = "swarm.deployment.[" + WAR + "].persistence-path";
  private static final String PERSISTENCE_KEY = "META-INF/persistence.xml";
  private static final String BEANS_KEY = "META-INF/beans.xml";

  /**
   * 启动服务器
   * 
   * @param args
   * @throws Exception
   */
  public static void start(String... args) throws Exception {
    Swarm swarm = new Swarm(args).start();
    // 构建启动需要的war文件
    JAXRSArchive archive = ShrinkWrap.create(JAXRSArchive.class, WAR);
    archive.addModule("org.apache.httpcomponents", "main");// 增加httpclient
    archive.addAsResource(new ClassLoaderAsset(BEANS_KEY, Main.class.getClassLoader()), BEANS_KEY);
    String persistence = swarm.configView().resolve(PERS_KEY).withDefault("").getValue();
    File file = persistence.isEmpty() ? null : new File(persistence);
    if (file == null || !file.exists() || !file.isFile()) {
      if (Main.class.getClassLoader().getResource(PERSISTENCE_KEY) != null ) {
        archive.addAsResource(new ClassLoaderAsset(PERSISTENCE_KEY, Main.class.getClassLoader()), PERSISTENCE_KEY);
      }
    } else {
      archive.addAsResource(file, PERSISTENCE_KEY);
    }
    // 解析需要的依赖
    archive.addAllDependencies();
    String root = swarm.configView().resolve(ROOT_KEY).withDefault("api").getValue();
    archive.setContextRoot(root);
    swarm.deploy(archive); // 部署存档
  }
}
