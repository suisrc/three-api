  package net.icgear.product.test;

import net.icgear.product.Config;

/**
 * 程序入口
 * 
 * 该对象需要在pom.xml文件中进行配置
 * 
 * @author Y13
 *
 */
public class TMain1 {

    public static void main(String[] args) throws Exception {
        args = new String[]{"-sproject-1.yml"}; // 替换启动文件
        Config.start(args);
    }
}
